#-------------------------------------------------------------------------------------------------#
#
#  Company      : Micran, Department of Telecommunications
#
#  Engineer     : Pogodaev Danil
#
#-------------------------------------------------------------------------------------------------#
#
#  Library      : lib_ethernet / library of ethernet components
#
#  File Name    : directory .py (python 3)
#
#-------------------------------------------------------------------------------------------------#
#
#  Description  : Specify project libraries for Quartus. Used for 'q10x' folder.
#
#  Purpose      : Creating a text file 'directory.txt' with 'SEARCH_PATH' for the '.qsf' file.
#
#-------------------------------------------------------------------------------------------------#
#
#  Note         : Instructions
#
#                 1. Launch the python-script 'directory.py'
#                 2. Copy the contents (paths) of the text file 'directory .txt'
#                 3. Paste paths in the .qsf file (SEARCH_PATH)
#                 4. Launch the Quartus Compiler
#
#-------------------------------------------------------------------------------------------------#
#
#  Limitation   : Placement of the python-script file 'directory.py'
#
#                 ../hard/quartus/q10x/directory.py
#
#-------------------------------------------------------------------------------------------------#
#
#  Revision List
#
#  Version   Date       Author
#
#  1.0.0     16.09.16   Pogodaev Danil
#
#-------------------------------------------------------------------------------------------------#

#-------------------------------------------------------------------------------------------------#
# import
#-------------------------------------------------------------------------------------------------#

import os
import re

#-------------------------------------------------------------------------------------------------#
# path to folder 'quartus\q10x'
#-------------------------------------------------------------------------------------------------#

# path getting

path_q10x = os.path.dirname(__file__)
path_q10x_len = len(path_q10x)

# path checking

path_check = "hard\quartus\q10x"
path_true  = 0

if path_check in path_q10x:
    path_true = 1
else:
    path_true = 0

    print("\n Error!\n                                                     ")
    print("\n Reason  : Incorrect placement of the file 'directory.py'.    ")
    print("\n Actions : Move the file 'directory.py' to correct placement: ")
    print("\n           .../hard/quartus/q10x/directory.py                 ")

#-------------------------------------------------------------------------------------------------#
# path to folder 'hard'
#-------------------------------------------------------------------------------------------------#

# length calculator

if path_true:

    path_over = "quartus\q10x"

    path_over_len = len(path_over) + 1
    path_hard_len = path_q10x_len - path_over_len

# path getting

if path_true:

    path_cnt  = 0
    path_hard = ""

    for letter in path_q10x:
        if path_cnt < path_hard_len:
            path_hard += letter
            path_cnt  += 1

    path_display = path_hard

# double backslash: (\) to (\\)

if path_true:

    backslash = "\\"
    path_hard_slash = ""

    for letter in path_hard:
        if letter not in backslash:
            path_hard_slash += letter
        else:
            path_hard_slash += "\\\\" ;

# lower case

if path_true:

    path_hard = path_hard_slash.lower()

#-------------------------------------------------------------------------------------------------#
# path to inside folders
#-------------------------------------------------------------------------------------------------#

if path_true:

    # folder: hard

    dir_fun = "set_global_assignment -name SEARCH_PATH \"" + path_hard + "\\\\function/\""
    dir_inc = "set_global_assignment -name SEARCH_PATH \"" + path_hard + "\\\\include/\""
    dir_rtl = "set_global_assignment -name SEARCH_PATH \"" + path_hard + "\\\\rtl/\""

    # folder: hard/function

    dir_fun__lib_int = "set_global_assignment -name SEARCH_PATH \"" + path_hard + "\\\\function\\\\lib_int/\""
    dir_fun__lib_ext = "set_global_assignment -name SEARCH_PATH \"" + path_hard + "\\\\function\\\\lib_ext/\""
    dir_fun__lib_glb = "set_global_assignment -name SEARCH_PATH \"" + path_hard + "\\\\function\\\\lib_ext\\\\lib_global/\""
    dir_fun__lib_eth = "set_global_assignment -name SEARCH_PATH \"" + path_hard + "\\\\function\\\\lib_ext\\\\lib_ethernet/\""

    # folder: hard/include

    dir_inc__top_rtl = "set_global_assignment -name SEARCH_PATH \"" + path_hard + "\\\\include\\\\top_rtl/\""

    # folder: hard/rtl

    dir_rtl__top_rtl = "set_global_assignment -name SEARCH_PATH \"" + path_hard + "\\\\rtl\\\\top_rtl/\""
    dir_rtl__lib_int = "set_global_assignment -name SEARCH_PATH \"" + path_hard + "\\\\rtl\\\\lib_int/\""
    dir_rtl__lib_ext = "set_global_assignment -name SEARCH_PATH \"" + path_hard + "\\\\rtl\\\\lib_ext/\""
    dir_rtl__lib_glb = "set_global_assignment -name SEARCH_PATH \"" + path_hard + "\\\\rtl\\\\lib_ext\\\\lib_global/\""
    dir_rtl__lib_eth = "set_global_assignment -name SEARCH_PATH \"" + path_hard + "\\\\rtl\\\\lib_ext\\\\lib_ethernet/\""

#-------------------------------------------------------------------------------------------------#
# create file 'directory.txt'
#-------------------------------------------------------------------------------------------------#

if path_true:

    text_file = open("directory.txt", "w")

    # line comment

    comment_cnt = 0
    commemt_len = 147 # 147 + 3 = 150
    comment     = ""

    for i in range(commemt_len):
    
        if comment_cnt == 0:
            comment_cnt += 1
            comment += "\n#" # first symbol
        
        elif comment_cnt < (commemt_len - 1):
            comment_cnt += 1
            comment += "-"

        elif comment_cnt == (commemt_len - 1):
            comment_cnt += 1
            comment += "#\n" # last symbol

    # write path to file

    text_file.writelines(comment)
    text_file.writelines("\n")
    text_file.writelines(dir_fun)
    text_file.writelines("\n")
    text_file.writelines(dir_inc)
    text_file.writelines("\n")
    text_file.writelines(dir_rtl)
    text_file.writelines("\n")
    text_file.writelines(dir_fun__lib_int)
    text_file.writelines("\n")
    text_file.writelines(dir_fun__lib_ext)
    text_file.writelines("\n")
    text_file.writelines(dir_fun__lib_glb)
    text_file.writelines("\n")
    text_file.writelines(dir_fun__lib_eth)
    text_file.writelines("\n")
    text_file.writelines(dir_inc__top_rtl)
    text_file.writelines("\n")
    text_file.writelines(dir_rtl__top_rtl)
    text_file.writelines("\n")
    text_file.writelines(dir_rtl__lib_int)
    text_file.writelines("\n")
    text_file.writelines(dir_rtl__lib_ext)
    text_file.writelines("\n")
    text_file.writelines(dir_rtl__lib_glb)
    text_file.writelines("\n")
    text_file.writelines(dir_rtl__lib_eth)
    text_file.writelines("\n")
    text_file.writelines(comment)
    text_file.writelines("# folder: hard")
    text_file.writelines(comment)
    text_file.writelines("\n")
    text_file.writelines(dir_fun)
    text_file.writelines("\n")
    text_file.writelines(dir_inc)
    text_file.writelines("\n")
    text_file.writelines(dir_rtl)
    text_file.writelines("\n")
    text_file.writelines(comment)
    text_file.writelines("# folder: hard/function")
    text_file.writelines(comment)
    text_file.writelines("\n")
    text_file.writelines(dir_fun__lib_int)
    text_file.writelines("\n")
    text_file.writelines(dir_fun__lib_ext)
    text_file.writelines("\n")
    text_file.writelines(dir_fun__lib_glb)
    text_file.writelines("\n")
    text_file.writelines(dir_fun__lib_eth)
    text_file.writelines("\n")
    text_file.writelines(comment)
    text_file.writelines("# folder: hard/include")
    text_file.writelines(comment)
    text_file.writelines("\n")
    text_file.writelines(dir_inc__top_rtl)
    text_file.writelines("\n")
    text_file.writelines(comment)
    text_file.writelines("# folder: hard/rtl")
    text_file.writelines(comment)
    text_file.writelines("\n")
    text_file.writelines(dir_rtl__top_rtl)
    text_file.writelines("\n")
    text_file.writelines(dir_rtl__lib_int)
    text_file.writelines("\n")
    text_file.writelines(dir_rtl__lib_ext)
    text_file.writelines("\n")
    text_file.writelines(dir_rtl__lib_glb)
    text_file.writelines("\n")
    text_file.writelines(dir_rtl__lib_eth)
    text_file.writelines("\n")
    text_file.writelines(comment)
    text_file.close()

#-------------------------------------------------------------------------------------------------#
# print information
#-------------------------------------------------------------------------------------------------#

# path info

if path_true:

    print(" \n                    "                )
    print(" PATH:                 "                )
    print(" \n                    "                )
    print(" Path to folder 'hard':", path_display  )

# files info

if path_true:

    print(" \n                                    ")
    print(" FILES:                                ")
    print(" \n                                    ")
    print(" Creating a text file: 'directory.txt' ")

# instructions

if path_true:

    print(" \n                                                              ")
    print(" Instructions:                                                   ")
    print(" ------------                                                    ")
    print(" \n                                                              ")
    print(" 1. Launch the python-script 'directory.py'.                     ")
    print(" 2. Copy the contents (paths) of the text file 'directory .txt'. ")
    print(" 3. Paste paths in the .qsf file (SEARCH_PATH).                  ")
    print(" 4. Launch the Quartus Compiler.                                 ")

#-------------------------------------------------------------------------------------------------#
# waiting...
#-------------------------------------------------------------------------------------------------#

input("\n\n Press Enter to exit...")

#-------------------------------------------------------------------------------------------------#




@rem #--------------------------------------------------------------------------------------------#
@rem #
@rem #  Company      : Micran, Department of Telecommunications
@rem #
@rem #  Engineer     : Pogodaev Danil
@rem #
@rem #--------------------------------------------------------------------------------------------#
@rem #
@rem #  Library      : hdl_all / library of soft-files for hdl components
@rem #
@rem #  File Name    : _cleaner_.bat (ms-dos)
@rem # 
@rem #--------------------------------------------------------------------------------------------#
@rem #
@rem #  Description  : Delete unnecessary files and folders for libraries of hdl components
@rem #
@rem #  Purpose (1)  : Delete unnecessary folder of quartus compilation
@rem #
@rem #                  - folder 'db'
@rem #                  - folder 'incremental_db'
@rem #
@rem #  Purpose (2)  : Delete unnecessary report files of quartus compilation
@rem #
@rem #                  - file *.rpt
@rem #                  - file *.summary
@rem #                  - file *.smsg
@rem #                  - file *.done
@rem #                  - file *.qdf
@rem #
@rem #  Purpose (3)  : Delete programming files of quartus compilation (output files)
@rem #
@rem #                  - file *.sof
@rem #                  - file *.pof
@rem #                  - file *.rbf
@rem #                  - file *.pin
@rem #                  - file *.jdi
@rem #
@rem #  Purpose (4)  : Delete backup files (*.bak).
@rem #
@rem #--------------------------------------------------------------------------------------------#
@rem #
@rem #  Note (1)     : Command 'rd' (delete catalog)
@rem #
@rem #                  /q - without prompts for confirmation
@rem #                  /s � delete include files in folder
@rem #
@rem #  Note (2)     : Command 'del' (delete file)
@rem #
@rem #                  /q - without prompts for confirmation
@rem #
@rem #--------------------------------------------------------------------------------------------#
@rem #
@rem #  Revision List
@rem #
@rem #  Version   Date       Author
@rem #
@rem #  1.0.0     16.09.16   Pogodaev Danil
@rem #
@rem #--------------------------------------------------------------------------------------------#

@rem #--------------------------------------------------------------------------------------------#
 rem # delete quartus folderes
@rem #--------------------------------------------------------------------------------------------#

     rd /s /q db
     rd /s /q incremental_db

@rem #--------------------------------------------------------------------------------------------#
 rem # delete quartus files
@rem #--------------------------------------------------------------------------------------------#

@rem # for quartus v9 and earlier versions 

     del /q *.rpt
     del /q *.summary
     del /q *.smsg
     del /q *.done
     del /q *.qdf

     del /q *.sof
     del /q *.pof
     del /q *.rbf
     del /q *.pin
     del /q *.jdi

@rem # for quartus v10 and later versions

     del /q output_files\*.rpt
     del /q output_files\*.summary
     del /q output_files\*.smsg
     del /q output_files\*.done
     del /q output_files\*.qdf

     del /q output_files\*.sof
     del /q output_files\*.pof
     del /q output_files\*.rbf
     del /q output_files\*.pin
     del /q output_files\*.jdi

@rem #--------------------------------------------------------------------------------------------#
 rem # delete backup files (*.bak)
@rem #--------------------------------------------------------------------------------------------#

     del /q *.bak

@rem #--------------------------------------------------------------------------------------------#




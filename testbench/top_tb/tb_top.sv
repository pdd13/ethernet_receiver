  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Company      : Micran, Department of Telecommunications
  //
  //  Engineer     : Pogodaev Danil
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Library      : lib_ethernet / library of ethernet components
  //
  //  Component    : eth_rx_v2 / receiver of ethernet frames (packets)
  //
  //  File Name    : tb_top .sv (testbench)
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Description  : Testbench top file for module 'eth_rx'.
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Include Files
  //  
  //  Category       Type        Name                      Ver     Description
  //
  //  top_gen        testbench   tb_gen             .sv    2.0     top generator
  //  top_gen        include     tb_gen__define     .sv    2.0     top generator / defining macros
  //
  //  top_gen        testbench   tb_gen_emu         .sv    2.0     top generator / module 'eth_emu' / testbench generator
  //  top_gen        testbench   tb_gen_emu_m       .sv    2.0     top generator / module 'eth_emu' / testbench generator / manual mode
  //  top_gen        include     eth_emu__par_set   .svh   2.0     top generator / module 'eth_emu' / parameter setting
  //  top_gen        include     eth_emu__par_local .svh   2.0     top generator / module 'eth_emu' / parameter local
  //
  //  top_tb         include     eth_rx__par_set    .svh   2.0     top testbench / module 'eth_rx' / parameter setting
  //  top_tb         include     eth_rx__par_local  .svh   2.0     top testbench / module 'eth_rx' / parameter local
  //
  //  lib_internal   rtl         eth_rx_v2          .sv    2.0.0   receiver of ethernet frames
  //  lib_internal   rtl         eth_rx_cst_v2      .sv    2.0.0   ethernet preliminary receiver for custom mode
  //  lib_internal   rtl         eth_rx_mii_v2      .sv    2.0.0   ethernet preliminary receiver for mii-interface
  //  lib_internal   rtl         eth_rx_rmii_v2     .sv    2.0.0   ethernet preliminary receiver for rmii-interface
  //  lib_internal   rtl         eth_rx_gmii_v2     .sv    2.0.0   ethernet preliminary receiver for gmii-interface
  //  lib_internal   rtl         eth_rx_mux_v2      .sv    2.0.0   multiplexer of ethernet preliminary receivers
  //  lib_internal   rtl         eth_rx_head_v2     .sv    2.0.0   frame header detector (preamble and start-frame delimiter)
  //  lib_internal   rtl         eth_rx_str_v2      .sv    2.0.0   strobe generator and length calculator for ethernet frames
  //  lib_internal   rtl         eth_rx_dat_v2      .sv    2.0.0   data extractor for ip and mac headers
  //  lib_internal   rtl         eth_rx_out_v2      .sv    2.0.0   output registers and latching data (mac and ip headers)
  //
  //  lib_ethernet   function    eth_crc32_8d       .sv    1.0.0   calculator of crc-32 (data 8 bit)
  //  lib_ethernet   rtl         eth_cnt_v2         .sv    2.0.0   counters of ethernet frames
  //  lib_internal   rtl         eth_rx_crc_v2      .sv    2.0.0   crc calculator for received ethernet frames
  //  lib_ethernet   testbench   tb_eth_emu_v2      .sv    2.0.0   emulator of ethernet frames
  //  lib_ethernet   testbench   tb_eth_emu_clk_v2  .sv    2.0.0   clocks generator of ethernet frames
  //  lib_ethernet   testbench   tb_eth_emu_str_v2  .sv    2.0.0   strobes generator of ethernet frames
  //  lib_ethernet   testbench   tb_eth_emu_dat_v2  .sv    2.0.0   data stream generator of ethernet frames
  //  lib_ethernet   testbench   tb_eth_emu_lan_v2  .sv    2.0.0   lan ethernet interface implementation
  //  lib_ethernet   testbench   tb_eth_emu_spd_v2  .sv    2.0.0   speed calculator (calculator of inter-frame gap)
  //  lib_ethernet   testbench   tb_eth_emu_ini_v2  .sv    2.0.0   generator of initialization signal (for settings)
  //
  //  lib_global     function    cbit               .sv    1.0.0   bits calculator for input number (round up)
  //  lib_global     rtl         z_line             .sv    1.0.0   delay line for serial and parallel data
  //  lib_global     rtl         det_se             .sv    1.0.0   detector of single edges for input strobe
  //  lib_global     rtl         det_de             .sv    1.0.0   detector of double edges for input strobe
  //  lib_global     rtl         gen_npr            .sv    1.0.0   generator of registers with attribute "noprune"
  //  lib_global     rtl         gen_ced            .sv    1.0.0   generator of clock enable for data
  //  lib_global     rtl         gen_cen            .sv    1.0.0   generator of clock enable
  //  lib_global     rtl         gen_sstr           .sv    1.0.0   generator of single strobe
  //  lib_global     rtl         gen_mstr           .sv    1.0.0   generator of multi strobes
  //  lib_global     rtl         shift_s2p          .sv    1.0.0   shift register for serial to parallel
  //  lib_global     rtl         shift_p2s          .sv    1.0.0   shift register for parallel to serial
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Revision List
  //
  //  Version   Date       Author
  //
  //  2.0       26.09.16   Pogodaev Danil
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  `timescale 1ns/1ns

  module tb_top ();

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // include files : functions
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
                                                 //      |
  `include "cbit.sv"                             // req  | bits calculator for input number (round up)
                                                 //      |
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // include files : parameters
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
                                                 //      |
  // receiver of ethernet frames                 //------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------//
                                                 //      |
  `include "eth_rx__par_set.svh"                 // req  | top testbench / module 'eth_rx' / parameter setting
  `include "eth_rx__par_local.svh"               // req  | top testbench / module 'eth_rx' / parameter local
                                                 //      |
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // logic
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  //---------------------------------------------// ethernet receiver -------| [ eth_rx / eth_rx_v2 ]

  //                                                reset

  var    logic                                      eth_rx__i_reset_n        ;

  //                                                setting

  var    logic                                      eth_rx__i_set_cut        ;
  var    logic    [                       1 :0 ]    eth_rx__i_set_speed      ;
  var    logic                                      eth_rx__i_set_jumbo      ;

  //                                                lan interface

  var    logic                                      eth_rx__i_mii_clk        ;
  var    logic    [                       3 :0 ]    eth_rx__i_mii_rxd        ;
  var    logic                                      eth_rx__i_mii_rx_dv      ;

  var    logic                                      eth_rx__i_rmii_clk       ;
  var    logic                                      eth_rx__o_rmii_clk_s     ;
  var    logic    [                       1 :0 ]    eth_rx__i_rmii_rxd       ;
  var    logic                                      eth_rx__i_rmii_crs_dv    ;

  var    logic                                      eth_rx__i_gmii_clk       ;
  var    logic    [                       7 :0 ]    eth_rx__i_gmii_rxd       ;
  var    logic                                      eth_rx__i_gmii_rx_dv     ;

  //                                                custom mode

  var    logic                                      eth_rx__i_cst_clk        ;
  var    logic                                      eth_rx__i_cst_cen        ;
  var    logic                                      eth_rx__i_cst_ced        ;
  var    logic    [                       7 :0 ]    eth_rx__i_cst_rxd        ;
  var    logic                                      eth_rx__i_cst_rx_dv      ;

  //                                                rx packet

  var    logic                                      eth_rx__o_rx_clk         ;
  var    logic                                      eth_rx__o_rx_cen         ;
  var    logic                                      eth_rx__o_rx_ced         ;

  var    logic    [                       2 :0 ]    eth_rx__o_rx_str_body    ;
  var    logic    [ ETH_RX__lpW_RX_LEN   -1 :0 ]    eth_rx__o_rx_cnt_body    ;
  var    logic    [                       2 :0 ]    eth_rx__o_rx_str_mac     ;
  var    logic    [                       3 :0 ]    eth_rx__o_rx_cnt_mac     ;
  var    logic    [                       2 :0 ]    eth_rx__o_rx_str_mda     ;
  var    logic    [                       2 :0 ]    eth_rx__o_rx_str_msa     ;
  var    logic    [                       2 :0 ]    eth_rx__o_rx_str_mlt     ;
  var    logic    [                       2 :0 ]    eth_rx__o_rx_str_ip      ;
  var    logic    [ ETH_RX__lpW_RX_IP_C  -1 :0 ]    eth_rx__o_rx_cnt_ip      ;
  var    logic    [                       2 :0 ]    eth_rx__o_rx_str_pay     ;
  var    logic    [ ETH_RX__lpW_RX_LEN   -1 :0 ]    eth_rx__o_rx_cnt_pay     ;
  var    logic    [                       2 :0 ]    eth_rx__o_rx_str_gap     ;
  var    logic    [ ETH_RX__pW_RX_GAP    -1 :0 ]    eth_rx__o_rx_cnt_gap     ;

  var    logic                                      eth_rx__o_rx_dat_load    ;
  var    logic    [                       7 :0 ]    eth_rx__o_rx_dat_body    ;
  var    logic                                      eth_rx__o_rx_rdy_mac     ;
  var    logic    [                      47 :0 ]    eth_rx__o_rx_dat_mda     ;
  var    logic    [                      47 :0 ]    eth_rx__o_rx_dat_msa     ;
  var    logic    [                      15 :0 ]    eth_rx__o_rx_dat_mlt     ;
  var    logic                                      eth_rx__o_rx_rdy_ip      ;
  var    logic    [                      47 :0 ]    eth_rx__o_rx_dat_ip      ;

  var    logic                                      eth_rx__o_rx_rdy_body    ;
  var    logic    [ ETH_RX__lpW_RX_LEN   -1 :0 ]    eth_rx__o_rx_len_body    ;
  var    logic                                      eth_rx__o_rx_rdy_gap     ;
  var    logic    [ ETH_RX__pW_RX_GAP    -1 :0 ]    eth_rx__o_rx_len_gap     ;

  var    logic                                      eth_rx__o_rx_crc_rdy     ;
  var    logic                                      eth_rx__o_rx_crc_err     ;

  //                                                rx counters / setting

  var    logic    [                       1 :0 ]    eth_rx__i_rx_set_p_spd   ;
  var    logic    [ ETH_RX__pN_RX_CNT       :1 ]
                  [ ETH_RX__lpW_RX_LEN   -1 :0 ]    eth_rx__i_rx_set_p_top   ;
  var    logic    [ ETH_RX__pN_RX_CNT       :1 ]
                  [ ETH_RX__lpW_RX_LEN   -1 :0 ]    eth_rx__i_rx_set_p_low   ;

  //                                                rx counters / detector

  var    logic                                      eth_rx__o_rx_det_p_rdy   ;
  var    logic                                      eth_rx__o_rx_det_p_err   ;
  var    logic                                      eth_rx__o_rx_det_p_cut   ;
  var    logic                                      eth_rx__i_rx_det_p_off   ;

  //                                                rx counters / counters

  var    logic                                      eth_rx__i_rx_cnt_p_clr   ;
  var    logic                                      eth_rx__o_rx_cnt_p_rdy   ;
  var    logic    [ ETH_RX__pN_RX_CNT       :0 ]
                  [ ETH_RX__pW_RX_CNT_P  -1 :0 ]    eth_rx__o_rx_cnt_p_all   ;
  var    logic    [ ETH_RX__pN_RX_CNT       :0 ]
                  [ ETH_RX__pW_RX_CNT_P  -1 :0 ]    eth_rx__o_rx_cnt_p_err   ;
  var    logic    [ ETH_RX__pN_RX_CNT       :0 ]
                  [ ETH_RX__pW_RX_CNT_P  -1 :0 ]    eth_rx__o_rx_cnt_p_off   ;

  var    logic    [ ETH_RX__pW_RX_CNT_S  -1 :0 ]    eth_rx__o_rx_cnt_p_spd   ;

  //                                                rx counters / rhythm

  var    logic                                      eth_rx__i_rx_rcnt_p_clr  ;
  var    logic                                      eth_rx__i_rx_rcnt_p_udp  ;
  var    logic                                      eth_rx__o_rx_rcnt_p_rdy  ;
  var    logic    [ ETH_RX__pN_RX_CNT       :0 ]
                  [ ETH_RX__pW_RX_CNT_P  -1 :0 ]    eth_rx__o_rx_rcnt_p_all  ;
  var    logic    [ ETH_RX__pN_RX_CNT       :0 ]
                  [ ETH_RX__pW_RX_CNT_P  -1 :0 ]    eth_rx__o_rx_rcnt_p_err  ;
  var    logic    [ ETH_RX__pN_RX_CNT       :0 ]
                  [ ETH_RX__pW_RX_CNT_P  -1 :0 ]    eth_rx__o_rx_rcnt_p_off  ;

  var    logic    [ ETH_RX__pW_RX_CNT_S  -1 :0 ]    eth_rx__o_rx_rcnt_p_spd  ;

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  //==========================================================================================================================================================================================================================//
  // body of module
  //==========================================================================================================================================================================================================================//

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // receiver of ethernet frames  [ eth_rx / eth_rx_v2 ]
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  //-------------------------------------------------------------------------------------|
                                                         //                              |
  eth_rx_v2              eth_rx                          //                              |
  (                                                      //                              |
  // reset ----------------------------------------------//------------------------------|
                                                         //           |                  |
     .i_reset_n          ( eth_rx__i_reset_n       ),    //  in       |       opt        |
                                                         //           |                  |
  // setting --------------------------------------------//-----------|------------------|
                                                         //           |                  |
     .i_set_cut          ( eth_rx__i_set_cut       ),    //  in       |       opt        |
     .i_set_speed        ( eth_rx__i_set_speed     ),    //  in       |  req             |
     .i_set_jumbo        ( eth_rx__i_set_jumbo     ),    //  in       |       opt        |
                                                         //           |                  |
  // lan interface --------------------------------------//-----------|------------------|
                                                         //           |                  |
  // lan mii             lan mii                         //-----------|------------------|
                                                         //           |                  |
     .i_mii_clk          ( eth_rx__i_mii_clk       ),    //  in       |  req             |
     .i_mii_rxd          ( eth_rx__i_mii_rxd       ),    //  in       |  req             |
     .i_mii_rx_dv        ( eth_rx__i_mii_rx_dv     ),    //  in       |  req             |
                                                         //           |                  |
  // lan rmii            lan rmii                        //-----------|------------------|
                                                         //           |                  |
     .i_rmii_clk         ( eth_rx__i_rmii_clk      ),    //  in       |  req             |
     .o_rmii_clk_s       ( eth_rx__o_rmii_clk_s    ),    //      out  |            test  |
     .i_rmii_rxd         ( eth_rx__i_rmii_rxd      ),    //  in       |  req             |
     .i_rmii_crs_dv      ( eth_rx__i_rmii_crs_dv   ),    //  in       |  req             |
                                                         //           |                  |
  // lan gmii            lan gmii                        //-----------|------------------|
                                                         //           |                  |
     .i_gmii_clk         ( eth_rx__i_gmii_clk      ),    //  in       |  req             |
     .i_gmii_rxd         ( eth_rx__i_gmii_rxd      ),    //  in       |  req             |
     .i_gmii_rx_dv       ( eth_rx__i_gmii_rx_dv    ),    //  in       |  req             |
                                                         //           |                  |
  // custom mode ----------------------------------------//-----------|------------------|
                                                         //           |                  |
     .i_cst_clk          ( eth_rx__i_cst_clk       ),    //  in       |       opt        |
     .i_cst_cen          ( eth_rx__i_cst_cen       ),    //  in       |       opt        |
     .i_cst_ced          ( eth_rx__i_cst_ced       ),    //  in       |       opt        |
     .i_cst_rxd          ( eth_rx__i_cst_rxd       ),    //  in       |       opt        |
     .i_cst_rx_dv        ( eth_rx__i_cst_rx_dv     ),    //  in       |       opt        |
                                                         //           |                  |
  // rx packet ------------------------------------------//-----------|------------------|
                                                         //           |                  |
  // clock               clock                           //-----------|------------------|
                                                         //           |                  |
     .o_rx_clk           ( eth_rx__o_rx_clk        ),    //      out  |  req             |
     .o_rx_cen           ( eth_rx__o_rx_cen        ),    //      out  |  req             |
     .o_rx_ced           ( eth_rx__o_rx_ced        ),    //      out  |  req             |
                                                         //           |                  |
  // strob               strob                           //-----------|------------------|
                                                         //           |                  |
     .o_rx_str_body      ( eth_rx__o_rx_str_body   ),    //      out  |  req             |
     .o_rx_cnt_body      ( eth_rx__o_rx_cnt_body   ),    //      out  |       opt        |
     .o_rx_str_mac       ( eth_rx__o_rx_str_mac    ),    //      out  |       opt        |
     .o_rx_cnt_mac       ( eth_rx__o_rx_cnt_mac    ),    //      out  |       opt        |
     .o_rx_str_mda       ( eth_rx__o_rx_str_mda    ),    //      out  |       opt        |
     .o_rx_str_msa       ( eth_rx__o_rx_str_msa    ),    //      out  |       opt        |
     .o_rx_str_mlt       ( eth_rx__o_rx_str_mlt    ),    //      out  |       opt        |
     .o_rx_str_ip        ( eth_rx__o_rx_str_ip     ),    //      out  |       opt        |
     .o_rx_cnt_ip        ( eth_rx__o_rx_cnt_ip     ),    //      out  |       opt        |
     .o_rx_str_pay       ( eth_rx__o_rx_str_pay    ),    //      out  |       opt        |
     .o_rx_cnt_pay       ( eth_rx__o_rx_cnt_pay    ),    //      out  |       opt        |
     .o_rx_str_gap       ( eth_rx__o_rx_str_gap    ),    //      out  |       opt        |
     .o_rx_cnt_gap       ( eth_rx__o_rx_cnt_gap    ),    //      out  |       opt        |
                                                         //           |                  |
  // data                data                            //-----------|------------------|
                                                         //           |                  |
     .o_rx_dat_load      ( eth_rx__o_rx_dat_load   ),    //      out  |  req             |
     .o_rx_dat_body      ( eth_rx__o_rx_dat_body   ),    //      out  |  req             |
                                                         //           |                  |
     .o_rx_rdy_mac       ( eth_rx__o_rx_rdy_mac    ),    //      out  |       opt        |
     .o_rx_dat_mda       ( eth_rx__o_rx_dat_mda    ),    //      out  |       opt        |
     .o_rx_dat_msa       ( eth_rx__o_rx_dat_msa    ),    //      out  |       opt        |
     .o_rx_dat_mlt       ( eth_rx__o_rx_dat_mlt    ),    //      out  |       opt        |
                                                         //           |                  |
     .o_rx_rdy_ip        ( eth_rx__o_rx_rdy_ip     ),    //      out  |       opt        |
     .o_rx_dat_ip        ( eth_rx__o_rx_dat_ip     ),    //      out  |       opt        |
                                                         //           |                  |
  // length              length                          //-----------|------------------|
                                                         //           |                  |
     .o_rx_rdy_body      ( eth_rx__o_rx_rdy_body   ),    //      out  |  req             |
     .o_rx_len_body      ( eth_rx__o_rx_len_body   ),    //      out  |  req             |
     .o_rx_rdy_gap       ( eth_rx__o_rx_rdy_gap    ),    //      out  |       opt        |
     .o_rx_len_gap       ( eth_rx__o_rx_len_gap    ),    //      out  |       opt        |
                                                         //           |                  |
  // fcs crc             fcs crc                         //-----------|------------------|
                                                         //           |                  |
     .o_rx_crc_rdy       ( eth_rx__o_rx_crc_rdy    ),    //      out  |  req             |
     .o_rx_crc_err       ( eth_rx__o_rx_crc_err    ),    //      out  |  req             |
                                                         //           |                  |
  // rx counters ----------------------------------------//-----------|------------------|
                                                         //           |                  |
  // setting             setting                         //-----------|------------------|
                                                         //           |                  |
     .i_rx_set_p_top     ( eth_rx__i_rx_set_p_top  ),    //  in       |       opt        |
     .i_rx_set_p_low     ( eth_rx__i_rx_set_p_low  ),    //  in       |       opt        |
     .i_rx_set_p_spd     ( eth_rx__i_rx_set_p_spd  ),    //  in       |       opt        |
                                                         //           |                  |
  // detector            detector                        //-----------|------------------|
                                                         //           |                  |
     .o_rx_det_p_rdy     ( eth_rx__o_rx_det_p_rdy  ),    //      out  |       opt        |
     .o_rx_det_p_err     ( eth_rx__o_rx_det_p_err  ),    //      out  |       opt        |
     .o_rx_det_p_cut     ( eth_rx__o_rx_det_p_cut  ),    //      out  |       opt        |
     .i_rx_det_p_off     ( eth_rx__i_rx_det_p_off  ),    //  in       |       opt        |
                                                         //           |                  |
  // counters            counters                        //-----------|------------------|
                                                         //           |                  |
     .i_rx_cnt_p_clr     ( eth_rx__i_rx_cnt_p_clr  ),    //  in       |       opt        |
     .o_rx_cnt_p_rdy     ( eth_rx__o_rx_cnt_p_rdy  ),    //      out  |       opt        |
     .o_rx_cnt_p_all     ( eth_rx__o_rx_cnt_p_all  ),    //      out  |       opt        |
     .o_rx_cnt_p_err     ( eth_rx__o_rx_cnt_p_err  ),    //      out  |       opt        |
     .o_rx_cnt_p_off     ( eth_rx__o_rx_cnt_p_off  ),    //      out  |       opt        |
     .o_rx_cnt_p_spd     ( eth_rx__o_rx_cnt_p_spd  ),    //      out  |       opt        |
                                                         //           |                  |
  // counters            counters (rhythm)               //-----------|------------------|
                                                         //           |                  |
     .i_rx_rcnt_p_clr    ( eth_rx__i_rx_rcnt_p_clr ),    //  in       |       opt        |
     .i_rx_rcnt_p_upd    ( eth_rx__i_rx_rcnt_p_upd ),    //  in       |       opt        |
     .o_rx_rcnt_p_rdy    ( eth_rx__o_rx_rcnt_p_rdy ),    //      out  |       opt        |
     .o_rx_rcnt_p_all    ( eth_rx__o_rx_rcnt_p_all ),    //      out  |       opt        |
     .o_rx_rcnt_p_err    ( eth_rx__o_rx_rcnt_p_err ),    //      out  |       opt        |
     .o_rx_rcnt_p_off    ( eth_rx__o_rx_rcnt_p_off ),    //      out  |       opt        |
     .o_rx_rcnt_p_spd    ( eth_rx__o_rx_rcnt_p_spd )     //      out  |       opt        |
                                                         //           |                  |
  );//-----------------------------------------------------------------------------------|

  // interface

  defparam  eth_rx.pS_IO_TYPE   = ETH_RX__pS_IO_TYPE  ;

  // custom mode

  defparam  eth_rx.pT_CST_CEN   = ETH_RX__pT_CST_CEN  ;
  defparam  eth_rx.pT_CST_CED   = ETH_RX__pT_CST_CED  ;

  // packet manipulation

  defparam  eth_rx.pE_RX_CUT    = ETH_RX__pE_RX_CUT   ;
  defparam  eth_rx.pE_RX_JUMBO  = ETH_RX__pE_RX_JUMBO ;

  // packet structure
  
  defparam  eth_rx.pE_RX_HEAD   = ETH_RX__pE_RX_HEAD  ;
  defparam  eth_rx.pW_RX_LEN    = ETH_RX__pW_RX_LEN   ;
  defparam  eth_rx.pV_RX_LEN    = ETH_RX__pV_RX_LEN   ;
  defparam  eth_rx.pW_RX_LEN_J  = ETH_RX__pW_RX_LEN_J ;
  defparam  eth_rx.pV_RX_LEN_J  = ETH_RX__pV_RX_LEN_J ;
  defparam  eth_rx.pW_RX_GAP    = ETH_RX__pW_RX_GAP   ;
  defparam  eth_rx.pE_RX_IP     = ETH_RX__pE_RX_IP    ;
  defparam  eth_rx.pN_RX_IPL    = ETH_RX__pN_RX_IPL   ;
  defparam  eth_rx.pN_RX_IPH    = ETH_RX__pN_RX_IPH   ;
  defparam  eth_rx.pE_RX_MAC    = ETH_RX__pE_RX_MAC   ;

  // packet counters

  defparam  eth_rx.pN_RX_CNT    = ETH_RX__pN_RX_CNT   ;
  defparam  eth_rx.pW_RX_CNT_P  = ETH_RX__pW_RX_CNT_P ;
  defparam  eth_rx.pW_RX_CNT_S  = ETH_RX__pW_RX_CNT_S ;

  // signal tap (for tetsing)

  defparam  eth_rx.pE_GEN_NPR   = ETH_RX__pE_GEN_NPR  ;

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // receiver of ethernet frames / tb generator  [ tb_gen / tb_gen ]
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  //-------------------------------------------------------------------------------------|
                                                         //                              |
  tb_gen                 tb_gen                          //                              |
  (                                                      //                              |
  // reset ----------------------------------------------//------------------------------|
                                                         //           |                  |
     .i_reset_n          ( eth_rx__i_reset_n       ),    //  in       |       opt        |
                                                         //           |                  |
  // setting --------------------------------------------//-----------|------------------|
                                                         //           |                  |
     .i_set_cut          ( eth_rx__i_set_cut       ),    //  in       |       opt        |
     .i_set_speed        ( eth_rx__i_set_speed     ),    //  in       |  req             |
     .i_set_jumbo        ( eth_rx__i_set_jumbo     ),    //  in       |       opt        |
                                                         //           |                  |
  // lan interface --------------------------------------//-----------|------------------|
                                                         //           |                  |
  // lan mii             lan mii                         //-----------|------------------|
                                                         //           |                  |
     .i_mii_clk          ( eth_rx__i_mii_clk       ),    //  in       |  req             |
     .i_mii_rxd          ( eth_rx__i_mii_rxd       ),    //  in       |  req             |
     .i_mii_rx_dv        ( eth_rx__i_mii_rx_dv     ),    //  in       |  req             |
                                                         //           |                  |
  // lan rmii            lan rmii                        //-----------|------------------|
                                                         //           |                  |
     .i_rmii_clk         ( eth_rx__i_rmii_clk      ),    //  in       |  req             |
     .o_rmii_clk_s       ( eth_rx__o_rmii_clk_s    ),    //      out  |            test  |
     .i_rmii_rxd         ( eth_rx__i_rmii_rxd      ),    //  in       |  req             |
     .i_rmii_crs_dv      ( eth_rx__i_rmii_crs_dv   ),    //  in       |  req             |
                                                         //           |                  |
  // lan gmii            lan gmii                        //-----------|------------------|
                                                         //           |                  |
     .i_gmii_clk         ( eth_rx__i_gmii_clk      ),    //  in       |  req             |
     .i_gmii_rxd         ( eth_rx__i_gmii_rxd      ),    //  in       |  req             |
     .i_gmii_rx_dv       ( eth_rx__i_gmii_rx_dv    ),    //  in       |  req             |
                                                         //           |                  |
  // custom mode ----------------------------------------//-----------|------------------|
                                                         //           |                  |
     .i_cst_clk          ( eth_rx__i_cst_clk       ),    //  in       |       opt        |
     .i_cst_cen          ( eth_rx__i_cst_cen       ),    //  in       |       opt        |
     .i_cst_ced          ( eth_rx__i_cst_ced       ),    //  in       |       opt        |
     .i_cst_rxd          ( eth_rx__i_cst_rxd       ),    //  in       |       opt        |
     .i_cst_rx_dv        ( eth_rx__i_cst_rx_dv     ),    //  in       |       opt        |
                                                         //           |                  |
  // rx packet ------------------------------------------//-----------|------------------|
                                                         //           |                  |
  // clock               clock                           //-----------|------------------|
                                                         //           |                  |
     .o_rx_clk           ( eth_rx__o_rx_clk        ),    //      out  |  req             |
     .o_rx_cen           ( eth_rx__o_rx_cen        ),    //      out  |  req             |
     .o_rx_ced           ( eth_rx__o_rx_ced        ),    //      out  |  req             |
                                                         //           |                  |
  // strob               strob                           //-----------|------------------|
                                                         //           |                  |
     .o_rx_str_body      ( eth_rx__o_rx_str_body   ),    //      out  |  req             |
     .o_rx_cnt_body      ( eth_rx__o_rx_cnt_body   ),    //      out  |       opt        |
     .o_rx_str_mac       ( eth_rx__o_rx_str_mac    ),    //      out  |       opt        |
     .o_rx_cnt_mac       ( eth_rx__o_rx_cnt_mac    ),    //      out  |       opt        |
     .o_rx_str_mda       ( eth_rx__o_rx_str_mda    ),    //      out  |       opt        |
     .o_rx_str_msa       ( eth_rx__o_rx_str_msa    ),    //      out  |       opt        |
     .o_rx_str_mlt       ( eth_rx__o_rx_str_mlt    ),    //      out  |       opt        |
     .o_rx_str_ip        ( eth_rx__o_rx_str_ip     ),    //      out  |       opt        |
     .o_rx_cnt_ip        ( eth_rx__o_rx_cnt_ip     ),    //      out  |       opt        |
     .o_rx_str_pay       ( eth_rx__o_rx_str_pay    ),    //      out  |       opt        |
     .o_rx_cnt_pay       ( eth_rx__o_rx_cnt_pay    ),    //      out  |       opt        |
     .o_rx_str_gap       ( eth_rx__o_rx_str_gap    ),    //      out  |       opt        |
     .o_rx_cnt_gap       ( eth_rx__o_rx_cnt_gap    ),    //      out  |       opt        |
                                                         //           |                  |
  // data                data                            //-----------|------------------|
                                                         //           |                  |
     .o_rx_dat_load      ( eth_rx__o_rx_dat_load   ),    //      out  |  req             |
     .o_rx_dat_body      ( eth_rx__o_rx_dat_body   ),    //      out  |  req             |
                                                         //           |                  |
     .o_rx_rdy_mac       ( eth_rx__o_rx_rdy_mac    ),    //      out  |       opt        |
     .o_rx_dat_mda       ( eth_rx__o_rx_dat_mda    ),    //      out  |       opt        |
     .o_rx_dat_msa       ( eth_rx__o_rx_dat_msa    ),    //      out  |       opt        |
     .o_rx_dat_mlt       ( eth_rx__o_rx_dat_mlt    ),    //      out  |       opt        |
                                                         //           |                  |
     .o_rx_rdy_ip        ( eth_rx__o_rx_rdy_ip     ),    //      out  |       opt        |
     .o_rx_dat_ip        ( eth_rx__o_rx_dat_ip     ),    //      out  |       opt        |
                                                         //           |                  |
  // length              length                          //-----------|------------------|
                                                         //           |                  |
     .o_rx_rdy_body      ( eth_rx__o_rx_rdy_body   ),    //      out  |  req             |
     .o_rx_len_body      ( eth_rx__o_rx_len_body   ),    //      out  |  req             |
     .o_rx_rdy_gap       ( eth_rx__o_rx_rdy_gap    ),    //      out  |       opt        |
     .o_rx_len_gap       ( eth_rx__o_rx_len_gap    ),    //      out  |       opt        |
                                                         //           |                  |
  // fcs crc             fcs crc                         //-----------|------------------|
                                                         //           |                  |
     .o_rx_crc_rdy       ( eth_rx__o_rx_crc_rdy    ),    //      out  |  req             |
     .o_rx_crc_err       ( eth_rx__o_rx_crc_err    ),    //      out  |  req             |
                                                         //           |                  |
  // rx counters ----------------------------------------//-----------|------------------|
                                                         //           |                  |
  // setting             setting                         //-----------|------------------|
                                                         //           |                  |
     .i_rx_set_p_top     ( eth_rx__i_rx_set_p_top  ),    //  in       |       opt        |
     .i_rx_set_p_low     ( eth_rx__i_rx_set_p_low  ),    //  in       |       opt        |
     .i_rx_set_p_spd     ( eth_rx__i_rx_set_p_spd  ),    //  in       |       opt        |
                                                         //           |                  |
  // detector            detector                        //-----------|------------------|
                                                         //           |                  |
     .o_rx_det_p_rdy     ( eth_rx__o_rx_det_p_rdy  ),    //      out  |       opt        |
     .o_rx_det_p_err     ( eth_rx__o_rx_det_p_err  ),    //      out  |       opt        |
     .o_rx_det_p_cut     ( eth_rx__o_rx_det_p_cut  ),    //      out  |       opt        |
     .i_rx_det_p_off     ( eth_rx__i_rx_det_p_off  ),    //  in       |       opt        |
                                                         //           |                  |
  // counters            counters                        //-----------|------------------|
                                                         //           |                  |
     .i_rx_cnt_p_clr     ( eth_rx__i_rx_cnt_p_clr  ),    //  in       |       opt        |
     .o_rx_cnt_p_rdy     ( eth_rx__o_rx_cnt_p_rdy  ),    //      out  |       opt        |
     .o_rx_cnt_p_all     ( eth_rx__o_rx_cnt_p_all  ),    //      out  |       opt        |
     .o_rx_cnt_p_err     ( eth_rx__o_rx_cnt_p_err  ),    //      out  |       opt        |
     .o_rx_cnt_p_off     ( eth_rx__o_rx_cnt_p_off  ),    //      out  |       opt        |
     .o_rx_cnt_p_spd     ( eth_rx__o_rx_cnt_p_spd  ),    //      out  |       opt        |
                                                         //           |                  |
  // counters            counters (rhythm)               //-----------|------------------|
                                                         //           |                  |
     .i_rx_rcnt_p_clr    ( eth_rx__i_rx_rcnt_p_clr ),    //  in       |       opt        |
     .i_rx_rcnt_p_upd    ( eth_rx__i_rx_rcnt_p_upd ),    //  in       |       opt        |
     .o_rx_rcnt_p_rdy    ( eth_rx__o_rx_rcnt_p_rdy ),    //      out  |       opt        |
     .o_rx_rcnt_p_all    ( eth_rx__o_rx_rcnt_p_all ),    //      out  |       opt        |
     .o_rx_rcnt_p_err    ( eth_rx__o_rx_rcnt_p_err ),    //      out  |       opt        |
     .o_rx_rcnt_p_off    ( eth_rx__o_rx_rcnt_p_off ),    //      out  |       opt        |
     .o_rx_rcnt_p_spd    ( eth_rx__o_rx_rcnt_p_spd )     //      out  |       opt        |
                                                         //           |                  |
  );//-----------------------------------------------------------------------------------|

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  endmodule




  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Company      : Micran, Department of Telecommunications
  //
  //  Engineer     : Pogodaev Danil
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Library      : lib_ethernet / library of ethernet components
  //
  //  File Name    : tb_eth_emu_v2 .sv (testbench)
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Description  : Testbench emulator of ethernet frames (auto generated) / see note (1)
  //
  //  Purpose (1)  : Generate clocks of ethernet frames / see table (1)
  //
  //                  - clock                 / o_lan_clk
  //                  - clock enable          / o_lan_cen / width: o_lan_clk
  //                  - clock enable for data / o_lan_ced / width: o_lan_cen
  //
  //  Purpose (2)  : Generate strobes of ethernet frames / see table (2)
  //
  //                  - strobe                / o_lan_str_name [0]
  //                  - strobe / start bit    / o_lan_str_name [1] / width: o_lan_cen
  //                  - strobe / stop bit     / o_lan_str_name [2] / width: o_lan_cen
  //                  - strobe / byte counter / o_lan_cnt_name
  //
  //  Purpose (3)  : Calculate length of ethernet frames
  //
  //                  - length frame   / o_lan_len_frame
  //                  - length body    / o_lan_len_body
  //                  - length payload / o_lan_len_pay
  //                  - length gap     / o_lan_len_gap
  //
  //  Purpose (4)  : Generate data stream of ethernet frames
  //
  //                  - data load  / o_lan_dat_load
  //                  - data frame / o_lan_dat_frame
  //
  //  Purpose (5)  : Count the number of ethernet frames (packets) / see note (2)
  //
  //                  - packet all   / o_lan_cnt_p_all
  //                  - packet error / o_lan_cnt_p_err
  //
  //  Purpose (6)  : Calculate frame check sequence (fcs) crc-32
  //
  //  Purpose (7)  : Insert crc error into generated frame / i_set_err_ena
  //
  //  Purpose (8)  : LAN ethernet interface implementation
  //
  //                  - interface mii  / data 4 bit
  //                  - interface rmii / data 2 bit
  //                  - interface gmii / data 8 bit
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Note (1)     : Structure of ethernet frames (std. 802.3) / see also table (2)
  //
  //                  - frame header preamble
  //                  - start-frame delimiter (sfd)
  //                  - destination mac address (mda)
  //                  - source mac address (msa)
  //                  - length of payload / type of protocol (mlt)
  //                  - data payload and pad
  //                  - frame check sequence (fcs) crc-32
  //                  - inter-frame gap
  //
  //  Note (2)     : Group counters for packet length (frame body)
  //
  //                 For example / SunLite GigE (Sunrise Telecom)
  //
  //                  - all size       / o_lan_cnt_p [0]
  //                  - 64-127 byte    / o_lan_cnt_p [1]
  //                  - 128-255 byte   / o_lan_cnt_p [2]
  //                  - 256-511 byte   / o_lan_cnt_p [3]
  //                  - 512-1023 byte  / o_lan_cnt_p [4]
  //                  - 1024-1518 byte / o_lan_cnt_p [5]
  //                  - oversize       / o_lan_cnt_p [6]
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Attached (1) : Additional (detailed) code comments / tb_eth_emu_v2 - comment .txt
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Table (1)    : Clock manager of ethernet frames for testbench emulator
  //
  //                 |-----------------------------------------------------------------------------------------------|
  //                 |                   |           |                      frequency (period)                       |
  //                 |      setting      |   clock   |---------------------------------------------------------------|
  //                 |                   |           |      10 mbit/s      |     100 mbit/s     |    1000 mbit/s     |
  //                 |===================|===========|=====================|====================|====================|
  //                 |                   |  lan_clk  |  2.50 mhz (400 ns)  |  25.0 mhz (40 ns)  |  no                |
  //                 |  mii interface:   |-----------|---------------------|--------------------|--------------------|
  //                 |                   |  lan_cen  |  1'b1               |  1'b1              |  no                |
  //                 |  set_io_type = 2  |-----------|---------------------|--------------------|--------------------|
  //                 |                   |  lan_ced  |  1.25 mhz (800 ns)  |  12.5 mhz (80 ns)  |  no                |
  //                 |===================|===========|=====================|====================|====================|
  //                 |                   |  lan_clk  |  50.00 mhz (20 ns)  |  50.00 mhz (20 ns) |  no                |
  //                 |  rmii interface:  |-----------|---------------------|--------------------|--------------------|
  //                 |                   |  lan_cen  |  5.00 mhz (200 ns)  |  1'b1              |  no                |
  //                 |  set_io_type = 1  |-----------|---------------------|--------------------|--------------------|
  //                 |                   |  lan_ced  |  1.25 mhz (800 ns)  |  12.5 mhz (80 ns)  |  no                |
  //                 |===================|===========|=====================|====================|====================|
  //                 |                   |  lan_clk  |  2.50 mhz (400 ns)  |  25.0 mhz (40 ns)  |  125.0 mhz (8 ns)  |
  //                 |  gmii interface:  |-----------|---------------------|--------------------|--------------------|
  //                 |                   |  lan_cen  |  1'b1               |  1'b1              |  1'b1              |
  //                 |  set_io_type = 0  |-----------|---------------------|--------------------|--------------------|
  //                 |                   |  lan_ced  |  1.25 mhz (800 ns)  |  12.5 mhz (80 ns)  |  1'b1              |
  //                 |===================|===========|===============================================================|
  //                 |                   |  lan_clk  |  timescale * pT_CST_CLK                                       |
  //                 |  custom mode:     |-----------|---------------------------------------------------------------|
  //                 |                   |  lan_cen  |  timescale * pT_CST_CLK * pT_CST_CEN                          |
  //                 |  pE_CST_CLK = 1   |-----------|---------------------------------------------------------------|
  //                 |                   |  lan_ced  |  timescale * pT_CST_CLK * pT_CST_CEN * pT_CST_CED             |
  //                 |-----------------------------------------------------------------------------------------------|
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Table (2)    : Structure of ethernet frames (std. 802.3) for testbench emulator / see also note (1)
  //
  //                 |--------------------------------------------------------------------------------|
  //                 | frame header  |      mac header       |        data       |  fcs  | testbench  |
  //                 |---------------|-----------------------|-------------------|-------| ethrenet   |
  //                 |       8       |          14           |      46-1500      |   4   | emulator   |
  //                 |===============|=======================|===================|=======|============|
  //                 |  prm  |  sfd  |  mda  |  msa  |  mlt  |  payload  |  pad  |  crc  | strob      |
  //                 |-------|-------|-------|-------|-------|-----------|-------|-------| name       |
  //                 |   7   |   1   |   6   |   6   |   2   |  0-1500   | 0-46  |   4   |            |
  //                 |=======|=======|=======|=======|=======|===========|=======|=======|============|
  //                 |       |       |       |       |       |           |       |       |            |
  //                 | ***** | ***** | ***** | ***** | ***** | ********* | ***** | ***** | str_frame  |
  //                 | ***** | ***** |       |       |       |           |       |       | str_head   |
  //                 | ***** |       |       |       |       |           |       |       | str_prm    |
  //                 |       | ***** |       |       |       |           |       |       | str_sfd    |
  //                 |       |       | ***** | ***** | ***** | ********* | ***** | ***** | str_body   |
  //                 |       |       | ***** | ***** | ***** |           |       |       | str_mac    |
  //                 |       |       | ***** |       |       |           |       |       | str_mda    |
  //                 |       |       |       | ***** |       |           |       |       | str_msa    |
  //                 |       |       |       |       | ***** |           |       |       | str_mlt    |
  //                 |       |       |       |       |       | ********* | ***** |       | str_pay    |
  //                 |       |       |       |       |       | ***       |       |       | str_ip     |
  //                 |       |       |       |       |       |           |       | ***** | str_crc    |
  //                 |       |       |       |       |       |           |       |       |            |
  //                 |--------------------------------------------------------------------------------|
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Diagram (1)  : Connection options (interface) of ethernet components
  //
  //
  //    --------------  � lan_signals:     ------------  � rx_signals:     --------------  � rxb_signals:      -----------
  //   |              |------------------>|            |----------------->|              |------------------->|           |
  //   |  tb_eth_emu  |   lan interface   |   eth_rx   |   rx packet      |  eth_rx_buf  |   rxb packet       |           |
  //   |              |   lan counters    |            |   rx counters    |              |   rxb counters     |           |
  //    --------------    lan packet       ------------                    --------------                     |           |
  //                                                                                                          |           |
  //                                                                                                          |           |
  //    --------------  � lan_signals:     ------------  � rx_signals:     --------------  � rxb_signals:     |           |
  //   |              |------------------>|            |----------------->|              |------------------->|           |
  //   |   lan phy    |   lan interface   |   eth_rx   |   rx packet      |  eth_rx_buf  |   rxb packet       |   logic   |
  //   |              |                   |            |   rx counters    |              |   rxb counters     |           |
  //    --------------                     ------------                    --------------                     |           |
  //                                                                                                          |           |
  //                                                                                                          |           |
  //    --------------  � lan_signals:     ------------  � tx_signals:     --------------  � txb_signals:     |           |
  //   |              |<------------------|            |<-----------------|              |<-------------------|           |
  //   |   lan phy    |   lan interface   |   eth_tx   |   tx packet      |  eth_tx_buf  |   txb packet       |           |
  //   |              |   lan counters    |            |   tx counters    |              |   txb counters     |           |
  //    --------------    lan packet       ------------                    --------------                      -----------
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Literature (1) : IEEE Std 802.3-2002 / Clause 22 : Media Independent Interface (MII)
  //                                       / Clause 35 : Gigabit Media Independent Interface (GMII)
  //
  //  Literature (2) : RMII Specification (RMII Consortium) [1998]
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Include Files
  //  
  //  Category       Type        Name                    Ver     Description
  //
  //  lib_internal   testbench   tb_eth_emu_clk_v2 .sv   2.0.0   clocks generator of ethernet frames
  //  lib_internal   testbench   tb_eth_emu_str_v2 .sv   2.0.0   strobes generator of ethernet frames
  //  lib_internal   testbench   tb_eth_emu_dat_v2 .sv   2.0.0   data stream generator of ethernet frames
  //  lib_internal   testbench   tb_eth_emu_lan_v2 .sv   2.0.0   lan ethernet interface implementation
  //  lib_internal   testbench   tb_eth_emu_spd_v2 .sv   2.0.0   speed calculator (calculator of inter-frame gap)
  //  lib_internal   testbench   tb_eth_emu_ini_v2 .sv   2.0.0   generator of initialization signal (for settings)
  //
  //  lib_ethernet   function    eth_crc32_8d      .sv   1.0.0   calculator of frame check sequence crc-32 (data 8 bit)
  //
  //  lib_global     function    cbit              .sv   1.0.0   bits calculator for input number (round up)
  //  lib_global     rtl         z_line            .sv   1.0.0   delay line for serial and parallel data
  //  lib_global     rtl         gen_npr           .sv   1.0.0   generator of registers with attribute "noprune"
  //  lib_global     rtl         det_se            .sv   1.0.0   detector of single edge for input strobe
  //  lib_global     rtl         shift_p2s         .sv   1.0.0   shift register for parallel to serial
  //  lib_global     rtl         gen_sstr          .sv   1.0.0   generator of single strobe
  //  lib_global     rtl         gen_mstr          .sv   1.0.0   generator of multi strobes
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Revision List
  //
  //  Version   Date       Author
  //
  //  2.0.0     26.09.16   Pogodaev Danil
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  `timescale 1ns/1ns

  module tb_eth_emu_v2    (

  //---- reset -----------|
    
         i_reset_n        ,

  //---- setting ---------|

  //     update

         i_set_upd        ,
         o_set_upd_ena    ,

  //     transmit speed

         i_set_tx_mode    ,
         i_set_tx_speed   ,
         o_set_tx_speed   ,
         o_set_tx_limit   ,
         i_set_tx_sel     ,

  //     interface
     
         i_set_io_type    ,
         i_set_io_speed   ,
         i_set_io_pause   ,

  //     crc error

         i_set_err_ena    ,
         i_set_err_per    ,

  //     mac header

         i_set_mac_ena    ,
         i_set_mda_type   ,
         i_set_mda_const  ,
         i_set_mda_min    ,
         i_set_mda_max    ,
         i_set_msa_type   ,
         i_set_msa_const  ,
         i_set_msa_min    ,
         i_set_msa_max    ,
         i_set_mlt_type   ,
         i_set_mlt_const  ,
         i_set_mlt_min    ,
         i_set_mlt_max    ,

  //     ip header

         i_set_ip_ena     , 
         i_set_ip_type    ,
         i_set_ip_const   ,
         i_set_ip_min     ,
         i_set_ip_max     ,

  //     data payload

         i_set_pay_type   ,
         i_set_pay_const  ,
         i_set_pay_min    ,
         i_set_pay_max    ,

  //     frame length

         i_set_len_type   ,
         i_set_len_const  ,
         i_set_len_min    ,
         i_set_len_max    ,

  //     frame gap

         i_set_gap_type   ,
         i_set_gap_const  ,
         i_set_gap_min    ,
         i_set_gap_max    ,

  //---- lan packet ------|

  //     clock

         o_lan_clk        ,
         o_lan_cen        ,
         o_lan_ced        ,

  //     strob

         o_lan_str_frame  ,
         o_lan_cnt_frame  ,
         o_lan_str_head   ,
         o_lan_cnt_head   ,
         o_lan_str_prm    ,
         o_lan_str_sfd    ,
         o_lan_str_body   ,
         o_lan_cnt_body   ,
         o_lan_str_mac    ,
         o_lan_cnt_mac    ,
         o_lan_str_mda    ,
         o_lan_str_msa    ,
         o_lan_str_mlt    ,
         o_lan_str_pay    ,
         o_lan_cnt_pay    ,
         o_lan_str_ip     ,
         o_lan_cnt_ip     ,
         o_lan_str_crc    ,
         o_lan_cnt_crc    ,
         o_lan_str_gap    ,
         o_lan_cnt_gap    ,

  //     data

         o_lan_dat_load   ,
         o_lan_dat_frame  ,

  //     length

         o_lan_len_frame  ,
         o_lan_len_body   ,
         o_lan_len_pay    ,
         o_lan_len_gap    ,

  //     fcs crc

         o_lan_crc_rdy    ,
         o_lan_crc_err    ,

  //---- lan counters ----|

  //     setting

         i_lan_set_p_top  ,
         i_lan_set_p_low  ,

  //     detector

         o_lan_det_p_rdy  ,
         o_lan_det_p_err  ,

  //     counters

         i_lan_cnt_p_clr  ,
         o_lan_cnt_p_rdy  ,
         o_lan_cnt_p_all  ,
         o_lan_cnt_p_err  ,
 
  //---- lan interface ---|

  //     lan mii
  
         o_mii_txd        ,
         o_mii_tx_en      ,
         o_mii_tx_clk     ,

  //     lan rmii

         o_rmii_txd       ,
         o_rmii_tx_en     ,
         o_rmii_tx_clk    ,
         o_rmii_tx_clk_s  ,

  //     lan gmii

         o_gmii_txd       ,
         o_gmii_tx_en     ,
         o_gmii_tx_clk   );

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // include files : functions
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
                                                                     //      |
  `include "cbit.sv"                                                 // req  | bits calculator for input number (round up)
                                                                     //      |
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // parameters : setting
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
                                                                     //      |                                                 |          |           |
  // packet structure -----------------------------------------------//------|-------------------------------------------------|----------|-----------|-----------------------------------------------------------------------//
                                                                     //      |                                                 |          |           |
  // frame header                                                    //------|-------------------------------------------------|----------|-----------|-----------------------------------------------------------------------//
                                                                     //      |                                                 |          |           |
  parameter bit  pE_LAN_HEAD  = 1                               ;    // req  | enable of frame header                          | on/off   | def: 1    | info: used for custom mode ; see 'comment.txt' (1)
                                                                     //      |                                                 |          |           |
  // frame length                                                    //------|-------------------------------------------------|----------|-----------|-----------------------------------------------------------------------//
                                                                     //      |                                                 |          |           |
  parameter int  pW_LAN_LEN   = 11                              ;    // req  | frame length / width of bus                     | bit      | def: 11   | value: 14 (for jumbo-frame)
  parameter int  pW_LAN_GAP   = 16                              ;    // req  | frame gap    / width of bus                     | bit      | def: 16   |
                                                                     //      |                                                 |          |           |
  // ip header                                                       //------|-------------------------------------------------|----------|-----------|-----------------------------------------------------------------------//
                                                                     //      |                                                 |          |           |
  parameter int  pN_LAN_IPL   = 1                               ;    // opt  | number of first (low) byte in payload           | byte     | def: 1    |
  parameter int  pN_LAN_IPH   = 4                               ;    // opt  | number of last (high) byte in payload           | byte     |           |
                                                                     //      |                                                 |          |           |
  // payload                                                         //------|-------------------------------------------------|----------|-----------|-----------------------------------------------------------------------//
                                                                     //      |                                                 |          |           |
  parameter int  pW_LAN_PAY   = 8                               ;    // req  | payload constant / width of bus                 | bit      | def: 8    | info: for constant value only (i_set_pay_const); value: 8/16/24/...
                                                                     //      |                                                 |          |           |
  // packet counters ------------------------------------------------//------|-------------------------------------------------|----------|-----------|-----------------------------------------------------------------------//
                                                                     //      |                                                 |          |           |
  parameter int  pN_LAN_CNT   = 0                               ;    // opt  | number of counters groups                       | group    | def: 0    | info: see purpose (5), note (2)
  parameter int  pW_LAN_CNT   = 24                              ;    // opt  | width of counters                               | bit      | def: 24   |
                                                                     //      |                                                 |          |           |
  // custom mode (see table 1) --------------------------------------//------|-------------------------------------------------|----------|-----------|-----------------------------------------------------------------------//
                                                                     //      |                                                 |          |           |
  parameter bit  pE_CST_CLK   = 0                               ;    // opt  | enable of custom clock                          | on/off   | def: 0    | info: see table (1); lan interface 'off', used lan packet only
                                                                     //      |                                                 |          |           |
  parameter int  pT_CST_CLK   = 10                              ;    // opt  | period of custom clock                          | time     | def: 10   | info: 0 = 10
  parameter int  pT_CST_CEN   = 1                               ;    // opt  | period of custom clock enable                   | t.clk    | def: 1    | info: 0 = 1
  parameter int  pT_CST_CED   = 1                               ;    // opt  | period of custom clock enable for data          | t.cen    | def: 1    | info: 0 = 1
                                                                     //      |                                                 |          |           |
  // simulation -----------------------------------------------------//------|-------------------------------------------------|----------|-----------|-----------------------------------------------------------------------//
                                                                     //      |                                                 |          |           |
  parameter bit  pS_SIM_GLTCH = 1                               ;    // opt  | simulator glitch off (add out registers)        | on/off   | def: 1    | info: see 'comment.txt' (2)
                                                                     //      |                                                 |          |           |
                                                                     //      |   0 : set real time     / glitch on             |          |           | info: used for real time setting of payload constant
                                                                     //      |   1 : add out registers / glitch off            |          |           | info: used for better visual view only
                                                                     //      |                                                 |          |           |
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // parameters : local (for i/o)
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
                                                                     //      |                                                 |          |           |
  // packet structure -----------------------------------------------//------|-------------------------------------------------|----------|-----------|-----------------------------------------------------------------------//
                                                                     //      |                                                 |          |           |
  // ip header                                                       //------|-------------------------------------------------|----------|-----------|-----------------------------------------------------------------------//
                                                                     //      |                                                 |          |           |
  parameter int  lpN_LAN_IPL  = pN_LAN_IPL                      ;    // opt  | number of first (low) byte in payload           | byte     | def: 1    |
  parameter int  lpN_LAN_IPH  = pN_LAN_IPH >= pN_LAN_IPL ?           // opt  | number of last (high) byte in payload           | byte     |           |
                                pN_LAN_IPH  : pN_LAN_IPL        ;    //      |                                                 |          |           |
                                                                     //      |                                                 |          |           |
  parameter int  lpN_LAN_IP   = lpN_LAN_IPH <= lpN_LAN_IPL ? 1  :    //      |                                                 |          |           |
                                lpN_LAN_IPH  - lpN_LAN_IPL + 1  ;    // opt  | number of bytes                                 | byte     |           |
                                                                     //      |                                                 |          |           |
  parameter int  lpW_LAN_IP   = lpN_LAN_IP * 8                  ;    // opt  | number of bits                                  | bit      |           | info: used for 'lan_dat_ip'
                                                                     //      |                                                 |          |           |
  parameter int  lpW_LAN_IP_C = cbit(1,lpN_LAN_IP)              ;    // opt  | width of byte counter                           | bit      |           | info: used for 'lan_cnt_ip'
                                                                     //      |                                                 |          |           |
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // interface
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
                                                                     //      |                                                 |          |           |
  //-------------------------------------// reset -------------------//------|-------------------------------------------------|----------|-----------|-----------------------------------------------------------------------//
                                                                     //      |                                                 |          |           |
  input  logic                              i_reset_n           ;    // opt  | sync clear (ative low)                          | strob    | def: 1    | info: used 'lan_clk' (input reg)
                                                                     //      |                                                 |          |           |
  //-------------------------------------// setting -----------------//------|-------------------------------------------------|----------|-----------|-----------------------------------------------------------------------//
                                                                     //      |                                                 |          |           |
  //                                        update                   //------|-------------------------------------------------|----------|-----------|-----------------------------------------------------------------------//
                                                                     //      |                                                 |          |           |
  input  logic                              i_set_upd           ;    // opt  | setting : update request                        | strob    | def: 0    | info: sensitivity to rising edge ; see 'comment.txt' (3)
  output logic                              o_set_upd_ena       ;    // opt  | setting : update enable                         | pulse    |           | width: lan_cen
                                                                     //      |                                                 |          |           |
  //                                        interface                //------|-------------------------------------------------|----------|-----------|-----------------------------------------------------------------------//
                                                                     //      |                                                 |          |           |
  input  logic    [               1 :0 ]    i_set_io_type       ;    // req  | setting : transmit type                         | pdata    |           |
                                                                     //      |       0 : transmit gmii                         |          |           |
                                                                     //      |       1 : transmit rmii                         |          |           |
                                                                     //      |       2 : transmit mii                          |          |           |
                                                                     //      |                                                 |          |           |
  input  logic    [               1 :0 ]    i_set_io_speed      ;    // req  | setting : transmit speed                        | pdata    |           |
                                                                     //      |       0 : 1000 mbit/s                           |          |           |
                                                                     //      |       1 : 100  mbit/s                           |          |           |
                                                                     //      |       2 : 10   mbit/s                           |          |           |
                                                                     //      |                                                 |          |           |
  input  logic                              i_set_io_pause      ;    // opt  | setting : transmit pause                        | strob    | def: 0    | info: flow control ; see 'comment.txt' (4)
                                                                     //      |       0 : transmit enable                       |          |           |
                                                                     //      |       1 : transmit disable                      |          |           |
                                                                     //      |                                                 |          |           |
  //                                        transmit speed           //------|-------------------------------------------------|----------|-----------|-----------------------------------------------------------------------//
                                                                     //      |                                                 |          |           |
  input  logic                              i_set_tx_mode       ;    // opt  | setting : transmit mode                         | strob    | def: 0    | info: see 'comment.txt' (5)
                                                                     //      |       0 : transmit gap manual                   |          |           | info: used 'i_set_gap_type'
                                                                     //      |       1 : transmit gap auto                     |          |           | info: used 'i_set_tx_speed'
                                                                     //      |                                                 |          |           |
  input  logic    [              19 :0 ]    i_set_tx_speed      ;    // opt  | setting : transmit speed (kbit/sec) / set       | pdata    |           | value: 0-1000000 kbit/sec
  output logic    [              19 :0 ]    o_set_tx_speed      ;    // opt  | setting : transmit speed (kbit/sec) / real      | pdata    |           | value: 0-1000000 kbit/sec
  output logic    [              19 :0 ]    o_set_tx_limit      ;    // opt  | setting : transmit speed (kbit/sec) / limit     | pdata    |           | value: 0-1000000 kbit/sec ; info: see 'comment.txt' (6)
                                                                     //      |                                                 |          |           |
  input  logic    [               1 :0 ]    i_set_tx_sel        ;    // opt  | setting : transmit speed selection              | pdata    | def: 0    |
                                                                     //      |                                                 |          |           |
                                                                     //      |       0 : payload                               |          |           |
                                                                     //      |       1 : payload + mac                         |          |           |
                                                                     //      |       2 : payload + mac + fcs crc               |          |           |
                                                                     //      |       3 : payload + mac + fcs crc + head        |          |           |
                                                                     //      |                                                 |          |           |
  //                                        crc error                //------|-------------------------------------------------|----------|-----------|-----------------------------------------------------------------------//
                                                                     //      |                                                 |          |           |
  input  logic                              i_set_err_ena       ;    // opt  | setting : crc error                             | strob    | def: 0    |
                                                                     //      |       0 : crc error insert disable              |          |           |
                                                                     //      |       1 : crc error insert enable               |          |           | info: crc = 32'h ffffffff
                                                                     //      |                                                 |          |           |
  input  logic    [               6 :0 ]    i_set_err_per       ;    // opt  | setting : crc error probability (0-100%)        | pdata    | def: 0    | info: used $random
                                                                     //      |                                                 |          |           |
  //                                        mac header               //------|-------------------------------------------------|----------|-----------|-----------------------------------------------------------------------//
                                                                     //      |                                                 |          |           |
  input  logic                              i_set_mac_ena       ;    // opt  | setting : mac header                            | strob    | def: 0    |
                                                                     //      |       0 : mac inserter disable                  |          |           |
                                                                     //      |       1 : mac inserter enable                   |          |           |
                                                                     //      |                                                 |          |           |
  //                                        mac header / mda         //------|-------------------------------------------------|----------|-----------|-----------------------------------------------------------------------//
                                                                     //      |                                                 |          |           |
  input  logic    [               2 :0 ]    i_set_mda_type      ;    // opt  | setting : mda type                              | pdata    | def: 0    |
                                                                     //      |       0 : mda constant                          |          |           | value: set_mda_const
                                                                     //      |       1 : mda random                            |          |           | value: set_mda_min to set_mda_max
                                                                     //      |       2 : mda counter of byte (individual)      |          |           | value: set_mda_min to set_mda_max
                                                                     //      |       3 : mda counter of byte (continuous)      |          |           | value: set_mda_min to set_mda_max
                                                                     //      |       4 : mda counter of packets                |          |           | value: set_mda_min to set_mda_max
                                                                     //      |                                                 |          |           |
  input  logic    [              47 :0 ]    i_set_mda_const     ;    // opt  | setting : mda constant / value                  | pdata    |           |
  input  logic    [              47 :0 ]    i_set_mda_min       ;    // opt  | setting : mda minimum  / value                  | pdata    | def: '0   |
  input  logic    [              47 :0 ]    i_set_mda_max       ;    // opt  | setting : mda maximum  / value                  | pdata    | def: '1   |
                                                                     //      |                                                 |          |           |
  //                                        mac header / msa         //------|-------------------------------------------------|----------|-----------|-----------------------------------------------------------------------//
                                                                     //      |                                                 |          |           |
  input  logic    [               2 :0 ]    i_set_msa_type      ;    // opt  | setting : msa type                              | pdata    | def: 0    |
                                                                     //      |       0 : msa constant                          |          |           | value: set_msa_const
                                                                     //      |       1 : msa random                            |          |           | value: set_msa_min to set_msa_max
                                                                     //      |       2 : msa counter of byte (individual)      |          |           | value: set_msa_min to set_msa_max
                                                                     //      |       3 : msa counter of byte (continuous)      |          |           | value: set_msa_min to set_msa_max
                                                                     //      |       4 : msa counter of packets                |          |           | value: set_msa_min to set_msa_max
                                                                     //      |                                                 |          |           |
  input  logic    [              47 :0 ]    i_set_msa_const     ;    // opt  | setting : msa constant / value                  | pdata    |           |
  input  logic    [              47 :0 ]    i_set_msa_min       ;    // opt  | setting : msa minimum  / value                  | pdata    | def: '0   |
  input  logic    [              47 :0 ]    i_set_msa_max       ;    // opt  | setting : msa maximum  / value                  | pdata    | def: '1   |
                                                                     //      |                                                 |          |           |
  //                                        mac header / mlt         //------|-------------------------------------------------|----------|-----------|-----------------------------------------------------------------------//
                                                                     //      |                                                 |          |           |
  input  logic    [               2 :0 ]    i_set_mlt_type      ;    // opt  | setting : mlt type                              | pdata    | def: 5    |
                                                                     //      |       0 : mlt constant                          |          |           | value: set_mlt_const
                                                                     //      |       1 : mlt random                            |          |           | value: set_mlt_min to set_mlt_max
                                                                     //      |       2 : mlt counter of byte (individual)      |          |           | value: set_mlt_min to set_mlt_max
                                                                     //      |       3 : mlt counter of byte (continuous)      |          |           | value: set_mlt_min to set_mlt_max
                                                                     //      |       4 : mlt counter of packets                |          |           | value: set_mlt_min to set_mlt_max
                                                                     //      |       5 : mlt length  of payload                |          |           | value: lan_len_pay
                                                                     //      |                                                 |          |           |
  input  logic    [              15 :0 ]    i_set_mlt_const     ;    // opt  | setting : mlt constant / value                  | pdata    |           |
  input  logic    [              15 :0 ]    i_set_mlt_min       ;    // opt  | setting : mlt minimum  / value                  | pdata    | def: '0   |
  input  logic    [              15 :0 ]    i_set_mlt_max       ;    // opt  | setting : mlt maximum  / value                  | pdata    | def: '1   |
                                                                     //      |                                                 |          |           |
  //                                        ip header                //------|-------------------------------------------------|----------|-----------|-----------------------------------------------------------------------//
                                                                     //      |                                                 |          |           |
  input  logic                              i_set_ip_ena        ;    // opt  | setting : ip header                             | strob    | def: 0    |
                                                                     //      |       0 : ip insert disable                     |          |           |
                                                                     //      |       1 : ip insert enable                      |          |           |
                                                                     //      |                                                 |          |           |
  input  logic    [               2 :0 ]    i_set_ip_type       ;    // opt  | setting : ip type                               | pdata    | def: 0    |
                                                                     //      |       0 : ip constant                           |          |           | value: set_ip_const
                                                                     //      |       1 : ip random                             |          |           | value: set_ip_min to set_ip_max
                                                                     //      |       2 : ip counter of byte (individual)       |          |           | value: set_ip_min to set_ip_max
                                                                     //      |       3 : ip counter of byte (continuous)       |          |           | value: set_ip_min to set_ip_max
                                                                     //      |       4 : ip counter of packets                 |          |           | value: set_ip_min to set_ip_max
                                                                     //      |                                                 |          |           |
  input  logic    [ lpW_LAN_IP   -1 :0 ]    i_set_ip_const      ;    // opt  | setting : ip constant                           | pdata    |           |
  input  logic    [ lpW_LAN_IP   -1 :0 ]    i_set_ip_min        ;    // opt  | setting : ip minimum                            | pdata    | def: '0   |
  input  logic    [ lpW_LAN_IP   -1 :0 ]    i_set_ip_max        ;    // opt  | setting : ip maximum                            | pdata    | def: '1   |
                                                                     //      |                                                 |          |           |
  //                                        payload                  //------|-------------------------------------------------|----------|-----------|-----------------------------------------------------------------------//
                                                                     //      |                                                 |          |           |
  input  logic    [               2 :0 ]    i_set_pay_type      ;    // req  | setting : payload type                          | pdata    | def: 0    |
                                                                     //      |       0 : payload constant                      |          |           | value: set_pay_const
                                                                     //      |       1 : payload random                        |          |           | value: set_pay_min to set_pay_max
                                                                     //      |       2 : payload counter of byte (individual)  |          |           | value: set_pay_min to set_pay_max
                                                                     //      |       3 : payload counter of byte (continuous)  |          |           | value: set_pay_min to set_pay_max
                                                                     //      |       4 : payload counter of packets            |          |           | value: set_pay_min to set_pay_max
                                                                     //      |                                                 |          |           |
  input  logic    [ pW_LAN_PAY   -1 :0 ]    i_set_pay_const     ;    // req  | setting : payload constant / value              | pdata    |           |
  input  logic    [               7 :0 ]    i_set_pay_min       ;    // opt  | setting : payload minimum  / value              | pdata    | def: '0   |
  input  logic    [               7 :0 ]    i_set_pay_max       ;    // opt  | setting : payload maximum  / value              | pdata    | def: '1   |
                                                                     //      |                                                 |          |           |
  //                                        frame length             //------|-------------------------------------------------|----------|-----------|-----------------------------------------------------------------------//
                                                                     //      |                                                 |          |           |
  input  logic     [              1 :0 ]    i_set_len_type      ;    // req  | setting : length type                           | pdata    | def: 0    |
                                                                     //      |       0 : length constant                       |          |           | value: set_len_const
                                                                     //      |       1 : length random                         |          |           | value: set_len_min to set_len_max
                                                                     //      |       2 : length counter                        |          |           | value: set_len_min to set_len_max
                                                                     //      |                                                 |          |           |
  input  logic    [ pW_LAN_LEN   -1 :0 ]    i_set_len_const     ;    // req  | setting : length constant / value (byte)        | pdata    |           |
  input  logic    [ pW_LAN_LEN   -1 :0 ]    i_set_len_min       ;    // opt  | setting : length minimum  / value (byte)        | pdata    | def: 72   |
  input  logic    [ pW_LAN_LEN   -1 :0 ]    i_set_len_max       ;    // opt  | setting : length maximum  / value (byte)        | pdata    | def: 1526 | value: 9018 (for jumbo-frame)
                                                                     //      |                                                 |          |           |
  //                                        frame gap                //------|-------------------------------------------------|----------|-----------|-----------------------------------------------------------------------//
                                                                     //      |                                                 |          |           |
  input  logic    [               1 :0 ]    i_set_gap_type      ;    // req  | setting : gap type                              | pdata    | def: 0    |
                                                                     //      |       0 : gap constant                          |          |           | value: set_gap_const
                                                                     //      |       1 : gap random                            |          |           | value: set_gap_min to set_gap_max
                                                                     //      |       2 : gap counter                           |          |           | value: set_gap_min to set_gap_max
                                                                     //      |                                                 |          |           |
  input  logic    [ pW_LAN_GAP   -1 :0 ]    i_set_gap_const     ;    // req  | setting : gap constant / value (byte)           | pdata    | def: 12   |
  input  logic    [ pW_LAN_GAP   -1 :0 ]    i_set_gap_min       ;    // opt  | setting : gap minimum  / value (byte)           | pdata    | def: 12   | info: min gap = 96 bit or 12 byte (std. 802.3)
  input  logic    [ pW_LAN_GAP   -1 :0 ]    i_set_gap_max       ;    // opt  | setting : gap maximum  / value (byte)           | pdata    |           |
                                                                     //      |                                                 |          |           |
  //-------------------------------------// lan packet --------------//------|-------------------------------------------------|----------|-----------|-----------------------------------------------------------------------//
                                                                     //      |                                                 |          |           |
  //                                        clock                    //------|-------------------------------------------------|----------|-----------|-----------------------------------------------------------------------//
                                                                     //      |                                                 |          |           |
  output logic                              o_lan_clk           ;    // req  | clock                                           | clock    |           | info: see table (1)
  output logic                              o_lan_cen           ;    // req  | clock enable                                    | pulse    |           | info: see table (1) ; width: lan_clk
  output logic                              o_lan_ced           ;    // req  | clock enable for data                           | pulse    |           | info: see table (1) ; width: lan_cen
                                                                     //      |                                                 |          |           |
  //                                        strob                    //------|-------------------------------------------------|----------|-----------|-----------------------------------------------------------------------//
                                                                     //      |                                                 |          |           |
  output logic    [               2 :0 ]    o_lan_str_frame     ;    // req  | frame (packet)          / strob                 | strob    |           | info: see purpose (2)
  output logic    [ pW_LAN_LEN   -1 :0 ]    o_lan_cnt_frame     ;    // opt  | frame (packet)          / strob counter         | count up |           |
  output logic    [               2 :0 ]    o_lan_str_head      ;    // opt  | frame header            / strob                 | strob    |           |
  output logic    [               3 :0 ]    o_lan_cnt_head      ;    // opt  | frame header            / strob counter         | count up |           |
  output logic    [               2 :0 ]    o_lan_str_prm       ;    // opt  | frame header preamble   / strob                 | strob    |           |
  output logic    [               2 :0 ]    o_lan_str_sfd       ;    // opt  | frame header sfd        / strob                 | strob    |           |
  output logic    [               2 :0 ]    o_lan_str_body      ;    // opt  | frame body              / strob                 | strob    |           |
  output logic    [ pW_LAN_LEN   -1 :0 ]    o_lan_cnt_body      ;    // opt  | frame body              / strob counter         | count up |           |
  output logic    [               2 :0 ]    o_lan_str_mac       ;    // opt  | mac header              / strob                 | strob    |           |
  output logic    [               3 :0 ]    o_lan_cnt_mac       ;    // opt  | mac header              / strob counter         | count up |           |
  output logic    [               2 :0 ]    o_lan_str_mda       ;    // opt  | mac destination address / strob                 | strob    |           |
  output logic    [               2 :0 ]    o_lan_str_msa       ;    // opt  | mac source address      / strob                 | strob    |           |
  output logic    [               2 :0 ]    o_lan_str_mlt       ;    // opt  | mac length or type      / strob                 | strob    |           |
  output logic    [               2 :0 ]    o_lan_str_ip        ;    // opt  | ip header               / strob                 | strob    |           |
  output logic    [ lpW_LAN_IP_C -1 :0 ]    o_lan_cnt_ip        ;    // opt  | ip header               / strob counter         | count up |           |
  output logic    [               2 :0 ]    o_lan_str_pay       ;    // opt  | payload & pad           / strob                 | strob    |           |
  output logic    [ pW_LAN_LEN   -1 :0 ]    o_lan_cnt_pay       ;    // opt  | payload & pad           / strob counter         | count up |           |
  output logic    [               2 :0 ]    o_lan_str_crc       ;    // opt  | fcs crc-32              / strob                 | strob    |           |
  output logic    [               2 :0 ]    o_lan_cnt_crc       ;    // opt  | fcs crc-32              / strob counter         | count up |           |
  output logic    [               2 :0 ]    o_lan_str_gap       ;    // opt  | inter-frame gap         / strob                 | strob    |           |
  output logic    [ pW_LAN_GAP   -1 :0 ]    o_lan_cnt_gap       ;    // opt  | inter-frame gap         / strob counter         | count up |           |
                                                                     //      |                                                 |          |           |
  //                                        data                     //------|-------------------------------------------------|----------|-----------|-----------------------------------------------------------------------//
                                                                     //      |                                                 |          |           |
  output logic                              o_lan_dat_load      ;    // req  | frame (packet)          / load (byte)           | pulse    |           | width: lan_cen
  output logic    [               7 :0 ]    o_lan_dat_frame     ;    // req  | frame (packet)          / data (byte)           | pdata    |           |
                                                                     //      |                                                 |          |           |
  //                                        length                   //------|-------------------------------------------------|----------|-----------|-----------------------------------------------------------------------//
                                                                     //      |                                                 |          |           |
  output logic    [ pW_LAN_LEN   -1 :0 ]    o_lan_len_frame     ;    // req  | frame (packet)          / length (byte)         | pdata    |           |
  output logic    [ pW_LAN_LEN   -1 :0 ]    o_lan_len_body      ;    // opt  | frame body              / length (byte)         | pdata    |           |
  output logic    [ pW_LAN_LEN   -1 :0 ]    o_lan_len_pay       ;    // opt  | payload                 / length (byte)         | pdata    |           |
  output logic    [ pW_LAN_GAP   -1 :0 ]    o_lan_len_gap       ;    // opt  | inter-frame gap         / length (byte)         | pdata    |           |
                                                                     //      |                                                 |          |           |
  //                                        fcs crc                  //------|-------------------------------------------------|----------|-----------|-----------------------------------------------------------------------//
                                                                     //      |                                                 |          |           |
  output logic                              o_lan_crc_rdy       ;    // req  | fcs crc-32              / ready                 | pulse    |           | width: lan_cen
  output logic                              o_lan_crc_err       ;    // req  | fcs crc-32              / error                 | strob    |           |
                                                                     //      |                                                 |          |           |
  //-------------------------------------// lan counters ------------//------|-------------------------------------------------|----------|-----------|-----------------------------------------------------------------------//
                                                                     //      |                                                 |          |           |
  //                                        setting                  //------|-------------------------------------------------|----------|-----------|-----------------------------------------------------------------------//
  input  logic    [ pN_LAN_CNT      :1 ]                             //      |                                                 |          |           |
                  [ pW_LAN_LEN   -1 :0 ]    i_lan_set_p_top     ;    // opt  | top bound of body length (not frame)            | array    |           | info: see purpose (5) and note (2)
  input  logic    [ pN_LAN_CNT      :1 ]                             //      |                                                 |          |           |
                  [ pW_LAN_LEN   -1 :0 ]    i_lan_set_p_low     ;    // opt  | low bound of body length (not frame)            | array    |           | info: see purpose (5) and note (2)
                                                                     //      |                                                 |          |           |
  //                                        detector                 //------|-------------------------------------------------|----------|-----------|-----------------------------------------------------------------------//
                                                                     //      |                                                 |          |           |
  output logic                              o_lan_det_p_rdy     ;    // opt  | packet ready                                    | pulse    |           | width: lan_cen
  output logic                              o_lan_det_p_err     ;    // opt  | packet error                                    | strob    |           |
                                                                     //      |                                                 |          |           |
  //                                        counters                 //------|-------------------------------------------------|----------|-----------|-----------------------------------------------------------------------//
                                                                     //      |                                                 |          |           |
  input  logic                              i_lan_cnt_p_clr     ;    // opt  | current value / clear                           | strob    | def: 0    | info: sensitivity to rising edge
                                                                     //      |                                                 |          |           |
  output logic                              o_lan_cnt_p_rdy     ;    // opt  | current value / ready                           | pulse    |           | width: lan_cen
  output logic    [ pN_LAN_CNT      :0 ]                             //      |                                                 |          |           |
                  [ pW_LAN_CNT   -1 :0 ]    o_lan_cnt_p_all     ;    // opt  | current value / packet all                      | count up |           |
  output logic    [ pN_LAN_CNT      :0 ]                             //      |                                                 |          |           |
                  [ pW_LAN_CNT   -1 :0 ]    o_lan_cnt_p_err     ;    // opt  | current value / packet error                    | count up |           |
                                                                     //      |                                                 |          |           |
  //-------------------------------------// lan interface -----------//------|-------------------------------------------------|----------|-----------|-----------------------------------------------------------------------//
                                                                     //      |                                                 |          |           |
  //                                        lan mii                  //------|-------------------------------------------------|----------|-----------|-----------------------------------------------------------------------//
                                                                     //      |                                                 |          |           |
  output logic    [               3 :0 ]    o_mii_txd           ;    // req  | transmit data                                   | pdata    |           | info: for latching used rising edge of clock
  output logic                              o_mii_tx_en         ;    // req  | transmit enable                                 | strob    |           |
  output logic                              o_mii_tx_clk        ;    // req  | transmit clock                                  | clock    |           | info: see table (1)
                                                                     //      |                                                 |          |           |
  //                                        lan rmii                 //------|-------------------------------------------------|----------|-----------|-----------------------------------------------------------------------//
                                                                     //      |                                                 |          |           |
  output logic    [               1 :0 ]    o_rmii_txd          ;    // req  | transmit data                                   | pdata    |           | info: for latching used rising edge of clock
  output logic                              o_rmii_tx_en        ;    // req  | transmit enable                                 | strob    |           | 
  output logic                              o_rmii_tx_clk       ;    // req  | transmit clock                                  | clock    |           | info: see table (1)
  output logic                              o_rmii_tx_clk_s     ;    // test | transmit clock / speed adapter                  | strob    |           |
                                                                     //      |                                                 |          |           |
                                                                     //      |  - for 100 mbit/s : 50 mhz                      |          |           |
                                                                     //      |  - for 10  mbit/s :  5 mhz (not 50 mhz)         |          |           |
                                                                     //      |                                                 |          |           |
  //                                        lan gmii                 //------|-------------------------------------------------|----------|-----------|-----------------------------------------------------------------------//
                                                                     //      |                                                 |          |           |
  output logic    [               7 :0 ]    o_gmii_txd          ;    // req  | transmit data                                   | pdata    |           | info: for latching used rising edge of clock
  output logic                              o_gmii_tx_en        ;    // req  | transmit enable                                 | strob    |           | 
  output logic                              o_gmii_tx_clk       ;    // req  | transmit clock                                  | clock    |           | info: see table (1)
                                                                     //      |                                                 |          |           |
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // logic
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  genvar                                    j                         ;

  //-------------------------------------// in logic -----------------| [ after in reg ]

  var    logic                              ri_reset_n                ;

  //-------------------------------------// out logic / output -------| [ after out reg ]

  //                                        setting / update

  var    logic                              ro_set_upd_ena            ;

  //                                        setting / tx speed

  var    logic    [              19 :0 ]    ro_set_tx_limit           ;
  var    logic    [              19 :0 ]    ro_set_tx_speed           ;

  //                                        lan packet

  var    logic                              ro_lan_cen                ;
  var    logic                              ro_lan_ced                ;

  var    logic    [               2 :0 ]    ro_lan_str_frame          ;
  var    logic    [ pW_LAN_LEN   -1 :0 ]    ro_lan_cnt_frame          ;
  var    logic    [               2 :0 ]    ro_lan_str_head           ;
  var    logic    [               3 :0 ]    ro_lan_cnt_head           ;
  var    logic    [               2 :0 ]    ro_lan_str_prm            ;
  var    logic    [               2 :0 ]    ro_lan_str_sfd            ;
  var    logic    [               2 :0 ]    ro_lan_str_body           ;
  var    logic    [ pW_LAN_LEN   -1 :0 ]    ro_lan_cnt_body           ;
  var    logic    [               2 :0 ]    ro_lan_str_mac            ;
  var    logic    [               3 :0 ]    ro_lan_cnt_mac            ;
  var    logic    [               2 :0 ]    ro_lan_str_mda            ;
  var    logic    [               2 :0 ]    ro_lan_str_msa            ;
  var    logic    [               2 :0 ]    ro_lan_str_mlt            ;
  var    logic    [               2 :0 ]    ro_lan_str_ip             ;
  var    logic    [ lpW_LAN_IP_C -1 :0 ]    ro_lan_cnt_ip             ;
  var    logic    [               2 :0 ]    ro_lan_str_pay            ;
  var    logic    [ pW_LAN_LEN   -1 :0 ]    ro_lan_cnt_pay            ;
  var    logic    [               2 :0 ]    ro_lan_str_crc            ;
  var    logic    [               2 :0 ]    ro_lan_cnt_crc            ;
  var    logic    [               2 :0 ]    ro_lan_str_gap            ;
  var    logic    [ pW_LAN_GAP   -1 :0 ]    ro_lan_cnt_gap            ;

  var    logic                              ro_lan_dat_load           ;
  var    logic    [               7 :0 ]    ro_lan_dat_frame          ;

  var    logic    [ pW_LAN_LEN   -1 :0 ]    ro_lan_len_frame          ;
  var    logic    [ pW_LAN_LEN   -1 :0 ]    ro_lan_len_body           ;
  var    logic    [ pW_LAN_LEN   -1 :0 ]    ro_lan_len_pay            ;
  var    logic    [ pW_LAN_GAP   -1 :0 ]    ro_lan_len_gap            ;

  var    logic                              ro_lan_crc_rdy            ;
  var    logic                              ro_lan_crc_err            ;

  //                                        lan counters

  var    logic                              ro_lan_det_p_rdy          ;
  var    logic                              ro_lan_det_p_err          ;

  var    logic                              ro_lan_cnt_p_rdy          ;
  var    logic    [ pN_LAN_CNT      :0 ]
                  [ pW_LAN_CNT   -1 :0 ]    ro_lan_cnt_p_all          ;
  var    logic    [ pN_LAN_CNT      :0 ]
                  [ pW_LAN_CNT   -1 :0 ]    ro_lan_cnt_p_err          ;

  //                                        lan interface

  var    logic    [               3 :0 ]    ro_mii_txd                ;
  var    logic                              ro_mii_tx_en              ;

  var    logic    [               1 :0 ]    ro_rmii_txd               ;
  var    logic                              ro_rmii_tx_en             ;
  var    logic                              ro_rmii_tx_clk_s          ;

  var    logic    [               7 :0 ]    ro_gmii_txd               ;
  var    logic                              ro_gmii_tx_en             ;

  //-------------------------------------// out logic / next to out --| [ before out reg ]

  //                                        setting / update

  var    logic                              no_set_upd_ena            ;

  //                                        setting / tx speed

  var    logic    [              19 :0 ]    no_set_tx_limit           ;
  var    logic    [              19 :0 ]    no_set_tx_speed           ;

  //                                        lan packet

  var    logic                              no_lan_clk                ;
  var    logic                              no_lan_cen                ;
  var    logic                              no_lan_ced                ;

  var    logic    [               2 :0 ]    no_lan_str_frame          ;
  var    logic    [ pW_LAN_LEN   -1 :0 ]    no_lan_cnt_frame          ;
  var    logic    [               2 :0 ]    no_lan_str_head           ;
  var    logic    [               3 :0 ]    no_lan_cnt_head           ;
  var    logic    [               2 :0 ]    no_lan_str_prm            ;
  var    logic    [               2 :0 ]    no_lan_str_sfd            ;
  var    logic    [               2 :0 ]    no_lan_str_body           ;
  var    logic    [ pW_LAN_LEN   -1 :0 ]    no_lan_cnt_body           ;
  var    logic    [               2 :0 ]    no_lan_str_mac            ;
  var    logic    [               3 :0 ]    no_lan_cnt_mac            ;
  var    logic    [               2 :0 ]    no_lan_str_mda            ;
  var    logic    [               2 :0 ]    no_lan_str_msa            ;
  var    logic    [               2 :0 ]    no_lan_str_mlt            ;
  var    logic    [               2 :0 ]    no_lan_str_ip             ;
  var    logic    [ lpW_LAN_IP_C -1 :0 ]    no_lan_cnt_ip             ;
  var    logic    [               2 :0 ]    no_lan_str_pay            ;
  var    logic    [ pW_LAN_LEN   -1 :0 ]    no_lan_cnt_pay            ;
  var    logic    [               2 :0 ]    no_lan_str_crc            ;
  var    logic    [               2 :0 ]    no_lan_cnt_crc            ;
  var    logic    [               2 :0 ]    no_lan_str_gap            ;
  var    logic    [ pW_LAN_GAP   -1 :0 ]    no_lan_cnt_gap            ;

  var    logic                              no_lan_dat_load           ;
  var    logic    [               7 :0 ]    no_lan_dat_frame          ;

  var    logic    [ pW_LAN_LEN   -1 :0 ]    no_lan_len_frame          ;
  var    logic    [ pW_LAN_LEN   -1 :0 ]    no_lan_len_body           ;
  var    logic    [ pW_LAN_LEN   -1 :0 ]    no_lan_len_pay            ;
  var    logic    [ pW_LAN_GAP   -1 :0 ]    no_lan_len_gap            ;

  var    logic                              no_lan_crc_rdy            ;
  var    logic                              no_lan_crc_err            ;

  //                                        lan counters

  var    logic                              no_lan_det_p_rdy          ;
  var    logic                              no_lan_det_p_err          ;
  var    logic                              no_lan_cnt_p_rdy          ;
  var    logic    [ pN_LAN_CNT      :0 ]
                  [ pW_LAN_CNT   -1 :0 ]    no_lan_cnt_p_all          ;
  var    logic    [ pN_LAN_CNT      :0 ]
                  [ pW_LAN_CNT   -1 :0 ]    no_lan_cnt_p_err          ;

  //                                        lan interface

  var    logic    [               3 :0 ]    no_mii_txd                ;
  var    logic                              no_mii_tx_en              ;
  var    logic                              no_mii_tx_clk             ;

  var    logic    [               1 :0 ]    no_rmii_txd               ;
  var    logic                              no_rmii_tx_en             ;
  var    logic                              no_rmii_tx_clk            ;
  var    logic                              no_rmii_tx_clk_s          ;

  var    logic    [               7 :0 ]    no_gmii_txd               ;
  var    logic                              no_gmii_tx_en             ;
  var    logic                              no_gmii_tx_clk            ;

  //-------------------------------------// setting ------------------|

  //                                        update

  var    logic                              set_upd                   ;

  //                                        interface

  var    logic    [               1 :0 ]    set_io_type               ;
  var    logic    [               1 :0 ]    set_io_speed              ;
  var    logic                              set_io_pause              ;

  //                                        transmit speed

  var    logic                              set_tx_mode               ;
  var    logic    [              19 :0 ]    set_tx_speed              ;
  var    logic    [               1 :0 ]    set_tx_sel                ;
  
  //                                        crc error

  var    logic                              set_err_ena               ;
  var    logic    [               6 :0 ]    set_err_per               ;

  //                                        mac header

  var    logic                              set_mac_ena               ;
  var    logic    [               2 :0 ]    set_mda_type              ;
  var    logic    [              47 :0 ]    set_mda_const             ;
  var    logic    [              47 :0 ]    set_mda_min               ;
  var    logic    [              47 :0 ]    set_mda_max               ;
  var    logic    [               2 :0 ]    set_msa_type              ;
  var    logic    [              47 :0 ]    set_msa_const             ;
  var    logic    [              47 :0 ]    set_msa_min               ;
  var    logic    [              47 :0 ]    set_msa_max               ;
  var    logic    [              15 :0 ]    set_mlt_type              ;
  var    logic    [              47 :0 ]    set_mlt_const             ;
  var    logic    [              47 :0 ]    set_mlt_min               ;
  var    logic    [              47 :0 ]    set_mlt_max               ;

  //                                        ip header

  var    logic                              set_ip_ena                ;
  var    logic    [               2 :0 ]    set_ip_type               ;
  var    logic    [ lpW_LAN_IP   -1 :0 ]    set_ip_const              ;
  var    logic    [ lpW_LAN_IP   -1 :0 ]    set_ip_min                ;
  var    logic    [ lpW_LAN_IP   -1 :0 ]    set_ip_max                ;

  //                                        payload

  var    logic    [               2 :0 ]    set_pay_type              ;
  var    logic    [ pW_LAN_PAY   -1 :0 ]    set_pay_const             ;
  var    logic    [               7 :0 ]    set_pay_min               ;
  var    logic    [               7 :0 ]    set_pay_max               ;

  //                                        frame length

  var    logic    [               1 :0 ]    set_len_type              ;
  var    logic    [ pW_LAN_LEN   -1 :0 ]    set_len_const             ;
  var    logic    [ pW_LAN_LEN   -1 :0 ]    set_len_min               ;
  var    logic    [ pW_LAN_LEN   -1 :0 ]    set_len_max               ;

  //                                        frame gap

  var    logic    [               1 :0 ]    set_gap_type              ;
  var    logic    [ pW_LAN_GAP   -1 :0 ]    set_gap_const             ;
  var    logic    [ pW_LAN_GAP   -1 :0 ]    set_gap_min               ;
  var    logic    [ pW_LAN_GAP   -1 :0 ]    set_gap_max               ;

  //-------------------------------------// generator of ini ---------| [ emu_ini / tb_eth_emu_ini_v2 ]

  //                                        clear

  var    logic                              emu_ini__i_sclr_n         ;

  //                                        setting

  var    logic                              emu_ini__o_set_ini        ;
  var    logic                              emu_ini__i_set_upd        ;
  var    logic                              emu_ini__o_set_upd_ena    ;
  var    logic                              emu_ini__o_set_pause      ;
  var    logic                              emu_ini__i_set_pause      ;

  //                                        lan packet

  var    logic                              emu_ini__i_clk            ;
  var    logic                              emu_ini__i_cen            ;
  var    logic                              emu_ini__i_ced            ;
  var    logic    [               2 :0 ]    emu_ini__i_str_frame      ;
  var    logic    [               2 :0 ]    emu_ini__i_str_gap        ;

  //-------------------------------------// clocks generator ---------| [ emu_clk / tb_eth_emu_clk_v2 ]

  //                                        clear

  var    logic                              emu_clk__i_sclr_n         ;

  //                                        setting

  var    logic    [               1 :0 ]    emu_clk__i_set_type       ;
  var    logic    [               1 :0 ]    emu_clk__i_set_speed      ;

  //                                        mii clock

  var    logic                              emu_clk__o_mii_clk        ;
  var    logic                              emu_clk__o_mii_cen        ;
  var    logic                              emu_clk__o_mii_ced        ;

  //                                        rmii clock

  var    logic                              emu_clk__o_rmii_clk       ;
  var    logic                              emu_clk__o_rmii_cen       ;
  var    logic                              emu_clk__o_rmii_ced       ;

  //                                        gmii clock

  var    logic                              emu_clk__o_gmii_clk       ;
  var    logic                              emu_clk__o_gmii_cen       ;
  var    logic                              emu_clk__o_gmii_ced       ;

  //                                        custom clock

  var    logic                              emu_clk__o_cst_clk        ;
  var    logic                              emu_clk__o_cst_cen        ;
  var    logic                              emu_clk__o_cst_ced        ;

  //                                        active clock

  var    logic                              emu_clk__o_act_clk        ;
  var    logic                              emu_clk__o_act_cen        ;
  var    logic                              emu_clk__o_act_ced        ;

  //-------------------------------------// strobes generator --------| [ emu_str / tb_eth_emu_str_v2 ]

  //                                        clear

  var    logic                              emu_str__i_sclr_n         ;

  //                                        setting

  var    logic                              emu_str__i_set_ini        ;
  var    logic                              emu_str__i_set_io_pause   ;
  var    logic                              emu_str__i_set_tx_mode    ;
  var    logic    [ pW_LAN_GAP   -1 :0 ]    emu_str__i_set_tx_gap     ;
  var    logic    [               1 :0 ]    emu_str__i_set_ip_ena     ;
  var    logic                              emu_str__i_set_err_ena    ;
  var    logic    [               6 :0 ]    emu_str__i_set_err_per    ;
  var    logic                              emu_str__i_set_mac_ena    ;

  var    logic    [               1 :0 ]    emu_str__i_set_len_type   ;
  var    logic    [ pW_LAN_GAP   -1 :0 ]    emu_str__i_set_len_const  ;
  var    logic    [ pW_LAN_GAP   -1 :0 ]    emu_str__i_set_len_min    ;
  var    logic    [ pW_LAN_GAP   -1 :0 ]    emu_str__i_set_len_max    ;

  var    logic    [               1 :0 ]    emu_str__i_set_gap_type   ;
  var    logic    [ pW_LAN_GAP   -1 :0 ]    emu_str__i_set_gap_const  ;
  var    logic    [ pW_LAN_GAP   -1 :0 ]    emu_str__i_set_gap_min    ;
  var    logic    [ pW_LAN_GAP   -1 :0 ]    emu_str__i_set_gap_max    ;

  //                                        lan packet

  var    logic                              emu_str__i_clk            ;
  var    logic                              emu_str__i_cen            ;
  var    logic                              emu_str__i_ced            ;

  var    logic    [               2 :0 ]    emu_str__o_str_frame      ;
  var    logic    [ pW_LAN_LEN   -1 :0 ]    emu_str__o_cnt_frame      ;
  var    logic    [               2 :0 ]    emu_str__o_str_head       ;
  var    logic    [               3 :0 ]    emu_str__o_cnt_head       ;
  var    logic    [               2 :0 ]    emu_str__o_str_prm        ;
  var    logic    [               2 :0 ]    emu_str__o_str_sfd        ;
  var    logic    [               2 :0 ]    emu_str__o_str_body       ;
  var    logic    [ pW_LAN_LEN   -1 :0 ]    emu_str__o_cnt_body       ;
  var    logic    [               2 :0 ]    emu_str__o_str_mac        ;
  var    logic    [               3 :0 ]    emu_str__o_cnt_mac        ;
  var    logic    [               2 :0 ]    emu_str__o_str_mda        ;
  var    logic    [               2 :0 ]    emu_str__o_str_msa        ;
  var    logic    [               2 :0 ]    emu_str__o_str_mlt        ;
  var    logic    [               2 :0 ]    emu_str__o_str_ip         ;
  var    logic    [ lpW_LAN_IP_C -1 :0 ]    emu_str__o_cnt_ip         ;
  var    logic    [               2 :0 ]    emu_str__o_str_pay        ;
  var    logic    [ pW_LAN_LEN   -1 :0 ]    emu_str__o_cnt_pay        ;
  var    logic    [               2 :0 ]    emu_str__o_str_crc        ;
  var    logic    [               2 :0 ]    emu_str__o_cnt_crc        ;
  var    logic    [               2 :0 ]    emu_str__o_str_gap        ;
  var    logic    [ pW_LAN_GAP   -1 :0 ]    emu_str__o_cnt_gap        ;

  var    logic    [ pW_LAN_LEN   -1 :0 ]    emu_str__o_len_frame      ;
  var    logic    [ pW_LAN_LEN   -1 :0 ]    emu_str__o_len_body       ;
  var    logic    [ pW_LAN_LEN   -1 :0 ]    emu_str__o_len_pay        ;
  var    logic    [ pW_LAN_GAP   -1 :0 ]    emu_str__o_len_gap        ;

  var    logic                              emu_str__o_crc_rdy        ;
  var    logic                              emu_str__o_crc_err        ;

  //                                        lan counters
  var    logic    [ pN_LAN_CNT      :1 ]
                  [ pW_LAN_LEN   -1 :0 ]    emu_str__i_set_p_top      ;
  var    logic    [ pN_LAN_CNT      :1 ]
                  [ pW_LAN_LEN   -1 :0 ]    emu_str__i_set_p_low      ;

  var    logic                              emu_str__o_det_p_rdy      ;
  var    logic                              emu_str__o_det_p_err      ;

  var    logic                              emu_str__i_cnt_p_clr      ;
  var    logic                              emu_str__o_cnt_p_rdy      ;
  var    logic    [ pN_LAN_CNT      :0 ]
                  [ pW_LAN_CNT   -1 :0 ]    emu_str__o_cnt_p_all      ;
  var    logic    [ pN_LAN_CNT      :0 ]
                  [ pW_LAN_CNT   -1 :0 ]    emu_str__o_cnt_p_err      ;

  //-------------------------------------// data generator -----------| [ emu_dat / tb_eth_emu_dat_v2 ]

  //                                        clear

  var    logic                              emu_dat__i_sclr_n         ;

  //                                        setting

  var    logic                              emu_dat__i_set_err_ena    ;
  var    logic    [               6 :0 ]    emu_dat__i_set_err_per    ;

  var    logic                              emu_dat__i_set_mac_ena    ;
  var    logic    [               2 :0 ]    emu_dat__i_set_mda_type   ;
  var    logic    [              47 :0 ]    emu_dat__i_set_mda_const  ;
  var    logic    [              47 :0 ]    emu_dat__i_set_mda_min    ;
  var    logic    [              47 :0 ]    emu_dat__i_set_mda_max    ;
  var    logic    [               2 :0 ]    emu_dat__i_set_msa_type   ;
  var    logic    [              47 :0 ]    emu_dat__i_set_msa_const  ;
  var    logic    [              47 :0 ]    emu_dat__i_set_msa_min    ;
  var    logic    [              47 :0 ]    emu_dat__i_set_msa_max    ;
  var    logic    [              15 :0 ]    emu_dat__i_set_mlt_type   ;
  var    logic    [              47 :0 ]    emu_dat__i_set_mlt_const  ;
  var    logic    [              47 :0 ]    emu_dat__i_set_mlt_min    ;
  var    logic    [              47 :0 ]    emu_dat__i_set_mlt_max    ;

  var    logic                              emu_dat__i_set_ip_ena     ;
  var    logic    [               2 :0 ]    emu_dat__i_set_ip_type    ;
  var    logic    [ lpW_LAN_IP   -1 :0 ]    emu_dat__i_set_ip_const   ;
  var    logic    [ lpW_LAN_IP   -1 :0 ]    emu_dat__i_set_ip_min     ;
  var    logic    [ lpW_LAN_IP   -1 :0 ]    emu_dat__i_set_ip_max     ;

  var    logic    [               2 :0 ]    emu_dat__i_set_pay_type   ;
  var    logic    [ pW_LAN_PAY   -1 :0 ]    emu_dat__i_set_pay_const  ;
  var    logic    [               7 :0 ]    emu_dat__i_set_pay_min    ;
  var    logic    [               7 :0 ]    emu_dat__i_set_pay_max    ;

  //                                        lan packet

  var    logic                              emu_dat__i_clk            ;
  var    logic                              emu_dat__i_cen            ;
  var    logic                              emu_dat__i_ced            ;

  var    logic    [               2 :0 ]    emu_dat__i_str_frame      ;
  var    logic    [ pW_LAN_LEN   -1 :0 ]    emu_dat__i_cnt_frame      ;
  var    logic    [               2 :0 ]    emu_dat__i_str_head       ;
  var    logic    [               3 :0 ]    emu_dat__i_cnt_head       ;
  var    logic    [               2 :0 ]    emu_dat__i_str_prm        ;
  var    logic    [               2 :0 ]    emu_dat__i_str_sfd        ;
  var    logic    [               2 :0 ]    emu_dat__i_str_body       ;
  var    logic    [ pW_LAN_LEN   -1 :0 ]    emu_dat__i_cnt_body       ;
  var    logic    [               2 :0 ]    emu_dat__i_str_mac        ;
  var    logic    [               3 :0 ]    emu_dat__i_cnt_mac        ;
  var    logic    [               2 :0 ]    emu_dat__i_str_mda        ;
  var    logic    [               2 :0 ]    emu_dat__i_str_msa        ;
  var    logic    [               2 :0 ]    emu_dat__i_str_mlt        ;
  var    logic    [               2 :0 ]    emu_dat__i_str_ip         ;
  var    logic    [ lpW_LAN_IP_C -1 :0 ]    emu_dat__i_cnt_ip         ;
  var    logic    [               2 :0 ]    emu_dat__i_str_pay        ;
  var    logic    [ pW_LAN_LEN   -1 :0 ]    emu_dat__i_cnt_pay        ;
  var    logic    [               2 :0 ]    emu_dat__i_str_crc        ;
  var    logic    [               2 :0 ]    emu_dat__i_cnt_crc        ;
  var    logic    [               2 :0 ]    emu_dat__i_str_gap        ;
  var    logic    [ pW_LAN_GAP   -1 :0 ]    emu_dat__i_cnt_gap        ;

  var    logic                              emu_dat__o_dat_load       ;
  var    logic    [               7 :0 ]    emu_dat__o_dat_frame      ;

  var    logic    [ pW_LAN_LEN   -1 :0 ]    emu_dat__i_len_frame      ;
  var    logic    [ pW_LAN_LEN   -1 :0 ]    emu_dat__i_len_body       ;
  var    logic    [ pW_LAN_LEN   -1 :0 ]    emu_dat__i_len_pay        ;
  var    logic    [ pW_LAN_GAP   -1 :0 ]    emu_dat__i_len_gap        ;

  var    logic                              emu_dat__i_crc_rdy        ;
  var    logic                              emu_dat__i_crc_err        ;

  //-------------------------------------// lan interface ------------| [ emu_lan / tb_eth_emu_lan_v2 ]

  //                                        clear

  var    logic                              emu_lan__i_sclr_n         ;

  //                                        setting

  var    logic    [               1 :0 ]    emu_lan__i_set_type       ;
  var    logic    [               1 :0 ]    emu_lan__i_set_speed      ;

  //                                        lan packet

  var    logic                              emu_lan__i_clk            ;
  var    logic                              emu_lan__i_cen            ;
  var    logic                              emu_lan__i_ced            ;

  var    logic    [               2 :0 ]    emu_lan__i_str_frame      ;
  var    logic    [               7 :0 ]    emu_lan__i_dat_frame      ;

  //                                        lan interface

  var    logic    [               3 :0 ]    emu_lan__o_mii_txd        ;
  var    logic                              emu_lan__o_mii_tx_en      ;
  var    logic                              emu_lan__o_mii_tx_clk     ;

  var    logic    [               1 :0 ]    emu_lan__o_rmii_txd       ;
  var    logic                              emu_lan__o_rmii_tx_en     ;
  var    logic                              emu_lan__o_rmii_tx_clk    ;
  var    logic                              emu_lan__o_rmii_tx_clk_s  ;

  var    logic    [               7 :0 ]    emu_lan__o_gmii_txd       ;
  var    logic                              emu_lan__o_gmii_tx_en     ;
  var    logic                              emu_lan__o_gmii_tx_clk    ;

  //-------------------------------------// speed calculator ---------| [ emu_spd / tb_eth_emu_spd_v2 ]

  //                                        setting

  var    logic    [               1 :0 ]    emu_spd__i_set_io_type    ;
  var    logic    [               1 :0 ]    emu_spd__i_set_io_speed   ;

  var    logic                              emu_spd__i_set_tx_mode    ;
  var    logic    [              19 :0 ]    emu_spd__i_set_tx_speed   ;
  var    logic    [              19 :0 ]    emu_spd__o_set_tx_speed   ;
  var    logic    [              19 :0 ]    emu_spd__o_set_tx_limit   ;
  var    logic    [               1 :0 ]    emu_spd__i_set_tx_sel     ;

  var    logic    [               1 :0 ]    emu_spd__i_set_len_type   ;
  var    logic    [ pW_LAN_LEN   -1 :0 ]    emu_spd__i_set_len_const  ;
  var    logic    [ pW_LAN_LEN   -1 :0 ]    emu_spd__i_set_len_min    ;
  var    logic    [ pW_LAN_LEN   -1 :0 ]    emu_spd__i_set_len_max    ;

  //                                        lan packet

  var    logic    [ pW_LAN_LEN   -1 :0 ]    emu_spd__i_len_frame      ;
  var    logic    [ pW_LAN_GAP   -1 :0 ]    emu_spd__i_len_gap        ;
  var    logic    [ pW_LAN_GAP   -1 :0 ]    emu_spd__o_len_gap        ;

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  //==========================================================================================================================================================================================================================//
  // body of module
  //==========================================================================================================================================================================================================================//

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // out logic : output (after out reg)
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  // setting / update

  assign  o_set_upd_ena     = no_set_upd_ena      ;

  // setting / speed

  assign  o_set_tx_limit    = no_set_tx_limit     ;
  assign  o_set_tx_speed    = no_set_tx_speed     ;

  // lan packet

  assign  o_lan_clk         = no_lan_clk          ;
  assign  o_lan_cen         = ro_lan_cen          ;
  assign  o_lan_ced         = ro_lan_ced          ;

  assign  o_lan_str_frame   = ro_lan_str_frame    ;
  assign  o_lan_cnt_frame   = ro_lan_cnt_frame    ;
  assign  o_lan_str_head    = ro_lan_str_head     ;
  assign  o_lan_cnt_head    = ro_lan_cnt_head     ;
  assign  o_lan_str_prm     = ro_lan_str_prm      ;
  assign  o_lan_str_sfd     = ro_lan_str_sfd      ;
  assign  o_lan_str_body    = ro_lan_str_body     ;
  assign  o_lan_cnt_body    = ro_lan_cnt_body     ;
  assign  o_lan_str_mac     = ro_lan_str_mac      ;
  assign  o_lan_cnt_mac     = ro_lan_cnt_mac      ;
  assign  o_lan_str_mda     = ro_lan_str_mda      ;
  assign  o_lan_str_msa     = ro_lan_str_msa      ;
  assign  o_lan_str_mlt     = ro_lan_str_mlt      ;
  assign  o_lan_str_ip      = ro_lan_str_ip       ;
  assign  o_lan_cnt_ip      = ro_lan_cnt_ip       ;
  assign  o_lan_str_pay     = ro_lan_str_pay      ;
  assign  o_lan_cnt_pay     = ro_lan_cnt_pay      ;
  assign  o_lan_str_crc     = ro_lan_str_crc      ;
  assign  o_lan_cnt_crc     = ro_lan_cnt_crc      ;
  assign  o_lan_str_gap     = ro_lan_str_gap      ;
  assign  o_lan_cnt_gap     = ro_lan_cnt_gap      ;

  assign  o_lan_dat_load    = ro_lan_dat_load     ;
  assign  o_lan_dat_frame   = ro_lan_dat_frame    ;

  assign  o_lan_len_frame   = ro_lan_len_frame    ;
  assign  o_lan_len_body    = ro_lan_len_body     ;
  assign  o_lan_len_pay     = ro_lan_len_pay      ;
  assign  o_lan_len_gap     = ro_lan_len_gap      ;

  assign  o_lan_crc_rdy     = ro_lan_crc_rdy      ;
  assign  o_lan_crc_err     = ro_lan_crc_err      ;

  // lan counters

  assign  o_lan_det_p_rdy   = ro_lan_det_p_rdy    ;
  assign  o_lan_det_p_err   = ro_lan_det_p_err    ;

  assign  o_lan_cnt_p_rdy   = ro_lan_cnt_p_rdy    ;
  assign  o_lan_cnt_p_all   = ro_lan_cnt_p_all    ;
  assign  o_lan_cnt_p_err   = ro_lan_cnt_p_err    ;

  // lan interface

  assign  o_mii_txd         = ro_mii_txd          ;
  assign  o_mii_tx_en       = ro_mii_tx_en        ;
  assign  o_mii_tx_clk      = no_mii_tx_clk == 0  ; // invert

  assign  o_rmii_txd        = ro_rmii_txd         ;
  assign  o_rmii_tx_en      = ro_rmii_tx_en       ;
  assign  o_rmii_tx_clk     = no_rmii_tx_clk == 0 ; // invert
  assign  o_rmii_tx_clk_s   = ro_rmii_tx_clk_s    ;

  assign  o_gmii_txd        = ro_gmii_txd         ;
  assign  o_gmii_tx_en      = ro_gmii_tx_en       ;
  assign  o_gmii_tx_clk     = no_gmii_tx_clk == 0 ; // invert

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // out logic : next to output (before out reg)
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  // setting / update

  assign  no_set_upd_ena    = emu_ini__o_set_upd_ena   ;

  // setting / speed

  assign  no_set_tx_limit   = emu_spd__o_set_tx_limit  ;
  assign  no_set_tx_speed   = emu_spd__o_set_tx_speed  ;

  // lan packet

  assign  no_lan_clk        = emu_clk__o_act_clk       ;
  assign  no_lan_cen        = emu_clk__o_act_cen       ;
  assign  no_lan_ced        = emu_clk__o_act_ced       ;

  assign  no_lan_str_frame  = emu_str__o_str_frame     ;
  assign  no_lan_cnt_frame  = emu_str__o_cnt_frame     ;
  assign  no_lan_str_head   = emu_str__o_str_head      ;
  assign  no_lan_cnt_head   = emu_str__o_cnt_head      ;
  assign  no_lan_str_prm    = emu_str__o_str_prm       ;
  assign  no_lan_str_sfd    = emu_str__o_str_sfd       ;
  assign  no_lan_str_body   = emu_str__o_str_body      ;
  assign  no_lan_cnt_body   = emu_str__o_cnt_body      ;
  assign  no_lan_str_mac    = emu_str__o_str_mac       ;
  assign  no_lan_cnt_mac    = emu_str__o_cnt_mac       ;
  assign  no_lan_str_mda    = emu_str__o_str_mda       ;
  assign  no_lan_str_msa    = emu_str__o_str_msa       ;
  assign  no_lan_str_mlt    = emu_str__o_str_mlt       ;
  assign  no_lan_str_ip     = emu_str__o_str_ip        ;
  assign  no_lan_cnt_ip     = emu_str__o_cnt_ip        ;
  assign  no_lan_str_pay    = emu_str__o_str_pay       ;
  assign  no_lan_cnt_pay    = emu_str__o_cnt_pay       ;
  assign  no_lan_str_crc    = emu_str__o_str_crc       ;
  assign  no_lan_cnt_crc    = emu_str__o_cnt_crc       ;
  assign  no_lan_str_gap    = emu_str__o_str_gap       ;
  assign  no_lan_cnt_gap    = emu_str__o_cnt_gap       ;

  assign  no_lan_dat_load   = emu_dat__o_dat_load      ;
  assign  no_lan_dat_frame  = emu_dat__o_dat_frame     ;

  assign  no_lan_len_frame  = emu_str__o_len_frame     ;
  assign  no_lan_len_body   = emu_str__o_len_body      ;
  assign  no_lan_len_pay    = emu_str__o_len_pay       ;
  assign  no_lan_len_gap    = emu_str__o_len_gap       ;

  assign  no_lan_crc_rdy    = emu_str__o_crc_rdy       ;
  assign  no_lan_crc_err    = emu_str__o_crc_err       ;

  // lan counters

  assign  no_lan_det_p_rdy  = emu_str__o_det_p_rdy     ;
  assign  no_lan_det_p_err  = emu_str__o_det_p_err     ;

  assign  no_lan_cnt_p_rdy  = emu_str__o_cnt_p_rdy     ;
  assign  no_lan_cnt_p_all  = emu_str__o_cnt_p_all     ;
  assign  no_lan_cnt_p_err  = emu_str__o_cnt_p_err     ;

  // lan interface

  assign  no_mii_txd        = emu_lan__o_mii_txd       ;
  assign  no_mii_tx_en      = emu_lan__o_mii_tx_en     ;
  assign  no_mii_tx_clk     = emu_lan__o_mii_tx_clk    ;

  assign  no_rmii_txd       = emu_lan__o_rmii_txd      ;
  assign  no_rmii_tx_en     = emu_lan__o_rmii_tx_en    ;
  assign  no_rmii_tx_clk    = emu_lan__o_rmii_tx_clk   ;
  assign  no_rmii_tx_clk_s  = emu_lan__o_rmii_tx_clk_s ;

  assign  no_gmii_txd       = emu_lan__o_gmii_txd      ;
  assign  no_gmii_tx_en     = emu_lan__o_gmii_tx_en    ;
  assign  no_gmii_tx_clk    = emu_lan__o_gmii_tx_clk   ;

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // setting (fool-tolerance)
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  // update ----------------------------------------------------------------------------------|

  assign  set_upd        =                                                    i_set_upd       ;
  
  // interface -------------------------------------------------------------------------------|

  assign  set_io_type    = ( i_set_io_type    > 2           ) ? 0           : i_set_io_type   ;
  assign  set_io_speed   = ( i_set_io_speed   > 2           ) ? 0           : i_set_io_speed  ;
  assign  set_io_pause   =                                                    i_set_io_pause  ;

  // transmit speed --------------------------------------------------------------------------|

  assign  set_tx_mode    =                                                    i_set_tx_mode   ;
  assign  set_tx_speed   = ( i_set_tx_speed   > 1000000     ) ? 1000000     : i_set_tx_speed  ;
  assign  set_tx_sel     = ( i_set_tx_sel     > 3           ) ? 0           : i_set_tx_sel    ;

  // crc error -------------------------------------------------------------------------------|

  assign  set_err_ena    =                                                    i_set_err_ena   ;
  assign  set_err_per    = ( i_set_err_per    > 100         ) ? 100         : i_set_err_per   ;

  // mac header ------------------------------------------------------------------------------|

  assign  set_mac_ena    =                                                    i_set_mac_ena   ;

  // mac header / mda ------------------------------------------------------------------------|

  assign  set_mda_type   = ( i_set_mda_type   > 4           ) ? 0           : i_set_mda_type  ;
  assign  set_mda_const  =                                                    i_set_mda_const ;
  assign  set_mda_min    =                                                    i_set_mda_min   ;
  assign  set_mda_max    = ( i_set_mda_max   <= set_mda_min ) ? set_mda_min : i_set_mda_max   ;

  // mac header / msa ------------------------------------------------------------------------|

  assign  set_msa_type   = ( i_set_msa_type   > 4           ) ? 0           : i_set_msa_type  ;
  assign  set_msa_const  =                                                    i_set_msa_const ;
  assign  set_msa_min    =                                                    i_set_msa_min   ;
  assign  set_msa_max    = ( i_set_msa_max   <= set_msa_min ) ? set_msa_min : i_set_msa_max   ;

  // mac header / mlt ------------------------------------------------------------------------|

  assign  set_mlt_type   = ( i_set_mlt_type   > 5           ) ? 5           : i_set_mlt_type  ;
  assign  set_mlt_const  =                                                    i_set_mlt_const ;
  assign  set_mlt_min    =                                                    i_set_mlt_min   ;
  assign  set_mlt_max    = ( i_set_mlt_max   <= set_mlt_min ) ? set_mlt_min : i_set_mlt_max   ;

  // ip header -------------------------------------------------------------------------------|

  assign  set_ip_ena     =                                                    i_set_ip_ena    ;
  assign  set_ip_type    = ( i_set_ip_type    > 4           ) ? 0           : i_set_ip_type   ;
  assign  set_ip_const   =                                                    i_set_ip_const  ;
  assign  set_ip_min     =                                                    i_set_ip_min    ;
  assign  set_ip_max     = ( i_set_ip_max    <= set_ip_min  ) ? set_ip_min  : i_set_ip_max    ;

  // payload ---------------------------------------------------------------------------------|

  assign  set_pay_type   = ( i_set_pay_type   > 4           ) ? 0           : i_set_pay_type  ;
  assign  set_pay_const  =                                                    i_set_pay_const ;
  assign  set_pay_min    =                                                    i_set_pay_min   ;
  assign  set_pay_max    = ( i_set_pay_max   <= set_pay_min ) ? set_pay_min : i_set_pay_max   ;

  // frame length ----------------------------------------------------------------------------|

  assign  set_len_type   = ( i_set_len_type   > 2           ) ? 0           : i_set_len_type  ;
  assign  set_len_const  = ( i_set_len_const <= 72          ) ? 72          : i_set_len_const ;
  assign  set_len_min    = ( i_set_len_min   <= 72          ) ? 72          : i_set_len_min   ;
  assign  set_len_max    = ( i_set_len_max   <= 72          ) ? 72          : 
                           ( i_set_len_max   <= set_len_min ) ? set_len_min : i_set_len_max   ;

  // frame gap -------------------------------------------------------------------------------|

  assign  set_gap_type   = ( i_set_gap_type   > 2           ) ? 0           : i_set_gap_type  ;
  assign  set_gap_const  = ( i_set_gap_const == 0           ) ? 1           : i_set_gap_const ;
  assign  set_gap_min    = ( i_set_gap_min   == 0           ) ? 1           : i_set_gap_min   ;
  assign  set_gap_max    = ( i_set_gap_max   == 0           ) ? 1           : 
                           ( i_set_gap_max   <= set_gap_min ) ? set_gap_min : i_set_gap_max   ;

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // generator of initialization signal (for settings)  [ emu_ini / tb_eth_emu_ini_v2 ]
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  // clear

  assign  emu_ini__i_sclr_n     = ri_reset_n           ;

  // setting

  assign  emu_ini__i_set_upd    = set_upd              ;
  assign  emu_ini__i_set_pause  = set_io_pause         ;

  // lan packet

  assign  emu_ini__i_clk        = emu_clk__o_act_clk   ;
  assign  emu_ini__i_cen        = emu_clk__o_act_cen   ;
  assign  emu_ini__i_ced        = emu_clk__o_act_ced   ;

  assign  emu_ini__i_str_frame  = emu_str__o_str_frame ;
  assign  emu_ini__i_str_gap    = emu_str__o_str_gap   ;

  //----------------------------------------------------------------------------------|
                                                      //                              |
  tb_eth_emu_ini_v2    emu_ini                        //                              |
  (                                                   //                              |
  // clear -------------------------------------------//------------------------------|
                                                      //           |                  |
     .i_sclr_n         ( emu_ini__i_sclr_n      ),    //  in       |       opt        |
                                                      //           |                  |
  // setting -----------------------------------------//-----------|------------------|
                                                      //           |                  |
     .o_set_ini        ( emu_ini__o_set_ini     ),    //      out  |  req             |
                                                      //           |                  |
  // update            update                         //-----------|------------------|
                                                      //           |                  |
     .i_set_upd        ( emu_ini__i_set_upd     ),    //  in       |       opt        |
     .o_set_upd_ena    ( emu_ini__o_set_upd_ena ),    //      out  |       opt        |
                                                      //           |                  |
  // pause             pause                          //-----------|------------------|
                                                      //           |                  |
     .o_set_pause      ( emu_ini__o_set_pause   ),    //      out  |  req             |
     .i_set_pause      ( emu_ini__i_set_pause   ),    //  in       |       opt        |
                                                      //           |                  |
  // lan packet --------------------------------------//-----------|------------------|
                                                      //           |                  |
  // clock             clock                          //-----------|------------------|
                                                      //           |                  |
     .i_clk            ( emu_ini__i_clk         ),    //  in       |  req             |
     .i_cen            ( emu_ini__i_cen         ),    //  in       |  req             |
     .i_ced            ( emu_ini__i_ced         ),    //  in       |  req             |
                                                      //           |                  |
  // strob             strob                          //-----------|------------------|
                                                      //           |                  |
     .i_str_frame      ( emu_ini__i_str_frame   ),    //  in       |  req             |
     .i_str_gap        ( emu_ini__i_str_gap     )     //  in       |  req             |
                                                      //           |                  |
  );//--------------------------------------------------------------------------------|

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // clocks generator of ethernet frames  [ emu_clk / tb_eth_emu_clk_v2 ]
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  // clear

  assign  emu_clk__i_sclr_n     = ri_reset_n   ;

  // setting / interface

  assign  emu_clk__i_set_type   = set_io_type  ;
  assign  emu_clk__i_set_speed  = set_io_speed ;
  
  //--------------------------------------------------------------------------------|
                                                    //                              |
  tb_eth_emu_clk_v2    emu_clk                      //                              |
  (                                                 //                              |
  // clear -----------------------------------------//------------------------------|
                                                    //           |                  |
     .i_sclr_n         ( emu_clk__i_sclr_n    ),    //  in       |       opt        |
                                                    //           |                  |
  // setting ---------------------------------------//-----------|------------------|
                                                    //           |                  |
     .i_set_type       ( emu_clk__i_set_type  ),    //  in       |  req             |
     .i_set_speed      ( emu_clk__i_set_speed ),    //  in       |  req             |
                                                    //           |                  |
  // mii clock -------------------------------------//-----------|------------------|
                                                    //           |                  |
     .o_mii_clk        ( emu_clk__o_mii_clk   ),    //      out  |       opt        |
     .o_mii_cen        ( emu_clk__o_mii_cen   ),    //      out  |       opt        |
     .o_mii_ced        ( emu_clk__o_mii_ced   ),    //      out  |       opt        |
                                                    //           |                  |
  // rmii clock ------------------------------------//-----------|------------------|
                                                    //           |                  |
     .o_rmii_clk       ( emu_clk__o_rmii_clk  ),    //      out  |       opt        |
     .o_rmii_cen       ( emu_clk__o_rmii_cen  ),    //      out  |       opt        |
     .o_rmii_ced       ( emu_clk__o_rmii_ced  ),    //      out  |       opt        |
                                                    //           |                  |
  // gmii clock ------------------------------------//-----------|------------------|
                                                    //           |                  |
     .o_gmii_clk       ( emu_clk__o_gmii_clk  ),    //      out  |       opt        |
     .o_gmii_cen       ( emu_clk__o_gmii_cen  ),    //      out  |       opt        |
     .o_gmii_ced       ( emu_clk__o_gmii_ced  ),    //      out  |       opt        |
                                                    //           |                  |
  // custom clock ----------------------------------//-----------|------------------|
                                                    //           |                  |
     .o_cst_clk        ( emu_clk__o_cst_clk   ),    //      out  |       opt        |
     .o_cst_cen        ( emu_clk__o_cst_cen   ),    //      out  |       opt        |
     .o_cst_ced        ( emu_clk__o_cst_ced   ),    //      out  |       opt        |
                                                    //           |                  |
  // active clock ----------------------------------//-----------|------------------|
                                                    //           |                  |
     .o_act_clk        ( emu_clk__o_act_clk   ),    //      out  |  req             |
     .o_act_cen        ( emu_clk__o_act_cen   ),    //      out  |  req             |
     .o_act_ced        ( emu_clk__o_act_ced   )     //      out  |  req             |
                                                    //           |                  |
  );//------------------------------------------------------------------------------|

  // custom mode (clock)

  defparam  emu_clk.pE_CST_CLK  = pE_CST_CLK ;
  defparam  emu_clk.pT_CST_CLK  = pT_CST_CLK ;
  defparam  emu_clk.pT_CST_CEN  = pT_CST_CEN ;
  defparam  emu_clk.pT_CST_CED  = pT_CST_CED ;

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // strobes generator of ethernet frames (auto generated)  [ emu_str / tb_eth_emu_str_v2 ]
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  // clear

  assign  emu_str__i_sclr_n         = ri_reset_n           ;

  // setting / initialization

  assign  emu_str__i_set_ini        = emu_ini__o_set_ini   ;

  // setting / interface

  assign  emu_str__i_set_io_pause   = emu_ini__o_set_pause ;

  // setting / transmit speed

  assign  emu_str__i_set_tx_mode    = set_tx_mode          ;
  assign  emu_str__i_set_tx_gap     = emu_spd__o_len_gap   ;

  // setting / error

  assign  emu_str__i_set_err_ena    = set_err_ena          ;
  assign  emu_str__i_set_err_per    = set_err_per          ;

  // setting / mac & ip headers

  assign  emu_str__i_set_mac_ena    = set_mac_ena          ;
  assign  emu_str__i_set_ip_ena     = set_ip_ena           ;

  // setting / frame length

  assign  emu_str__i_set_len_type   = set_len_type         ;
  assign  emu_str__i_set_len_const  = set_len_const        ;
  assign  emu_str__i_set_len_min    = set_len_min          ;
  assign  emu_str__i_set_len_max    = set_len_max          ;

  // setting / frame gap

  assign  emu_str__i_set_gap_type   = set_gap_type         ;
  assign  emu_str__i_set_gap_const  = set_gap_const        ;
  assign  emu_str__i_set_gap_min    = set_gap_min          ;
  assign  emu_str__i_set_gap_max    = set_gap_max          ;

  // lan packet / clock

  assign  emu_str__i_clk            = emu_clk__o_act_clk   ;
  assign  emu_str__i_cen            = emu_clk__o_act_cen   ;
  assign  emu_str__i_ced            = emu_clk__o_act_ced   ;

  // lan counters / setting

  assign  emu_str__i_set_p_top      = i_lan_set_p_top      ;
  assign  emu_str__i_set_p_low      = i_lan_set_p_low      ;

  // lan counters / clear

  assign  emu_str__i_cnt_p_clr      = i_lan_cnt_p_clr      ;

  //--------------------------------------------------------------------------------------|
                                                          //                              |
  tb_eth_emu_str_v2      emu_str                          //                              |
  (                                                       //                              |
  // clear -----------------------------------------------//------------------------------|
                                                          //           |                  |
     .i_sclr_n           ( emu_str__i_sclr_n        ),    //  in       |       opt        |
                                                          //           |                  |
  // setting ---------------------------------------------//-----------|------------------|
                                                          //           |                  |
     .i_set_ini          ( emu_str__i_set_ini       ),    //  in       |  req             |
                                                          //           |                  |
  // interface           interface                        //-----------|------------------|
                                                          //           |                  |
     .i_set_io_pause     ( emu_str__i_set_io_pause  ),    //  in       |       opt        |
                                                          //           |                  |
  // transmit speed      transmit speed                   //-----------|------------------|
                                                          //           |                  |
     .i_set_tx_mode      ( emu_str__i_set_tx_mode   ),    //  in       |       opt        |
     .i_set_tx_gap       ( emu_str__i_set_tx_gap    ),    //  in       |       opt        |
                                                          //           |                  |
  // crc error           crc error                        //-----------|------------------|
                                                          //           |                  |
     .i_set_err_ena      ( emu_str__i_set_err_ena   ),    //  in       |       opt        |
     .i_set_err_per      ( emu_str__i_set_err_per   ),    //  in       |       opt        |
                                                          //           |                  |
  // mac header          mac header                       //-----------|------------------|
                                                          //           |                  |
     .i_set_mac_ena      ( emu_str__i_set_mac_ena   ),    //  in       |       opt        |
                                                          //           |                  |
  // ip header           ip header                        //-----------|------------------|
                                                          //           |                  |
     .i_set_ip_ena       ( emu_str__i_set_ip_ena    ),    //  in       |       opt        |
                                                          //           |                  |
  // frame length        frame length                     //-----------|------------------|
                                                          //           |                  |
     .i_set_len_type     ( emu_str__i_set_len_type  ),    //  in       |  req             |
     .i_set_len_const    ( emu_str__i_set_len_const ),    //  in       |  req             |
     .i_set_len_min      ( emu_str__i_set_len_min   ),    //  in       |       opt        |
     .i_set_len_max      ( emu_str__i_set_len_max   ),    //  in       |       opt        |
                                                          //           |                  |
  // frame gap           frame gap                        //-----------|------------------|
                                                          //           |                  |
     .i_set_gap_type     ( emu_str__i_set_gap_type  ),    //  in       |  req             |
     .i_set_gap_const    ( emu_str__i_set_gap_const ),    //  in       |  req             |
     .i_set_gap_min      ( emu_str__i_set_gap_min   ),    //  in       |       opt        |
     .i_set_gap_max      ( emu_str__i_set_gap_max   ),    //  in       |       opt        |
                                                          //           |                  |
  // lan packet ------------------------------------------//-----------|------------------|
                                                          //           |                  |
  // clock               clock                            //-----------|------------------|
                                                          //           |                  |
     .i_clk              ( emu_str__i_clk           ),    //  in       |  req             |
     .i_cen              ( emu_str__i_cen           ),    //  in       |  req             |
     .i_ced              ( emu_str__i_ced           ),    //  in       |  req             |
                                                          //           |                  |
  // strob               strob                            //-----------|------------------|
                                                          //           |                  |
     .o_str_frame        ( emu_str__o_str_frame     ),    //      out  |  req             |
     .o_cnt_frame        ( emu_str__o_cnt_frame     ),    //      out  |       opt        |
     .o_str_head         ( emu_str__o_str_head      ),    //      out  |       opt        |
     .o_cnt_head         ( emu_str__o_cnt_head      ),    //      out  |       opt        |
     .o_str_prm          ( emu_str__o_str_prm       ),    //      out  |       opt        |
     .o_str_sfd          ( emu_str__o_str_sfd       ),    //      out  |       opt        |
     .o_str_body         ( emu_str__o_str_body      ),    //      out  |       opt        |
     .o_cnt_body         ( emu_str__o_cnt_body      ),    //      out  |       opt        |
     .o_str_mac          ( emu_str__o_str_mac       ),    //      out  |       opt        |
     .o_cnt_mac          ( emu_str__o_cnt_mac       ),    //      out  |       opt        |
     .o_str_mda          ( emu_str__o_str_mda       ),    //      out  |       opt        |
     .o_str_msa          ( emu_str__o_str_msa       ),    //      out  |       opt        |
     .o_str_mlt          ( emu_str__o_str_mlt       ),    //      out  |       opt        |
     .o_str_ip           ( emu_str__o_str_ip        ),    //      out  |       opt        |
     .o_cnt_ip           ( emu_str__o_cnt_ip        ),    //      out  |       opt        |
     .o_str_pay          ( emu_str__o_str_pay       ),    //      out  |       opt        |
     .o_cnt_pay          ( emu_str__o_cnt_pay       ),    //      out  |       opt        |
     .o_str_crc          ( emu_str__o_str_crc       ),    //      out  |       opt        |
     .o_cnt_crc          ( emu_str__o_cnt_crc       ),    //      out  |       opt        |
     .o_str_gap          ( emu_str__o_str_gap       ),    //      out  |       opt        |
     .o_cnt_gap          ( emu_str__o_cnt_gap       ),    //      out  |       opt        |
                                                          //           |                  |
  // length              length                           //-----------|------------------|
                                                          //           |                  |
     .o_len_frame        ( emu_str__o_len_frame     ),    //      out  |  req             |
     .o_len_body         ( emu_str__o_len_body      ),    //      out  |       opt        |
     .o_len_pay          ( emu_str__o_len_pay       ),    //      out  |       opt        |
     .o_len_gap          ( emu_str__o_len_gap       ),    //      out  |       opt        |
                                                          //           |                  |
  // fcs crc             fcs crc                          //-----------|------------------|
                                                          //           |                  |
     .o_crc_rdy          ( emu_str__o_crc_rdy       ),    //      out  |  req             |
     .o_crc_err          ( emu_str__o_crc_err       ),    //      out  |  req             |
                                                          //           |                  |
  // lan counters ----------------------------------------//-----------|------------------|
                                                          //           |                  |
  // setting             setting                          //-----------|------------------|
                                                          //           |                  |
     .i_set_p_top        ( emu_str__i_set_p_top     ),    //  in       |       opt        |
     .i_set_p_low        ( emu_str__i_set_p_low     ),    //  in       |       opt        |
                                                          //           |                  |
  // detector            detector                         //-----------|------------------|
                                                          //           |                  |
     .o_det_p_rdy        ( emu_str__o_det_p_rdy     ),    //      out  |       opt        |
     .o_det_p_err        ( emu_str__o_det_p_err     ),    //      out  |       opt        |
                                                          //           |                  |
  // counters            counters                         //-----------|------------------|
                                                          //           |                  |
     .i_cnt_p_clr        ( emu_str__i_cnt_p_clr     ),    //  in       |       opt        |
     .o_cnt_p_rdy        ( emu_str__o_cnt_p_rdy     ),    //      out  |       opt        |
     .o_cnt_p_all        ( emu_str__o_cnt_p_all     ),    //      out  |       opt        |
     .o_cnt_p_err        ( emu_str__o_cnt_p_err     )     //      out  |       opt        |
                                                          //           |                  |
  );//------------------------------------------------------------------------------------|

  // packet structure / frame header

  defparam  emu_str.pE_LAN_HEAD  = pE_LAN_HEAD ;

  // packet structure / frame length

  defparam  emu_str.pW_LAN_LEN   = pW_LAN_LEN  ;
  defparam  emu_str.pW_LAN_GAP   = pW_LAN_GAP  ;

  // packet structure / ip header

  defparam  emu_str.pN_LAN_IPL   = lpN_LAN_IPL ;
  defparam  emu_str.pN_LAN_IPH   = lpN_LAN_IPH ;

  // packet counters

  defparam  emu_str.pN_LAN_CNT   = pN_LAN_CNT  ;
  defparam  emu_str.pW_LAN_CNT   = pW_LAN_CNT  ;

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // data stream generator of ethernet frames  [ emu_dat / tb_eth_emu_dat_v2 ]
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  // clear

  assign  emu_dat__i_sclr_n         = ri_reset_n            ;

  // setting / initialization

  assign  emu_dat__i_set_ini        = emu_ini__o_set_ini    ;

  // setting / error

  assign  emu_dat__i_set_err_ena    = set_err_ena           ;
  assign  emu_dat__i_set_err_per    = set_err_per           ;

  // setting / mac header

  assign  emu_dat__i_set_mac_ena    = set_mac_ena           ;
  assign  emu_dat__i_set_mda_type   = set_mda_type          ;
  assign  emu_dat__i_set_mda_const  = set_mda_const         ;
  assign  emu_dat__i_set_mda_min    = set_mda_min           ;
  assign  emu_dat__i_set_mda_max    = set_mda_max           ;
  assign  emu_dat__i_set_msa_type   = set_msa_type          ;
  assign  emu_dat__i_set_msa_const  = set_msa_const         ;
  assign  emu_dat__i_set_msa_min    = set_msa_min           ;
  assign  emu_dat__i_set_msa_max    = set_msa_max           ;
  assign  emu_dat__i_set_mlt_type   = set_mlt_type          ;
  assign  emu_dat__i_set_mlt_const  = set_mlt_const         ;
  assign  emu_dat__i_set_mlt_min    = set_mlt_min           ;
  assign  emu_dat__i_set_mlt_max    = set_mlt_max           ;

  // setting / ip header

  assign  emu_dat__i_set_ip_ena     = set_ip_ena            ;
  assign  emu_dat__i_set_ip_type    = set_ip_type           ;
  assign  emu_dat__i_set_ip_const   = set_ip_const          ;
  assign  emu_dat__i_set_ip_min     = set_ip_min            ;
  assign  emu_dat__i_set_ip_max     = set_ip_max            ;

  // setting / payload

  assign  emu_dat__i_set_pay_type   = set_pay_type          ;
  assign  emu_dat__i_set_pay_const  = set_pay_const         ;
  assign  emu_dat__i_set_pay_min    = set_pay_min           ;
  assign  emu_dat__i_set_pay_max    = set_pay_max           ;

  // lan packet / clock

  assign  emu_dat__i_clk            = emu_clk__o_act_clk    ;
  assign  emu_dat__i_cen            = emu_clk__o_act_cen    ;
  assign  emu_dat__i_ced            = emu_clk__o_act_ced    ;

  // lan packet / strob

  assign  emu_dat__i_str_frame      = emu_str__o_str_frame  ;
  assign  emu_dat__i_cnt_frame      = emu_str__o_cnt_frame  ;
  assign  emu_dat__i_str_head       = emu_str__o_str_head   ;
  assign  emu_dat__i_cnt_head       = emu_str__o_cnt_head   ;
  assign  emu_dat__i_str_prm        = emu_str__o_str_prm    ;
  assign  emu_dat__i_str_sfd        = emu_str__o_str_sfd    ;
  assign  emu_dat__i_str_body       = emu_str__o_str_body   ;
  assign  emu_dat__i_cnt_body       = emu_str__o_cnt_body   ;
  assign  emu_dat__i_str_mac        = emu_str__o_str_mac    ;
  assign  emu_dat__i_cnt_mac        = emu_str__o_cnt_mac    ;
  assign  emu_dat__i_str_mda        = emu_str__o_str_mda    ;
  assign  emu_dat__i_str_msa        = emu_str__o_str_msa    ;
  assign  emu_dat__i_str_mlt        = emu_str__o_str_mlt    ;
  assign  emu_dat__i_str_ip         = emu_str__o_str_ip     ;
  assign  emu_dat__i_cnt_ip         = emu_str__o_cnt_ip     ;
  assign  emu_dat__i_str_pay        = emu_str__o_str_pay    ;
  assign  emu_dat__i_cnt_pay        = emu_str__o_cnt_pay    ;
  assign  emu_dat__i_str_crc        = emu_str__o_str_crc    ;
  assign  emu_dat__i_cnt_crc        = emu_str__o_cnt_crc    ;
  assign  emu_dat__i_str_gap        = emu_str__o_str_gap    ;
  assign  emu_dat__i_cnt_gap        = emu_str__o_cnt_gap    ;

  // lan packet / length

  assign  emu_dat__i_len_frame      = emu_str__o_len_frame  ;
  assign  emu_dat__i_len_body       = emu_str__o_len_body   ;
  assign  emu_dat__i_len_pay        = emu_str__o_len_pay    ;
  assign  emu_dat__i_len_gap        = emu_str__o_len_gap    ;

  // lan packet / fcs crc

  assign  emu_dat__i_crc_rdy        = emu_str__o_crc_rdy    ;
  assign  emu_dat__i_crc_err        = emu_str__o_crc_err    ;

  //--------------------------------------------------------------------------------------|
                                                          //                              |
  tb_eth_emu_dat_v2      emu_dat                          //                              |
  (                                                       //                              |
  // clear -----------------------------------------------//------------------------------|
                                                          //           |                  |
     .i_sclr_n           ( emu_dat__i_sclr_n        ),    //  in       |       opt        |
                                                          //           |                  |
  // setting ---------------------------------------------//-----------|------------------|
                                                          //           |                  |
     .i_set_ini          ( emu_dat__i_set_ini       ),    //  in       |  req             |
                                                          //           |                  |
  // mac header          mac header                       //-----------|------------------|
                                                          //           |                  |
     .i_set_mac_ena      ( emu_dat__i_set_mac_ena   ),    //  in       |       opt        |
                                                          //           |                  |
     .i_set_mda_type     ( emu_dat__i_set_mda_type  ),    //  in       |       opt        |
     .i_set_mda_const    ( emu_dat__i_set_mda_const ),    //  in       |       opt        |
     .i_set_mda_min      ( emu_dat__i_set_mda_min   ),    //  in       |       opt        |
     .i_set_mda_max      ( emu_dat__i_set_mda_max   ),    //  in       |       opt        |
                                                          //           |                  |
     .i_set_msa_type     ( emu_dat__i_set_msa_type  ),    //  in       |       opt        |
     .i_set_msa_const    ( emu_dat__i_set_msa_const ),    //  in       |       opt        |
     .i_set_msa_min      ( emu_dat__i_set_msa_min   ),    //  in       |       opt        |
     .i_set_msa_max      ( emu_dat__i_set_msa_max   ),    //  in       |       opt        |
                                                          //           |                  |
     .i_set_mlt_type     ( emu_dat__i_set_mlt_type  ),    //  in       |       opt        |
     .i_set_mlt_const    ( emu_dat__i_set_mlt_const ),    //  in       |       opt        |
     .i_set_mlt_min      ( emu_dat__i_set_mlt_min   ),    //  in       |       opt        |
     .i_set_mlt_max      ( emu_dat__i_set_mlt_max   ),    //  in       |       opt        |
                                                          //           |                  |
  // ip header           ip header                        //-----------|------------------|
                                                          //           |                  |
     .i_set_ip_ena       ( emu_dat__i_set_ip_ena    ),    //  in       |       opt        |
     .i_set_ip_type      ( emu_dat__i_set_ip_type   ),    //  in       |       opt        |
     .i_set_ip_const     ( emu_dat__i_set_ip_const  ),    //  in       |       opt        |
     .i_set_ip_min       ( emu_dat__i_set_ip_min    ),    //  in       |       opt        |
     .i_set_ip_max       ( emu_dat__i_set_ip_max    ),    //  in       |       opt        |
                                                          //           |                  |
  // payload             payload                          //-----------|------------------|
                                                          //           |                  |
     .i_set_pay_type     ( emu_dat__i_set_pay_type  ),    //  in       |  req             |
     .i_set_pay_const    ( emu_dat__i_set_pay_const ),    //  in       |  req             |
     .i_set_pay_min      ( emu_dat__i_set_pay_min   ),    //  in       |       opt        |
     .i_set_pay_max      ( emu_dat__i_set_pay_max   ),    //  in       |       opt        |
                                                          //           |                  |
  // lan packet ------------------------------------------//-----------|------------------|
                                                          //           |                  |
  // clock               clock                            //-----------|------------------|
                                                          //           |                  |
     .i_clk              ( emu_dat__i_clk           ),    //  in       |  req             |
     .i_cen              ( emu_dat__i_cen           ),    //  in       |  req             |
     .i_ced              ( emu_dat__i_ced           ),    //  in       |  req             |
                                                          //           |                  |
  // strob               strob                            //-----------|------------------|
                                                          //           |                  |
     .i_str_frame        ( emu_dat__i_str_frame     ),    //  in       |  req             |
     .i_cnt_frame        ( emu_dat__i_cnt_frame     ),    //  in       |            test  |
     .i_str_head         ( emu_dat__i_str_head      ),    //  in       |  req             |
     .i_cnt_head         ( emu_dat__i_cnt_head      ),    //  in       |            test  |
     .i_str_prm          ( emu_dat__i_str_prm       ),    //  in       |  req             |
     .i_str_sfd          ( emu_dat__i_str_sfd       ),    //  in       |  req             |
     .i_str_body         ( emu_dat__i_str_body      ),    //  in       |            test  |
     .i_cnt_body         ( emu_dat__i_cnt_body      ),    //  in       |            test  |
     .i_str_mac          ( emu_dat__i_str_mac       ),    //  in       |       opt        |
     .i_cnt_mac          ( emu_dat__i_cnt_mac       ),    //  in       |            test  |
     .i_str_mda          ( emu_dat__i_str_mda       ),    //  in       |       opt        |
     .i_str_msa          ( emu_dat__i_str_msa       ),    //  in       |       opt        |
     .i_str_mlt          ( emu_dat__i_str_mlt       ),    //  in       |       opt        |
     .i_str_ip           ( emu_dat__i_str_ip        ),    //  in       |       opt        |
     .i_cnt_ip           ( emu_dat__i_cnt_ip        ),    //  in       |            test  |
     .i_str_pay          ( emu_dat__i_str_pay       ),    //  in       |  req             |
     .i_cnt_pay          ( emu_dat__i_cnt_pay       ),    //  in       |            test  |
     .i_str_crc          ( emu_dat__i_str_crc       ),    //  in       |  req             |
     .i_cnt_crc          ( emu_dat__i_cnt_crc       ),    //  in       |  req             |
     .i_str_gap          ( emu_dat__i_str_gap       ),    //  in       |            test  |
     .i_cnt_gap          ( emu_dat__i_cnt_gap       ),    //  in       |            test  |
                                                          //           |                  |
  // data                data                             //-----------|------------------|
                                                          //           |                  |
     .o_dat_load         ( emu_dat__o_dat_load      ),    //      out  |  req             |
     .o_dat_frame        ( emu_dat__o_dat_frame     ),    //      out  |  req             |
                                                          //           |                  |
  // length              length                           //-----------|------------------|
                                                          //           |                  |
     .i_len_frame        ( emu_dat__i_len_frame     ),    //  in       |            test  |
     .i_len_body         ( emu_dat__i_len_body      ),    //  in       |            test  |
     .i_len_pay          ( emu_dat__i_len_pay       ),    //  in       |  req             |
     .i_len_gap          ( emu_dat__i_len_gap       ),    //  in       |            test  |
                                                          //           |                  |
  // fcs crc             fcs crc                          //-----------|------------------|
                                                          //           |                  |
     .i_crc_rdy          ( emu_dat__i_crc_rdy       ),    //  in       |       opt        |
     .i_crc_err          ( emu_dat__i_crc_err       )     //  in       |       opt        |
                                                          //           |                  |
  );//------------------------------------------------------------------------------------|

  // packet structure / frame length

  defparam  emu_dat.pW_LAN_LEN  = pW_LAN_LEN  ;
  defparam  emu_dat.pW_LAN_GAP  = pW_LAN_GAP  ;

  // packet structure / ip header

  defparam  emu_dat.pN_LAN_IPL  = lpN_LAN_IPL ;
  defparam  emu_dat.pN_LAN_IPH  = lpN_LAN_IPH ;

  // packet structure / payload

  defparam  emu_dat.pW_LAN_PAY  = pW_LAN_PAY  ;

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // lan interface implementation  [ emu_lan / tb_eth_emu_lan_v2 ]
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  // clear

  assign  emu_lan__i_sclr_n     = ri_reset_n           ;

  // setting / interface

  assign  emu_lan__i_set_type   = set_io_type          ;
  assign  emu_lan__i_set_speed  = set_io_speed         ;

  // lan packet / clock

  assign  emu_lan__i_clk        = emu_clk__o_act_clk   ;
  assign  emu_lan__i_cen        = emu_clk__o_act_cen   ;
  assign  emu_lan__i_ced        = emu_clk__o_act_ced   ;

  // lan packet / data

  assign  emu_lan__i_str_frame  = emu_str__o_str_frame ;
  assign  emu_lan__i_dat_frame  = emu_dat__o_dat_frame ;

  //--------------------------------------------------------------------------------------|
                                                          //                              |
  tb_eth_emu_lan_v2      emu_lan                          //                              |
  (                                                       //                              |
  // clear -----------------------------------------------//------------------------------|
                                                          //           |                  |
     .i_sclr_n           ( emu_lan__i_sclr_n        ),    //  in       |       opt        |
                                                          //           |                  |
  // setting ---------------------------------------------//-----------|------------------|
                                                          //           |                  |
     .i_set_type         ( emu_lan__i_set_type      ),    //  in       |  req             |
     .i_set_speed        ( emu_lan__i_set_speed     ),    //  in       |  req             |
                                                          //           |                  |
  // lan packet ------------------------------------------//-----------|------------------|
                                                          //           |                  |
  // clock               clock                            //-----------|------------------|
                                                          //           |                  |
     .i_clk              ( emu_lan__i_clk           ),    //  in       |  req             |
     .i_cen              ( emu_lan__i_cen           ),    //  in       |  req             |
     .i_ced              ( emu_lan__i_ced           ),    //  in       |  req             |
                                                          //           |                  |
  // data                data                             //-----------|------------------|
                                                          //           |                  |
     .i_str_frame        ( emu_lan__i_str_frame     ),    //  in       |  req             |
     .i_dat_frame        ( emu_lan__i_dat_frame     ),    //  in       |  req             |
                                                          //           |                  |
  // lan interface ---------------------------------------//-----------|------------------|
                                                          //           |                  |
  // lan mii             lan mii                          //-----------|------------------|
                                                          //           |                  |
     .o_mii_txd          ( emu_lan__o_mii_txd       ),    //      out  |  req             |
     .o_mii_tx_en        ( emu_lan__o_mii_tx_en     ),    //      out  |  req             |
     .o_mii_tx_clk       ( emu_lan__o_mii_tx_clk    ),    //      out  |  req             |
                                                          //           |                  |
  // lan rmii            lan rmii                         //-----------|------------------|
                                                          //           |                  |
     .o_rmii_txd         ( emu_lan__o_rmii_txd      ),    //      out  |  req             |
     .o_rmii_tx_en       ( emu_lan__o_rmii_tx_en    ),    //      out  |  req             |
     .o_rmii_tx_clk      ( emu_lan__o_rmii_tx_clk   ),    //      out  |  req             |
     .o_rmii_tx_clk_s    ( emu_lan__o_rmii_tx_clk_s ),    //      out  |            test  |
                                                          //           |                  |
  // lan gmii            lan gmii                         //-----------|------------------|
                                                          //           |                  |
     .o_gmii_txd         ( emu_lan__o_gmii_txd      ),    //      out  |  req             |
     .o_gmii_tx_en       ( emu_lan__o_gmii_tx_en    ),    //      out  |  req             |
     .o_gmii_tx_clk      ( emu_lan__o_gmii_tx_clk   )     //      out  |  req             |
                                                          //           |                  |
  );//------------------------------------------------------------------------------------|

  // custom mode (clock)

  defparam  emu_lan.pE_CST_CLK  = pE_CST_CLK ;

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // speed calculator (calculator of inter-frame gap)  [ emu_spd / tb_eth_emu_spd_v2 ]
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  // setting / interface

  assign  emu_spd__i_set_io_type    = set_io_type   ;
  assign  emu_spd__i_set_io_speed   = set_io_speed  ;

  // setting / transmit speed

  assign  emu_spd__i_set_tx_mode    = set_tx_mode   ;
  assign  emu_spd__i_set_tx_speed   = set_tx_speed  ;
  assign  emu_spd__i_set_tx_sel     = set_tx_sel    ;

  // setting / mac header

  assign  emu_spd__i_set_mac_ena    = set_mac_ena   ;

  // setting / frame length

  assign  emu_spd__i_set_len_type   = set_len_type  ;
  assign  emu_spd__i_set_len_const  = set_len_const ;
  assign  emu_spd__i_set_len_min    = set_len_min   ;
  assign  emu_spd__i_set_len_max    = set_len_max   ;

  // lan packet / length

  assign  emu_spd__i_len_frame      = emu_str__o_len_frame ;
  assign  emu_spd__i_len_gap        = emu_str__o_len_gap   ;

  //--------------------------------------------------------------------------------------|
                                                          //                              |
  tb_eth_emu_spd_v2      emu_spd                          //                              |
  (                                                       //                              |
  // setting ---------------------------------------------//-----------|------------------|
                                                          //           |                  |
  // interface           interface                        //-----------|------------------|
                                                          //           |                  |
     .i_set_io_type      ( emu_spd__i_set_io_type   ),    //  in       |  req             |
     .i_set_io_speed     ( emu_spd__i_set_io_speed  ),    //  in       |  req             |
                                                          //           |                  |
  // transmit speed      transmit speed                   //-----------|------------------|
                                                          //           |                  |
     .i_set_tx_mode      ( emu_spd__i_set_tx_mode   ),    //  in       |  req             |
     .i_set_tx_speed     ( emu_spd__i_set_tx_speed  ),    //  in       |  req             |
     .o_set_tx_speed     ( emu_spd__o_set_tx_speed  ),    //      out  |       opt        |
     .o_set_tx_limit     ( emu_spd__o_set_tx_limit  ),    //      out  |       opt        |
     .i_set_tx_sel       ( emu_spd__i_set_tx_sel    ),    //  in       |  req             |
                                                          //           |                  |
  // frame length        frame length                     //-----------|------------------|
                                                          //           |                  |
     .i_set_len_type     ( emu_spd__i_set_len_type  ),    //  in       |  req             |
     .i_set_len_const    ( emu_spd__i_set_len_const ),    //  in       |  req             |
     .i_set_len_min      ( emu_spd__i_set_len_min   ),    //  in       |       opt        |
     .i_set_len_max      ( emu_spd__i_set_len_max   ),    //  in       |       opt        |
                                                          //           |                  |
  // lan packet ------------------------------------------//-----------|------------------|
                                                          //           |                  |
  // length              length                           //-----------|------------------|
                                                          //           |                  |
     .i_len_frame        ( emu_spd__i_len_frame     ),    //  in       |  req             |
     .i_len_gap          ( emu_spd__i_len_gap       ),    //  in       |       opt        |
     .o_len_gap          ( emu_spd__o_len_gap       )     //      out  |  req             |
                                                          //           |                  |
  );//------------------------------------------------------------------------------------|

  // packet structure

  defparam  emu_spd.pW_LAN_LEN  = pW_LAN_LEN ;
  defparam  emu_spd.pW_LAN_GAP  = pW_LAN_GAP ;

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // in/out registers : befor reg (i/no) / after reg (ri/ro)
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //                                                                                                                befor reg              after reg
  // reset --------------------------------------------------// reset ------------------------------------------------------------------------------------------//

  z_line #(.pV_DELAY (1),            .pW_DATA (1)            )  ri__reset_n        ( o_lan_clk, 1'b1, 1'b0        , i_reset_n            , ri_reset_n           );
  //                                                                                                                ^                      ^
  // setting ------------------------------------------------// setting ----------------------------------------------------------------------------------------//

  // update                                                     update

  z_line #(.pV_DELAY (pS_SIM_GLTCH), .pW_DATA (1)            )  ro__set_upd_ena    ( o_lan_clk, 1'b1, ~ri_reset_n , no_set_upd_ena       , ro_set_upd_ena       );
  //                                                                                                                ^                      ^
  // transmit speed                                             transmit speed

  z_line #(.pV_DELAY (pS_SIM_GLTCH), .pW_DATA (20)           )  ro__set_tx_limit   ( o_lan_clk, 1'b1, ~ri_reset_n , no_set_tx_limit      , ro_set_tx_limit      );
  z_line #(.pV_DELAY (pS_SIM_GLTCH), .pW_DATA (20)           )  ro__set_tx_speed   ( o_lan_clk, 1'b1, ~ri_reset_n , no_set_tx_speed      , ro_set_tx_speed      );
  //                                                                                                                ^                      ^
  // lan packet ---------------------------------------------// lan packet -------------------------------------------------------------------------------------//

  // clock                                                      clock

  z_line #(.pV_DELAY (pS_SIM_GLTCH), .pW_DATA (1)            )  ro__lan_cen        ( o_lan_clk, 1'b1, ~ri_reset_n , no_lan_cen           , ro_lan_cen           );
  z_line #(.pV_DELAY (pS_SIM_GLTCH), .pW_DATA (1)            )  ro__lan_ced        ( o_lan_clk, 1'b1, ~ri_reset_n , no_lan_ced           , ro_lan_ced           );
  //                  ^                                                                                             ^                      ^
  // strob                                                      strob

  z_line #(.pV_DELAY (pS_SIM_GLTCH), .pW_DATA (3)            )  ro__lan_str_frame  ( o_lan_clk, 1'b1, ~ri_reset_n , no_lan_str_frame     , ro_lan_str_frame     );
  z_line #(.pV_DELAY (pS_SIM_GLTCH), .pW_DATA (pW_LAN_LEN)   )  ro__lan_cnt_frame  ( o_lan_clk, 1'b1, ~ri_reset_n , no_lan_cnt_frame     , ro_lan_cnt_frame     );
  z_line #(.pV_DELAY (pS_SIM_GLTCH), .pW_DATA (3)            )  ro__lan_str_head   ( o_lan_clk, 1'b1, ~ri_reset_n , no_lan_str_head      , ro_lan_str_head      );
  z_line #(.pV_DELAY (pS_SIM_GLTCH), .pW_DATA (4)            )  ro__lan_cnt_head   ( o_lan_clk, 1'b1, ~ri_reset_n , no_lan_cnt_frame     , ro_lan_cnt_head      );
  z_line #(.pV_DELAY (pS_SIM_GLTCH), .pW_DATA (3)            )  ro__lan_str_prm    ( o_lan_clk, 1'b1, ~ri_reset_n , no_lan_str_prm       , ro_lan_str_prm       );
  z_line #(.pV_DELAY (pS_SIM_GLTCH), .pW_DATA (3)            )  ro__lan_str_sfd    ( o_lan_clk, 1'b1, ~ri_reset_n , no_lan_str_sfd       , ro_lan_str_sfd       );
  z_line #(.pV_DELAY (pS_SIM_GLTCH), .pW_DATA (3)            )  ro__lan_str_body   ( o_lan_clk, 1'b1, ~ri_reset_n , no_lan_str_body      , ro_lan_str_body      );
  z_line #(.pV_DELAY (pS_SIM_GLTCH), .pW_DATA (pW_LAN_LEN)   )  ro__lan_cnt_body   ( o_lan_clk, 1'b1, ~ri_reset_n , no_lan_cnt_body      , ro_lan_cnt_body      );
  z_line #(.pV_DELAY (pS_SIM_GLTCH), .pW_DATA (3)            )  ro__lan_str_mac    ( o_lan_clk, 1'b1, ~ri_reset_n , no_lan_str_mac       , ro_lan_str_mac       );
  z_line #(.pV_DELAY (pS_SIM_GLTCH), .pW_DATA (4)            )  ro__lan_cnt_mac    ( o_lan_clk, 1'b1, ~ri_reset_n , no_lan_cnt_mac       , ro_lan_cnt_mac       );
  z_line #(.pV_DELAY (pS_SIM_GLTCH), .pW_DATA (3)            )  ro__lan_str_mda    ( o_lan_clk, 1'b1, ~ri_reset_n , no_lan_str_mda       , ro_lan_str_mda       );
  z_line #(.pV_DELAY (pS_SIM_GLTCH), .pW_DATA (3)            )  ro__lan_str_msa    ( o_lan_clk, 1'b1, ~ri_reset_n , no_lan_str_msa       , ro_lan_str_msa       );
  z_line #(.pV_DELAY (pS_SIM_GLTCH), .pW_DATA (3)            )  ro__lan_str_mlt    ( o_lan_clk, 1'b1, ~ri_reset_n , no_lan_str_mlt       , ro_lan_str_mlt       );
  z_line #(.pV_DELAY (pS_SIM_GLTCH), .pW_DATA (3)            )  ro__lan_str_ip     ( o_lan_clk, 1'b1, ~ri_reset_n , no_lan_str_ip        , ro_lan_str_ip        );
  z_line #(.pV_DELAY (pS_SIM_GLTCH), .pW_DATA (lpW_LAN_IP_C) )  ro__lan_cnt_ip     ( o_lan_clk, 1'b1, ~ri_reset_n , no_lan_cnt_ip        , ro_lan_cnt_ip        );
  z_line #(.pV_DELAY (pS_SIM_GLTCH), .pW_DATA (3)            )  ro__lan_str_pay    ( o_lan_clk, 1'b1, ~ri_reset_n , no_lan_str_pay       , ro_lan_str_pay       );
  z_line #(.pV_DELAY (pS_SIM_GLTCH), .pW_DATA (pW_LAN_LEN)   )  ro__lan_cnt_pay    ( o_lan_clk, 1'b1, ~ri_reset_n , no_lan_cnt_pay       , ro_lan_cnt_pay       );
  z_line #(.pV_DELAY (pS_SIM_GLTCH), .pW_DATA (3)            )  ro__lan_str_crc    ( o_lan_clk, 1'b1, ~ri_reset_n , no_lan_str_crc       , ro_lan_str_crc       );
  z_line #(.pV_DELAY (pS_SIM_GLTCH), .pW_DATA (pW_LAN_LEN)   )  ro__lan_cnt_crc    ( o_lan_clk, 1'b1, ~ri_reset_n , no_lan_cnt_crc       , ro_lan_cnt_crc       );
  z_line #(.pV_DELAY (pS_SIM_GLTCH), .pW_DATA (3)            )  ro__lan_str_gap    ( o_lan_clk, 1'b1, ~ri_reset_n , no_lan_str_gap       , ro_lan_str_gap       );
  z_line #(.pV_DELAY (pS_SIM_GLTCH), .pW_DATA (pW_LAN_GAP)   )  ro__lan_cnt_gap    ( o_lan_clk, 1'b1, ~ri_reset_n , no_lan_cnt_gap       , ro_lan_cnt_gap       );
  //                  ^                                                                                             ^                      ^
  // data                                                       data

  z_line #(.pV_DELAY (pS_SIM_GLTCH), .pW_DATA (1)            )  ro__lan_dat_load   ( o_lan_clk, 1'b1, ~ri_reset_n , no_lan_dat_load      , ro_lan_dat_load      );
  z_line #(.pV_DELAY (pS_SIM_GLTCH), .pW_DATA (8)            )  ro__lan_dat_frame  ( o_lan_clk, 1'b1, ~ri_reset_n , no_lan_dat_frame     , ro_lan_dat_frame     );
  //                  ^                                                                                             ^                      ^
  // length                                                     length

  z_line #(.pV_DELAY (pS_SIM_GLTCH), .pW_DATA (pW_LAN_LEN)   )  ro__lan_len_frame  ( o_lan_clk, 1'b1, ~ri_reset_n , no_lan_len_frame     , ro_lan_len_frame     );
  z_line #(.pV_DELAY (pS_SIM_GLTCH), .pW_DATA (pW_LAN_LEN)   )  ro__lan_len_body   ( o_lan_clk, 1'b1, ~ri_reset_n , no_lan_len_body      , ro_lan_len_body      );
  z_line #(.pV_DELAY (pS_SIM_GLTCH), .pW_DATA (pW_LAN_LEN)   )  ro__lan_len_pay    ( o_lan_clk, 1'b1, ~ri_reset_n , no_lan_len_pay       , ro_lan_len_pay       );
  z_line #(.pV_DELAY (pS_SIM_GLTCH), .pW_DATA (pW_LAN_GAP)   )  ro__lan_len_gap    ( o_lan_clk, 1'b1, ~ri_reset_n , no_lan_len_gap       , ro_lan_len_gap       );
  //                  ^                                                                                             ^                      ^
  // fcs crc                                                    fcs crc

  z_line #(.pV_DELAY (pS_SIM_GLTCH), .pW_DATA (1)            )  ro__lan_crc_rdy    ( o_lan_clk, 1'b1, ~ri_reset_n , no_lan_crc_rdy       , ro_lan_crc_rdy       );
  z_line #(.pV_DELAY (pS_SIM_GLTCH), .pW_DATA (8)            )  ro__lan_crc_err    ( o_lan_clk, 1'b1, ~ri_reset_n , no_lan_crc_err       , ro_lan_crc_err       );
  //                  ^                                                                                             ^                      ^
  // lan counters -------------------------------------------// lan counters -----------------------------------------------------------------------------------//

  // detector                                                   detector

  z_line #(.pV_DELAY (pS_SIM_GLTCH), .pW_DATA (1)            )  ro__lan_det_p_rdy  ( o_lan_clk, 1'b1, ~ri_reset_n , no_lan_det_p_rdy     , ro_lan_det_p_rdy     );
  z_line #(.pV_DELAY (pS_SIM_GLTCH), .pW_DATA (1)            )  ro__lan_det_p_err  ( o_lan_clk, 1'b1, ~ri_reset_n , no_lan_det_p_err     , ro_lan_det_p_err     );
  //                  ^                                                                                             ^                      ^
  // counters                                                   counters

  generate for (j=0; j<=pN_LAN_CNT; j++) begin:                 ro__lan_cnt

  z_line #(.pV_DELAY (pS_SIM_GLTCH), .pW_DATA (pW_LAN_CNT)   )  ro__lan_cnt_p_all  ( o_lan_clk, 1'b1, ~ri_reset_n , no_lan_cnt_p_all [j] , ro_lan_cnt_p_all [j] );
  z_line #(.pV_DELAY (pS_SIM_GLTCH), .pW_DATA (pW_LAN_CNT)   )  ro__lan_cnt_p_err  ( o_lan_clk, 1'b1, ~ri_reset_n , no_lan_cnt_p_err [j] , ro_lan_cnt_p_err [j] ); end endgenerate
  z_line #(.pV_DELAY (pS_SIM_GLTCH), .pW_DATA (1)            )  ro__lan_cnt_p_rdy  ( o_lan_clk, 1'b1, ~ri_reset_n , no_lan_cnt_p_rdy     , ro_lan_cnt_p_rdy     );
  //                  ^                                                                                             ^                      ^
  // lan interface ------------------------------------------// lan interface ----------------------------------------------------------------------------------//

  // lan mii                                                    lan mii

  z_line #(.pV_DELAY (pS_SIM_GLTCH), .pW_DATA (4)            )  ro__mii_txd        ( o_lan_clk, 1'b1, ~ri_reset_n , no_mii_txd           , ro_mii_txd           );
  z_line #(.pV_DELAY (pS_SIM_GLTCH), .pW_DATA (1)            )  ro__mii_tx_en      ( o_lan_clk, 1'b1, ~ri_reset_n , no_mii_tx_en         , ro_mii_tx_en         );
  //                  ^                                                                                             ^                      ^
  // lan rmii                                                   lan rmii

  z_line #(.pV_DELAY (pS_SIM_GLTCH), .pW_DATA (2)            )  ro__rmii_txd       ( o_lan_clk, 1'b1, ~ri_reset_n , no_rmii_txd          , ro_rmii_txd          );
  z_line #(.pV_DELAY (pS_SIM_GLTCH), .pW_DATA (1)            )  ro__rmii_tx_en     ( o_lan_clk, 1'b1, ~ri_reset_n , no_rmii_tx_en        , ro_rmii_tx_en        );
  z_line #(.pV_DELAY (pS_SIM_GLTCH), .pW_DATA (1)            )  ro__rmii_tx_clk_s  ( o_lan_clk, 1'b1, ~ri_reset_n , no_rmii_tx_clk_s     , ro_rmii_tx_clk_s     );
  //                  ^                                                                                             ^                      ^
  // lan gmii                                                   lan gmii

  z_line #(.pV_DELAY (pS_SIM_GLTCH), .pW_DATA (8)            )  ro__gmii_txd       ( o_lan_clk, 1'b1, ~ri_reset_n , no_gmii_txd          , ro_gmii_txd          );
  z_line #(.pV_DELAY (pS_SIM_GLTCH), .pW_DATA (1)            )  ro__gmii_tx_en     ( o_lan_clk, 1'b1, ~ri_reset_n , no_gmii_tx_en        , ro_gmii_tx_en        );
  //                  ^                                                                                             ^                      ^
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  endmodule

  //==========================================================================================================================================================================================================================//
  // instantiation template
  //==========================================================================================================================================================================================================================//
  /*
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // interface
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  //----------------------------------------------// ethernet emulator --------| [ eth_emu / tb_eth_emu_v2 ]

  //                                                 reset

  var    logic                                       eth_emu__i_reset_n        ;

  //                                                 setting

  var    logic                                       eth_emu__i_set_upd        ;
  var    logic                                       eth_emu__o_set_upd_ena    ;

  var    logic    [                        1 :0 ]    eth_emu__i_set_io_type    ;
  var    logic    [                        1 :0 ]    eth_emu__i_set_io_speed   ;
  var    logic                                       eth_emu__i_set_io_pause   ;

  var    logic                                       eth_emu__i_set_tx_mode    ;
  var    logic    [                       19 :0 ]    eth_emu__i_set_tx_speed   ;
  var    logic    [                       19 :0 ]    eth_emu__o_set_tx_speed   ;
  var    logic    [                       19 :0 ]    eth_emu__o_set_tx_limit   ;
  var    logic    [                        1 :0 ]    eth_emu__i_set_tx_sel     ;

  var    logic                                       eth_emu__i_set_err_ena    ;
  var    logic    [                        6 :0 ]    eth_emu__i_set_err_per    ;

  var    logic                                       eth_emu__i_set_mac_ena    ;
  var    logic    [                        2 :0 ]    eth_emu__i_set_mda_type   ;
  var    logic    [                       47 :0 ]    eth_emu__i_set_mda_const  ;
  var    logic    [                       47 :0 ]    eth_emu__i_set_mda_min    ;
  var    logic    [                       47 :0 ]    eth_emu__i_set_mda_max    ;
  var    logic    [                        2 :0 ]    eth_emu__i_set_msa_type   ;
  var    logic    [                       47 :0 ]    eth_emu__i_set_msa_const  ;
  var    logic    [                       47 :0 ]    eth_emu__i_set_msa_min    ;
  var    logic    [                       47 :0 ]    eth_emu__i_set_msa_max    ;
  var    logic    [                        2 :0 ]    eth_emu__i_set_mlt_type   ;
  var    logic    [                       15 :0 ]    eth_emu__i_set_mlt_const  ;
  var    logic    [                       15 :0 ]    eth_emu__i_set_mlt_min    ;
  var    logic    [                       15 :0 ]    eth_emu__i_set_mlt_max    ;

  var    logic                                       eth_emu__i_set_ip_ena     ;
  var    logic    [                        2 :0 ]    eth_emu__i_set_ip_type    ;
  var    logic    [ ETH_EMU__lpW_LAN_IP   -1 :0 ]    eth_emu__i_set_ip_const   ;
  var    logic    [ ETH_EMU__lpW_LAN_IP   -1 :0 ]    eth_emu__i_set_ip_min     ;
  var    logic    [ ETH_EMU__lpW_LAN_IP   -1 :0 ]    eth_emu__i_set_ip_max     ;

  var    logic    [                        2 :0 ]    eth_emu__i_set_pay_type   ;
  var    logic    [ ETH_EMU__pW_LAN_PAY   -1 :0 ]    eth_emu__i_set_pay_const  ;
  var    logic    [                        7 :0 ]    eth_emu__i_set_pay_min    ;
  var    logic    [                        7 :0 ]    eth_emu__i_set_pay_max    ;

  var    logic     [                       1 :0 ]    eth_emu__i_set_len_type   ;
  var    logic    [ ETH_EMU__pW_LAN_LEN   -1 :0 ]    eth_emu__i_set_len_const  ;
  var    logic    [ ETH_EMU__pW_LAN_LEN   -1 :0 ]    eth_emu__i_set_len_min    ;
  var    logic    [ ETH_EMU__pW_LAN_LEN   -1 :0 ]    eth_emu__i_set_len_max    ;

  var    logic    [                        1 :0 ]    eth_emu__i_set_gap_type   ;
  var    logic    [ ETH_EMU__pW_LAN_GAP   -1 :0 ]    eth_emu__i_set_gap_const  ;
  var    logic    [ ETH_EMU__pW_LAN_GAP   -1 :0 ]    eth_emu__i_set_gap_min    ;
  var    logic    [ ETH_EMU__pW_LAN_GAP   -1 :0 ]    eth_emu__i_set_gap_max    ;

  //                                                 lan packet

  var    logic                                       eth_emu__o_lan_clk        ;
  var    logic                                       eth_emu__o_lan_cen        ;
  var    logic                                       eth_emu__o_lan_ced        ;

  var    logic    [                        2 :0 ]    eth_emu__o_lan_str_frame  ;
  var    logic    [ ETH_EMU__pW_LAN_LEN   -1 :0 ]    eth_emu__o_lan_cnt_frame  ;
  var    logic    [                        2 :0 ]    eth_emu__o_lan_str_head   ;
  var    logic    [                        3 :0 ]    eth_emu__o_lan_cnt_head   ;
  var    logic    [                        2 :0 ]    eth_emu__o_lan_str_prm    ;
  var    logic    [                        2 :0 ]    eth_emu__o_lan_str_sfd    ;
  var    logic    [                        2 :0 ]    eth_emu__o_lan_str_body   ;
  var    logic    [ ETH_EMU__pW_LAN_LEN   -1 :0 ]    eth_emu__o_lan_cnt_body   ;
  var    logic    [                        2 :0 ]    eth_emu__o_lan_str_mac    ;
  var    logic    [                        3 :0 ]    eth_emu__o_lan_cnt_mac    ;
  var    logic    [                        2 :0 ]    eth_emu__o_lan_str_mda    ;
  var    logic    [                        2 :0 ]    eth_emu__o_lan_str_msa    ;
  var    logic    [                        2 :0 ]    eth_emu__o_lan_str_mlt    ;
  var    logic    [                        2 :0 ]    eth_emu__o_lan_str_ip     ;
  var    logic    [ ETH_EMU__lpW_LAN_IP_C -1 :0 ]    eth_emu__o_lan_cnt_ip     ;
  var    logic    [                        2 :0 ]    eth_emu__o_lan_str_pay    ;
  var    logic    [ ETH_EMU__pW_LAN_LEN   -1 :0 ]    eth_emu__o_lan_cnt_pay    ;
  var    logic    [                        2 :0 ]    eth_emu__o_lan_str_crc    ;
  var    logic    [                        2 :0 ]    eth_emu__o_lan_cnt_crc    ;
  var    logic    [                        2 :0 ]    eth_emu__o_lan_str_gap    ;
  var    logic    [ ETH_EMU__pW_LAN_GAP   -1 :0 ]    eth_emu__o_lan_cnt_gap    ;

  var    logic                                       eth_emu__o_lan_dat_load   ;
  var    logic    [                        7 :0 ]    eth_emu__o_lan_dat_frame  ;

  var    logic    [ ETH_EMU__pW_LAN_LEN   -1 :0 ]    eth_emu__o_lan_len_frame  ;
  var    logic    [ ETH_EMU__pW_LAN_LEN   -1 :0 ]    eth_emu__o_lan_len_body   ;
  var    logic    [ ETH_EMU__pW_LAN_LEN   -1 :0 ]    eth_emu__o_lan_len_pay    ;
  var    logic    [ ETH_EMU__pW_LAN_GAP   -1 :0 ]    eth_emu__o_lan_len_gap    ;

  var    logic                                       eth_emu__o_lan_crc_rdy    ;
  var    logic                                       eth_emu__o_lan_crc_err    ;

  //                                                 lan counters
  var    logic    [ ETH_EMU__pN_LAN_CNT      :1 ]
                  [ ETH_EMU__pW_LAN_LEN   -1 :0 ]    eth_emu__i_lan_set_p_top  ;
  var    logic    [ ETH_EMU__pN_LAN_CNT      :1 ]
                  [ ETH_EMU__pW_LAN_LEN   -1 :0 ]    eth_emu__i_lan_set_p_low  ;

  var    logic                                       eth_emu__o_lan_det_p_rdy  ;
  var    logic                                       eth_emu__o_lan_det_p_err  ;

  var    logic                                       eth_emu__i_lan_cnt_p_clr  ;

  var    logic                                       eth_emu__o_lan_cnt_p_rdy  ;
  var    logic    [ ETH_EMU__pN_LAN_CNT      :0 ]
                  [ ETH_EMU__pW_LAN_CNT   -1 :0 ]    eth_emu__o_lan_cnt_p_all  ;
  var    logic    [ ETH_EMU__pN_LAN_CNT      :0 ]
                  [ ETH_EMU__pW_LAN_CNT   -1 :0 ]    eth_emu__o_lan_cnt_p_err  ;

  //                                                 lan interface

  var    logic    [                        3 :0 ]    eth_emu__o_mii_txd        ;
  var    logic                                       eth_emu__o_mii_tx_en      ;
  var    logic                                       eth_emu__o_mii_tx_clk     ;

  var    logic    [                        1 :0 ]    eth_emu__o_rmii_txd       ;
  var    logic                                       eth_emu__o_rmii_tx_en     ;
  var    logic                                       eth_emu__o_rmii_tx_clk    ;
  var    logic                                       eth_emu__o_rmii_tx_clk_s  ;

  var    logic    [                        7 :0 ]    eth_emu__o_gmii_txd       ;
  var    logic                                       eth_emu__o_gmii_tx_en     ;
  var    logic                                       eth_emu__o_gmii_tx_clk    ;

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // testbench emulator of ethernet frames (auto generated)  [ eth_emu / tb_eth_emu_v2 ]
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  // reset

  assign  eth_emu__i_reset_n        = ;

  // setting / update

  assign  eth_emu__i_set_upd        = ;

  // setting / interface

  assign  eth_emu__i_set_io_type    = ;
  assign  eth_emu__i_set_io_speed   = ;
  assign  eth_emu__i_set_io_pause   = ;

  // setting / transmit speed

  assign  eth_emu__i_set_tx_mode    = ;
  assign  eth_emu__i_set_tx_speed   = ;
  assign  eth_emu__i_set_tx_sel     = ;

  // setting / crc error

  assign  eth_emu__i_set_err_ena    = ;
  assign  eth_emu__i_set_err_per    = ;

  // setting / mac header

  assign  eth_emu__i_set_mac_ena    = ;

  assign  eth_emu__i_set_mda_type   = ;
  assign  eth_emu__i_set_mda_const  = ;
  assign  eth_emu__i_set_mda_min    = ;
  assign  eth_emu__i_set_mda_max    = ;

  assign  eth_emu__i_set_msa_type   = ;
  assign  eth_emu__i_set_msa_const  = ;
  assign  eth_emu__i_set_msa_min    = ;
  assign  eth_emu__i_set_msa_max    = ;

  assign  eth_emu__i_set_mlt_type   = ;
  assign  eth_emu__i_set_mlt_const  = ;
  assign  eth_emu__i_set_mlt_min    = ;
  assign  eth_emu__i_set_mlt_max    = ;

  // setting / ip header

  assign  eth_emu__i_set_ip_ena     = ;
  assign  eth_emu__i_set_ip_type    = ;
  assign  eth_emu__i_set_ip_const   = ;
  assign  eth_emu__i_set_ip_min     = ;
  assign  eth_emu__i_set_ip_max     = ;

  // setting / payload

  assign  eth_emu__i_set_pay_type   = ;
  assign  eth_emu__i_set_pay_const  = ;
  assign  eth_emu__i_set_pay_min    = ;
  assign  eth_emu__i_set_pay_max    = ;

  // setting / frame length

  assign  eth_emu__i_set_len_type   = ;
  assign  eth_emu__i_set_len_const  = ;
  assign  eth_emu__i_set_len_min    = ;
  assign  eth_emu__i_set_len_max    = ;

  // setting / frame gap

  assign  eth_emu__i_set_gap_type   = ;
  assign  eth_emu__i_set_gap_const  = ;
  assign  eth_emu__i_set_gap_min    = ;
  assign  eth_emu__i_set_gap_max    = ;

  // lan counters / setting

  assign  eth_emu__i_lan_set_p_top  = ;
  assign  eth_emu__i_lan_set_p_low  = ;

  // lan counters / clear

  assign  eth_emu__i_lan_cnt_p_clr  = ;

  //--------------------------------------------------------------------------------------|
                                                          //                              |
  tb_eth_emu_v2          eth_emu                          //                              |
  (                                                       //                              |
  // reset -----------------------------------------------//------------------------------|
                                                          //           |                  |
     .i_reset_n          ( eth_emu__i_reset_n       ),    //  in       |       opt        |
                                                          //           |                  |
  // setting ---------------------------------------------//-----------|------------------|
                                                          //           |                  |
  // update              update                           //-----------|------------------|
                                                          //           |                  |
     .i_set_upd          ( eth_emu__i_set_upd       ),    //  in       |       opt        |
     .o_set_upd_ena      ( eth_emu__o_set_upd_ena   ),    //      out  |       opt        |
                                                          //           |                  |
  // interface           interface                        //-----------|------------------|
                                                          //           |                  |
     .i_set_io_type      ( eth_emu__i_set_io_type   ),    //  in       |  req             |
     .i_set_io_speed     ( eth_emu__i_set_io_speed  ),    //  in       |  req             |
     .i_set_io_pause     ( eth_emu__i_set_io_pause  ),    //  in       |       opt        |
                                                          //           |                  |
  // transmit speed      transmit speed                   //-----------|------------------|
                                                          //           |                  |
     .i_set_tx_mode      ( eth_emu__i_set_tx_mode   ),    //  in       |       opt        |
     .i_set_tx_speed     ( eth_emu__i_set_tx_speed  ),    //  in       |       opt        |
     .o_set_tx_speed     ( eth_emu__o_set_tx_speed  ),    //      out  |       opt        |
     .o_set_tx_limit     ( eth_emu__o_set_tx_limit  ),    //      out  |       opt        |
     .i_set_tx_sel       ( eth_emu__i_set_tx_sel    ),    //  in       |       opt        |
                                                          //           |                  |
  // crc error           crc error                        //-----------|------------------|
                                                          //           |                  |
     .i_set_err_ena      ( eth_emu__i_set_err_ena   ),    //  in       |       opt        |
     .i_set_err_per      ( eth_emu__i_set_err_per   ),    //  in       |       opt        |
                                                          //           |                  |
  // mac header          mac header                       //-----------|------------------|
                                                          //           |                  |
     .i_set_mac_ena      ( eth_emu__i_set_mac_ena   ),    //  in       |       opt        |
                                                          //           |                  |
     .i_set_mda_type     ( eth_emu__i_set_mda_type  ),    //  in       |       opt        |
     .i_set_mda_const    ( eth_emu__i_set_mda_const ),    //  in       |       opt        |
     .i_set_mda_min      ( eth_emu__i_set_mda_min   ),    //  in       |       opt        |
     .i_set_mda_max      ( eth_emu__i_set_mda_max   ),    //  in       |       opt        |
                                                          //           |                  |
     .i_set_msa_type     ( eth_emu__i_set_msa_type  ),    //  in       |       opt        |
     .i_set_msa_const    ( eth_emu__i_set_msa_const ),    //  in       |       opt        |
     .i_set_msa_min      ( eth_emu__i_set_msa_min   ),    //  in       |       opt        |
     .i_set_msa_max      ( eth_emu__i_set_msa_max   ),    //  in       |       opt        |
                                                          //           |                  |
     .i_set_mlt_type     ( eth_emu__i_set_mlt_type  ),    //  in       |       opt        |
     .i_set_mlt_const    ( eth_emu__i_set_mlt_const ),    //  in       |       opt        |
     .i_set_mlt_min      ( eth_emu__i_set_mlt_min   ),    //  in       |       opt        |
     .i_set_mlt_max      ( eth_emu__i_set_mlt_max   ),    //  in       |       opt        |
                                                          //           |                  |
  // ip header           ip header                        //-----------|------------------|
                                                          //           |                  |
     .i_set_ip_ena       ( eth_emu__i_set_ip_ena    ),    //  in       |       opt        |
     .i_set_ip_type      ( eth_emu__i_set_ip_type   ),    //  in       |       opt        |
     .i_set_ip_const     ( eth_emu__i_set_ip_const  ),    //  in       |       opt        |
     .i_set_ip_min       ( eth_emu__i_set_ip_min    ),    //  in       |       opt        |
     .i_set_ip_max       ( eth_emu__i_set_ip_max    ),    //  in       |       opt        |
                                                          //           |                  |
  // payload             payload                          //-----------|------------------|
                                                          //           |                  |
     .i_set_pay_type     ( eth_emu__i_set_pay_type  ),    //  in       |  req             |
     .i_set_pay_const    ( eth_emu__i_set_pay_const ),    //  in       |  req             |
     .i_set_pay_min      ( eth_emu__i_set_pay_min   ),    //  in       |       opt        |
     .i_set_pay_max      ( eth_emu__i_set_pay_max   ),    //  in       |       opt        |
                                                          //           |                  |
  // frame length        frame length                     //-----------|------------------|
                                                          //           |                  |
     .i_set_len_type     ( eth_emu__i_set_len_type  ),    //  in       |  req             |
     .i_set_len_const    ( eth_emu__i_set_len_const ),    //  in       |  req             |
     .i_set_len_min      ( eth_emu__i_set_len_min   ),    //  in       |       opt        |
     .i_set_len_max      ( eth_emu__i_set_len_max   ),    //  in       |       opt        |
                                                          //           |                  |
  // frame gap           frame gap                        //-----------|------------------|
                                                          //           |                  |
     .i_set_gap_type     ( eth_emu__i_set_gap_type  ),    //  in       |  req             |
     .i_set_gap_const    ( eth_emu__i_set_gap_const ),    //  in       |  req             |
     .i_set_gap_min      ( eth_emu__i_set_gap_min   ),    //  in       |       opt        |
     .i_set_gap_max      ( eth_emu__i_set_gap_max   ),    //  in       |       opt        |
                                                          //           |                  |
  // lan packet ------------------------------------------//-----------|------------------|
                                                          //           |                  |
  // clock               clock                            //-----------|------------------|
                                                          //           |                  |
     .o_lan_clk          ( eth_emu__o_lan_clk       ),    //      out  |  req             |
     .o_lan_cen          ( eth_emu__o_lan_cen       ),    //      out  |  req             |
     .o_lan_ced          ( eth_emu__o_lan_ced       ),    //      out  |  req             |
                                                          //           |                  |
  // strob               strob                            //-----------|------------------|
                                                          //           |                  |
     .o_lan_str_frame    ( eth_emu__o_lan_str_frame ),    //      out  |  req             |
     .o_lan_cnt_frame    ( eth_emu__o_lan_cnt_frame ),    //      out  |       opt        |
     .o_lan_str_head     ( eth_emu__o_lan_str_head  ),    //      out  |       opt        |
     .o_lan_cnt_head     ( eth_emu__o_lan_cnt_head  ),    //      out  |       opt        |
     .o_lan_str_prm      ( eth_emu__o_lan_str_prm   ),    //      out  |       opt        |
     .o_lan_str_sfd      ( eth_emu__o_lan_str_sfd   ),    //      out  |       opt        |
     .o_lan_str_body     ( eth_emu__o_lan_str_body  ),    //      out  |       opt        |
     .o_lan_cnt_body     ( eth_emu__o_lan_cnt_body  ),    //      out  |       opt        |
     .o_lan_str_mac      ( eth_emu__o_lan_str_mac   ),    //      out  |       opt        |
     .o_lan_cnt_mac      ( eth_emu__o_lan_cnt_mac   ),    //      out  |       opt        |
     .o_lan_str_mda      ( eth_emu__o_lan_str_mda   ),    //      out  |       opt        |
     .o_lan_str_msa      ( eth_emu__o_lan_str_msa   ),    //      out  |       opt        |
     .o_lan_str_mlt      ( eth_emu__o_lan_str_mlt   ),    //      out  |       opt        |
     .o_lan_str_ip       ( eth_emu__o_lan_str_ip    ),    //      out  |       opt        |
     .o_lan_cnt_ip       ( eth_emu__o_lan_cnt_ip    ),    //      out  |       opt        |
     .o_lan_str_pay      ( eth_emu__o_lan_str_pay   ),    //      out  |       opt        |
     .o_lan_cnt_pay      ( eth_emu__o_lan_cnt_pay   ),    //      out  |       opt        |
     .o_lan_str_crc      ( eth_emu__o_lan_str_crc   ),    //      out  |       opt        |
     .o_lan_cnt_crc      ( eth_emu__o_lan_cnt_crc   ),    //      out  |       opt        |
     .o_lan_str_gap      ( eth_emu__o_lan_str_gap   ),    //      out  |       opt        |
     .o_lan_cnt_gap      ( eth_emu__o_lan_cnt_gap   ),    //      out  |       opt        |
                                                          //           |                  |
  // data                data                             //-----------|------------------|
                                                          //           |                  |
     .o_lan_dat_load     ( eth_emu__o_lan_dat_load  ),    //      out  |  req             |
     .o_lan_dat_frame    ( eth_emu__o_lan_dat_frame ),    //      out  |  req             |
                                                          //           |                  |
  // length              length                           //-----------|------------------|
                                                          //           |                  |
     .o_lan_len_frame    ( eth_emu__o_lan_len_frame ),    //      out  |  req             |
     .o_lan_len_body     ( eth_emu__o_lan_len_body  ),    //      out  |       opt        |
     .o_lan_len_pay      ( eth_emu__o_lan_len_pay   ),    //      out  |       opt        |
     .o_lan_len_gap      ( eth_emu__o_lan_len_gap   ),    //      out  |       opt        |
                                                          //           |                  |
  // fcs crc             fcs crc                          //-----------|------------------|
                                                          //           |                  |
     .o_lan_crc_rdy      ( eth_emu__o_lan_crc_rdy   ),    //      out  |  req             |
     .o_lan_crc_err      ( eth_emu__o_lan_crc_err   ),    //      out  |  req             |
                                                          //           |                  |
  // lan counters ----------------------------------------//-----------|------------------|
                                                          //           |                  |
  // setting             setting                          //-----------|------------------|
                                                          //           |                  |
     .i_lan_set_p_top    ( eth_emu__i_lan_set_p_top ),    //  in       |       opt        |
     .i_lan_set_p_low    ( eth_emu__i_lan_set_p_low ),    //  in       |       opt        |
                                                          //           |                  |
  // detector            detector                         //-----------|------------------|
                                                          //           |                  |
     .o_lan_det_p_rdy    ( eth_emu__o_lan_det_p_rdy ),    //      out  |       opt        |
     .o_lan_det_p_err    ( eth_emu__o_lan_det_p_err ),    //      out  |       opt        |
                                                          //           |                  |
  // counters            counters                         //-----------|------------------|
                                                          //           |                  |
     .i_lan_cnt_p_clr    ( eth_emu__i_lan_cnt_p_clr ),    //  in       |       opt        |
     .o_lan_cnt_p_rdy    ( eth_emu__o_lan_cnt_p_rdy ),    //      out  |       opt        |
     .o_lan_cnt_p_all    ( eth_emu__o_lan_cnt_p_all ),    //      out  |       opt        |
     .o_lan_cnt_p_err    ( eth_emu__o_lan_cnt_p_err ),    //      out  |       opt        |
                                                          //           |                  |
  // lan interface ---------------------------------------//-----------|------------------|
                                                          //           |                  |
  // lan mii             lan mii                          //-----------|------------------|
                                                          //           |                  |
     .o_mii_txd          ( eth_emu__o_mii_txd       ),    //      out  |  req             |
     .o_mii_tx_en        ( eth_emu__o_mii_tx_en     ),    //      out  |  req             |
     .o_mii_tx_clk       ( eth_emu__o_mii_tx_clk    ),    //      out  |  req             |
                                                          //           |                  |
  // lan rmii            lan rmii                         //-----------|------------------|
                                                          //           |                  |
     .o_rmii_txd         ( eth_emu__o_rmii_txd      ),    //      out  |  req             |
     .o_rmii_tx_en       ( eth_emu__o_rmii_tx_en    ),    //      out  |  req             |
     .o_rmii_tx_clk      ( eth_emu__o_rmii_tx_clk   ),    //      out  |  req             |
     .o_rmii_tx_clk_s    ( eth_emu__o_rmii_tx_clk_s ),    //      out  |            test  |
                                                          //           |                  |
  // lan gmii            lan gmii                         //-----------|------------------|
                                                          //           |                  |
     .o_gmii_txd         ( eth_emu__o_gmii_txd      ),    //      out  |  req             |
     .o_gmii_tx_en       ( eth_emu__o_gmii_tx_en    ),    //      out  |  req             |
     .o_gmii_tx_clk      ( eth_emu__o_gmii_tx_clk   )     //      out  |  req             |
                                                          //           |                  |
  );//------------------------------------------------------------------------------------|
 
  // packet structure

  defparam  eth_emu.pE_LAN_HEAD  = ETH_EMU__pE_LAN_HEAD  ;
  defparam  eth_emu.pW_LAN_LEN   = ETH_EMU__pW_LAN_LEN   ;
  defparam  eth_emu.pW_LAN_GAP   = ETH_EMU__pW_LAN_GAP   ;
  defparam  eth_emu.pN_LAN_IPL   = ETH_EMU__pN_LAN_IPL   ;
  defparam  eth_emu.pN_LAN_IPH   = ETH_EMU__pN_LAN_IPH   ;
  defparam  eth_emu.pW_LAN_PAY   = ETH_EMU__pW_LAN_PAY   ;

  // packet counters

  defparam  eth_emu.pN_LAN_CNT   = ETH_EMU__pN_LAN_CNT   ;
  defparam  eth_emu.pW_LAN_CNT   = ETH_EMU__pW_LAN_CNT   ;

  // custom mode

  defparam  eth_emu.pE_CST_CLK   = ETH_EMU__pE_CST_CLK   ;
  defparam  eth_emu.pT_CST_CLK   = ETH_EMU__pT_CST_CLK   ;
  defparam  eth_emu.pT_CST_CEN   = ETH_EMU__pT_CST_CEN   ;
  defparam  eth_emu.pT_CST_CED   = ETH_EMU__pT_CST_CED   ;

  // simulation

  defparam  eth_emu.pS_SIM_GLTCH = ETH_EMU__pS_SIM_GLTCH ;

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  */




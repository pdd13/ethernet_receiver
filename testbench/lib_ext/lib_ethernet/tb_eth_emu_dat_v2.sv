  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Company      : Micran, Department of Telecommunications
  //
  //  Engineer     : Pogodaev Danil
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Library      : lib_ethernet / library of ethernet components
  //
  //  Component    : tb_eth_emu_v2 / testbench emulator of ethernet frames (packets)
  //
  //  File Name    : tb_eth_emu_dat_v2 .sv (testbench)
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Description  : Data stream generator of ethernet frames / see note (1)
  //
  //  Purpose (1)  : Generate data stream of ethernet frames
  //
  //                  - data load  / dat_load
  //                  - data frame / dat_frame
  //
  //  Purpose (2)  : Calculate frame check sequence (fcs) crc-32
  //
  //  Purpose (3)  : Insert crc error into generated frame (crc = 32'h ffffffff)
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Note (1)     : Structure of ethernet frames (std. 802.3) / see also table (2)
  //
  //                  - frame header preamble
  //                  - start-frame delimiter (sfd)
  //                  - destination mac address (mda)
  //                  - source mac address (msa)
  //                  - length of payload / type of protocol (mlt)
  //                  - data payload and pad
  //                  - frame check sequence (fcs) crc-32
  //                  - inter-frame gap
  //
  //  Note (2)     : Structure of ethernet strobes (testbench emulator)
  //
  //                  - strob                / str_name [0]
  //                  - strob / start bit    / str_name [1] / width: cen
  //                  - strob / stop bit     / str_name [2] / width: cen
  //                  - strob / byte counter / cnt_name
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Table (1)    : Clock manager of ethernet frames for testbench emulator
  //
  //                 |-----------------------------------------------------------------------------------------------|
  //                 |                   |           |                      frequency (period)                       |
  //                 |      setting      |   clock   |---------------------------------------------------------------|
  //                 |                   |           |      10 mbit/s      |     100 mbit/s     |    1000 mbit/s     |
  //                 |===================|===========|=====================|====================|====================|
  //                 |                   |  lan_clk  |  2.50 mhz (400 ns)  |  25.0 mhz (40 ns)  |  no                |
  //                 |  mii interface:   |-----------|---------------------|--------------------|--------------------|
  //                 |                   |  lan_cen  |  1'b1               |  1'b1              |  no                |
  //                 |  set_io_type = 2  |-----------|---------------------|--------------------|--------------------|
  //                 |                   |  lan_ced  |  1.25 mhz (800 ns)  |  12.5 mhz (80 ns)  |  no                |
  //                 |===================|===========|=====================|====================|====================|
  //                 |                   |  lan_clk  |  50.00 mhz (20 ns)  |  50.00 mhz (20 ns) |  no                |
  //                 |  rmii interface:  |-----------|---------------------|--------------------|--------------------|
  //                 |                   |  lan_cen  |  5.00 mhz (200 ns)  |  1'b1              |  no                |
  //                 |  set_io_type = 1  |-----------|---------------------|--------------------|--------------------|
  //                 |                   |  lan_ced  |  1.25 mhz (800 ns)  |  12.5 mhz (80 ns)  |  no                |
  //                 |===================|===========|=====================|====================|====================|
  //                 |                   |  lan_clk  |  2.50 mhz (400 ns)  |  25.0 mhz (40 ns)  |  125.0 mhz (8 ns)  |
  //                 |  gmii interface:  |-----------|---------------------|--------------------|--------------------|
  //                 |                   |  lan_cen  |  1'b1               |  1'b1              |  1'b1              |
  //                 |  set_io_type = 0  |-----------|---------------------|--------------------|--------------------|
  //                 |                   |  lan_ced  |  1.25 mhz (800 ns)  |  12.5 mhz (80 ns)  |  1'b1              |
  //                 |===================|===========|===============================================================|
  //                 |                   |  lan_clk  |  timescale * pT_CST_CLK                                       |
  //                 |  custom mode:     |-----------|---------------------------------------------------------------|
  //                 |                   |  lan_cen  |  timescale * pT_CST_CLK * pT_CST_CEN                          |
  //                 |  pE_CST_CLK = 1   |-----------|---------------------------------------------------------------|
  //                 |                   |  lan_ced  |  timescale * pT_CST_CLK * pT_CST_CEN * pT_CST_CED             |
  //                 |-----------------------------------------------------------------------------------------------|
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Table (2)    : Structure of ethernet frames (std. 802.3) for testbench emulator / see also note (1)
  //
  //                 |--------------------------------------------------------------------------------|
  //                 | frame header  |      mac header       |        data       |  fcs  | testbench  |
  //                 |---------------|-----------------------|-------------------|-------| ethrenet   |
  //                 |       8       |          14           |      46-1500      |   4   | emulator   |
  //                 |===============|=======================|===================|=======|============|
  //                 |  prm  |  sfd  |  mda  |  msa  |  mlt  |  payload  |  pad  |  crc  | strob      |
  //                 |-------|-------|-------|-------|-------|-----------|-------|-------| name       |
  //                 |   7   |   1   |   6   |   6   |   2   |  0-1500   | 0-46  |   4   |            |
  //                 |=======|=======|=======|=======|=======|===========|=======|=======|============|
  //                 |       |       |       |       |       |           |       |       |            |
  //                 | ***** | ***** | ***** | ***** | ***** | ********* | ***** | ***** | str_frame  |
  //                 | ***** | ***** |       |       |       |           |       |       | str_head   |
  //                 | ***** |       |       |       |       |           |       |       | str_prm    |
  //                 |       | ***** |       |       |       |           |       |       | str_sfd    |
  //                 |       |       | ***** | ***** | ***** | ********* | ***** | ***** | str_body   |
  //                 |       |       | ***** | ***** | ***** |           |       |       | str_mac    |
  //                 |       |       | ***** |       |       |           |       |       | str_mda    |
  //                 |       |       |       | ***** |       |           |       |       | str_msa    |
  //                 |       |       |       |       | ***** |           |       |       | str_mlt    |
  //                 |       |       |       |       |       | ********* | ***** |       | str_pay    |
  //                 |       |       |       |       |       | ***       |       |       | str_ip     |
  //                 |       |       |       |       |       |           |       | ***** | str_crc    |
  //                 |       |       |       |       |       |           |       |       |            |
  //                 |--------------------------------------------------------------------------------|
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Include Files
  //  
  //  Category       Type       Name               Ver     Description
  //
  //  lib_ethernet   function   eth_crc32_8d .sv   1.0.0   calculator of frame check sequence crc-32 (data 8 bit)
  //
  //  lib_global     function   cbit         .sv   1.0.0   bits calculator for input number (round up)
  //  lib_global     rtl        z_line       .sv   1.0.0   delay line for serial and parallel data
  //  lib_global     rtl        gen_npr      .sv   1.0.0   generator of registers with attribute "noprune"
  //  lib_global     rtl        shift_p2s    .sv   1.0.0   shift register for parallel to serial
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Revision List
  //
  //  Version   Date       Author
  //
  //  2.0.0     26.09.16   Pogodaev Danil
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  `timescale 1ns/1ns

  module tb_eth_emu_dat_v2 (

  //---- clear ------------|

         i_sclr_n          ,

  //---- setting ----------|

         i_set_ini         ,

  //     mac header

         i_set_mac_ena     ,
         i_set_mda_type    ,
         i_set_mda_const   ,
         i_set_mda_min     ,
         i_set_mda_max     ,
         i_set_msa_type    ,
         i_set_msa_const   ,
         i_set_msa_min     ,
         i_set_msa_max     ,
         i_set_mlt_type    ,
         i_set_mlt_const   ,
         i_set_mlt_min     ,
         i_set_mlt_max     ,

  //     ip header

         i_set_ip_ena      , 
         i_set_ip_type     ,
         i_set_ip_const    ,
         i_set_ip_min      ,
         i_set_ip_max      ,

  //     payload

         i_set_pay_type    ,
         i_set_pay_const   ,
         i_set_pay_min     ,
         i_set_pay_max     ,

  //---- lan packet -------|

  //     clock

         i_clk             ,
         i_cen             ,
         i_ced             ,

  //     strob

         i_str_frame       ,
         i_cnt_frame       ,
         i_str_head        ,
         i_cnt_head        ,
         i_str_prm         ,
         i_str_sfd         ,
         i_str_body        ,
         i_cnt_body        ,
         i_str_mac         ,
         i_cnt_mac         ,
         i_str_mda         ,
         i_str_msa         ,
         i_str_mlt         ,
         i_str_ip          ,
         i_cnt_ip          ,
         i_str_pay         ,
         i_cnt_pay         ,
         i_str_crc         ,
         i_cnt_crc         ,
         i_str_gap         ,
         i_cnt_gap         ,

  //     data

         o_dat_load        ,
         o_dat_frame       ,

  //     length

         i_len_frame       ,
         i_len_body        ,
         i_len_pay         ,
         i_len_gap         ,

  //     fcs crc

         i_crc_rdy         ,
         i_crc_err        );

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // include files : functions
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
                                                                     //      |
  `include "cbit.sv"                                                 // req  | bits calculator for input number (round up)
  `include "eth_crc32_8d.sv"                                         // req  | calculator of crc-32 (data 8 bit)
                                                                     //      |
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // parameters : setting
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
                                                                     //      |                                                 |          |           |
  // packet structure -----------------------------------------------//------|-------------------------------------------------|----------|-----------|-----------------------------------------------------------------------//
                                                                     //      |                                                 |          |           |
  // frame length                                                    //------|-------------------------------------------------|----------|-----------|-----------------------------------------------------------------------//
                                                                     //      |                                                 |          |           |
  parameter int  pW_LAN_LEN  = 11                               ;    // req  | frame length / width of bus                     | bit      | def: 11   | value: 14 (for jumbo-frame)
  parameter int  pW_LAN_GAP  = 16                               ;    // req  | frame gap    / width of bus                     | bit      | def: 16   |
                                                                     //      |                                                 |          |           |
  // ip header                                                       //------|-------------------------------------------------|----------|-----------|-----------------------------------------------------------------------//
                                                                     //      |                                                 |          |           |  
  parameter int  pN_LAN_IPL  = 1                                ;    // opt  | number of first (low) byte in payload           | byte     | def: 1    |
  parameter int  pN_LAN_IPH  = 4                                ;    // opt  | number of last (high) byte in payload           | byte     |           |
                                                                     //      |                                                 |          |           |
  // payload                                                         //------|-------------------------------------------------|----------|-----------|-----------------------------------------------------------------------//
                                                                     //      |                                                 |          |           |
  parameter int  pW_LAN_PAY  = 8                                ;    // req  | payload constant / width of bus                 | bit      | def: 8    | info: for constant value only (i_set_pay_const); value: 8/16/24/...
                                                                     //      |                                                 |          |           |
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // parameters : local (for i/o)
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
                                                                     //      |                                                 |          |           |
  // packet structure -----------------------------------------------//------|-------------------------------------------------|----------|-----------|-----------------------------------------------------------------------//
                                                                     //      |                                                 |          |           |
  // ip header                                                       //------|-------------------------------------------------|----------|-----------|-----------------------------------------------------------------------//
                                                                     //      |                                                 |          |           |
  parameter int  lpN_LAN_IPL  = pN_LAN_IPL                      ;    // opt  | number of first (low) byte in payload           | byte     | def: 1    |
  parameter int  lpN_LAN_IPH  = pN_LAN_IPH >= pN_LAN_IPL ?           // opt  | number of last (high) byte in payload           | byte     |           |
                                pN_LAN_IPH  : pN_LAN_IPL        ;    //      |                                                 |          |           |
                                                                     //      |                                                 |          |           |
  parameter int  lpN_LAN_IP   = lpN_LAN_IPH <= lpN_LAN_IPL ? 1  :    //      |                                                 |          |           |
                                lpN_LAN_IPH  - lpN_LAN_IPL + 1  ;    // opt  | number of bytes                                 | byte     |           |
                                                                     //      |                                                 |          |           |
  parameter int  lpW_LAN_IP   = lpN_LAN_IP * 8                  ;    // opt  | number of bits                                  | bit      |           | info: used for 'dat_ip'
                                                                     //      |                                                 |          |           |
  parameter int  lpW_LAN_IP_C = cbit(1,lpN_LAN_IP)              ;    // opt  | width of byte counter                           | bit      |           | info: used for 'cnt_ip'
                                                                     //      |                                                 |          |           |
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // interface
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
                                                                     //      |                                                 |          |           |
  //-------------------------------------// clear -------------------//------|-------------------------------------------------|----------|-----------|-----------------------------------------------------------------------//
                                                                     //      |                                                 |          |           |
  input  logic                              i_sclr_n            ;    // opt  | sync clear (ative low)                          | strob    | def: 1    |
                                                                     //      |                                                 |          |           |
  //-------------------------------------// setting -----------------//------|-------------------------------------------------|----------|-----------|-----------------------------------------------------------------------//
                                                                     //      |                                                 |          |           |
  input  logic                              i_set_ini           ;    // req  | setting : initialization                        | pulse    |           | width: cen
                                                                     //      |                                                 |          |           |
  //                                        mac header               //------|-------------------------------------------------|----------|-----------|-----------------------------------------------------------------------//
                                                                     //      |                                                 |          |           |
  input  logic                              i_set_mac_ena       ;    // opt  | setting : mac header                            | strob    | def: 0    |
                                                                     //      |       0 : mac inserter disable                  |          |           |
                                                                     //      |       1 : mac inserter enable                   |          |           |
                                                                     //      |                                                 |          |           |  
  //                                        mac header / mda         //------|-------------------------------------------------|----------|-----------|-----------------------------------------------------------------------//
                                                                     //      |                                                 |          |           |
  input  logic    [               2 :0 ]    i_set_mda_type      ;    // opt  | setting : mda type                              | pdata    | def: 0    |
                                                                     //      |       0 : mda constant                          |          |           | value: set_mda_const
                                                                     //      |       1 : mda random                            |          |           | value: set_mda_min to set_mda_max
                                                                     //      |       2 : mda counter of byte (individual)      |          |           | value: set_mda_min to set_mda_max
                                                                     //      |       3 : mda counter of byte (continuous)      |          |           | value: set_mda_min to set_mda_max
                                                                     //      |       4 : mda counter of packets                |          |           | value: set_mda_min to set_mda_max
                                                                     //      |                                                 |          |           |
  input  logic    [              47 :0 ]    i_set_mda_const     ;    // opt  | setting : mda constant / value                  | pdata    |           |
  input  logic    [              47 :0 ]    i_set_mda_min       ;    // opt  | setting : mda minimum  / value                  | pdata    | def: '0   |
  input  logic    [              47 :0 ]    i_set_mda_max       ;    // opt  | setting : mda maximum  / value                  | pdata    | def: '1   |
                                                                     //      |                                                 |          |           |  
  //                                        mac header / msa         //------|-------------------------------------------------|----------|-----------|-----------------------------------------------------------------------//
                                                                     //      |                                                 |          |           |
  input  logic    [               2 :0 ]    i_set_msa_type      ;    // opt  | setting : msa type                              | pdata    | def: 0    |
                                                                     //      |       0 : msa constant                          |          |           | value: set_msa_const
                                                                     //      |       1 : msa random                            |          |           | value: set_msa_min to set_msa_max
                                                                     //      |       2 : msa counter of byte (individual)      |          |           | value: set_msa_min to set_msa_max
                                                                     //      |       3 : msa counter of byte (continuous)      |          |           | value: set_msa_min to set_msa_max
                                                                     //      |       4 : msa counter of packets                |          |           | value: set_msa_min to set_msa_max
                                                                     //      |                                                 |          |           |
  input  logic    [              47 :0 ]    i_set_msa_const     ;    // opt  | setting : msa constant / value                  | pdata    |           |
  input  logic    [              47 :0 ]    i_set_msa_min       ;    // opt  | setting : msa minimum  / value                  | pdata    | def: '0   |
  input  logic    [              47 :0 ]    i_set_msa_max       ;    // opt  | setting : msa maximum  / value                  | pdata    | def: '1   |
                                                                     //      |                                                 |          |           |  
  //                                        mac header / mlt         //------|-------------------------------------------------|----------|-----------|-----------------------------------------------------------------------//
                                                                     //      |                                                 |          |           |
  input  logic    [               2 :0 ]    i_set_mlt_type      ;    // opt  | setting : mlt type                              | pdata    | def: 5    |
                                                                     //      |       0 : mlt constant                          |          |           | value: set_mlt_const
                                                                     //      |       1 : mlt random                            |          |           | value: set_mlt_min to set_mlt_max
                                                                     //      |       2 : mlt counter of byte (individual)      |          |           | value: set_mlt_min to set_mlt_max
                                                                     //      |       3 : mlt counter of byte (continuous)      |          |           | value: set_mlt_min to set_mlt_max
                                                                     //      |       4 : mlt counter of packets                |          |           | value: set_mlt_min to set_mlt_max
                                                                     //      |       5 : mlt length  of payload                |          |           | value: lan_len_pay
                                                                     //      |                                                 |          |           |
  input  logic    [              15 :0 ]    i_set_mlt_const     ;    // opt  | setting : mlt constant / value                  | pdata    |           |
  input  logic    [              15 :0 ]    i_set_mlt_min       ;    // opt  | setting : mlt minimum  / value                  | pdata    | def: '0   |
  input  logic    [              15 :0 ]    i_set_mlt_max       ;    // opt  | setting : mlt maximum  / value                  | pdata    | def: '1   |
                                                                     //      |                                                 |          |           |
  //                                        ip header                //------|-------------------------------------------------|----------|-----------|-----------------------------------------------------------------------//
                                                                     //      |                                                 |          |           |
  input  logic                              i_set_ip_ena        ;    // opt  | setting : ip header                             | strob    | def: 0    |
                                                                     //      |       0 : ip insert disable                     |          |           |
                                                                     //      |       1 : ip insert enable                      |          |           |
                                                                     //      |                                                 |          |           |
  input  logic    [               2 :0 ]    i_set_ip_type       ;    // opt  | setting : ip type                               | pdata    | def: 0    |
                                                                     //      |       0 : ip constant                           |          |           | value: set_ip_const
                                                                     //      |       1 : ip random                             |          |           | value: set_ip_min to set_ip_max
                                                                     //      |       2 : ip counter of byte (individual)       |          |           | value: set_ip_min to set_ip_max
                                                                     //      |       3 : ip counter of byte (continuous)       |          |           | value: set_ip_min to set_ip_max
                                                                     //      |       4 : ip counter of packets                 |          |           | value: set_ip_min to set_ip_max
                                                                     //      |                                                 |          |           |
  input  logic    [ lpW_LAN_IP   -1 :0 ]    i_set_ip_const      ;    // opt  | setting : ip constant                           | pdata    |           |
  input  logic    [ lpW_LAN_IP   -1 :0 ]    i_set_ip_min        ;    // opt  | setting : ip minimum                            | pdata    |           |
  input  logic    [ lpW_LAN_IP   -1 :0 ]    i_set_ip_max        ;    // opt  | setting : ip maximum                            | pdata    |           |
                                                                     //      |                                                 |          |           |
  //                                        payload                  //------|-------------------------------------------------|----------|-----------|-----------------------------------------------------------------------//
                                                                     //      |                                                 |          |           |
  input  logic    [               2 :0 ]    i_set_pay_type      ;    // req  | setting : payload type                          | pdata    | def: 0    |
                                                                     //      |       0 : payload constant                      |          |           | value: set_pay_const
                                                                     //      |       1 : payload random                        |          |           | value: set_pay_min to set_pay_max
                                                                     //      |       2 : payload counter of byte (individual)  |          |           | value: set_pay_min to set_pay_max
                                                                     //      |       3 : payload counter of byte (continuous)  |          |           | value: set_pay_min to set_pay_max
                                                                     //      |       4 : payload counter of packets            |          |           | value: set_pay_min to set_pay_max
                                                                     //      |                                                 |          |           |
  input  logic    [ pW_LAN_PAY   -1 :0 ]    i_set_pay_const     ;    // req  | setting : payload constant / value              | pdata    |           |
  input  logic    [               7 :0 ]    i_set_pay_min       ;    // opt  | setting : payload minimum  / value              | pdata    | def: '0   |
  input  logic    [               7 :0 ]    i_set_pay_max       ;    // opt  | setting : payload maximum  / value              | pdata    | def: '1   |
                                                                     //      |                                                 |          |           |
  //-------------------------------------// lan packet --------------//------|-------------------------------------------------|----------|-----------|-----------------------------------------------------------------------//
                                                                     //      |                                                 |          |           |
  //                                        clock                    //------|-------------------------------------------------|----------|-----------|-----------------------------------------------------------------------//
                                                                     //      |                                                 |          |           |
  input  logic                              i_clk               ;    // req  | clock                                           | clock    |           | info: see table (1)
  input  logic                              i_cen               ;    // req  | clock enable                                    | pulse    |           | info: see table (1) ; width: clk
  input  logic                              i_ced               ;    // req  | clock enable for data                           | pulse    |           | info: see table (1) ; width: cen
                                                                     //      |                                                 |          |           |
  //                                        strob                    //------|-------------------------------------------------|----------|-----------|-----------------------------------------------------------------------//
                                                                     //      |                                                 |          |           |
  input  logic    [               2 :0 ]    i_str_frame         ;    // req  | frame (packet)          / strob                 | strob    |           | info : see note (2)
  input  logic    [ pW_LAN_LEN   -1 :0 ]    i_cnt_frame         ;    // test | frame (packet)          / strob counter         | count up |           |
  input  logic    [               2 :0 ]    i_str_head          ;    // req  | frame header            / strob                 | strob    |           |
  input  logic    [               3 :0 ]    i_cnt_head          ;    // test | frame header            / strob counter         | count up |           |
  input  logic    [               2 :0 ]    i_str_prm           ;    // req  | frame header preamble   / strob                 | strob    |           |
  input  logic    [               2 :0 ]    i_str_sfd           ;    // req  | frame header sfd        / strob                 | strob    |           |
  input  logic    [               2 :0 ]    i_str_body          ;    // test | frame body              / strob                 | strob    |           |
  input  logic    [ pW_LAN_LEN   -1 :0 ]    i_cnt_body          ;    // test | frame body              / strob counter         | count up |           |
  input  logic    [               2 :0 ]    i_str_mac           ;    // opt  | mac header              / strob                 | strob    |           |
  input  logic    [               3 :0 ]    i_cnt_mac           ;    // test | mac header              / strob counter         | count up |           |
  input  logic    [               2 :0 ]    i_str_mda           ;    // opt  | mac destination address / strob                 | strob    |           |
  input  logic    [               2 :0 ]    i_str_msa           ;    // opt  | mac source address      / strob                 | strob    |           |
  input  logic    [               2 :0 ]    i_str_mlt           ;    // opt  | mac length or type      / strob                 | strob    |           |
  input  logic    [               2 :0 ]    i_str_ip            ;    // opt  | ip header               / strob                 | strob    |           |
  input  logic    [ lpW_LAN_IP_C -1 :0 ]    i_cnt_ip            ;    // test | ip header               / strob counter         | count up |           |
  input  logic    [               2 :0 ]    i_str_pay           ;    // req  | payload & pad           / strob                 | strob    |           |
  input  logic    [ pW_LAN_LEN   -1 :0 ]    i_cnt_pay           ;    // test | payload & pad           / strob counter         | count up |           |
  input  logic    [               2 :0 ]    i_str_crc           ;    // req  | fcs crc-32              / strob                 | strob    |           |
  input  logic    [               2 :0 ]    i_cnt_crc           ;    // req  | fcs crc-32              / strob counter         | count up |           |
  input  logic    [               2 :0 ]    i_str_gap           ;    // test | inter-frame gap         / strob                 | strob    |           |
  input  logic    [ pW_LAN_GAP   -1 :0 ]    i_cnt_gap           ;    // test | inter-frame gap         / strob counter         | count up |           |
                                                                     //      |                                                 |          |           |
  //                                        data                     //------|-------------------------------------------------|----------|-----------|-----------------------------------------------------------------------//
                                                                     //      |                                                 |          |           |
  output logic                              o_dat_load          ;    // req  | frame (packet)          / load (byte)           | pulse    |           | width: cen
  output logic    [               7 :0 ]    o_dat_frame         ;    // req  | frame (packet)          / data (byte)           | pdata    |           |
                                                                     //      |                                                 |          |           |
  //                                        length                   //------|-------------------------------------------------|----------|-----------|-----------------------------------------------------------------------//
                                                                     //      |                                                 |          |           |
  input  logic    [ pW_LAN_LEN   -1 :0 ]    i_len_frame         ;    // test | frame (packet)          / length (byte)         | pdata    |           |
  input  logic    [ pW_LAN_LEN   -1 :0 ]    i_len_body          ;    // test | frame body              / length (byte)         | pdata    |           |
  input  logic    [ pW_LAN_LEN   -1 :0 ]    i_len_pay           ;    // req  | payload                 / length (byte)         | pdata    |           |
  input  logic    [ pW_LAN_GAP   -1 :0 ]    i_len_gap           ;    // test | inter-frame gap         / length (byte)         | pdata    |           |
                                                                     //      |                                                 |          |           |
  //                                        fcs crc                  //------|-------------------------------------------------|----------|-----------|-----------------------------------------------------------------------//
                                                                     //      |                                                 |          |           |
  input  logic                              i_crc_rdy           ;    // opt  | fcs crc-32              / ready                 | pulse    |           | width: cen
  input  logic                              i_crc_err           ;    // opt  | fcs crc-32              / error                 | strob    |           |
                                                                     //      |                                                 |          |           |
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // logic
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  genvar                                    j                   ;

  //-------------------------------------// frame / data -------|
  
  var    logic                              dat_load            ;
  var    logic    [               7 :0 ]    dat_frame           ;

  //-------------------------------------// header / data ------|
  
  var    logic    [               7 :0 ]    dat_head            ;

  //-------------------------------------// mac / data ---------|
  
  var    logic    [               7 :0 ]    dat_mac             ;

  //                                        mda
  
  var    logic    [               7 :0 ]    dat_mda             ;
  var    logic    [               7 :0 ]    dat_mda_0           ;
  var    logic    [               7 :0 ]    dat_mda_1       = 0 ;
  var    logic    [               7 :0 ]    dat_mda_2       = 0 ;
  var    logic    [               7 :0 ]    dat_mda_3       = 0 ;
  var    logic    [               7 :0 ]    dat_mda_4       = 0 ;

  //                                        msa
  
  var    logic    [               7 :0 ]    dat_msa             ;
  var    logic    [               7 :0 ]    dat_msa_0           ;
  var    logic    [               7 :0 ]    dat_msa_1       = 0 ;
  var    logic    [               7 :0 ]    dat_msa_2       = 0 ;
  var    logic    [               7 :0 ]    dat_msa_3       = 0 ;
  var    logic    [               7 :0 ]    dat_msa_4       = 0 ;

  //                                        mlt
  
  var    logic    [               7 :0 ]    dat_mlt             ;
  var    logic    [               7 :0 ]    dat_mlt_0           ;
  var    logic    [               7 :0 ]    dat_mlt_1       = 0 ;
  var    logic    [               7 :0 ]    dat_mlt_2       = 0 ;
  var    logic    [               7 :0 ]    dat_mlt_3       = 0 ;
  var    logic    [               7 :0 ]    dat_mlt_4       = 0 ;
  var    logic    [               7 :0 ]    dat_mlt_5       = 0 ;
  var    logic                              dat_mlt_5_b1        ;
  var    logic                              dat_mlt_5_b1_z      ;
  var    logic                              dat_mlt_5_b2        ;

  //-------------------------------------// ip / data ----------|

  var    logic    [               7 :0 ]    dat_ip              ;
  var    logic    [               7 :0 ]    dat_ip_0            ;
  var    logic    [               7 :0 ]    dat_ip_1        = 0 ;
  var    logic    [               7 :0 ]    dat_ip_2        = 0 ;
  var    logic    [               7 :0 ]    dat_ip_3        = 0 ;
  var    logic    [               7 :0 ]    dat_ip_4        = 0 ;

  //-------------------------------------// payload / data -----|

  var    logic    [               7 :0 ]    dat_pay             ;
  var    logic    [               7 :0 ]    dat_pay_0           ;
  var    logic    [               7 :0 ]    dat_pay_1       = 0 ;
  var    logic    [               7 :0 ]    dat_pay_2       = 0 ;
  var    logic    [               7 :0 ]    dat_pay_3       = 0 ;
  var    logic    [               7 :0 ]    dat_pay_4       = 0 ;

  //-------------------------------------// fcs crc / data -----|

  var    logic    [               7 :0 ]    dat_crc             ;

  //-------------------------------------// fcs crc / calc -----|

  var    logic    [              31 :0 ]    crc_calc        ='1 ;
  var    logic    [               7 :0 ]
                  [               3 :0 ]    crc_dec             ;
  var    logic    [               3 :0 ]
                  [               7 :0 ]    crc_sum             ;

  //-------------------------------------// mda / shifter ------| [ mda_p2s / shift_p2s ]

  //                                        clock

  var    logic                              mda_p2s__i_clk      ;
  var    logic                              mda_p2s__i_clk_en   ;
  var    logic                              mda_p2s__i_sclr     ;

  //                                        serial

  var    logic                              mda_p2s__i_sload    ;
  var    logic                              mda_p2s__i_sload_en ;
  var    logic    [               7 :0 ]    mda_p2s__o_sdata    ;

  //                                        parallel

  var    logic                              mda_p2s__i_pload    ;
  var    logic    [              47 :0 ]    mda_p2s__i_pdata    ;

  //-------------------------------------// msa / shifter ------| [ msa_p2s / shift_p2s ]

  //                                        clock

  var    logic                              msa_p2s__i_clk      ;
  var    logic                              msa_p2s__i_clk_en   ;
  var    logic                              msa_p2s__i_sclr     ;

  //                                        serial

  var    logic                              msa_p2s__i_sload    ;
  var    logic                              msa_p2s__i_sload_en ;
  var    logic    [               7 :0 ]    msa_p2s__o_sdata    ;

  //                                        parallel

  var    logic                              msa_p2s__i_pload    ;
  var    logic    [              47 :0 ]    msa_p2s__i_pdata    ;

  //-------------------------------------// mlt / shifter ------| [ mlt_p2s / shift_p2s ]

  //                                        clock

  var    logic                              mlt_p2s__i_clk      ;
  var    logic                              mlt_p2s__i_clk_en   ;
  var    logic                              mlt_p2s__i_sclr     ;

  //                                        serial

  var    logic                              mlt_p2s__i_sload    ;
  var    logic                              mlt_p2s__i_sload_en ;
  var    logic    [               7 :0 ]    mlt_p2s__o_sdata    ;

  //                                        parallel

  var    logic                              mlt_p2s__i_pload    ;
  var    logic    [              15 :0 ]    mlt_p2s__i_pdata    ;

  //-------------------------------------// ip / shifter -------| [ ip_p2s / shift_p2s ]

  //                                        clock

  var    logic                              ip_p2s__i_clk       ;
  var    logic                              ip_p2s__i_clk_en    ;
  var    logic                              ip_p2s__i_sclr      ;

  //                                        serial

  var    logic                              ip_p2s__i_sload     ;
  var    logic                              ip_p2s__i_sload_en  ;
  var    logic    [               7 :0 ]    ip_p2s__o_sdata     ;

  //                                        parallel

  var    logic                              ip_p2s__i_pload     ;
  var    logic    [ lpW_LAN_IP   -1 :0 ]    ip_p2s__i_pdata     ;

  //-------------------------------------// pay / shifter ------| [ pay_p2s / shift_p2s ]

  //                                        clock

  var    logic                              pay_p2s__i_clk      ;
  var    logic                              pay_p2s__i_clk_en   ;
  var    logic                              pay_p2s__i_sclr     ;

  //                                        serial

  var    logic                              pay_p2s__i_sload    ;
  var    logic                              pay_p2s__i_sload_en ;
  var    logic    [               7 :0 ]    pay_p2s__o_sdata    ;

  //                                        parallel

  var    logic                              pay_p2s__i_pload    ;
  var    logic    [ pW_LAN_PAY   -1 :0 ]    pay_p2s__i_pdata    ;

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  //==========================================================================================================================================================================================================================//
  // body of module
  //==========================================================================================================================================================================================================================//

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // out logic (without out reg)
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  assign  o_dat_load   = dat_load  ;
  assign  o_dat_frame  = dat_frame ;

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // frame (packet) : data load (byte)
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  assign  dat_load = i_str_frame [0] & i_ced; // width = cen

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // frame (packet) : data generator
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  assign  dat_frame = i_str_head [0] ? dat_head :
                      i_str_mac  [0] ? dat_mac  :
                      i_str_ip   [0] ? dat_ip   :
                      i_str_pay  [0] ? dat_pay  :
                      i_str_crc  [0] ? dat_crc  : 8'h0;

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // frmae header : data generator
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  assign  dat_head  = i_str_prm  [0] ? 8'h55 :       // preamble
                      i_str_sfd  [0] ? 8'hd5 : 8'h0; // sfd

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // mac header : data generator
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  assign  dat_mac   = i_str_mda  [0] ? dat_mda :
                      i_str_msa  [0] ? dat_msa :
                      i_str_mlt  [0] ? dat_mlt : 8'h0;

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // destination mac address (mda) : data generator
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
 
  // type selection

  assign  dat_mda = (i_str_mda[0] == 1) ? (i_set_mda_type == 0 ? dat_mda_0 :
                                           i_set_mda_type == 1 ? dat_mda_1 :
                                           i_set_mda_type == 2 ? dat_mda_2 :
                                           i_set_mda_type == 3 ? dat_mda_3 :
                                           i_set_mda_type == 4 ? dat_mda_4 : 8'h0) : 8'h0;
  // constant value

  assign  dat_mda_0 = (i_set_mda_type == 0) ? mda_p2s__o_sdata : 8'h0; // from shift register (p2s)

  // random value

  always_ff @(posedge i_clk)
    if (~i_sclr_n)
      dat_mda_1 <= 8'h0;
    else
      if (i_cen & i_ced) // period = 1 byte
        begin
          if (i_set_mda_type == 1)
            begin
              if (i_str_mda[1] | i_str_mda[0] & ~i_str_mda[2]) // start bit or strob & not stop bit
               begin
                 dat_mda_1 <= i_set_mda_min + ({$random} % (i_set_mda_max - i_set_mda_min));
               end
            end
        end

  // byte counter up (individual)

  always_ff @(posedge i_clk)
    if (~i_sclr_n)
      dat_mda_2 <= 8'h0;
    else
      if (i_cen & i_ced) // period = 1 byte
        begin
          if (i_set_mda_type == 2)
            begin
              if (i_str_mda[1]) // start bit
                 dat_mda_2 <= i_set_mda_min;
              else
                if (i_str_mda[0] & ~i_str_mda[2]) // strob and not stop bit
                  begin
                    if (dat_mda_2 == i_set_mda_max)
                      dat_mda_2 <= i_set_mda_min;
                    else
                      dat_mda_2 <= dat_mda_2 + 1;
                  end
            end
        end

  // byte counter up (continuous)

  always_ff @(posedge i_clk)
    if (~i_sclr_n)
      dat_mda_3 <= 8'h0;
    else
      if (i_cen & i_ced) // period = 1 byte
        begin
          if (i_set_mda_type == 3)
            begin
              if (i_set_ini)
                dat_mda_3 <= i_set_mda_min - 1; // initial value
              else
                if (i_str_mda[1] | i_str_mda[0] & ~i_str_mda[2]) // start bit or strob & not stop bit
                  begin
                    if (dat_mda_3 == i_set_mda_max)
                      dat_mda_3 <= i_set_mda_min;
                    else
                      dat_mda_3 <= dat_mda_3 + 1;
                  end
            end
        end

  // packet counter up

  always_ff @(posedge i_clk)
    if (~i_sclr_n)
      dat_mda_4 <= 8'h0;
    else
      if (i_cen & i_ced) // period = 1 byte
        begin
          if (i_set_mda_type == 4)
            begin
              if (i_set_ini)
                dat_mda_4 <= i_set_mda_min - 1; // initial value
              else
                if (i_str_mda[1]) // start bit
                  begin
                    if (dat_mda_4 == i_set_mda_max)
                      dat_mda_4 <= i_set_mda_min;
                    else
                      dat_mda_4 <= dat_mda_4 + 1;
                  end
            end
        end

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // destination mac address (msa) : data generator
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  // type selection

  assign  dat_msa = (i_str_msa[0] == 1) ? (i_set_msa_type == 0 ? dat_msa_0 :
                                           i_set_msa_type == 1 ? dat_msa_1 :
                                           i_set_msa_type == 2 ? dat_msa_2 :
                                           i_set_msa_type == 3 ? dat_msa_3 :
                                           i_set_msa_type == 4 ? dat_msa_4 : 8'h0) : 8'h0;
  // constant value

 assign  dat_msa_0 = (i_set_msa_type == 0) ? msa_p2s__o_sdata : 8'h0; // from shift register (p2s)

  // random value

  always_ff @(posedge i_clk)
    if (~i_sclr_n)
      dat_msa_1 <= 8'h0;
    else
      if (i_cen & i_ced) // period = 1 byte
        begin
          if (i_set_msa_type == 1)
            begin
              if (i_str_msa[1] | i_str_msa[0] & ~i_str_msa[2]) // start bit or strob & not stop bit
               begin
                 dat_msa_1 <= i_set_msa_min + ({$random} % (i_set_msa_max - i_set_msa_min));
               end
            end
        end

  // byte counter up (individual)

  always_ff @(posedge i_clk)
    if (~i_sclr_n)
      dat_msa_2 <= 8'h0;
    else
      if (i_cen & i_ced) // period = 1 byte
        begin
          if (i_set_msa_type == 2)
            begin
              if (i_str_msa[1]) // start bit
                 dat_msa_2 <= i_set_msa_min;
              else
                if (i_str_msa[0] & ~i_str_msa[2]) // strob or not stop bit
                  begin
                    if (dat_msa_2 == i_set_msa_max)
                      dat_msa_2 <= i_set_msa_min;
                    else
                      dat_msa_2 <= dat_msa_2 + 1;
                  end
            end
        end

  // byte counter up (continuous)

  always_ff @(posedge i_clk)
    if (~i_sclr_n)
      dat_msa_3 <= 8'h0;
    else
      if (i_cen & i_ced) // period = 1 byte
        begin
          if (i_set_msa_type == 3)
            begin
              if (i_set_ini)
                dat_msa_3 <= i_set_msa_min - 1; // initial value
              else
                if (i_str_msa[1] | i_str_msa[0] & ~i_str_msa[2]) // start bit or strob & not stop bit
                  begin
                    if (dat_msa_3 == i_set_msa_max)
                      dat_msa_3 <= i_set_msa_min;
                    else
                      dat_msa_3 <= dat_msa_3 + 1;
                  end
            end
        end

  // packet counter up

  always_ff @(posedge i_clk)
    if (~i_sclr_n)
      dat_msa_4 <= 8'h0;
    else
      if (i_cen & i_ced) // period = 1 byte
        begin
          if (i_set_msa_type == 4)
            begin
              if (i_set_ini)
                dat_msa_4 <= i_set_msa_min - 1; // initial value
              else
                if (i_str_msa[1]) // start bit
                  begin
                    if (dat_msa_4 == i_set_msa_max)
                      dat_msa_4 <= i_set_msa_min;
                    else
                      dat_msa_4 <= dat_msa_4 + 1;
                  end
            end
        end

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // length of payload or type of protocol (mlt) : data generator
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
 
  // type selection

  assign  dat_mlt = (i_str_mlt[0] == 1) ? (i_set_mlt_type == 0 ? dat_mlt_0 :
                                           i_set_mlt_type == 1 ? dat_mlt_1 :
                                           i_set_mlt_type == 2 ? dat_mlt_2 :
                                           i_set_mlt_type == 3 ? dat_mlt_3 :
                                           i_set_mlt_type == 4 ? dat_mlt_4 :
                                           i_set_mlt_type == 5 ? dat_mlt_5 : 8'h0) : 8'h0;
  // constant value

 assign  dat_mlt_0 = (i_set_mlt_type == 0) ? mlt_p2s__o_sdata : 8'h0; // from shift register (p2s)

  // random value

  always_ff @(posedge i_clk)
    if (~i_sclr_n)
      dat_mlt_1 <= 8'h0;
    else
      if (i_cen & i_ced) // period = 1 byte
        begin
          if (i_set_mlt_type == 1)
            begin
              if (i_str_mlt[1] | i_str_mlt[0] & ~i_str_mlt[2]) // start bit or strob & not stop bit
               begin
                 dat_mlt_1 <= i_set_mlt_min + ({$random} % (i_set_mlt_max - i_set_mlt_min));
               end
            end
        end

  // byte counter up (individual)

  always_ff @(posedge i_clk)
    if (~i_sclr_n)
      dat_mlt_2 <= 8'h0;
    else
      if (i_cen & i_ced) // period = 1 byte
        begin
          if (i_set_mlt_type == 2)
            begin
              if (i_str_mlt[1]) // start bit
                 dat_mlt_2 <= i_set_mlt_min;
              else
                if (i_str_mlt[0] & ~i_str_mlt[2]) // strob & not stop bit
                  begin
                    if (dat_mlt_2 == i_set_mlt_max)
                      dat_mlt_2 <= i_set_mlt_min;
                    else
                      dat_mlt_2 <= dat_mlt_2 + 1;
                  end
            end
        end

  // byte counter up (continuous)

  always_ff @(posedge i_clk)
    if (~i_sclr_n)
      dat_mlt_3 <= 8'h0;
    else
      if (i_cen & i_ced) // period = 1 byte
        begin
          if (i_set_mlt_type == 3)
            begin
              if (i_set_ini)
                dat_mlt_3 <= i_set_mlt_min - 1; // initial value
              else
                if (i_str_mlt[1] | i_str_mlt[0] & ~i_str_mlt[2]) // start bit or strob & not stop bit
                  begin
                    if (dat_mlt_3 == i_set_mlt_max)
                      dat_mlt_3 <= i_set_mlt_min;
                    else
                      dat_mlt_3 <= dat_mlt_3 + 1;
                  end
            end
        end

  // packet counter up

  always_ff @(posedge i_clk)
    if (~i_sclr_n)
      dat_mlt_4 <= 8'h0;
    else
      if (i_cen & i_ced) // period = 1 byte
        begin
          if (i_set_mlt_type == 4)
            begin
              if (i_set_ini)
                dat_mlt_4 <= i_set_mlt_min - 1; // initial value
              else
                if (i_str_mlt[1]) // start bit
                  begin
                    if (dat_mlt_4 == i_set_mlt_max)
                      dat_mlt_4 <= i_set_mlt_min;
                    else
                      dat_mlt_4 <= dat_mlt_4 + 1;
                  end
            end
        end

  // length of payload

  always_ff @(posedge i_clk)
    if (~i_sclr_n)
      dat_mlt_5 <= 8'h0;
    else
      if (i_cen & i_ced) // period = 1 byte
        begin
          if (i_set_mlt_type == 5)
            begin
              if (dat_mlt_5_b1) dat_mlt_5 <= i_len_pay [pW_LAN_LEN -1:8]; else
              if (dat_mlt_5_b2) dat_mlt_5 <= i_len_pay [            7:0];
            end
        end

  // length of payload / byte selector

  assign  dat_mlt_5_b1 = i_str_mlt[1]   ; // start bit
  assign  dat_mlt_5_b2 = dat_mlt_5_b1_z ; // start bit / z1

  z_line // delay line

  #(.pV_DELAY (1), // value of delay | t.clk_en
    .pW_DATA  (1)) // width of data  | bit

  z__dat_mlt_5_b1 (.i_clk(i_clk), .i_clk_en(i_cen & i_ced), .i_sclr(~i_sclr_n), .i_data(dat_mlt_5_b1), .o_data(dat_mlt_5_b1_z) );
  //                                                                                    ^                      ^
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // ip header : data generator
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
 
  // type selection

  assign  dat_ip = ((i_set_ip_ena == 1) & (i_str_ip[0] == 1)) ? (i_set_ip_type == 0 ? dat_ip_0 :
                                                                 i_set_ip_type == 1 ? dat_ip_1 :
                                                                 i_set_ip_type == 2 ? dat_ip_2 :
                                                                 i_set_ip_type == 3 ? dat_ip_3 :
                                                                 i_set_ip_type == 4 ? dat_ip_4 : 8'h0) : 8'h0;
  // constant value

 assign  dat_ip_0 = (i_set_ip_type == 0) ? ip_p2s__o_sdata : 8'h0; // from shift register (p2s)

  // random value

  always_ff @(posedge i_clk)
    if (~i_sclr_n)
      dat_ip_1 <= 8'h0;
    else
      if (i_cen & i_ced) // period = 1 byte
        begin
          if (i_set_ip_type == 1)
            begin
              if (i_str_ip[1] | i_str_ip[0] & ~i_str_ip[2]) // start bit or strob & not stop bit
               begin
                   dat_ip_1 <= i_set_ip_min + ({$random} % (i_set_ip_max - i_set_ip_min));
               end
            end
        end

  // byte counter up (individual)

  always_ff @(posedge i_clk)
    if (~i_sclr_n)
      dat_ip_2 <= 8'h0;
    else
      if (i_cen & i_ced) // period = 1 byte
        begin
          if (i_set_ip_type == 2)
            begin
              if (i_str_ip[1]) // start bit
                 dat_ip_2 <= i_set_ip_min;
              else
                if (i_str_ip[0] & ~i_str_ip[2]) // strob & not stop bit
                  begin
                    if (dat_ip_2 == i_set_ip_max)
                      dat_ip_2 <= i_set_ip_min;
                    else
                      dat_ip_2 <= dat_ip_2 + 1;
                  end
            end
        end

  // byte counter up (continuous)

  always_ff @(posedge i_clk)
    if (~i_sclr_n)
      dat_ip_3 <= 8'h0;
    else
      if (i_cen & i_ced) // period = 1 byte
        begin
          if (i_set_ip_type == 3)
            begin
              if (i_set_ini)
                dat_ip_3 <= i_set_ip_min - 1; // initial value
              else
                if (i_str_ip[1] | i_str_ip[0] & ~i_str_ip[2]) // start bit or strob & not stop bit
                  begin
                    if (dat_ip_3 == i_set_ip_max)
                      dat_ip_3 <= i_set_ip_min;
                    else
                      dat_ip_3 <= dat_ip_3 + 1;
                  end
            end
        end

  // packet counter up

  always_ff @(posedge i_clk)
    if (~i_sclr_n)
      dat_ip_4 <= 8'h0;
    else
      if (i_cen & i_ced) // period = 1 byte
        begin
          if (i_set_ip_type == 4)
            begin
              if (i_set_ini)
                dat_ip_4 <= i_set_ip_min - 1; // initial value
              else
                if (i_str_ip[1]) // start bit
                  begin
                    if (dat_ip_4 == i_set_ip_max)
                      dat_ip_4 <= i_set_ip_min;
                    else
                      dat_ip_4 <= dat_ip_4 + 1;
                  end
            end
        end

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // payload : data generator
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  // type selection

  assign  dat_pay = (i_str_pay[0] == 1) ? (i_set_pay_type == 0 ? dat_pay_0 :
                                           i_set_pay_type == 1 ? dat_pay_1 :
                                           i_set_pay_type == 2 ? dat_pay_2 :
                                           i_set_pay_type == 3 ? dat_pay_3 :
                                           i_set_pay_type == 4 ? dat_pay_4 : 8'h0) : 8'h0;
  // constant value

 assign  dat_pay_0 = (i_set_pay_type != 0) ? 8'h0 : ((pW_LAN_PAY == 8) ? i_set_pay_const : pay_p2s__o_sdata); // from shift register (p2s)

  // random value

  always_ff @(posedge i_clk)
    if (~i_sclr_n)
      dat_pay_1 <= 8'h0;
    else
      if (i_cen & i_ced) // period = 1 byte
        begin
          if (i_set_pay_type == 1)
            begin
              if (i_str_pay[1] | i_str_pay[0] & ~i_str_pay[2] & ~i_str_ip[0]) // start bit or strob & not stop bit (and not strob ip)
               begin
                 dat_pay_1 <= i_set_pay_min + ({$random} % (i_set_pay_max - i_set_pay_min));
               end
            end
        end

  // byte counter up (individual)

  always_ff @(posedge i_clk)
    if (~i_sclr_n)
      dat_pay_2 <= 8'h0;
    else
      if (i_cen & i_ced) // period = 1 byte
        begin
          if (i_set_pay_type == 2)
            begin
              if (i_str_pay[1]) // start bit
                 dat_pay_2 <= i_set_pay_min;
              else
                if (i_str_pay[0] & ~i_str_pay[2] & ~i_str_ip[0]) // strob & not stop bit (and not strob ip)
                  begin
                    if (dat_pay_2 == i_set_pay_max)
                      dat_pay_2 <= i_set_pay_min;
                    else
                      dat_pay_2 <= dat_pay_2 + 1;
                  end
            end
        end

  // byte counter up (continuous)

  always_ff @(posedge i_clk)
    if (~i_sclr_n)
      dat_pay_3 <= 8'h0;
    else
      if (i_cen & i_ced) // period = 1 byte
        begin
          if (i_set_pay_type == 3)
            begin
              if (i_set_ini)
                dat_pay_3 <= i_set_pay_min - 1; // initial value
              else
                if (i_str_pay[1] | i_str_pay[0] & ~i_str_pay[2] & ~i_str_ip[0]) // start bit or strob & not stop bit (and not strob ip)
                  begin
                    if (dat_pay_3 == i_set_pay_max)
                      dat_pay_3 <= i_set_pay_min;
                    else
                      dat_pay_3 <= dat_pay_3 + 1;
                  end
            end
        end

  // packet counter up

  always_ff @(posedge i_clk)
    if (~i_sclr_n)
      dat_pay_4 <= 8'h0;
    else
      if (i_cen & i_ced) // period = 1 byte
        begin
          if (i_set_pay_type == 4)
            begin
              if (i_set_ini)
                dat_pay_4 <= i_set_pay_min - 1; // initial value
              else
                if (i_str_pay[1]) // start bit
                  begin
                    if (dat_pay_4 == i_set_pay_max)
                      dat_pay_4 <= i_set_pay_min;
                    else
                      dat_pay_4 <= dat_pay_4 + 1;
                  end
            end
        end

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // fcs crc-32 : data generator (for error insert 8'hff)
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  assign  dat_crc = (i_cnt_crc == 1) ? (i_crc_err ? 8'hff : crc_sum [3]) :
                    (i_cnt_crc == 2) ? (i_crc_err ? 8'hff : crc_sum [2]) :
                    (i_cnt_crc == 3) ? (i_crc_err ? 8'hff : crc_sum [1]) :
                    (i_cnt_crc == 4) ? (i_crc_err ? 8'hff : crc_sum [0]) : 8'h0;

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // fcs crc-32 : sum calculator
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  // crc-32 calculator

  always_ff @(posedge i_clk)
    if (~i_sclr_n)
      crc_calc <= '1; // all bits set to 1
    else  
      if (i_cen & i_ced) // period = 1 byte
        begin
          if (i_str_mac[0] | i_str_pay[0]) // mac and payload strob (not head & crc)
            begin
              crc_calc <= eth_crc32_8d (crc_calc, dat_frame);
            end
          else
            if (i_str_frame[2]) // stop bit
              begin
                crc_calc <= '1; // all bits set to 1
              end
        end

  // crc-32 decoder

  generate for (j=31; j>=0; j=j-1) begin: gen__crc_dec
  
  assign crc_dec [j[4:2]][3-j[1:0]] = ~crc_calc[j]; end
  
  endgenerate
  
  // crc-32 sum (result)
  
  assign  crc_sum [3][7:0] = { crc_dec [6][3:0], crc_dec [7][3:0] };
  assign  crc_sum [2][7:0] = { crc_dec [4][3:0], crc_dec [5][3:0] };
  assign  crc_sum [1][7:0] = { crc_dec [2][3:0], crc_dec [3][3:0] };
  assign  crc_sum [0][7:0] = { crc_dec [0][3:0], crc_dec [1][3:0] };

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // destination mac address (mda) : shifter / parallel to serial  [ mda_p2s / shift_p2s ]
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  // clock

  assign  mda_p2s__i_clk      = i_clk           ;
  assign  mda_p2s__i_clk_en   = i_cen & i_ced   ;
  assign  mda_p2s__i_sclr     = ~i_sclr_n       ;

  // parallel

  assign  mda_p2s__i_pload    = i_str_frame[1]  ; // frame start bit
  assign  mda_p2s__i_pdata    = i_set_mda_const ;

  // serial

  assign  mda_p2s__i_sload    = i_str_mda[0]    ; // mda strob
  assign  mda_p2s__i_sload_en = 1'b1            ;

  //----------------------------------------------------------------------------|
                                                //                              |
  shift_p2s         mda_p2s                     //                              |
  (                                             //                              |
  // clock -------------------------------------//------------------------------|
                                                //           |                  |
     .i_clk         ( mda_p2s__i_clk      ),    //  in       |  req             |
     .i_clk_en      ( mda_p2s__i_clk_en   ),    //  in       |       opt        |
     .i_sclr        ( mda_p2s__i_sclr     ),    //  in       |       opt        |
                                                //           |                  |
  // parallel ----------------------------------//-----------|------------------|
                                                //           |                  |
     .i_pload       ( mda_p2s__i_pload    ),    //  in       |  req             |
     .i_pdata       ( mda_p2s__i_pdata    ),    //  in       |  req             |
                                                //           |                  |
  // serial ------------------------------------//-----------|------------------|
                                                //           |                  |
     .i_sload       ( mda_p2s__i_sload    ),    //  in       |  req             |
     .i_sload_en    ( mda_p2s__i_sload_en ),    //  in       |       opt        |
     .o_sdata       ( mda_p2s__o_sdata    )     //      out  |  req             |
                                                //           |                  |
  );//--------------------------------------------------------------------------|

  // width of data

  defparam  mda_p2s.pW_SDATA  = 8  ; // 1 byte
  defparam  mda_p2s.pW_PDATA  = 48 ; // 6 bytes

  // first word

  defparam  mda_p2s.pS_FIRST  = "msb";

  // auto mode

  defparam  mda_p2s.pE_AUTO   = 0; // def: 0
  defparam  mda_p2s.pT_AUTO   = 0; // def: 0

  // signal tap (for tetsing)

  defparam  mda_p2s.pE_GNPR   = 0; // for testing only

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // destination mac address (msa) : shifter / parallel to serial  [ msa_p2s / shift_p2s ]
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  // clock

  assign  msa_p2s__i_clk      = i_clk           ;
  assign  msa_p2s__i_clk_en   = i_cen & i_ced   ;
  assign  msa_p2s__i_sclr     = ~i_sclr_n       ;

  // parallel

  assign  msa_p2s__i_pload    = i_str_frame[1]  ; // frame start bit
  assign  msa_p2s__i_pdata    = i_set_msa_const ;

  // serial

  assign  msa_p2s__i_sload    = i_str_msa[0]    ; // msa strob
  assign  msa_p2s__i_sload_en = 1'b1            ;

  //----------------------------------------------------------------------------|
                                                //                              |
  shift_p2s         msa_p2s                     //                              |
  (                                             //                              |
  // clock -------------------------------------//------------------------------|
                                                //           |                  |
     .i_clk         ( msa_p2s__i_clk      ),    //  in       |  req             |
     .i_clk_en      ( msa_p2s__i_clk_en   ),    //  in       |       opt        |
     .i_sclr        ( msa_p2s__i_sclr     ),    //  in       |       opt        |
                                                //           |                  |
  // parallel ----------------------------------//-----------|------------------|
                                                //           |                  |
     .i_pload       ( msa_p2s__i_pload    ),    //  in       |  req             |
     .i_pdata       ( msa_p2s__i_pdata    ),    //  in       |  req             |
                                                //           |                  |
  // serial ------------------------------------//-----------|------------------|
                                                //           |                  |
     .i_sload       ( msa_p2s__i_sload    ),    //  in       |  req             |
     .i_sload_en    ( msa_p2s__i_sload_en ),    //  in       |       opt        |
     .o_sdata       ( msa_p2s__o_sdata    )     //      out  |  req             |
                                                //           |                  |
  );//--------------------------------------------------------------------------|

  // width of data

  defparam  msa_p2s.pW_SDATA  = 8  ; // 1 byte
  defparam  msa_p2s.pW_PDATA  = 48 ; // 6 bytes

  // first word

  defparam  msa_p2s.pS_FIRST  = "msb";

  // auto mode

  defparam  msa_p2s.pE_AUTO   = 0; // def: 0
  defparam  msa_p2s.pT_AUTO   = 0; // def: 0

  // signal tap (for tetsing)

  defparam  msa_p2s.pE_GNPR   = 0; // for testing only

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // length of payload or type of protocol (mlt) : shifter / parallel to serial  [ mlt_p2s / shift_p2s ]
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  // clock

  assign  mlt_p2s__i_clk      = i_clk           ;
  assign  mlt_p2s__i_clk_en   = i_cen & i_ced   ;
  assign  mlt_p2s__i_sclr     = ~i_sclr_n       ;

  // parallel

  assign  mlt_p2s__i_pload    = i_str_frame[1]  ; // frame start bit
  assign  mlt_p2s__i_pdata    = i_set_mlt_const ;

  // serial

  assign  mlt_p2s__i_sload    = i_str_mlt[0]    ; // mlt strob
  assign  mlt_p2s__i_sload_en = 1'b1            ;

  //----------------------------------------------------------------------------|
                                                //                              |
  shift_p2s         mlt_p2s                     //                              |
  (                                             //                              |
  // clock -------------------------------------//------------------------------|
                                                //           |                  |
     .i_clk         ( mlt_p2s__i_clk      ),    //  in       |  req             |
     .i_clk_en      ( mlt_p2s__i_clk_en   ),    //  in       |       opt        |
     .i_sclr        ( mlt_p2s__i_sclr     ),    //  in       |       opt        |
                                                //           |                  |
  // parallel ----------------------------------//-----------|------------------|
                                                //           |                  |
     .i_pload       ( mlt_p2s__i_pload    ),    //  in       |  req             |
     .i_pdata       ( mlt_p2s__i_pdata    ),    //  in       |  req             |
                                                //           |                  |
  // serial ------------------------------------//-----------|------------------|
                                                //           |                  |
     .i_sload       ( mlt_p2s__i_sload    ),    //  in       |  req             |
     .i_sload_en    ( mlt_p2s__i_sload_en ),    //  in       |       opt        |
     .o_sdata       ( mlt_p2s__o_sdata    )     //      out  |  req             |
                                                //           |                  |
  );//--------------------------------------------------------------------------|

  // width of data

  defparam  mlt_p2s.pW_SDATA  = 8  ; // 1 byte
  defparam  mlt_p2s.pW_PDATA  = 16 ; // 2 bytes

  // first word

  defparam  mlt_p2s.pS_FIRST  = "msb";

  // auto mode

  defparam  mlt_p2s.pE_AUTO   = 0; // def: 0
  defparam  mlt_p2s.pT_AUTO   = 0; // def: 0

  // signal tap (for tetsing)

  defparam  mlt_p2s.pE_GNPR   = 0; // for testing only

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // ip header : shifter / parallel to serial  [ ip_p2s / shift_p2s ]
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  // clock

  assign  ip_p2s__i_clk      = i_clk           ;
  assign  ip_p2s__i_clk_en   = i_cen & i_ced   ;
  assign  ip_p2s__i_sclr     = ~i_sclr_n       ;

  // parallel

  assign  ip_p2s__i_pload    = i_str_frame[1]  ; // frame start bit
  assign  ip_p2s__i_pdata    = i_set_ip_const  ;

  // serial

  assign  ip_p2s__i_sload    = i_str_ip[0]     ; // ip strob
  assign  ip_p2s__i_sload_en = 1'b1            ;

  //---------------------------------------------------------------------------|
                                               //                              |
  shift_p2s         ip_p2s                     //                              |
  (                                            //                              |
  // clock ------------------------------------//------------------------------|
                                               //           |                  |
     .i_clk         ( ip_p2s__i_clk      ),    //  in       |  req             |
     .i_clk_en      ( ip_p2s__i_clk_en   ),    //  in       |       opt        |
     .i_sclr        ( ip_p2s__i_sclr     ),    //  in       |       opt        |
                                               //           |                  |
  // parallel ---------------------------------//-----------|------------------|
                                               //           |                  |
     .i_pload       ( ip_p2s__i_pload    ),    //  in       |  req             |
     .i_pdata       ( ip_p2s__i_pdata    ),    //  in       |  req             |
                                               //           |                  |
  // serial -----------------------------------//-----------|------------------|
                                               //           |                  |
     .i_sload       ( ip_p2s__i_sload    ),    //  in       |  req             |
     .i_sload_en    ( ip_p2s__i_sload_en ),    //  in       |       opt        |
     .o_sdata       ( ip_p2s__o_sdata    )     //      out  |  req             |
                                               //           |                  |
  );//-------------------------------------------------------------------------|

  // width of data

  defparam  ip_p2s.pW_SDATA  = 8          ; // 1 byte
  defparam  ip_p2s.pW_PDATA  = lpW_LAN_IP ; // x bytes

  // first word

  defparam  ip_p2s.pS_FIRST  = "msb";

  // auto mode

  defparam  ip_p2s.pE_AUTO   = 0; // def: 0
  defparam  ip_p2s.pT_AUTO   = 0; // def: 0

  // signal tap (for tetsing)

  defparam  ip_p2s.pE_GNPR   = 0; // for testing only

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // payload : shifter / parallel to serial  [ pay_p2s / shift_p2s ]
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  // clock

  assign  pay_p2s__i_clk      = i_clk           ;
  assign  pay_p2s__i_clk_en   = i_cen & i_ced   ;
  assign  pay_p2s__i_sclr     = ~i_sclr_n       ;

  // parallel

  assign  pay_p2s__i_pload    = i_str_frame[1]  ; // frame start bit
  assign  pay_p2s__i_pdata    = i_set_pay_const ;

  // serial

  assign  pay_p2s__i_sload    = i_str_pay[0]    ; // pay strob
  assign  pay_p2s__i_sload_en = 1'b1            ;

  //----------------------------------------------------------------------------|
                                                //                              |
  shift_p2s         pay_p2s                     //                              |
  (                                             //                              |
  // clock -------------------------------------//------------------------------|
                                                //           |                  |
     .i_clk         ( pay_p2s__i_clk      ),    //  in       |  req             |
     .i_clk_en      ( pay_p2s__i_clk_en   ),    //  in       |       opt        |
     .i_sclr        ( pay_p2s__i_sclr     ),    //  in       |       opt        |
                                                //           |                  |
  // parallel ----------------------------------//-----------|------------------|
                                                //           |                  |
     .i_pload       ( pay_p2s__i_pload    ),    //  in       |  req             |
     .i_pdata       ( pay_p2s__i_pdata    ),    //  in       |  req             |
                                                //           |                  |
  // serial ------------------------------------//-----------|------------------|
                                                //           |                  |
     .i_sload       ( pay_p2s__i_sload    ),    //  in       |  req             |
     .i_sload_en    ( pay_p2s__i_sload_en ),    //  in       |       opt        |
     .o_sdata       ( pay_p2s__o_sdata    )     //      out  |  req             |
                                                //           |                  |
  );//--------------------------------------------------------------------------|

  // width of data

  defparam  pay_p2s.pW_SDATA  = 8          ; // 1 byte
  defparam  pay_p2s.pW_PDATA  = pW_LAN_PAY ; // x bytes

  // first word

  defparam  pay_p2s.pS_FIRST  = "msb";

  // auto mode

  defparam  pay_p2s.pE_AUTO   = 1            ; // auto mode : enable
  defparam  pay_p2s.pT_AUTO   = pW_LAN_PAY/8 ; // auto mode : period (bytes)

  // signal tap (for tetsing)

  defparam  pay_p2s.pE_GNPR   = 0; // for testing only

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  endmodule




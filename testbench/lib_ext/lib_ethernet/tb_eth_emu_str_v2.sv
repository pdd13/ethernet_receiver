  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Company      : Micran, Department of Telecommunications
  //
  //  Engineer     : Pogodaev Danil
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Library      : lib_ethernet / library of ethernet components
  //
  //  Component    : tb_eth_emu_v2 / testbench emulator of ethernet frames (packets)
  //
  //  File Name    : tb_eth_emu_str_v2 .sv (testbench)
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Description  : Strobes generator (structure) of ethernet frames (auto generated) / see note (1)
  //
  //  Purpose (1)  : Generate strobes of ethernet frames / see table (2)
  //
  //                  - strobe                / o_str_name [0]
  //                  - strobe / start bit    / o_str_name [1] / width: o_cen
  //                  - strobe / stop bit     / o_str_name [2] / width: o_cen
  //                  - strobe / byte counter / o_cnt_name
  //
  //  Purpose (2)  : Calculate length of ethernet frames
  //
  //                  - length frame   / o_len_frame
  //                  - length body    / o_len_body
  //                  - length payload / o_len_pay
  //                  - length gap     / o_len_gap
  //
  //  Purpose (3)  : Count the number of ethernet frames (packets) / see note (2)
  //
  //                  - packet all   / o_cnt_p_all
  //                  - packet error / o_cnt_p_err
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Note (1)     : Structure of ethernet frames (std. 802.3) / see also table (2)
  //
  //                  - frame header preamble
  //                  - start-frame delimiter (sfd)
  //                  - destination mac address (mda)
  //                  - source mac address (msa)
  //                  - length of payload / type of protocol (mlt)
  //                  - data payload and pad
  //                  - frame check sequence (fcs) crc-32
  //                  - inter-frame gap
  //
  //  Note (2)     : Group counters for packet length (frame body)
  //
  //                 For example / SunLite GigE (Sunrise Telecom)
  //
  //                  - all size       / o_lan_cnt_p [0]
  //                  - 64-127 byte    / o_lan_cnt_p [1]
  //                  - 128-255 byte   / o_lan_cnt_p [2]
  //                  - 256-511 byte   / o_lan_cnt_p [3]
  //                  - 512-1023 byte  / o_lan_cnt_p [4]
  //                  - 1024-1518 byte / o_lan_cnt_p [5]
  //                  - oversize       / o_lan_cnt_p [6]
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Attached (1) : Additional (detailed) code comments / tb_eth_emu_str_v2 - comment .txt
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Table (1)    : Clock manager of ethernet frames for testbench emulator
  //
  //                 |-----------------------------------------------------------------------------------------------|
  //                 |                   |           |                      frequency (period)                       |
  //                 |      setting      |   clock   |---------------------------------------------------------------|
  //                 |                   |           |      10 mbit/s      |     100 mbit/s     |    1000 mbit/s     |
  //                 |===================|===========|=====================|====================|====================|
  //                 |                   |  act_clk  |  2.50 mhz (400 ns)  |  25.0 mhz (40 ns)  |  no                |
  //                 |  mii interface:   |-----------|---------------------|--------------------|--------------------|
  //                 |                   |  act_cen  |  1'b1               |  1'b1              |  no                |
  //                 |  set_type = 2     |-----------|---------------------|--------------------|--------------------|
  //                 |                   |  act_ced  |  1.25 mhz (800 ns)  |  12.5 mhz (80 ns)  |  no                |
  //                 |===================|===========|=====================|====================|====================|
  //                 |                   |  act_clk  |  50.00 mhz (20 ns)  |  50.00 mhz (20 ns) |  no                |
  //                 |  rmii interface:  |-----------|---------------------|--------------------|--------------------|
  //                 |                   |  act_cen  |  5.00 mhz (200 ns)  |  1'b1              |  no                |
  //                 |  set_type = 1     |-----------|---------------------|--------------------|--------------------|
  //                 |                   |  act_ced  |  1.25 mhz (800 ns)  |  12.5 mhz (80 ns)  |  no                |
  //                 |===================|===========|=====================|====================|====================|
  //                 |                   |  act_clk  |  2.50 mhz (400 ns)  |  25.0 mhz (40 ns)  |  125.0 mhz (8 ns)  |
  //                 |  gmii interface:  |-----------|---------------------|--------------------|--------------------|
  //                 |                   |  act_cen  |  1'b1               |  1'b1              |  1'b1              |
  //                 |  set_type = 0     |-----------|---------------------|--------------------|--------------------|
  //                 |                   |  act_ced  |  1.25 mhz (800 ns)  |  12.5 mhz (80 ns)  |  1'b1              |
  //                 |===================|===========|===============================================================|
  //                 |                   |  act_clk  |  timescale * pT_CST_CLK                                       |
  //                 |  custom mode:     |-----------|---------------------------------------------------------------|
  //                 |                   |  act_cen  |  timescale * pT_CST_CLK * pT_CST_CEN                          |
  //                 |  pE_CST_CLK = 1   |-----------|---------------------------------------------------------------|
  //                 |                   |  act_ced  |  timescale * pT_CST_CLK * pT_CST_CEN * pT_CST_CED             |
  //                 |-----------------------------------------------------------------------------------------------|
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Table (2)    : Structure of ethernet frames (std. 802.3) for testbench emulator / see also note (1)
  //
  //                 |--------------------------------------------------------------------------------|
  //                 | frame header  |      mac header       |        data       |  fcs  | testbench  |
  //                 |---------------|-----------------------|-------------------|-------| ethrenet   |
  //                 |       8       |          14           |      46-1500      |   4   | emulator   |
  //                 |===============|=======================|===================|=======|============|
  //                 |  prm  |  sfd  |  mda  |  msa  |  mlt  |  payload  |  pad  |  crc  | strob      |
  //                 |-------|-------|-------|-------|-------|-----------|-------|-------| name       |
  //                 |   7   |   1   |   6   |   6   |   2   |  0-1500   | 0-46  |   4   |            |
  //                 |=======|=======|=======|=======|=======|===========|=======|=======|============|
  //                 |       |       |       |       |       |           |       |       |            |
  //                 | ***** | ***** | ***** | ***** | ***** | ********* | ***** | ***** | str_frame  |
  //                 | ***** | ***** |       |       |       |           |       |       | str_head   |
  //                 | ***** |       |       |       |       |           |       |       | str_prm    |
  //                 |       | ***** |       |       |       |           |       |       | str_sfd    |
  //                 |       |       | ***** | ***** | ***** | ********* | ***** | ***** | str_body   |
  //                 |       |       | ***** | ***** | ***** |           |       |       | str_mac    |
  //                 |       |       | ***** |       |       |           |       |       | str_mda    |
  //                 |       |       |       | ***** |       |           |       |       | str_msa    |
  //                 |       |       |       |       | ***** |           |       |       | str_mlt    |
  //                 |       |       |       |       |       | ********* | ***** |       | str_pay    |
  //                 |       |       |       |       |       | ***       |       |       | str_ip     |
  //                 |       |       |       |       |       |           |       | ***** | str_crc    |
  //                 |       |       |       |       |       |           |       |       |            |
  //                 |--------------------------------------------------------------------------------|
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Include Files
  //  
  //  Category     Type       Name           Ver     Description
  //
  //  lib_global   function   cbit     .sv   1.0.0   bits calculator for input number (round up)
  //  lib_global   rtl        z_line   .sv   1.0.0   delay line for serial and parallel data
  //  lib_global   rtl        gen_npr  .sv   1.0.0   generator of registers with attribute "noprune"
  //  lib_global   rtl        det_se   .sv   1.0.0   detector of single edge for input strobe
  //  lib_global   rtl        gen_sstr .sv   1.0.0   generator of single strobe
  //  lib_global   rtl        gen_mstr .sv   1.0.0   generator of multi strobes
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Revision List
  //
  //  Version   Date       Author
  //
  //  2.0.0     26.09.16   Pogodaev Danil
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  `timescale 1ns/1ns

  module tb_eth_emu_str_v2 (

  //---- clear ------------|

         i_sclr_n          ,

  //---- setting ----------|

         i_set_ini         ,

  //     interface

         i_set_io_pause    ,

  //     transmit speed

         i_set_tx_mode     ,
         i_set_tx_gap      ,

  //     crc error

         i_set_err_ena     ,
         i_set_err_per     ,

  //     mac header

         i_set_mac_ena     ,

  //     ip header

         i_set_ip_ena      ,

  //     frame length

         i_set_len_type    ,
         i_set_len_const   ,
         i_set_len_min     ,
         i_set_len_max     ,

  //     frame gap

         i_set_gap_type    ,
         i_set_gap_const   ,
         i_set_gap_min     ,
         i_set_gap_max     ,

  //---- lan packet -------|

  //     clock

         i_clk             ,
         i_cen             ,
         i_ced             ,

  //     strob

         o_str_frame       ,
         o_cnt_frame       ,
         o_str_head        ,
         o_cnt_head        ,
         o_str_prm         ,
         o_str_sfd         ,
         o_str_body        ,
         o_cnt_body        ,
         o_str_mac         ,
         o_cnt_mac         ,
         o_str_mda         ,
         o_str_msa         ,
         o_str_mlt         ,
         o_str_ip          ,
         o_cnt_ip          ,
         o_str_pay         ,
         o_cnt_pay         ,
         o_str_crc         ,
         o_cnt_crc         ,
         o_str_gap         ,
         o_cnt_gap         ,

  //     length

         o_len_frame       ,
         o_len_body        ,
         o_len_pay         ,
         o_len_gap         ,

  //     fcs crc

         o_crc_rdy         ,
         o_crc_err         ,

  //---- lan counters -----|

  //     setting

         i_set_p_top       ,
         i_set_p_low       ,

  //     detector

         o_det_p_rdy       ,
         o_det_p_err       ,

  //     counters

         i_cnt_p_clr       ,
         o_cnt_p_rdy       ,
         o_cnt_p_all       ,
         o_cnt_p_err      );

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // include files : functions
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
                                                                     //      |
  `include "cbit.sv"                                                 // req  | bits calculator for input number (round up)
                                                                     //      |
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // parameters : setting
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
                                                                     //      |                                                 |          |           |
  // packet structure -----------------------------------------------//------|-------------------------------------------------|----------|-----------|-----------------------------------------------------------------------//
                                                                     //      |                                                 |          |           |
  // frame header                                                    //------|-------------------------------------------------|----------|-----------|-----------------------------------------------------------------------//
                                                                     //      |                                                 |          |           |
  parameter bit  pE_LAN_HEAD  = 1                               ;    // req  | enable of frame header                          | on/off   | def: 1    | info: used for custom mode
                                                                     //      |                                                 |          |           |
  // frame length                                                    //------|-------------------------------------------------|----------|-----------|-----------------------------------------------------------------------//
                                                                     //      |                                                 |          |           |
  parameter int  pW_LAN_LEN   = 11                              ;    // req  | frame length / width of bus                     | bit      | def: 11   | value: 14 (for jumbo-frame)
  parameter int  pW_LAN_GAP   = 16                              ;    // req  | frame gap    / width of bus                     | bit      | def: 16   |
                                                                     //      |                                                 |          |           |
  // ip header                                                       //------|-------------------------------------------------|----------|-----------|-----------------------------------------------------------------------//
                                                                     //      |                                                 |          |           |  
  parameter int  pN_LAN_IPL   = 1                               ;    // opt  | number of first (low) byte in payload           | byte     | def: 1    |
  parameter int  pN_LAN_IPH   = 4                               ;    // opt  | number of last (high) byte in payload           | byte     |           |
                                                                     //      |                                                 |          |           |
  // packet counters ------------------------------------------------//------|-------------------------------------------------|----------|-----------|-----------------------------------------------------------------------//
                                                                     //      |                                                 |          |           |
  parameter int  pN_LAN_CNT   = 0                               ;    // opt  | number of counters groups                       | group    | def: 0    | info: see purpose (3), note (2)
  parameter int  pW_LAN_CNT   = 24                              ;    // opt  | width of counters                               | bit      | def: 24   |
                                                                     //      |                                                 |          |           |
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // parameters : local (for i/o)
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
                                                                     //      |                                                 |          |           |
  // packet structure -----------------------------------------------//------|-------------------------------------------------|----------|-----------|-----------------------------------------------------------------------//
                                                                     //      |                                                 |          |           |
  // ip header                                                       //------|-------------------------------------------------|----------|-----------|-----------------------------------------------------------------------//
                                                                     //      |                                                 |          |           |
  parameter int  lpN_LAN_IP   = pN_LAN_IPH <= pN_LAN_IPL ? 1    :    //      |                                                 |          |           |
                                pN_LAN_IPH  - pN_LAN_IPL + 1    ;    // opt  | number of bytes                                 | byte     |           |
                                                                     //      |                                                 |          |           |
  parameter int  lpW_LAN_IP_C = cbit(1,lpN_LAN_IP)              ;    // opt  | width of byte counter                           | bit      |           | info: used for 'cnt_ip'
                                                                     //      |                                                 |          |           |
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // parameters : local (for logic)
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
                                                                     //      |                                                 |          |           |
  // multi strob generator ------------------------------------------//------|-------------------------------------------------|----------|-----------|-----------------------------------------------------------------------//
                                                                     //      |                                                 |          |           |
  // strob settings                                                  //------|-------------------------------------------------|----------|-----------|-----------------------------------------------------------------------//
                                                                     //      |                                                 |          |           |
  parameter int  lpN_STR_GEN  = 7                               ;    // req  | number of strobes                               | strob    | def: 7    |
  parameter int  lpW_STR_GEN  = pW_LAN_LEN                      ;    // req  | width of length                                 | bit      |           |
                                                                     //      |                                                 |          |           |
  // strob numbering                                                 //------|-------------------------------------------------|----------|-----------|-----------------------------------------------------------------------//
                                                                     //      |                                                 |          |           |
  parameter int  lpN_STR_PRM  = 1                               ;    // req  | frame header preamble                           |          | def: 1    |
  parameter int  lpN_STR_SFD  = 2                               ;    // req  | start-frame delimiter (sfd)                     |          | def: 2    |
  parameter int  lpN_STR_MDA  = 3                               ;    // req  | destination mac address (mda)                   |          | def: 3    |
  parameter int  lpN_STR_MSA  = 4                               ;    // req  | source mac address (msa)                        |          | def: 4    |
  parameter int  lpN_STR_MLT  = 5                               ;    // req  | length of payload / type of protocol (mlt)      |          | def: 5    |
  parameter int  lpN_STR_PAY  = 6                               ;    // req  | data payload and pad                            |          | def: 6    |
  parameter int  lpN_STR_CRC  = 7                               ;    // req  | frame check sequence (fcs) crc-32               |          | def: 7    |
                                                                     //      |                                                 |          |           |
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // interface
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
                                                                     //      |                                                 |          |           |
  //-------------------------------------// clear -------------------//------|-------------------------------------------------|----------|-----------|-----------------------------------------------------------------------//
                                                                     //      |                                                 |          |           |
  input  logic                              i_sclr_n            ;    // opt  | sync clear (ative low)                          | strob    | def: 1    |
                                                                     //      |                                                 |          |           |
  //-------------------------------------// setting -----------------//------|-------------------------------------------------|----------|-----------|-----------------------------------------------------------------------//
                                                                     //      |                                                 |          |           |
  input  logic                              i_set_ini           ;    // req  | setting : initialization                        | pulse    |           | width: cen ; info: see 'comment.txt' (1)
                                                                     //      |                                                 |          |           |
  //                                        interface                //------|-------------------------------------------------|----------|-----------|-----------------------------------------------------------------------//
                                                                     //      |                                                 |          |           |
  input  logic                              i_set_io_pause      ;    // opt  | setting : transmit pause                        | strob    | def: 0    | info: flow control ; see 'comment.txt' (2)
                                                                     //      |       0 : transmit enable                       |          |           |
                                                                     //      |       1 : transmit disable                      |          |           |
                                                                     //      |                                                 |          |           |
  //                                        transmit speed           //------|-------------------------------------------------|----------|-----------|-----------------------------------------------------------------------//
                                                                     //      |                                                 |          |           |
  input  logic                              i_set_tx_mode       ;    // opt  | setting : transmit mode                         | strob    | def: 0    | 
                                                                     //      |       0 : transmit gap manual                   |          |           | info: used 'i_set_gap_type'
                                                                     //      |       1 : transmit gap auto                     |          |           | info: used 'i_set_tx_gap'
                                                                     //      |                                                 |          |           |
  input  logic    [ pW_LAN_GAP   -1 :0 ]    i_set_tx_gap        ;    // opt  | setting : gap / value (byte)                    | pdata    |           |
                                                                     //      |                                                 |          |           |
  //                                        crc error                //------|-------------------------------------------------|----------|-----------|-----------------------------------------------------------------------//
                                                                     //      |                                                 |          |           |
  input  logic                              i_set_err_ena       ;    // opt  | setting : crc error                             | strob    | def: 0    |
                                                                     //      |       0 : crc error insert disable              |          |           |
                                                                     //      |       1 : crc error insert enable               |          |           | info: crc = 32'h ffffffff
                                                                     //      |                                                 |          |           |
  input  logic    [               6 :0 ]    i_set_err_per       ;    // opt  | setting : crc error probability (0-100%)        | pdata    | def: 0    | info: used $random
                                                                     //      |                                                 |          |           |
  //                                        mac header               //------|-------------------------------------------------|----------|-----------|-----------------------------------------------------------------------//
                                                                     //      |                                                 |          |           |
  input  logic                              i_set_mac_ena       ;    // opt  | setting : mac header                            | strob    | def: 0    |
                                                                     //      |       0 : mac inserter disable                  |          |           |
                                                                     //      |       1 : mac inserter enable                   |          |           |
                                                                     //      |                                                 |          |           |
  //                                        ip header                //------|-------------------------------------------------|----------|-----------|-----------------------------------------------------------------------//
                                                                     //      |                                                 |          |           |
  input  logic                              i_set_ip_ena        ;    // opt  | setting : ip header                             | strob    | def: 0    |
                                                                     //      |       0 : ip insert disable                     |          |           |
                                                                     //      |       1 : ip insert enable                      |          |           |
                                                                     //      |                                                 |          |           |
  //                                        frame length             //------|-------------------------------------------------|----------|-----------|-----------------------------------------------------------------------//
                                                                     //      |                                                 |          |           |
  input  logic     [              1 :0 ]    i_set_len_type      ;    // req  | setting : length type                           | pdata    | def: 0    |
                                                                     //      |       0 : length constant                       |          |           | value: set_len_const
                                                                     //      |       1 : length random                         |          |           | value: set_len_min to set_len_max
                                                                     //      |       2 : length counter                        |          |           | value: set_len_min to set_len_max
                                                                     //      |                                                 |          |           |
  input  logic    [ pW_LAN_LEN   -1 :0 ]    i_set_len_const     ;    // req  | setting : length constant / value (byte)        | pdata    |           |
  input  logic    [ pW_LAN_LEN   -1 :0 ]    i_set_len_min       ;    // opt  | setting : length minimum  / value (byte)        | pdata    | def: 72   |
  input  logic    [ pW_LAN_LEN   -1 :0 ]    i_set_len_max       ;    // opt  | setting : length maximum  / value (byte)        | pdata    | def: 1526 | value: 9018 (for jumbo-frame)
                                                                     //      |                                                 |          |           |
  //                                        frame gap                //------|-------------------------------------------------|----------|-----------|-----------------------------------------------------------------------//
                                                                     //      |                                                 |          |           |
  input  logic    [               1 :0 ]    i_set_gap_type      ;    // req  | setting : gap type                              | pdata    | def: 0    |
                                                                     //      |       0 : gap constant                          |          |           | value: set_gap_const
                                                                     //      |       1 : gap random                            |          |           | value: set_gap_min to set_gap_max
                                                                     //      |       2 : gap counter                           |          |           | value: set_gap_min to set_gap_max
                                                                     //      |                                                 |          |           |
  input  logic    [ pW_LAN_GAP   -1 :0 ]    i_set_gap_const     ;    // req  | setting : gap constant / value (byte)           | pdata    | def: 12   |
  input  logic    [ pW_LAN_GAP   -1 :0 ]    i_set_gap_min       ;    // opt  | setting : gap minimum  / value (byte)           | pdata    | def: 12   | info: min gap = 96 bit or 12 byte (std. 802.3)
  input  logic    [ pW_LAN_GAP   -1 :0 ]    i_set_gap_max       ;    // opt  | setting : gap maximum  / value (byte)           | pdata    |           |
                                                                     //      |                                                 |          |           |
  //-------------------------------------// lan packet --------------//------|-------------------------------------------------|----------|-----------|-----------------------------------------------------------------------//
                                                                     //      |                                                 |          |           |
  //                                        clock                    //------|-------------------------------------------------|----------|-----------|-----------------------------------------------------------------------//
                                                                     //      |                                                 |          |           |
  input  logic                              i_clk               ;    // req  | clock                                           | clock    |           | info: see table (1)
  input  logic                              i_cen               ;    // req  | clock enable                                    | pulse    |           | info: see table (1) ; width: clk
  input  logic                              i_ced               ;    // req  | clock enable for data                           | pulse    |           | info: see table (1) ; width: cen
                                                                     //      |                                                 |          |           |
  //                                        strob                    //------|-------------------------------------------------|----------|-----------|-----------------------------------------------------------------------//
                                                                     //      |                                                 |          |           |
  output logic    [               2 :0 ]    o_str_frame         ;    // req  | frame (packet)          / strob                 | strob    |           | info : see note (1)
  output logic    [ pW_LAN_LEN   -1 :0 ]    o_cnt_frame         ;    // opt  | frame (packet)          / strob counter         | count up |           |
  output logic    [               2 :0 ]    o_str_head          ;    // opt  | frame header            / strob                 | strob    |           |
  output logic    [               3 :0 ]    o_cnt_head          ;    // opt  | frame header            / strob counter         | count up |           |
  output logic    [               2 :0 ]    o_str_prm           ;    // opt  | frame header preamble   / strob                 | strob    |           |
  output logic    [               2 :0 ]    o_str_sfd           ;    // opt  | frame header sfd        / strob                 | strob    |           |
  output logic    [               2 :0 ]    o_str_body          ;    // opt  | frame body              / strob                 | strob    |           |
  output logic    [ pW_LAN_LEN   -1 :0 ]    o_cnt_body          ;    // opt  | frame body              / strob counter         | count up |           |
  output logic    [               2 :0 ]    o_str_mac           ;    // opt  | mac header              / strob                 | strob    |           |
  output logic    [               3 :0 ]    o_cnt_mac           ;    // opt  | mac header              / strob counter         | count up |           |
  output logic    [               2 :0 ]    o_str_mda           ;    // opt  | mac destination address / strob                 | strob    |           |
  output logic    [               2 :0 ]    o_str_msa           ;    // opt  | mac source address      / strob                 | strob    |           |
  output logic    [               2 :0 ]    o_str_mlt           ;    // opt  | mac length or type      / strob                 | strob    |           |
  output logic    [               2 :0 ]    o_str_ip            ;    // opt  | ip header               / strob                 | strob    |           |
  output logic    [ lpW_LAN_IP_C -1 :0 ]    o_cnt_ip            ;    // opt  | ip header               / strob counter         | count up |           |
  output logic    [               2 :0 ]    o_str_pay           ;    // opt  | payload & pad           / strob                 | strob    |           |
  output logic    [ pW_LAN_LEN   -1 :0 ]    o_cnt_pay           ;    // opt  | payload & pad           / strob counter         | count up |           |
  output logic    [               2 :0 ]    o_str_crc           ;    // opt  | fcs crc-32              / strob                 | strob    |           |
  output logic    [               2 :0 ]    o_cnt_crc           ;    // opt  | fcs crc-32              / strob counter         | count up |           |
  output logic    [               2 :0 ]    o_str_gap           ;    // opt  | inter-frame gap         / strob                 | strob    |           |
  output logic    [ pW_LAN_GAP   -1 :0 ]    o_cnt_gap           ;    // opt  | inter-frame gap         / strob counter         | count up |           |
                                                                     //      |                                                 |          |           |
  //                                        length                   //------|-------------------------------------------------|----------|-----------|-----------------------------------------------------------------------//
                                                                     //      |                                                 |          |           |
  output logic    [ pW_LAN_LEN   -1 :0 ]    o_len_frame         ;    // req  | frame (packet)          / length (byte)         | pdata    |           |
  output logic    [ pW_LAN_LEN   -1 :0 ]    o_len_body          ;    // opt  | frame body              / length (byte)         | pdata    |           |
  output logic    [ pW_LAN_LEN   -1 :0 ]    o_len_pay           ;    // opt  | payload                 / length (byte)         | pdata    |           |
  output logic    [ pW_LAN_GAP   -1 :0 ]    o_len_gap           ;    // opt  | inter-frame gap         / length (byte)         | pdata    |           |
                                                                     //      |                                                 |          |           |
  //                                        fcs crc                  //------|-------------------------------------------------|----------|-----------|-----------------------------------------------------------------------//
                                                                     //      |                                                 |          |           |
  output logic                              o_crc_rdy           ;    // req  | fcs crc-32              / ready                 | pulse    |           | width: cen
  output logic                              o_crc_err           ;    // req  | fcs crc-32              / error                 | strob    |           |
                                                                     //      |                                                 |          |           |
  //-------------------------------------// lan counters ------------//------|-------------------------------------------------|----------|-----------|-----------------------------------------------------------------------//
                                                                     //      |                                                 |          |           |
  //                                        setting                  //------|-------------------------------------------------|----------|-----------|-----------------------------------------------------------------------//
  input  logic    [ pN_LAN_CNT      :1 ]                             //      |                                                 |          |           |
                  [ pW_LAN_LEN   -1 :0 ]    i_set_p_top         ;    // opt  | top bound of body length (not frame)            | array    |           | info: see purpose (3) and note (2)
  input  logic    [ pN_LAN_CNT      :1 ]                             //      |                                                 |          |           |
                  [ pW_LAN_LEN   -1 :0 ]    i_set_p_low         ;    // opt  | low bound of body length (not frame)            | array    |           | info: see purpose (3) and note (2)
                                                                     //      |                                                 |          |           |
  //                                        detector                 //------|-------------------------------------------------|----------|-----------|-----------------------------------------------------------------------//
                                                                     //      |                                                 |          |           |
  output logic                              o_det_p_rdy         ;    // opt  | packet ready                                    | pulse    |           | width: cen
  output logic                              o_det_p_err         ;    // opt  | packet error                                    | strob    |           |
                                                                     //      |                                                 |          |           |
  //                                        counters                 //------|-------------------------------------------------|----------|-----------|-----------------------------------------------------------------------//
                                                                     //      |                                                 |          |           |
  input  logic                              i_cnt_p_clr         ;    // opt  | current value / clear                           | strob    | def: 0    | info: sensitivity to rising edge
                                                                     //      |                                                 |          |           |
  output logic                              o_cnt_p_rdy         ;    // opt  | current value / ready                           | pulse    |           | width: cen
  output logic    [ pN_LAN_CNT      :0 ]                             //      |                                                 |          |           |
                  [ pW_LAN_CNT   -1 :0 ]    o_cnt_p_all         ;    // opt  | current value / packet all                      | count up |           |
  output logic    [ pN_LAN_CNT      :0 ]                             //      |                                                 |          |           |
                  [ pW_LAN_CNT   -1 :0 ]    o_cnt_p_err         ;    // opt  | current value / packet error                    | count up |           |
                                                                     //      |                                                 |          |           |
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // logic
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
 
  //-------------------------------------// start of emulator -------|
  
  var    logic                              state_idle               ;
  var    logic                              state_idle_re            ;
  var    logic                              start_first              ;
  var    logic                              start_nxt                ;
  var    logic                              start                    ;
  
  //-------------------------------------// frame / length  ---------|

  var    logic    [ pW_LAN_LEN   -1 :0 ]    len_frame_nxt        = 0 ;
  var    logic    [ pW_LAN_LEN   -1 :0 ]    len_frame            = 0 ;

  //-------------------------------------// body / length  ----------|

  var    logic    [               3 :0 ]    len_head                 ;
  var    logic    [               3 :0 ]    len_mac                  ;
  var    logic    [               2 :0 ]    len_crc                  ;

  var    logic    [ pW_LAN_LEN   -1 :0 ]    len_pay              = 0 ;
  var    logic    [ pW_LAN_LEN   -1 :0 ]    len_pay_nxt              ;

  var    logic    [ pW_LAN_LEN   -1 :0 ]    len_body             = 0 ;
  var    logic    [ pW_LAN_LEN   -1 :0 ]    len_body_nxt             ;

  //-------------------------------------// gap / length ------------|

  var    logic    [ pW_LAN_GAP   -1 :0 ]    len_gap              = 0 ;
  var    logic    [ pW_LAN_GAP   -1 :0 ]    len_gap_nxt          = 0 ;

  //-------------------------------------// frame / multi -----------| [ mstr / gen_mstr ]

  //                                        clock

  var    logic                              mstr__i_clk              ;
  var    logic                              mstr__i_clk_en           ;
  var    logic                              mstr__i_sclr             ;

  //                                        control
  var    logic    [ lpN_STR_GEN     :1 ]
                  [ lpW_STR_GEN  -1 :0 ]    mstr__i_length           ;
  var    logic                              mstr__i_start            ;
  var    logic    [ lpN_STR_GEN     :0 ]    mstr__i_reset            ;

  //                                        generator

  var    logic    [ lpN_STR_GEN     :0 ]    mstr__o_start            ;
  var    logic    [ lpN_STR_GEN     :0 ]    mstr__o_stop             ;
  var    logic    [ lpN_STR_GEN     :0 ]    mstr__o_strob            ;

  //-------------------------------------// header / strob ----------|

  var    logic    [               2 :0 ]    str_prm                  ;
  var    logic    [               2 :0 ]    str_sfd                  ;
  var    logic    [               2 :0 ]    str_head                 ;
  var    logic    [               3 :0 ]    cnt_head                 ;
  var    logic    [               3 :0 ]    cnt_head_nxt         = 0 ;

  var    logic    [               2 :0 ]    str_prm_x                ;
  var    logic    [               2 :0 ]    str_sfd_x                ;
  var    logic    [               2 :0 ]    str_head_x               ;
  var    logic    [               3 :0 ]    cnt_head_x               ;

  //-------------------------------------// mac / strob -------------|

  var    logic    [               2 :0 ]    str_mda                  ;
  var    logic    [               2 :0 ]    str_msa                  ;
  var    logic    [               2 :0 ]    str_mlt                  ;
  var    logic    [               2 :0 ]    str_mac                  ;
  var    logic    [               3 :0 ]    cnt_mac                  ;
  var    logic    [               3 :0 ]    cnt_mac_nxt          = 0 ;

  //-------------------------------------// payload / strob ---------|

  var    logic    [               2 :0 ]    str_pay                  ;
  var    logic    [ pW_LAN_LEN   -1 :0 ]    cnt_pay                  ;
  var    logic    [ pW_LAN_LEN   -1 :0 ]    cnt_pay_nxt          = 0 ;

  //-------------------------------------// ip / strob --------------|

  var    logic    [               2 :0 ]    str_ip               = 0 ;
  var    logic    [ pW_LAN_LEN   -1 :0 ]    cnt_ip                   ;
  var    logic    [ pW_LAN_LEN   -1 :0 ]    cnt_ip_nxt           = 0 ;

  //-------------------------------------// fsc / strob -------------|

  var    logic    [               2 :0 ]    str_crc                  ;
  var    logic    [               3 :0 ]    cnt_crc                  ;
  var    logic    [               3 :0 ]    cnt_crc_nxt          = 0 ;

  //-------------------------------------// body / strob ------------|

  var    logic    [               2 :0 ]    str_body                 ;
  var    logic    [ pW_LAN_LEN   -1 :0 ]    cnt_body                 ;
  var    logic    [ pW_LAN_LEN   -1 :0 ]    cnt_body_nxt         = 0 ;

  //-------------------------------------// frame / strob -----------|

  var    logic    [               2 :0 ]    str_frame                ;
  var    logic    [ pW_LAN_LEN   -1 :0 ]    cnt_frame                ;
  var    logic    [ pW_LAN_LEN   -1 :0 ]    cnt_frame_nxt        = 0 ;

  //-------------------------------------// gap / strob -------------|

  var    logic    [               2 :0 ]    str_gap              = 0 ;
  var    logic    [ pW_LAN_GAP   -1 :0 ]    cnt_gap                  ;
  var    logic    [ pW_LAN_GAP   -1 :0 ]    cnt_gap_nxt          = 0 ;
  var    logic    [ pW_LAN_GAP   -1 :0 ]    cnt_gap_stop         = 0 ;

  //-------------------------------------// fsc / error -------------|

  var    logic                              crc_err              = 0 ;
  var    logic                              crc_err_nxt          = 0 ;
  var    logic                              crc_rdy                  ;
  var    logic                              crc_rdy_nxt          = 0 ;

  //-------------------------------------// counters ----------------|

  var    logic                              det_p_err                ;
  var    logic                              det_p_rdy                ;
  var    logic                              cnt_p_clr                ;
  var    logic                              cnt_p_rdy                ;
  var    logic                              cnt_p_rdy_nxt        = 0 ;
  var    logic    [ pN_LAN_CNT      :0 ]
                  [ pW_LAN_CNT   -1 :0 ]    cnt_p_all            = 0 ;
  var    logic    [ pN_LAN_CNT      :0 ]
                  [ pW_LAN_CNT   -1 :0 ]    cnt_p_err            = 0 ;

  var    logic                              i_cnt_p_clr_re           ;

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  //==========================================================================================================================================================================================================================//
  // body of module
  //==========================================================================================================================================================================================================================//

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // out logic (without out reg)
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  // lan packet / strob

  assign  o_str_frame  = str_frame  ;
  assign  o_cnt_frame  = cnt_frame  ;
  assign  o_str_head   = str_head_x ; // 'x'
  assign  o_cnt_head   = cnt_head_x ; // 'x'
  assign  o_str_prm    = str_prm_x  ; // 'x'
  assign  o_str_sfd    = str_sfd_x  ; // 'x'
  assign  o_str_body   = str_body   ;
  assign  o_cnt_body   = cnt_body   ;
  assign  o_str_mac    = str_mac    ;
  assign  o_cnt_mac    = cnt_mac    ;
  assign  o_str_mda    = str_mda    ;
  assign  o_str_msa    = str_msa    ;
  assign  o_str_mlt    = str_mlt    ;
  assign  o_str_pay    = str_pay    ;
  assign  o_cnt_pay    = cnt_pay    ;
  assign  o_str_ip     = str_ip     ;
  assign  o_cnt_ip     = cnt_ip     ;
  assign  o_str_crc    = str_crc    ;
  assign  o_cnt_crc    = cnt_crc    ;
  assign  o_str_gap    = str_gap    ;
  assign  o_cnt_gap    = cnt_gap    ;

  // lan packet / length

  assign  o_len_frame  = len_frame  ;
  assign  o_len_body   = len_body   ;
  assign  o_len_pay    = len_pay    ;
  assign  o_len_gap    = len_gap    ;

  // lan packet / fcs crc

  assign  o_crc_rdy    = crc_rdy    ;
  assign  o_crc_err    = crc_err    ;

  // lan counters

  assign  o_det_p_err  = det_p_err  ;
  assign  o_det_p_rdy  = det_p_rdy  ;

  assign  o_cnt_p_rdy  = cnt_p_rdy  ;
  assign  o_cnt_p_all  = cnt_p_all  ;
  assign  o_cnt_p_err  = cnt_p_err  ;

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // start of emulator / see 'comment.txt' (3)
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  // start / first

  assign  state_idle = (str_frame[0] == 0) & (str_gap[0] == 0); // emulator sleeps

  assign  start_first = i_ced & state_idle_re; // width = cen

  det_se // detector of rising edge

  #(.pS_EDGE  (1), // edge setting
    .pN_REG_I (0), // number of reg in  | register
    .pN_REG_O (1)) // number of reg out | register

  re__state_idle (.i_clk(i_clk), .i_clk_en(i_cen & i_ced), .i_sclr(~i_sclr_n), .i_strob(state_idle), .o_edge(state_idle_re) );
  //                                                                                    ^                    ^
  // start / next to (preparatory)

  assign  start_nxt = i_set_ini | str_gap[1]; // start bit of gap / width = cen

  // start / sync to frame

  assign  start = str_gap[2]; // stop bit of gap / width = cen

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // frame (packet) : length (bytes)
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  // length / next to

  always_ff @(posedge i_clk)
    if (~i_sclr_n)
      len_frame_nxt <= 0;
    else
      if (i_cen & i_ced) // period = 1 byte
        begin
          if (start_nxt) // preparatory

            case ({i_set_len_type})

              // length constant

              0: len_frame_nxt <= i_set_len_const;

              // length random

              1: len_frame_nxt <= i_set_len_min + ({$random} % (i_set_len_max - i_set_len_min));

              // length counter

              2: if (i_set_ini)

                   len_frame_nxt <= i_set_len_min; // initial value
                 else
                   begin
                     if (len_frame_nxt == i_set_len_max)
                         len_frame_nxt <= i_set_len_min;
                     else
                         len_frame_nxt <= len_frame_nxt + 1;
                   end

              // length default

              default len_frame_nxt <= 72; // min = 72 bytes (std. 802.3)

            endcase
        end

  // length / sync to frame

  always_ff @(posedge i_clk)
    if (~i_sclr_n)
      len_frame <= 0;
    else
      if (i_cen & i_ced)
        begin
          if (start)
            len_frame <= len_frame_nxt;
        end

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // frame body : length (bytes)
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  
  // length / constant

  assign  len_head =  8 ;
  assign  len_mac  = 14 ;
  assign  len_crc  =  4 ;

  // length / next to

  assign  len_body_nxt =                        ( len_frame_nxt - len_head                     );
  assign  len_pay_nxt  = (i_set_mac_ena == 1) ? ( len_frame_nxt - len_head - len_crc - len_mac ): // mac enable
                                                ( len_frame_nxt - len_head - len_crc           ); // mac disable
  // length / sync to frame

  always_ff @(posedge i_clk)
    if (~i_sclr_n)
      begin
        len_body <= 0;
        len_pay  <= 0;
      end
    else
      if (i_cen & i_ced)
        begin
          if (start) // sync to frame
            begin
              len_body <= len_body_nxt ;
              len_pay  <= len_pay_nxt  ;
            end
        end

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // frame gap : length (bytes)
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  // length / next to

  always_ff @(posedge i_clk)
    if (~i_sclr_n)
      len_gap_nxt <= 0;
    else
      if (i_cen & i_ced) // period = 1 byte
        begin
          if (start_nxt) // preparatory
            begin
              if (i_set_tx_mode) // transmit gap auto

                len_gap_nxt <= i_set_tx_gap;

              else // transmit gap manual

                case ({i_set_gap_type})

                  // gap constant

                  0: len_gap_nxt <= i_set_gap_const;

                  // gap random
  
                  1: len_gap_nxt <= i_set_gap_min + ({$random} % (i_set_gap_max - i_set_gap_min));

                  // gap counter

                  2: if (i_set_ini)

                       len_gap_nxt <= i_set_gap_min; // initial value
                     else
                       if (len_gap_nxt == i_set_gap_max)
                         begin
                           len_gap_nxt <= i_set_gap_min;
                         end
                       else
                         len_gap_nxt <= len_gap_nxt + 1;

                  // gap default

                  default len_gap_nxt <= 12; // min = 12 bytes (std. 802.3)

                endcase
            end
        end

  // length / sync to frame

  always_ff @(posedge i_clk)
    if (~i_sclr_n)
      len_gap <= 0;
    else
      if (i_cen & i_ced)
        begin
          if (start) // sync to frame
            len_gap <= len_gap_nxt;
        end

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // frame (packet) : multi strobes generator
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  // clock

  assign  mstr__i_clk                   = i_clk                 ;
  assign  mstr__i_clk_en                = i_cen & i_ced         ;
  assign  mstr__i_sclr                  = ~i_sclr_n             ;

  // control

  assign  mstr__i_start                 = start                 ; // sync
  assign  mstr__i_reset                 = 1'b0                  ;

  assign  mstr__i_length [lpN_STR_PRM]  = pE_LAN_HEAD   ? 7 : 0 ;
  assign  mstr__i_length [lpN_STR_SFD]  = pE_LAN_HEAD   ? 1 : 0 ;
  assign  mstr__i_length [lpN_STR_MDA]  = i_set_mac_ena ? 6 : 0 ;
  assign  mstr__i_length [lpN_STR_MSA]  = i_set_mac_ena ? 6 : 0 ;
  assign  mstr__i_length [lpN_STR_MLT]  = i_set_mac_ena ? 2 : 0 ;
  assign  mstr__i_length [lpN_STR_PAY]  = len_pay_nxt           ; // next to
  assign  mstr__i_length [lpN_STR_CRC]  = 4                     ;

  //---------------------------------------------------------------------|
                                         //                              |
  gen_mstr        mstr                   //                              |
  (                                      //                              |
  // clock ------------------------------//------------------------------|
                                         //           |                  |
     .i_clk       ( mstr__i_clk    ),    //  in       |  req             |
     .i_clk_en    ( mstr__i_clk_en ),    //  in       |       opt        |
     .i_sclr      ( mstr__i_sclr   ),    //  in       |       opt        |
                                         //           |                  |
  // control ----------------------------//-----------|------------------|
                                         //           |                  |
     .i_start     ( mstr__i_start  ),    //  in       |  req             |
     .i_length    ( mstr__i_length ),    //  in       |  req             |
     .i_reset     ( mstr__i_reset  ),    //  in       |       opt        |
                                         //           |                  |
  // generator --------------------------//-----------|------------------|
                                         //           |                  |
     .o_start     ( mstr__o_start  ),    //      out  |       opt        |
     .o_stop      ( mstr__o_stop   ),    //      out  |       opt        |
     .o_strob     ( mstr__o_strob  )     //      out  |  req             |
                                         //           |                  |
  );//-------------------------------------------------------------------|
  
  // mode setting

  defparam  mstr.pE_AUTO  = 0; // def: 0

  // strob setting

  defparam  mstr.pN_STR   = lpN_STR_GEN ;
  defparam  mstr.pW_LEN   = lpW_STR_GEN ;

  // in/out registers

  defparam  mstr.pN_REG_I = 0; // def: 0
  defparam  mstr.pN_REG_O = 0; // def: 0

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // frame header : strob generator
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  // preamble / strob

  assign  str_prm  [0] = mstr__o_strob [ lpN_STR_PRM ]         ; // strob
  assign  str_prm  [1] = mstr__o_start [ lpN_STR_PRM ] & i_ced ; // start bit / width = cen
  assign  str_prm  [2] = mstr__o_stop  [ lpN_STR_PRM ] & i_ced ; // stop  bit / width = cen
  
  // sfd / strob
  
  assign  str_sfd  [0] = mstr__o_strob [ lpN_STR_SFD ]         ; // strob
  assign  str_sfd  [1] = mstr__o_start [ lpN_STR_SFD ] & i_ced ; // start bit / width = cen
  assign  str_sfd  [2] = mstr__o_stop  [ lpN_STR_SFD ] & i_ced ; // stop  bit / width = cen
  
  // header / strob
 
  assign  str_head [0] = mstr__o_strob [ lpN_STR_PRM ] |            
                         mstr__o_strob [ lpN_STR_SFD ]         ; // strob
  assign  str_head [1] = mstr__o_start [ lpN_STR_PRM ] & i_ced ; // start bit / width = cen
  assign  str_head [2] = mstr__o_stop  [ lpN_STR_SFD ] & i_ced ; // stop  bit / width = cen

  // header / strob counter up

  always_ff @(posedge i_clk)
    if (~i_sclr_n)
      cnt_head_nxt <= 4'd0;
    else
      if (i_cen & i_ced) // period = 1 byte
        begin
          if (str_head[1]) // start bit
            cnt_head_nxt <= 1;
          else
            begin
              if (str_head[0]) // strob
                cnt_head_nxt <= cnt_head_nxt + 1;
              else
                cnt_head_nxt <= 4'd0;
            end
        end  

  assign  cnt_head = (str_head[0] == 1) ? cnt_head_nxt : 0; // nulling for strob outside

  // header / nulling (setting)

  assign  str_head_x  = (pE_LAN_HEAD == 0) ? 0 : str_head ;
  assign  cnt_head_x  = (pE_LAN_HEAD == 0) ? 0 : cnt_head ;
  assign  str_prm_x   = (pE_LAN_HEAD == 0) ? 0 : str_prm  ;
  assign  str_sfd_x   = (pE_LAN_HEAD == 0) ? 0 : str_sfd  ;

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // mac header : strob generator
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  // mda / strob
  
  assign  str_mda [0] = (i_set_mac_ena == 1) & mstr__o_strob [ lpN_STR_MDA ]         ; // strob
  assign  str_mda [1] = (i_set_mac_ena == 1) & mstr__o_start [ lpN_STR_MDA ] & i_ced ; // start bit / width = cen
  assign  str_mda [2] = (i_set_mac_ena == 1) & mstr__o_stop  [ lpN_STR_MDA ] & i_ced ; // stop  bit / width = cen

  // msa / strob
  
  assign  str_msa [0] = (i_set_mac_ena == 1) & mstr__o_strob [ lpN_STR_MSA ]         ; // strob
  assign  str_msa [1] = (i_set_mac_ena == 1) & mstr__o_start [ lpN_STR_MSA ] & i_ced ; // start bit / width = cen
  assign  str_msa [2] = (i_set_mac_ena == 1) & mstr__o_stop  [ lpN_STR_MSA ] & i_ced ; // stop  bit / width = cen
  
  // mlt / strob
  
  assign  str_mlt [0] = (i_set_mac_ena == 1) & mstr__o_strob [ lpN_STR_MLT ]         ; // strob
  assign  str_mlt [1] = (i_set_mac_ena == 1) & mstr__o_start [ lpN_STR_MLT ] & i_ced ; // start bit / width = cen
  assign  str_mlt [2] = (i_set_mac_ena == 1) & mstr__o_stop  [ lpN_STR_MLT ] & i_ced ; // stop  bit / width = cen

  // mac / strob

  assign  str_mac [0] = (i_set_mac_ena == 1) & mstr__o_strob [ lpN_STR_MDA ] |
                                               mstr__o_strob [ lpN_STR_MSA ] |
                                               mstr__o_strob [ lpN_STR_MLT ]         ; // strob
  assign  str_mac [1] = (i_set_mac_ena == 1) & mstr__o_start [ lpN_STR_MDA ] & i_ced ; // start bit / width = cen
  assign  str_mac [2] = (i_set_mac_ena == 1) & mstr__o_stop  [ lpN_STR_MLT ] & i_ced ; // stop  bit / width = cen

  // mac / strob counter up
  
  always_ff @(posedge i_clk)
    if (~i_sclr_n)
      cnt_mac_nxt <= 4'd0;
    else
      if (i_cen & i_ced) // period = 1 byte
        begin
          if (str_mac[1]) // start bit
            cnt_mac_nxt <= 1;
          else
            begin
              if (str_mac[0]) // strob
                cnt_mac_nxt <= cnt_mac_nxt + 1;
              else
                cnt_mac_nxt <= 4'd0;
            end
        end

  assign  cnt_mac = (str_mac[0] == 1) ? cnt_mac_nxt : 0; // nulling for strob outside

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // payload : strob generator
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  // strob

  assign  str_pay [0] = mstr__o_strob [ lpN_STR_PAY ]         ; // strob
  assign  str_pay [1] = mstr__o_start [ lpN_STR_PAY ] & i_ced ; // start bit / width = cen
  assign  str_pay [2] = mstr__o_stop  [ lpN_STR_PAY ] & i_ced ; // stop  bit / width = cen
  
  // strob counter up

  always_ff @(posedge i_clk)
    if (~i_sclr_n)
      cnt_pay_nxt <= 0;
    else
      if (i_cen & i_ced) // period = 1 byte
        begin
          if (str_mlt[2]) // stop bit
            cnt_pay_nxt <= 1;
          else
            begin
              if (str_pay[0]) // strob
                cnt_pay_nxt <= cnt_pay_nxt + 1;
              else
                cnt_pay_nxt <= 0;
            end
        end
            
  assign  cnt_pay = (str_pay[0] == 1) ? cnt_pay_nxt : 0; // nulling for strob outside

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // ip header : strob generator
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  // start & stop bit
  
  always_comb
    begin
      str_ip[1] <= (i_set_ip_ena == 1) & ((pN_LAN_IPL == 1) ? str_pay[1]   : (cnt_pay == pN_LAN_IPL-1)); // start bit / width = cen
      str_ip[2] <= (i_set_ip_ena == 1) & ((pN_LAN_IPH == 1) ? cnt_pay == 1 : (cnt_pay == pN_LAN_IPH  )); // stop  bit / width = cen
    end

  // strob

  always_ff @(posedge i_clk)
    if (~i_sclr_n)
      str_ip[0] <= 1'b0;
    else
      if (i_cen & i_ced) // period = 1 byte
        begin
          if (str_ip[1]) // start bit
            str_ip[0] <= 1'b1;
          else
            if (str_ip[2]) // stop bit
              str_ip[0] <= 1'b0;
        end  

  // strob counter up

  always_ff @(posedge i_clk)
    if (~i_sclr_n)
      cnt_ip_nxt <= 0;
    else
      if (i_cen & i_ced) // period = 1 byte
        begin
          if (str_ip[1]) // start bit
            cnt_ip_nxt <= 1;
          else
            begin
              if (str_ip[0]) // strob
                cnt_ip_nxt <= cnt_ip_nxt + 1;
              else
                cnt_ip_nxt <= 0;
            end
        end

  assign  cnt_ip = (str_ip[0] == 1) ? cnt_ip_nxt : 0; // nulling for strob outside

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // fsc crc : strob generator
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  // strob

  assign  str_crc [0] = mstr__o_strob [ lpN_STR_CRC ]         ; // strob
  assign  str_crc [1] = mstr__o_start [ lpN_STR_CRC ] & i_ced ; // start bit / width = cen
  assign  str_crc [2] = mstr__o_stop  [ lpN_STR_CRC ] & i_ced ; // stop  bit / width = cen

  // strob counter up

  always_ff @(posedge i_clk)
    if (~i_sclr_n)
      cnt_crc_nxt <= 3'd0;
    else
      if (i_cen & i_ced) // period = 1 byte
        begin
          if (str_pay[2]) // stop bit
            cnt_crc_nxt <= 1;
          else
            begin
              if (str_crc[0]) // strob
                cnt_crc_nxt <= cnt_crc_nxt + 1;
              else
                cnt_crc_nxt <= 3'd0;
            end
        end

  assign  cnt_crc = (str_crc[0] == 1) ? cnt_crc_nxt : 0; // nulling for strob outside

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // frame body : strob generator
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  // strob

  assign  str_body  [0] = mstr__o_strob [ lpN_STR_MDA ] |
                          mstr__o_strob [ lpN_STR_MSA ] |
                          mstr__o_strob [ lpN_STR_MLT ] |
                          mstr__o_strob [ lpN_STR_PAY ] |
                          mstr__o_strob [ lpN_STR_CRC ]         ; // strob
  assign  str_body  [1] = mstr__o_start [ lpN_STR_MDA ] & i_ced ; // start bit / width = cen
  assign  str_body  [2] = mstr__o_stop  [ lpN_STR_CRC ] & i_ced ; // stop  bit / width = cen
  
  // strob counter up

  always_ff @(posedge i_clk)
    if (~i_sclr_n)
      cnt_body_nxt <= 0;
    else
      if (i_cen & i_ced) // period = 1 byte
        begin
          if (str_body[1]) // start bit
            cnt_body_nxt <= 1;
          else
            begin
              if (str_body[0]) // strob
                cnt_body_nxt <= cnt_body_nxt + 1;
              else
                cnt_body_nxt <= 0;
            end
        end

  assign  cnt_body = (str_body[0] == 1) ? cnt_body_nxt : 0; // nulling for strob outside

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // frame (packet) : strob generator
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  
  // strob
  
  assign  str_frame [0] = mstr__o_strob [ lpN_STR_PRM ] |
                          mstr__o_strob [ lpN_STR_SFD ] |
                          mstr__o_strob [ lpN_STR_MDA ] |
                          mstr__o_strob [ lpN_STR_MSA ] |
                          mstr__o_strob [ lpN_STR_MLT ] |
                          mstr__o_strob [ lpN_STR_PAY ] |
                          mstr__o_strob [ lpN_STR_CRC ]         ; // strob
  assign  str_frame [1] = mstr__o_start [ lpN_STR_PRM ] & i_ced ; // start bit / width = cen
  assign  str_frame [2] = mstr__o_stop  [ lpN_STR_CRC ] & i_ced ; // stop  bit / width = cen

  // strob counter up
  
  always_ff @(posedge i_clk)
    if (~i_sclr_n)
      cnt_frame_nxt <= 0;
    else
      if (i_cen & i_ced) // period = 1 byte
        begin
          if (str_frame[1]) // start bit
            cnt_frame_nxt <= 1;
          else
            begin
              if (str_frame[0]) // strob
                cnt_frame_nxt <= cnt_frame_nxt + 1;
              else
                cnt_frame_nxt <= 0;
            end
        end

  assign  cnt_frame = (str_frame[0] == 1) ? cnt_frame_nxt : 0; // nulling for strob outside

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // frame gap : strob generator
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  // start & stop bits

  always_comb
    begin
      str_gap[1] <= i_ced & str_crc[2] | start_first       ; // start bit / width = cen
      str_gap[2] <= i_ced & str_gap[0] & cnt_gap_stop == 0 ; // stop  bit / width = cen
    end

  // stop bit / counter down

  always_ff @(posedge i_clk)
    if (~i_sclr_n)
      cnt_gap_stop <= 3'd0;
    else
      if (i_cen & i_ced) // period = 1 byte
        begin
          if (str_gap[1]) // start bit
            begin
              if (start_first)
                cnt_gap_stop <= 3'd4; // first gap = 4 byte
              else
                cnt_gap_stop <= len_gap_nxt - 1;
            end
          else
            if (i_set_io_pause == 0) // enable
              begin
                if (cnt_gap_stop != 0)
                  cnt_gap_stop <= cnt_gap_stop - 1;
              end
        end

  // strob

  always_ff @(posedge i_clk)
    if (~i_sclr_n)
      str_gap[0] <= 1'b0;
    else
      if (i_cen & i_ced) // period = 1 byte
        begin
          if (str_gap[1]) // start bit
            str_gap[0] <= 1'b1;
          else
            if (str_gap[2]) // stop bit
              str_gap[0] <= 1'b0;
        end

  // strob counter up

  always_ff @(posedge i_clk)
    if (~i_sclr_n)
      cnt_gap_nxt  <= 0;
    else
      if (i_cen & i_ced) // period = 1 byte
        begin
          if (str_gap[1]) // start bit
            cnt_gap_nxt  <= 1;
          else
            if (i_set_io_pause == 0) // enable
              begin
                if (str_gap[0]) // strob
                  cnt_gap_nxt <= cnt_gap_nxt + 1;
                else
                  cnt_gap_nxt <= 0;
            end
        end

  assign  cnt_gap = (str_gap[0] == 1) ? cnt_gap_nxt : 0; // nulling for strob outside

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // fcs crc : error emulate
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  // error / next to

  always_ff @(posedge i_clk)
    if (~i_sclr_n)
      crc_err_nxt <= 1'b0;
    else
      if (i_cen & i_ced) // period = 1 byte
        begin
          if (i_set_err_ena == 0)
            crc_err_nxt <= 1'b0;
          else
            if (start_nxt)
              crc_err_nxt <= ({$random} % 100) < i_set_err_per; // random value (0 to 100%)
        end

  // error / sync to frame

  always_ff @(posedge i_clk)
    if (~i_sclr_n)
      crc_err <= 1'b0;
    else
      if (i_cen & i_ced) // period = 1 byte
        begin
          if (start) // sync to frame
            crc_err <= crc_err_nxt;
        end

  // error / ready
  
  always_ff @(posedge i_clk)
    if (~i_sclr_n)
      crc_rdy_nxt <= 1'b0;
    else
      if (i_cen & i_ced) // period = 1 byte
        begin
          crc_rdy_nxt <= start; // sync to frame
        end

  assign  crc_rdy = i_ced & crc_rdy_nxt; // width = cen

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // packet counters : detector
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  assign  det_p_rdy = crc_rdy; // repeater
  assign  det_p_err = crc_err; // repeater

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // packet counters : clear
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  assign  cnt_p_clr = i_ced & i_cnt_p_clr_re; // width = cen
    
  det_se // detector of rising edge

  #(.pS_EDGE  (1), // edge setting
    .pN_REG_I (1), // number of reg in  | register
    .pN_REG_O (0)) // number of reg out | register

  re__i_cnt_p_clr (.i_clk(i_clk), .i_clk_en(i_cen & i_ced), .i_sclr(~i_sclr_n), .i_strob(i_cnt_p_clr), .o_edge(i_cnt_p_clr_re) );
  //                                                                                     ^                     ^
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // packet counters : ready
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  always_ff @(posedge i_clk)
    if (~i_sclr_n)
      cnt_p_rdy_nxt <= 1'b0;
    else
      if (i_cen & i_ced)
        begin
          cnt_p_rdy_nxt <= start; // sync to frame
        end

  assign  cnt_p_rdy = i_ced & cnt_p_rdy_nxt; // width = cen

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // packet counters : packet all
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  always_ff @(posedge i_clk)
    if (~i_sclr_n)
      cnt_p_all <= '0;
    else
      if (i_cen & i_ced)
        begin
          if (cnt_p_clr)
            cnt_p_all <= '0;
          else
            if (start) // sync to frame
              begin
                cnt_p_all[0] <= cnt_p_all[0] + 1; // [0]: all size frame

                for (int i=1; i<= pN_LAN_CNT; i++)
                  begin
                    if ((len_body_nxt >= i_set_p_low[i]) & ((len_body_nxt <= i_set_p_top[i]) | i_set_p_top[i] == 0)) // '=0' used for oversize
                      begin
                        cnt_p_all[i] <= cnt_p_all[i] + 1;
                      end
                  end
              end
        end

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // packet counters : packet error
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  always_ff @(posedge i_clk)
    if (~i_sclr_n)
      cnt_p_err <= '0;
    else
      if (i_cen & i_ced)
        begin
          if (cnt_p_clr)
            cnt_p_err <= '0;
          else
            if (start & crc_err_nxt) // sync to frame
              begin
                cnt_p_err[0] <= cnt_p_err[0] + 1; // [0]: all size frame

                for (int i=1; i<= pN_LAN_CNT; i++)
                  begin
                    if ((len_body_nxt >= i_set_p_low[i]) & ((len_body_nxt <= i_set_p_top[i]) | i_set_p_top[i] == 0)) // '=0' used for oversize
                      begin
                        cnt_p_err[i] <= cnt_p_err[i] + 1;
                      end
                  end
              end
        end

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  endmodule




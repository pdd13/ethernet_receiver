  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Company      : Micran, Department of Telecommunications
  //
  //  Engineer     : Pogodaev Danil
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Library      : lib_ethernet / library of ethernet components
  //
  //  Component    : eth_rx_v2 / receiver of ethernet frames (packets)
  //
  //  File Name    : tb_gen .sv (testbench)
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Description  : Testbench generator for module 'eth_emu'.
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Attached (1) : Additional (detailed) code comments / tb_eth_emu_v2 - comment .txt
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Include Files
  //  
  //  Category     Type       Name                      Ver     Description
  //
  //  top_tb       include    eth_emu__par_set   .svh   2.0     top testbench / module 'eth_emu' / parameter setting
  //  top_tb       include    eth_emu__par_local .svh   2.0     top testbench / module 'eth_emu' / parameter local
  //
  //  lib_global   function   cbit               .sv    1.0.0   bits calculator for input number (round up)
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Revision List
  //
  //  Version   Date       Author
  //
  //  2.0       26.09.16   Pogodaev Danil
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  `timescale 1ns/1ns

  module tb_gen_emu       ( // for 'eth_emu'

  //---- reset -----------|
    
         i_reset_n        ,

  //---- setting ---------|

  //     update

         i_set_upd        ,
         o_set_upd_ena    ,

  //     interface
     
         i_set_io_type    ,
         i_set_io_speed   ,
         i_set_io_pause   ,

  //     transmit speed

         i_set_tx_mode    ,
         i_set_tx_speed   ,
         o_set_tx_speed   ,
         o_set_tx_limit   ,
         i_set_tx_sel     ,

  //     crc error

         i_set_err_ena    ,
         i_set_err_per    ,

  //     mac header

         i_set_mac_ena    ,

         i_set_mda_type   ,
         i_set_mda_const  ,
         i_set_mda_min    ,
         i_set_mda_max    ,

         i_set_msa_type   ,
         i_set_msa_const  ,
         i_set_msa_min    ,
         i_set_msa_max    ,

         i_set_mlt_type   ,
         i_set_mlt_const  ,
         i_set_mlt_min    ,
         i_set_mlt_max    ,

  //     ip header

         i_set_ip_ena     , 
         i_set_ip_type    ,
         i_set_ip_const   ,
         i_set_ip_min     ,
         i_set_ip_max     ,

  //     data payload

         i_set_pay_type   ,
         i_set_pay_const  ,
         i_set_pay_min    ,
         i_set_pay_max    ,

  //     frame length

         i_set_len_type   ,
         i_set_len_const  ,
         i_set_len_min    ,
         i_set_len_max    ,

  //     frame gap

         i_set_gap_type   ,
         i_set_gap_const  ,
         i_set_gap_min    ,
         i_set_gap_max    ,

  //---- lan packet ------|

  //     clock

         o_lan_clk        ,
         o_lan_cen        ,
         o_lan_ced        ,

  //     strob

         o_lan_str_frame  ,
         o_lan_cnt_frame  ,
         o_lan_str_head   ,
         o_lan_cnt_head   ,
         o_lan_str_prm    ,
         o_lan_str_sfd    ,
         o_lan_str_body   ,
         o_lan_cnt_body   ,
         o_lan_str_mac    ,
         o_lan_cnt_mac    ,
         o_lan_str_mda    ,
         o_lan_str_msa    ,
         o_lan_str_mlt    ,
         o_lan_str_pay    ,
         o_lan_cnt_pay    ,
         o_lan_str_ip     ,
         o_lan_cnt_ip     ,
         o_lan_str_crc    ,
         o_lan_cnt_crc    ,
         o_lan_str_gap    ,
         o_lan_cnt_gap    ,

  //     data

         o_lan_dat_load   ,
         o_lan_dat_frame  ,

  //     length

         o_lan_len_frame  ,
         o_lan_len_body   ,
         o_lan_len_pay    ,
         o_lan_len_gap    ,

  //     fcs crc

         o_lan_crc_rdy    ,
         o_lan_crc_err    ,

  //---- counters --------|

  //     setting

         i_lan_set_p_top  ,
         i_lan_set_p_low  ,

  //     detector

         o_lan_det_p_rdy  ,
         o_lan_det_p_err  ,

  //     counters

         i_lan_cnt_p_clr  ,
         o_lan_cnt_p_rdy  ,
         o_lan_cnt_p_all  ,
         o_lan_cnt_p_err  ,
 
  //---- lan interface ---|

  //     lan mii
  
         o_mii_txd        ,
         o_mii_tx_en      ,
         o_mii_tx_clk     ,

  //     lan rmii

         o_rmii_txd       ,
         o_rmii_tx_en     ,
         o_rmii_tx_clk    ,
         o_rmii_tx_clk_s  ,

  //     lan gmii

         o_gmii_txd       ,
         o_gmii_tx_en     ,
         o_gmii_tx_clk   );

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // include files : functions
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
                                                                              //      |
  `include "cbit.sv"                                                          // req  | bits calculator for input number (round up)
                                                                              //      |
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // include files : parameters
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
                                                                              //      |
  // emulator of ethernet frames                                              //------|---------------------------------------------------------------------------------------------------------------------------------------//
                                                                              //      |
  `include "eth_emu__par_set.svh"                                             // req  | top testbench / module 'eth_emu' / parameter setting
  `include "eth_emu__par_local.svh"                                           // req  | top testbench / module 'eth_emu' / parameter local
                                                                              //      |
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // interface
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
                                                                              //      |                                                 |          |           |
  //----------------------------------------------// reset -------------------//------|-------------------------------------------------|----------|-----------|--------------------------------------------------------------//
                                                                              //      |                                                 |          |           |
  output logic                                       i_reset_n           ;    // opt  | sync clear (ative low)                          | strob    | def: 1    | info: used 'lan_clk' (input reg)
                                                                              //      |                                                 |          |           |
  //----------------------------------------------// setting -----------------//------|-------------------------------------------------|----------|-----------|--------------------------------------------------------------//
                                                                              //      |                                                 |          |           |
  //                                                 update                   //------|-------------------------------------------------|----------|-----------|--------------------------------------------------------------//
                                                                              //      |                                                 |          |           |
  output logic                                       i_set_upd           ;    // opt  | setting : update request                        | pulse    | def: 0    | info: sensitivity to rising edge ; see 'comment.txt' (3)
  input  logic                                       o_set_upd_ena       ;    // opt  | setting : update enable                         | pulse    |           | width: lan_cen
                                                                              //      |                                                 |          |           |
  //                                                 interface                //------|-------------------------------------------------|----------|-----------|--------------------------------------------------------------//
                                                                              //      |                                                 |          |           |
  output logic    [                        1 :0 ]    i_set_io_type       ;    // req  | setting : transmit type                         | pdata    |           |
                                                                              //      |       0 : transmit gmii                         |          |           |
                                                                              //      |       1 : transmit rmii                         |          |           |
                                                                              //      |       2 : transmit mii                          |          |           |
                                                                              //      |                                                 |          |           |
  output logic    [                        1 :0 ]    i_set_io_speed      ;    // req  | setting : transmit speed                        | pdata    |           |
                                                                              //      |       0 : 1000 mbit/s                           |          |           |
                                                                              //      |       1 : 100  mbit/s                           |          |           |
                                                                              //      |       2 : 10   mbit/s                           |          |           |
                                                                              //      |                                                 |          |           |
  output logic                                       i_set_io_pause      ;    // opt  | setting : transmit pause                        | strob    | def: 0    | info: flow control ; see 'comment.txt' (4)
                                                                              //      |       0 : transmit enable                       |          |           |
                                                                              //      |       1 : transmit disable                      |          |           |
                                                                              //      |                                                 |          |           |
  //                                                 transmit speed           //------|-------------------------------------------------|----------|-----------|--------------------------------------------------------------//
                                                                              //      |                                                 |          |           |
  output logic                                       i_set_tx_mode       ;    // opt  | setting : transmit mode                         | strob    | def: 0    | info: see 'comment.txt' (5)
                                                                              //      |       0 : transmit gap manual                   |          |           | info: used 'i_set_gap_type'
                                                                              //      |       1 : transmit gap auto                     |          |           | info: used 'i_set_tx_speed'
                                                                              //      |                                                 |          |           |
  output logic    [                       19 :0 ]    i_set_tx_speed      ;    // opt  | setting : transmit speed (kbit/sec) / set       | pdata    |           | value: 0-1000000 kbit/sec
  input  logic    [                       19 :0 ]    o_set_tx_speed      ;    // req  | setting : transmit speed (kbit/sec) / real      | pdata    |           | value: 0-1000000 kbit/sec
  input  logic    [                       19 :0 ]    o_set_tx_limit      ;    // opt  | setting : transmit speed (kbit/sec) / limit     | pdata    |           | value: 0-1000000 kbit/sec ; info: see 'comment.txt' (6)
                                                                              //      |                                                 |          |           |
  output logic    [                        1 :0 ]    i_set_tx_sel        ;    // opt  | setting : transmit speed selection              | pdata    | def: 0    |
                                                                              //      |                                                 |          |           |
                                                                              //      |       0 : payload                               |          |           |
                                                                              //      |       1 : payload + mac                         |          |           |
                                                                              //      |       2 : payload + mac + fcs crc               |          |           |
                                                                              //      |       3 : payload + mac + fcs crc + head        |          |           |
                                                                              //      |                                                 |          |           |
  //                                                 crc error                //------|-------------------------------------------------|----------|-----------|--------------------------------------------------------------//
                                                                              //      |                                                 |          |           |
  output logic                                       i_set_err_ena       ;    // opt  | setting : crc error                             | strob    | def: 0    |
                                                                              //      |       0 : crc error insert disable              |          |           |
                                                                              //      |       1 : crc error insert enable               |          |           | info: crc = 32'h ffffffff
                                                                              //      |                                                 |          |           |
  output logic    [                        6 :0 ]    i_set_err_per       ;    // opt  | setting : crc error probability (0-100%)        | pdata    | def: 0    | info: used $random
                                                                              //      |                                                 |          |           |  
  //                                                 mac header               //------|-------------------------------------------------|----------|-----------|--------------------------------------------------------------//
                                                                              //      |                                                 |          |           |
  output logic                                       i_set_mac_ena       ;    // opt  | setting : mac header                            | strob    | def: 0    |
                                                                              //      |       0 : mac inserter disable                  |          |           |
                                                                              //      |       1 : mac inserter enable                   |          |           |
                                                                              //      |                                                 |          |           |  
  //                                                 mac header / mda         //------|-------------------------------------------------|----------|-----------|--------------------------------------------------------------//
                                                                              //      |                                                 |          |           |
  output logic    [                        2 :0 ]    i_set_mda_type      ;    // opt  | setting : mda type                              | pdata    | def: 0    |
                                                                              //      |       0 : mda constant                          |          |           | value: set_mda_const
                                                                              //      |       1 : mda random                            |          |           | value: set_mda_min to set_mda_max
                                                                              //      |       2 : mda counter of byte (individual)      |          |           | value: set_mda_min to set_mda_max
                                                                              //      |       3 : mda counter of byte (continuous)      |          |           | value: set_mda_min to set_mda_max
                                                                              //      |       4 : mda counter of packets                |          |           | value: set_mda_min to set_mda_max
                                                                              //      |                                                 |          |           |
  output logic    [                       47 :0 ]    i_set_mda_const     ;    // opt  | setting : mda constant / value                  | pdata    |           |
  output logic    [                       47 :0 ]    i_set_mda_min       ;    // opt  | setting : mda minimum  / value                  | pdata    | def: '0   |
  output logic    [                       47 :0 ]    i_set_mda_max       ;    // opt  | setting : mda maximum  / value                  | pdata    | def: '1   |
                                                                              //      |                                                 |          |           |  
  //                                                 mac header / msa         //------|-------------------------------------------------|----------|-----------|--------------------------------------------------------------//
                                                                              //      |                                                 |          |           |
  output logic    [                        2 :0 ]    i_set_msa_type      ;    // opt  | setting : msa type                              | pdata    | def: 0    |
                                                                              //      |       0 : msa constant                          |          |           | value: set_msa_const
                                                                              //      |       1 : msa random                            |          |           | value: set_msa_min to set_msa_max
                                                                              //      |       2 : msa counter of byte (individual)      |          |           | value: set_msa_min to set_msa_max
                                                                              //      |       3 : msa counter of byte (continuous)      |          |           | value: set_msa_min to set_msa_max
                                                                              //      |       4 : msa counter of packets                |          |           | value: set_msa_min to set_msa_max
                                                                              //      |                                                 |          |           |
  output logic    [                       47 :0 ]    i_set_msa_const     ;    // opt  | setting : msa constant / value                  | pdata    |           |
  output logic    [                       47 :0 ]    i_set_msa_min       ;    // opt  | setting : msa minimum  / value                  | pdata    | def: '0   |
  output logic    [                       47 :0 ]    i_set_msa_max       ;    // opt  | setting : msa maximum  / value                  | pdata    | def: '1   |
                                                                              //      |                                                 |          |           |  
  //                                                 mac header / mlt         //------|-------------------------------------------------|----------|-----------|--------------------------------------------------------------//
                                                                              //      |                                                 |          |           |
  output logic    [                        2 :0 ]    i_set_mlt_type      ;    // opt  | setting : mlt type                              | pdata    | def: 5    |
                                                                              //      |       0 : mlt constant                          |          |           | value: set_mlt_const
                                                                              //      |       1 : mlt random                            |          |           | value: set_mlt_min to set_mlt_max
                                                                              //      |       2 : mlt counter of byte (individual)      |          |           | value: set_mlt_min to set_mlt_max
                                                                              //      |       3 : mlt counter of byte (continuous)      |          |           | value: set_mlt_min to set_mlt_max
                                                                              //      |       4 : mlt counter of packets                |          |           | value: set_mlt_min to set_mlt_max
                                                                              //      |       5 : mlt length  of payload                |          |           | value: lan_len_pay
                                                                              //      |                                                 |          |           |
                                                                              //      |                                                 |          |           |
  output logic    [                       15 :0 ]    i_set_mlt_const     ;    // opt  | setting : mlt constant / value                  | pdata    |           |
  output logic    [                       15 :0 ]    i_set_mlt_min       ;    // opt  | setting : mlt minimum  / value                  | pdata    | def: '0   |
  output logic    [                       15 :0 ]    i_set_mlt_max       ;    // opt  | setting : mlt maximum  / value                  | pdata    | def: '1   |
                                                                              //      |                                                 |          |           |
  //                                                 ip header                //------|-------------------------------------------------|----------|-----------|--------------------------------------------------------------//
                                                                              //      |                                                 |          |           |
  output logic                                       i_set_ip_ena        ;    // opt  | setting : ip header                             | strob    | def: 0    |
                                                                              //      |       0 : ip insert disable                     |          |           |
                                                                              //      |       1 : ip insert enable                      |          |           |
                                                                              //      |                                                 |          |           |
  output logic    [                        2 :0 ]    i_set_ip_type       ;    // opt  | setting : ip type                               | pdata    | def: 0    |
                                                                              //      |       0 : ip constant                           |          |           | value: set_ip_const
                                                                              //      |       1 : ip random                             |          |           | value: set_ip_min to set_ip_max
                                                                              //      |       2 : ip counter of byte (individual)       |          |           | value: set_ip_min to set_ip_max
                                                                              //      |       3 : ip counter of byte (continuous)       |          |           | value: set_ip_min to set_ip_max
                                                                              //      |       4 : ip counter of packets                 |          |           | value: set_ip_min to set_ip_max
                                                                              //      |                                                 |          |           |
  output logic    [ ETH_EMU__lpW_LAN_IP   -1 :0 ]    i_set_ip_const      ;    // opt  | setting : ip constant                           | pdata    |           |
  output logic    [ ETH_EMU__lpW_LAN_IP   -1 :0 ]    i_set_ip_min        ;    // opt  | setting : ip minimum                            | pdata    |           |
  output logic    [ ETH_EMU__lpW_LAN_IP   -1 :0 ]    i_set_ip_max        ;    // opt  | setting : ip maximum                            | pdata    |           |
                                                                              //      |                                                 |          |           |
  //                                                 payload                  //------|-------------------------------------------------|----------|-----------|--------------------------------------------------------------//
                                                                              //      |                                                 |          |           |
  output logic    [                        2 :0 ]    i_set_pay_type      ;    // req  | setting : payload type                          | pdata    | def: 0    |
                                                                              //      |       0 : payload constant                      |          |           | value: set_pay_const
                                                                              //      |       1 : payload random                        |          |           | value: set_pay_min to set_pay_max
                                                                              //      |       2 : payload counter of byte (individual)  |          |           | value: set_pay_min to set_pay_max
                                                                              //      |       3 : payload counter of byte (continuous)  |          |           | value: set_pay_min to set_pay_max
                                                                              //      |       4 : payload counter of packets            |          |           | value: set_pay_min to set_pay_max
                                                                              //      |                                                 |          |           |
  output logic    [ ETH_EMU__pW_LAN_PAY   -1 :0 ]    i_set_pay_const     ;    // req  | setting : payload constant / value              | pdata    |           |
  output logic    [                        7 :0 ]    i_set_pay_min       ;    // opt  | setting : payload minimum  / value              | pdata    | def: '0   |
  output logic    [                        7 :0 ]    i_set_pay_max       ;    // opt  | setting : payload maximum  / value              | pdata    | def: '1   |
                                                                              //      |                                                 |          |           |
  //                                                 frame length             //------|-------------------------------------------------|----------|-----------|--------------------------------------------------------------//
                                                                              //      |                                                 |          |           |
  output logic     [                       1 :0 ]    i_set_len_type      ;    // req  | setting : length type                           | pdata    | def: 0    |
                                                                              //      |       0 : length constant                       |          |           | value: set_len_const
                                                                              //      |       1 : length random                         |          |           | value: set_len_min to set_len_max
                                                                              //      |       2 : length counter                        |          |           | value: set_len_min to set_len_max
                                                                              //      |                                                 |          |           |
  output logic    [ ETH_EMU__pW_LAN_LEN   -1 :0 ]    i_set_len_const     ;    // req  | setting : length constant / value (byte)        | pdata    |           |
  output logic    [ ETH_EMU__pW_LAN_LEN   -1 :0 ]    i_set_len_min       ;    // opt  | setting : length minimum  / value (byte)        | pdata    | def: 72   |
  output logic    [ ETH_EMU__pW_LAN_LEN   -1 :0 ]    i_set_len_max       ;    // opt  | setting : length maximum  / value (byte)        | pdata    | def: 1526 | value: 9018 (for jumbo-frame)
                                                                              //      |                                                 |          |           |
  //                                                 frame gap                //------|-------------------------------------------------|----------|-----------|--------------------------------------------------------------//
                                                                              //      |                                                 |          |           |
  output logic    [                        1 :0 ]    i_set_gap_type      ;    // req  | setting : gap type                              | pdata    | def: 0    |
                                                                              //      |       0 : gap constant                          |          |           | value: set_gap_const
                                                                              //      |       1 : gap random                            |          |           | value: set_gap_min to set_gap_max
                                                                              //      |       2 : gap counter                           |          |           | value: set_gap_min to set_gap_max
                                                                              //      |                                                 |          |           |
  output logic    [ ETH_EMU__pW_LAN_GAP   -1 :0 ]    i_set_gap_const     ;    // req  | setting : gap constant / value (byte)           | pdata    | def: 12   |
  output logic    [ ETH_EMU__pW_LAN_GAP   -1 :0 ]    i_set_gap_min       ;    // opt  | setting : gap minimum  / value (byte)           | pdata    | def: 12   | info: min gap = 96 bit or 12 byte (std. 802.3)
  output logic    [ ETH_EMU__pW_LAN_GAP   -1 :0 ]    i_set_gap_max       ;    // opt  | setting : gap maximum  / value (byte)           | pdata    |           |
                                                                              //      |                                                 |          |           |
  //----------------------------------------------// lan packet --------------//------|-------------------------------------------------|----------|-----------|--------------------------------------------------------------//
                                                                              //      |                                                 |          |           |
  //                                                 clock                    //------|-------------------------------------------------|----------|-----------|--------------------------------------------------------------//
                                                                              //      |                                                 |          |           |
  input  logic                                       o_lan_clk           ;    // req  | clock                                           | clock    |           | info: see table (2)
  input  logic                                       o_lan_cen           ;    // req  | clock enable                                    | pulse    |           | info: see table (2) ; width: lan_clk
  input  logic                                       o_lan_ced           ;    // req  | clock enable for data                           | pulse    |           | info: see table (2) ; width: lan_cen
                                                                              //      |                                                 |          |           |
  //                                                 strob                    //------|-------------------------------------------------|----------|-----------|--------------------------------------------------------------//
                                                                              //      |                                                 |          |           |
  input  logic    [                        2 :0 ]    o_lan_str_frame     ;    // req  | frame (packet)          / strob                 | strob    |           | info : see purpose (2)
  input  logic    [ ETH_EMU__pW_LAN_LEN   -1 :0 ]    o_lan_cnt_frame     ;    // opt  | frame (packet)          / strob counter         | count up |           |
  input  logic    [                        2 :0 ]    o_lan_str_head      ;    // opt  | frame header            / strob                 | strob    |           |
  input  logic    [                        3 :0 ]    o_lan_cnt_head      ;    // opt  | frame header            / strob counter         | count up |           |
  input  logic    [                        2 :0 ]    o_lan_str_prm       ;    // opt  | frame header preamble   / strob                 | strob    |           |
  input  logic    [                        2 :0 ]    o_lan_str_sfd       ;    // opt  | frame header sfd        / strob                 | strob    |           |
  input  logic    [                        2 :0 ]    o_lan_str_body      ;    // opt  | frame body              / strob                 | strob    |           |
  input  logic    [ ETH_EMU__pW_LAN_LEN   -1 :0 ]    o_lan_cnt_body      ;    // opt  | frame body              / strob counter         | count up |           |
  input  logic    [                        2 :0 ]    o_lan_str_mac       ;    // opt  | mac header              / strob                 | strob    |           |
  input  logic    [                        3 :0 ]    o_lan_cnt_mac       ;    // opt  | mac header              / strob counter         | count up |           |
  input  logic    [                        2 :0 ]    o_lan_str_mda       ;    // opt  | mac destination address / strob                 | strob    |           |
  input  logic    [                        2 :0 ]    o_lan_str_msa       ;    // opt  | mac source address      / strob                 | strob    |           |
  input  logic    [                        2 :0 ]    o_lan_str_mlt       ;    // opt  | mac length or type      / strob                 | strob    |           |
  input  logic    [                        2 :0 ]    o_lan_str_ip        ;    // opt  | ip header               / strob                 | strob    |           |
  input  logic    [ ETH_EMU__lpW_LAN_IP_C -1 :0 ]    o_lan_cnt_ip        ;    // opt  | ip header               / strob counter         | count up |           |
  input  logic    [                        2 :0 ]    o_lan_str_pay       ;    // opt  | payload & pad           / strob                 | strob    |           |
  input  logic    [ ETH_EMU__pW_LAN_LEN   -1 :0 ]    o_lan_cnt_pay       ;    // opt  | payload & pad           / strob counter         | count up |           |
  input  logic    [                        2 :0 ]    o_lan_str_crc       ;    // opt  | fcs crc-32              / strob                 | strob    |           |
  input  logic    [                        2 :0 ]    o_lan_cnt_crc       ;    // opt  | fcs crc-32              / strob counter         | count up |           |
  input  logic    [                        2 :0 ]    o_lan_str_gap       ;    // opt  | inter-frame gap         / strob                 | strob    |           |
  input  logic    [ ETH_EMU__pW_LAN_GAP   -1 :0 ]    o_lan_cnt_gap       ;    // opt  | inter-frame gap         / strob counter         | count up |           |
                                                                              //      |                                                 |          |           |
  //                                                 data                     //------|-------------------------------------------------|----------|-----------|--------------------------------------------------------------//
                                                                              //      |                                                 |          |           |
  input  logic                                       o_lan_dat_load      ;    // req  | frame (packet)          / load (byte)           | pulse    |           | width: lan_cen
  input  logic    [                        7 :0 ]    o_lan_dat_frame     ;    // req  | frame (packet)          / data (byte)           | pdata    |           |
                                                                              //      |                                                 |          |           |
  //                                                 length                   //------|-------------------------------------------------|----------|-----------|--------------------------------------------------------------//
                                                                              //      |                                                 |          |           |
  input  logic    [ ETH_EMU__pW_LAN_LEN   -1 :0 ]    o_lan_len_frame     ;    // req  | frame (packet)          / length (byte)         | pdata    |           |
  input  logic    [ ETH_EMU__pW_LAN_LEN   -1 :0 ]    o_lan_len_body      ;    // opt  | frame body              / length (byte)         | pdata    |           |
  input  logic    [ ETH_EMU__pW_LAN_LEN   -1 :0 ]    o_lan_len_pay       ;    // opt  | payload                 / length (byte)         | pdata    |           |
  input  logic    [ ETH_EMU__pW_LAN_GAP   -1 :0 ]    o_lan_len_gap       ;    // opt  | inter-frame gap         / length (byte)         | pdata    |           |
                                                                              //      |                                                 |          |           |
  //                                                 fcs crc                  //------|-------------------------------------------------|----------|-----------|--------------------------------------------------------------//
                                                                              //      |                                                 |          |           |
  input  logic                                       o_lan_crc_rdy       ;    // req  | fcs crc-32              / ready                 | pulse    |           | width: lan_cen
  input  logic                                       o_lan_crc_err       ;    // req  | fcs crc-32              / error                 | strob    |           |
                                                                              //      |                                                 |          |           |
  //----------------------------------------------// lan counters ------------//------|-------------------------------------------------|----------|-----------|--------------------------------------------------------------//
                                                                              //      |                                                 |          |           |
  //                                                 setting                  //------|-------------------------------------------------|----------|-----------|--------------------------------------------------------------//
  output logic    [ ETH_EMU__pN_LAN_CNT      :1 ]                             //      |                                                 |          |           |
                  [ ETH_EMU__pW_LAN_LEN   -1 :0 ]    i_lan_set_p_top     ;    // opt  | top bound of body length (not frame)            | array    |           | info: see purpose (5) and note (2)
  output logic    [ ETH_EMU__pN_LAN_CNT      :1 ]                             //      |                                                 |          |           |
                  [ ETH_EMU__pW_LAN_LEN   -1 :0 ]    i_lan_set_p_low     ;    // opt  | low bound of body length (not frame)            | array    |           | info: see purpose (5) and note (2)
                                                                              //      |                                                 |          |           |
  //                                                 detector                 //------|-------------------------------------------------|----------|-----------|--------------------------------------------------------------//
                                                                              //      |                                                 |          |           |
  input  logic                                       o_lan_det_p_rdy     ;    // opt  | packet ready                                    | pulse    |           | width: lan_cen
  input  logic                                       o_lan_det_p_err     ;    // opt  | packet error                                    | strob    |           |
                                                                              //      |                                                 |          |           |
  //                                                 counters                 //------|-------------------------------------------------|----------|-----------|--------------------------------------------------------------//
                                                                              //      |                                                 |          |           |
  output logic                                       i_lan_cnt_p_clr     ;    // opt  | current value / clear                           | strob    | def: 0    | info: sensitivity to rising edge
                                                                              //      |                                                 |          |           |
  input  logic                                       o_lan_cnt_p_rdy     ;    // opt  | current value / ready                           | pulse    |           | width: lan_cen
  input  logic    [ ETH_EMU__pN_LAN_CNT      :0 ]                             //      |                                                 |          |           |
                  [ ETH_EMU__pW_LAN_CNT   -1 :0 ]    o_lan_cnt_p_all     ;    // opt  | current value / packet all                      | count up |           |
  input  logic    [ ETH_EMU__pN_LAN_CNT      :0 ]                             //      |                                                 |          |           |
                  [ ETH_EMU__pW_LAN_CNT   -1 :0 ]    o_lan_cnt_p_err     ;    // opt  | current value / packet error                    | count up |           |
                                                                              //      |                                                 |          |           |
  //----------------------------------------------// lan interface -----------//------|-------------------------------------------------|----------|-----------|--------------------------------------------------------------//
                                                                              //      |                                                 |          |           |
  //                                                 lan mii                  //------|-------------------------------------------------|----------|-----------|--------------------------------------------------------------//
                                                                              //      |                                                 |          |           |
  input  logic    [                        3 :0 ]    o_mii_txd           ;    // req  | transmit data                                   | pdata    |           | info: for latching used rising edge of clock
  input  logic                                       o_mii_tx_en         ;    // req  | transmit enable                                 | strob    |           |
  input  logic                                       o_mii_tx_clk        ;    // req  | transmit clock                                  | clock    |           | info: see table (2)
                                                                              //      |                                                 |          |           |
  //                                                 lan rmii                 //------|-------------------------------------------------|----------|-----------|--------------------------------------------------------------//
                                                                              //      |                                                 |          |           |
  input  logic    [                        1 :0 ]    o_rmii_txd          ;    // req  | transmit data                                   | pdata    |           | info: for latching used rising edge of clock
  input  logic                                       o_rmii_tx_en        ;    // req  | transmit enable                                 | strob    |           | 
  input  logic                                       o_rmii_tx_clk       ;    // req  | transmit clock                                  | clock    |           | info: see table (2)
  input  logic                                       o_rmii_tx_clk_s     ;    // test | transmit clock / speed adapter                  | strob    |           |
                                                                              //      |                                                 |          |           |
                                                                              //      |  - for 100 mbit/s : 50 mhz                      |          |           |
                                                                              //      |  - for 10  mbit/s :  5 mhz (not 50 mhz)         |          |           |
                                                                              //      |                                                 |          |           |
  //                                                 lan gmii                 //------|-------------------------------------------------|----------|-----------|--------------------------------------------------------------//
                                                                              //      |                                                 |          |           |
  input  logic    [                        7 :0 ]    o_gmii_txd          ;    // req  | transmit data                                   | pdata    |           | info: for latching used rising edge of clock
  input  logic                                       o_gmii_tx_en        ;    // req  | transmit enable                                 | strob    |           | 
  input  logic                                       o_gmii_tx_clk       ;    // req  | transmit clock                                  | clock    |           | info: see table (2)
                                                                              //      |                                                 |          |           |
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // logic
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  //----------------------------------------------// global -------------|

  var    logic                                       clk                 ;
  var    logic                                       cen                 ;
  var    logic                                       ced                 ;
  var    logic                                       sclr                ;

  //----------------------------------------------// sync counters ------|

  var    logic    [                       31 :0 ]    cnt_clk         = 0 ;
  var    logic    [                       31 :0 ]    cnt_cen         = 0 ;
  var    logic    [                       31 :0 ]    cnt_ced         = 0 ;
  var    logic    [ ETH_EMU__pW_LAN_CNT   -1 :0 ]    cnt_pack            ;

  //----------------------------------------------// reset --------------|

  var    logic                                       reset_n         = 1 ;
  var    logic    [                       31 :0 ]    reset_n_cnt     = 0 ;

  //----------------------------------------------// setting ------------|

  //                                                 update

  var    logic                                       set_upd         = 0 ;

  //                                                 interface

  var    logic    [                        1 :0 ]    set_io_type         ;
  var    logic    [                        1 :0 ]    set_io_speed        ;
  var    logic                                       set_io_pause    = 0 ;

  //                                                 transmit speed

  var    logic                                       set_tx_mode         ;
  var    logic    [                       19 :0 ]    set_tx_speed        ;
  var    logic    [                        1 :0 ]    set_tx_sel          ;

  //                                                 crc error

  var    logic                                       set_err_ena         ;
  var    logic    [                        6 :0 ]    set_err_per         ;

  //                                                 mac header

  var    logic                                       set_mac_ena         ;

  var    logic    [                        2 :0 ]    set_mda_type        ;
  var    logic    [                       47 :0 ]    set_mda_const       ;
  var    logic    [                       47 :0 ]    set_mda_min         ;
  var    logic    [                       47 :0 ]    set_mda_max         ;

  var    logic    [                        2 :0 ]    set_msa_type        ;
  var    logic    [                       47 :0 ]    set_msa_const       ;
  var    logic    [                       47 :0 ]    set_msa_min         ;
  var    logic    [                       47 :0 ]    set_msa_max         ;

  var    logic    [                        2 :0 ]    set_mlt_type        ;
  var    logic    [                       15 :0 ]    set_mlt_const       ;
  var    logic    [                       15 :0 ]    set_mlt_min         ;
  var    logic    [                       15 :0 ]    set_mlt_max         ;

  //                                                 ip header

  var    logic                                       set_ip_ena          ;
  var    logic    [                        2 :0 ]    set_ip_type         ;
  var    logic    [ ETH_EMU__lpW_LAN_IP   -1 :0 ]    set_ip_const        ;
  var    logic    [ ETH_EMU__lpW_LAN_IP   -1 :0 ]    set_ip_min          ;
  var    logic    [ ETH_EMU__lpW_LAN_IP   -1 :0 ]    set_ip_max          ;

  //                                                 payload

  var    logic    [                        2 :0 ]    set_pay_type        ;
  var    logic    [ ETH_EMU__pW_LAN_PAY   -1 :0 ]    set_pay_const       ;
  var    logic    [                        7 :0 ]    set_pay_min         ;
  var    logic    [                        7 :0 ]    set_pay_max         ;

  //                                                 frame length

  var    logic     [                       1 :0 ]    set_len_type        ;
  var    logic    [ ETH_EMU__pW_LAN_LEN   -1 :0 ]    set_len_const       ;
  var    logic    [ ETH_EMU__pW_LAN_LEN   -1 :0 ]    set_len_min         ;
  var    logic    [ ETH_EMU__pW_LAN_LEN   -1 :0 ]    set_len_max         ;

  //                                                 frame gap

  var    logic    [                        1 :0 ]    set_gap_type        ;
  var    logic    [ ETH_EMU__pW_LAN_GAP   -1 :0 ]    set_gap_const       ;
  var    logic    [ ETH_EMU__pW_LAN_GAP   -1 :0 ]    set_gap_min         ;
  var    logic    [ ETH_EMU__pW_LAN_GAP   -1 :0 ]    set_gap_max         ;

  //----------------------------------------------// lan counters -------|

  //                                                 setting
  var    logic    [ ETH_EMU__pN_LAN_CNT      :1 ]
                  [ ETH_EMU__pW_LAN_LEN   -1 :0 ]    lan_set_p_top       ;
  var    logic    [ ETH_EMU__pN_LAN_CNT      :1 ]
                  [ ETH_EMU__pW_LAN_LEN   -1 :0 ]    lan_set_p_low       ;

  //                                                 counters

  var    logic                                       lan_cnt_p_clr   = 0 ;

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  //==========================================================================================================================================================================================================================//
  // body of module
  //==========================================================================================================================================================================================================================//

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // out logic
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  // reset

  assign  i_reset_n        = reset_n        ;

  // setting / update

  assign  i_set_upd        = set_upd        ;

  // setting / interface

  assign  i_set_io_type    = set_io_type    ;
  assign  i_set_io_speed   = set_io_speed   ;
  assign  i_set_io_pause   = set_io_pause   ;

  // setting / transmit speed

  assign  i_set_tx_mode    = set_tx_mode    ;
  assign  i_set_tx_speed   = set_tx_speed   ;
  assign  i_set_tx_sel     = set_tx_sel     ;

  // setting / crc error

  assign  i_set_err_ena    = set_err_ena    ;
  assign  i_set_err_per    = set_err_per    ;

  // setting / mac header

  assign  i_set_mac_ena    = set_mac_ena    ;

  assign  i_set_mda_type   = set_mda_type   ;
  assign  i_set_mda_const  = set_mda_const  ;
  assign  i_set_mda_min    = set_mda_min    ;
  assign  i_set_mda_max    = set_mda_max    ;

  assign  i_set_msa_type   = set_msa_type   ;
  assign  i_set_msa_const  = set_msa_const  ;
  assign  i_set_msa_min    = set_msa_min    ;
  assign  i_set_msa_max    = set_msa_max    ;

  assign  i_set_mlt_type   = set_mlt_type   ;
  assign  i_set_mlt_const  = set_mlt_const  ;
  assign  i_set_mlt_min    = set_mlt_min    ;
  assign  i_set_mlt_max    = set_mlt_max    ;

  // setting / ip header

  assign  i_set_ip_ena     = set_ip_ena     ;
  assign  i_set_ip_type    = set_ip_type    ;
  assign  i_set_ip_const   = set_ip_const   ;
  assign  i_set_ip_min     = set_ip_min     ;
  assign  i_set_ip_max     = set_ip_max     ;

  // setting / payload

  assign  i_set_pay_type   = set_pay_type   ;
  assign  i_set_pay_const  = set_pay_const  ;
  assign  i_set_pay_min    = set_pay_min    ;
  assign  i_set_pay_max    = set_pay_max    ;

  // setting / frame length

  assign  i_set_len_type   = set_len_type   ;
  assign  i_set_len_const  = set_len_const  ;
  assign  i_set_len_min    = set_len_min    ;
  assign  i_set_len_max    = set_len_max    ;

  // setting / frame gap

  assign  i_set_gap_type   = set_gap_type   ;
  assign  i_set_gap_const  = set_gap_const  ;
  assign  i_set_gap_min    = set_gap_min    ;
  assign  i_set_gap_max    = set_gap_max    ;

  // lan counters / setting

  assign  i_lan_set_p_top  = lan_set_p_top  ;
  assign  i_lan_set_p_low  = lan_set_p_low  ;

  // lan counters / counters

  assign  i_lan_cnt_p_clr  = lan_cnt_p_clr  ;

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // global
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  // clock

  assign  clk  = o_lan_clk ;
  assign  cen  = o_lan_cen ;
  assign  ced  = o_lan_ced ; // period = 1 byte

  // clear

  assign  sclr = ~reset_n  ;

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // sync counters
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  // clk counter

  always_ff @(posedge clk)
    if (&cnt_clk == 0)
      cnt_clk <= cnt_clk + 1;

  // cen counter

  always_ff @(posedge clk)
    if (cen)
      begin
        if (&cnt_cen == 0)
          cnt_cen <= cnt_cen + 1;
      end

  // ced counter

  always_ff @(posedge clk)
    if (cen & ced) // period = 1 byte
      begin
        if (&cnt_ced == 0)
          cnt_ced <= cnt_ced + 1;
      end

  // packet counter

  assign  cnt_pack = o_lan_cnt_p_all[0];

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // reset
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  localparam bit lpE_RESET_N = 0; // enable of reset generator

  always_ff @(posedge clk)
    reset_n_cnt <= reset_n_cnt + 1;

  always_ff @(posedge clk)
    if (!lpE_RESET_N)
      reset_n <= 1'b1;
    else
      begin
        if (reset_n_cnt == 800)  reset_n <= 0 ; else // dimension: t.clk
        if (reset_n_cnt == 900)  reset_n <= 1 ;
      end

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // setting / update
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  localparam bit lpE_SET_UPD = 0; // enable of update (request) generator

  always_ff @(posedge clk)
    if (!lpE_SET_UPD)
      set_upd <= 1'b0;
    else
      if (ced)
        begin
          if (cnt_ced == 2_000)  set_upd <= 1 ; else // dimension: byte
          if (cnt_ced == 2_010)  set_upd <= 0 ;
        end

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // setting / pause
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  localparam bit lpE_SET_PAUSE = 0; // enable of pause generator

  always_ff @(posedge clk)
    if (!lpE_SET_PAUSE)
      set_io_pause <= 1'b0;
    else
      if (ced)
        begin
          if (cnt_ced == 8_000)  set_io_pause <= 1 ; else // dimension: byte
          if (cnt_ced == 9_000)  set_io_pause <= 0 ;
        end

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // lan counters / clear
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  localparam bit lpE_CNT_CLR = 0; // enable of clear generator

  always_ff @(posedge clk)
    if (!lpE_CNT_CLR)
      lan_cnt_p_clr <= 1'b0;
    else
      if (ced)
        begin
          if (cnt_ced == 1_000)  lan_cnt_p_clr <= 1 ; else // dimension: byte
          if (cnt_ced == 2_000)  lan_cnt_p_clr <= 0 ;
        end

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // setting / interface
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
                                                                    //      |                                                 |          |           |
  initial begin  set_io_type    = 1                        ; end    // req  | setting : transmit type                         | pdata    |           |
                                                                    //      |       0 : transmit gmii                         |          |           |
                                                                    //      |       1 : transmit rmii                         |          |           |
                                                                    //      |       2 : transmit mii                          |          |           |
                                                                    //      |                                                 |          |           |
  initial begin  set_io_speed   = 0                        ; end    // req  | setting : transmit speed                        | pdata    |           |
                                                                    //      |       0 : 1000 mbit/s                           |          |           |
                                                                    //      |       1 : 100  mbit/s                           |          |           |
                                                                    //      |       2 : 10   mbit/s                           |          |           |
                                                                    //      |                                                 |          |           |
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // setting / transmit speed
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
                                                                    //      |                                                 |          |           |
  initial begin  set_tx_mode    = 0                        ; end    // opt  | setting : transmit mode                         | strob    | def: 0    | info: see 'comment.txt' (3)
                                                                    //      |       0 : transmit gap manual                   |          |           | info: used 'i_set_gap_type'
                                                                    //      |       1 : transmit gap auto                     |          |           | info: used 'i_set_tx_speed'
                                                                    //      |                                                 |          |           |
  initial begin  set_tx_speed   = 30_000                   ; end    // opt  | setting : transmit speed (kbit/sec) / set       | pdata    |           | value: 0-1000000 kbit/sec
  initial begin  set_tx_sel     = 0                        ; end    // opt  | setting : transmit speed (kbit/sec) / select    | pdata    | def: 0    |
                                                                    //      |                                                 |          |           |
                                                                    //      |       0 : payload                               |          |           |
                                                                    //      |       1 : payload + mac                         |          |           |
                                                                    //      |       2 : payload + mac + fcs crc               |          |           |
                                                                    //      |       3 : payload + mac + fcs crc + head        |          |           |
                                                                    //      |                                                 |          |           |
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // setting / crc error
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
                                                                    //      |                                                 |          |           |
  initial begin  set_err_ena    = 1                        ; end    // opt  | setting : crc error                             | strob    | def: 0    |
                                                                    //      |       0 : crc error insert disable              |          |           |
                                                                    //      |       1 : crc error insert enable               |          |           | info: crc = 32'h ffffffff
                                                                    //      |                                                 |          |           |
  initial begin  set_err_per    = 50                       ; end    // opt  | setting : crc error probability (0-100%)        | pdata    | def: 0    | info: used $random
                                                                    //      |                                                 |          |           |
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // setting / mac header
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
                                                                    //      |                                                 |          |           |
  initial begin  set_mac_ena    = 1                        ; end    // opt  | setting : mac header                            | strob    | def: 0    |
                                                                    //      |       0 : mac inserter disable                  |          |           |
                                                                    //      |       1 : mac inserter enable                   |          |           |
                                                                    //      |                                                 |          |           |
  // mac destination address (mda)                                  //------|-------------------------------------------------|----------|-----------|------------------------------------------------------------------------//
                                                                    //      |                                                 |          |           |
  initial begin  set_mda_type   = 0                        ; end    // opt  | setting : mda type                              | pdata    | def: 0    |
                                                                    //      |       0 : mda constant                          |          |           | value: set_mda_const
                                                                    //      |       1 : mda random                            |          |           | value: set_mda_min to set_mda_max
                                                                    //      |       2 : mda counter of byte (individual)      |          |           | value: set_mda_min to set_mda_max
                                                                    //      |       3 : mda counter of byte (continuous)      |          |           | value: set_mda_min to set_mda_max
                                                                    //      |       4 : mda counter of packets                |          |           | value: set_mda_min to set_mda_max
                                                                    //      |                                                 |          |           |
  initial begin  set_mda_const  = 48'ha1_a2_a3_a4_a5_a6    ; end    // opt  | setting : mda constant / value                  | pdata    |           |
  initial begin  set_mda_min    = 48'h00_00_00_00_00_00    ; end    // opt  | setting : mda minimum  / value                  | pdata    | def: '0   |
  initial begin  set_mda_max    = 48'hff_ff_ff_ff_ff_ff    ; end    // opt  | setting : mda maximum  / value                  | pdata    | def: '1   |
                                                                    //      |                                                 |          |           |
  // mac source address (msa)                                       //------|-------------------------------------------------|----------|-----------|------------------------------------------------------------------------//
                                                                    //      |                                                 |          |           |
  initial begin  set_msa_type   = 0                        ; end    // opt  | setting : msa type                              | pdata    | def: 0    |
                                                                    //      |       0 : msa constant                          |          |           | value: set_msa_const
                                                                    //      |       1 : msa random                            |          |           | value: set_msa_min to set_msa_max
                                                                    //      |       2 : msa counter of byte (individual)      |          |           | value: set_msa_min to set_msa_max
                                                                    //      |       3 : msa counter of byte (continuous)      |          |           | value: set_msa_min to set_msa_max
                                                                    //      |       4 : msa counter of packets                |          |           | value: set_msa_min to set_msa_max
                                                                    //      |                                                 |          |           |
  initial begin  set_msa_const  = 48'hb1_b2_b3_b4_b5_b6    ; end    // opt  | setting : msa constant / value                  | pdata    |           |
  initial begin  set_msa_min    = 48'h00_00_00_00_00_00    ; end    // opt  | setting : msa minimum  / value                  | pdata    | def: '0   |
  initial begin  set_msa_max    = 48'hff_ff_ff_ff_ff_ff    ; end    // opt  | setting : msa maximum  / value                  | pdata    | def: '1   |
                                                                    //      |                                                 |          |           |
  // mac length or type (mlt)                                       //------|-------------------------------------------------|----------|-----------|------------------------------------------------------------------------//
                                                                    //      |                                                 |          |           |
  initial begin  set_mlt_type   = 5                        ; end    // opt  | setting : mlt type                              | pdata    | def: 5    |
                                                                    //      |       0 : mlt constant                          |          |           | value: set_mlt_const
                                                                    //      |       1 : mlt random                            |          |           | value: set_mlt_min to set_mlt_max
                                                                    //      |       2 : mlt counter of byte (individual)      |          |           | value: set_mlt_min to set_mlt_max
                                                                    //      |       3 : mlt counter of byte (continuous)      |          |           | value: set_mlt_min to set_mlt_max
                                                                    //      |       4 : mlt counter of packets                |          |           | value: set_mlt_min to set_mlt_max
                                                                    //      |       5 : mlt length  of payload                |          |           | value: lan_len_pay
                                                                    //      |                                                 |          |           |
  initial begin  set_mlt_const  = 48'hc1_c2                ; end    // opt  | setting : mlt constant / value                  | pdata    |           |
  initial begin  set_mlt_min    = 48'h00_00                ; end    // opt  | setting : mlt minimum  / value                  | pdata    | def: '0   |
  initial begin  set_mlt_max    = 48'hff_ff                ; end    // opt  | setting : mlt maximum  / value                  | pdata    | def: '1   |
                                                                    //      |                                                 |          |           |
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // setting / ip header
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
                                                                    //      |                                                 |          |           |
  initial begin  set_ip_ena     = 0                        ; end    // opt  | setting : ip header                             | pdata    | def: 0    |
                                                                    //      |       0 : ip inserter disable                   |          |           |
                                                                    //      |       1 : ip inserter enable                    |          |           |
                                                                    //      |                                                 |          |           |
  initial begin  set_ip_type    = 0                        ; end    // opt  | setting : ip type                               | pdata    | def: 0    |
                                                                    //      |       0 : ip constant                           |          |           | value: set_ip_const
                                                                    //      |       1 : ip random                             |          |           | value: set_ip_min to set_ip_max
                                                                    //      |       2 : ip counter of byte (individual)       |          |           | value: set_ip_min to set_ip_max
                                                                    //      |       3 : ip counter of byte (continuous)       |          |           | value: set_ip_min to set_ip_max
                                                                    //      |       4 : ip counter of packets                 |          |           | value: set_ip_min to set_ip_max
                                                                    //      |                                                 |          |           |
  initial begin  set_ip_const   = 32'hd1_d2_d3_d4          ; end    // opt  | setting : ip constant                           | pdata    |           |
  initial begin  set_ip_min     = 32'h00_00_00_00          ; end    // opt  | setting : ip minimum                            | pdata    |           |
  initial begin  set_ip_max     = 32'hff_ff_ff_ff          ; end    // opt  | setting : ip maximum                            | pdata    |           |
                                                                    //      |                                                 |          |           |
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // setting / payload
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
                                                                    //      |                                                 |          |           |
  initial begin  set_pay_type   = 0                        ; end    // req  | setting : payload type                          | pdata    | def: 0    |
                                                                    //      |       0 : payload constant                      |          |           | value: set_pay_const
                                                                    //      |       1 : payload random                        |          |           | value: set_pay_min to set_pay_max
                                                                    //      |       2 : payload counter of byte (individual)  |          |           | value: set_pay_min to set_pay_max
                                                                    //      |       3 : payload counter of byte (continuous)  |          |           | value: set_pay_min to set_pay_max
                                                                    //      |       4 : payload counter of packets            |          |           | value: set_pay_min to set_pay_max
                                                                    //      |                                                 |          |           |
  initial begin  set_pay_const  = 8'hff                    ; end    // req  | setting : payload constant / value              | pdata    |           |
  initial begin  set_pay_min    = 0'h00                    ; end    // opt  | setting : payload minimum  / value              | pdata    | def: '0   |
  initial begin  set_pay_max    = 0'hff                    ; end    // opt  | setting : payload maximum  / value              | pdata    | def: '1   |
                                                                    //      |                                                 |          |           |
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // setting / frame length
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
                                                                    //      |                                                 |          |           |
  initial begin  set_len_type   = 2                        ; end    // req  | setting : length type                           | pdata    | def: 0    |
                                                                    //      |       0 : length constant                       |          |           | value: set_len_const
                                                                    //      |       1 : length random                         |          |           | value: set_len_min to set_len_max
                                                                    //      |       2 : length counter                        |          |           | value: set_len_min to set_len_max
                                                                    //      |                                                 |          |           |
  initial begin  set_len_const  = 72                       ; end    // req  | setting : length constant / value (byte)        | pdata    |           |
  initial begin  set_len_min    = 72                       ; end    // opt  | setting : length minimum  / value (byte)        | pdata    | def: 72   |
  initial begin  set_len_max    = 1550                     ; end    // opt  | setting : length maximum  / value (byte)        | pdata    | def: 1526 | info: min gap = 96 bit or 12 byte (std. 802.3)
                                                                    //      |                                                 |          |           |
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // setting / frame gap
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
                                                                    //      |                                                 |          |           |
  initial begin  set_gap_type   = 0                        ; end    // req  | setting : gap type                              | pdata    | def: 0    |
                                                                    //      |       0 : gap constant                          |          |           | value: set_gap_const
                                                                    //      |       1 : gap random                            |          |           | value: set_gap_min to set_gap_max
                                                                    //      |       2 : gap counter                           |          |           | value: set_gap_min to set_gap_max
                                                                    //      |                                                 |          |           |
  initial begin  set_gap_const  = 12                       ; end    // req  | setting : gap constant / value (byte)           | pdata    | def: 12   |
  initial begin  set_gap_min    = 1                        ; end    // opt  | setting : gap minimum  / value (byte)           | pdata    | def: 12   | info: min gap = 96 bit (std. 802.3)
  initial begin  set_gap_max    = 40                       ; end    // opt  | setting : gap maximum  / value (byte)           | pdata    |           |
                                                                    //      |                                                 |          |           |
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // lan counters / setting
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
                                                                    //      |                                                 |          |           |
  initial begin  lan_set_p_low [1] = 64                    ; end    // opt  | low bound of body length (not frame)            | array    | def: 64   |
  initial begin  lan_set_p_top [1] = 127                   ; end    // opt  | top bound of body length (not frame)            | array    | def: 127  |
                                                                    //      |                                                 |          |           |
  initial begin  lan_set_p_low [2] = 128                   ; end    // opt  | low bound of body length (not frame)            | array    | def: 127  |
  initial begin  lan_set_p_top [2] = 255                   ; end    // opt  | top bound of body length (not frame)            | array    | def: 255  |
                                                                    //      |                                                 |          |           |
  initial begin  lan_set_p_low [3] = 256                   ; end    // opt  | low bound of body length (not frame)            | array    | def: 256  |
  initial begin  lan_set_p_top [3] = 511                   ; end    // opt  | top bound of body length (not frame)            | array    | def: 511  |
                                                                    //      |                                                 |          |           |
  initial begin  lan_set_p_low [4] = 512                   ; end    // opt  | low bound of body length (not frame)            | array    | def: 512  |
  initial begin  lan_set_p_top [4] = 1023                  ; end    // opt  | top bound of body length (not frame)            | array    | def: 1023 |
                                                                    //      |                                                 |          |           |
  initial begin  lan_set_p_low [5] = 1024                  ; end    // opt  | low bound of body length (not frame)            | array    | def: 1024 |
  initial begin  lan_set_p_top [5] = 1518                  ; end    // opt  | top bound of body length (not frame)            | array    | def: 1518 |
                                                                    //      |                                                 |          |           |
  initial begin  lan_set_p_low [6] = 1519                  ; end    // opt  | low bound of body length (not frame)            | array    | def: 1519 |
  initial begin  lan_set_p_top [6] = 0     /* oversize */  ; end    // opt  | top bound of body length (not frame)            | array    | def: 0    |
                                                                    //      |                                                 |          |           |
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  endmodule




  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Company      : Micran, Department of Telecommunications
  //
  //  Engineer     : Pogodaev Danil
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Library      : lib_ethernet / library of ethernet components
  //
  //  Component    : eth_rx_v2 / receiver of ethernet frames (packets)
  //
  //  File Name    : tb_gen .sv (testbench)
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Description  : Testbench generator for module 'eth_rx'.
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Attached (1) : Additional (detailed) code comments / eth_rx_v2 - comment .txt / default (!)
  //
  //  Attached (2) : Additional (detailed) code comments / tb_gen - comment .txt
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Include Files
  //  
  //  Category       Type        Name                      Ver     Description
  //
  //  top_gen        testbench   tb_gen             .sv    2.0     top generator
  //  top_gen        include     tb_gen__define     .sv    2.0     top generator / defining macros
  //
  //  top_gen        testbench   tb_gen_emu         .sv    2.0     top generator / module 'eth_emu' / testbench generator
  //  top_gen        testbench   tb_gen_emu_m       .sv    2.0     top generator / module 'eth_emu' / testbench generator / manual mode
  //  top_gen        include     eth_emu__par_set   .svh   2.0     top generator / module 'eth_emu' / parameter setting
  //  top_gen        include     eth_emu__par_local .svh   2.0     top generator / module 'eth_emu' / parameter local
  //
  //  top_tb         include     eth_rx__par_set    .svh   2.0     top testbench / module 'eth_rx' / parameter setting
  //  top_tb         include     eth_rx__par_local  .svh   2.0     top testbench / module 'eth_rx' / parameter local
  //
  //  lib_ethernet   function    eth_crc32_8d       .sv    1.0.0   calculator of frame check sequence crc-32 (data 8 bit)
  //
  //  lib_ethernet   testbench   tb_eth_emu_v2      .sv    2.0.0   emulator of ethernet frames
  //  lib_ethernet   testbench   tb_eth_emu_clk_v2  .sv    2.0.0   clocks generator of ethernet frames
  //  lib_ethernet   testbench   tb_eth_emu_str_v2  .sv    2.0.0   strobes generator of ethernet frames
  //  lib_ethernet   testbench   tb_eth_emu_dat_v2  .sv    2.0.0   data stream generator of ethernet frames
  //  lib_ethernet   testbench   tb_eth_emu_lan_v2  .sv    2.0.0   lan ethernet interface implementation
  //  lib_ethernet   testbench   tb_eth_emu_spd_v2  .sv    2.0.0   speed calculator (calculator of inter-frame gap)
  //  lib_ethernet   testbench   tb_eth_emu_ini_v2  .sv    2.0.0   generator of initialization signal (for settings)
  //
  //  lib_global     function    cbit               .sv    1.0.0   bits calculator for input number (round up)
  //  lib_global     rtl         z_line             .sv    1.0.0   delay line for serial and parallel data
  //  lib_global     rtl         gen_npr            .sv    1.0.0   generator of registers with attribute "noprune"
  //  lib_global     rtl         det_se             .sv    1.0.0   detector of single edge for input strobe
  //  lib_global     rtl         gen_sstr           .sv    1.0.0   generator of single strobe
  //  lib_global     rtl         gen_mstr           .sv    1.0.0   generator of multi strobes
  //  lib_global     rtl         shift_p2s          .sv    1.0.0   shift register for parallel to serial
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Revision List
  //
  //  Version   Date       Author           Changes
  //
  //  2.0       26.09.16   Pogodaev Danil   new version
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  `timescale 1ns/1ns

  module tb_gen          ( // for 'eth_rx'

  //---- reset -----------|

         i_reset_n        ,

  //---- setting ---------|

         i_set_cut        ,
         i_set_speed      ,
         i_set_jumbo      ,

  //---- lan interface ---|

  //     lan gmii

         i_gmii_clk       ,
         i_gmii_rxd       ,
         i_gmii_rx_dv     ,

  //     lan rmii

         i_rmii_clk       ,
         o_rmii_clk_s     ,
         i_rmii_rxd       ,
         i_rmii_crs_dv    ,

  //     lan rmii

         i_mii_clk        ,
         i_mii_rxd        ,
         i_mii_rx_dv      ,

  //---- custom mode -----|

         i_cst_clk        ,
         i_cst_cen        ,
         i_cst_ced        ,
         i_cst_rxd        ,
         i_cst_rx_dv      ,

  //---- rx packet -------|

  //     clock

         o_rx_clk         ,
         o_rx_cen         ,
         o_rx_ced         ,

  //     strob

         o_rx_str_body    ,
         o_rx_cnt_body    ,
         o_rx_str_mac     ,
         o_rx_cnt_mac     ,
         o_rx_str_mda     ,
         o_rx_str_msa     ,
         o_rx_str_mlt     ,
         o_rx_str_pay     ,
         o_rx_cnt_pay     ,
         o_rx_str_ip      ,
         o_rx_cnt_ip      ,
         o_rx_str_gap     ,
         o_rx_cnt_gap     ,

  //     data

         o_rx_dat_load    ,
         o_rx_dat_body    ,
         o_rx_rdy_mac     ,
         o_rx_dat_mda     ,
         o_rx_dat_msa     ,
         o_rx_dat_mlt     ,
         o_rx_rdy_ip      ,
         o_rx_dat_ip      ,

  //     length

         o_rx_rdy_body    ,
         o_rx_len_body    ,
         o_rx_rdy_gap     ,
         o_rx_len_gap     ,

  //     fcs crc

         o_rx_crc_rdy     ,
         o_rx_crc_err     ,

  //---- rx counters -----|

  //     setting

         i_rx_set_p_top   ,
         i_rx_set_p_low   ,
         i_rx_set_p_spd   ,

  //     detector

         i_rx_det_p_off   ,
         o_rx_det_p_err   ,
         o_rx_det_p_cut   ,
         o_rx_det_p_rdy   ,

  //     counters

         i_rx_cnt_p_clr   ,
         o_rx_cnt_p_rdy   ,
         o_rx_cnt_p_all   ,
         o_rx_cnt_p_err   ,
         o_rx_cnt_p_off   ,
         o_rx_cnt_p_spd   ,

  //     counters (rhythm)

         i_rx_rcnt_p_clr  ,
         i_rx_rcnt_p_upd  ,
         o_rx_rcnt_p_rdy  ,
         o_rx_rcnt_p_all  ,
         o_rx_rcnt_p_err  ,
         o_rx_rcnt_p_off  ,
         o_rx_rcnt_p_spd );

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // include files : functions
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
                                                                           //      |
  `include "cbit.sv"                                                       // req  | bits calculator for input number (round up)
                                                                           //      |
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // include files : define
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
                                                                           //      |
  // receiver of ethernet frames / tb generator                            //------|------------------------------------------------------------------------------------------------------------------------------------------//
                                                                           //      |
  `include "tb_gen__define.svh"                                            // req  | top generator / module 'tb_gen' / defining macros
                                                                           //      |
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // include files : parameters
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
                                                                           //      |
  // receiver of ethernet frames                                           //------|------------------------------------------------------------------------------------------------------------------------------------------//
                                                                           //      |
  `include "eth_rx__par_set.svh"                                           // req  | top testbench / module 'eth_rx' / parameter setting
  `include "eth_rx__par_local.svh"                                         // req  | top testbench / module 'eth_rx' / parameter local
                                                                           //      |
  // emulator of ethernet frames                                           //------|------------------------------------------------------------------------------------------------------------------------------------------//
                                                                           //      |
  `include "eth_emu__par_set.svh"                                          // req  | top generator / module 'eth_emu' / parameter setting
  `include "eth_emu__par_local.svh"                                        // req  | top generator / module 'eth_emu' / parameter local
                                                                           //      |
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // parameters : setting
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
                                                                           //      |                                            |          |           |
  // rx counters ----------------------------------------------------------//------|--------------------------------------------|----------|-----------|----------------------------------------------------------------------//
                                                                           //      |                                            |          |           |
  parameter bit  lpS_RCNT_P_UPD  = 0                                  ;    // test | update setting for counters (rhythm)       |          | def: 0    | info: see 'top_gen - comment.txt' (1)
                                                                           //      |                                            |          |           |
                                                                           //      |   0 : setting to packet counter            |          |           |
                                                                           //      |   1 : setting to clock counter             |          |           | info: used for speed calculation
                                                                           //      |                                            |          |           |
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // parameters : local (for logic)
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
                                                                           //      |                                            |          |           |
  // custom mode ----------------------------------------------------------//------|--------------------------------------------|----------|-----------|----------------------------------------------------------------------//
                                                                           //      |                                            |          |           |
  parameter bit  lpE_CST_CLK  = (ETH_RX__pS_IO_TYPE == "custom")      ;    // test | enable of custom clock                     | on/off   | def: 0    | info: lan interface 'off', used lan packet only
                                                                           //      |                                            |          |           |
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // interface
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
                                                                           //      |                                            |          |           |
  //---------------------------------------------// reset -----------------//------|--------------------------------------------|----------|-----------|----------------------------------------------------------------------//
                                                                           //      |                                            |          |           |
  output logic                                      i_reset_n         ;    // opt  | sync clear (ative low)                     | strob    | def: 1    | info: used 'rx_clk' ; see note (5) and 'comment.txt' (6)
                                                                           //      |                                            |          |           |
  //---------------------------------------------// setting ---------------//------|--------------------------------------------|----------|-----------|----------------------------------------------------------------------//
                                                                           //      |                                            |          |           |
  output logic                                      i_set_cut         ;    // opt  | enable of cut off oversize frames          | strob    | def: 0    | info: see also 'pE_RX_CUT'
                                                                           //      |                                            |          |           |
  output logic                                      i_set_jumbo       ;    // opt  | enable of jumbo-frames support             | strob    | def: 0    | info: see also 'pE_RX_JUMBO'
                                                                           //      |                                            |          |           |
  output logic    [                       1 :0 ]    i_set_speed       ;    // req  | speed of interface                         | strob    |           | info: see 'comment.txt' (6)
                                                                           //      |                                            |          |           |
                                                                           //      |   0 : 1000 mbit/sec                        |          |           |
                                                                           //      |   1 :  100 mbit/sec                        |          |           |
                                                                           //      |   2 :   10 mbit/sec                        |          |           |
                                                                           //      |                                            |          |           |
  //---------------------------------------------// lan interface ---------//------|--------------------------------------------|----------|-----------|----------------------------------------------------------------------//
                                                                           //      |                                            |          |           |
  //                                                lan mii                //------|--------------------------------------------|----------|-----------|----------------------------------------------------------------------//
                                                                           //      |                                            |          |           |
  output logic    [                       3 :0 ]    i_mii_rxd         ;    // req  | receive data                               | pdata    |           | info: see note  (4) ; info: for latching used rising edge of clock
  output logic                                      i_mii_rx_dv       ;    // req  | receive data valid                         | strob    |           | info: see note  (4)
  output logic                                      i_mii_clk         ;    // req  | receive clock                              | clock    |           | info: see table (1) ; see 'comment.txt' (6)
                                                                           //      |                                            |          |           |
  //                                                lan rmii               //------|--------------------------------------------|----------|-----------|----------------------------------------------------------------------//
                                                                           //      |                                            |          |           |
  output logic    [                       1 :0 ]    i_rmii_rxd        ;    // req  | receive data                               | pdata    |           | info: see note  (4) ; info: for latching used rising edge of clock
  output logic                                      i_rmii_crs_dv     ;    // req  | receive data valid & carrier sense         | strob    |           | info: see note  (4)
  output logic                                      i_rmii_clk        ;    // req  | receive clock                              | clock    |           | info: see table (1) ; see 'comment.txt' (6)
  input  logic                                      o_rmii_clk_s      ;    // test | receive clock / speed adapter              |          |           |
                                                                           //      |                                            |          |           |
                                                                           //      |  - for 100 mbit/s : 50 mhz                 | clock    |           |
                                                                           //      |  - for 10  mbit/s :  5 mhz (not 50 mhz)    | strob    |           |
                                                                           //      |                                            |          |           |
  //                                                lan gmii               //------|--------------------------------------------|----------|-----------|----------------------------------------------------------------------//
                                                                           //      |                                            |          |           |
  output logic    [                       7 :0 ]    i_gmii_rxd        ;    // req  | receive data                               | pdata    |           | info: see note  (4) ; info: for latching used rising edge of clock
  output logic                                      i_gmii_rx_dv      ;    // req  | receive data valid                         | strob    |           | info: see note  (4)
  output logic                                      i_gmii_clk        ;    // req  | receive clock                              | clock    |           | info: see table (1) ; see 'comment.txt' (6)
                                                                           //      |                                            |          |           |
  //---------------------------------------------// custom mode -----------//------|--------------------------------------------|----------|-----------|----------------------------------------------------------------------//
                                                                           //      |                                            |          |           |
  output logic                                      i_cst_clk         ;    // opt  | receive clock                              | clock    |           |
  output logic                                      i_cst_cen         ;    // opt  | receive clock enable                       | pulse    | def: 1    | width: cst_clk
  output logic                                      i_cst_ced         ;    // opt  | receive clock enable for data              | pulse    | def: 1    | width: cst_cen
                                                                           //      |                                            |          |           |
  output logic    [                       7 :0 ]    i_cst_rxd         ;    // opt  | receive data                               | pdata    |           | info: for latching used rising edge of clock
  output logic                                      i_cst_rx_dv       ;    // opt  | receive data valid                         | strob    |           | 
                                                                           //      |                                            |          |           |
  //---------------------------------------------// rx packet -------------//------|--------------------------------------------|----------|-----------|----------------------------------------------------------------------//
                                                                           //      |                                            |          |           |
  //                                                clock                  //------|--------------------------------------------|----------|-----------|----------------------------------------------------------------------//
                                                                           //      |                                            |          |           |
  input  logic                                      o_rx_clk          ;    // req  | clock                                      | clock    |           | info: see table (1)
  input  logic                                      o_rx_cen          ;    // req  | clock enable                               | pulse    |           | info: see table (1) ; width: rx_clk
  input  logic                                      o_rx_ced          ;    // req  | clock enable for data                      | pulse    |           | info: see table (1) ; width: rx_cen
                                                                           //      |                                            |          |           |
  //                                                strob                  //------|--------------------------------------------|----------|-----------|----------------------------------------------------------------------//
                                                                           //      |                                            |          |           |
  input  logic    [                       2 :0 ]    o_rx_str_body     ;    // req  | frame body              / strob            | strob    |           | info : see purpose (6)
  input  logic    [ ETH_RX__lpW_RX_LEN   -1 :0 ]    o_rx_cnt_body     ;    // opt  | frame body              / strob counter    | count up |           |
  input  logic    [                       2 :0 ]    o_rx_str_mac      ;    // opt  | mac header              / strob            | strob    |           |
  input  logic    [                       3 :0 ]    o_rx_cnt_mac      ;    // opt  | mac header              / strob counter    | count up |           |
  input  logic    [                       2 :0 ]    o_rx_str_mda      ;    // opt  | mac destination address / strob            | strob    |           |
  input  logic    [                       2 :0 ]    o_rx_str_msa      ;    // opt  | mac source address      / strob            | strob    |           |
  input  logic    [                       2 :0 ]    o_rx_str_mlt      ;    // opt  | mac length or type      / strob            | strob    |           |
  input  logic    [                       2 :0 ]    o_rx_str_ip       ;    // opt  | ip header               / strob            | strob    |           |
  input  logic    [ ETH_RX__lpW_RX_IP_C  -1 :0 ]    o_rx_cnt_ip       ;    // opt  | ip header               / strob counter    | count up |           |
  input  logic    [                       2 :0 ]    o_rx_str_pay      ;    // opt  | payload & pad           / strob            | strob    |           | info: with crc strobe (!) ; see table (2)
  input  logic    [ ETH_RX__lpW_RX_LEN   -1 :0 ]    o_rx_cnt_pay      ;    // opt  | payload & pad           / strob counter    | count up |           |
  input  logic    [                       2 :0 ]    o_rx_str_gap      ;    // opt  | inter-frame gap         / strob            | strob    |           |
  input  logic    [ ETH_RX__pW_RX_GAP    -1 :0 ]    o_rx_cnt_gap      ;    // opt  | inter-frame gap         / strob counter    | count up |           |
                                                                           //      |                                            |          |           |
  //                                                data                   //------|--------------------------------------------|----------|-----------|----------------------------------------------------------------------//
                                                                           //      |                                            |          |           |
  input  logic                                      o_rx_dat_load     ;    // req  | frame body              / load (byte)      | pulse    |           | width: rx_cen
  input  logic    [                       7 :0 ]    o_rx_dat_body     ;    // req  | frame body              / data (byte)      | pdata    |           |
                                                                           //      |                                            |          |           |
  input  logic                                      o_rx_rdy_mac      ;    // opt  | mac header              / ready            | pulse    |           | width: rx_cen ; info: used for latch 'rx_dat_mac'
  input  logic    [                      47 :0 ]    o_rx_dat_mda      ;    // opt  | mac destination address / data             | pdata    |           |
  input  logic    [                      47 :0 ]    o_rx_dat_msa      ;    // opt  | mac source address      / data             | pdata    |           |
  input  logic    [                      15 :0 ]    o_rx_dat_mlt      ;    // opt  | mac length or type      / data             | pdata    |           |
                                                                           //      |                                            |          |           |
  input  logic                                      o_rx_rdy_ip       ;    // opt  | ip header               / ready            | pulse    |           | width: rx_cen ; info: used for latch 'rx_dat_ip'
  input  logic    [ ETH_RX__lpW_RX_IP    -1 :0 ]    o_rx_dat_ip       ;    // opt  | ip header               / data             | pdata    |           |
                                                                           //      |                                            |          |           |
  //                                                length                 //------|--------------------------------------------|----------|-----------|----------------------------------------------------------------------//
                                                                           //      |                                            |          |           |
  input  logic                                      o_rx_rdy_body     ;    // req  | frame body              / ready            | pulse    |           | width: rx_cen ; info: used for latch 'rx_len_body'
  input  logic    [ ETH_RX__lpW_RX_LEN   -1 :0 ]    o_rx_len_body     ;    // req  | frame body              / length (byte)    | pdata    |           |
  input  logic                                      o_rx_rdy_gap      ;    // opt  | inter-frame gap         / ready            | pulse    |           | width: rx_cen ; info: used for latch 'rx_len_gap'
  input  logic    [ ETH_RX__pW_RX_GAP    -1 :0 ]    o_rx_len_gap      ;    // opt  | inter-frame gap         / length (byte)    | pdata    |           |
                                                                           //      |                                            |          |           |
  //                                                fcs crc                //------|--------------------------------------------|----------|-----------|----------------------------------------------------------------------//
                                                                           //      |                                            |          |           |
  input  logic                                      o_rx_crc_rdy      ;    // req  | fcs crc-32              / ready            | pulse    |           | width: rx_cen ; info: used for latch 'rx_crc_err'
  input  logic                                      o_rx_crc_err      ;    // req  | fcs crc-32              / error            | strob    |           |
                                                                           //      |                                            |          |           |
  //---------------------------------------------// rx counters -----------//------|--------------------------------------------|----------|-----------|----------------------------------------------------------------------//
                                                                           //      |                                            |          |           |
  //                                                setting                //------|--------------------------------------------|----------|-----------|----------------------------------------------------------------------//
                                                                           //      |                                            |          |           |
  output logic    [                       1 :0 ]    i_rx_set_p_spd    ;    // opt  | speed selection                            | pdata    | def: 0    | info: see 'comment.txt' (6,10)
                                                                           //      |                                            |          |           |
                                                                           //      |   0 : payload                              |          |           | info: used 'o_rx_str_body' - 17 bytes
                                                                           //      |   1 : payload + mac                        |          |           | info: used 'o_rx_str_body' -  4 bytes
                                                                           //      |   2 : payload + mac + fcs crc              |          |           | info: used 'o_rx_str_body'
  output logic    [ ETH_RX__pN_RX_CNT       :1 ]                           //      |                                            |          |           |
                  [ ETH_RX__lpW_RX_LEN   -1 :0 ]    i_rx_set_p_top    ;    // opt  | top bound of body length (not frame)       | array    |           | lim: constant (!) ; info: see purpose (10), note (2) and 'comment.txt' (7)
  output logic    [ ETH_RX__pN_RX_CNT       :1 ]                           //      |                                            |          |           |
                  [ ETH_RX__lpW_RX_LEN   -1 :0 ]    i_rx_set_p_low    ;    // opt  | low bound of body length (not frame)       | array    |           | lim: constant (!) ; info: see purpose (10), note (2) and 'comment.txt' (7)
                                                                           //      |                                            |          |           |
  //                                                detector               //------|--------------------------------------------|----------|-----------|----------------------------------------------------------------------//
                                                                           //      |                                            |          |           |
  input  logic                                      o_rx_det_p_rdy    ;    // opt  | packet ready                               | pulse    |           | width: rx_cen
  input  logic                                      o_rx_det_p_err    ;    // opt  | packet error                               | strob    |           |
  input  logic                                      o_rx_det_p_cut    ;    // opt  | packet cut off (for oversize frame)        | pulse    |           | width: rx_cen
  output logic                                      i_rx_det_p_off    ;    // opt  | packet offcast (from buffer)               | pulse    | def: 0    | width: rx_cen ; lim: must appear before stop bit of frame ; see comment (8)
                                                                           //      |                                            |          |           |
  //                                                counters               //------|--------------------------------------------|----------|-----------|----------------------------------------------------------------------//
                                                                           //      |                                            |          |           |
  output logic                                      i_rx_cnt_p_clr    ;    // opt  | current value / clear                      | strob    | def: 0    | info: sensitivity to rising edge ; see 'comment.txt' (6)
                                                                           //      |                                            |          |           |
  input  logic                                      o_rx_cnt_p_rdy    ;    // opt  | current value / ready                      | pulse    |           | width: rx_cen
  input  logic    [ ETH_RX__pN_RX_CNT       :0 ]                           //      |                                            |          |           |
                  [ ETH_RX__pW_RX_CNT_P  -1 :0 ]    o_rx_cnt_p_all    ;    // opt  | current value / packet all                 | count up |           | info: see comment (9)
  input  logic    [ ETH_RX__pN_RX_CNT       :0 ]                           //      |                                            |          |           |
                  [ ETH_RX__pW_RX_CNT_P  -1 :0 ]    o_rx_cnt_p_err    ;    // opt  | current value / packet error               | count up |           | info: see comment (9)
  input  logic    [ ETH_RX__pN_RX_CNT       :0 ]                           //      |                                            |          |           |
                  [ ETH_RX__pW_RX_CNT_P  -1 :0 ]    o_rx_cnt_p_off    ;    // opt  | current value / packet offcast             | count up |           | info: see comment (9)
                                                                           //      |                                            |          |           |
  input  logic    [ ETH_RX__pW_RX_CNT_S  -1 :0 ]    o_rx_cnt_p_spd    ;    // opt  | current value / packet speed               | count up |           | info: byte counter ; see also 'i_rx_set_p_spd' and 'comment.txt' (9,10)
                                                                           //      |                                            |          |           |
  //                                                counters (rhythm)      //------|--------------------------------------------|----------|-----------|----------------------------------------------------------------------//
                                                                           //      |                                            |          |           |
  output logic                                      i_rx_rcnt_p_clr   ;    // opt  | periodic value / clear                     | strob    | def: 0    | info: sensitivity to rising edge ; see 'comment.txt' (6)
                                                                           //      |                                            |          |           |
  output logic                                      i_rx_rcnt_p_upd   ;    // opt  | periodic value / update                    | strob    | def: 0    | info: sensitivity to rising edge ; see 'comment.txt' (6)
                                                                           //      |                                            |          |           |
  input  logic                                      o_rx_rcnt_p_rdy   ;    // opt  | periodic value / ready                     | pulse    |           | width: rx_cen
  input  logic    [ ETH_RX__pN_RX_CNT       :0 ]                           //      |                                            |          |           |
                  [ ETH_RX__pW_RX_CNT_P  -1 :0 ]    o_rx_rcnt_p_all   ;    // opt  | periodic value / packet all                | pdata    |           | info: see comment (9)
  input  logic    [ ETH_RX__pN_RX_CNT       :0 ]                           //      |                                            |          |           |
                  [ ETH_RX__pW_RX_CNT_P  -1 :0 ]    o_rx_rcnt_p_err   ;    // opt  | periodic value / packet error              | pdata    |           | info: see comment (9)
  input  logic    [ ETH_RX__pN_RX_CNT       :0 ]                           //      |                                            |          |           |
                  [ ETH_RX__pW_RX_CNT_P  -1 :0 ]    o_rx_rcnt_p_off   ;    // opt  | periodic value / packet offcast            | pdata    |           | info: see comment (9)
                                                                           //      |                                            |          |           |
  input  logic    [ ETH_RX__pW_RX_CNT_S  -1 :0 ]    o_rx_rcnt_p_spd   ;    // opt  | periodic value / packet speed              | pdata    |           | info: byte counter ; see also 'i_rx_set_p_spd' and 'comment.txt' (9,10)
                                                                           //      |                                            |          |           |
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // logic
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  //----------------------------------------------// global ---------------------|

  var    logic                                       clk                         ;
  var    logic                                       cen                         ;
  var    logic                                       ced                         ;
  var    logic                                       sclr                        ;

  //----------------------------------------------// sync counters --------------|

  var    logic    [                       31 :0 ]    cnt_clk                 = 0 ;
  var    logic    [                       31 :0 ]    cnt_cen                 = 0 ;
  var    logic    [                       31 :0 ]    cnt_ced                 = 0 ;
  var    logic    [ ETH_RX__pW_RX_CNT_P   -1 :0 ]    cnt_pack                    ;

  //----------------------------------------------// reset ----------------------|

  var    logic                                       reset_n                 = 1 ;
  var    logic    [                       31 :0 ]    reset_n_cnt             = 0 ;

  //----------------------------------------------// setting --------------------|

  var    logic                                       set_cut                     ;
  var    logic                                       set_jumbo                   ;
  var    logic    [                        1 :0 ]    set_speed                   ;

  //----------------------------------------------// detector -------------------|

  var    logic                                       det_p_off               = 0 ;
  var    logic                                       det_p_off_sync              ;

  //----------------------------------------------// counters / clear -----------|

  var    logic                                       cnt_p_clr               = 0 ;
  var    logic                                       rcnt_p_clr              = 0 ;

  //----------------------------------------------// counters / update ----------|

  var    logic                                       rcnt_p_upd              = 0 ;
  var    logic    [                       31 :0 ]    rcnt_p_upd_cnt          = 0 ;
  var    logic                                       rcnt_p_upd_sync             ;

  //----------------------------------------------// counters / update (speed) --|

  var    logic                                       rcnt_p_upd_s                ;
  var    logic    [                       31 :0 ]    rcnt_p_upd_s_cnt        = 0 ;
  var    logic                                       rcnt_p_upd_s_nxt        = 0 ;
  var    logic                                       rcnt_p_upd_s_sync       = 0 ;
  var    logic    [ ETH_RX__pN_RX_CNT        :0 ]
                  [                       63 :0 ]    rx_speed                    ;

  //----------------------------------------------// counters / setting ---------|

  var    logic    [                        1 :0 ]    set_p_spd                   ;
  var    logic    [ ETH_RX__pN_RX_CNT        :1 ]
                  [ ETH_RX__lpW_RX_LEN    -1 :0 ]    set_p_top                   ;
  var    logic    [ ETH_RX__pN_RX_CNT        :1 ]
                  [ ETH_RX__lpW_RX_LEN    -1 :0 ]    set_p_low                   ;

  //----------------------------------------------// ethernet emulator ----------| [ eth_emu / tb_eth_emu_v2 ]

  //                                                 reset

  var    logic                                       eth_emu__i_reset_n          ;

  //                                                 setting

  var    logic                                       eth_emu__i_set_upd          ;
  var    logic                                       eth_emu__o_set_upd_ena      ;

  var    logic    [                        1 :0 ]    eth_emu__i_set_io_type      ;
  var    logic    [                        1 :0 ]    eth_emu__i_set_io_speed     ;
  var    logic                                       eth_emu__i_set_io_pause     ;

  var    logic                                       eth_emu__i_set_tx_mode      ;
  var    logic    [                       19 :0 ]    eth_emu__i_set_tx_speed     ;
  var    logic    [                       19 :0 ]    eth_emu__o_set_tx_speed     ;
  var    logic    [                       19 :0 ]    eth_emu__o_set_tx_limit     ;
  var    logic    [                        1 :0 ]    eth_emu__i_set_tx_sel       ;

  var    logic                                       eth_emu__i_set_err_ena      ;
  var    logic    [                        6 :0 ]    eth_emu__i_set_err_per      ;

  var    logic                                       eth_emu__i_set_mac_ena      ;
  var    logic    [                        2 :0 ]    eth_emu__i_set_mda_type     ;
  var    logic    [                       47 :0 ]    eth_emu__i_set_mda_const    ;
  var    logic    [                       47 :0 ]    eth_emu__i_set_mda_min      ;
  var    logic    [                       47 :0 ]    eth_emu__i_set_mda_max      ;
  var    logic    [                        2 :0 ]    eth_emu__i_set_msa_type     ;
  var    logic    [                       47 :0 ]    eth_emu__i_set_msa_const    ;
  var    logic    [                       47 :0 ]    eth_emu__i_set_msa_min      ;
  var    logic    [                       47 :0 ]    eth_emu__i_set_msa_max      ;
  var    logic    [                        2 :0 ]    eth_emu__i_set_mlt_type     ;
  var    logic    [                       15 :0 ]    eth_emu__i_set_mlt_const    ;
  var    logic    [                       15 :0 ]    eth_emu__i_set_mlt_min      ;
  var    logic    [                       15 :0 ]    eth_emu__i_set_mlt_max      ;

  var    logic                                       eth_emu__i_set_ip_ena       ;
  var    logic    [                        2 :0 ]    eth_emu__i_set_ip_type      ;
  var    logic    [ ETH_EMU__lpW_LAN_IP   -1 :0 ]    eth_emu__i_set_ip_const     ;
  var    logic    [ ETH_EMU__lpW_LAN_IP   -1 :0 ]    eth_emu__i_set_ip_min       ;
  var    logic    [ ETH_EMU__lpW_LAN_IP   -1 :0 ]    eth_emu__i_set_ip_max       ;

  var    logic    [                        2 :0 ]    eth_emu__i_set_pay_type     ;
  var    logic    [ ETH_EMU__pW_LAN_PAY   -1 :0 ]    eth_emu__i_set_pay_const    ;
  var    logic    [                        7 :0 ]    eth_emu__i_set_pay_min      ;
  var    logic    [                        7 :0 ]    eth_emu__i_set_pay_max      ;

  var    logic     [                       1 :0 ]    eth_emu__i_set_len_type     ;
  var    logic    [ ETH_EMU__pW_LAN_LEN   -1 :0 ]    eth_emu__i_set_len_const    ;
  var    logic    [ ETH_EMU__pW_LAN_LEN   -1 :0 ]    eth_emu__i_set_len_min      ;
  var    logic    [ ETH_EMU__pW_LAN_LEN   -1 :0 ]    eth_emu__i_set_len_max      ;

  var    logic    [                        1 :0 ]    eth_emu__i_set_gap_type     ;
  var    logic    [ ETH_EMU__pW_LAN_GAP   -1 :0 ]    eth_emu__i_set_gap_const    ;
  var    logic    [ ETH_EMU__pW_LAN_GAP   -1 :0 ]    eth_emu__i_set_gap_min      ;
  var    logic    [ ETH_EMU__pW_LAN_GAP   -1 :0 ]    eth_emu__i_set_gap_max      ;

  //                                                 lan packet

  var    logic                                       eth_emu__o_lan_clk          ;
  var    logic                                       eth_emu__o_lan_cen          ;
  var    logic                                       eth_emu__o_lan_ced          ;

  var    logic    [                        2 :0 ]    eth_emu__o_lan_str_frame    ;
  var    logic    [ ETH_EMU__pW_LAN_LEN   -1 :0 ]    eth_emu__o_lan_cnt_frame    ;
  var    logic    [                        2 :0 ]    eth_emu__o_lan_str_head     ;
  var    logic    [                        3 :0 ]    eth_emu__o_lan_cnt_head     ;
  var    logic    [                        2 :0 ]    eth_emu__o_lan_str_prm      ;
  var    logic    [                        2 :0 ]    eth_emu__o_lan_str_sfd      ;
  var    logic    [                        2 :0 ]    eth_emu__o_lan_str_body     ;
  var    logic    [ ETH_EMU__pW_LAN_LEN   -1 :0 ]    eth_emu__o_lan_cnt_body     ;
  var    logic    [                        2 :0 ]    eth_emu__o_lan_str_mac      ;
  var    logic    [                        3 :0 ]    eth_emu__o_lan_cnt_mac      ;
  var    logic    [                        2 :0 ]    eth_emu__o_lan_str_mda      ;
  var    logic    [                        2 :0 ]    eth_emu__o_lan_str_msa      ;
  var    logic    [                        2 :0 ]    eth_emu__o_lan_str_mlt      ;
  var    logic    [                        2 :0 ]    eth_emu__o_lan_str_ip       ;
  var    logic    [ ETH_EMU__lpW_LAN_IP_C -1 :0 ]    eth_emu__o_lan_cnt_ip       ;
  var    logic    [                        2 :0 ]    eth_emu__o_lan_str_pay      ;
  var    logic    [ ETH_EMU__pW_LAN_LEN   -1 :0 ]    eth_emu__o_lan_cnt_pay      ;
  var    logic    [                        2 :0 ]    eth_emu__o_lan_str_crc      ;
  var    logic    [                        2 :0 ]    eth_emu__o_lan_cnt_crc      ;
  var    logic    [                        2 :0 ]    eth_emu__o_lan_str_gap      ;
  var    logic    [ ETH_EMU__pW_LAN_GAP   -1 :0 ]    eth_emu__o_lan_cnt_gap      ;

  var    logic                                       eth_emu__o_lan_dat_load     ;
  var    logic    [                        7 :0 ]    eth_emu__o_lan_dat_frame    ;

  var    logic    [ ETH_EMU__pW_LAN_LEN   -1 :0 ]    eth_emu__o_lan_len_frame    ;
  var    logic    [ ETH_EMU__pW_LAN_LEN   -1 :0 ]    eth_emu__o_lan_len_body     ;
  var    logic    [ ETH_EMU__pW_LAN_LEN   -1 :0 ]    eth_emu__o_lan_len_pay      ;
  var    logic    [ ETH_EMU__pW_LAN_GAP   -1 :0 ]    eth_emu__o_lan_len_gap      ;

  var    logic                                       eth_emu__o_lan_crc_rdy      ;
  var    logic                                       eth_emu__o_lan_crc_err      ;

  //                                                 lan counters
  var    logic    [ ETH_EMU__pN_LAN_CNT      :1 ]
                  [ ETH_EMU__pW_LAN_LEN   -1 :0 ]    eth_emu__i_lan_set_p_top    ;
  var    logic    [ ETH_EMU__pN_LAN_CNT      :1 ]
                  [ ETH_EMU__pW_LAN_LEN   -1 :0 ]    eth_emu__i_lan_set_p_low    ;

  var    logic                                       eth_emu__o_lan_det_p_rdy    ;
  var    logic                                       eth_emu__o_lan_det_p_err    ;

  var    logic                                       eth_emu__i_lan_cnt_p_clr    ;

  var    logic                                       eth_emu__o_lan_cnt_p_rdy    ;
  var    logic    [ ETH_EMU__pN_LAN_CNT      :0 ]
                  [ ETH_EMU__pW_LAN_CNT   -1 :0 ]    eth_emu__o_lan_cnt_p_all    ;
  var    logic    [ ETH_EMU__pN_LAN_CNT      :0 ]
                  [ ETH_EMU__pW_LAN_CNT   -1 :0 ]    eth_emu__o_lan_cnt_p_err    ;

  //                                                 lan interface

  var    logic    [                        3 :0 ]    eth_emu__o_mii_txd          ;
  var    logic                                       eth_emu__o_mii_tx_en        ;
  var    logic                                       eth_emu__o_mii_tx_clk       ;

  var    logic    [                        1 :0 ]    eth_emu__o_rmii_txd         ;
  var    logic                                       eth_emu__o_rmii_tx_en       ;
  var    logic                                       eth_emu__o_rmii_tx_clk      ;
  var    logic                                       eth_emu__o_rmii_tx_clk_s    ;

  var    logic    [                        7 :0 ]    eth_emu__o_gmii_txd         ;
  var    logic                                       eth_emu__o_gmii_tx_en       ;
  var    logic                                       eth_emu__o_gmii_tx_clk      ;

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  //==========================================================================================================================================================================================================================//
  // body of module
  //==========================================================================================================================================================================================================================//

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // out logic
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  // reset

  assign  i_reset_n        = reset_n                   ;

  // setting

  assign  i_set_cut        = set_cut                   ;
  assign  i_set_jumbo      = set_jumbo                 ;
  assign  i_set_speed      = set_speed                 ;

  // rx counters / setting

  assign  i_rx_set_p_spd   = set_p_spd                 ;
  assign  i_rx_set_p_top   = set_p_top                 ;
  assign  i_rx_set_p_low   = set_p_low                 ;
  
  // rx counters / detector
    
  assign  i_rx_det_p_off   = det_p_off                 ;
  
  // rx counters / counters
    
  assign  i_rx_cnt_p_clr   = cnt_p_clr                 ;

  // rx counters / counters (rhythm)

  assign  i_rx_rcnt_p_clr  = rcnt_p_clr                ;
  assign  i_rx_rcnt_p_upd  = (lpS_RCNT_P_UPD == 0)     ? rcnt_p_upd : rcnt_p_upd_s ;

  // lan interface

  assign  i_mii_rxd        = eth_emu__o_mii_txd        ;
  assign  i_mii_rx_dv      = eth_emu__o_mii_tx_en      ;
  assign  i_mii_clk        = eth_emu__o_mii_tx_clk     ;

  assign  i_rmii_rxd       = eth_emu__o_rmii_txd       ;
  assign  i_rmii_crs_dv    = eth_emu__o_rmii_tx_en     ;
  assign  i_rmii_clk       = eth_emu__o_rmii_tx_clk    ;

  assign  i_gmii_rxd       = eth_emu__o_gmii_txd       ;
  assign  i_gmii_rx_dv     = eth_emu__o_gmii_tx_en     ;
  assign  i_gmii_clk       = eth_emu__o_gmii_tx_clk    ;

  // custom mode

  assign  i_cst_clk        = eth_emu__o_lan_clk        ;
  assign  i_cst_cen        = eth_emu__o_lan_cen        ;
  assign  i_cst_ced        = eth_emu__o_lan_ced        ;
  assign  i_cst_rxd        = eth_emu__o_lan_dat_frame  ;
  assign  i_cst_rx_dv      = eth_emu__o_lan_str_frame  ;

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // global
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  // clock

  assign  clk  = eth_emu__o_lan_clk ;
  assign  cen  = eth_emu__o_lan_cen ;
  assign  ced  = eth_emu__o_lan_ced ; // period = 1 byte

  // clear

  assign  sclr = ~reset_n ;

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // sync counters
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  // clk counter

  always_ff @(posedge clk)
    if (&cnt_clk == 0)
      cnt_clk <= cnt_clk + 1;

  // cen counter

  always_ff @(posedge clk)
    if (cen)
      begin
        if (&cnt_cen == 0)
          cnt_cen <= cnt_cen + 1;
      end

  // ced counter

  always_ff @(posedge clk)
    if (cen & ced) // period = 1 byte
      begin
        if (&cnt_ced == 0)
          cnt_ced <= cnt_ced + 1;
      end

  // packet counter

  assign  cnt_pack = o_rx_cnt_p_all[0];

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // reset
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  localparam bit  lpE_RESET_N = 0; // enable of reset generator

  // reset / counter

  always_ff @(posedge clk)
    if (&reset_n_cnt == 0)
      reset_n_cnt <= reset_n_cnt + 1;

  // reset / strob

  always_ff @(posedge clk)
    if (!lpE_RESET_N)
      reset_n <= 1'b1;
    else
      begin
        if (reset_n_cnt == 50_000)  reset_n <= 0 ; else // dimension: t.clk
        if (reset_n_cnt == 60_000)  reset_n <= 1 ;
      end

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // setting
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  assign  set_cut   = 1 ; // 0/1
  assign  set_jumbo = 0 ; // 0/1
  assign  set_speed = 0 ; // 0/1/2 : 1000/100/10 mbit/sec

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // rx counters / detector / offcast
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  localparam bit  lpE_DET_P_OFF = 0; // enable of packer offcast

  // offcast / pulse

  always_ff @(posedge clk)
    if (!lpE_DET_P_OFF)
      det_p_off <= 1'b0;
    else
      if (cen)
        begin
          if (det_p_off_sync)
            begin
              if (cnt_pack ==  50)  det_p_off <= 1 ; else // dimension: packet
              if (cnt_pack == 100)  det_p_off <= 1 ; else
              if (cnt_pack == 150)  det_p_off <= 1 ; else
              if (cnt_pack == 200)  det_p_off <= 1 ; else
              if (cnt_pack == 250)  det_p_off <= 1 ; else
              if (cnt_pack == 300)  det_p_off <= 1 ; else
              if (cnt_pack == 350)  det_p_off <= 1 ; else
              if (cnt_pack == 400)  det_p_off <= 1 ; else
              if (cnt_pack == 450)  det_p_off <= 1 ; else
              if (cnt_pack == 500)  det_p_off <= 1 ; else
                                    det_p_off <= 0 ;
            end
          else
            det_p_off <= 1'b0;
        end

  // offcast / sync

  assign  det_p_off_sync = eth_emu__o_lan_str_sfd [1]; // start bit of sfd / width = 1 cen

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // rx counters / counters / clear
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  localparam bit  lpE_CNT_P_CLR = 0; // enable of clear generator

  // clear / strob

  always_ff @(posedge clk)
    if (!lpE_CNT_P_CLR)
      cnt_p_clr <= 1'b0;
    else
      if (ced) // period = 1 byte
        begin
          if (cnt_ced == 100_000)  cnt_p_clr <= 1 ; else // dimension: byte
          if (cnt_ced == 105_000)  cnt_p_clr <= 0 ;
        end

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // rx counters / counters (rhythm) / clear
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  localparam bit  lpE_RCNT_P_CLR = 0; // enable of clear generator

  // clear / strob

  always_ff @(posedge clk)
    if (!lpE_RCNT_P_CLR)
      rcnt_p_clr <= 1'b0;
    else
      if (ced) // period = 1 byte
        begin
          if (cnt_ced == 2_946_569)  rcnt_p_clr <= 1 ; else // dimension: byte
          if (cnt_ced == 2_950_000)  rcnt_p_clr <= 0 ;
        end

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // rx counters / update / setting to packet counter
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  // update / settings

  localparam int  lpT_UDP = 1508; // period of 'rcnt_p_upd' / packets

  // update / period counter

  always_ff @(posedge clk)
    if (cen)
      begin
        if (rcnt_p_upd_sync)
          begin
            if (rcnt_p_upd_cnt == lpT_UDP - 1)
              rcnt_p_upd_cnt <= 0;
            else
              rcnt_p_upd_cnt <= rcnt_p_upd_cnt + 1;
          end
      end

  assign  rcnt_p_upd_sync = eth_emu__o_lan_str_sfd [1]; // start bit of sfd / width = 1 cen

  // update / pulse

  always_ff @(posedge clk)
    if (cen)
      begin
        rcnt_p_upd <= rcnt_p_upd_sync & (rcnt_p_upd_cnt == lpT_UDP - 1); // width = 1 cen
      end

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // rx counters /  update / setting to clock counter (for speed calculation)
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  // update / settings

  localparam int  lpT_CLK    = 400        ; // period of 'clk'          / ns / see table (1)
  localparam int  lpT_UDP_S  = 10_000_000 ; // period of 'rcnt_p_upd_s' / ns / def 10 ms

  localparam int  lpT_CNT    = lpT_UDP_S     / lpT_CLK   ; // period of counter
  localparam int  lpC_SEC    = 1_000_000_000 / lpT_UDP_S ; // coef for seconds

  // update / period counter

  always_ff @(posedge clk)
    if (rcnt_p_upd_s_cnt == lpT_CNT - 1)
      begin
        rcnt_p_upd_s_nxt <= 1'b1;
        rcnt_p_upd_s_cnt <= 0;
      end
    else
      begin
        rcnt_p_upd_s_nxt <= 1'b0;
        rcnt_p_upd_s_cnt <= rcnt_p_upd_s_cnt + 1;
      end

  // update / sync to clock ena

  always_ff @(posedge clk)
    if (rcnt_p_upd_s_nxt)
      rcnt_p_upd_s_sync <= 1'b1;
    else
      if (cen)
        begin
          rcnt_p_upd_s_sync <= 1'b0;
        end

  // update / strob

  gen_sstr // generator of single strob

  #(.pW_LEN   (3), // width of length   | bit
    .pN_REG_I (0), // number of reg in  | register
    .pN_REG_O (0)) // number of reg out | register

  gen_rcnt_p_upd_s (.i_clk(i_clk), .i_clk_en(cen), .i_sclr(0), .i_start(rcnt_p_upd_s_sync), .i_length(5), .o_strob(rcnt_p_upd_s) );
  //                                                                    ^                             ^            ^
  // update / speed calculation

  assign  rx_speed = (lpS_RCNT_P_UPD == 0) ? 0 : o_rx_rcnt_p_spd * lpC_SEC  // byte per second
                                                                 * 8        // bits per second
                                                                 / 1000 ;   // kbit per second

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // rx counters / setting
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  // speed selection

  assign  set_p_spd = 0 ; // 0 : payload
                          // 1 : payload + mac
                          // 2 : payload + mac + fcs crc

  // bounds of body length

  generate
    if (ETH_RX__pN_RX_CNT == 0) begin

      assign  set_p_low = '0;
      assign  set_p_top = '0;

    end
    else begin

      assign  set_p_low [1] = 64   ;
      assign  set_p_top [1] = 127  ;
      assign  set_p_low [2] = 128  ;
      assign  set_p_top [2] = 255  ;
      assign  set_p_low [3] = 256  ;
      assign  set_p_top [3] = 511  ;
      assign  set_p_low [4] = 512  ;
      assign  set_p_top [4] = 1023 ;
      assign  set_p_low [5] = 1024 ;
      assign  set_p_top [5] = 1518 ;
      assign  set_p_low [6] = 1519 ;
      assign  set_p_top [6] = 0    ; // oversize

    end
  endgenerate

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // testbench emulator of ethernet frames  [ eth_emu / tb_eth_emu_v2 ]  / see ' tb_gen - comment.txt' (2)
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  // setting / interface

  assign  eth_emu__i_set_io_type    = (ETH_RX__pS_IO_TYPE == "gmii") ? 0 :
                                      (ETH_RX__pS_IO_TYPE == "rmii") ? 1 :
                                      (ETH_RX__pS_IO_TYPE == "mii" ) ? 2 : 0;

  assign  eth_emu__i_set_io_speed   = set_speed ;

  // setting / transmit speed

  assign  eth_emu__i_set_tx_sel     = set_p_spd ;

  // lan counters / setting

  assign  eth_emu__i_lan_set_p_top  = set_p_top ;
  assign  eth_emu__i_lan_set_p_low  = set_p_low ;

  //--------------------------------------------------------------------------------------|
                                                          //                              |
  tb_eth_emu_v2          eth_emu                          //                              |
  (                                                       //                              |
  // reset -----------------------------------------------//------------------------------|
                                                          //           |                  |
     .i_reset_n          ( eth_emu__i_reset_n       ),    //  in       |       opt        |
                                                          //           |                  |
  // setting ---------------------------------------------//-----------|------------------|
                                                          //           |                  |
  // update              update                           //-----------|------------------|
                                                          //           |                  |
     .i_set_upd          ( eth_emu__i_set_upd       ),    //  in       |       opt        |
     .o_set_upd_ena      ( eth_emu__o_set_upd_ena   ),    //      out  |       opt        |
                                                          //           |                  |
  // interface           interface                        //-----------|------------------|
                                                          //           |                  |
     .i_set_io_type      ( eth_emu__i_set_io_type   ),    //  in       |  req             |
     .i_set_io_speed     ( eth_emu__i_set_io_speed  ),    //  in       |  req             |
     .i_set_io_pause     ( eth_emu__i_set_io_pause  ),    //  in       |       opt        |
                                                          //           |                  |
  // transmit speed      transmit speed                   //-----------|------------------|
                                                          //           |                  |
     .i_set_tx_mode      ( eth_emu__i_set_tx_mode   ),    //  in       |       opt        |
     .i_set_tx_speed     ( eth_emu__i_set_tx_speed  ),    //  in       |       opt        |
     .o_set_tx_speed     ( eth_emu__o_set_tx_speed  ),    //      out  |       opt        |
     .o_set_tx_limit     ( eth_emu__o_set_tx_limit  ),    //      out  |       opt        |
     .i_set_tx_sel       ( eth_emu__i_set_tx_sel    ),    //  in       |       opt        |
                                                          //           |                  |
  // crc error           crc error                        //-----------|------------------|
                                                          //           |                  |
     .i_set_err_ena      ( eth_emu__i_set_err_ena   ),    //  in       |       opt        |
     .i_set_err_per      ( eth_emu__i_set_err_per   ),    //  in       |       opt        |
                                                          //           |                  |
  // mac header          mac header                       //-----------|------------------|
                                                          //           |                  |
     .i_set_mac_ena      ( eth_emu__i_set_mac_ena   ),    //  in       |       opt        |
                                                          //           |                  |
     .i_set_mda_type     ( eth_emu__i_set_mda_type  ),    //  in       |       opt        |
     .i_set_mda_const    ( eth_emu__i_set_mda_const ),    //  in       |       opt        |
     .i_set_mda_min      ( eth_emu__i_set_mda_min   ),    //  in       |       opt        |
     .i_set_mda_max      ( eth_emu__i_set_mda_max   ),    //  in       |       opt        |
                                                          //           |                  |
     .i_set_msa_type     ( eth_emu__i_set_msa_type  ),    //  in       |       opt        |
     .i_set_msa_const    ( eth_emu__i_set_msa_const ),    //  in       |       opt        |
     .i_set_msa_min      ( eth_emu__i_set_msa_min   ),    //  in       |       opt        |
     .i_set_msa_max      ( eth_emu__i_set_msa_max   ),    //  in       |       opt        |
                                                          //           |                  |
     .i_set_mlt_type     ( eth_emu__i_set_mlt_type  ),    //  in       |       opt        |
     .i_set_mlt_const    ( eth_emu__i_set_mlt_const ),    //  in       |       opt        |
     .i_set_mlt_min      ( eth_emu__i_set_mlt_min   ),    //  in       |       opt        |
     .i_set_mlt_max      ( eth_emu__i_set_mlt_max   ),    //  in       |       opt        |
                                                          //           |                  |
  // ip header           ip header                        //-----------|------------------|
                                                          //           |                  |
     .i_set_ip_ena       ( eth_emu__i_set_ip_ena    ),    //  in       |       opt        |
     .i_set_ip_type      ( eth_emu__i_set_ip_type   ),    //  in       |       opt        |
     .i_set_ip_const     ( eth_emu__i_set_ip_const  ),    //  in       |       opt        |
     .i_set_ip_min       ( eth_emu__i_set_ip_min    ),    //  in       |       opt        |
     .i_set_ip_max       ( eth_emu__i_set_ip_max    ),    //  in       |       opt        |
                                                          //           |                  |
  // payload             payload                          //-----------|------------------|
                                                          //           |                  |
     .i_set_pay_type     ( eth_emu__i_set_pay_type  ),    //  in       |  req             |
     .i_set_pay_const    ( eth_emu__i_set_pay_const ),    //  in       |  req             |
     .i_set_pay_min      ( eth_emu__i_set_pay_min   ),    //  in       |       opt        |
     .i_set_pay_max      ( eth_emu__i_set_pay_max   ),    //  in       |       opt        |
                                                          //           |                  |
  // frame length        frame length                     //-----------|------------------|
                                                          //           |                  |
     .i_set_len_type     ( eth_emu__i_set_len_type  ),    //  in       |  req             |
     .i_set_len_const    ( eth_emu__i_set_len_const ),    //  in       |  req             |
     .i_set_len_min      ( eth_emu__i_set_len_min   ),    //  in       |       opt        |
     .i_set_len_max      ( eth_emu__i_set_len_max   ),    //  in       |       opt        |
                                                          //           |                  |
  // frame gap           frame gap                        //-----------|------------------|
                                                          //           |                  |
     .i_set_gap_type     ( eth_emu__i_set_gap_type  ),    //  in       |  req             |
     .i_set_gap_const    ( eth_emu__i_set_gap_const ),    //  in       |  req             |
     .i_set_gap_min      ( eth_emu__i_set_gap_min   ),    //  in       |       opt        |
     .i_set_gap_max      ( eth_emu__i_set_gap_max   ),    //  in       |       opt        |
                                                          //           |                  |
  // lan packet ------------------------------------------//-----------|------------------|
                                                          //           |                  |
  // clock               clock                            //-----------|------------------|
                                                          //           |                  |
     .o_lan_clk          ( eth_emu__o_lan_clk       ),    //      out  |  req             |
     .o_lan_cen          ( eth_emu__o_lan_cen       ),    //      out  |  req             |
     .o_lan_ced          ( eth_emu__o_lan_ced       ),    //      out  |  req             |
                                                          //           |                  |
  // strob               strob                            //-----------|------------------|
                                                          //           |                  |
     .o_lan_str_frame    ( eth_emu__o_lan_str_frame ),    //      out  |  req             |
     .o_lan_cnt_frame    ( eth_emu__o_lan_cnt_frame ),    //      out  |       opt        |
     .o_lan_str_head     ( eth_emu__o_lan_str_head  ),    //      out  |       opt        |
     .o_lan_cnt_head     ( eth_emu__o_lan_cnt_head  ),    //      out  |       opt        |
     .o_lan_str_prm      ( eth_emu__o_lan_str_prm   ),    //      out  |       opt        |
     .o_lan_str_sfd      ( eth_emu__o_lan_str_sfd   ),    //      out  |       opt        |
     .o_lan_str_body     ( eth_emu__o_lan_str_body  ),    //      out  |       opt        |
     .o_lan_cnt_body     ( eth_emu__o_lan_cnt_body  ),    //      out  |       opt        |
     .o_lan_str_mac      ( eth_emu__o_lan_str_mac   ),    //      out  |       opt        |
     .o_lan_cnt_mac      ( eth_emu__o_lan_cnt_mac   ),    //      out  |       opt        |
     .o_lan_str_mda      ( eth_emu__o_lan_str_mda   ),    //      out  |       opt        |
     .o_lan_str_msa      ( eth_emu__o_lan_str_msa   ),    //      out  |       opt        |
     .o_lan_str_mlt      ( eth_emu__o_lan_str_mlt   ),    //      out  |       opt        |
     .o_lan_str_ip       ( eth_emu__o_lan_str_ip    ),    //      out  |       opt        |
     .o_lan_cnt_ip       ( eth_emu__o_lan_cnt_ip    ),    //      out  |       opt        |
     .o_lan_str_pay      ( eth_emu__o_lan_str_pay   ),    //      out  |       opt        |
     .o_lan_cnt_pay      ( eth_emu__o_lan_cnt_pay   ),    //      out  |       opt        |
     .o_lan_str_crc      ( eth_emu__o_lan_str_crc   ),    //      out  |       opt        |
     .o_lan_cnt_crc      ( eth_emu__o_lan_cnt_crc   ),    //      out  |       opt        |
     .o_lan_str_gap      ( eth_emu__o_lan_str_gap   ),    //      out  |       opt        |
     .o_lan_cnt_gap      ( eth_emu__o_lan_cnt_gap   ),    //      out  |       opt        |
                                                          //           |                  |
  // data                data                             //-----------|------------------|
                                                          //           |                  |
     .o_lan_dat_load     ( eth_emu__o_lan_dat_load  ),    //      out  |  req             |
     .o_lan_dat_frame    ( eth_emu__o_lan_dat_frame ),    //      out  |  req             |
                                                          //           |                  |
  // length              length                           //-----------|------------------|
                                                          //           |                  |
     .o_lan_len_frame    ( eth_emu__o_lan_len_frame ),    //      out  |  req             |
     .o_lan_len_body     ( eth_emu__o_lan_len_body  ),    //      out  |       opt        |
     .o_lan_len_pay      ( eth_emu__o_lan_len_pay   ),    //      out  |       opt        |
     .o_lan_len_gap      ( eth_emu__o_lan_len_gap   ),    //      out  |       opt        |
                                                          //           |                  |
  // fcs crc             fcs crc                          //-----------|------------------|
                                                          //           |                  |
     .o_lan_crc_rdy      ( eth_emu__o_lan_crc_rdy   ),    //      out  |  req             |
     .o_lan_crc_err      ( eth_emu__o_lan_crc_err   ),    //      out  |  req             |
                                                          //           |                  |
  // lan counters ----------------------------------------//-----------|------------------|
                                                          //           |                  |
  // setting             setting                          //-----------|------------------|
                                                          //           |                  |
     .i_lan_set_p_top    ( eth_emu__i_lan_set_p_top ),    //  in       |       opt        |
     .i_lan_set_p_low    ( eth_emu__i_lan_set_p_low ),    //  in       |       opt        |
                                                          //           |                  |
  // detector            detector                         //-----------|------------------|
                                                          //           |                  |
     .o_lan_det_p_rdy    ( eth_emu__o_lan_det_p_rdy ),    //      out  |       opt        |
     .o_lan_det_p_err    ( eth_emu__o_lan_det_p_err ),    //      out  |       opt        |
                                                          //           |                  |
  // counters            counters                         //-----------|------------------|
                                                          //           |                  |
     .i_lan_cnt_p_clr    ( eth_emu__i_lan_cnt_p_clr ),    //  in       |       opt        |
     .o_lan_cnt_p_rdy    ( eth_emu__o_lan_cnt_p_rdy ),    //      out  |       opt        |
     .o_lan_cnt_p_all    ( eth_emu__o_lan_cnt_p_all ),    //      out  |       opt        |
     .o_lan_cnt_p_err    ( eth_emu__o_lan_cnt_p_err ),    //      out  |       opt        |
                                                          //           |                  |
  // lan interface ---------------------------------------//-----------|------------------|
                                                          //           |                  |
  // lan mii             lan mii                          //-----------|------------------|
                                                          //           |                  |
     .o_mii_txd          ( eth_emu__o_mii_txd       ),    //      out  |  req             |
     .o_mii_tx_en        ( eth_emu__o_mii_tx_en     ),    //      out  |  req             |
     .o_mii_tx_clk       ( eth_emu__o_mii_tx_clk    ),    //      out  |  req             |
                                                          //           |                  |
  // lan rmii            lan rmii                         //-----------|------------------|
                                                          //           |                  |
     .o_rmii_txd         ( eth_emu__o_rmii_txd      ),    //      out  |  req             |
     .o_rmii_tx_en       ( eth_emu__o_rmii_tx_en    ),    //      out  |  req             |
     .o_rmii_tx_clk      ( eth_emu__o_rmii_tx_clk   ),    //      out  |  req             |
     .o_rmii_tx_clk_s    ( eth_emu__o_rmii_tx_clk_s ),    //      out  |            test  |
                                                          //           |                  |
  // lan gmii            lan gmii                         //-----------|------------------|
                                                          //           |                  |
     .o_gmii_txd         ( eth_emu__o_gmii_txd      ),    //      out  |  req             |
     .o_gmii_tx_en       ( eth_emu__o_gmii_tx_en    ),    //      out  |  req             |
     .o_gmii_tx_clk      ( eth_emu__o_gmii_tx_clk   )     //      out  |  req             |
                                                          //           |                  |
  );//------------------------------------------------------------------------------------|
 
  // packet structure

  defparam  eth_emu.pE_LAN_HEAD  = ETH_EMU__pE_LAN_HEAD  ;
  defparam  eth_emu.pW_LAN_LEN   = ETH_EMU__pW_LAN_LEN   ;
  defparam  eth_emu.pW_LAN_GAP   = ETH_EMU__pW_LAN_GAP   ;
  defparam  eth_emu.pN_LAN_IPL   = ETH_EMU__pN_LAN_IPL   ;
  defparam  eth_emu.pN_LAN_IPH   = ETH_EMU__pN_LAN_IPH   ;
  defparam  eth_emu.pW_LAN_PAY   = ETH_EMU__pW_LAN_PAY   ;

  // packet counters

  defparam  eth_emu.pN_LAN_CNT   = ETH_EMU__pN_LAN_CNT   ;
  defparam  eth_emu.pW_LAN_CNT   = ETH_EMU__pW_LAN_CNT   ;

  // custom mode

  defparam  eth_emu.pE_CST_CLK   =         lpE_CST_CLK   ;
  defparam  eth_emu.pT_CST_CLK   = ETH_EMU__pT_CST_CLK   ;
  defparam  eth_emu.pT_CST_CEN   = ETH_EMU__pT_CST_CEN   ;
  defparam  eth_emu.pT_CST_CED   = ETH_EMU__pT_CST_CED   ;

  // simulation

  defparam  eth_emu.pS_SIM_GLTCH = ETH_EMU__pS_SIM_GLTCH ;

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // testbench emulator of ethernet frames / tb generator  [ tb_gen_emu / tb_gen_emu ]  / see ' tb_gen - comment.txt' (2,3)
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  `ifndef __emu_manual__ // see 'tb_top__define.sv'

  //--------------------------------------------------------------------------------------|
                                                          //                              |
  tb_gen_emu             tb_gen_emu                       //                              |
  (                                                       //                              |
  // reset -----------------------------------------------//------------------------------|
                                                          //           |                  |
     .i_reset_n          ( eth_emu__i_reset_n       ),    //  in       |       opt        |
                                                          //           |                  |
  // setting ---------------------------------------------//-----------|------------------|
                                                          //           |                  |
  // update              update                           //-----------|------------------|
                                                          //           |                  |
     .i_set_upd          ( eth_emu__i_set_upd       ),    //  in       |       opt        |
     .o_set_upd_ena      ( eth_emu__o_set_upd_ena   ),    //      out  |       opt        |
                                                          //           |                  |
  // interface           interface                        //-----------|------------------|
                                                          //           |                  |
     .i_set_io_type      ( /* not used */           ),    //  in       |  req             |
     .i_set_io_speed     ( /* not used */           ),    //  in       |  req             |
     .i_set_io_pause     ( eth_emu__i_set_io_pause  ),    //  in       |       opt        |
                                                          //           |                  |
  // transmit speed      transmit speed                   //-----------|------------------|
                                                          //           |                  |
     .i_set_tx_mode      ( eth_emu__i_set_tx_mode   ),    //  in       |       opt        |
     .i_set_tx_speed     ( eth_emu__i_set_tx_speed  ),    //  in       |       opt        |
     .o_set_tx_speed     ( eth_emu__o_set_tx_speed  ),    //      out  |       opt        |
     .o_set_tx_limit     ( eth_emu__o_set_tx_limit  ),    //      out  |       opt        |
     .i_set_tx_sel       ( /* not used */           ),    //  in       |       opt        |
                                                          //           |                  |
  // crc error           crc error                        //-----------|------------------|
                                                          //           |                  |
     .i_set_err_ena      ( eth_emu__i_set_err_ena   ),    //  in       |       opt        |
     .i_set_err_per      ( eth_emu__i_set_err_per   ),    //  in       |       opt        |
                                                          //           |                  |
  // mac header          mac header                       //-----------|------------------|
                                                          //           |                  |
     .i_set_mac_ena      ( eth_emu__i_set_mac_ena   ),    //  in       |       opt        |
                                                          //           |                  |
     .i_set_mda_type     ( eth_emu__i_set_mda_type  ),    //  in       |       opt        |
     .i_set_mda_const    ( eth_emu__i_set_mda_const ),    //  in       |       opt        |
     .i_set_mda_min      ( eth_emu__i_set_mda_min   ),    //  in       |       opt        |
     .i_set_mda_max      ( eth_emu__i_set_mda_max   ),    //  in       |       opt        |
                                                          //           |                  |
     .i_set_msa_type     ( eth_emu__i_set_msa_type  ),    //  in       |       opt        |
     .i_set_msa_const    ( eth_emu__i_set_msa_const ),    //  in       |       opt        |
     .i_set_msa_min      ( eth_emu__i_set_msa_min   ),    //  in       |       opt        |
     .i_set_msa_max      ( eth_emu__i_set_msa_max   ),    //  in       |       opt        |
                                                          //           |                  |
     .i_set_mlt_type     ( eth_emu__i_set_mlt_type  ),    //  in       |       opt        |
     .i_set_mlt_const    ( eth_emu__i_set_mlt_const ),    //  in       |       opt        |
     .i_set_mlt_min      ( eth_emu__i_set_mlt_min   ),    //  in       |       opt        |
     .i_set_mlt_max      ( eth_emu__i_set_mlt_max   ),    //  in       |       opt        |
                                                          //           |                  |
  // ip header           ip header                        //-----------|------------------|
                                                          //           |                  |
     .i_set_ip_ena       ( eth_emu__i_set_ip_ena    ),    //  in       |       opt        |
     .i_set_ip_type      ( eth_emu__i_set_ip_type   ),    //  in       |       opt        |
     .i_set_ip_const     ( eth_emu__i_set_ip_const  ),    //  in       |       opt        |
     .i_set_ip_min       ( eth_emu__i_set_ip_min    ),    //  in       |       opt        |
     .i_set_ip_max       ( eth_emu__i_set_ip_max    ),    //  in       |       opt        |
                                                          //           |                  |
  // payload             payload                          //-----------|------------------|
                                                          //           |                  |
     .i_set_pay_type     ( eth_emu__i_set_pay_type  ),    //  in       |  req             |
     .i_set_pay_const    ( eth_emu__i_set_pay_const ),    //  in       |  req             |
     .i_set_pay_min      ( eth_emu__i_set_pay_min   ),    //  in       |       opt        |
     .i_set_pay_max      ( eth_emu__i_set_pay_max   ),    //  in       |       opt        |
                                                          //           |                  |
  // frame length        frame length                     //-----------|------------------|
                                                          //           |                  |
     .i_set_len_type     ( eth_emu__i_set_len_type  ),    //  in       |  req             |
     .i_set_len_const    ( eth_emu__i_set_len_const ),    //  in       |  req             |
     .i_set_len_min      ( eth_emu__i_set_len_min   ),    //  in       |       opt        |
     .i_set_len_max      ( eth_emu__i_set_len_max   ),    //  in       |       opt        |
                                                          //           |                  |
  // frame gap           frame gap                        //-----------|------------------|
                                                          //           |                  |
     .i_set_gap_type     ( eth_emu__i_set_gap_type  ),    //  in       |  req             |
     .i_set_gap_const    ( eth_emu__i_set_gap_const ),    //  in       |  req             |
     .i_set_gap_min      ( eth_emu__i_set_gap_min   ),    //  in       |       opt        |
     .i_set_gap_max      ( eth_emu__i_set_gap_max   ),    //  in       |       opt        |
                                                          //           |                  |
  // lan packet ------------------------------------------//-----------|------------------|
                                                          //           |                  |
  // clock               clock                            //-----------|------------------|
                                                          //           |                  |
     .o_lan_clk          ( eth_emu__o_lan_clk       ),    //      out  |  req             |
     .o_lan_cen          ( eth_emu__o_lan_cen       ),    //      out  |  req             |
     .o_lan_ced          ( eth_emu__o_lan_ced       ),    //      out  |  req             |
                                                          //           |                  |
  // strob               strob                            //-----------|------------------|
                                                          //           |                  |
     .o_lan_str_frame    ( eth_emu__o_lan_str_frame ),    //      out  |  req             |
     .o_lan_cnt_frame    ( eth_emu__o_lan_cnt_frame ),    //      out  |       opt        |
     .o_lan_str_head     ( eth_emu__o_lan_str_head  ),    //      out  |       opt        |
     .o_lan_cnt_head     ( eth_emu__o_lan_cnt_head  ),    //      out  |       opt        |
     .o_lan_str_prm      ( eth_emu__o_lan_str_prm   ),    //      out  |       opt        |
     .o_lan_str_sfd      ( eth_emu__o_lan_str_sfd   ),    //      out  |       opt        |
     .o_lan_str_body     ( eth_emu__o_lan_str_body  ),    //      out  |       opt        |
     .o_lan_cnt_body     ( eth_emu__o_lan_cnt_body  ),    //      out  |       opt        |
     .o_lan_str_mac      ( eth_emu__o_lan_str_mac   ),    //      out  |       opt        |
     .o_lan_cnt_mac      ( eth_emu__o_lan_cnt_mac   ),    //      out  |       opt        |
     .o_lan_str_mda      ( eth_emu__o_lan_str_mda   ),    //      out  |       opt        |
     .o_lan_str_msa      ( eth_emu__o_lan_str_msa   ),    //      out  |       opt        |
     .o_lan_str_mlt      ( eth_emu__o_lan_str_mlt   ),    //      out  |       opt        |
     .o_lan_str_ip       ( eth_emu__o_lan_str_ip    ),    //      out  |       opt        |
     .o_lan_cnt_ip       ( eth_emu__o_lan_cnt_ip    ),    //      out  |       opt        |
     .o_lan_str_pay      ( eth_emu__o_lan_str_pay   ),    //      out  |       opt        |
     .o_lan_cnt_pay      ( eth_emu__o_lan_cnt_pay   ),    //      out  |       opt        |
     .o_lan_str_crc      ( eth_emu__o_lan_str_crc   ),    //      out  |       opt        |
     .o_lan_cnt_crc      ( eth_emu__o_lan_cnt_crc   ),    //      out  |       opt        |
     .o_lan_str_gap      ( eth_emu__o_lan_str_gap   ),    //      out  |       opt        |
     .o_lan_cnt_gap      ( eth_emu__o_lan_cnt_gap   ),    //      out  |       opt        |
                                                          //           |                  |
  // data                data                             //-----------|------------------|
                                                          //           |                  |
     .o_lan_dat_load     ( eth_emu__o_lan_dat_load  ),    //      out  |  req             |
     .o_lan_dat_frame    ( eth_emu__o_lan_dat_frame ),    //      out  |  req             |
                                                          //           |                  |
  // length              length                           //-----------|------------------|
                                                          //           |                  |
     .o_lan_len_frame    ( eth_emu__o_lan_len_frame ),    //      out  |  req             |
     .o_lan_len_body     ( eth_emu__o_lan_len_body  ),    //      out  |       opt        |
     .o_lan_len_pay      ( eth_emu__o_lan_len_pay   ),    //      out  |       opt        |
     .o_lan_len_gap      ( eth_emu__o_lan_len_gap   ),    //      out  |       opt        |
                                                          //           |                  |
  // fcs crc             fcs crc                          //-----------|------------------|
                                                          //           |                  |
     .o_lan_crc_rdy      ( eth_emu__o_lan_crc_rdy   ),    //      out  |  req             |
     .o_lan_crc_err      ( eth_emu__o_lan_crc_err   ),    //      out  |  req             |
                                                          //           |                  |
  // lan counters ----------------------------------------//-----------|------------------|
                                                          //           |                  |
  // setting             setting                          //-----------|------------------|
                                                          //           |                  |
     .i_lan_set_p_top    ( /* not used */           ),    //  in       |       opt        |
     .i_lan_set_p_low    ( /* not used */           ),    //  in       |       opt        |
                                                          //           |                  |
  // detector            detector                         //-----------|------------------|
                                                          //           |                  |
     .o_lan_det_p_rdy    ( eth_emu__o_lan_det_p_rdy ),    //      out  |       opt        |
     .o_lan_det_p_err    ( eth_emu__o_lan_det_p_err ),    //      out  |       opt        |
                                                          //           |                  |
  // counters            counters                         //-----------|------------------|
                                                          //           |                  |
     .i_lan_cnt_p_clr    ( eth_emu__i_lan_cnt_p_clr ),    //  in       |       opt        |
     .o_lan_cnt_p_rdy    ( eth_emu__o_lan_cnt_p_rdy ),    //      out  |       opt        |
     .o_lan_cnt_p_all    ( eth_emu__o_lan_cnt_p_all ),    //      out  |       opt        |
     .o_lan_cnt_p_err    ( eth_emu__o_lan_cnt_p_err ),    //      out  |       opt        |
                                                          //           |                  |
  // lan interface ---------------------------------------//-----------|------------------|
                                                          //           |                  |
  // lan mii             lan mii                          //-----------|------------------|
                                                          //           |                  |
     .o_mii_txd          ( eth_emu__o_mii_txd       ),    //      out  |  req             |
     .o_mii_tx_en        ( eth_emu__o_mii_tx_en     ),    //      out  |  req             |
     .o_mii_tx_clk       ( eth_emu__o_mii_tx_clk    ),    //      out  |  req             |
                                                          //           |                  |
  // lan rmii            lan rmii                         //-----------|------------------|
                                                          //           |                  |
     .o_rmii_txd         ( eth_emu__o_rmii_txd      ),    //      out  |  req             |
     .o_rmii_tx_en       ( eth_emu__o_rmii_tx_en    ),    //      out  |  req             |
     .o_rmii_tx_clk      ( eth_emu__o_rmii_tx_clk   ),    //      out  |  req             |
     .o_rmii_tx_clk_s    ( eth_emu__o_rmii_tx_clk_s ),    //      out  |            test  |
                                                          //           |                  |
  // lan gmii            lan gmii                         //-----------|------------------|
                                                          //           |                  |
     .o_gmii_txd         ( eth_emu__o_gmii_txd      ),    //      out  |  req             |
     .o_gmii_tx_en       ( eth_emu__o_gmii_tx_en    ),    //      out  |  req             |
     .o_gmii_tx_clk      ( eth_emu__o_gmii_tx_clk   )     //      out  |  req             |
                                                          //           |                  |
  );//------------------------------------------------------------------------------------|

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // testbench emulator of ethernet frames / tb generator (manual mode)  [ tb_gen_emu_m / tb_gen_emu_m ]  / see ' tb_gen - comment.txt' (2,3)
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  `elsif __emu_manual__ // see 'tb_gen__define.sv'

  //--------------------------------------------------------------------------------------|
                                                          //                              |
  tb_gen_emu_m           tb_gen_emu_m                     //                              |
  (                                                       //                              |
  // reset -----------------------------------------------//------------------------------|
                                                          //           |                  |
     .i_reset_n          ( eth_emu__i_reset_n       ),    //  in       |       opt        |
                                                          //           |                  |
  // setting ---------------------------------------------//-----------|------------------|
                                                          //           |                  |
  // update              update                           //-----------|------------------|
                                                          //           |                  |
     .i_set_upd          ( eth_emu__i_set_upd       ),    //  in       |       opt        |
     .o_set_upd_ena      ( eth_emu__o_set_upd_ena   ),    //      out  |       opt        |
                                                          //           |                  |
  // interface           interface                        //-----------|------------------|
                                                          //           |                  |
     .i_set_io_type      ( /* not used */           ),    //  in       |  req             |
     .i_set_io_speed     ( /* not used */           ),    //  in       |  req             |
     .i_set_io_pause     ( eth_emu__i_set_io_pause  ),    //  in       |       opt        |
                                                          //           |                  |
  // transmit speed      transmit speed                   //-----------|------------------|
                                                          //           |                  |
     .i_set_tx_mode      ( eth_emu__i_set_tx_mode   ),    //  in       |       opt        |
     .i_set_tx_speed     ( eth_emu__i_set_tx_speed  ),    //  in       |       opt        |
     .o_set_tx_speed     ( eth_emu__o_set_tx_speed  ),    //      out  |       opt        |
     .o_set_tx_limit     ( eth_emu__o_set_tx_limit  ),    //      out  |       opt        |
     .i_set_tx_sel       ( /* not used */           ),    //  in       |       opt        |
                                                          //           |                  |
  // crc error           crc error                        //-----------|------------------|
                                                          //           |                  |
     .i_set_err_ena      ( eth_emu__i_set_err_ena   ),    //  in       |       opt        |
     .i_set_err_per      ( eth_emu__i_set_err_per   ),    //  in       |       opt        |
                                                          //           |                  |
  // mac header          mac header                       //-----------|------------------|
                                                          //           |                  |
     .i_set_mac_ena      ( eth_emu__i_set_mac_ena   ),    //  in       |       opt        |
                                                          //           |                  |
     .i_set_mda_type     ( eth_emu__i_set_mda_type  ),    //  in       |       opt        |
     .i_set_mda_const    ( eth_emu__i_set_mda_const ),    //  in       |       opt        |
     .i_set_mda_min      ( eth_emu__i_set_mda_min   ),    //  in       |       opt        |
     .i_set_mda_max      ( eth_emu__i_set_mda_max   ),    //  in       |       opt        |
                                                          //           |                  |
     .i_set_msa_type     ( eth_emu__i_set_msa_type  ),    //  in       |       opt        |
     .i_set_msa_const    ( eth_emu__i_set_msa_const ),    //  in       |       opt        |
     .i_set_msa_min      ( eth_emu__i_set_msa_min   ),    //  in       |       opt        |
     .i_set_msa_max      ( eth_emu__i_set_msa_max   ),    //  in       |       opt        |
                                                          //           |                  |
     .i_set_mlt_type     ( eth_emu__i_set_mlt_type  ),    //  in       |       opt        |
     .i_set_mlt_const    ( eth_emu__i_set_mlt_const ),    //  in       |       opt        |
     .i_set_mlt_min      ( eth_emu__i_set_mlt_min   ),    //  in       |       opt        |
     .i_set_mlt_max      ( eth_emu__i_set_mlt_max   ),    //  in       |       opt        |
                                                          //           |                  |
  // ip header           ip header                        //-----------|------------------|
                                                          //           |                  |
     .i_set_ip_ena       ( eth_emu__i_set_ip_ena    ),    //  in       |       opt        |
     .i_set_ip_type      ( eth_emu__i_set_ip_type   ),    //  in       |       opt        |
     .i_set_ip_const     ( eth_emu__i_set_ip_const  ),    //  in       |       opt        |
     .i_set_ip_min       ( eth_emu__i_set_ip_min    ),    //  in       |       opt        |
     .i_set_ip_max       ( eth_emu__i_set_ip_max    ),    //  in       |       opt        |
                                                          //           |                  |
  // payload             payload                          //-----------|------------------|
                                                          //           |                  |
     .i_set_pay_type     ( eth_emu__i_set_pay_type  ),    //  in       |  req             |
     .i_set_pay_const    ( eth_emu__i_set_pay_const ),    //  in       |  req             |
     .i_set_pay_min      ( eth_emu__i_set_pay_min   ),    //  in       |       opt        |
     .i_set_pay_max      ( eth_emu__i_set_pay_max   ),    //  in       |       opt        |
                                                          //           |                  |
  // frame length        frame length                     //-----------|------------------|
                                                          //           |                  |
     .i_set_len_type     ( eth_emu__i_set_len_type  ),    //  in       |  req             |
     .i_set_len_const    ( eth_emu__i_set_len_const ),    //  in       |  req             |
     .i_set_len_min      ( eth_emu__i_set_len_min   ),    //  in       |       opt        |
     .i_set_len_max      ( eth_emu__i_set_len_max   ),    //  in       |       opt        |
                                                          //           |                  |
  // frame gap           frame gap                        //-----------|------------------|
                                                          //           |                  |
     .i_set_gap_type     ( eth_emu__i_set_gap_type  ),    //  in       |  req             |
     .i_set_gap_const    ( eth_emu__i_set_gap_const ),    //  in       |  req             |
     .i_set_gap_min      ( eth_emu__i_set_gap_min   ),    //  in       |       opt        |
     .i_set_gap_max      ( eth_emu__i_set_gap_max   ),    //  in       |       opt        |
                                                          //           |                  |
  // lan packet ------------------------------------------//-----------|------------------|
                                                          //           |                  |
  // clock               clock                            //-----------|------------------|
                                                          //           |                  |
     .o_lan_clk          ( eth_emu__o_lan_clk       ),    //      out  |  req             |
     .o_lan_cen          ( eth_emu__o_lan_cen       ),    //      out  |  req             |
     .o_lan_ced          ( eth_emu__o_lan_ced       ),    //      out  |  req             |
                                                          //           |                  |
  // strob               strob                            //-----------|------------------|
                                                          //           |                  |
     .o_lan_str_frame    ( eth_emu__o_lan_str_frame ),    //      out  |  req             |
     .o_lan_cnt_frame    ( eth_emu__o_lan_cnt_frame ),    //      out  |       opt        |
     .o_lan_str_head     ( eth_emu__o_lan_str_head  ),    //      out  |       opt        |
     .o_lan_cnt_head     ( eth_emu__o_lan_cnt_head  ),    //      out  |       opt        |
     .o_lan_str_prm      ( eth_emu__o_lan_str_prm   ),    //      out  |       opt        |
     .o_lan_str_sfd      ( eth_emu__o_lan_str_sfd   ),    //      out  |       opt        |
     .o_lan_str_body     ( eth_emu__o_lan_str_body  ),    //      out  |       opt        |
     .o_lan_cnt_body     ( eth_emu__o_lan_cnt_body  ),    //      out  |       opt        |
     .o_lan_str_mac      ( eth_emu__o_lan_str_mac   ),    //      out  |       opt        |
     .o_lan_cnt_mac      ( eth_emu__o_lan_cnt_mac   ),    //      out  |       opt        |
     .o_lan_str_mda      ( eth_emu__o_lan_str_mda   ),    //      out  |       opt        |
     .o_lan_str_msa      ( eth_emu__o_lan_str_msa   ),    //      out  |       opt        |
     .o_lan_str_mlt      ( eth_emu__o_lan_str_mlt   ),    //      out  |       opt        |
     .o_lan_str_ip       ( eth_emu__o_lan_str_ip    ),    //      out  |       opt        |
     .o_lan_cnt_ip       ( eth_emu__o_lan_cnt_ip    ),    //      out  |       opt        |
     .o_lan_str_pay      ( eth_emu__o_lan_str_pay   ),    //      out  |       opt        |
     .o_lan_cnt_pay      ( eth_emu__o_lan_cnt_pay   ),    //      out  |       opt        |
     .o_lan_str_crc      ( eth_emu__o_lan_str_crc   ),    //      out  |       opt        |
     .o_lan_cnt_crc      ( eth_emu__o_lan_cnt_crc   ),    //      out  |       opt        |
     .o_lan_str_gap      ( eth_emu__o_lan_str_gap   ),    //      out  |       opt        |
     .o_lan_cnt_gap      ( eth_emu__o_lan_cnt_gap   ),    //      out  |       opt        |
                                                          //           |                  |
  // data                data                             //-----------|------------------|
                                                          //           |                  |
     .o_lan_dat_load     ( eth_emu__o_lan_dat_load  ),    //      out  |  req             |
     .o_lan_dat_frame    ( eth_emu__o_lan_dat_frame ),    //      out  |  req             |
                                                          //           |                  |
  // length              length                           //-----------|------------------|
                                                          //           |                  |
     .o_lan_len_frame    ( eth_emu__o_lan_len_frame ),    //      out  |  req             |
     .o_lan_len_body     ( eth_emu__o_lan_len_body  ),    //      out  |       opt        |
     .o_lan_len_pay      ( eth_emu__o_lan_len_pay   ),    //      out  |       opt        |
     .o_lan_len_gap      ( eth_emu__o_lan_len_gap   ),    //      out  |       opt        |
                                                          //           |                  |
  // fcs crc             fcs crc                          //-----------|------------------|
                                                          //           |                  |
     .o_lan_crc_rdy      ( eth_emu__o_lan_crc_rdy   ),    //      out  |  req             |
     .o_lan_crc_err      ( eth_emu__o_lan_crc_err   ),    //      out  |  req             |
                                                          //           |                  |
  // lan counters ----------------------------------------//-----------|------------------|
                                                          //           |                  |
  // setting             setting                          //-----------|------------------|
                                                          //           |                  |
     .i_lan_set_p_top    ( /* not used */           ),    //  in       |       opt        |
     .i_lan_set_p_low    ( /* not used */           ),    //  in       |       opt        |
                                                          //           |                  |
  // detector            detector                         //-----------|------------------|
                                                          //           |                  |
     .o_lan_det_p_rdy    ( eth_emu__o_lan_det_p_rdy ),    //      out  |       opt        |
     .o_lan_det_p_err    ( eth_emu__o_lan_det_p_err ),    //      out  |       opt        |
                                                          //           |                  |
  // counters            counters                         //-----------|------------------|
                                                          //           |                  |
     .i_lan_cnt_p_clr    ( eth_emu__i_lan_cnt_p_clr ),    //  in       |       opt        |
     .o_lan_cnt_p_rdy    ( eth_emu__o_lan_cnt_p_rdy ),    //      out  |       opt        |
     .o_lan_cnt_p_all    ( eth_emu__o_lan_cnt_p_all ),    //      out  |       opt        |
     .o_lan_cnt_p_err    ( eth_emu__o_lan_cnt_p_err ),    //      out  |       opt        |
                                                          //           |                  |
  // lan interface ---------------------------------------//-----------|------------------|
                                                          //           |                  |
  // lan mii             lan mii                          //-----------|------------------|
                                                          //           |                  |
     .o_mii_txd          ( eth_emu__o_mii_txd       ),    //      out  |  req             |
     .o_mii_tx_en        ( eth_emu__o_mii_tx_en     ),    //      out  |  req             |
     .o_mii_tx_clk       ( eth_emu__o_mii_tx_clk    ),    //      out  |  req             |
                                                          //           |                  |
  // lan rmii            lan rmii                         //-----------|------------------|
                                                          //           |                  |
     .o_rmii_txd         ( eth_emu__o_rmii_txd      ),    //      out  |  req             |
     .o_rmii_tx_en       ( eth_emu__o_rmii_tx_en    ),    //      out  |  req             |
     .o_rmii_tx_clk      ( eth_emu__o_rmii_tx_clk   ),    //      out  |  req             |
     .o_rmii_tx_clk_s    ( eth_emu__o_rmii_tx_clk_s ),    //      out  |            test  |
                                                          //           |                  |
  // lan gmii            lan gmii                         //-----------|------------------|
                                                          //           |                  |
     .o_gmii_txd         ( eth_emu__o_gmii_txd      ),    //      out  |  req             |
     .o_gmii_tx_en       ( eth_emu__o_gmii_tx_en    ),    //      out  |  req             |
     .o_gmii_tx_clk      ( eth_emu__o_gmii_tx_clk   )     //      out  |  req             |
                                                          //           |                  |
  );//------------------------------------------------------------------------------------|
 
  `endif

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  endmodule




  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Company      : Micran, Department of Telecommunications
  //
  //  Engineer     : Pogodaev Danil
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Library      : lib_ethernet / library of ethernet components
  //
  //  File Name    : eth_rx_crc_v2 .sv (rtl)
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Description  : CRC calculator for received ethernet frames:
  //
  //                  - calculating the crc-32 sum for body of input frame
  //                  - compare calculated sum and residual constant (32'h c704dd7b)
  //                  - generating the crc error signal
  //
  //                 Delay of calculation: (pN_REG_I + 3 + pV_DELAY + pN_REG_O) * period (i_clk_en).
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Note (1)     : Structure of ethernet frames (std. 802.3)
  //
  //                  - frame header preamble
  //                  - start-frame delimiter (sfd)
  //                  - destination mac address (mda)
  //                  - source mac address (msa)
  //                  - length of payload / type of protocol (mlt)
  //                  - mac control operation code (opcode)
  //                  - pause time
  //                  - data payload and pad
  //                  - frame check sequence (fcs) crc-32
  //                  - inter-frame gap
  //
  //  Note (2)     : There is an internal initialization reset (sclr_ini).
  //                 Therefore, the external reset is optional (i_sclr).
  //
  //  Note (3)     : Used additional input registers for sync clear (i_sclr_n) / def: 3 reg
  //                 Reason : metastability and high fan-out.
  //
  //  Note (4)     : Attribute "synthesis noprune"
  //  
  //                 A Verilog HDL synthesis attribute that prevents the Quartus II software from removing
  //                 a register that does not directly or indirectly feed a top-level output or bidir pin,
  //                 such as a fan-out free register.
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Table (1)    : Structure of ethernet frames (std. 802.3) for CRC calculator / see also note (1)
  //
  //                 |------------------------------------------------------------------------------------|
  //                 | frame header  |      mac header       |           data          |  fcs  | ethernet |
  //                 |---------------|-----------------------|-------------------------|-------| rx crc   |
  //                 |       8       |          14           |         46-1500         |   4   | calc     |
  //                 |===============|=======================|=========================|=======|==========|
  //                 |  prm  |  sfd  |  mda  |  msa  |  mlt  |  code  |  time  |  pad  |  crc  | strob    |
  //                 |-------|-------|-------|-------|-------|--------|--------|-------|-------| name     |
  //                 |   7   |   1   |   6   |   6   |   2   |   2    |   2    |  42   |   4   |          |
  //                 |=======|=======|=======|=======|=======|========|========|=======|=======|==========|
  //                 |       |       |       |       |       |        |        |       |       |          |
  //                 |       |       | ***** | ***** | ***** | ****** | ****** | ***** | ***** | str_body |
  //                 |       |       |       |       |       |        |        |       |       |          |
  //                 |------------------------------------------------------------------------------------|
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Limitation   : Attribute "synthesis noprune" used for Altera only (parameter 'pE_GNPR')
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Attached (1) : Additional (detailed) code comments / eth_rx_crc_v2 - comment .txt
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Include Files
  //  
  //  Category       Type       Name               Ver     Description
  //
  //  lib_ethernet   function   eth_crc32_8d .sv   1.0.0   calculator of crc-32 (data 8 bit)
  //
  //  lib_global     function   cbit         .sv   1.0.0   bits calculator for input number (round up)
  //  lib_global     rtl        gen_npr      .sv   1.0.0   generator of registers with attribute "noprune"
  //  lib_global     rtl        det_se       .sv   1.0.0   detector of single edge for input strobe
  //  lib_global     rtl        z_line       .sv   1.0.0   delay line for serial and parallel data
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Revision List
  //
  //  Version   Date       Author
  //
  //  2.0.0     26.09.16   Pogodaev Danil
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  module eth_rx_crc_v2 (

  //     clock

         i_clk         ,
         i_clk_en      ,
         i_sclr_n      ,

  //     frame body

         i_str_body    ,
         i_dat_body    ,
         i_dat_load    ,

  //     fcs crc

         o_crc_rdy     ,
         o_crc_err    );

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // include files : functions
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
                                                  //      |
  `include "eth_crc32_8d.sv"                      // req  | calculator of crc-32 (data 8 bit)
  `include "cbit.sv"                              // req  | bits calculator for input number (round up)
                                                  //      |
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // parameters : setting
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
                                                  //      |                                    |          |         |
  // crc comparator ------------------------------//------|------------------------------------|----------|---------|---------------------------------------------------------------------------------------------------------//
                                                  //      |                                    |          |         |
  parameter int  pV_DELAY  = 0               ;    // opt  | value of delay for comparator      | t.clk_en | def: 0  | info: used for fpga with low speed grade (set 1 to 3) ; see 'comment.txt' (1)
                                                  //      |                                    |          |         |
  // in/out registers ----------------------------//------|------------------------------------|----------|---------|---------------------------------------------------------------------------------------------------------//
                                                  //      |                                    |          |         |
  parameter int  pN_REG_I  = 1               ;    // opt  | number of input registers          | register | def: 1  | info: 1 reg = 1 t.clk_en
  parameter int  pN_REG_O  = 0               ;    // opt  | number of output registers         | register | def: 0  | info: 1 reg = 1 t.clk_en
                                                  //      |                                    |          |         |
  // signal tap (for tetsing) --------------------//------|------------------------------------|----------|---------|---------------------------------------------------------------------------------------------------------//
                                                  //      |                                    |          |         |
  parameter bit  pE_GNPR   = 0               ;    // test | generator of reg with "noprune"    | on/off   | def: 0  | info: see note (4), for altera only (!)
                                                  //      |                                    |          |         |
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // interface
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
                                                  //      |                                    |          |         |
  //------------------------// clock -------------//------|------------------------------------|----------|---------|---------------------------------------------------------------------------------------------------------//
                                                  //      |                                    |          |         |
  input  logic                 i_clk         ;    // req  | clock                              | clock    |         |
  input  logic                 i_clk_en      ;    // opt  | clock enable                       | pulse    | def: 1  | width: clk
  input  logic                 i_sclr_n      ;    // opt  | sync clear (active low)            | strob    | def: 1  | info: see note (2,3)
                                                  //      |                                    |          |         |
  //------------------------// frame body --------//------|------------------------------------|----------|---------|---------------------------------------------------------------------------------------------------------//
                                                  //      |                                    |          |         |
  input  logic                 i_str_body    ;    // req  | frame body / strob                 | strob    |         | info: without frame header (!)
  input  logic    [  7 :0 ]    i_dat_body    ;    // req  | frame body / data (byte)           | pdata    |         |
  input  logic                 i_dat_load    ;    // req  | frame body / load (byte)           | pulse    |         | width: clk_en
                                                  //      |                                    |          |         |
  //------------------------// fcs crc -----------//------|------------------------------------|----------|---------|---------------------------------------------------------------------------------------------------------//
                                                  //      |                                    |          |         |
  output logic                 o_crc_rdy     ;    // req  | fcs crc-32 / ready                 | pulse    |         | width: clk_en
  output logic                 o_crc_err     ;    // req  | fcs crc-32 / error                 | strob    |         |
                                                  //      |                                    |          |         |
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // logic
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  //------------------------// in/out reg --------|

  //                           clear (ri)

  var    logic                 ri_sclr_n          ;

  //                           frame body (ri)

  var    logic                 ri_str_body        ;
  var    logic    [  7 :0 ]    ri_dat_body        ;
  var    logic                 ri_dat_load        ;

  //                           fcs crc (ro)
  
  var    logic                 ro_crc_rdy         ;
  var    logic                 ro_crc_err         ;

  //                           fcs crc (no)

  var    logic                 no_crc_rdy         ;
  var    logic                 no_crc_err         ;

  //------------------------// sync clear --------|

  var    logic                 sclr_n         = 0 ;
  var    logic                 sclr_n_ext         ;
  var    logic                 sclr_n_ini     = 0 ;
  var    logic    [  3 :0 ]    sclr_n_ini_cnt = 0 ;

  //------------------------// frame body --------|

  var    logic                 str_body           ;
  var    logic    [  7 :0 ]    dat_body           ;
  var    logic                 str_body_fe        ;
  var    logic                 str_body_fe_z      ;
   
  //------------------------// crc calculator ----|

  var    logic    [ 31 :0 ]    crc_sum        ='1 ;

  //------------------------// crc comparator ----|

  var    logic                 crc_check      = 0 ;
  var    logic                 crc_compare    = 0 ;
  var    logic                 crc_err        = 0 ;
  var    logic                 crc_rdy        = 0 ;

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  //==========================================================================================================================================================================================================================//
  // body of module
  //==========================================================================================================================================================================================================================//

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // out logic : output (after out reg)
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  assign  o_crc_rdy  = ro_crc_rdy ;
  assign  o_crc_err  = ro_crc_err ;

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // out logic : next to output (before out reg)
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  assign  no_crc_rdy  = crc_rdy ;
  assign  no_crc_err  = crc_err ;

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // sync clear
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  // external clear

  assign  sclr_n_ext = ri_sclr_n;

  z_line // delay line / (!) reason : metastability and high fan-out (!)

  #(.pV_DELAY (3), // value of delay | t.clk_en
    .pW_DATA  (1)) // width of data  | bit

  z__sclr_n (.i_clk(i_clk), .i_clk_en(1'b1), .i_sclr(1'b0), .i_data(i_sclr_n), .o_data(ri_sclr_n) );
  //                                                                ^                  ^
  // initialization clear

  always_ff @(posedge i_clk)
    begin
      sclr_n_ini <= (sclr_n_ini_cnt[3] == 1);

      if (sclr_n_ini_cnt[3] == 0)
        sclr_n_ini_cnt <= sclr_n_ini_cnt + 1;
    end
  
  // result clear

  always_ff @(posedge i_clk)
    sclr_n <= sclr_n_ini & sclr_n_ext;

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // frame body
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  assign  str_body  = ri_str_body ;
  assign  dat_body  = ri_dat_body ;
  assign  dat_load  = ri_dat_load ;

  det_se // detector of falling edge

  #(.pS_EDGE  (0), // edge setting
    .pN_REG_I (0), // number of reg in  | t.clk_en
    .pN_REG_O (0)) // number of reg out | t.clk_en

  fe__str_body (.i_clk(i_clk), .i_clk_en(i_clk_en), .i_sclr(~sclr_n), .i_strob(str_body), .o_edge(str_body_fe) );
  //                                                                           ^                  ^
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // crc calculator
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  always_ff @(posedge i_clk)
    if (~sclr_n)
      crc_sum <= '1; // (!)
    else
      if (i_clk_en)
        begin
          if (crc_rdy)
            crc_sum <= '1; // (!)
          else
            if (str_body)
              begin
                if (dat_load) // period = 1 byte
                  begin
                    crc_sum <= eth_crc32_8d (crc_sum, dat_body);
                  end
              end
        end

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // crc comparator
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  // crc check

  always_ff @(posedge i_clk)
    if (~sclr_n)
      crc_check <= 1'b0;
    else
      if (i_clk_en)
        crc_check <= str_body_fe_z; // delay for comparator

  z_line // delay line

  #(.pV_DELAY (pV_DELAY), // value of delay | t.clk_en
    .pW_DATA  (1       )) // width of data  | bit

  z__str_body_fe (.i_clk(i_clk), .i_clk_en(i_clk_en), .i_sclr(~i_sclr_n), .i_data(str_body_fe), .o_data(str_body_fe_z));
  //                                                                              ^                     ^
  // crc compare

  always_ff @(posedge i_clk)
    if (~sclr_n)
      crc_compare <= 1'b0;
    else
      if (i_clk_en)
        crc_compare <= (crc_sum == 32'hc7_04_dd_7b); // residual constant

  // crc error

  always_ff @(posedge i_clk)
    if (~sclr_n)
      crc_err <= 1'b0;
    else
      if (i_clk_en)
        begin
          if (crc_check)
            crc_err <= (crc_compare == 0);
        end

  // crc ready

  always_ff @(posedge i_clk)
    if (~sclr_n)
      crc_rdy <= 1'b0;
    else
      if (i_clk_en)
        crc_rdy <= crc_check;

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // in/out registers : befor reg (i/no) / after reg (ri/ro)
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  // frame body ------------------------------// frame body --------------------------------------------------------//

  z_line #(.pV_DELAY (pN_REG_I), .pW_DATA (1) )  ri__str_body  ( i_clk, i_clk_en, ~sclr_n, i_str_body , ri_str_body );
  z_line #(.pV_DELAY (pN_REG_I), .pW_DATA (1) )  ri__dat_load  ( i_clk, i_clk_en, ~sclr_n, i_dat_load , ri_dat_load );
  z_line #(.pV_DELAY (pN_REG_I), .pW_DATA (8) )  ri__dat_body  ( i_clk, i_clk_en, ~sclr_n, i_dat_body , ri_dat_body );
  //                  ^                                                                    ^            ^
  // fcs crc ---------------------------------// fcs crc -----------------------------------------------------------//

  z_line #(.pV_DELAY (pN_REG_O), .pW_DATA (1) )  ro__crc_rdy   ( i_clk, i_clk_en, ~sclr_n, no_crc_rdy , ro_crc_rdy  );
  z_line #(.pV_DELAY (pN_REG_O), .pW_DATA (1) )  ro__crc_err   ( i_clk, i_clk_en, ~sclr_n, no_crc_err , ro_crc_err  );
  //                  ^                                                                    ^            ^
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  //==========================================================================================================================================================================================================================//
  // testing (for signal tap)
  //==========================================================================================================================================================================================================================//

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // value of parameters
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  //-------------------------------// crc comparator -----------|

  logic    [ cbit(1,pV_DELAY) :0 ]    stp_pV_DELAY  = pV_DELAY  ;

  //-------------------------------// in/out registers ---------|

  logic    [ cbit(1,pN_REG_I) :0 ]    stp_pN_REG_I  = pN_REG_I  ;
  logic    [ cbit(1,pN_REG_O) :0 ]    stp_pN_REG_O  = pN_REG_O  ;

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // registers with attribute "noprune" : interface  (off: pN_REG = 0, on: pN_REG = 1)
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  generate if (pE_GNPR) begin: gen_npr__io

  // clock ---------------------------// clock ------------------------------//
 
  gen_npr #(.pN_REG (1), .pW_DATA (1) )  stp__i_clk_en    (i_clk, i_clk_en   );
  gen_npr #(.pN_REG (1), .pW_DATA (1) )  stp__i_sclr_n    (i_clk,   sclr_n   ); // not 'i'
  //                 ^                                              ^
  // frame body ----------------------// frame body -------------------------//
 
  gen_npr #(.pN_REG (1), .pW_DATA (1) )  stp__i_str_body  (i_clk, i_str_body );
  gen_npr #(.pN_REG (1), .pW_DATA (8) )  stp__i_dat_body  (i_clk, i_dat_body );
  gen_npr #(.pN_REG (1), .pW_DATA (1) )  stp__i_dat_load  (i_clk, i_dat_load );
  //                 ^                                            ^
  // fcs crc -------------------------// fcs crc ----------------------------//
 
  gen_npr #(.pN_REG (1), .pW_DATA (1) )  stp__o_crc_rdy   (i_clk, o_crc_rdy  );
  gen_npr #(.pN_REG (1), .pW_DATA (1) )  stp__o_crc_err   (i_clk, o_crc_err  );
  //                 ^                                            ^
  //-------------------------------------------------------------------------//
  end endgenerate

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // registers with attribute "noprune" : parameters  (off: pN_REG = 0, on: pN_REG = 1)
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  generate if (pE_GNPR) begin: gen_npr__par

  // crc comparator ---------------------------------// crc comparator ---------------------//

  gen_npr #(.pN_REG (0), .pW_DATA (cbit(1,pV_DELAY)) )  stp__pV_DELAY  (i_clk, stp_pV_DELAY );
  //                 ^                                                         ^
  // in/out registers -------------------------------// in/out registers -------------------//

  gen_npr #(.pN_REG (0), .pW_DATA (cbit(1,pN_REG_I)) )  stp__pN_REG_I  (i_clk, stp_pN_REG_I );
  gen_npr #(.pN_REG (0), .pW_DATA (cbit(1,pN_REG_O)) )  stp__pN_REG_O  (i_clk, stp_pN_REG_O );
  //                 ^                                                         ^
  //----------------------------------------------------------------------------------------//
  end endgenerate

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  endmodule

  //==========================================================================================================================================================================================================================//
  // instantiation template
  //==========================================================================================================================================================================================================================//
  /*
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // interface
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  //-----------------------// rx crc calculator ------| [ eth_rx_crc / eth_rx_crc_v2 ]

  //                          clock

  var    logic                eth_rx_crc__i_clk       ;
  var    logic                eth_rx_crc__i_clk_en    ;
  var    logic                eth_rx_crc__i_sclr_n    ;

  //                          frame body

  var    logic                eth_rx_crc__i_str_body  ;
  var    logic    [ 7 :0 ]    eth_rx_crc__i_dat_body  ;
  var    logic                eth_rx_crc__i_dat_load  ;

  //                          fcs crc

  var    logic                eth_rx_crc__o_crc_rdy   ;
  var    logic                eth_rx_crc__o_crc_err   ;

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // crc calculator for received ethernet frames  [ eth_rx_crc / eth_rx_crc_v2 ]
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  // clock

  assign  eth_rx_crc__i_clk       = ;
  assign  eth_rx_crc__i_clk_en    = ;
  assign  eth_rx_crc__i_sclr_n    = ;

  // frame body

  assign  eth_rx_crc__i_str_body  = ;
  assign  eth_rx_crc__i_dat_body  = ;
  assign  eth_rx_crc__i_dat_load  = ;

  //-------------------------------------------------------------------------------|
                                                   //                              |
  eth_rx_crc_v2     eth_rx_crc                     //                              |
  (                                                //                              |
  // clock ----------------------------------------//------------------------------|
                                                   //           |                  |
     .i_clk         ( eth_rx_crc__i_clk      ),    //  in       |  req             |
     .i_clk_en      ( eth_rx_crc__i_clk_en   ),    //  in       |       opt        |
     .i_sclr_n      ( eth_rx_crc__i_sclr_n   ),    //  in       |       opt        |
                                                   //           |                  |
  // frame body -----------------------------------//-----------|------------------|
                                                   //           |                  |
     .i_str_body    ( eth_rx_crc__i_str_body ),    //  in       |  req             |
     .i_dat_body    ( eth_rx_crc__i_dat_body ),    //  in       |  req             |
     .i_dat_load    ( eth_rx_crc__i_dat_load ),    //  in       |  req             |
                                                   //           |                  |
  // fcs crc --------------------------------------//-----------|------------------|
                                                   //           |                  |
     .o_crc_rdy     ( eth_rx_crc__o_crc_rdy  ),    //      out  |  req             |
     .o_crc_err     ( eth_rx_crc__o_crc_err  )     //      out  |  req             |
                                                   //           |                  |
  );//-----------------------------------------------------------------------------|

  // crc comparator

  defparam  eth_rx_crc.pV_DELAY  = 0; // def: 0

  // in/out registers

  defparam  eth_rx_crc.pN_REG_I  = 1; // def: 1
  defparam  eth_rx_crc.pN_REG_O  = 1; // def: 1

  // signal tap (for tetsing)

  defparam  eth_rx_crc.pE_GNPR   = 0; // for tetsing only

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  */




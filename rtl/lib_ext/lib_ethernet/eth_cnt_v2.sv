  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Company      : Micran, Department of Telecommunications
  //
  //  Engineer     : Pogodaev Danil
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Library      : lib_ethernet / library of ethernet components
  //
  //  File Name    : eth_cnt_v2 .sv (rtl)
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Description  : Counters of ethernet frames (packets)
  //
  //                  - packet all     / o_cnt_p_all
  //                  - packet error   / o_cnt_p_err
  //                  - packet offcast / o_cnt_p_off
  //                  - packet speed   / o_cnt_p_spd
  //
  //                 CRC calculator for ethernet frames
  //
  //                  - calculating the crc-32 sum for body of input frame
  //                  - compare calculated sum and residual constant (32'h c704dd7b)
  //                  - generating the crc error signal (o_det_p_err)
  //
  //                 Delay of counters ready (o_cnt_p_rdy): (6 + pV_DELAY + pN_REG_CNT) * period (i_clk_en)
  //
  //                 Delay of crc calculation (o_det_p_rdy): (5 + pV_DELAY) * period (i_clk_en)
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Note (1)     : Group counters for packet length (frame body)
  //
  //                 For example / SunLite GigE (Sunrise Telecom)
  //
  //                  - all size       / lan_cnt_p [0]
  //                  - 64-127 byte    / lan_cnt_p [1]
  //                  - 128-255 byte   / lan_cnt_p [2]
  //                  - 256-511 byte   / lan_cnt_p [3]
  //                  - 512-1023 byte  / lan_cnt_p [4]
  //                  - 1024-1518 byte / lan_cnt_p [5]
  //                  - oversize       / lan_cnt_p [6]
  //
  //  Note (2)     : There is an internal initialization reset (sclr_ini).
  //                 Therefore, the external reset is optional (i_sclr_n).
  //
  //  Note (3)     : Used additional input registers for these signals / def: 3 reg
  //
  //                  - i_sclr_n     / sync clear
  //                  - i_set_p_spd  / setting
  //                  - i_cnt_p_clr  / counters clear
  //                  - i_rcnt_p_clr / counters (rhythm) clear
  //                  - i_rcnt_p_upd / counters (rhythm) update
  //
  //                 Reason: metastability and high fan-out.
  //
  //  Note (4)     : Attribute "synthesis noprune"
  //  
  //                 A Verilog HDL synthesis attribute that prevents the Quartus II software from removing
  //                 a register that does not directly or indirectly feed a top-level output or bidir pin,
  //                 such as a fan-out free register.
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Limitation   : Attribute "synthesis noprune" used for Altera only (parameter 'pE_GEN_NPR')
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Attached (1) : Additional (detailed) code comments / eth_cnt_v2 - comment .txt
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Include Files
  //  
  //  Category       Type       Name                Ver     Description
  //
  //  lib_ethernet   function   eth_crc32_8d  .sv   1.0.0   calculator of crc-32 (data 8 bit)
  //
  //  lib_ethernet   rtl        eth_rx_crc_v2 .sv   2.0.0   crc calculator for received ethernet frames
  //
  //  lib_global     function   cbit          .sv   1.0.0   bits calculator for input number (round up)
  //  lib_global     rtl        gen_npr       .sv   1.0.0   generator of registers with attribute "noprune"
  //  lib_global     rtl        det_se        .sv   1.0.0   detector of single edge for input strobe
  //  lib_global     rtl        z_line        .sv   1.0.0   delay line for serial and parallel data
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Revision List
  //
  //  Version   Date       Author
  //
  //  2.0.0     26.09.16   Pogodaev Danil
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  module eth_cnt_v2    (

  //     clock

         i_clk         ,
         i_clk_en      ,
         i_sclr_n      ,

  //     frame body

         i_str_body    ,
         i_dat_body    ,
         i_dat_load    ,
         i_len_body    ,

  //     setting

         i_set_p_top   ,
         i_set_p_low   ,
         i_set_p_spd   ,

  //     detector

         o_det_p_rdy   ,
         o_det_p_err   ,
         i_det_p_off   ,

  //     counters

         i_cnt_p_clr   ,
         o_cnt_p_rdy   ,
         o_cnt_p_all   ,
         o_cnt_p_err   ,
         o_cnt_p_off   ,
         o_cnt_p_spd   ,

         i_rcnt_p_clr  ,
         i_rcnt_p_upd  ,
         o_rcnt_p_rdy  ,
         o_rcnt_p_all  ,
         o_rcnt_p_err  ,
         o_rcnt_p_off  ,
         o_rcnt_p_spd );

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // include files : functions
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
                                                            //      |
  `include "cbit.sv"                                        // req  | bits calculator for input number (round up)
                                                            //      |
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // parameters : setting
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
                                                            //      |                                             |          |          |
  // frame body --------------------------------------------//------|---------------------------------------------|----------|----------|-------------------------------------------------------------------------------------//
                                                            //      |                                             |          |          |
  parameter int  pW_LEN      = 11                      ;    // opt  | length of frame body / width of bus         | bit      | def: 11  | value: 14 (for jumbo-frame)
                                                            //      |                                             |          |          |
  // packet counters ---------------------------------------//------|---------------------------------------------|----------|----------|-------------------------------------------------------------------------------------//
                                                            //      |                                             |          |          |
  parameter int  pN_CNT      = 3                       ;    // opt  | counters groups / number                    | group    | def: 0   | info: see note (1) and 'comment.txt' (1)
  parameter int  pW_CNT_P    = 24                      ;    // req  | counters packet / width of bus              | bit      | def: 24  |
  parameter int  pW_CNT_S    = 32                      ;    // opt  | counters speed  / width of bus              | bit      | def: 32  | info: byte counter
                                                            //      |                                             |          |          |
  // crc comparator ----------------------------------------//------|---------------------------------------------|----------|----------|-------------------------------------------------------------------------------------//
                                                            //      |                                             |          |          |
  parameter int  pV_CRC_DLY  = 0                       ;    // opt  | value of delay for crc comporator           | t.clk_en | def: 0   | info: used for fpga with low speed grade (set 1 to 3) ; see 'comment.txt' (2)
                                                            //      |                                             |          |          |
  // signal tap (for tetsing) ------------------------------//------|---------------------------------------------|----------|----------|-------------------------------------------------------------------------------------//
                                                            //      |                                             |          |          |
  parameter bit  pE_GEN_NPR  = 0                       ;    // test | generator of registers with "noprune"       | on/off   | def: 0   | info: see note (4), for altera only (!)
                                                            //      |                                             |          |          |
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // parameters : local (for logic)
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
                                                            //      |                                             |          |          |
  // out registers -----------------------------------------//------|---------------------------------------------|----------|----------|-------------------------------------------------------------------------------------//
                                                            //      |                                             |          |          |
  parameter int  pN_REG_CNT  = 0                       ;    // opt  | number of output registers / for counters   | register | def: 0   | info: 1 reg = 1 t.clk_en ; see 'comment.txt' (3)
                                                            //      |                                             |          |          |
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // interface
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
                                                            //      |                                             |          |          |
  //---------------------------------// clock --------------//------|---------------------------------------------|----------|----------|-------------------------------------------------------------------------------------//
                                                            //      |                                             |          |          |
  input  logic                          i_clk          ;    // req  | clock                                       | clock    |          |
  input  logic                          i_clk_en       ;    // opt  | clock enable                                | pulse    | def: 1   | width: clk
  input  logic                          i_sclr_n       ;    // opt  | sync clear (active low)                     | strob    | def: 1   | info: see note (2,3) and 'comment.txt' (4)
                                                            //      |                                             |          |          |
  //---------------------------------// frame body ---------//------|---------------------------------------------|----------|----------|-------------------------------------------------------------------------------------//
                                                            //      |                                             |          |          |
  input  logic                          i_str_body     ;    // req  | frame body / strob                          | strob    |          | info: without frame header (!)
  input  logic    [           7 :0 ]    i_dat_body     ;    // req  | frame body / data (byte)                    | pdata    |          |
  input  logic                          i_dat_load     ;    // req  | frame body / load (byte)                    | pulse    |          | width: clk_en
  input  logic    [ pW_LEN   -1 :0 ]    i_len_body     ;    // opt  | frame body / length (byte)                  | pdata    |          | lim: must appear before the 'det_crc_rdy' ; info: see comment (5)
                                                            //      |                                             |          |          |
  //---------------------------------// setting ------------//------|---------------------------------------------|----------|----------|-------------------------------------------------------------------------------------//
                                                            //      |                                             |          |          |
  input  logic    [           1 :0 ]    i_set_p_spd    ;    // opt  | speed selection                             | pdata    | def: 0   | info: see 'comment.txt' (4,9)
                                                            //      |                                             |          |          |
                                                            //      |   0 : payload                               |          |          | info: used 'i_str_body' - 17 bytes
                                                            //      |   1 : payload + mac                         |          |          | info: used 'i_str_body' -  4 bytes
                                                            //      |   2 : payload + mac + fcs crc               |          |          | info: used 'i_str_body'
  input  logic    [ pN_CNT      :1 ]                        //      |                                             |          |          |
                  [ pW_LEN   -1 :0 ]    i_set_p_top    ;    // opt  | top bound of body length (not frame)        | array    |          | lim: constant (!) ; info: see note (1) and 'comment.txt' (6)
  input  logic    [ pN_CNT      :1 ]                        //      |                                             |          |          |
                  [ pW_LEN   -1 :0 ]    i_set_p_low    ;    // opt  | low bound of body length (not frame)        | array    |          | lim: constant (!) ; info: see note (1) and 'comment.txt' (6)
                                                            //      |                                             |          |          |
  //---------------------------------// detector -----------//------|---------------------------------------------|----------|----------|-------------------------------------------------------------------------------------//
                                                            //      |                                             |          |          |
  output logic                          o_det_p_rdy    ;    // req  | packet ready                                | pulse    |          | width: clk_en
  output logic                          o_det_p_err    ;    // req  | packet error                                | strob    |          |
  input  logic                          i_det_p_off    ;    // opt  | packet offcast (from buffer)                | pulse    | def: 0   | width: clk_en ; lim: must appear before the stop bit of frame (!) ; see comment (7)
                                                            //      |                                             |          |          |
  //---------------------------------// counters -----------//------|---------------------------------------------|----------|----------|-------------------------------------------------------------------------------------//
                                                            //      |                                             |          |          |
  input  logic                          i_cnt_p_clr    ;    // opt  | current value / clear                       | strob    | def: 0   | info: sensitivity to rising edge ; see 'comment.txt' (4)
                                                            //      |                                             |          |          |
  output logic                          o_cnt_p_rdy    ;    // req  | current value / ready                       | pulse    |          | width: clk_en
  output logic    [ pN_CNT      :0 ]                        //      |                                             |          |          |
                  [ pW_CNT_P -1 :0 ]    o_cnt_p_all    ;    // req  | current value / packet all                  | count up |          | info: see comment (8)
  output logic    [ pN_CNT      :0 ]                        //      |                                             |          |          |
                  [ pW_CNT_P -1 :0 ]    o_cnt_p_err    ;    // req  | current value / packet error                | count up |          | info: see comment (8)
  output logic    [ pN_CNT      :0 ]                        //      |                                             |          |          |
                  [ pW_CNT_P -1 :0 ]    o_cnt_p_off    ;    // opt  | current value / packet offcast              | count up |          | info: see comment (8)
  output logic    [ pN_CNT      :0 ]                        //      |                                             |          |          |
                  [ pW_CNT_S -1 :0 ]    o_cnt_p_spd    ;    // opt  | current value / packet speed                | count up |          | info: byte counter ; see also 'i_set_p_spd' and 'comment.txt' (8,9)
                                                            //      |                                             |          |          |
  //---------------------------------// counters (rhythm) --//------|---------------------------------------------|----------|----------|-------------------------------------------------------------------------------------//
                                                            //      |                                             |          |          |
  input  logic                          i_rcnt_p_clr   ;    // opt  | periodic value / clear                      | strob    | def: 0   | info: sensitivity to rising edge ; see 'comment.txt' (4)
                                                            //      |                                             |          |          |
  input  logic                          i_rcnt_p_upd   ;    // opt  | periodic value / update                     | strob    | def: 0   | info: sensitivity to rising edge ; see 'comment.txt' (4)
                                                            //      |                                             |          |          |
  output logic                          o_rcnt_p_rdy   ;    // opt  | periodic value / ready                      | pulse    |          | width: clk_en
  output logic    [ pN_CNT      :0 ]                        //      |                                             |          |          |
                  [ pW_CNT_P -1 :0 ]    o_rcnt_p_all   ;    // opt  | periodic value / packet all                 | pdata    |          | info: see comment (8)
  output logic    [ pN_CNT      :0 ]                        //      |                                             |          |          |
                  [ pW_CNT_P -1 :0 ]    o_rcnt_p_err   ;    // opt  | periodic value / packet error               | pdata    |          | info: see comment (8)
  output logic    [ pN_CNT      :0 ]                        //      |                                             |          |          |
                  [ pW_CNT_P -1 :0 ]    o_rcnt_p_off   ;    // opt  | periodic value / packet offcast             | pdata    |          | info: see comment (8)
  output logic    [ pN_CNT      :0 ]                        //      |                                             |          |          |
                  [ pW_CNT_S -1 :0 ]    o_rcnt_p_spd   ;    // opt  | periodic value / packet speed               | pdata    |          | info: byte counter ; see also 'i_set_p_spd' and 'comment.txt' (8,9)
                                                            //      |                                             |          |          |
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // logic
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  genvar                                j                     ;

  //---------------------------------// in logic -------------| [ after in reg ]

  //                                    clear

  var    logic                          ri_sclr_n             ;

  //                                    frame body

  var    logic                          ri_str_body           ;
  var    logic                          ri_str_body_fe        ;
  var    logic    [           7 :0 ]    ri_dat_body           ;
  var    logic                          ri_dat_load           ;
  var    logic    [ pW_LEN   -1 :0 ]    ri_len_body           ;

  //                                    setting

  var    logic    [           1 :0 ]    ri_set_p_spd          ;
  var    logic    [ pN_CNT      :1 ]
                  [ pW_LEN   -1 :0 ]    ri_set_p_top          ;
  var    logic    [ pN_CNT      :1 ]
                  [ pW_LEN   -1 :0 ]    ri_set_p_low          ;

  //                                    detector

  var    logic                          ri_det_p_off          ;

  //                                    counters

  var    logic                          ri_cnt_p_clr          ;
  var    logic                          ri_cnt_p_clr_re       ;

  var    logic                          ri_rcnt_p_clr         ;
  var    logic                          ri_rcnt_p_clr_re      ;

  var    logic                          ri_rcnt_p_upd         ;
  var    logic                          ri_rcnt_p_upd_re      ;

  //---------------------------------// out logic ------------| [ after out reg ]

  //                                    detector

  var    logic                          ro_det_p_rdy          ;
  var    logic                          ro_det_p_err          ;

  //                                    counters

  var    logic                          ro_cnt_p_rdy          ;
  var    logic    [ pN_CNT      :0 ]
                  [ pW_CNT_P -1 :0 ]    ro_cnt_p_all          ;
  var    logic    [ pN_CNT      :0 ]
                  [ pW_CNT_P -1 :0 ]    ro_cnt_p_err          ;
  var    logic    [ pN_CNT      :0 ]
                  [ pW_CNT_P -1 :0 ]    ro_cnt_p_off          ;
  var    logic    [ pN_CNT      :0 ]
                  [ pW_CNT_S -1 :0 ]    ro_cnt_p_spd          ;

  //                                    counters (rhythm)

  var    logic                          ro_rcnt_p_rdy         ;
  var    logic    [ pN_CNT      :0 ]
                  [ pW_CNT_P -1 :0 ]    ro_rcnt_p_all         ;
  var    logic    [ pN_CNT      :0 ]
                  [ pW_CNT_P -1 :0 ]    ro_rcnt_p_err         ;
  var    logic    [ pN_CNT      :0 ]
                  [ pW_CNT_P -1 :0 ]    ro_rcnt_p_off         ;
  var    logic    [ pN_CNT      :0 ]
                  [ pW_CNT_S -1 :0 ]    ro_rcnt_p_spd         ;

  //---------------------------------// out logic ------------| [ before out reg ]

  //                                    detector

  var    logic                          no_det_p_rdy          ;
  var    logic                          no_det_p_err          ;

  //                                    counters

  var    logic                          no_cnt_p_rdy          ;
  var    logic    [ pN_CNT      :0 ]
                  [ pW_CNT_P -1 :0 ]    no_cnt_p_all          ;
  var    logic    [ pN_CNT      :0 ]
                  [ pW_CNT_P -1 :0 ]    no_cnt_p_err          ;
  var    logic    [ pN_CNT      :0 ]
                  [ pW_CNT_P -1 :0 ]    no_cnt_p_off          ;
  var    logic    [ pN_CNT      :0 ]
                  [ pW_CNT_S -1 :0 ]    no_cnt_p_spd          ;

  //                                    counters (rhythm)

  var    logic                          no_rcnt_p_rdy         ;
  var    logic    [ pN_CNT      :0 ]
                  [ pW_CNT_P -1 :0 ]    no_rcnt_p_all         ;
  var    logic    [ pN_CNT      :0 ]
                  [ pW_CNT_P -1 :0 ]    no_rcnt_p_err         ;
  var    logic    [ pN_CNT      :0 ]
                  [ pW_CNT_P -1 :0 ]    no_rcnt_p_off         ;
  var    logic    [ pN_CNT      :0 ]
                  [ pW_CNT_S -1 :0 ]    no_rcnt_p_spd         ;

  //---------------------------------// sync clear -----------|

  var    logic                          sclr_n            = 0 ;
  var    logic                          sclr_n_ext            ;
  var    logic                          sclr_n_ini        = 0 ;
  var    logic    [           3 :0 ]    sclr_n_ini_cnt    = 0 ;

  //---------------------------------// count : detector -----|

  var    logic                          det_crc_err           ;
  var    logic                          det_crc_rdy           ;
  var    logic                          det_crc_rdy_z         ;

  //---------------------------------// count : update -------|

  var    logic                          rcnt_p_upd            ;
  var    logic                          rcnt_p_upd_z          ;
  var    logic                          rcnt_p_upd_ext        ;
  var    logic                          rcnt_p_upd_req    = 0 ;
  var    logic                          rcnt_p_upd_nxt        ;
  var    logic                          rcnt_p_upd_nxt_re     ;
  var    logic                          rcnt_p_upd_dis    = 0 ;

  //---------------------------------// count : clear --------|

  var    logic                          cnt_p_clr             ;
  var    logic                          cnt_p_clr_z           ;

  var    logic                          rcnt_p_clr            ;
  var    logic                          rcnt_p_clr_z          ;

  //---------------------------------// count : speed sel ----|

  var    logic    [           4 :0 ]    cnt_body_cut      = 0 ;
  var    logic                          cnt_body_cut_1    = 0 ;
  var    logic                          cnt_body_cut_2    = 0 ;

  var    logic                          str_body_spd      = 0 ;
  var    logic                          str_body_spd_nxt      ;
  var    logic                          dat_load_spd      = 0 ;
  var    logic                          dat_load_spd_nxt      ;

  //---------------------------------// count : len bounds ---|

  var    logic    [ pN_CNT      :1 ]    cnt_len_bound     = 0 ;
  var    logic    [ pN_CNT      :1 ]    cnt_len_bound_low = 0 ;
  var    logic    [ pN_CNT      :1 ]    cnt_len_bound_top = 0 ;

  //---------------------------------// count : ready --------|

  var    logic                          cnt_p_rdy             ;
  var    logic                          cnt_p_rdy_z           ;
  var    logic                          rcnt_p_rdy            ;

  //---------------------------------// count : packet all ---|
  var    logic    [ pN_CNT      :0 ]
                  [ pW_CNT_P -1 :0 ]    cnt_p_all         ='0 ;
  var    logic    [ pN_CNT      :0 ]
                  [ pW_CNT_P -1 :0 ]    rcnt_p_all        ='0 ;

  //---------------------------------// count : packet err ---|
  var    logic    [ pN_CNT      :0 ]
                  [ pW_CNT_P -1 :0 ]    cnt_p_err         ='0 ;
  var    logic    [ pN_CNT      :0 ]
                  [ pW_CNT_P -1 :0 ]    rcnt_p_err        ='0 ;

  //---------------------------------// count : packet off ---|
  
  var    logic                          det_p_off         ='0 ;
  var    logic    [ pN_CNT      :0 ]
                  [ pW_CNT_P -1 :0 ]    cnt_p_off         ='0 ;
  var    logic    [ pN_CNT      :0 ]
                  [ pW_CNT_P -1 :0 ]    rcnt_p_off        ='0 ;

  //---------------------------------// count : packet spd ---|
  var    logic    [ pN_CNT      :0 ]
                  [ pW_CNT_P -1 :0 ]    cnt_p_spd         ='0 ;
  var    logic    [ pN_CNT      :0 ]
                  [ pW_CNT_S -1 :0 ]    rcnt_p_spd        ='0 ;

  //---------------------------------// rx crc calculator ----| [ rx_crc / eth_rx_crc_v2 ]

  //                                    clock

  var    logic                          rx_crc__i_clk         ;
  var    logic                          rx_crc__i_clk_en      ;
  var    logic                          rx_crc__i_sclr_n      ;

  //                                    frame body

  var    logic                          rx_crc__i_str_body    ;
  var    logic    [           7 :0 ]    rx_crc__i_dat_body    ;
  var    logic                          rx_crc__i_dat_load    ;

  //                                    fcs crc

  var    logic                          rx_crc__o_crc_rdy     ;
  var    logic                          rx_crc__o_crc_err     ;

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  //==========================================================================================================================================================================================================================//
  // body of module
  //==========================================================================================================================================================================================================================//

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // out logic : output (after out reg)
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  // detector

  assign  o_det_p_rdy   = ro_det_p_rdy  ;
  assign  o_det_p_err   = ro_det_p_err  ;

  // counters

  assign  o_cnt_p_rdy   = ro_cnt_p_rdy  ;
  assign  o_cnt_p_all   = ro_cnt_p_all  ;
  assign  o_cnt_p_err   = ro_cnt_p_err  ;
  assign  o_cnt_p_off   = ro_cnt_p_off  ;
  assign  o_cnt_p_spd   = ro_cnt_p_spd  ;

  // counters (rhythm)

  assign  o_rcnt_p_rdy  = ro_rcnt_p_rdy ;
  assign  o_rcnt_p_all  = ro_rcnt_p_all ;
  assign  o_rcnt_p_err  = ro_rcnt_p_err ;
  assign  o_rcnt_p_off  = ro_rcnt_p_off ;
  assign  o_rcnt_p_spd  = ro_rcnt_p_spd ;

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // out logic : next to output (before out reg)
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  // detector

  assign  no_det_p_rdy   = det_crc_rdy ;
  assign  no_det_p_err   = det_crc_err ;

  // counters

  assign  no_cnt_p_rdy   = cnt_p_rdy   ;
  assign  no_cnt_p_all   = cnt_p_all   ;
  assign  no_cnt_p_err   = cnt_p_err   ;
  assign  no_cnt_p_off   = cnt_p_off   ;
  assign  no_cnt_p_spd   = cnt_p_spd   ;

  // counters (rhythm)

  assign  no_rcnt_p_rdy  = rcnt_p_rdy  ;
  assign  no_rcnt_p_all  = rcnt_p_all  ;
  assign  no_rcnt_p_err  = rcnt_p_err  ;
  assign  no_rcnt_p_off  = rcnt_p_off  ;
  assign  no_rcnt_p_spd  = rcnt_p_spd  ;

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // sync clear
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  // external clear

  assign  sclr_n_ext = ri_sclr_n;

  z_line // delay line / (!) reason : metastability and high fan-out (!)

  #(.pV_DELAY (3), // value of delay | t.clk_en
    .pW_DATA  (1)) // width of data  | bit

  z__i_sclr_n (.i_clk(i_clk), .i_clk_en(1'b1), .i_sclr(1'b0), .i_data(i_sclr_n), .o_data(ri_sclr_n) );
  //                                                                  ^                  ^
  // initialization clear

  always_ff @(posedge i_clk)
    begin
      sclr_n_ini <= (sclr_n_ini_cnt[3] == 1);

      if (sclr_n_ini_cnt[3] == 0)
        sclr_n_ini_cnt <= sclr_n_ini_cnt + 1;
    end
  
  // result clear

  always_ff @(posedge i_clk)
    sclr_n <= sclr_n_ini & sclr_n_ext;

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // counters : detector
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  assign  det_crc_rdy  = rx_crc__o_crc_rdy ;
  assign  det_crc_err  = rx_crc__o_crc_err ;

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // counters : update
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  // update / external

  assign  rcnt_p_upd_ext = ri_rcnt_p_upd_re; // width = clk_en

  z_line // delay line / (!) reason : metastability and high fan-out (!)

  #(.pV_DELAY (3), // value of delay | t.clk_en
    .pW_DATA  (1)) // width of data  | bit

  z__i_rcnt_p_upd (.i_clk(i_clk), .i_clk_en(1'b1), .i_sclr(1'b0), .i_data(i_rcnt_p_upd), .o_data(ri_rcnt_p_upd) );
  //                                                                      ^                      ^
  det_se // detector of rising edge

  #(.pS_EDGE  (1), // edge setting
    .pN_REG_I (1), // number of reg in  | t.clk_en
    .pN_REG_O (0)) // number of reg out | t.clk_en

  re__ri_rcnt_p_upd (.i_clk(i_clk), .i_clk_en(i_clk_en), .i_sclr(~sclr_n), .i_strob(ri_rcnt_p_upd), .o_edge(ri_rcnt_p_upd_re) );
  //                                                                                ^                       ^
  // update / request

  always_ff @(posedge i_clk)
    if (~sclr_n)
      rcnt_p_upd_req <= 1'b0;
    else
      if (i_clk_en)
        begin
          if (rcnt_p_upd_ext)
            rcnt_p_upd_req <= 1'b1;
          else
            if (rcnt_p_upd == 1)
              rcnt_p_upd_req <= 1'b0;
        end

  // update / next to

  assign  rcnt_p_upd_nxt = rcnt_p_upd_req & (rcnt_p_upd_dis == 0); // see 'comment.txt' (10)

  // update / result

  assign  rcnt_p_upd = rcnt_p_upd_nxt_re; // width = clk_en

  det_se // detector of rising edge

  #(.pS_EDGE  (1), // edge setting
    .pN_REG_I (0), // number of reg in  | t.clk_en
    .pN_REG_O (1)) // number of reg out | t.clk_en

  re__rcnt_p_upd_nxt (.i_clk(i_clk), .i_clk_en(i_clk_en), .i_sclr(~sclr_n), .i_strob(rcnt_p_upd_nxt), .o_edge(rcnt_p_upd_nxt_re) );
  //                                                                                 ^                        ^
  // update / disable / see 'comment.txt' (10)

  always_ff @(posedge i_clk)
    if (~sclr_n)
      rcnt_p_upd_dis <= 1'b0;
    else
      if (i_clk_en)
        begin
          if (ri_str_body_fe)
            rcnt_p_upd_dis <= 1'b1;
          else
            if (cnt_p_rdy)
              rcnt_p_upd_dis <= 1'b0;
        end

  det_se // detector of falling edge

  #(.pS_EDGE  (0), // edge setting
    .pN_REG_I (0), // number of reg in  | t.clk_en
    .pN_REG_O (0)) // number of reg out | t.clk_en

  fe__ri_str_body (.i_clk(i_clk), .i_clk_en(i_clk_en), .i_sclr(~sclr_n), .i_strob(ri_str_body), .o_edge(ri_str_body_fe) );
  //                                                                              ^                     ^
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // counters : clear (sensitivity to rising edge)
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  // counters

  assign cnt_p_clr = ri_cnt_p_clr_re; // width = clk_en

  z_line // delay line / (!) reason : metastability and high fan-out (!)

  #(.pV_DELAY (3), // value of delay | t.clk_en
    .pW_DATA  (1)) // width of data  | bit

  z__i_cnt_p_clr (.i_clk(i_clk), .i_clk_en(1'b1), .i_sclr(1'b0), .i_data(i_cnt_p_clr), .o_data(ri_cnt_p_clr) );
  //                                                                     ^                     ^
  det_se // detector of rising edge

  #(.pS_EDGE  (1), // edge setting
    .pN_REG_I (0), // number of reg in  | t.clk_en
    .pN_REG_O (0)) // number of reg out | t.clk_en

  re__ri_cnt_p_clr (.i_clk(i_clk), .i_clk_en(i_clk_en), .i_sclr(~sclr_n), .i_strob(ri_cnt_p_clr), .o_edge(ri_cnt_p_clr_re) );
  //                                                                               ^                      ^
  // counters (rhythm)

  assign rcnt_p_clr = ri_rcnt_p_clr_re; // width = clk_en

  z_line // delay line / (!) reason : metastability and high fan-out (!)

  #(.pV_DELAY (3), // value of delay | t.clk_en
    .pW_DATA  (1)) // width of data  | bit

  z__i_rcnt_p_clr (.i_clk(i_clk), .i_clk_en(1'b1), .i_sclr(1'b0), .i_data(i_rcnt_p_clr), .o_data(ri_rcnt_p_clr) );
  //                                                                      ^                      ^
  det_se // detector of rising edge

  #(.pS_EDGE  (1), // edge setting
    .pN_REG_I (0), // number of reg in  | t.clk_en
    .pN_REG_O (0)) // number of reg out | t.clk_en

  re__ri_rcnt_p_clr (.i_clk(i_clk), .i_clk_en(i_clk_en), .i_sclr(~sclr_n), .i_strob(ri_rcnt_p_clr), .o_edge(ri_rcnt_p_clr_re) );
  //                                                                                ^                       ^
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // counters : ready
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  // counters

  assign  cnt_p_rdy = det_crc_rdy_z; // 'det_crc_rdy' used for cnt increment, therefore used delayed signal (z)

  z_line // delay line (def: 2)

  #(.pV_DELAY (2), // value of delay | t.clk_en
    .pW_DATA  (1)) // width of data  | bit

  z__det_crc_rdy (.i_clk(i_clk), .i_clk_en(i_clk_en), .i_sclr(~sclr_n), .i_data(det_crc_rdy), .o_data(det_crc_rdy_z));
  //                                                                            ^                     ^
  // counters (rhythm)

  assign  rcnt_p_rdy = rcnt_p_upd_z; // 'rcnt_p_upd' used for latched 'cnt' to 'rcnt', therefore used delayed signal (z)

  z_line // delay line (def: 2)

  #(.pV_DELAY (2), // value of delay | t.clk_en
    .pW_DATA  (1)) // width of data  | bit

  z__rcnt_p_upd (.i_clk(i_clk), .i_clk_en(i_clk_en), .i_sclr(~sclr_n), .i_data(rcnt_p_upd), .o_data(rcnt_p_upd_z));
  //                                                                           ^                    ^
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // counters : speed selection
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  // setting

  z_line // delay line / (!) reason : metastability (!)

  #(.pV_DELAY (3), // value of delay | t.clk_en
    .pW_DATA  (2)) // width of data  | bit

  z__i_set_p_spd (.i_clk(i_clk), .i_clk_en(1'b1), .i_sclr(1'b0), .i_data(i_set_p_spd), .o_data(ri_set_p_spd) );
  //                                                                     ^                     ^
  // speed strob / cut counter

  always_ff @(posedge i_clk)
    if (~sclr_n)
      begin
        cnt_body_cut   <=  'd0;
        cnt_body_cut_1 <= 1'b0;
        cnt_body_cut_2 <= 1'b0;
      end
    else
      if (i_clk_en)
        begin
          if (ri_str_body)
            begin
              if (ri_dat_load) // period = 1 byte
                begin
                  if (cnt_body_cut < 17) // 14 (mac) + 4 (crc) - 1 = 17
                    cnt_body_cut <= cnt_body_cut + 1 ;

                  cnt_body_cut_1 <= cnt_body_cut >=  3 ; //            4 (crc) - 1 = 3
                  cnt_body_cut_2 <= cnt_body_cut >= 17 ; // 14 (mac) + 4 (crc) - 1 = 17
                end
            end
         else
           begin
             cnt_body_cut   <=  'd0;
             cnt_body_cut_1 <= 1'b0;
             cnt_body_cut_2 <= 1'b0;
           end
        end

  // speed strob / next to

  assign dat_load_spd_nxt = ri_dat_load ;
  assign str_body_spd_nxt = ri_str_body & ((ri_set_p_spd == 2) ? 1'b1           :        // select : payload + mac + fcs crc
                                           (ri_set_p_spd == 1) ? cnt_body_cut_1 :        // select : payload + mac
                                           (ri_set_p_spd == 0) ? cnt_body_cut_2 : 1'b0); // select : payload
  // speed strob

  always_ff @(posedge i_clk)
    if (~sclr_n)
      begin
        dat_load_spd <= 1'b0;
        str_body_spd <= 1'b0;
      end
    else
      if (i_clk_en)
        begin
          dat_load_spd <= dat_load_spd_nxt;
          str_body_spd <= str_body_spd_nxt;
        end

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // counters : bounds of length
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  always_ff @(posedge i_clk)
    if (~sclr_n)
      begin
        cnt_len_bound     <= '0;
        cnt_len_bound_low <= '0;
        cnt_len_bound_top <= '0;
      end
    else
      if (i_clk_en)
        begin
          for (int i=1; i<= pN_CNT; i++)
            begin
              cnt_len_bound_low[i] <= (ri_len_body >= ri_set_p_low[i]) ;
              cnt_len_bound_top[i] <= (ri_len_body <= ri_set_p_top[i]) | (ri_set_p_top[i] == 0); // '=0' used for oversize

              cnt_len_bound[i] <= cnt_len_bound_low[i] & cnt_len_bound_top[i]; // used for enable
            end
        end

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // counters : packet all
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  // counters

  always_ff @(posedge i_clk)
    if (~sclr_n)
      cnt_p_all <= '0;
    else
      if (i_clk_en)
        begin
          if (cnt_p_clr | rcnt_p_upd_z) // 'rcnt_p_upd' also used for latched 'cnt' to 'rcnt', therefore used delayed signal (z)
            cnt_p_all <= '0;
          else
            if (det_crc_rdy) // sync to crc ready
              begin
                cnt_p_all[0] <= cnt_p_all[0] + 1; // [0]: all size frame

                for (int i=1; i<= pN_CNT; i++)
                  begin
                    if (cnt_len_bound[i])
                      cnt_p_all[i] <= cnt_p_all[i] + 1;
                  end
              end
        end

  // counters (rhythm)

  always_ff @(posedge i_clk)
    if (~sclr_n)
      rcnt_p_all <= '0;
    else
      if (i_clk_en)
        begin
          if (rcnt_p_clr)
            rcnt_p_all <= '0;
          else
            if (rcnt_p_upd)
              begin
                for (int i=0; i<= pN_CNT; i++)
                  rcnt_p_all[i] <= cnt_p_all[i];
              end
        end

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // counters : packet error
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  // counter

  always_ff @(posedge i_clk)
    if (~sclr_n)
      cnt_p_err <= '0;
    else
      if (i_clk_en)
        begin
          if (cnt_p_clr | rcnt_p_upd_z) // 'rcnt_p_upd' also used for latched 'cnt' to 'rcnt', therefore used delayed signal (z)
            cnt_p_err <= '0;
          else
            if (det_crc_rdy & det_crc_err) // sync to crc ready
              begin
                cnt_p_err[0] <= cnt_p_err[0] + 1; // [0]: all size frame

                for (int i=1; i<= pN_CNT; i++)
                  begin
                    if (cnt_len_bound[i])
                      cnt_p_err[i] <= cnt_p_err[i] + 1;
                  end
              end
        end

  // counter (rhythm)

  always_ff @(posedge i_clk)
    if (~sclr_n)
      rcnt_p_err <= '0;
    else
      if (i_clk_en)
        begin
          if (rcnt_p_clr)
            rcnt_p_err <= '0;
          else
            if (rcnt_p_upd)
              begin
                for (int i=0; i<= pN_CNT; i++)
                  rcnt_p_err[i] <= cnt_p_err[i];
              end
        end

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // counters : packet offcast
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  // offcast strob

  always_ff @(posedge i_clk)
    if (~sclr_n)
      det_p_off <= '0;
    else
      if (i_clk_en)
        begin
          if (ri_det_p_off)
            det_p_off <= 1'b1;
          else
            if (det_crc_rdy) // sync to crc ready / see 'comment.txt' (7)
              det_p_off <= 1'b0;
        end

  // counter

  always_ff @(posedge i_clk)
    if (~sclr_n)
      cnt_p_off <= '0;
    else
      if (i_clk_en)
        begin
          if (cnt_p_clr | rcnt_p_upd_z) // 'rcnt_p_upd' also used for latched 'cnt' to 'rcnt', therefore used delayed signal (z)
            cnt_p_off <= '0;
          else
            if (det_crc_rdy & det_p_off) // sync to crc ready
              begin
                cnt_p_off[0] <= cnt_p_off[0] + 1; // [0]: all size frame

                for (int i=1; i<= pN_CNT; i++)
                  begin
                    if (cnt_len_bound[i])
                      cnt_p_off[i] <= cnt_p_off[i] + 1;
                  end
              end
        end

  // counter (rhythm)

  always_ff @(posedge i_clk)
    if (~sclr_n)
      rcnt_p_off <= '0;
    else
      if (i_clk_en)
        begin
          if (rcnt_p_clr)
            rcnt_p_off <= '0;
          else
            if (rcnt_p_upd)
              begin
                for (int i=0; i<= pN_CNT; i++)
                  rcnt_p_off[i] <= rcnt_p_off[i];
              end
        end

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // counters : packet speed (byte counter)
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  // counters

  always_ff @(posedge i_clk)
    if (~sclr_n)
      cnt_p_spd <= '0;
    else
      if (i_clk_en)
        begin
          if (cnt_p_clr | rcnt_p_upd_z) // 'rcnt_p_upd' also used for latched 'cnt' to 'rcnt', therefore used delayed signal (z)
            cnt_p_spd <= '0;
          else
            if (dat_load_spd) // period = 1 byte
              begin
                if (str_body_spd) // used speed strob (!)
                  begin
                    cnt_p_spd[0] <= cnt_p_spd[0] + 1; // [0]: all size frame

                    for (int i=1; i<= pN_CNT; i++)
                      begin
                        if (cnt_len_bound[i])
                          cnt_p_spd[i] <= cnt_p_spd[i] + 1;
                      end
                  end
              end
        end

  // counters (rhythm)

  always_ff @(posedge i_clk)
    if (~sclr_n)
      rcnt_p_spd <= '0;
    else
      if (i_clk_en)
        begin
          if (rcnt_p_clr)
            rcnt_p_spd <= '0;
          else
            if (rcnt_p_upd)
              begin
                for (int i=0; i<= pN_CNT; i++)
                  rcnt_p_spd[i] <= cnt_p_spd[i];
              end
        end

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // crc calculator for received ethernet frames  [ rx_crc / eth_rx_crc_v2 ]
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  // clock

  assign  rx_crc__i_clk       = i_clk       ;
  assign  rx_crc__i_clk_en    = i_clk_en    ;
  assign  rx_crc__i_sclr_n    = i_sclr_n    ; // not 'ri' / used internal 3 reg

  // frame body

  assign  rx_crc__i_str_body  = ri_str_body ;
  assign  rx_crc__i_dat_body  = ri_dat_body ;
  assign  rx_crc__i_dat_load  = ri_dat_load ;

  //---------------------------------------------------------------------------|
                                               //                              |
  eth_rx_crc_v2     rx_crc                     //                              |
  (                                            //                              |
  // clock ------------------------------------//------------------------------|
                                               //           |                  |
     .i_clk         ( rx_crc__i_clk      ),    //  in       |  req             |
     .i_clk_en      ( rx_crc__i_clk_en   ),    //  in       |       opt        |
     .i_sclr_n      ( rx_crc__i_sclr_n   ),    //  in       |       opt        |
                                               //           |                  |
  // frame body -------------------------------//-----------|------------------|
                                               //           |                  |
     .i_str_body    ( rx_crc__i_str_body ),    //  in       |  req             |
     .i_dat_body    ( rx_crc__i_dat_body ),    //  in       |  req             |
     .i_dat_load    ( rx_crc__i_dat_load ),    //  in       |  req             |
                                               //           |                  |
  // fcs crc ----------------------------------//-----------|------------------|
                                               //           |                  |
     .o_crc_rdy     ( rx_crc__o_crc_rdy  ),    //      out  |  req             |
     .o_crc_err     ( rx_crc__o_crc_err  )     //      out  |  req             |
                                               //           |                  |
  );//-------------------------------------------------------------------------|

  // crc comparator

  defparam  rx_crc.pV_DELAY  = pV_CRC_DLY;

  // in/out registers

  defparam  rx_crc.pN_REG_I  = 0;
  defparam  rx_crc.pN_REG_O  = 0;

  // signal tap (for tetsing)

  defparam  rx_crc.pE_GNPR   = 0; // for tetsing only

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // in/out registers : befor reg (i/no) / after reg (ri/ro)
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //                                                                                                   befor reg          after reg
  // frame body ---------------------------------------// frame body -----------------------------------------------------------------------//

  z_line #(.pV_DELAY (1)         , .pW_DATA (3)        )  ri__str_body    ( i_clk, i_clk_en, ~sclr_n,  i_str_body       , ri_str_body       );
  z_line #(.pV_DELAY (1)         , .pW_DATA (1)        )  ri__dat_load    ( i_clk, i_clk_en, ~sclr_n,  i_dat_load       , ri_dat_load       );
  z_line #(.pV_DELAY (1)         , .pW_DATA (8)        )  ri__dat_body    ( i_clk, i_clk_en, ~sclr_n,  i_dat_body       , ri_dat_body       );
  z_line #(.pV_DELAY (1)         , .pW_DATA (pW_LEN)   )  ri__len_body    ( i_clk, i_clk_en, ~sclr_n,  i_len_body       , ri_len_body       );
  //                                                                                                   ^                  ^
  // setting ------------------------------------------// setting --------------------------------------------------------------------------//

  generate for (j=1; j<=pN_CNT; j++) begin:               ri__cnt_set

  z_line #(.pV_DELAY (0)         , .pW_DATA (pW_LEN)   )  ri__cnt_p_top   ( i_clk, i_clk_en, ~sclr_n,  i_set_p_top [j]  , ri_set_p_top [j]  );
  z_line #(.pV_DELAY (0)         , .pW_DATA (pW_LEN)   )  ri__cnt_p_low   ( i_clk, i_clk_en, ~sclr_n,  i_set_p_low [j]  , ri_set_p_low [j]  ); end endgenerate
  //                                                                                                   ^                  ^
  // counters -----------------------------------------// counters -------------------------------------------------------------------------//

  // detector                                             detector

  z_line #(.pV_DELAY (1)         , .pW_DATA (1)        )  ro__det_p_rdy   ( i_clk, i_clk_en, ~sclr_n, no_det_p_rdy      , ro_det_p_rdy      );
  z_line #(.pV_DELAY (1)         , .pW_DATA (1)        )  ro__det_p_err   ( i_clk, i_clk_en, ~sclr_n, no_det_p_err      , ro_det_p_err      );
  z_line #(.pV_DELAY (1)         , .pW_DATA (1)        )  ri__det_p_off   ( i_clk, i_clk_en, ~sclr_n,  i_det_p_off      , ri_det_p_off      );
  //                                                                                                    ^                 ^
  // counters                                             counters

  generate for (j=0; j<=pN_CNT; j++) begin:               ro__cnt

  z_line #(.pV_DELAY (pN_REG_CNT), .pW_DATA (pW_CNT_P) )  ro__cnt_p_all   ( i_clk, i_clk_en, ~sclr_n, no_cnt_p_all [j]  , ro_cnt_p_all [j]  );
  z_line #(.pV_DELAY (pN_REG_CNT), .pW_DATA (pW_CNT_P) )  ro__cnt_p_err   ( i_clk, i_clk_en, ~sclr_n, no_cnt_p_err [j]  , ro_cnt_p_err [j]  );
  z_line #(.pV_DELAY (pN_REG_CNT), .pW_DATA (pW_CNT_P) )  ro__cnt_p_off   ( i_clk, i_clk_en, ~sclr_n, no_cnt_p_off [j]  , ro_cnt_p_off [j]  );
  z_line #(.pV_DELAY (pN_REG_CNT), .pW_DATA (pW_CNT_S) )  ro__cnt_p_spd   ( i_clk, i_clk_en, ~sclr_n, no_cnt_p_spd [j]  , ro_cnt_p_spd [j]  ); end endgenerate
  z_line #(.pV_DELAY (pN_REG_CNT), .pW_DATA (1)        )  ro__cnt_p_rdy   ( i_clk, i_clk_en, ~sclr_n, no_cnt_p_rdy      , ro_cnt_p_rdy      );
  //                                                                                                  ^                   ^
  // counters (rhythm)                                    counters (rhythm)

  generate for (j=0; j<=pN_CNT; j++) begin:               ro__rcnt

  z_line #(.pV_DELAY (pN_REG_CNT), .pW_DATA (pW_CNT_P) )  ro__rcnt_p_all  ( i_clk, i_clk_en, ~sclr_n, no_rcnt_p_all [j] , ro_rcnt_p_all [j] );
  z_line #(.pV_DELAY (pN_REG_CNT), .pW_DATA (pW_CNT_P) )  ro__rcnt_p_err  ( i_clk, i_clk_en, ~sclr_n, no_rcnt_p_err [j] , ro_rcnt_p_err [j] );
  z_line #(.pV_DELAY (pN_REG_CNT), .pW_DATA (pW_CNT_P) )  ro__rcnt_p_off  ( i_clk, i_clk_en, ~sclr_n, no_rcnt_p_off [j] , ro_rcnt_p_off [j] );
  z_line #(.pV_DELAY (pN_REG_CNT), .pW_DATA (pW_CNT_S) )  ro__rcnt_p_spd  ( i_clk, i_clk_en, ~sclr_n, no_rcnt_p_spd [j] , ro_rcnt_p_spd [j] ); end endgenerate
  z_line #(.pV_DELAY (pN_REG_CNT), .pW_DATA (1)        )  ro__rcnt_p_rdy  ( i_clk, i_clk_en, ~sclr_n, no_rcnt_p_rdy     , ro_rcnt_p_rdy     );
  //                                                                                                  ^                   ^
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  //==========================================================================================================================================================================================================================//
  // testing (for signal tap)
  //==========================================================================================================================================================================================================================//

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // value of parameters
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  //---------------------------------// packet body -----------------|

  logic    [ cbit(1,pW_LEN    ) :0 ]    stp_pW_LEN      = pW_LEN     ;

  //---------------------------------// packet counters -------------|

  logic    [ cbit(1,pN_CNT    ) :0 ]    stp_pN_CNT      = pN_CNT     ;
  logic    [ cbit(1,pW_CNT_P  ) :0 ]    stp_pW_CNT_P    = pW_CNT_P   ;
  logic    [ cbit(1,pW_CNT_S  ) :0 ]    stp_pW_CNT_S    = pW_CNT_S   ;

  //---------------------------------// crc comparator --------------|

  logic    [ cbit(1,pV_CRC_DLY) :0 ]    stp_pV_CRC_DLY  = pV_CRC_DLY ;

  //---------------------------------// local (for logic) -----------|

  //                                    in/out registers

  logic    [ cbit(1,pN_REG_CNT) :0 ]    stp_pN_REG_CNT  = pN_REG_CNT ;

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // registers with attribute "noprune" : interface  (off: pN_REG = 0, on: pN_REG = 1)
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  generate if (pE_GEN_NPR) begin: gen_npr__io

  // clock ----------------------------------// clock --------------------------------------//
 
  gen_npr #(.pN_REG (1), .pW_DATA (1)        )  stp__i_clk_en      (i_clk, i_clk_en         );
  gen_npr #(.pN_REG (1), .pW_DATA (1)        )  stp__i_sclr_n      (i_clk,   sclr_n         ); // not 'i'
  //                 ^                                                       ^
  // frame body -----------------------------// frame body ---------------------------------//
 
  gen_npr #(.pN_REG (1), .pW_DATA (1)        )  stp__i_str_body    (i_clk, i_str_body       );
  gen_npr #(.pN_REG (1), .pW_DATA (8)        )  stp__i_dat_body    (i_clk, i_dat_body       );
  gen_npr #(.pN_REG (1), .pW_DATA (1)        )  stp__i_dat_load    (i_clk, i_dat_load       );
  gen_npr #(.pN_REG (1), .pW_DATA (pW_LEN)   )  stp__i_len_body    (i_clk, i_len_body       );
  //                 ^                                                     ^
  // setting --------------------------------// setting ------------------------------------//
 
  for (j=1; j<=pN_CNT; j++) begin:              stp__i_cnt_set

  gen_npr #(.pN_REG (1), .pW_DATA (pW_LEN)   )  stp__i_set_p_top   (i_clk, i_set_p_top [j]  );
  gen_npr #(.pN_REG (1), .pW_DATA (pW_LEN)   )  stp__i_set_p_low   (i_clk, i_set_p_low [j]  ); end
  gen_npr #(.pN_REG (1), .pW_DATA (2)        )  stp__i_set_p_spd   (i_clk, i_set_p_spd      );
  //                 ^                                                     ^
  // counters -------------------------------// counters -----------------------------------//

  // detector                                   detector

  gen_npr #(.pN_REG (1), .pW_DATA (1)        )  stp__o_det_p_rdy   (i_clk, o_det_p_rdy      );
  gen_npr #(.pN_REG (1), .pW_DATA (1)        )  stp__o_det_p_err   (i_clk, o_det_p_err      );
  gen_npr #(.pN_REG (1), .pW_DATA (1)        )  stp__i_det_p_off   (i_clk, i_det_p_off      );
  //                 ^                                                     ^
  // counters                                   counters

  for (j=0; j<=pN_CNT; j++) begin:              stp__o_cnt

  gen_npr #(.pN_REG (1), .pW_DATA (pW_CNT_P) )  stp__o_cnt_p_all   (i_clk, o_cnt_p_all  [j] );
  gen_npr #(.pN_REG (1), .pW_DATA (pW_CNT_P) )  stp__o_cnt_p_err   (i_clk, o_cnt_p_err  [j] );
  gen_npr #(.pN_REG (1), .pW_DATA (pW_CNT_P) )  stp__o_cnt_p_off   (i_clk, o_cnt_p_off  [j] );
  gen_npr #(.pN_REG (1), .pW_DATA (pW_CNT_S) )  stp__o_cnt_p_spd   (i_clk, o_cnt_p_spd  [j] ); end
  gen_npr #(.pN_REG (1), .pW_DATA (1)        )  stp__i_cnt_p_clr   (i_clk, i_cnt_p_clr      );
  gen_npr #(.pN_REG (1), .pW_DATA (1)        )  stp__o_cnt_p_rdy   (i_clk, o_cnt_p_rdy      );
  //                 ^                                                     ^
  // counters (rhythm)                          counters (rhythm)

  for (j=0; j<=pN_CNT; j++) begin:              stp__o_rcnt

  gen_npr #(.pN_REG (1), .pW_DATA (pW_CNT_P) )  stp__o_rcnt_p_all  (i_clk, o_rcnt_p_all [j] );
  gen_npr #(.pN_REG (1), .pW_DATA (pW_CNT_P) )  stp__o_rcnt_p_err  (i_clk, o_rcnt_p_err [j] );
  gen_npr #(.pN_REG (1), .pW_DATA (pW_CNT_P) )  stp__o_rcnt_p_off  (i_clk, o_rcnt_p_off [j] );
  gen_npr #(.pN_REG (1), .pW_DATA (pW_CNT_S) )  stp__o_rcnt_p_spd  (i_clk, o_rcnt_p_spd [j] ); end
  gen_npr #(.pN_REG (1), .pW_DATA (1)        )  stp__i_rcnt_p_clr  (i_clk, i_rcnt_p_clr     );
  gen_npr #(.pN_REG (1), .pW_DATA (1)        )  stp__i_rcnt_p_upd  (i_clk, i_rcnt_p_upd     );
  gen_npr #(.pN_REG (1), .pW_DATA (1)        )  stp__o_rcnt_p_rdy  (i_clk, o_rcnt_p_rdy     );
  //                 ^                                                     ^
  //----------------------------------------------------------------------------------------//
  end endgenerate

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // registers with attribute "noprune" : parameters  (off: pN_REG = 0, on: pN_REG = 1)
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  generate if (pE_GEN_NPR) begin: gen_npr__par

  // packet body --------------------------------------// packet body ----------------------------//

  gen_npr #(.pN_REG (0), .pW_DATA (cbit(1,pW_LEN    )) )  stp__pW_LEN      (i_clk, stp_pW_LEN     );
  //                 ^                                                             ^
  // packet counters ----------------------------------// packet counters ------------------------//

  gen_npr #(.pN_REG (0), .pW_DATA (cbit(1,pN_CNT    )) )  stp__pN_CNT      (i_clk, stp_pN_CNT     );
  gen_npr #(.pN_REG (0), .pW_DATA (cbit(1,pW_CNT_P  )) )  stp__pW_CNT_P    (i_clk, stp_pW_CNT_P   );
  gen_npr #(.pN_REG (0), .pW_DATA (cbit(1,pW_CNT_S  )) )  stp__pW_CNT_S    (i_clk, stp_pW_CNT_S   );
  //                 ^                                                             ^
  // crc comparator -----------------------------------// crc comparator -------------------------//

  gen_npr #(.pN_REG (0), .pW_DATA (cbit(1,pV_CRC_DLY)) )  stp__pV_CRC_DLY  (i_clk, stp_pV_CRC_DLY );
  //                 ^                                                             ^
  // local (for logic) --------------------------------// local (for logic) ----------------------//

  // in/out registers                                     in/out registers

  gen_npr #(.pN_REG (0), .pW_DATA (cbit(1,pN_REG_CNT)) )  stp__pN_REG_CNT  (i_clk, stp_pN_REG_CNT );
  //                 ^                                                             ^
  //----------------------------------------------------------------------------------------------//
  end endgenerate

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  endmodule

  //==========================================================================================================================================================================================================================//
  // instantiation template
  //==========================================================================================================================================================================================================================//
  /*
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // interface
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  //------------------------------------------// packet counters -------| [ eth_cnt / eth_cnt_v2 ]

  //                                             clock

  var    logic                                   eth_cnt__i_clk         ;
  var    logic                                   eth_cnt__i_clk_en      ;
  var    logic                                   eth_cnt__i_sclr_n      ;

  //                                             frame body

  var    logic                                   eth_cnt__i_str_body    ;
  var    logic    [                    7 :0 ]    eth_cnt__i_dat_body    ;
  var    logic                                   eth_cnt__i_dat_load    ;
  var    logic    [ ETH_CNT__pW_LEN   -1 :0 ]    eth_cnt__i_len_body    ;

  //                                             setting

  var    logic    [                    1 :0 ]    eth_cnt__i_set_p_spd   ;
  var    logic    [ ETH_CNT__pN_CNT      :1 ]
                  [ ETH_CNT__pW_LEN   -1 :0 ]    eth_cnt__i_set_p_top   ;
  var    logic    [ ETH_CNT__pN_CNT      :1 ]
                  [ ETH_CNT__pW_LEN   -1 :0 ]    eth_cnt__i_set_p_low   ;

  //                                             detector

  var    logic                                   eth_cnt__o_det_p_rdy   ;
  var    logic                                   eth_cnt__o_det_p_err   ;
  var    logic                                   eth_cnt__i_det_p_off   ;

  //                                             counters

  var    logic                                   eth_cnt__i_cnt_p_clr   ;

  var    logic                                   eth_cnt__o_cnt_p_rdy   ;
  var    logic    [ ETH_CNT__pN_CNT      :0 ]
                  [ ETH_CNT__pW_CNT_P -1 :0 ]    eth_cnt__o_cnt_p_all   ;
  var    logic    [ ETH_CNT__pN_CNT      :0 ]
                  [ ETH_CNT__pW_CNT_P -1 :0 ]    eth_cnt__o_cnt_p_err   ;
  var    logic    [ ETH_CNT__pN_CNT      :0 ]
                  [ ETH_CNT__pW_CNT_P -1 :0 ]    eth_cnt__o_cnt_p_off   ;
  var    logic    [ ETH_CNT__pN_CNT      :0 ]
                  [ ETH_CNT__pW_CNT_S -1 :0 ]    eth_cnt__o_cnt_p_spd   ;

  //                                             counters (rhythm)

  var    logic                                   eth_cnt__i_rcnt_p_clr  ;

  var    logic                                   eth_cnt__i_rcnt_p_upd  ;

  var    logic                                   eth_cnt__o_rcnt_p_rdy  ;
  var    logic    [ ETH_CNT__pN_CNT      :0 ]
                  [ ETH_CNT__pW_CNT_P -1 :0 ]    eth_cnt__o_rcnt_p_all  ;
  var    logic    [ ETH_CNT__pN_CNT      :0 ]
                  [ ETH_CNT__pW_CNT_P -1 :0 ]    eth_cnt__o_rcnt_p_err  ;
  var    logic    [ ETH_CNT__pN_CNT      :0 ]
                  [ ETH_CNT__pW_CNT_P -1 :0 ]    eth_cnt__o_rcnt_p_off  ;
  var    logic    [ ETH_CNT__pN_CNT      :0 ]
                  [ ETH_CNT__pW_CNT_S -1 :0 ]    eth_cnt__o_rcnt_p_spd  ;

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // counters of ethernet frames (packets)  [ eth_cnt / eth_cnt_v2 ]
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  // clock

  assign  eth_cnt__i_clk         = ;
  assign  eth_cnt__i_clk_en      = ;
  assign  eth_cnt__i_sclr_n      = ;

  // frame body

  assign  eth_cnt__i_str_body    = ;
  assign  eth_cnt__i_dat_body    = ;
  assign  eth_cnt__i_dat_load    = ;
  assign  eth_cnt__i_len_body    = ;

  // setting

  assign  eth_cnt__i_set_p_top   = ;
  assign  eth_cnt__i_set_p_low   = ;
  assign  eth_cnt__i_set_p_spd   = ;

  // detector

  assign  eth_cnt__i_det_p_off   = ;

  // counters

  assign  eth_cnt__i_cnt_p_clr   = ;
  assign  eth_cnt__i_rcnt_p_clr  = ;
  assign  eth_cnt__i_rcnt_p_upd  = ;

  //--------------------------------------------------------------------------------|
                                                    //                              |
  eth_cnt_v2          eth_cnt                       //                              |
  (                                                 //                              |
  // clock -----------------------------------------//------------------------------|
                                                    //           |                  |
     .i_clk           ( eth_cnt__i_clk        ),    //  in       |  req             |
     .i_clk_en        ( eth_cnt__i_clk_en     ),    //  in       |       opt        |
     .i_sclr_n        ( eth_cnt__i_sclr_n     ),    //  in       |       opt        |
                                                    //           |                  |
  // frame body ------------------------------------//-----------|------------------|
                                                    //           |                  |
     .i_str_body      ( eth_cnt__i_str_body   ),    //  in       |  req             |
     .i_dat_body      ( eth_cnt__i_dat_body   ),    //  in       |  req             |
     .i_dat_load      ( eth_cnt__i_dat_load   ),    //  in       |  req             |
     .i_len_body      ( eth_cnt__i_len_body   ),    //  in       |       opt        |
                                                    //           |                  |
  // setting ---------------------------------------//-----------|------------------|
                                                    //           |                  |
     .i_set_p_top     ( eth_cnt__i_set_p_top  ),    //  in       |       opt        |
     .i_set_p_low     ( eth_cnt__i_set_p_low  ),    //  in       |       opt        |
     .i_set_p_spd     ( eth_cnt__i_set_p_spd  ),    //  in       |       opt        |
                                                    //           |                  |
  // detector --------------------------------------//-----------|------------------|
                                                    //           |                  |
     .o_det_p_rdy     ( eth_cnt__o_det_p_rdy  ),    //      out  |  req             |
     .o_det_p_err     ( eth_cnt__o_det_p_err  ),    //      out  |  req             |
     .i_det_p_off     ( eth_cnt__i_det_p_off  ),    //  in       |       opt        |
                                                    //           |                  |
  // counters --------------------------------------//-----------|------------------|
                                                    //           |                  |
     .i_cnt_p_clr     ( eth_cnt__i_cnt_p_clr  ),    //  in       |       opt        |
     .o_cnt_p_rdy     ( eth_cnt__o_cnt_p_rdy  ),    //      out  |  req             |
     .o_cnt_p_all     ( eth_cnt__o_cnt_p_all  ),    //      out  |  req             |
     .o_cnt_p_err     ( eth_cnt__o_cnt_p_err  ),    //      out  |  req             |
     .o_cnt_p_off     ( eth_cnt__o_cnt_p_off  ),    //      out  |       opt        |
     .o_cnt_p_spd     ( eth_cnt__o_cnt_p_spd  ),    //      out  |       opt        |
                                                    //           |                  |
  // counters (rhythm) -----------------------------//-----------|------------------|
                                                    //           |                  |
     .i_rcnt_p_clr    ( eth_cnt__i_rcnt_p_clr ),    //  in       |       opt        |
     .i_rcnt_p_upd    ( eth_cnt__i_rcnt_p_upd ),    //  in       |       opt        |
     .o_rcnt_p_rdy    ( eth_cnt__o_rcnt_p_rdy ),    //      out  |       opt        |
     .o_rcnt_p_all    ( eth_cnt__o_rcnt_p_all ),    //      out  |       opt        |
     .o_rcnt_p_err    ( eth_cnt__o_rcnt_p_err ),    //      out  |       opt        |
     .o_rcnt_p_off    ( eth_cnt__o_rcnt_p_off ),    //      out  |       opt        |
     .o_rcnt_p_spd    ( eth_cnt__o_rcnt_p_spd )     //      out  |       opt        |
                                                    //           |                  |
  );//------------------------------------------------------------------------------|

  // frame body

  defparam  eth_cnt.pW_LEN      = ETH_CNT__pW_LEN     ;

  // packet counters

  defparam  eth_cnt.pN_CNT      = ETH_CNT__pN_CNT     ;
  defparam  eth_cnt.pW_CNT_P    = ETH_CNT__pW_CNT_P   ;
  defparam  eth_cnt.pW_CNT_S    = ETH_CNT__pW_CNT_S   ;

  // crc comparator

  defparam  eth_cnt.pV_CRC_DLY  = ETH_CNT__pV_CRC_DLY ;

  // signal tap (for tetsing)

  defparam  eth_cnt.pE_GEN_NPR  = 0; // for testing only

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  */




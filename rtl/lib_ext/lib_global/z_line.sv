  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Company      : Micran, Department of Telecommunications
  //
  //  Engineer     : Pogodaev Danil
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Library      : lib_global / library of general purpose components (global)
  //
  //  File Name    : z_line .sv (rtl)
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Description  : Delay line for serial and parallel data.
  //                 The value of delay sets by parameter 'pV_DELAY'.
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Note (1)     : Attribute "synthesis noprune"
  //  
  //                 A Verilog HDL synthesis attribute that prevents the Quartus II software from removing
  //                 a register that does not directly or indirectly feed a top-level output or bidir pin,
  //                 such as a fan-out free register.
  //
  //                 This attribute differs from the preserve attribute, which prevents a register from
  //                 being reduced to a constant or merged with a duplicate register. Standard synthesis
  //                 optimizations remove nodes that do not directly or indirectly feed a top-level
  //                 output pin or bidir pin.
  //
  //  Note (2)     : Attribute "synthesis preserve"
  //  
  //                 A Verilog HDL synthesis attribute that prevents Analysis & Synthesis from minimizing
  //                 or removing a particular register.
  //
  //                 See also option "Remove Duplicate Registers" / Quartus Setting : Analysis & Synthesis Setting.
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Limitation   : Attribute "synthesis preserve" used for Altera only (parameter 'pE_GNPR').
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Include Files
  //  
  //  Category     Type       Name          Ver     Description
  //
  //  lib_global   function   cbit    .sv   1.0.0   bits calculator for input number (round up)
  //  lib_global   rtl        gen_npr .sv   1.0.0   generator of registers with attribute "noprune"
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Revision List
  //
  //  Version   Date       Author
  //
  //  1.0.0     26.09.16   Pogodaev Danil
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  module z_line    (

  //     clock
     
         i_clk     ,
         i_clk_en  ,
         i_sclr    ,

  //     i/o data

         i_data    ,
         o_data    ,

  //     delay line

         o_delay  );

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // include files : functions
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
                                                         //      |
  `include "cbit.sv"                                     // req  | bits calculator for input number (round up)
                                                         //      |
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // parameters : setting
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
                                                         //      |                                  |          |         |
  // delay setting --------------------------------------//------|----------------------------------|----------|---------|----------------------------------------------------------------------------------------------------//
                                                         //      |                                  |          |         |
  parameter int  pW_DATA   = 1                      ;    // req  | width of data                    | bit      | def: 1  |
  parameter int  pV_DELAY  = 1                      ;    // req  | value of delay / for 'o_data'    | t.clk_en |         | info: 1 t.clk_en = 1 reg
                                                         //      |                                  |          |         |
  // signal tap (for tetsing) ---------------------------//------|----------------------------------|----------|---------|----------------------------------------------------------------------------------------------------//
                                                         //      |                                  |          |         |
  parameter bit  pE_GNPR   = 0                      ;    // test | generator of reg with "noprune"  | on/off   | def: 0  | info: see note (1), for altera only (!)
                                                         //      |                                  |          |         |
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // parameters : local (for logic)
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
                                                         //      |                                  |          |         |
  // shift register -------------------------------------//------|----------------------------------|----------|---------|----------------------------------------------------------------------------------------------------//
                                                         //      |                                  |          |         |
  parameter int  lpW_SHIFTER  = pW_DATA             ;    // req  | width of shift register          | bit      | def: 1  |
  parameter int  lpD_SHIFTER  = pV_DELAY            ;    // req  | depth of shift register          | word     |         |
                                                         //      |                                  |          |         |
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // interface
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
                                                         //      |                                  |          |         |
  //--------------------------------// clock ------------//------|----------------------------------|----------|---------|----------------------------------------------------------------------------------------------------//
                                                         //      |                                  |          |         |
  input  logic                         i_clk        ;    // req  | clock                            | clock    |         |
  input  logic                         i_clk_en     ;    // opt  | clock ena                        | pulse    | def: 1  | width: clk
  input  logic                         i_sclr       ;    // opt  | sync clear                       | strob    | def: 0  |
                                                         //      |                                  |          |         |
  //--------------------------------// i/o data ---------//------|----------------------------------|----------|---------|----------------------------------------------------------------------------------------------------//
                                                         //      |                                  |          |         |
  input  logic    [ pW_DATA -1 :0 ]    i_data       ;    // req  | input data                       | pdata    |         | 
  output logic    [ pW_DATA -1 :0 ]    o_data       ;    // req  | output data / after delay        | pdata    |         |
                                                         //      |                                  |          |         |
  //--------------------------------// delay line -------//------|----------------------------------|----------|---------|----------------------------------------------------------------------------------------------------//
                                                         //      |                                  |          |         |
  output logic    [ pV_DELAY   :0 ]                      //      |                                  |          |         |
                  [ pW_DATA -1 :0 ]    o_delay      ;    // opt  | delay line (shifter)             | array    |         |
                                                         //      |                                  |          |         |
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // logic
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  genvar                               j            ;

  //--------------------------------// shifter -----|

  var    logic    [ pV_DELAY   :1 ]
                  [ pW_DATA -1 :0 ]    shifter = '0 /* synthesis preserve */; // see note (2)

  //--------------------------------// delay -------|

  var    logic    [ pW_DATA -1 :0 ]    data         ;
  var    logic    [ pV_DELAY   :0 ]
                  [ pW_DATA -1 :0 ]    delay        ;
                    
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  //==========================================================================================================================================================================================================================//
  // body of module
  //==========================================================================================================================================================================================================================//

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // out logic
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  assign  o_data   = data  ;
  assign  o_delay  = delay ;

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // shift register
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  always_ff @(posedge i_clk)
    if (i_sclr)
      shifter <= '0;
    else
      if (i_clk_en)
        begin
          for (int i=1; i<=lpD_SHIFTER; i++)
            begin
              shifter[i] <= (i==1) ? i_data : shifter[i-1];
            end
        end

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // output data
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  assign  data = (pV_DELAY == 0) ? i_data : shifter [lpD_SHIFTER];

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // output delay line
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  always_comb
    for (int i=0; i<=lpD_SHIFTER; i++)
      begin
        delay[i] <= (i==0) ? i_data : shifter[i];
      end

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  //==========================================================================================================================================================================================================================//
  // testing (for signal tap)
  //==========================================================================================================================================================================================================================//

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // value of parameters
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  //----------------------------------// delay setting ------------------|

  logic    [ cbit(1,pW_DATA    ) :0 ]    stp_pW_DATA      = pW_DATA      ;
  logic    [ cbit(1,pV_DELAY   ) :0 ]    stp_pV_DELAY     = pV_DELAY     ;

  //----------------------------------// local (for logic) --------------|

  //                                     shift register

  logic    [ cbit(1,lpW_SHIFTER) :0 ]    stp_lpW_SHIFTER  = lpW_SHIFTER  ;
  logic    [ cbit(1,lpD_SHIFTER) :0 ]    stp_lpD_SHIFTER  = lpD_SHIFTER  ;

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // registers with attribute "noprune" : interface  (off: pN_REG = 0, on: pN_REG = 1)
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  generate if (pE_GNPR) begin: gen_npr__io

  // clock ---------------------------------// clock -----------------------------//
 
  gen_npr #(.pN_REG (1), .pW_DATA (1)       )  stp__i_clk_en  (i_clk, i_clk_en    );
  gen_npr #(.pN_REG (1), .pW_DATA (1)       )  stp__i_sclr    (i_clk, i_sclr      );
  //                 ^                                                ^
  // i/o data ------------------------------// i/o data --------------------------//
 
  gen_npr #(.pN_REG (1), .pW_DATA (pW_DATA) )  stp__i_data    (i_clk, i_data      );
  gen_npr #(.pN_REG (1), .pW_DATA (pW_DATA) )  stp__o_data    (i_clk, o_data      );
  //                 ^                                                ^
  // delay line ----------------------------// delay line ------------------------//
 
  for (j=0; j<=pV_DELAY; j++) begin:           gen__o_delay

  gen_npr #(.pN_REG (1), .pW_DATA (pW_DATA) )  stp__o_delay   (i_clk, o_delay [j] ); end
  //                 ^                                                ^
  //------------------------------------------------------------------------------//
  end  endgenerate

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // registers with attribute "noprune" : parameters  (off: pN_REG = 0, on: pN_REG = 1)
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  generate if (pE_GNPR) begin: gen_npr__par

  // delay setting -------------------------------------// delay setting ----------------------------//

  gen_npr #(.pN_REG (0), .pW_DATA (cbit(1,pW_DATA    )) )  stp__pW_DATA      (i_clk, stp_pW_DATA     );
  gen_npr #(.pN_REG (0), .pW_DATA (cbit(1,pV_DELAY   )) )  stp__pV_DELAY     (i_clk, stp_pV_DELAY    );
  //                 ^                                                               ^
  // local (for logic) ---------------------------------// local (for logic) ------------------------//

  // shift register                                        shift register
  
  gen_npr #(.pN_REG (0), .pW_DATA (cbit(1,lpW_SHIFTER)) )  stp__lpW_SHIFTER  (i_clk, stp_lpW_SHIFTER );
  gen_npr #(.pN_REG (0), .pW_DATA (cbit(1,lpD_SHIFTER)) )  stp__lpD_SHIFTER  (i_clk, stp_lpD_SHIFTER );
  //                 ^                                                               ^
  //-------------------------------------------------------------------------------------------------//
  end  endgenerate

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  endmodule

  //==========================================================================================================================================================================================================================//
  // instantiation template
  //==========================================================================================================================================================================================================================//
  /*
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // interface
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  //----------------------------------------// delay line -------| [ z_line / z_line ]

  //                                           clock

  var    logic                                 z_line__i_clk     ;
  var    logic                                 z_line__i_clk_en  ;
  var    logic                                 z_line__i_sclr    ;
  
  //                                           i/o data
  
  var    logic    [ Z_LINE__pW_DATA -1 :0 ]    z_line__i_data    ;
  var    logic    [ Z_LINE__pW_DATA -1 :0 ]    z_line__o_data    ;
  
  //                                           delay line
  var    logic    [ Z_LINE__pV_DELAY   :0 ]   
                  [ Z_LINE__pW_DATA -1 :0 ]    z_line__o_delay   ;

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // delay line for serial and parallel data  [ z_line / z_line ]
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  // clock

  assign  z_line__i_clk     = ;
  assign  z_line__i_clk_en  = ;
  assign  z_line__i_sclr    = ;

  // i/o data

  assign  z_line__i_data    = ;

  //-----------------------------------------------------------------------|
                                           //                              |
  z_line          z_line                   //                              |
  (                                        //                              |
  // clock --------------------------------//------------------------------|
                                           //           |                  |
     .i_clk       ( z_line__i_clk    ),    //  in       |  req             |
     .i_clk_en    ( z_line__i_clk_en ),    //  in       |       opt        |
     .i_sclr      ( z_line__i_sclr   ),    //  in       |       opt        |
                                           //           |                  |
  // i/o data -----------------------------//-----------|------------------|
                                           //           |                  |
     .i_data      ( z_line__i_data   ),    //  in       |  req             |
     .o_data      ( z_line__o_data   ),    //      out  |  req             |
                                           //           |                  |
  // delay line ---------------------------//-----------|------------------|
                                           //           |                  |
     .o_delay     ( z_line__o_delay  )     //      out  |       opt        |
                                           //           |                  |
  );//---------------------------------------------------------------------|
  
  // delay setting

  defparam  z_line.pW_DATA   = Z_LINE__pW_DATA  ;
  defparam  z_line.pV_DELAY  = Z_LINE__pV_DELAY ;

  // signal tap (for tetsing)

  defparam  z_line.pE_GNPR   = 0; // for testing only

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // delay line for serial and parallel data  [ z__xxx / z_line ]
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  z_line // delay line / for signal 'xxx'

  #(.pV_DELAY (1), // value of delay      | t.clk_en
    .pW_DATA  (1), // width of data       | bit
    .pE_GNPR  (0)) // generator "noprune" | on/off

  z__xxx (.i_clk( ), .i_clk_en( ), .i_sclr( ), .i_data( ), .o_data( ), .o_delay( ) );
  //                                                   ^           ^            ^
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // delay line for serial and parallel data  [ z__xxx / z_line ]
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  z_line // delay line / for signal 'xxx'

  #(.pV_DELAY (1), // value of delay | t.clk_en
    .pW_DATA  (1)) // width of data  | bit

  z__xxx (.i_clk( ), .i_clk_en( ), .i_sclr( ), .i_data( ), .o_data( ), .o_delay( ) );
  //                                                   ^           ^            ^
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // delay line for serial and parallel data  [ z__xxx / z_line ]
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  z_line  #(.pV_DELAY (1), .pW_DATA (1))  z__xxx  (i_clk, i_clk_en, i_sclr, i_data, o_data, o_delay );
  z_line  #(.pV_DELAY (1), .pW_DATA (1))  z__xxx  (i_clk, i_clk_en, i_sclr, i_data, o_data          );
  z_line  #(.pV_DELAY (1), .pW_DATA (1))  z__xxx  (i_clk, i_clk_en, i_sclr, i_data,       , o_delay );
  //                   ^                                                    ^       ^       ^
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  */




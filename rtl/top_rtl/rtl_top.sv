  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Company      : Micran, Department of Telecommunications
  //
  //  Engineer     : Pogodaev Danil
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Library      : lib_ethernet / library of ethernet components
  //
  //  Component    : eth_rx_v2 / receiver of ethernet frames (packets)
  // 
  //  File Name    : rtl_top .sv (rtl)
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Description  : RTL top file for module 'eth_rx'.
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Attached (1) : Additional (detailed) code comments / eth_rx_v2 - comment .txt / default (!)
  //
  //  Attached (2) : Additional (detailed) code comments / rtl_top - comment .txt
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Include Files
  //
  //  Category       Type       Name                     Ver     Description
  //
  //  top_rtl        include    eth_rx__par_set   .svh   2.0     module 'eth_rx' / parameter setting
  //  top_rtl        include    eth_rx__par_local .svh   2.0     module 'eth_rx' / parameter local
  //
  //  lib_internal   rtl        eth_rx_v2         .sv    2.0.0   receiver of ethernet frames
  //  lib_internal   rtl        eth_rx_cst_v2     .sv    2.0.0   ethernet preliminary receiver for custom mode
  //  lib_internal   rtl        eth_rx_mii_v2     .sv    2.0.0   ethernet preliminary receiver for mii-interface
  //  lib_internal   rtl        eth_rx_rmii_v2    .sv    2.0.0   ethernet preliminary receiver for rmii-interface
  //  lib_internal   rtl        eth_rx_gmii_v2    .sv    2.0.0   ethernet preliminary receiver for gmii-interface
  //  lib_internal   rtl        eth_rx_mux_v2     .sv    2.0.0   multiplexer of ethernet preliminary receivers
  //  lib_internal   rtl        eth_rx_head_v2    .sv    2.0.0   frame header detector (preamble and start-frame delimiter)
  //  lib_internal   rtl        eth_rx_str_v2     .sv    2.0.0   strobe generator and length calculator for ethernet frames
  //  lib_internal   rtl        eth_rx_dat_v2     .sv    2.0.0   data extractor for ip and mac headers
  //  lib_internal   rtl        eth_rx_out_v2     .sv    2.0.0   output registers and latching data (mac and ip headers)
  //
  //  lib_ethernet   rtl        eth_cnt_v2        .sv    2.0.0   counters of ethernet frames
  //  lib_ethernet   rtl        eth_rx_crc_v2     .sv    2.0.0   crc calculator for received ethernet frames
  //  lib_ethernet   function   eth_crc32_8d      .sv    1.0.0   calculator of crc-32 (data 8 bit)
  //
  //  lib_global     function   cbit              .sv    1.0.0   bits calculator for input number (round up)
  //  lib_global     rtl        det_se            .sv    1.0.0   detector of single edges for input strobe
  //  lib_global     rtl        det_de            .sv    1.0.0   detector of double edges for input strobe
  //  lib_global     rtl        gen_npr           .sv    1.0.0   generator of registers with attribute "noprune"
  //  lib_global     rtl        gen_ced           .sv    1.0.0   generator of clock enable for data
  //  lib_global     rtl        gen_cen           .sv    1.0.0   generator of clock enable
  //  lib_global     rtl        gen_sstr          .sv    1.0.0   generator of single strobe
  //  lib_global     rtl        z_line            .sv    1.0.0   delay line for serial and parallel data
  //  lib_global     rtl        shift_s2p         .sv    1.0.0   shift register for serial to parallel
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Revision List
  //
  //  Version   Date       Author
  //
  //  2.0       26.09.16   Pogodaev Danil
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  
  module rtl_top          (

  //---- reset -----------|

         i_reset_n        ,

  //---- setting ---------|

         i_set_cut        ,
         i_set_speed      ,
         i_set_jumbo      ,

  //---- lan interface ---|

  //     lan gmii

         i_gmii_clk       ,
         i_gmii_rxd       ,
         i_gmii_rx_dv     ,

  //     lan rmii

         i_rmii_clk       ,
         o_rmii_clk_s     ,
         i_rmii_rxd       ,
         i_rmii_crs_dv    ,

  //     lan rmii

         i_mii_clk        ,
         i_mii_rxd        ,
         i_mii_rx_dv      ,

  //---- custom mode -----|

         i_cst_clk        ,
         i_cst_cen        ,
         i_cst_ced        ,
         i_cst_rxd        ,
         i_cst_rx_dv      ,

  //---- rx packet -------|

  //     clock

         o_rx_clk         ,
         o_rx_cen         ,
         o_rx_ced         ,

  //     strob

         o_rx_str_body    ,
         o_rx_cnt_body    ,
         o_rx_str_mac     ,
         o_rx_cnt_mac     ,
         o_rx_str_mda     ,
         o_rx_str_msa     ,
         o_rx_str_mlt     ,
         o_rx_str_pay     ,
         o_rx_cnt_pay     ,
         o_rx_str_ip      ,
         o_rx_cnt_ip      ,
         o_rx_str_gap     ,
         o_rx_cnt_gap     ,

  //     data

         o_rx_dat_load    ,
         o_rx_dat_body    ,
         o_rx_rdy_mac     ,
         o_rx_dat_mda     ,
         o_rx_dat_msa     ,
         o_rx_dat_mlt     ,
         o_rx_rdy_ip      ,
         o_rx_dat_ip      ,

  //     length

         o_rx_rdy_body    ,
         o_rx_len_body    ,
         o_rx_rdy_gap     ,
         o_rx_len_gap     ,

  //     fcs crc

         o_rx_crc_rdy     ,
         o_rx_crc_err     ,

  //---- rx counters -----|

  //     setting

  //     i_rx_set_p_top   , // constant, so pins not required / see 'rtl_top - comment.txt' (2)
  //     i_rx_set_p_low   , // constant, so pins not required / see 'rtl_top - comment.txt' (2)
         i_rx_set_p_spd   ,

  //     detector

         i_rx_det_p_off   ,
         o_rx_det_p_err   ,
         o_rx_det_p_cut   ,
         o_rx_det_p_rdy   ,

  //     counters

         i_rx_cnt_p_clr   ,
         o_rx_cnt_p_rdy   ,
         o_rx_cnt_p_all   ,
         o_rx_cnt_p_err   ,
         o_rx_cnt_p_off   ,
         o_rx_cnt_p_spd   ,

  //     counters (rhythm)

         i_rx_rcnt_p_clr  ,
         i_rx_rcnt_p_upd  ,
         o_rx_rcnt_p_rdy  ,
         o_rx_rcnt_p_all  ,
         o_rx_rcnt_p_err  ,
         o_rx_rcnt_p_off  ,
         o_rx_rcnt_p_spd );

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // include files : functions
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
                                                                           //      |
  `include "cbit.sv"                                                       // req  | bits calculator for input number (round up)
                                                                           //      |
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // include files : parameters
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
                                                                           //      |
  // receiver of ethernet frames                                           //------|------------------------------------------------------------------------------------------------------------------------------------------//
                                                                           //      |
  `include "eth_rx__par_set.svh"                                           // req  | module 'eth_rx' / parameter setting
  `include "eth_rx__par_local.svh"                                         // req  | module 'eth_rx' / parameter local
                                                                           //      |
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // interface
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
                                                                           //      |                                            |          |           |
  //---------------------------------------------// reset -----------------//------|--------------------------------------------|----------|-----------|----------------------------------------------------------------------//
                                                                           //      |                                            |          |           |
  input  logic                                      i_reset_n         ;    // opt  | sync clear (ative low)                     | strob    | def: 1    | info: used 'rx_clk' ; see note (5) and 'comment.txt' (6)
                                                                           //      |                                            |          |           |
  //---------------------------------------------// setting ---------------//------|--------------------------------------------|----------|-----------|----------------------------------------------------------------------//
                                                                           //      |                                            |          |           |
  input  logic                                      i_set_cut         ;    // opt  | enable of cut off oversize frames          | strob    | def: 0    | info: see also 'pE_RX_CUT'
                                                                           //      |                                            |          |           |
  input  logic                                      i_set_jumbo       ;    // opt  | enable of jumbo-frames support             | strob    | def: 0    | info: see also 'pE_RX_JUMBO'
                                                                           //      |                                            |          |           |
  input  logic    [                       1 :0 ]    i_set_speed       ;    // req  | speed of interface                         | strob    |           | info: see 'comment.txt' (6)
                                                                           //      |                                            |          |           |
                                                                           //      |   0 : 1000 mbit/sec                        |          |           |
                                                                           //      |   1 :  100 mbit/sec                        |          |           |
                                                                           //      |   2 :   10 mbit/sec                        |          |           |
                                                                           //      |                                            |          |           |
  //---------------------------------------------// lan interface ---------//------|--------------------------------------------|----------|-----------|----------------------------------------------------------------------//
                                                                           //      |                                            |          |           |
  //                                                lan mii                //------|--------------------------------------------|----------|-----------|----------------------------------------------------------------------//
                                                                           //      |                                            |          |           |
  input  logic    [                       3 :0 ]    i_mii_rxd         ;    // req  | receive data                               | pdata    |           | info: see note  (4) ; info: for latching used rising edge of clock
  input  logic                                      i_mii_rx_dv       ;    // req  | receive data valid                         | strob    |           | info: see note  (4)
  input  logic                                      i_mii_clk         ;    // req  | receive clock                              | clock    |           | info: see table (1) ; see 'comment.txt' (6)
                                                                           //      |                                            |          |           |
  //                                                lan rmii               //------|--------------------------------------------|----------|-----------|----------------------------------------------------------------------//
                                                                           //      |                                            |          |           |
  input  logic    [                       1 :0 ]    i_rmii_rxd        ;    // req  | receive data                               | pdata    |           | info: see note  (4) ; info: for latching used rising edge of clock
  input  logic                                      i_rmii_crs_dv     ;    // req  | receive data valid & carrier sense         | strob    |           | info: see note  (4)
  input  logic                                      i_rmii_clk        ;    // req  | receive clock                              |          |           | info: see table (1) ; see 'comment.txt' (6)
  output logic                                      o_rmii_clk_s      ;    // test | receive clock / speed adapter              |          |           |
                                                                           //      |                                            |          |           |
                                                                           //      |  - for 100 mbit/s : 50 mhz                 | clock    |           |
                                                                           //      |  - for 10  mbit/s :  5 mhz (not 50 mhz)    | strob    |           |
                                                                           //      |                                            |          |           |
  //                                                lan gmii               //------|--------------------------------------------|----------|-----------|----------------------------------------------------------------------//
                                                                           //      |                                            |          |           |
  input  logic    [                       7 :0 ]    i_gmii_rxd        ;    // req  | receive data                               | pdata    |           | info: see note  (4) ; info: for latching used rising edge of clock
  input  logic                                      i_gmii_rx_dv      ;    // req  | receive data valid                         | strob    |           | info: see note  (4)
  input  logic                                      i_gmii_clk        ;    // req  | receive clock                              | clock    |           | info: see table (1) ; see 'comment.txt' (6)
                                                                           //      |                                            |          |           |
  //---------------------------------------------// custom mode -----------//------|--------------------------------------------|----------|-----------|----------------------------------------------------------------------//
                                                                           //      |                                            |          |           |
  input  logic                                      i_cst_clk         ;    // opt  | receive clock                              | clock    |           |
  input  logic                                      i_cst_cen         ;    // opt  | receive clock enable                       | pulse    | def: 1    | width: cst_clk
  input  logic                                      i_cst_ced         ;    // opt  | receive clock enable for data              | pulse    | def: 1    | width: cst_cen
                                                                           //      |                                            |          |           |
  input  logic    [                       7 :0 ]    i_cst_rxd         ;    // opt  | receive data                               | pdata    |           | info: for latching used rising edge of clock
  input  logic                                      i_cst_rx_dv       ;    // opt  | receive data valid                         | strob    |           | 
                                                                           //      |                                            |          |           |
  //---------------------------------------------// rx packet -------------//------|--------------------------------------------|----------|-----------|----------------------------------------------------------------------//
                                                                           //      |                                            |          |           |
  //                                                clock                  //------|--------------------------------------------|----------|-----------|----------------------------------------------------------------------//
                                                                           //      |                                            |          |           |
  output logic                                      o_rx_clk          ;    // req  | clock                                      | clock    |           | info: see table (1)
  output logic                                      o_rx_cen          ;    // req  | clock enable                               | pulse    |           | info: see table (1) ; width: rx_clk
  output logic                                      o_rx_ced          ;    // req  | clock enable for data                      | pulse    |           | info: see table (1) ; width: rx_cen
                                                                           //      |                                            |          |           |
  //                                                strob                  //------|--------------------------------------------|----------|-----------|----------------------------------------------------------------------//
                                                                           //      |                                            |          |           |
  output logic    [                       2 :0 ]    o_rx_str_body     ;    // req  | frame body              / strob            | strob    |           | info : see purpose (6)
  output logic    [ ETH_RX__lpW_RX_LEN   -1 :0 ]    o_rx_cnt_body     ;    // opt  | frame body              / strob counter    | count up |           |
  output logic    [                       2 :0 ]    o_rx_str_mac      ;    // opt  | mac header              / strob            | strob    |           |
  output logic    [                       3 :0 ]    o_rx_cnt_mac      ;    // opt  | mac header              / strob counter    | count up |           |
  output logic    [                       2 :0 ]    o_rx_str_mda      ;    // opt  | mac destination address / strob            | strob    |           |
  output logic    [                       2 :0 ]    o_rx_str_msa      ;    // opt  | mac source address      / strob            | strob    |           |
  output logic    [                       2 :0 ]    o_rx_str_mlt      ;    // opt  | mac length or type      / strob            | strob    |           |
  output logic    [                       2 :0 ]    o_rx_str_ip       ;    // opt  | ip header               / strob            | strob    |           |
  output logic    [ ETH_RX__lpW_RX_IP_C  -1 :0 ]    o_rx_cnt_ip       ;    // opt  | ip header               / strob counter    | count up |           |
  output logic    [                       2 :0 ]    o_rx_str_pay      ;    // opt  | payload & pad           / strob            | strob    |           | info: with crc strobe (!) ; see table (2)
  output logic    [ ETH_RX__lpW_RX_LEN   -1 :0 ]    o_rx_cnt_pay      ;    // opt  | payload & pad           / strob counter    | count up |           |
  output logic    [                       2 :0 ]    o_rx_str_gap      ;    // opt  | inter-frame gap         / strob            | strob    |           |
  output logic    [ ETH_RX__pW_RX_GAP    -1 :0 ]    o_rx_cnt_gap      ;    // opt  | inter-frame gap         / strob counter    | count up |           |
                                                                           //      |                                            |          |           |
  //                                                data                   //------|--------------------------------------------|----------|-----------|----------------------------------------------------------------------//
                                                                           //      |                                            |          |           |
  output logic                                      o_rx_dat_load     ;    // req  | frame body              / load (byte)      | pulse    |           | width: rx_cen
  output logic    [                       7 :0 ]    o_rx_dat_body     ;    // req  | frame body              / data (byte)      | pdata    |           |
                                                                           //      |                                            |          |           |
  output logic                                      o_rx_rdy_mac      ;    // opt  | mac header              / ready            | pulse    |           | width: rx_cen ; info: used for latch 'rx_dat_mac'
  output logic    [                      47 :0 ]    o_rx_dat_mda      ;    // opt  | mac destination address / data             | pdata    |           |
  output logic    [                      47 :0 ]    o_rx_dat_msa      ;    // opt  | mac source address      / data             | pdata    |           |
  output logic    [                      15 :0 ]    o_rx_dat_mlt      ;    // opt  | mac length or type      / data             | pdata    |           |
                                                                           //      |                                            |          |           |
  output logic                                      o_rx_rdy_ip       ;    // opt  | ip header               / ready            | pulse    |           | width: rx_cen ; info: used for latch 'rx_dat_ip'
  output logic    [ ETH_RX__lpW_RX_IP    -1 :0 ]    o_rx_dat_ip       ;    // opt  | ip header               / data             | pdata    |           |
                                                                           //      |                                            |          |           |
  //                                                length                 //------|--------------------------------------------|----------|-----------|----------------------------------------------------------------------//
                                                                           //      |                                            |          |           |
  output logic                                      o_rx_rdy_body     ;    // req  | frame body              / ready            | pulse    |           | width: rx_cen ; info: used for latch 'rx_len_body'
  output logic    [ ETH_RX__lpW_RX_LEN   -1 :0 ]    o_rx_len_body     ;    // req  | frame body              / length (byte)    | pdata    |           |
  output logic                                      o_rx_rdy_gap      ;    // opt  | inter-frame gap         / ready            | pulse    |           | width: rx_cen ; info: used for latch 'rx_len_gap'
  output logic    [ ETH_RX__pW_RX_GAP    -1 :0 ]    o_rx_len_gap      ;    // opt  | inter-frame gap         / length (byte)    | pdata    |           |
                                                                           //      |                                            |          |           |
  //                                                fcs crc                //------|--------------------------------------------|----------|-----------|----------------------------------------------------------------------//
                                                                           //      |                                            |          |           |
  output logic                                      o_rx_crc_rdy      ;    // req  | fcs crc-32              / ready            | pulse    |           | width: rx_cen ; info: used for latch 'rx_crc_err'
  output logic                                      o_rx_crc_err      ;    // req  | fcs crc-32              / error            | strob    |           |
                                                                           //      |                                            |          |           |
  //---------------------------------------------// rx counters -----------//------|--------------------------------------------|----------|-----------|----------------------------------------------------------------------//
                                                                           //      |                                            |          |           |
  //                                                setting                //------|--------------------------------------------|----------|-----------|----------------------------------------------------------------------//
                                                                           //      |                                            |          |           |
  input  logic    [                       1 :0 ]    i_rx_set_p_spd    ;    // opt  | speed selection                            | pdata    | def: 0    | info: see 'comment.txt' (6,10)
                                                                           //      |                                            |          |           |
                                                                           //      |   0 : payload                              |          |           | info: used 'o_rx_str_body' - 17 bytes
                                                                           //      |   1 : payload + mac                        |          |           | info: used 'o_rx_str_body' -  4 bytes
                                                                           //      |   2 : payload + mac + fcs crc              |          |           | info: used 'o_rx_str_body'
//input  logic    [ ETH_RX__pN_RX_CNT       :1 ]                           //      |                                            |          |           |
//                [ ETH_RX__lpW_RX_LEN   -1 :0 ]    i_rx_set_p_top    ;    // opt  | top bound of body length (not frame)       | array    |           | lim: constant (!) ; info: see note (2) and 'comment.txt' (7)
//input  logic    [ ETH_RX__pN_RX_CNT       :1 ]                           //      |                                            |          |           |
//                [ ETH_RX__lpW_RX_LEN   -1 :0 ]    i_rx_set_p_low    ;    // opt  | low bound of body length (not frame)       | array    |           | lim: constant (!) ; info: see note (2) and 'comment.txt' (7)
                                                                           //      |                                            |          |           |
  //                                                detector               //------|--------------------------------------------|----------|-----------|----------------------------------------------------------------------//
                                                                           //      |                                            |          |           |
  output logic                                      o_rx_det_p_rdy    ;    // opt  | packet ready                               | pulse    |           | width: rx_cen
  output logic                                      o_rx_det_p_err    ;    // opt  | packet error                               | strob    |           |
  output logic                                      o_rx_det_p_cut    ;    // opt  | packet cut off (for oversize frame)        | pulse    |           | width: rx_cen
  input  logic                                      i_rx_det_p_off    ;    // opt  | packet offcast (from buffer)               | pulse    | def: 0    | width: rx_cen ; lim: must appear before stop bit of frame ; see comment (8)
                                                                           //      |                                            |          |           |
  //                                                counters               //------|--------------------------------------------|----------|-----------|----------------------------------------------------------------------//
                                                                           //      |                                            |          |           |
  input  logic                                      i_rx_cnt_p_clr    ;    // opt  | current value / clear                      | strob    | def: 0    | info: sensitivity to rising edge ; see 'comment.txt' (6)
                                                                           //      |                                            |          |           |
  output logic                                      o_rx_cnt_p_rdy    ;    // opt  | current value / ready                      | pulse    |           | width: rx_cen
  output logic    [ ETH_RX__pN_RX_CNT       :0 ]                           //      |                                            |          |           |
                  [ ETH_RX__pW_RX_CNT_P  -1 :0 ]    o_rx_cnt_p_all    ;    // opt  | current value / packet all                 | count up |           | info: see comment (9)
  output logic    [ ETH_RX__pN_RX_CNT       :0 ]                           //      |                                            |          |           |
                  [ ETH_RX__pW_RX_CNT_P  -1 :0 ]    o_rx_cnt_p_err    ;    // opt  | current value / packet error               | count up |           | info: see comment (9)
  output logic    [ ETH_RX__pN_RX_CNT       :0 ]                           //      |                                            |          |           |
                  [ ETH_RX__pW_RX_CNT_P  -1 :0 ]    o_rx_cnt_p_off    ;    // opt  | current value / packet offcast             | count up |           | info: see comment (9)
                                                                           //      |                                            |          |           |
  output logic    [ ETH_RX__pW_RX_CNT_S  -1 :0 ]    o_rx_cnt_p_spd    ;    // opt  | current value / packet speed               | count up |           | info: byte counter ; see also 'i_rx_set_p_spd' and 'comment.txt' (9,10)
                                                                           //      |                                            |          |           |
  //                                                counters (rhythm)      //------|--------------------------------------------|----------|-----------|----------------------------------------------------------------------//
                                                                           //      |                                            |          |           |
  input  logic                                      i_rx_rcnt_p_clr   ;    // opt  | periodic value / clear                     | strob    | def: 0    | info: sensitivity to rising edge ; see 'comment.txt' (6)
                                                                           //      |                                            |          |           |
  input  logic                                      i_rx_rcnt_p_upd   ;    // opt  | periodic value / update                    | strob    | def: 0    | info: sensitivity to rising edge ; see 'comment.txt' (6)
                                                                           //      |                                            |          |           |
  output logic                                      o_rx_rcnt_p_rdy   ;    // opt  | periodic value / ready                     | pulse    |           | width: rx_cen
  output logic    [ ETH_RX__pN_RX_CNT       :0 ]                           //      |                                            |          |           |
                  [ ETH_RX__pW_RX_CNT_P  -1 :0 ]    o_rx_rcnt_p_all   ;    // opt  | periodic value / packet all                | pdata    |           | info: see comment (9)
  output logic    [ ETH_RX__pN_RX_CNT       :0 ]                           //      |                                            |          |           |
                  [ ETH_RX__pW_RX_CNT_P  -1 :0 ]    o_rx_rcnt_p_err   ;    // opt  | periodic value / packet error              | pdata    |           | info: see comment (9)
  output logic    [ ETH_RX__pN_RX_CNT       :0 ]                           //      |                                            |          |           |
                  [ ETH_RX__pW_RX_CNT_P  -1 :0 ]    o_rx_rcnt_p_off   ;    // opt  | periodic value / packet offcast            | pdata    |           | info: see comment (9)
                                                                           //      |                                            |          |           |
  output logic    [ ETH_RX__pW_RX_CNT_S  -1 :0 ]    o_rx_rcnt_p_spd   ;    // opt  | periodic value / packet speed              | pdata    |           | info: byte counter ; see also 'i_rx_set_p_spd' and 'comment.txt' (9,10)
                                                                           //      |                                            |          |           |
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // logic
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  //---------------------------------------------// ethernet receiver -------| [ eth_rx / eth_rx_v2 ]

  //                                                reset

  var    logic                                      eth_rx__i_reset_n        ;

  //                                                setting

  var    logic                                      eth_rx__i_set_cut        ;
  var    logic    [                       1 :0 ]    eth_rx__i_set_speed      ;
  var    logic                                      eth_rx__i_set_jumbo      ;

  //                                                lan interface

  var    logic                                      eth_rx__i_mii_clk        ;
  var    logic    [                       3 :0 ]    eth_rx__i_mii_rxd        ;
  var    logic                                      eth_rx__i_mii_rx_dv      ;

  var    logic                                      eth_rx__i_rmii_clk       ;
  var    logic                                      eth_rx__o_rmii_clk_s     ;
  var    logic    [                       1 :0 ]    eth_rx__i_rmii_rxd       ;
  var    logic                                      eth_rx__i_rmii_crs_dv    ;

  var    logic                                      eth_rx__i_gmii_clk       ;
  var    logic    [                       7 :0 ]    eth_rx__i_gmii_rxd       ;
  var    logic                                      eth_rx__i_gmii_rx_dv     ;

  //                                                custom mode

  var    logic                                      eth_rx__i_cst_clk        ;
  var    logic                                      eth_rx__i_cst_cen        ;
  var    logic                                      eth_rx__i_cst_ced        ;
  var    logic    [                       7 :0 ]    eth_rx__i_cst_rxd        ;
  var    logic                                      eth_rx__i_cst_rx_dv      ;

  //                                                rx packet

  var    logic                                      eth_rx__o_rx_clk         ;
  var    logic                                      eth_rx__o_rx_cen         ;
  var    logic                                      eth_rx__o_rx_ced         ;

  var    logic    [                       2 :0 ]    eth_rx__o_rx_str_body    ;
  var    logic    [ ETH_RX__lpW_RX_LEN   -1 :0 ]    eth_rx__o_rx_cnt_body    ;
  var    logic    [                       2 :0 ]    eth_rx__o_rx_str_mac     ;
  var    logic    [                       3 :0 ]    eth_rx__o_rx_cnt_mac     ;
  var    logic    [                       2 :0 ]    eth_rx__o_rx_str_mda     ;
  var    logic    [                       2 :0 ]    eth_rx__o_rx_str_msa     ;
  var    logic    [                       2 :0 ]    eth_rx__o_rx_str_mlt     ;
  var    logic    [                       2 :0 ]    eth_rx__o_rx_str_ip      ;
  var    logic    [ ETH_RX__lpW_RX_IP_C  -1 :0 ]    eth_rx__o_rx_cnt_ip      ;
  var    logic    [                       2 :0 ]    eth_rx__o_rx_str_pay     ;
  var    logic    [ ETH_RX__lpW_RX_LEN   -1 :0 ]    eth_rx__o_rx_cnt_pay     ;
  var    logic    [                       2 :0 ]    eth_rx__o_rx_str_gap     ;
  var    logic    [ ETH_RX__pW_RX_GAP    -1 :0 ]    eth_rx__o_rx_cnt_gap     ;

  var    logic                                      eth_rx__o_rx_dat_load    ;
  var    logic    [                       7 :0 ]    eth_rx__o_rx_dat_body    ;
  var    logic                                      eth_rx__o_rx_rdy_mac     ;
  var    logic    [                      47 :0 ]    eth_rx__o_rx_dat_mda     ;
  var    logic    [                      47 :0 ]    eth_rx__o_rx_dat_msa     ;
  var    logic    [                      15 :0 ]    eth_rx__o_rx_dat_mlt     ;
  var    logic                                      eth_rx__o_rx_rdy_ip      ;
  var    logic    [                      47 :0 ]    eth_rx__o_rx_dat_ip      ;

  var    logic                                      eth_rx__o_rx_rdy_body    ;
  var    logic    [ ETH_RX__lpW_RX_LEN   -1 :0 ]    eth_rx__o_rx_len_body    ;
  var    logic                                      eth_rx__o_rx_rdy_gap     ;
  var    logic    [ ETH_RX__pW_RX_GAP    -1 :0 ]    eth_rx__o_rx_len_gap     ;

  var    logic                                      eth_rx__o_rx_crc_rdy     ;
  var    logic                                      eth_rx__o_rx_crc_err     ;

  //                                                rx counters / setting

  var    logic    [                       1 :0 ]    eth_rx__i_rx_set_p_spd   ;
  var    logic    [ ETH_RX__pN_RX_CNT       :1 ]
                  [ ETH_RX__lpW_RX_LEN   -1 :0 ]    eth_rx__i_rx_set_p_top   ;
  var    logic    [ ETH_RX__pN_RX_CNT       :1 ]
                  [ ETH_RX__lpW_RX_LEN   -1 :0 ]    eth_rx__i_rx_set_p_low   ;

  //                                                rx counters / detector

  var    logic                                      eth_rx__o_rx_det_p_rdy   ;
  var    logic                                      eth_rx__o_rx_det_p_err   ;
  var    logic                                      eth_rx__o_rx_det_p_cut   ;
  var    logic                                      eth_rx__i_rx_det_p_off   ;

  //                                                rx counters / counters

  var    logic                                      eth_rx__i_rx_cnt_p_clr   ;

  var    logic                                      eth_rx__o_rx_cnt_p_rdy   ;
  var    logic    [ ETH_RX__pN_RX_CNT       :0 ]
                  [ ETH_RX__pW_RX_CNT_P  -1 :0 ]    eth_rx__o_rx_cnt_p_all   ;
  var    logic    [ ETH_RX__pN_RX_CNT       :0 ]
                  [ ETH_RX__pW_RX_CNT_P  -1 :0 ]    eth_rx__o_rx_cnt_p_err   ;
  var    logic    [ ETH_RX__pN_RX_CNT       :0 ]
                  [ ETH_RX__pW_RX_CNT_P  -1 :0 ]    eth_rx__o_rx_cnt_p_off   ;

  var    logic    [ ETH_RX__pW_RX_CNT_S  -1 :0 ]    eth_rx__o_rx_cnt_p_spd   ;

  //                                                rx counters / rhythm

  var    logic                                      eth_rx__i_rx_rcnt_p_clr  ;

  var    logic                                      eth_rx__i_rx_rcnt_p_udp  ;

  var    logic                                      eth_rx__o_rx_rcnt_p_rdy  ;
  var    logic    [ ETH_RX__pN_RX_CNT       :0 ]
                  [ ETH_RX__pW_RX_CNT_P  -1 :0 ]    eth_rx__o_rx_rcnt_p_all  ;
  var    logic    [ ETH_RX__pN_RX_CNT       :0 ]
                  [ ETH_RX__pW_RX_CNT_P  -1 :0 ]    eth_rx__o_rx_rcnt_p_err  ;
  var    logic    [ ETH_RX__pN_RX_CNT       :0 ]
                  [ ETH_RX__pW_RX_CNT_P  -1 :0 ]    eth_rx__o_rx_rcnt_p_off  ;

  var    logic    [ ETH_RX__pW_RX_CNT_S  -1 :0 ]    eth_rx__o_rx_rcnt_p_spd  ;
  
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  //==========================================================================================================================================================================================================================//
  // body of module
  //==========================================================================================================================================================================================================================//

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // out logic / see 'rtl_top - comment.txt' (1)
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  // rx packet

  assign  o_rx_clk         = eth_rx__o_rx_clk        ;
  assign  o_rx_cen         = eth_rx__o_rx_cen        ;
  assign  o_rx_ced         = eth_rx__o_rx_ced        ;

  assign  o_rx_str_body    = eth_rx__o_rx_str_body   ;
//assign  o_rx_cnt_body    = eth_rx__o_rx_cnt_body   ;
//assign  o_rx_str_mac     = eth_rx__o_rx_str_mac    ;
//assign  o_rx_cnt_mac     = eth_rx__o_rx_cnt_mac    ;
//assign  o_rx_str_mda     = eth_rx__o_rx_str_mda    ;
//assign  o_rx_str_msa     = eth_rx__o_rx_str_msa    ;
//assign  o_rx_str_mlt     = eth_rx__o_rx_str_mlt    ;
//assign  o_rx_str_ip      = eth_rx__o_rx_str_ip     ;
//assign  o_rx_cnt_ip      = eth_rx__o_rx_cnt_ip     ;
//assign  o_rx_str_pay     = eth_rx__o_rx_str_pay    ;
//assign  o_rx_cnt_pay     = eth_rx__o_rx_cnt_pay    ;
//assign  o_rx_str_gap     = eth_rx__o_rx_str_gap    ;
//assign  o_rx_cnt_gap     = eth_rx__o_rx_cnt_gap    ;

  assign  o_rx_dat_load    = eth_rx__o_rx_dat_load   ;
  assign  o_rx_dat_body    = eth_rx__o_rx_dat_body   ;
//assign  o_rx_rdy_mac     = eth_rx__o_rx_rdy_mac    ;
//assign  o_rx_dat_mda     = eth_rx__o_rx_dat_mda    ;
//assign  o_rx_dat_msa     = eth_rx__o_rx_dat_msa    ;
//assign  o_rx_dat_mlt     = eth_rx__o_rx_dat_mlt    ;
//assign  o_rx_rdy_ip      = eth_rx__o_rx_rdy_ip     ;
//assign  o_rx_dat_ip      = eth_rx__o_rx_dat_ip     ;

  assign  o_rx_rdy_body    = eth_rx__o_rx_rdy_body   ;
  assign  o_rx_len_body    = eth_rx__o_rx_len_body   ;
//assign  o_rx_rdy_gap     = eth_rx__o_rx_rdy_gap    ;
//assign  o_rx_len_gap     = eth_rx__o_rx_len_gap    ;

  assign  o_rx_crc_rdy     = eth_rx__o_rx_crc_rdy    ;
  assign  o_rx_crc_err     = eth_rx__o_rx_crc_err    ;

  // rx counters

//assign  o_rx_det_p_rdy   = eth_rx__o_rx_det_p_rdy  ;
//assign  o_rx_det_p_err   = eth_rx__o_rx_det_p_err  ;
//assign  o_rx_det_p_cut   = eth_rx__o_rx_det_p_cut  ;

//assign  o_rx_cnt_p_rdy   = eth_rx__o_rx_cnt_p_rdy  ;
//assign  o_rx_cnt_p_all   = eth_rx__o_rx_cnt_p_all  ;
//assign  o_rx_cnt_p_err   = eth_rx__o_rx_cnt_p_err  ;
//assign  o_rx_cnt_p_off   = eth_rx__o_rx_cnt_p_off  ;
//assign  o_rx_cnt_p_spd   = eth_rx__o_rx_cnt_p_spd  ;

//assign  o_rx_rcnt_p_rdy  = eth_rx__o_rx_rcnt_p_rdy ;
//assign  o_rx_rcnt_p_all  = eth_rx__o_rx_rcnt_p_all ;
//assign  o_rx_rcnt_p_err  = eth_rx__o_rx_rcnt_p_err ;
//assign  o_rx_rcnt_p_off  = eth_rx__o_rx_rcnt_p_off ;
//assign  o_rx_rcnt_p_spd  = eth_rx__o_rx_rcnt_p_spd ;

  // lan interface

//assign  o_rmii_clk_s     = eth_rx__o_rmii_clk_s    ;

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // receiver of ethernet frames  [ eth_rx / eth_rx_v2 ]
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  // reset

  assign  eth_rx__i_reset_n        = i_reset_n        ;

  // setting

  assign  eth_rx__i_set_cut        = i_set_cut        ;
  assign  eth_rx__i_set_speed      = i_set_speed      ;
  assign  eth_rx__i_set_jumbo      = i_set_jumbo      ;

  // lan interface

  assign  eth_rx__i_mii_clk        = i_mii_clk        ;
  assign  eth_rx__i_mii_rxd        = i_mii_rxd        ;
  assign  eth_rx__i_mii_rx_dv      = i_mii_rx_dv      ;

  assign  eth_rx__i_rmii_clk       = i_rmii_clk       ;
  assign  eth_rx__i_rmii_rxd       = i_rmii_rxd       ;
  assign  eth_rx__i_rmii_crs_dv    = i_rmii_crs_dv    ;

  assign  eth_rx__i_gmii_clk       = i_gmii_clk       ;
  assign  eth_rx__i_gmii_rx_dv     = i_gmii_rx_dv     ;
  assign  eth_rx__i_gmii_rxd       = i_gmii_rxd       ;

  // custom mode

  assign  eth_rx__i_cst_clk        = i_cst_clk        ;
  assign  eth_rx__i_cst_cen        = i_cst_cen        ;
  assign  eth_rx__i_cst_ced        = i_cst_ced        ;
  assign  eth_rx__i_cst_rxd        = i_cst_rxd        ;
  assign  eth_rx__i_cst_rx_dv      = i_cst_rx_dv      ;
 
  // rx counters / setting

  assign  eth_rx__i_rx_set_p_top   = '0               ; // constant, so pins not required / see 'rtl_top - comment.txt' (2)
  assign  eth_rx__i_rx_set_p_low   = '0               ; // constant, so pins not required / see 'rtl_top - comment.txt' (2)
  assign  eth_rx__i_rx_set_p_spd   = i_rx_set_p_spd   ;

  // rx counters / detector

  assign  eth_rx__i_rx_det_p_off   = i_rx_det_p_off   ;

  // rx counters / counters

  assign  eth_rx__i_rx_cnt_p_clr   = i_rx_cnt_p_clr   ;
  assign  eth_rx__i_rx_rcnt_p_clr  = i_rx_rcnt_p_clr  ;
  assign  eth_rx__i_rx_rcnt_p_upd  = i_rx_rcnt_p_upd  ;

  //-------------------------------------------------------------------------------------|
                                                         //                              |
  eth_rx_v2              eth_rx                          //                              |
  (                                                      //                              |
  // reset ----------------------------------------------//------------------------------|
                                                         //           |                  |
     .i_reset_n          ( eth_rx__i_reset_n       ),    //  in       |       opt        |
                                                         //           |                  |
  // setting --------------------------------------------//-----------|------------------|
                                                         //           |                  |
     .i_set_cut          ( eth_rx__i_set_cut       ),    //  in       |       opt        |
     .i_set_speed        ( eth_rx__i_set_speed     ),    //  in       |  req             |
     .i_set_jumbo        ( eth_rx__i_set_jumbo     ),    //  in       |       opt        |
                                                         //           |                  |
  // lan interface --------------------------------------//-----------|------------------|
                                                         //           |                  |
  // lan mii             lan mii                         //-----------|------------------|
                                                         //           |                  |
     .i_mii_clk          ( eth_rx__i_mii_clk       ),    //  in       |  req             |
     .i_mii_rxd          ( eth_rx__i_mii_rxd       ),    //  in       |  req             |
     .i_mii_rx_dv        ( eth_rx__i_mii_rx_dv     ),    //  in       |  req             |
                                                         //           |                  |
  // lan rmii            lan rmii                        //-----------|------------------|
                                                         //           |                  |
     .i_rmii_clk         ( eth_rx__i_rmii_clk      ),    //  in       |  req             |
     .o_rmii_clk_s       ( eth_rx__o_rmii_clk_s    ),    //      out  |            test  |
     .i_rmii_rxd         ( eth_rx__i_rmii_rxd      ),    //  in       |  req             |
     .i_rmii_crs_dv      ( eth_rx__i_rmii_crs_dv   ),    //  in       |  req             |
                                                         //           |                  |
  // lan gmii            lan gmii                        //-----------|------------------|
                                                         //           |                  |
     .i_gmii_clk         ( eth_rx__i_gmii_clk      ),    //  in       |  req             |
     .i_gmii_rxd         ( eth_rx__i_gmii_rxd      ),    //  in       |  req             |
     .i_gmii_rx_dv       ( eth_rx__i_gmii_rx_dv    ),    //  in       |  req             |
                                                         //           |                  |
  // custom mode ----------------------------------------//-----------|------------------|
                                                         //           |                  |
     .i_cst_clk          ( eth_rx__i_cst_clk       ),    //  in       |       opt        |
     .i_cst_cen          ( eth_rx__i_cst_cen       ),    //  in       |       opt        |
     .i_cst_ced          ( eth_rx__i_cst_ced       ),    //  in       |       opt        |
     .i_cst_rxd          ( eth_rx__i_cst_rxd       ),    //  in       |       opt        |
     .i_cst_rx_dv        ( eth_rx__i_cst_rx_dv     ),    //  in       |       opt        |
                                                         //           |                  |
  // rx packet ------------------------------------------//-----------|------------------|
                                                         //           |                  |
  // clock               clock                           //-----------|------------------|
                                                         //           |                  |
     .o_rx_clk           ( eth_rx__o_rx_clk        ),    //      out  |  req             |
     .o_rx_cen           ( eth_rx__o_rx_cen        ),    //      out  |  req             |
     .o_rx_ced           ( eth_rx__o_rx_ced        ),    //      out  |  req             |
                                                         //           |                  |
  // strob               strob                           //-----------|------------------|
                                                         //           |                  |
     .o_rx_str_body      ( eth_rx__o_rx_str_body   ),    //      out  |  req             |
     .o_rx_cnt_body      ( eth_rx__o_rx_cnt_body   ),    //      out  |       opt        |
     .o_rx_str_mac       ( eth_rx__o_rx_str_mac    ),    //      out  |       opt        |
     .o_rx_cnt_mac       ( eth_rx__o_rx_cnt_mac    ),    //      out  |       opt        |
     .o_rx_str_mda       ( eth_rx__o_rx_str_mda    ),    //      out  |       opt        |
     .o_rx_str_msa       ( eth_rx__o_rx_str_msa    ),    //      out  |       opt        |
     .o_rx_str_mlt       ( eth_rx__o_rx_str_mlt    ),    //      out  |       opt        |
     .o_rx_str_ip        ( eth_rx__o_rx_str_ip     ),    //      out  |       opt        |
     .o_rx_cnt_ip        ( eth_rx__o_rx_cnt_ip     ),    //      out  |       opt        |
     .o_rx_str_pay       ( eth_rx__o_rx_str_pay    ),    //      out  |       opt        |
     .o_rx_cnt_pay       ( eth_rx__o_rx_cnt_pay    ),    //      out  |       opt        |
     .o_rx_str_gap       ( eth_rx__o_rx_str_gap    ),    //      out  |       opt        |
     .o_rx_cnt_gap       ( eth_rx__o_rx_cnt_gap    ),    //      out  |       opt        |
                                                         //           |                  |
  // data                data                            //-----------|------------------|
                                                         //           |                  |
     .o_rx_dat_load      ( eth_rx__o_rx_dat_load   ),    //      out  |  req             |
     .o_rx_dat_body      ( eth_rx__o_rx_dat_body   ),    //      out  |  req             |
                                                         //           |                  |
     .o_rx_rdy_mac       ( eth_rx__o_rx_rdy_mac    ),    //      out  |       opt        |
     .o_rx_dat_mda       ( eth_rx__o_rx_dat_mda    ),    //      out  |       opt        |
     .o_rx_dat_msa       ( eth_rx__o_rx_dat_msa    ),    //      out  |       opt        |
     .o_rx_dat_mlt       ( eth_rx__o_rx_dat_mlt    ),    //      out  |       opt        |
                                                         //           |                  |
     .o_rx_rdy_ip        ( eth_rx__o_rx_rdy_ip     ),    //      out  |       opt        |
     .o_rx_dat_ip        ( eth_rx__o_rx_dat_ip     ),    //      out  |       opt        |
                                                         //           |                  |
  // length              length                          //-----------|------------------|
                                                         //           |                  |
     .o_rx_rdy_body      ( eth_rx__o_rx_rdy_body   ),    //      out  |  req             |
     .o_rx_len_body      ( eth_rx__o_rx_len_body   ),    //      out  |  req             |
     .o_rx_rdy_gap       ( eth_rx__o_rx_rdy_gap    ),    //      out  |       opt        |
     .o_rx_len_gap       ( eth_rx__o_rx_len_gap    ),    //      out  |       opt        |
                                                         //           |                  |
  // fcs crc             fcs crc                         //-----------|------------------|
                                                         //           |                  |
     .o_rx_crc_rdy       ( eth_rx__o_rx_crc_rdy    ),    //      out  |  req             |
     .o_rx_crc_err       ( eth_rx__o_rx_crc_err    ),    //      out  |  req             |
                                                         //           |                  |
  // rx counters ----------------------------------------//-----------|------------------|
                                                         //           |                  |
  // setting             setting                         //-----------|------------------|
                                                         //           |                  |
     .i_rx_set_p_top     ( eth_rx__i_rx_set_p_top  ),    //  in       |       opt        |
     .i_rx_set_p_low     ( eth_rx__i_rx_set_p_low  ),    //  in       |       opt        |
     .i_rx_set_p_spd     ( eth_rx__i_rx_set_p_spd  ),    //  in       |       opt        |
                                                         //           |                  |
  // detector            detector                        //-----------|------------------|
                                                         //           |                  |
     .o_rx_det_p_rdy     ( eth_rx__o_rx_det_p_rdy  ),    //      out  |       opt        |
     .o_rx_det_p_err     ( eth_rx__o_rx_det_p_err  ),    //      out  |       opt        |
     .o_rx_det_p_cut     ( eth_rx__o_rx_det_p_cut  ),    //      out  |       opt        |
     .i_rx_det_p_off     ( eth_rx__i_rx_det_p_off  ),    //  in       |       opt        |
                                                         //           |                  |
  // counters            counters                        //-----------|------------------|
                                                         //           |                  |
     .i_rx_cnt_p_clr     ( eth_rx__i_rx_cnt_p_clr  ),    //  in       |       opt        |
     .o_rx_cnt_p_rdy     ( eth_rx__o_rx_cnt_p_rdy  ),    //      out  |       opt        |
     .o_rx_cnt_p_all     ( eth_rx__o_rx_cnt_p_all  ),    //      out  |       opt        |
     .o_rx_cnt_p_err     ( eth_rx__o_rx_cnt_p_err  ),    //      out  |       opt        |
     .o_rx_cnt_p_off     ( eth_rx__o_rx_cnt_p_off  ),    //      out  |       opt        |
     .o_rx_cnt_p_spd     ( eth_rx__o_rx_cnt_p_spd  ),    //      out  |       opt        |
                                                         //           |                  |
  // counters            counters (rhythm)               //-----------|------------------|
                                                         //           |                  |
     .i_rx_rcnt_p_clr    ( eth_rx__i_rx_rcnt_p_clr ),    //  in       |       opt        |
     .i_rx_rcnt_p_upd    ( eth_rx__i_rx_rcnt_p_upd ),    //  in       |       opt        |
     .o_rx_rcnt_p_rdy    ( eth_rx__o_rx_rcnt_p_rdy ),    //      out  |       opt        |
     .o_rx_rcnt_p_all    ( eth_rx__o_rx_rcnt_p_all ),    //      out  |       opt        |
     .o_rx_rcnt_p_err    ( eth_rx__o_rx_rcnt_p_err ),    //      out  |       opt        |
     .o_rx_rcnt_p_off    ( eth_rx__o_rx_rcnt_p_off ),    //      out  |       opt        |
     .o_rx_rcnt_p_spd    ( eth_rx__o_rx_rcnt_p_spd )     //      out  |       opt        |
                                                         //           |                  |
  );//-----------------------------------------------------------------------------------|

  // interface

  defparam  eth_rx.pS_IO_TYPE   = ETH_RX__pS_IO_TYPE  ;

  // custom mode

  defparam  eth_rx.pT_CST_CEN   = ETH_RX__pT_CST_CEN  ;
  defparam  eth_rx.pT_CST_CED   = ETH_RX__pT_CST_CED  ;

  // packet manipulation

  defparam  eth_rx.pE_RX_CUT    = ETH_RX__pE_RX_CUT   ;
  defparam  eth_rx.pE_RX_JUMBO  = ETH_RX__pE_RX_JUMBO ;

  // packet structure
  
  defparam  eth_rx.pE_RX_HEAD   = ETH_RX__pE_RX_HEAD  ;

  defparam  eth_rx.pW_RX_LEN    = ETH_RX__pW_RX_LEN   ;
  defparam  eth_rx.pV_RX_LEN    = ETH_RX__pV_RX_LEN   ;
  defparam  eth_rx.pW_RX_LEN_J  = ETH_RX__pW_RX_LEN_J ;
  defparam  eth_rx.pV_RX_LEN_J  = ETH_RX__pV_RX_LEN_J ;
  defparam  eth_rx.pW_RX_GAP    = ETH_RX__pW_RX_GAP   ;

  defparam  eth_rx.pE_RX_IP     = ETH_RX__pE_RX_IP    ;
  defparam  eth_rx.pN_RX_IPL    = ETH_RX__pN_RX_IPL   ;
  defparam  eth_rx.pN_RX_IPH    = ETH_RX__pN_RX_IPH   ;

  defparam  eth_rx.pE_RX_MAC    = ETH_RX__pE_RX_MAC   ;

  // packet counters

  defparam  eth_rx.pN_RX_CNT    = ETH_RX__pN_RX_CNT   ;
  defparam  eth_rx.pW_RX_CNT_P  = ETH_RX__pW_RX_CNT_P ;
  defparam  eth_rx.pW_RX_CNT_S  = ETH_RX__pW_RX_CNT_S ;

  // signal tap (for tetsing)

  defparam  eth_rx.pE_GEN_NPR   = ETH_RX__pE_GEN_NPR  ;

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  endmodule




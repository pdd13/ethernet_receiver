  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Company      : Micran, Department of Telecommunications
  //
  //  Engineer     : Pogodaev Danil
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Library      : lib_ethernet / library of ethernet components
  //
  //  Component    : eth_rx_v2 / receiver of ethernet frames (packets)
  //
  //  File Name    : eth_rx_rmii_v2 .sv (rtl)
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Description  : Ethernet preliminary receiver for RMII-interface.
  //
  //  Purpose (1)  : Generate clocks of ethernet frames / see table (1)
  //
  //                  - receive clock (repeater) / o_lan_clk
  //
  //                     for 100 mbit/s : 50 mhz
  //                     for 10  mbit/s : 50 mhz
  //
  //                  - receive clock enable / o_lan_cen
  //
  //                     for 100 mbit/s : high
  //                     for 10  mbit/s : 5 mhz
  //
  //                  - receive clock enable for data / o_lan_ced
  //
  //                     for 100 mbit/s : period = 4 lan_cen
  //                     for 10  mbit/s : period = 4 lan_cen
  //
  //  Purpose (2)  : Recover input signals of rmii-interface
  //
  //                  - carrier sense      / o_rmii_crs
  //                  - receive data valid / o_rmii_rx_dv
  //
  //  Purpose (3)  : Convert data stream 2-bit to 8-bit
  //
  //                  - receiver data 8 bit / o_lan_rxd
  //                  - receiver data valid / o_lan_rx_dv
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Table (1)    : Clock manager of ethernet frames for receiver
  //
  //                 |--------------------------------------------------------------------------------------------------|
  //                 |                       |          |                       frequency (period)                      |
  //                 |        setting        |  clock   |---------------------------------------------------------------|
  //                 |                       |          |      10 mbit/s      |     100 mbit/s     |    1000 mbit/s     |
  //                 |=======================|==========|=====================|====================|====================|
  //                 |                       |  rx_clk  |  2.50 mhz (400 ns)  |  25.0 mhz (40 ns)  |  no                |
  //                 |  mii interface:       |----------|---------------------|--------------------|--------------------|
  //                 |                       |  rx_cen  |  1'b1               |  1'b1              |  no                |
  //                 |  pS_IO_TYPE = mii     |----------|---------------------|--------------------|--------------------|
  //                 |                       |  rx_ced  |  1.25 mhz (800 ns)  |  12.5 mhz (80 ns)  |  no                |
  //                 |=======================|==========|=====================|====================|====================|
  //                 |                       |  rx_clk  |  50.00 mhz (20 ns)  |  50.00 mhz (20 ns) |  no                |
  //                 |  rmii interface:      |----------|---------------------|--------------------|--------------------|
  //                 |                       |  rx_cen  |  5.00 mhz (200 ns)  |  1'b1              |  no                |
  //                 |  pS_IO_TYPE = rmii    |----------|---------------------|--------------------|--------------------|
  //                 |                       |  rx_ced  |  1.25 mhz (800 ns)  |  12.5 mhz (80 ns)  |  no                |
  //                 |=======================|==========|=====================|====================|====================|
  //                 |                       |  rx_clk  |  2.50 mhz (400 ns)  |  25.0 mhz (40 ns)  |  125.0 mhz (8 ns)  |
  //                 |  gmii interface:      |----------|---------------------|--------------------|--------------------|
  //                 |                       |  rx_cen  |  1'b1               |  1'b1              |  1'b1              |
  //                 |  pS_IO_TYPE = gmii    |----------|---------------------|--------------------|--------------------|
  //                 |                       |  rx_ced  |  1.25 mhz (800 ns)  |  12.5 mhz (80 ns)  |  1'b1              |
  //                 |=======================|==========|===============================================================|
  //                 |                       |  rx_clk  |  i_cst_clk                                                    |
  //                 |  custom mode:         |----------|---------------------------------------------------------------|
  //                 |                       |  rx_cen  |  pT_CST_CEN = 0 : i_cst_cen                                   |
  //                 |  pS_IO_TYPE = custom  |          |  pT_CST_CEN > 0 : i_cst_clk * pT_CST_CEN                      |
  //                 |                       |----------|---------------------------------------------------------------|
  //                 |                       |  rx_ced  |  pT_CST_CED = 0 : i_cst_ced                                   |
  //                 |                       |          |  pT_CST_CED > 0 : i_cst_clk * pT_CST_CEN * pT_CST_CED         |
  //                 |==================================================================================================|
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Literature (1) : RMII Specification (RMII Consortium) [1998]
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Include Files
  //  
  //  Category     Type       Name           Ver     Description
  //
  //  lib_global   function   cbit     .sv   1.0.0   bits calculator for input number (round up)
  //  lib_global   rtl        z_line   .sv   1.0.0   delay line for serial and parallel data
  //  lib_global   rtl        det_de   .sv   1.0.0   detector of double edges for input strobe
  //  lib_global   rtl        gen_npr  .sv   1.0.0   generator of registers with attribute "noprune"
  //  lib_global   rtl        gen_ced  .sv   1.0.0   generator of clock enable for data
  //  lib_global   rtl        gen_cen  .sv   1.0.0   generator of clock enable
  //  lib_global   rtl        gen_sstr .sv   1.0.0   generator of single strobe
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Revision List
  //
  //  Version   Date       Author
  //
  //  2.0.0     26.09.16   Pogodaev Danil
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  module eth_rx_rmii_v2 (

  //     clear
    
         i_sclr_n       ,

  //     setting

         i_set_speed    ,

  //     rmii input

         i_rmii_clk     ,
         i_rmii_crs_dv  ,
         i_rmii_rxd     ,

  //     rmii recover

         o_rmii_clk     ,
         o_rmii_crs     ,
         o_rmii_rx_dv   ,
         o_rmii_rxd     ,

  //     rmii output

         o_lan_clk      ,
         o_lan_cen      ,
         o_lan_ced      ,
         o_lan_rx_dv    ,
         o_lan_rxd     );

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // interface
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
                                                      //      |                                          |         |         |
  //-----------------------// clear ------------------//------|------------------------------------------|---------|---------|------------------------------------------------------------------------------------------------//
                                                      //      |                                          |         |         |
  input  logic                i_sclr_n           ;    // opt  | sync clear (ative low)                   | strob   | def: 1  |
                                                      //      |                                          |         |         |
  //-----------------------// setting ----------------//------|------------------------------------------|---------|---------|------------------------------------------------------------------------------------------------//
                                                      //      |                                          |         |         |
  input  logic    [ 1 :0 ]    i_set_speed        ;    // req  | speed of interface                       | pdata   | def: 0  |
                                                      //      |                                          |         |         |
                                                      //      |   0 : 1000 mbit/s                        |         |         | info: not used for rmii interface
                                                      //      |   1 :  100 mbit/s                        |         |         |
                                                      //      |   2 :   10 mbit/s                        |         |         |
                                                      //      |                                          |         |         |
  //-----------------------// rmii input -------------//------|------------------------------------------|---------|---------|------------------------------------------------------------------------------------------------//
                                                      //      |                                          |         |         |
  input  logic                i_rmii_clk         ;    // req  | receive clock                            | clock   |         |
                                                      //      |                                          |         |         |
                                                      //      |  - for 100 mbit/s : 50 mhz               |         |         |
                                                      //      |  - for 10  mbit/s : 50 mhz               |         |         |
                                                      //      |                                          |         |         |
  input  logic    [ 1 :0 ]    i_rmii_rxd         ;    // req  | receive data                             | pdata   |         | info: for latching used rising edge of clock
  input  logic                i_rmii_crs_dv      ;    // req  | receive data valid & carrier sense       | strob   |         | 
                                                      //      |                                          |         |         |
  //-----------------------// rmii recover -----------//------|------------------------------------------|---------|---------|-----------------------------------------------------------------------------------------------//
                                                      //      |                                          |         |         |
  output logic                o_rmii_clk         ;    // test | receive clock / speed adapter            |         |         |
                                                      //      |                                          |         |         |
                                                      //      |  - for 100 mbit/s : 50 mhz               | clock   |         |
                                                      //      |  - for 10  mbit/s :  5 mhz               | strob   |         |
                                                      //      |                                          |         |         |
  output logic    [ 1 :0 ]    o_rmii_rxd         ;    // opt  | receive data                             | pdata   |         |
  output logic                o_rmii_rx_dv       ;    // opt  | receive data valid                       | strob   |         | 
  output logic                o_rmii_crs         ;    // opt  | receive carrier sense                    | strob   |         | 
                                                      //      |                                          |         |         |
  //-----------------------// rmii output ------------//------|------------------------------------------|---------|---------|------------------------------------------------------------------------------------------------//
                                                      //      |                                          |         |         |
  output logic                o_lan_clk          ;    // req  | receive clock                            | clock   |         |
                                                      //      |                                          |         |         |
                                                      //      |  - for 100 mbit/s : 50 mhz               |         |         |
                                                      //      |  - for 10  mbit/s : 50 mhz               |         |         |
                                                      //      |                                          |         |         |
  output logic                o_lan_cen          ;    // req  | receive clock enable                     | pulse   |         | width: lan_clk
                                                      //      |                                          |         |         |
                                                      //      |  - for 100 mbit/s : high                 |         |         |
                                                      //      |  - for 10  mbit/s : 5 mhz                |         |         |
                                                      //      |                                          |         |         |
  output logic                o_lan_ced          ;    // req  | receive clock enable for data            | pulse   |         | width: lan_cen
                                                      //      |                                          |         |         |
                                                      //      |  - for 100 mbit/s : period = 4 lan_cen   |         |         |
                                                      //      |  - for 10  mbit/s : period = 4 lan_cen   |         |         |
                                                      //      |                                          |         |         |
  output logic    [ 7 :0 ]    o_lan_rxd          ;    // req  | receive data                             | pdata   |         |
  output logic                o_lan_rx_dv        ;    // req  | receive data valid                       | strob   |         | 
                                                      //      |                                          |         |         |
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // logic
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  //-----------------------// in logic ----------| [ after in reg ]

  var    logic    [ 1 :0 ]    ri_rmii_rxd        ;
  var    logic    [ 2 :0 ]
                  [ 1 :0 ]    ri_rmii_rxd_z      ;

  var    logic                ri_rmii_crs_dv     ;
  var    logic    [ 2 :0 ]    ri_rmii_crs_dv_z   ;

  //-----------------------// clock manager -----|

  var    logic                rmii_clk           ;
  var    logic                rmii_clk_s         ;
  var    logic                rmii_cen           ;
  var    logic                rmii_ced           ;

  var    logic                gen_rmii_cen       ;
  var    logic                gen_rmii_ced       ;
  var    logic                gen_rmii_clk_10    ;

  //-----------------------// recover rx_dv -----|

  var    logic                rec_rx_dv      = 0 ;
  var    logic    [ 1 :0 ]    rec_rx_dv_z        ;
  var    logic                rec_rx_dv_keep     ;

  //-----------------------// recover crs -------|

  var    logic                rec_crs        = 0 ;
  var    logic    [ 1 :0 ]    rec_crs_z          ;

  var    logic                ri_rmii_crs_dv_re  ;
  var    logic                ri_rmii_crs_dv_fe  ;

  //-----------------------// rmii recover ------|

  var    logic    [ 1 :0 ]    rmii_rxd           ;
  var    logic                rmii_crs_dv        ;
  var    logic                rmii_crs           ;
  var    logic                rmii_rx_dv         ;

  //-----------------------// deserializer ------|

  var    logic                shift_ena          ;
  var    logic    [ 4 :0 ]    shift_ena_z        ;
  var    logic    [ 7 :0 ]    shift_reg      = 0 ;
  var    logic    [ 1 :0 ]    shift_cnt      = 0 ;
  var    logic    [ 1 :0 ]    shift_sd           ;
  var    logic    [ 7 :0 ]    shift_pd       = 0 ;
  var    logic                shift_latch    = 0 ;
  var    logic                shift_load     = 0 ;
  var    logic                shift_str      = 0 ;

  //-----------------------// rmii output -------|

  var    logic                lan_clk            ;
  var    logic                lan_cen            ;
  var    logic                lan_ced            ;

  var    logic    [ 7 :0 ]    lan_rxd        = 0 ;
  var    logic                lan_rx_dv      = 0 ;

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  //==========================================================================================================================================================================================================================//
  // body of module
  //==========================================================================================================================================================================================================================//

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // out logic (without out reg)
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  // rmii recover

  assign  o_rmii_rxd    = rmii_rxd   ;
  assign  o_rmii_rx_dv  = rmii_rx_dv ;
  assign  o_rmii_crs    = rmii_crs   ;
  assign  o_rmii_clk    = rmii_clk_s ;

  // rmii output

  assign  o_lan_clk     = lan_clk    ;
  assign  o_lan_cen     = lan_cen    ;
  assign  o_lan_ced     = lan_ced    ;
  assign  o_lan_rxd     = lan_rxd    ;
  assign  o_lan_rx_dv   = lan_rx_dv  ;

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // clock manager
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  assign  rmii_clk   =                                      i_rmii_clk    ; // repeat
  assign  rmii_clk_s = (i_set_speed == 1) ? i_rmii_clk : ~gen_rmii_clk_10 ;
  assign  rmii_cen   = (i_set_speed == 1) ? 1'b1       :  gen_rmii_cen    ;
  assign  rmii_ced   =                                    gen_rmii_ced    ;

  gen_cen // generator of clock enable

  #(.pT_CEN (10), // period of clock ena  | t.clk
    .pN_REG ( 0)) // number of output reg | register

  gen_cen (.i_sclr(~i_sclr_n), .i_clk(rmii_clk), .o_cen(gen_rmii_cen));
  //                                                    ^
  gen_ced // generator of clock enable for data

  #(.pT_CEN   (0), // period of clock ena          | t.clk
    .pT_CED   (4), // period of clock ena for data | t.cen
    .pN_REG_I (0), // number of reg input          | register
    .pN_REG_O (0)) // number of reg output         | register

  gen_ced (.i_sclr(~i_sclr_n), .i_clk(rmii_clk), .i_cen(rmii_cen), .o_ced(gen_rmii_ced));
  //                                                                      ^
  // generator of rmii clock (meander) for 10 mbit/s

  gen_sstr // generator of single strob

  #(.pW_LEN   (3), // width of length      | bit
    .pN_REG_I (0), // number of reg input  | register
    .pN_REG_O (0)) // number of reg output | register

  gen_sstr (.i_clk(rmii_clk), .i_clk_en(1'b1), .i_sclr(~i_sclr_n), .i_start(gen_rmii_cen), .i_length(5), .o_strob(gen_rmii_clk_10));
  //                                                                        ^                        ^            ^
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // recover signal / receive data valid (rx_dv)
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  // receive data valid

  always_ff @(posedge rmii_clk)
    if (~i_sclr_n)
      rec_rx_dv <= 1'b0;
    else
      if (rmii_cen)
        begin
          if (ri_rmii_crs_dv)
            begin
              if (ri_rmii_rxd == 2'b01) // search preambule
                rec_rx_dv <= 1'b1;
            end
          else
            if (rec_rx_dv_keep == 0) // keep checking / see literature (1)
              rec_rx_dv <= 1'b0;
        end

  // receive data valid / keep

  assign  rec_rx_dv_keep = ri_rmii_crs_dv | ri_rmii_crs_dv_z [1];

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // recover signal / carrier sense (crs)
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  always_ff @(posedge rmii_clk)
    if (~i_sclr_n)
      rec_crs <= 1'b0;
    else
      if (rmii_cen)
        begin
          if (ri_rmii_crs_dv_re & ~rec_rx_dv)
            rec_crs <= 1'b1;
          else
            if (ri_rmii_crs_dv_fe)
              rec_crs <= 1'b0;
        end

  det_de // detector of double edges

  #(.pN_REG_I (0), // number of reg in  | bit
    .pN_REG_O (0)) // number of reg out | bit

  de__ri_rmii_crs_dv (.i_clk(rmii_clk), .i_clk_en(rmii_cen), .i_sclr(~i_sclr_n), .i_strob(ri_rmii_crs_dv), .o_edge_re(ri_rmii_crs_dv_re), .o_edge_fe(ri_rmii_crs_dv_fe) );
  //                                                                                      ^                           ^                              ^
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // rmii interface (2 bit) / sync to recover
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  assign  rmii_rxd     = ri_rmii_rxd_z    [2] ;
  assign  rmii_crs_dv  = ri_rmii_crs_dv_z [2] ;
  assign  rmii_crs     = rec_crs_z        [1] ;
  assign  rmii_rx_dv   = rec_rx_dv_z      [1] & rec_rx_dv ; // truncate last di-bit / for sync

  // delay line

  z_line #(.pV_DELAY (2), .pW_DATA (2))  z__ri_rmii_rxd     ( i_rmii_clk, rmii_cen, ~i_sclr_n, ri_rmii_rxd    ,, ri_rmii_rxd_z    );
  z_line #(.pV_DELAY (2), .pW_DATA (1))  z__ri_rmii_crs_dv  ( i_rmii_clk, rmii_cen, ~i_sclr_n, ri_rmii_crs_dv ,, ri_rmii_crs_dv_z );
  z_line #(.pV_DELAY (1), .pW_DATA (1))  z__rec_crs         ( i_rmii_clk, rmii_cen, ~i_sclr_n, rec_crs        ,, rec_crs_z        );
  z_line #(.pV_DELAY (1), .pW_DATA (1))  z__rec_rx_dv       ( i_rmii_clk, rmii_cen, ~i_sclr_n, rec_rx_dv      ,, rec_rx_dv_z      );
  //                                                                                           ^                 ^
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // shift register / deserializer
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  
  assign  shift_ena = rmii_rx_dv ; // shift enable
  assign  shift_sd  = rmii_rxd   ; // serial data (2 bit)

  // shift register (8 bit)

  always_ff @(posedge rmii_clk)
    if (~i_sclr_n)
      shift_reg <= 8'h0;
    else
      if (rmii_cen)
        begin
          if (shift_ena == 0)
            shift_reg [7:0] <= 8'h0;
          else
            begin
              shift_reg [7:6] <= shift_sd  [1:0] ;
              shift_reg [5:4] <= shift_reg [7:6] ;
              shift_reg [3:2] <= shift_reg [5:4] ;
              shift_reg [1:0] <= shift_reg [3:2] ;
            end
        end

  // shift counter (0 to 3)

  always_ff @(posedge rmii_clk)
    if (~i_sclr_n)
      shift_cnt <= 2'd0;
    else
      if (rmii_cen)
        begin
          if (shift_ena == 0)
            shift_cnt <= 2'd0;
          else
            shift_cnt <= shift_cnt + 1;
        end

  // shift latch

  always_ff @(posedge rmii_clk)
    if (~i_sclr_n)
      shift_latch <= 1'b0;
    else
      if (rmii_cen)
        shift_latch <= & shift_cnt; // shift_cnt = 3

  // shift load & parallel data (8 bit)

  always_ff @(posedge rmii_clk)
    if (~i_sclr_n)
      begin
        shift_pd   <= 8'h0;
        shift_load <= 1'b0;
        shift_str  <= 1'b0;
      end
    else
      if (rmii_cen)
        begin
          if (shift_latch)
            shift_pd   <= shift_reg       ;
            shift_load <= shift_latch     ; // for testing only
            shift_str  <= shift_ena_z [4] ;
        end

  z_line // delay line

  #(.pV_DELAY (4), // value of delay | t.clk_en
    .pW_DATA  (1)) // width of data  | bit

  z__shift_ena (.i_clk(rmii_clk), .i_clk_en(rmii_cen), .i_sclr(~i_sclr_n), .i_data(shift_ena), .o_delay(shift_ena_z));
  //                                                                               ^                    ^
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // rmii output (8 bit)
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  // clock

  assign  lan_clk  = rmii_clk ;
  assign  lan_cen  = rmii_cen ;
  assign  lan_ced  = rmii_ced ;

  // data (8 bit)

  always_ff @(posedge rmii_clk)
    if (~i_sclr_n)
      begin
        lan_rx_dv <= 1'd0;
        lan_rxd   <= 8'd0;
      end
    else
      if (rmii_cen)
        begin
          if (rmii_ced) // sync to rmii_ced
            begin
              lan_rx_dv <= shift_str ;
              lan_rxd   <= shift_pd  ;
            end
        end

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // in/out registers : befor reg (i/no) / after reg (ri/ro)
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  // rmii input -----------------------// rmii input ---------------------------------------------------------------//
  
  z_line #(.pV_DELAY (2), .pW_DATA (2))  ri__rmii_rxd     ( i_rmii_clk, 1'b1, 1'b0, i_rmii_rxd    , ri_rmii_rxd    ); // used fast input reg
  z_line #(.pV_DELAY (2), .pW_DATA (1))  ri__rmii_crs_dv  ( i_rmii_clk, 1'b1, 1'b0, i_rmii_crs_dv , ri_rmii_crs_dv ); // used fast input reg
  //                  ^                                                             ^               ^
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  endmodule
  
  
  

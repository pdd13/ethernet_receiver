  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Company      : Micran, Department of Telecommunications
  //
  //  Engineer     : Pogodaev Danil
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Library      : lib_ethernet / library of ethernet components
  //
  //  File Name    : eth_rx_v2 .sv (rtl)
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Description  : Receiver of ethernet frames (packets).
  //
  //  Purpose (1)  : Custom mode implementation (custom clock) / see table (1)
  //
  //                  - clock                 / o_rx_clk / repeat
  //                  - clock enable          / o_rx_cen / period = pT_CST_CEN
  //                  - clock enable for data / o_rx_ced / period = pT_CST_CED
  //
  //  Purpose (2)  : MII-interface implementation
  //
  //                  - generate clocks of ethernet frames / see table (1)
  //
  //                     � receive clock (repeater) / o_rx_clk
  //
  //                        for 100 mbit/s : 25 mhz
  //                        for 10  mbit/s : 2.5 mhz
  //
  //                     � receive clock enable / o_rx_cen
  //
  //                        for 100 mbit/s : high
  //                        for 10  mbit/s : high
  //
  //                     � receive clock enable for data / o_rx_ced
  //
  //                        for 100 mbit/s : period = 2 rx_cen
  //                        for 10  mbit/s : period = 2 rx_cen
  //
  //                  - convert data stream 4-bit to 8-bit 
  //
  //  Purpose (3)  : RMII-interface implementation
  //
  //                  - generate clocks of ethernet frames / see table (1)
  //
  //                     � receive clock (repeater) / o_rx_clk
  //
  //                        for 100 mbit/s : 50 mhz
  //                        for 10  mbit/s : 50 mhz
  //
  //                     � receive clock enable / o_rx_cen
  //
  //                        for 100 mbit/s : high
  //                        for 10  mbit/s : 5 mhz
  //
  //                     � receive clock enable for data / o_rx_ced
  //
  //                        for 100 mbit/s : period = 4 rx_cen
  //                        for 10  mbit/s : period = 4 rx_cen
  //
  //                  - recover input signals of rmii-interface
  //
  //                     � carrier sense      / rmii_crs
  //                     � receive data valid / rmii_rx_dv
  //
  //                  - convert data stream 2-bit to 8-bit
  //
  //  Purpose (4)  : GMII-interface implementation
  //
  //                  - generate clocks of ethernet frames / see table (1)
  //
  //                     � receive clock (repeater) / o_rx_clk
  //
  //                        for 1000 mbit/s : 125 mhz
  //                        for 100  mbit/s : 25 mhz
  //                        for 10   mbit/s : 2.5 mhz
  //
  //                     � receive clock enable / o_rx_cen
  //
  //                        for 1000 mbit/s : high
  //                        for 100  mbit/s : high
  //                        for 10   mbit/s : high
  //
  //                     � receive clock enable for data / o_rx_ced
  //
  //                        for 1000 mbit/s : high
  //                        for 100  mbit/s : period = 2 rx_cen
  //                        for 10   mbit/s : period = 2 rx_cen
  //
  //                  - convert data stream 4-bit to 8-bit (for 10/100 mbit/s)
  //
  //  Purpose (5)  : Detect frame header / see parameter 'pE_RX_HEAD'
  //
  //                  - check the preamble
  //                  - check the start-frame delimiter (sfd)
  //                  - select the body of packets (truncate the header)
  //
  //  Purpose (6)  : Generate strobes for ethernet frames / see table (2)
  //
  //                  - strobe                / o_str_name [0]
  //                  - strobe / start bit    / o_str_name [1] / width: o_rx_cen
  //                  - strobe / stop bit     / o_str_name [2] / width: o_rx_cen
  //                  - strobe / byte counter / o_cnt_name
  //
  //  Purpose (7)  : Calculate length of ethernet frames
  //
  //                  - frame body / o_rx_len_body
  //                  - frame gap  / o_rx_len_gap
  //
  //  Purpose (8)  : Extract the IP and MAC headers
  //
  //                  - ip header               / o_dat_ip
  //                  - mac destination address / o_dat_mda
  //                  - mac source address      / o_dat_msa
  //                  - mac length or type      / o_dat_mlt
  //
  //  Purpose (9)  : Calculate frame check sequence (fcs) crc-32
  //
  //                  - calculating the crc-32 sum for body of input frame
  //                  - compare calculated sum and residual constant (32'h c704dd7b)
  //                  - generating the crc error signal (o_rx_crc_err)
  //
  //  Purpose (10) : Count the number of ethernet frames (packets) / see note (2)
  //
  //                  - packet all   / o_cnt_p_all
  //                  - packet error / o_cnt_p_err
  //                  - packet speed / o_cnt_p_spd
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Note (1)     : Structure of ethernet frames (std. 802.3)
  //
  //                  - frame header preamble
  //                  - start-frame delimiter (sfd)
  //                  - destination mac address (mda)
  //                  - source mac address (msa)
  //                  - length of payload / type of protocol (mlt)
  //                  - data payload and pad
  //                  - frame check sequence (fcs) crc-32
  //                  - inter-frame gap
  //
  //  Note (2)     : Group counters for packet length (frame body)
  //
  //                 For example / SunLite GigE (Sunrise Telecom)
  //
  //                  - all size       / o_rx_cnt_p [0]
  //                  - 64-127 byte    / o_rx_cnt_p [1]
  //                  - 128-255 byte   / o_rx_cnt_p [2]
  //                  - 256-511 byte   / o_rx_cnt_p [3]
  //                  - 512-1023 byte  / o_rx_cnt_p [4]
  //                  - 1024-1518 byte / o_rx_cnt_p [5]
  //                  - oversize       / o_rx_cnt_p [6]
  //
  //  Note (3)     : Attribute "synthesis noprune"
  //  
  //                 A Verilog HDL synthesis attribute that prevents the Quartus II software from removing
  //                 a register that does not directly or indirectly feed a top-level output or bidir pin,
  //                 such as a fan-out free register.
  //
  //  Note (4)     : Recommended Quartus assignment for pins of LAN interface: "Fast Input Register".
  //
  //  Note (5)     : Used additional input registers for these signals / def: 3 reg
  //
  //                  - i_reset_n       / reset
  //
  //                  - i_set_cut       / setting
  //                  - i_set_jumbo     / setting
  //                  - i_set_speed     / setting
  //
  //                  - i_rx_set_p_spd  / rx counters / setting
  //                  - i_rx_cnt_p_clr  / rx counters / counters
  //                  - i_rx_rcnt_p_clr / rx counters / counters (rhythm)
  //                  - i_rx_rcnt_p_upd / rx counters / counters (rhythm)
  //
  //                 Reason: metastability and high fan-out.
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Limitation   : Attribute "synthesis noprune" used for Altera only (parameter 'pE_GEN_NPR')
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Attached (1) : Additional (detailed) code comments / eth_rx_v2 - comment .txt
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Table (1)    : Clock manager of ethernet frames for receiver
  //
  //                 |--------------------------------------------------------------------------------------------------|
  //                 |                       |          |                       frequency (period)                      |
  //                 |        setting        |  clock   |---------------------------------------------------------------|
  //                 |                       |          |      10 mbit/s      |     100 mbit/s     |    1000 mbit/s     |
  //                 |=======================|==========|=====================|====================|====================|
  //                 |                       |  rx_clk  |  2.50 mhz (400 ns)  |  25.0 mhz (40 ns)  |  no                |
  //                 |  mii interface:       |----------|---------------------|--------------------|--------------------|
  //                 |                       |  rx_cen  |  1'b1               |  1'b1              |  no                |
  //                 |  pS_IO_TYPE = mii     |----------|---------------------|--------------------|--------------------|
  //                 |                       |  rx_ced  |  1.25 mhz (800 ns)  |  12.5 mhz (80 ns)  |  no                |
  //                 |=======================|==========|=====================|====================|====================|
  //                 |                       |  rx_clk  |  50.00 mhz (20 ns)  |  50.00 mhz (20 ns) |  no                |
  //                 |  rmii interface:      |----------|---------------------|--------------------|--------------------|
  //                 |                       |  rx_cen  |  5.00 mhz (200 ns)  |  1'b1              |  no                |
  //                 |  pS_IO_TYPE = rmii    |----------|---------------------|--------------------|--------------------|
  //                 |                       |  rx_ced  |  1.25 mhz (800 ns)  |  12.5 mhz (80 ns)  |  no                |
  //                 |=======================|==========|=====================|====================|====================|
  //                 |                       |  rx_clk  |  2.50 mhz (400 ns)  |  25.0 mhz (40 ns)  |  125.0 mhz (8 ns)  |
  //                 |  gmii interface:      |----------|---------------------|--------------------|--------------------|
  //                 |                       |  rx_cen  |  1'b1               |  1'b1              |  1'b1              |
  //                 |  pS_IO_TYPE = gmii    |----------|---------------------|--------------------|--------------------|
  //                 |                       |  rx_ced  |  1.25 mhz (800 ns)  |  12.5 mhz (80 ns)  |  1'b1              |
  //                 |=======================|==========|===============================================================|
  //                 |                       |  rx_clk  |  i_cst_clk                                                    |
  //                 |  custom mode:         |----------|---------------------------------------------------------------|
  //                 |                       |  rx_cen  |  pT_CST_CEN = 0 : i_cst_cen                                   |
  //                 |  pS_IO_TYPE = custom  |          |  pT_CST_CEN > 0 : i_cst_clk * pT_CST_CEN                      |
  //                 |                       |----------|---------------------------------------------------------------|
  //                 |                       |  rx_ced  |  pT_CST_CED = 0 : i_cst_ced                                   |
  //                 |                       |          |  pT_CST_CED > 0 : i_cst_clk * pT_CST_CEN * pT_CST_CED         |
  //                 |==================================================================================================|
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Table (2)    : Structure of ethernet frames (std. 802.3) for receiver / see also note (1)
  //
  //                 |------------------------------------------------------------------------------|
  //                 | frame header  |      mac header       |        data       |  fcs  | ethernet |
  //                 |---------------|-----------------------|-------------------|-------| receiver |
  //                 |       8       |          14           |      46-1500      |   4   |          |
  //                 |===============|=======================|===================|=======|==========|
  //                 |  prm  |  sfd  |  mda  |  msa  |  mlt  |  payload  |  pad  |  crc  | strob    |
  //                 |-------|-------|-------|-------|-------|-----------|-------|-------| name     |
  //                 |   7   |   1   |   6   |   6   |   2   |  0-1500   | 0-46  |   4   |          |
  //                 |=======|=======|=======|=======|=======|===========|=======|=======|==========|
  //                 |       |       |       |       |       |           |       |       |          |
  //                 |       |       | ***** | ***** | ***** | ********* | ***** | ***** | str_body |
  //                 |       |       | ***** | ***** | ***** |           |       |       | str_mac  |
  //                 |       |       | ***** |       |       |           |       |       | str_mda  |
  //                 |       |       |       | ***** |       |           |       |       | str_msa  |
  //                 |       |       |       |       | ***** |           |       |       | str_mlt  |
  //                 |       |       |       |       |       | ********* | ***** | ***** | str_pay  | with crc (!)
  //                 |       |       |       |       |       | ***       |       |       | str_ip   |
  //                 |       |       |       |       |       |           |       |       |          |
  //                 |------------------------------------------------------------------------------|
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Diagram (1)  : Connection options (interface) of ethernet components
  //
  //
  //    --------------  � lan_signals:     ------------  � rx_signals:     --------------  � rxb_signals:      -----------
  //   |              |------------------>|            |----------------->|              |------------------->|           |
  //   |  tb_eth_emu  |   lan interface   |   eth_rx   |   rx packet      |  eth_rx_buf  |   rxb packet       |           |
  //   |              |   lan counters    |            |   rx counters    |              |   rxb counters     |           |
  //    --------------    lan packet       ------------                    --------------                     |           |
  //                                                                                                          |           |
  //                                                                                                          |           |
  //    --------------  � lan_signals:     ------------  � rx_signals:     --------------  � rxb_signals:     |           |
  //   |              |------------------>|            |----------------->|              |------------------->|           |
  //   |   lan phy    |   lan interface   |   eth_rx   |   rx packet      |  eth_rx_buf  |   rxb packet       |   logic   |
  //   |              |                   |            |   rx counters    |              |   rxb counters     |           |
  //    --------------                     ------------                    --------------                     |           |
  //                                                                                                          |           |
  //                                                                                                          |           |
  //    --------------  � lan_signals:     ------------  � tx_signals:     --------------  � txb_signals:     |           |
  //   |              |<------------------|            |<-----------------|              |<-------------------|           |
  //   |   lan phy    |   lan interface   |   eth_tx   |   tx packet      |  eth_tx_buf  |   txb packet       |           |
  //   |              |   lan counters    |            |   tx counters    |              |   txb counters     |           |
  //    --------------    lan packet       ------------                    --------------                      -----------
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Literature (1) : IEEE Std 802.3-2002 / Clause 22 : Media Independent Interface (MII)
  //                                       / Clause 35 : Gigabit Media Independent Interface (GMII)
  //
  //  Literature (2) : RMII Specification (RMII Consortium) [1998]
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Include Files
  //  
  //  Category       Type       Name                 Ver     Description
  //
  //  lib_internal   rtl        eth_rx_cst_v2  .sv   2.0.0   ethernet preliminary receiver for custom mode
  //  lib_internal   rtl        eth_rx_mii_v2  .sv   2.0.0   ethernet preliminary receiver for mii-interface
  //  lib_internal   rtl        eth_rx_rmii_v2 .sv   2.0.0   ethernet preliminary receiver for rmii-interface
  //  lib_internal   rtl        eth_rx_gmii_v2 .sv   2.0.0   ethernet preliminary receiver for gmii-interface
  //  lib_internal   rtl        eth_rx_mux_v2  .sv   2.0.0   multiplexer of ethernet preliminary receivers
  //  lib_internal   rtl        eth_rx_head_v2 .sv   2.0.0   frame header detector (preamble and start-frame delimiter)
  //  lib_internal   rtl        eth_rx_str_v2  .sv   2.0.0   strobe generator and length calculator for ethernet frames
  //  lib_internal   rtl        eth_rx_dat_v2  .sv   2.0.0   data extractor for ip and mac headers
  //  lib_internal   rtl        eth_rx_out_v2  .sv   2.0.0   output registers and latching data (mac and ip headers)
  //
  //  lib_ethernet   function   eth_crc32_8d   .sv   1.0.0   calculator of crc-32 (data 8 bit)
  //  lib_ethernet   rtl        eth_cnt_v2     .sv   2.0.0   counters of ethernet frames (packets)
  //  lib_ethernet   rtl        eth_rx_crc_v2  .sv   2.0.0   crc calculator for received ethernet frames
  //
  //  lib_global     function   cbit           .sv   1.0.0   bits calculator for input number (round up)
  //  lib_global     rtl        det_se         .sv   1.0.0   detector of single edges for input strobe
  //  lib_global     rtl        det_de         .sv   1.0.0   detector of double edges for input strobe
  //  lib_global     rtl        gen_npr        .sv   1.0.0   generator of registers with attribute "noprune"
  //  lib_global     rtl        gen_ced        .sv   1.0.0   generator of clock enable for data
  //  lib_global     rtl        gen_cen        .sv   1.0.0   generator of clock enable
  //  lib_global     rtl        gen_sstr       .sv   1.0.0   generator of single strobe
  //  lib_global     rtl        z_line         .sv   1.0.0   delay line for serial and parallel data
  //  lib_global     rtl        shift_s2p      .sv   1.0.0   shift register for serial to parallel
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Revision List
  //
  //  Version   Date       Author
  //
  //  2.0.0     26.09.16   Pogodaev Danil
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  module eth_rx_v2        (

  //---- reset -----------|

         i_reset_n        ,

  //---- setting ---------|

         i_set_cut        ,
         i_set_speed      ,
         i_set_jumbo      ,

  //---- lan interface ---|

  //     lan gmii

         i_gmii_clk       ,
         i_gmii_rxd       ,
         i_gmii_rx_dv     ,

  //     lan rmii

         i_rmii_clk       ,
         o_rmii_clk_s     ,
         i_rmii_rxd       ,
         i_rmii_crs_dv    ,

  //     lan mii

         i_mii_clk        ,
         i_mii_rxd        ,
         i_mii_rx_dv      ,

  //---- custom mode -----|

         i_cst_clk        ,
         i_cst_cen        ,
         i_cst_ced        ,
         i_cst_rxd        ,
         i_cst_rx_dv      ,

  //---- rx packet -------|

  //     clock

         o_rx_clk         ,
         o_rx_cen         ,
         o_rx_ced         ,

  //     strob

         o_rx_str_body    ,
         o_rx_cnt_body    ,
         o_rx_str_mac     ,
         o_rx_cnt_mac     ,
         o_rx_str_mda     ,
         o_rx_str_msa     ,
         o_rx_str_mlt     ,
         o_rx_str_pay     ,
         o_rx_cnt_pay     ,
         o_rx_str_ip      ,
         o_rx_cnt_ip      ,
         o_rx_str_gap     ,
         o_rx_cnt_gap     ,

  //     data

         o_rx_dat_load    ,
         o_rx_dat_body    ,
         o_rx_rdy_mac     ,
         o_rx_dat_mda     ,
         o_rx_dat_msa     ,
         o_rx_dat_mlt     ,
         o_rx_rdy_ip      ,
         o_rx_dat_ip      ,

  //     length

         o_rx_rdy_body    ,
         o_rx_len_body    ,
         o_rx_rdy_gap     ,
         o_rx_len_gap     ,

  //     fcs crc

         o_rx_crc_rdy     ,
         o_rx_crc_err     ,

  //---- rx counters -----|

  //     setting

         i_rx_set_p_top   ,
         i_rx_set_p_low   ,
         i_rx_set_p_spd   ,

  //     detector

         i_rx_det_p_off   ,
         o_rx_det_p_err   ,
         o_rx_det_p_cut   ,
         o_rx_det_p_rdy   ,

  //     counters

         i_rx_cnt_p_clr   ,
         o_rx_cnt_p_rdy   ,
         o_rx_cnt_p_all   ,
         o_rx_cnt_p_err   ,
         o_rx_cnt_p_off   ,
         o_rx_cnt_p_spd   ,

  //     counters (rhythm)

         i_rx_rcnt_p_clr  ,
         i_rx_rcnt_p_upd  ,
         o_rx_rcnt_p_rdy  ,
         o_rx_rcnt_p_all  ,
         o_rx_rcnt_p_err  ,
         o_rx_rcnt_p_off  ,
         o_rx_rcnt_p_spd );

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // include files : functions
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
                                                                   //      |
  `include "cbit.sv"                                               // req  | bits calculator for input number (round up)
                                                                   //      |
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // parameters : setting
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
                                                                   //      |                                            |          |           |
  // interface ----------------------------------------------------//------|--------------------------------------------|----------|-----------|------------------------------------------------------------------------------//
                                                                   //      |                                            |          |           |
  parameter      pS_IO_TYPE   = "gmii"                        ;    // req  | setting of interface type                  | literal  |           |
                                                                   //      |                                            |          |           |
                                                                   //      |   "custom" : used custom mode              |          |           | info: used custom clock (see custom mode)
                                                                   //      |   "gmii"   : used gmii interface           |          |           |
                                                                   //      |   "rmii"   : used rmii interface           |          |           |
                                                                   //      |   "mii"    : used mii interface            |          |           |
                                                                   //      |                                            |          |           |
  // custom mode (see table 1) ------------------------------------//------|--------------------------------------------|----------|-----------|------------------------------------------------------------------------------//
                                                                   //      |                                            |          |           |
  parameter int  pT_CST_CEN   = 0                             ;    // opt  | period of clock enable                     | t.clk    | def: 0    | info: when '0', used 'i_cst_cen' / see table (1)
  parameter int  pT_CST_CED   = 0                             ;    // opt  | period of clock enable for data            | t.cen    | def: 0    | info: when '0', used 'i_cst_ced' / see table (1)
                                                                   //      |                                            |          |           |
  // packet manipulation ------------------------------------------//------|--------------------------------------------|----------|-----------|------------------------------------------------------------------------------//
                                                                   //      |                                            |          |           |
  parameter bit  pE_RX_CUT    = 0                             ;    // opt  | enable of cut off oversize frames          | on/off   | def: 0    | info: see also 'pV_RX_LEN'   and 'i_set_cut'
  parameter bit  pE_RX_JUMBO  = 0                             ;    // opt  | enable of jumbo-frames support             | on/off   | def: 0    | info: see also 'pV_RX_LEN_J' and 'i_set_jumbo'
                                                                   //      |                                            |          |           |
  // packet structure ---------------------------------------------//------|--------------------------------------------|----------|-----------|------------------------------------------------------------------------------//
                                                                   //      |                                            |          |           |
  // frame header                                                  //------|--------------------------------------------|----------|-----------|------------------------------------------------------------------------------//
                                                                   //      |                                            |          |           |
  parameter bit  pE_RX_HEAD   = 1                             ;    // req  | enable of frame header                     | on/off   | def: 1    | info: used for custom mode only ; see 'comment.txt' (1)
                                                                   //      |                                            |          |           |
  // frame body                                                    //------|--------------------------------------------|----------|-----------|------------------------------------------------------------------------------//
                                                                   //      |                                            |          |           |
  parameter int  pW_RX_LEN    = 11                            ;    // req  | width of length / 802.3                    | bit      | def: 11   |
  parameter int  pV_RX_LEN    = 1518                          ;    // opt  | value of length / 802.3                    | byte     | def: 1518 | info: used for cut off oversize frames
  parameter int  pW_RX_LEN_J  = 14                            ;    // opt  | width of length / jumbo                    | bit      | def: 14   |
  parameter int  pV_RX_LEN_J  = 9018                          ;    // opt  | value of length / jumbo                    | byte     | def: 9018 | info: used for cut off oversize frames
  parameter int  pW_RX_GAP    = 16                            ;    // opt  | width of gap                               | bit      | def: 16   |
                                                                   //      |                                            |          |           |
  // mac header                                                    //------|--------------------------------------------|----------|-----------|------------------------------------------------------------------------------//
                                                                   //      |                                            |          |           |
  parameter bit  pE_RX_MAC    = 0                             ;    // opt  | enable of mac header                       | on/off   | def: 0    |
                                                                   //      |                                            |          |           |
  // ip header                                                     //------|--------------------------------------------|----------|-----------|------------------------------------------------------------------------------//
                                                                   //      |                                            |          |           |  
  parameter bit  pE_RX_IP     = 0                             ;    // opt  | enable of ip header                        | on/off   | def: 0    |
  parameter int  pN_RX_IPL    = 1                             ;    // opt  | number of first (low) byte in payload      | byte     | def: 1    |
  parameter int  pN_RX_IPH    = 4                             ;    // opt  | number of last (high) byte in payload      | byte     |           |
                                                                   //      |                                            |          |           |
  // packet counters ----------------------------------------------//------|--------------------------------------------|----------|-----------|------------------------------------------------------------------------------//
                                                                   //      |                                            |          |           |
  parameter int  pN_RX_CNT    = 0                             ;    // opt  | counters groups / number                   | group    | def: 0    | info: see purpose (10), note (2) and 'comment.txt' (2)
  parameter int  pW_RX_CNT_P  = 24                            ;    // opt  | counters packet / width of bus             | bit      | def: 24   |
  parameter int  pW_RX_CNT_S  = 32                            ;    // opt  | counters speed  / width of bus             | bit      | def: 32   | info: byte counter
                                                                   //      |                                            |          |           |
  // signal tap (for tetsing) -------------------------------------//------|--------------------------------------------|----------|-----------|------------------------------------------------------------------------------//
                                                                   //      |                                            |          |           |
  parameter bit  pE_GEN_NPR   = 0                             ;    // test | generator of registers with "noprune"      | on/off   | def: 0    | info: see note (3), for altera only (!)
                                                                   //      |                                            |          |           |
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // parameters : local (for logic)
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
                                                                   //      |                                            |          |           |
  // crc comparator -----------------------------------------------//------|--------------------------------------------|----------|-----------|------------------------------------------------------------------------------//
                                                                   //      |                                            |          |           |
  parameter int  pV_CRC_DLY   = 0                             ;    // opt  | value of delay for crc comporator          | t.cen    | def: 0    | info: used for fpga with low speed grade (set 1 to 3) ; see 'comment.txt' (3)
                                                                   //      |                                            |          |           |
  // out registers ------------------------------------------------//------|--------------------------------------------|----------|-----------|------------------------------------------------------------------------------//
                                                                   //      |                                            |          |           |
  parameter bit  pE_REG_OUT   = 1                             ;    // opt  | number of out registers                    | register | def: 1    | info: 1 reg = 1 t.clk ; see 'comment.txt' (4)
  parameter bit  pE_REG_CNT   = 0                             ;    // opt  | number of out registers / for counters     | register | def: 0    | info: 1 reg = 1 t.clk ; see 'comment.txt' (5)
                                                                   //      |                                            |          |           |
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // parameters : local (for i/o)
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
                                                                   //      |                                            |          |           |
  // packet structure ---------------------------------------------//------|--------------------------------------------|----------|-----------|------------------------------------------------------------------------------//
                                                                   //      |                                            |          |           |
  // frame body                                                    //------|--------------------------------------------|----------|-----------|------------------------------------------------------------------------------//
                                                                   //      |                                            |          |           |
  parameter int  lpW_RX_LEN   = pE_RX_JUMBO == 1 ?                 // req  | width of length / receiver                 | bit      |           |
                                pW_RX_LEN_J      :                 //      | width of length / jumbo                    | bit      | def: 14   |
                                pW_RX_LEN                     ;    //      | width of length / 802.3                    | bit      | def: 11   |
                                                                   //      |                                            |          |           |
  // ip header                                                     //------|--------------------------------------------|----------|-----------|------------------------------------------------------------------------------//
                                                                   //      |                                            |          |           |
  parameter int  lpN_RX_IPL   = pN_RX_IPL                     ;    // opt  | number of first (low) byte in payload      | byte     | def: 1    |
  parameter int  lpN_RX_IPH   = pN_RX_IPH >= pN_RX_IPL ?           // opt  | number of last (high) byte in payload      | byte     |           |
                                pN_RX_IPH  : pN_RX_IPL        ;    //      |                                            |          |           |
                                                                   //      |                                            |          |           |
  parameter int  lpN_RX_IP    = lpN_RX_IPH <= lpN_RX_IPL ? 1  :    //      |                                            |          |           |
                                lpN_RX_IPH  - lpN_RX_IPL + 1  ;    // opt  | number of bytes                            | byte     |           |
                                                                   //      |                                            |          |           |
  parameter int  lpW_RX_IP    = lpN_RX_IP * 8                 ;    // opt  | number of bits                             | bit      |           | info: used for 'rx_dat_ip'
                                                                   //      |                                            |          |           |
  parameter int  lpW_RX_IP_C  = cbit(1,lpN_RX_IP)             ;    // opt  | width of byte counter                      | bit      |           | info: used for 'rx_cnt_ip'
                                                                   //      |                                            |          |           |
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // interface
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
                                                                   //      |                                            |          |           |
  //-------------------------------------// reset -----------------//------|--------------------------------------------|----------|-----------|------------------------------------------------------------------------------//
                                                                   //      |                                            |          |           |
  input  logic                              i_reset_n         ;    // opt  | sync clear (ative low)                     | strob    | def: 1    | info: used 'rx_clk' ; see note (5) and 'comment.txt' (6)
                                                                   //      |                                            |          |           |
  //-------------------------------------// setting ---------------//------|--------------------------------------------|----------|-----------|------------------------------------------------------------------------------//
                                                                   //      |                                            |          |           |
  input  logic                              i_set_cut         ;    // opt  | enable of cut off oversize frames          | strob    | def: 0    | info: see also 'pE_RX_CUT'
                                                                   //      |                                            |          |           |
  input  logic                              i_set_jumbo       ;    // opt  | enable of jumbo-frames support             | strob    | def: 0    | info: see also 'pE_RX_JUMBO'
                                                                   //      |                                            |          |           |
  input  logic    [               1 :0 ]    i_set_speed       ;    // req  | speed of interface                         | strob    |           | info: see 'comment.txt' (6)
                                                                   //      |                                            |          |           |
                                                                   //      |   0 : 1000 mbit/sec                        |          |           |
                                                                   //      |   1 :  100 mbit/sec                        |          |           |
                                                                   //      |   2 :   10 mbit/sec                        |          |           |
                                                                   //      |                                            |          |           |
  //-------------------------------------// lan interface ---------//------|--------------------------------------------|----------|-----------|------------------------------------------------------------------------------//
                                                                   //      |                                            |          |           |
  //                                        lan mii                //------|--------------------------------------------|----------|-----------|------------------------------------------------------------------------------//
                                                                   //      |                                            |          |           |
  input  logic    [               3 :0 ]    i_mii_rxd         ;    // req  | receive data                               | pdata    |           | info: see note  (4) ; info: for latching used rising edge of clock
  input  logic                              i_mii_rx_dv       ;    // req  | receive data valid                         | strob    |           | info: see note  (4)
  input  logic                              i_mii_clk         ;    // req  | receive clock                              | clock    |           | info: see table (1) ; see 'comment.txt' (6)
                                                                   //      |                                            |          |           |
  //                                        lan rmii               //------|--------------------------------------------|----------|-----------|------------------------------------------------------------------------------//
                                                                   //      |                                            |          |           |
  input  logic    [               1 :0 ]    i_rmii_rxd        ;    // req  | receive data                               | pdata    |           | info: see note  (4) ; info: for latching used rising edge of clock
  input  logic                              i_rmii_crs_dv     ;    // req  | receive data valid & carrier sense         | strob    |           | info: see note  (4)
  input  logic                              i_rmii_clk        ;    // req  | receive clock                              | clock    |           | info: see table (1) ; see 'comment.txt' (6)
  output logic                              o_rmii_clk_s      ;    // test | receive clock / speed adapter              |          |           |
                                                                   //      |                                            |          |           |
                                                                   //      |  - for 100 mbit/s : 50 mhz                 | clock    |           |
                                                                   //      |  - for 10  mbit/s :  5 mhz (not 50 mhz)    | strob    |           |
                                                                   //      |                                            |          |           |
  //                                        lan gmii               //------|--------------------------------------------|----------|-----------|------------------------------------------------------------------------------//
                                                                   //      |                                            |          |           |
  input  logic    [               7 :0 ]    i_gmii_rxd        ;    // req  | receive data                               | pdata    |           | info: see note  (4) ; info: for latching used rising edge of clock
  input  logic                              i_gmii_rx_dv      ;    // req  | receive data valid                         | strob    |           | info: see note  (4)
  input  logic                              i_gmii_clk        ;    // req  | receive clock                              | clock    |           | info: see table (1) ; see 'comment.txt' (6)
                                                                   //      |                                            |          |           |
  //-------------------------------------// custom mode -----------//------|--------------------------------------------|----------|-----------|------------------------------------------------------------------------------//
                                                                   //      |                                            |          |           |
  input  logic                              i_cst_clk         ;    // opt  | receive clock                              | clock    |           |
  input  logic                              i_cst_cen         ;    // opt  | receive clock enable                       | pulse    | def: 1    | width: cst_clk
  input  logic                              i_cst_ced         ;    // opt  | receive clock enable for data              | pulse    | def: 1    | width: cst_cen
                                                                   //      |                                            |          |           |
  input  logic    [               7 :0 ]    i_cst_rxd         ;    // opt  | receive data                               | pdata    |           | info: for latching used rising edge of clock
  input  logic                              i_cst_rx_dv       ;    // opt  | receive data valid                         | strob    |           | 
                                                                   //      |                                            |          |           |
  //-------------------------------------// rx packet -------------//------|--------------------------------------------|----------|-----------|------------------------------------------------------------------------------//
                                                                   //      |                                            |          |           |
  //                                        clock                  //------|--------------------------------------------|----------|-----------|------------------------------------------------------------------------------//
                                                                   //      |                                            |          |           |
  output logic                              o_rx_clk          ;    // req  | clock                                      | clock    |           | info: see table (1)
  output logic                              o_rx_cen          ;    // req  | clock enable                               | pulse    |           | info: see table (1) ; width: rx_clk
  output logic                              o_rx_ced          ;    // req  | clock enable for data                      | pulse    |           | info: see table (1) ; width: rx_cen
                                                                   //      |                                            |          |           |
  //                                        strob                  //------|--------------------------------------------|----------|-----------|------------------------------------------------------------------------------//
                                                                   //      |                                            |          |           |
  output logic    [               2 :0 ]    o_rx_str_body     ;    // req  | frame body              / strob            | strob    |           | info : see purpose (6)
  output logic    [ lpW_RX_LEN   -1 :0 ]    o_rx_cnt_body     ;    // opt  | frame body              / strob counter    | count up |           |
  output logic    [               2 :0 ]    o_rx_str_mac      ;    // opt  | mac header              / strob            | strob    |           |
  output logic    [               3 :0 ]    o_rx_cnt_mac      ;    // opt  | mac header              / strob counter    | count up |           |
  output logic    [               2 :0 ]    o_rx_str_mda      ;    // opt  | mac destination address / strob            | strob    |           |
  output logic    [               2 :0 ]    o_rx_str_msa      ;    // opt  | mac source address      / strob            | strob    |           |
  output logic    [               2 :0 ]    o_rx_str_mlt      ;    // opt  | mac length or type      / strob            | strob    |           |
  output logic    [               2 :0 ]    o_rx_str_ip       ;    // opt  | ip header               / strob            | strob    |           |
  output logic    [ lpW_RX_IP_C  -1 :0 ]    o_rx_cnt_ip       ;    // opt  | ip header               / strob counter    | count up |           |
  output logic    [               2 :0 ]    o_rx_str_pay      ;    // opt  | payload & pad           / strob            | strob    |           | info: with crc strob (!) ; see table (2)
  output logic    [ lpW_RX_LEN   -1 :0 ]    o_rx_cnt_pay      ;    // opt  | payload & pad           / strob counter    | count up |           |
  output logic    [               2 :0 ]    o_rx_str_gap      ;    // opt  | inter-frame gap         / strob            | strob    |           |
  output logic    [ pW_RX_GAP    -1 :0 ]    o_rx_cnt_gap      ;    // opt  | inter-frame gap         / strob counter    | count up |           |
                                                                   //      |                                            |          |           |
  //                                        data                   //------|--------------------------------------------|----------|-----------|------------------------------------------------------------------------------//
                                                                   //      |                                            |          |           |
  output logic                              o_rx_dat_load     ;    // req  | frame body              / load (byte)      | pulse    |           | width: rx_cen
  output logic    [               7 :0 ]    o_rx_dat_body     ;    // req  | frame body              / data (byte)      | pdata    |           |
                                                                   //      |                                            |          |           |
  output logic                              o_rx_rdy_mac      ;    // opt  | mac header              / ready            | pulse    |           | width: rx_cen ; info: used for latch 'rx_dat_mac'
  output logic    [              47 :0 ]    o_rx_dat_mda      ;    // opt  | mac destination address / data             | pdata    |           |
  output logic    [              47 :0 ]    o_rx_dat_msa      ;    // opt  | mac source address      / data             | pdata    |           |
  output logic    [              15 :0 ]    o_rx_dat_mlt      ;    // opt  | mac length or type      / data             | pdata    |           |
                                                                   //      |                                            |          |           |
  output logic                              o_rx_rdy_ip       ;    // opt  | ip header               / ready            | pulse    |           | width: rx_cen ; info: used for latch 'rx_dat_ip'
  output logic    [ lpW_RX_IP    -1 :0 ]    o_rx_dat_ip       ;    // opt  | ip header               / data             | pdata    |           |
                                                                   //      |                                            |          |           |
  //                                        length                 //------|--------------------------------------------|----------|-----------|------------------------------------------------------------------------------//
                                                                   //      |                                            |          |           |
  output logic                              o_rx_rdy_body     ;    // req  | frame body              / ready            | pulse    |           | width: rx_cen ; info: used for latch 'rx_len_body'
  output logic    [ lpW_RX_LEN   -1 :0 ]    o_rx_len_body     ;    // req  | frame body              / length (byte)    | pdata    |           |
  output logic                              o_rx_rdy_gap      ;    // opt  | inter-frame gap         / ready            | pulse    |           | width: rx_cen ; info: used for latch 'rx_len_gap'
  output logic    [ pW_RX_GAP    -1 :0 ]    o_rx_len_gap      ;    // opt  | inter-frame gap         / length (byte)    | pdata    |           |
                                                                   //      |                                            |          |           |
  //                                        fcs crc                //------|--------------------------------------------|----------|-----------|------------------------------------------------------------------------------//
                                                                   //      |                                            |          |           |
  output logic                              o_rx_crc_rdy      ;    // req  | fcs crc-32              / ready            | pulse    |           | width: rx_cen ; info: used for latch 'rx_crc_err'
  output logic                              o_rx_crc_err      ;    // req  | fcs crc-32              / error            | strob    |           |
                                                                   //      |                                            |          |           |
  //-------------------------------------// rx counters -----------//------|--------------------------------------------|----------|-----------|------------------------------------------------------------------------------//
                                                                   //      |                                            |          |           |
  //                                        setting                //------|--------------------------------------------|----------|-----------|------------------------------------------------------------------------------//
                                                                   //      |                                            |          |           |
  input  logic    [               1 :0 ]    i_rx_set_p_spd    ;    // opt  | speed selection                            | pdata    | def: 0    | info: see 'comment.txt' (6,10)
                                                                   //      |                                            |          |           |
                                                                   //      |   0 : payload                              |          |           | info: used 'o_rx_str_body' - 17 bytes
                                                                   //      |   1 : payload + mac                        |          |           | info: used 'o_rx_str_body' -  4 bytes
                                                                   //      |   2 : payload + mac + fcs crc              |          |           | info: used 'o_rx_str_body'
  input  logic    [ pN_RX_CNT       :1 ]                           //      |                                            |          |           |
                  [ lpW_RX_LEN   -1 :0 ]    i_rx_set_p_top    ;    // opt  | top bound of body length (not frame)       | array    |           | lim: constant (!) ; info: see purpose (10), note (2) and 'comment.txt' (7)
  input  logic    [ pN_RX_CNT       :1 ]                           //      |                                            |          |           |
                  [ lpW_RX_LEN   -1 :0 ]    i_rx_set_p_low    ;    // opt  | low bound of body length (not frame)       | array    |           | lim: constant (!) ; info: see purpose (10), note (2) and 'comment.txt' (7)
                                                                   //      |                                            |          |           |
  //                                        detector               //------|--------------------------------------------|----------|-----------|------------------------------------------------------------------------------//
                                                                   //      |                                            |          |           |
  output logic                              o_rx_det_p_rdy    ;    // opt  | packet ready                               | pulse    |           | width: rx_cen
  output logic                              o_rx_det_p_err    ;    // opt  | packet error                               | strob    |           |
  output logic                              o_rx_det_p_cut    ;    // opt  | packet cut off (for oversize frame)        | pulse    |           | width: rx_cen
  input  logic                              i_rx_det_p_off    ;    // opt  | packet offcast (from buffer)               | pulse    | def: 0    | width: rx_cen ; lim: must appear before stop bit of frame ; see comment (8)
                                                                   //      |                                            |          |           |
  //                                        counters               //------|--------------------------------------------|----------|-----------|------------------------------------------------------------------------------//
                                                                   //      |                                            |          |           |
  input  logic                              i_rx_cnt_p_clr    ;    // opt  | current value / clear                      | strob    | def: 0    | info: sensitivity to rising edge ; see 'comment.txt' (6)
                                                                   //      |                                            |          |           |
  output logic                              o_rx_cnt_p_rdy    ;    // opt  | current value / ready                      | pulse    |           | width: rx_cen
  output logic    [ pN_RX_CNT       :0 ]                           //      |                                            |          |           |
                  [ pW_RX_CNT_P  -1 :0 ]    o_rx_cnt_p_all    ;    // opt  | current value / packet all                 | count up |           | info: see comment (9)
  output logic    [ pN_RX_CNT       :0 ]                           //      |                                            |          |           |
                  [ pW_RX_CNT_P  -1 :0 ]    o_rx_cnt_p_err    ;    // opt  | current value / packet error               | count up |           | info: see comment (9)
  output logic    [ pN_RX_CNT       :0 ]                           //      |                                            |          |           |
                  [ pW_RX_CNT_P  -1 :0 ]    o_rx_cnt_p_off    ;    // opt  | current value / packet offcast             | count up |           | info: see comment (9)
                                                                   //      |                                            |          |           |
  output logic    [ pW_RX_CNT_S  -1 :0 ]    o_rx_cnt_p_spd    ;    // opt  | current value / packet speed               | count up |           | info: byte counter ; see also 'i_rx_set_p_spd' and 'comment.txt' (9,10)
                                                                   //      |                                            |          |           |
  //                                        counters (rhythm)      //------|--------------------------------------------|----------|-----------|------------------------------------------------------------------------------//
                                                                   //      |                                            |          |           |
  input  logic                              i_rx_rcnt_p_clr   ;    // opt  | periodic value / clear                     | strob    | def: 0    | info: sensitivity to rising edge ; see 'comment.txt' (6)
                                                                   //      |                                            |          |           |
  input  logic                              i_rx_rcnt_p_upd   ;    // opt  | periodic value / update                    | strob    | def: 0    | info: sensitivity to rising edge ; see 'comment.txt' (6)
                                                                   //      |                                            |          |           |
  output logic                              o_rx_rcnt_p_rdy   ;    // opt  | periodic value / ready                     | pulse    |           | width: rx_cen
  output logic    [ pN_RX_CNT       :0 ]                           //      |                                            |          |           |
                  [ pW_RX_CNT_P  -1 :0 ]    o_rx_rcnt_p_all   ;    // opt  | periodic value / packet all                | pdata    |           | info: see comment (9)
  output logic    [ pN_RX_CNT       :0 ]                           //      |                                            |          |           |
                  [ pW_RX_CNT_P  -1 :0 ]    o_rx_rcnt_p_err   ;    // opt  | periodic value / packet error              | pdata    |           | info: see comment (9)
  output logic    [ pN_RX_CNT       :0 ]                           //      |                                            |          |           |
                  [ pW_RX_CNT_P  -1 :0 ]    o_rx_rcnt_p_off   ;    // opt  | periodic value / packet offcast            | pdata    |           | info: see comment (9)
                                                                   //      |                                            |          |           |
  output logic    [ pW_RX_CNT_S  -1 :0 ]    o_rx_rcnt_p_spd   ;    // opt  | periodic value / packet speed              | pdata    |           | info: byte counter ; see also 'i_rx_set_p_spd' and 'comment.txt' (9,10)
                                                                   //      |                                            |          |           |
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // logic
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  genvar                                    j                       ;

  //-------------------------------------// reset ------------------|

  var    logic                              ri_reset_n              ;

  //-------------------------------------// settings ---------------|

  var    logic                              ri_set_cut              ;
  var    logic    [               1 :0 ]    ri_set_speed            ;
  var    logic                              ri_set_jumbo            ;

  //-------------------------------------// custom pre-receiver ----| [ rx_cst / eth_rx_cst_v2 ]

  //                                        clear

  var    logic                              rx_cst__i_sclr_n        ;

  //                                        castom input

  var    logic                              rx_cst__i_cst_clk       ;
  var    logic                              rx_cst__i_cst_cen       ;
  var    logic                              rx_cst__i_cst_ced       ;
  var    logic    [               7 :0 ]    rx_cst__i_cst_rxd       ;
  var    logic                              rx_cst__i_cst_rx_dv     ;

  //                                        castom output

  var    logic                              rx_cst__o_cst_clk       ;
  var    logic                              rx_cst__o_cst_cen       ;
  var    logic                              rx_cst__o_cst_ced       ;
  var    logic    [               7 :0 ]    rx_cst__o_cst_rxd       ;
  var    logic                              rx_cst__o_cst_rx_dv     ;

  //-------------------------------------// mii pre-receiver -------| [ rx_mii / eth_rx_mii_v2 ]

  //                                        clear

  var    logic                              rx_mii__i_sclr_n        ;

  //                                        mii input

  var    logic    [               3 :0 ]    rx_mii__i_mii_rxd       ;
  var    logic                              rx_mii__i_mii_rx_dv     ;
  var    logic                              rx_mii__i_mii_clk       ;

  //                                        mii output

  var    logic                              rx_mii__o_lan_clk       ;
  var    logic                              rx_mii__o_lan_cen       ;
  var    logic                              rx_mii__o_lan_ced       ;
  var    logic    [               7 :0 ]    rx_mii__o_lan_rxd       ;
  var    logic                              rx_mii__o_lan_rx_dv     ;

  //-------------------------------------// rmii pre-receiver ------| [ rx_rmii / eth_rx_rmii_v2 ]

  //                                        clear

  var    logic                              rx_rmii__i_sclr_n       ;

  //                                        setting

  var    logic    [               1 :0 ]    rx_rmii__i_set_speed    ;

  //                                        rmii input

  var    logic    [               1 :0 ]    rx_rmii__i_rmii_rxd     ;
  var    logic                              rx_rmii__i_rmii_crs_dv  ;
  var    logic                              rx_rmii__i_rmii_clk     ;

  //                                        rmii recover

  var    logic    [               1 :0 ]    rx_rmii__o_rmii_rxd     ;
  var    logic                              rx_rmii__o_rmii_rx_dv   ;
  var    logic                              rx_rmii__o_rmii_crs     ;
  var    logic                              rx_rmii__o_rmii_clk     ;

  //                                        rmii output

  var    logic                              rx_rmii__o_lan_clk      ;
  var    logic                              rx_rmii__o_lan_cen      ;
  var    logic                              rx_rmii__o_lan_ced      ;
  var    logic    [               7 :0 ]    rx_rmii__o_lan_rxd      ;
  var    logic                              rx_rmii__o_lan_rx_dv    ;

  //-------------------------------------// gmii pre-receiver ------| [ rx_gmii / eth_rx_gmii_v2 ]

  //                                        clear

  var    logic                              rx_gmii__i_sclr_n       ;

  //                                        setting

  var    logic    [               1 :0 ]    rx_gmii__i_set_speed    ;

  //                                        gmii input

  var    logic    [               7 :0 ]    rx_gmii__i_gmii_rxd     ;
  var    logic                              rx_gmii__i_gmii_rx_dv   ;
  var    logic                              rx_gmii__i_gmii_clk     ;

  //                                        gmii output

  var    logic                              rx_gmii__o_lan_clk      ;
  var    logic                              rx_gmii__o_lan_cen      ;
  var    logic                              rx_gmii__o_lan_ced      ;
  var    logic    [               7 :0 ]    rx_gmii__o_lan_rxd      ;
  var    logic                              rx_gmii__o_lan_rx_dv    ;

  //-------------------------------------// mux of pre-receivers ---| [ rx_mux / eth_rx_mux_v2 ]

  //                                        clear

  var    logic                              rx_mux__i_sclr_n        ;

  //                                        castom mode

  var    logic                              rx_mux__i_cst_clk       ;
  var    logic                              rx_mux__i_cst_cen       ;
  var    logic                              rx_mux__i_cst_ced       ;
  var    logic    [               7 :0 ]    rx_mux__i_cst_rxd       ;
  var    logic                              rx_mux__i_cst_rx_dv     ;

  //                                        lan mii

  var    logic                              rx_mux__i_mii_clk       ;
  var    logic                              rx_mux__i_mii_cen       ;
  var    logic                              rx_mux__i_mii_ced       ;
  var    logic    [               7 :0 ]    rx_mux__i_mii_rxd       ;
  var    logic                              rx_mux__i_mii_rx_dv     ;

  //                                        lan rmii

  var    logic                              rx_mux__i_rmii_clk      ;
  var    logic                              rx_mux__i_rmii_cen      ;
  var    logic                              rx_mux__i_rmii_ced      ;
  var    logic    [               7 :0 ]    rx_mux__i_rmii_rxd      ;
  var    logic                              rx_mux__i_rmii_rx_dv    ;

  //                                        lan gmii

  var    logic                              rx_mux__i_gmii_clk      ;
  var    logic                              rx_mux__i_gmii_cen      ;
  var    logic                              rx_mux__i_gmii_ced      ;
  var    logic    [               7 :0 ]    rx_mux__i_gmii_rxd      ;
  var    logic                              rx_mux__i_gmii_rx_dv    ;

  //                                        lan mux

  var    logic                              rx_mux__o_mux_clk       ;
  var    logic                              rx_mux__o_mux_cen       ;
  var    logic                              rx_mux__o_mux_ced       ;
  var    logic    [               7 :0 ]    rx_mux__o_mux_rxd       ;
  var    logic                              rx_mux__o_mux_rx_dv     ;

  //-------------------------------------// frame head detector ----| [ rx_head / eth_rx_head_v2 ]

  //                                        clear

  var    logic                              rx_head__i_sclr_n       ;

  //                                        setting

  var    logic                              rx_head__i_set_det      ;

  //                                        lan clock
  
  var    logic                              rx_head__i_lan_clk      ;
  var    logic                              rx_head__i_lan_cen      ;
  var    logic                              rx_head__i_lan_ced      ;
  
  //                                        lan data
  
  var    logic    [               7 :0 ]    rx_head__i_lan_rxd      ;
  var    logic                              rx_head__i_lan_rx_dv    ;
  
  //                                        lan packet
  
  var    logic    [               7 :0 ]    rx_head__o_dat_body     ;
  var    logic                              rx_head__o_str_body     ;

  //-------------------------------------// strob generator --------| [ rx_str / eth_rx_str_v2 ]

  //                                        clear

  var    logic                              rx_str__i_sclr_n        ;

  //                                        setting

  var    logic                              rx_str__i_set_cut       ;

  //                                        in packet

  var    logic                              rx_str__i_clk           ;
  var    logic                              rx_str__i_cen           ;
  var    logic                              rx_str__i_ced           ;
  var    logic                              rx_str__i_str_body      ;
  var    logic    [               7 :0 ]    rx_str__i_dat_body      ;

  //                                        out packet

  var    logic                              rx_str__o_clk           ;
  var    logic                              rx_str__o_cen           ;
  var    logic                              rx_str__o_ced           ;
  var    logic    [               2 :0 ]    rx_str__o_str_body      ;
  var    logic    [ lpW_RX_LEN   -1 :0 ]    rx_str__o_cnt_body      ;
  var    logic    [               2 :0 ]    rx_str__o_str_mac       ;
  var    logic    [               3 :0 ]    rx_str__o_cnt_mac       ;
  var    logic    [               2 :0 ]    rx_str__o_str_mda       ;
  var    logic    [               2 :0 ]    rx_str__o_str_msa       ;
  var    logic    [               2 :0 ]    rx_str__o_str_mlt       ;
  var    logic    [               2 :0 ]    rx_str__o_str_ip        ;
  var    logic    [ lpW_RX_IP_C  -1 :0 ]    rx_str__o_cnt_ip        ;
  var    logic    [               2 :0 ]    rx_str__o_str_pay       ;
  var    logic    [ lpW_RX_LEN   -1 :0 ]    rx_str__o_cnt_pay       ;
  var    logic    [               2 :0 ]    rx_str__o_str_gap       ;
  var    logic    [ pW_RX_GAP    -1 :0 ]    rx_str__o_cnt_gap       ;
  var    logic                              rx_str__o_dat_load      ;
  var    logic    [               7 :0 ]    rx_str__o_dat_body      ;
  var    logic                              rx_str__o_rdy_body      ;
  var    logic    [ lpW_RX_LEN   -1 :0 ]    rx_str__o_len_body      ;
  var    logic                              rx_str__o_rdy_gap       ;
  var    logic    [ pW_RX_GAP    -1 :0 ]    rx_str__o_len_gap       ;

  //                                        detector

  var    logic                              rx_str__o_det_p_cut     ;

  //-------------------------------------// data extractor ---------| [ rx_dat / eth_rx_dat_v2 ]

  //                                        clear

  var    logic                              rx_dat__i_sclr_n        ;

  //                                        clock

  var    logic                              rx_dat__i_clk           ;
  var    logic                              rx_dat__i_cen           ;
  var    logic                              rx_dat__i_ced           ;

  //                                        strob

  var    logic    [               2 :0 ]    rx_dat__i_str_mda       ;
  var    logic    [               2 :0 ]    rx_dat__i_str_msa       ;
  var    logic    [               2 :0 ]    rx_dat__i_str_mlt       ;
  var    logic    [               2 :0 ]    rx_dat__i_str_ip        ;

  //                                        data

  var    logic                              rx_dat__i_dat_load      ;
  var    logic    [               7 :0 ]    rx_dat__i_dat_body      ;
  var    logic                              rx_dat__o_rdy_mac       ;
  var    logic    [              47 :0 ]    rx_dat__o_dat_mda       ;
  var    logic    [              47 :0 ]    rx_dat__o_dat_msa       ;
  var    logic    [              15 :0 ]    rx_dat__o_dat_mlt       ;
  var    logic                              rx_dat__o_rdy_ip        ;
  var    logic    [ lpW_RX_IP    -1 :0 ]    rx_dat__o_dat_ip        ;

  //-------------------------------------// packet counters --------| [ rx_cnt / eth_cnt_v2 ]

  //                                        clock

  var    logic                              rx_cnt__i_clk           ;
  var    logic                              rx_cnt__i_clk_en        ;
  var    logic                              rx_cnt__i_sclr_n        ;

  //                                        packet body

  var    logic                              rx_cnt__i_str_body      ;
  var    logic    [               7 :0 ]    rx_cnt__i_dat_body      ;
  var    logic                              rx_cnt__i_dat_load      ;
  var    logic    [ lpW_RX_LEN   -1 :0 ]    rx_cnt__i_len_body      ;

  //                                        setting
  var    logic    [ pN_RX_CNT       :1 ]                    
                  [ lpW_RX_LEN   -1 :0 ]    rx_cnt__i_set_p_top     ;
  var    logic    [ pN_RX_CNT       :1 ]                    
                  [ lpW_RX_LEN   -1 :0 ]    rx_cnt__i_set_p_low     ;
  var    logic    [               1 :0 ]    rx_cnt__i_set_p_spd     ;

  //                                        detector

  var    logic                              rx_cnt__o_det_p_rdy     ;
  var    logic                              rx_cnt__o_det_p_err     ;
  var    logic                              rx_cnt__i_det_p_off     ;

  //                                        counters

  var    logic                              rx_cnt__i_cnt_p_clr     ;

  var    logic                              rx_cnt__o_cnt_p_rdy     ;
  var    logic    [ pN_RX_CNT       :0 ]                    
                  [ pW_RX_CNT_P  -1 :0 ]    rx_cnt__o_cnt_p_all     ;
  var    logic    [ pN_RX_CNT       :0 ]                    
                  [ pW_RX_CNT_P  -1 :0 ]    rx_cnt__o_cnt_p_err     ;
  var    logic    [ pN_RX_CNT       :0 ]                    
                  [ pW_RX_CNT_P  -1 :0 ]    rx_cnt__o_cnt_p_off     ;
  var    logic    [ pN_RX_CNT       :0 ]                    
                  [ pW_RX_CNT_P  -1 :0 ]    rx_cnt__o_cnt_p_spd     ;

  //                                        counters (rhythm)

  var    logic                              rx_cnt__i_rcnt_p_clr    ;

  var    logic                              rx_cnt__i_rcnt_p_upd    ;

  var    logic                              rx_cnt__o_rcnt_p_rdy    ;
  var    logic    [ pN_RX_CNT       :0 ]                    
                  [ pW_RX_CNT_P  -1 :0 ]    rx_cnt__o_rcnt_p_all    ;
  var    logic    [ pN_RX_CNT       :0 ]                    
                  [ pW_RX_CNT_P  -1 :0 ]    rx_cnt__o_rcnt_p_err    ;
  var    logic    [ pN_RX_CNT       :0 ]                    
                  [ pW_RX_CNT_P  -1 :0 ]    rx_cnt__o_rcnt_p_off    ;
  var    logic    [ pN_RX_CNT       :0 ]                    
                  [ pW_RX_CNT_S  -1 :0 ]    rx_cnt__o_rcnt_p_spd    ;

  //-------------------------------------// output regs and latch --| [ rx_out / eth_rx_out_v2 ]

  //                                        clear

  var    logic                              rx_out__i_sclr_n        ;

  //                                        in packet

  var    logic                              rx_out__i_clk           ;
  var    logic                              rx_out__i_cen           ;
  var    logic                              rx_out__i_ced           ;
  var    logic    [               2 :0 ]    rx_out__i_str_body      ;
  var    logic    [ lpW_RX_LEN   -1 :0 ]    rx_out__i_cnt_body      ;
  var    logic    [               2 :0 ]    rx_out__i_str_mac       ;
  var    logic    [               3 :0 ]    rx_out__i_cnt_mac       ;
  var    logic    [               2 :0 ]    rx_out__i_str_mda       ;
  var    logic    [               2 :0 ]    rx_out__i_str_msa       ;
  var    logic    [               2 :0 ]    rx_out__i_str_mlt       ;
  var    logic    [               2 :0 ]    rx_out__i_str_ip        ;
  var    logic    [ lpW_RX_IP_C  -1 :0 ]    rx_out__i_cnt_ip        ;
  var    logic    [               2 :0 ]    rx_out__i_str_pay       ;
  var    logic    [ lpW_RX_LEN   -1 :0 ]    rx_out__i_cnt_pay       ;
  var    logic    [               2 :0 ]    rx_out__i_str_gap       ;
  var    logic    [ pW_RX_GAP    -1 :0 ]    rx_out__i_cnt_gap       ;
  var    logic                              rx_out__i_dat_load      ;
  var    logic    [               7 :0 ]    rx_out__i_dat_body      ;
  var    logic                              rx_out__i_rdy_mac       ;
  var    logic    [              47 :0 ]    rx_out__i_dat_mda       ;
  var    logic    [              47 :0 ]    rx_out__i_dat_msa       ;
  var    logic    [              15 :0 ]    rx_out__i_dat_mlt       ;
  var    logic                              rx_out__i_rdy_ip        ;
  var    logic    [ lpW_RX_IP    -1 :0 ]    rx_out__i_dat_ip        ;
  var    logic                              rx_out__i_rdy_body      ;
  var    logic    [ lpW_RX_LEN   -1 :0 ]    rx_out__i_len_body      ;
  var    logic                              rx_out__i_rdy_gap       ;
  var    logic    [ pW_RX_GAP    -1 :0 ]    rx_out__i_len_gap       ;

  //                                        in counters

  var    logic                              rx_out__i_det_p_rdy     ;
  var    logic                              rx_out__i_det_p_err     ;
  var    logic                              rx_out__i_det_p_cut     ;
  
  var    logic                              rx_out__i_cnt_p_rdy     ;
  var    logic    [ pN_RX_CNT       :0 ]
                  [ pW_RX_CNT_P  -1 :0 ]    rx_out__i_cnt_p_all     ;
  var    logic    [ pN_RX_CNT       :0 ]
                  [ pW_RX_CNT_P  -1 :0 ]    rx_out__i_cnt_p_err     ;
  var    logic    [ pN_RX_CNT       :0 ]
                  [ pW_RX_CNT_P  -1 :0 ]    rx_out__i_cnt_p_off     ;

  var    logic    [ pW_RX_CNT_S  -1 :0 ]    rx_out__i_cnt_p_spd     ;

  var    logic                              rx_out__i_rcnt_p_rdy    ;
  var    logic    [ pN_RX_CNT       :0 ]
                  [ pW_RX_CNT_P  -1 :0 ]    rx_out__i_rcnt_p_all    ;
  var    logic    [ pN_RX_CNT       :0 ]
                  [ pW_RX_CNT_P  -1 :0 ]    rx_out__i_rcnt_p_err    ;
  var    logic    [ pN_RX_CNT       :0 ]
                  [ pW_RX_CNT_P  -1 :0 ]    rx_out__i_rcnt_p_off    ;

  var    logic    [ pW_RX_CNT_S  -1 :0 ]    rx_out__i_rcnt_p_spd    ;

  //                                        out packet

  var    logic                              rx_out__o_clk           ;
  var    logic                              rx_out__o_cen           ;
  var    logic                              rx_out__o_ced           ;
  var    logic    [               2 :0 ]    rx_out__o_str_body      ;
  var    logic    [ lpW_RX_LEN   -1 :0 ]    rx_out__o_cnt_body      ;
  var    logic    [               2 :0 ]    rx_out__o_str_mac       ;
  var    logic    [               3 :0 ]    rx_out__o_cnt_mac       ;
  var    logic    [               2 :0 ]    rx_out__o_str_mda       ;
  var    logic    [               2 :0 ]    rx_out__o_str_msa       ;
  var    logic    [               2 :0 ]    rx_out__o_str_mlt       ;
  var    logic    [               2 :0 ]    rx_out__o_str_ip        ;
  var    logic    [ lpW_RX_IP_C  -1 :0 ]    rx_out__o_cnt_ip        ;
  var    logic    [               2 :0 ]    rx_out__o_str_pay       ;
  var    logic    [ lpW_RX_LEN   -1 :0 ]    rx_out__o_cnt_pay       ;
  var    logic    [               2 :0 ]    rx_out__o_str_gap       ;
  var    logic    [ pW_RX_GAP    -1 :0 ]    rx_out__o_cnt_gap       ;
  var    logic                              rx_out__o_dat_load      ;
  var    logic    [               7 :0 ]    rx_out__o_dat_body      ;
  var    logic                              rx_out__o_rdy_mac       ;
  var    logic    [              47 :0 ]    rx_out__o_dat_mda       ;
  var    logic    [              47 :0 ]    rx_out__o_dat_msa       ;
  var    logic    [              15 :0 ]    rx_out__o_dat_mlt       ;
  var    logic                              rx_out__o_rdy_ip        ;
  var    logic    [ lpW_RX_IP    -1 :0 ]    rx_out__o_dat_ip        ;
  var    logic                              rx_out__o_rdy_body      ;
  var    logic    [ lpW_RX_LEN   -1 :0 ]    rx_out__o_len_body      ;
  var    logic                              rx_out__o_rdy_gap       ;
  var    logic    [ pW_RX_GAP    -1 :0 ]    rx_out__o_len_gap       ;

  //                                        out counters

  var    logic                              rx_out__o_det_p_rdy     ;
  var    logic                              rx_out__o_det_p_err     ;
  var    logic                              rx_out__o_cnt_p_rdy     ;
  var    logic                              rx_out__o_det_p_cut     ;
  var    logic    [ pN_RX_CNT       :0 ]
                  [ pW_RX_CNT_P  -1 :0 ]    rx_out__o_cnt_p_all     ;
  var    logic    [ pN_RX_CNT       :0 ]
                  [ pW_RX_CNT_P  -1 :0 ]    rx_out__o_cnt_p_err     ;
  var    logic    [ pN_RX_CNT       :0 ]
                  [ pW_RX_CNT_P  -1 :0 ]    rx_out__o_cnt_p_off     ;

  var    logic    [ pW_RX_CNT_S  -1 :0 ]    rx_out__o_cnt_p_spd     ;

  var    logic                              rx_out__o_rcnt_p_rdy    ;
  var    logic    [ pN_RX_CNT       :0 ]
                  [ pW_RX_CNT_P  -1 :0 ]    rx_out__o_rcnt_p_all    ;
  var    logic    [ pN_RX_CNT       :0 ]
                  [ pW_RX_CNT_P  -1 :0 ]    rx_out__o_rcnt_p_err    ;
  var    logic    [ pN_RX_CNT       :0 ]
                  [ pW_RX_CNT_P  -1 :0 ]    rx_out__o_rcnt_p_off    ;

  var    logic    [ pW_RX_CNT_S  -1 :0 ]    rx_out__o_rcnt_p_spd    ;

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  //==========================================================================================================================================================================================================================//
  // body of module
  //==========================================================================================================================================================================================================================//

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // out logic : output (after out reg)
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  // rx packet

  assign  o_rx_clk         = rx_out__o_clk        ;
  assign  o_rx_cen         = rx_out__o_cen        ;
  assign  o_rx_ced         = rx_out__o_ced        ;

  assign  o_rx_str_body    = rx_out__o_str_body   ;
  assign  o_rx_cnt_body    = rx_out__o_cnt_body   ;
  assign  o_rx_str_mac     = rx_out__o_str_mac    ;
  assign  o_rx_cnt_mac     = rx_out__o_cnt_mac    ;
  assign  o_rx_str_mda     = rx_out__o_str_mda    ;
  assign  o_rx_str_msa     = rx_out__o_str_msa    ;
  assign  o_rx_str_mlt     = rx_out__o_str_mlt    ;
  assign  o_rx_str_ip      = rx_out__o_str_ip     ;
  assign  o_rx_cnt_ip      = rx_out__o_cnt_ip     ;
  assign  o_rx_str_pay     = rx_out__o_str_pay    ;
  assign  o_rx_cnt_pay     = rx_out__o_cnt_pay    ;
  assign  o_rx_str_gap     = rx_out__o_str_gap    ;
  assign  o_rx_cnt_gap     = rx_out__o_cnt_gap    ;

  assign  o_rx_dat_load    = rx_out__o_dat_load   ;
  assign  o_rx_dat_body    = rx_out__o_dat_body   ;
  assign  o_rx_rdy_mac     = rx_out__o_rdy_mac    ;
  assign  o_rx_dat_mda     = rx_out__o_dat_mda    ;
  assign  o_rx_dat_msa     = rx_out__o_dat_msa    ;
  assign  o_rx_dat_mlt     = rx_out__o_dat_mlt    ;
  assign  o_rx_rdy_ip      = rx_out__o_rdy_ip     ;
  assign  o_rx_dat_ip      = rx_out__o_dat_ip     ;

  assign  o_rx_rdy_body    = rx_out__o_rdy_body   ;
  assign  o_rx_len_body    = rx_out__o_len_body   ;
  assign  o_rx_rdy_gap     = rx_out__o_rdy_gap    ;
  assign  o_rx_len_gap     = rx_out__o_len_gap    ;

  assign  o_rx_crc_rdy     = rx_out__o_det_p_rdy  ;
  assign  o_rx_crc_err     = rx_out__o_det_p_err  ;

  // rx counters

  assign  o_rx_det_p_rdy   = rx_out__o_det_p_rdy  ;
  assign  o_rx_det_p_err   = rx_out__o_det_p_err  ;
  assign  o_rx_det_p_cut   = rx_out__o_det_p_cut  ;

  assign  o_rx_cnt_p_rdy   = rx_out__o_cnt_p_rdy  ;
  assign  o_rx_cnt_p_all   = rx_out__o_cnt_p_all  ;
  assign  o_rx_cnt_p_err   = rx_out__o_cnt_p_err  ;
  assign  o_rx_cnt_p_off   = rx_out__o_cnt_p_off  ;
  assign  o_rx_cnt_p_spd   = rx_out__o_cnt_p_spd  ;

  assign  o_rx_rcnt_p_rdy  = rx_out__o_rcnt_p_rdy ;
  assign  o_rx_rcnt_p_all  = rx_out__o_rcnt_p_all ;
  assign  o_rx_rcnt_p_err  = rx_out__o_rcnt_p_err ;
  assign  o_rx_rcnt_p_off  = rx_out__o_rcnt_p_off ;
  assign  o_rx_rcnt_p_spd  = rx_out__o_rcnt_p_spd ;

  // lan interface

  assign  o_rmii_clk_s     = rx_rmii__o_rmii_clk  ; // for testing

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // sync clear
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  // delay line (!) reason : metastability and high fan-out (!)

  z_line #(.pV_DELAY (3), .pW_DATA (1))  z__i_reset_n  (.i_clk(o_rx_clk), .i_clk_en(1'b1), .i_sclr(1'b0), .i_data(i_reset_n), .o_data(ri_reset_n) );
  //                  ^                                                                                           ^                   ^
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // settings
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  // delay line (!) reason : metastability (!)

  z_line #(.pV_DELAY (3), .pW_DATA (1))  z__i_set_cut    (.i_clk(o_rx_clk), .i_clk_en(1'b1), .i_sclr(1'b0), .i_data(i_set_cut  ), .o_data(ri_set_cut  ) );
  z_line #(.pV_DELAY (3), .pW_DATA (1))  z__i_set_jumbo  (.i_clk(o_rx_clk), .i_clk_en(1'b1), .i_sclr(1'b0), .i_data(i_set_jumbo), .o_data(ri_set_jumbo) );
  z_line #(.pV_DELAY (3), .pW_DATA (2))  z__i_set_speed  (.i_clk(o_rx_clk), .i_clk_en(1'b1), .i_sclr(1'b0), .i_data(i_set_speed), .o_data(ri_set_speed) );
  //                  ^                                                                                             ^                     ^
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // ethernet preliminary receiver for custom mode  [ rx_cst / eth_rx_cst_v2 ]
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  // clear

  assign  rx_cst__i_sclr_n     = ri_reset_n  ;

  // custom input

  assign  rx_cst__i_cst_clk    = i_cst_clk   ;
  assign  rx_cst__i_cst_cen    = i_cst_cen   ;
  assign  rx_cst__i_cst_ced    = i_cst_ced   ;
  assign  rx_cst__i_cst_rxd    = i_cst_rxd   ;
  assign  rx_cst__i_cst_rx_dv  = i_cst_rx_dv ;
  
  //-----------------------------------------------------------------------------|
                                                 //                              |
  eth_rx_cst_v2      rx_cst                      //                              |
  (                                              //                              |
  // clear --------------------------------------//------------------------------|
                                                 //           |                  |
     .i_sclr_n       ( rx_cst__i_sclr_n    ),    //  in       |       opt        |
                                                 //           |                  |
  // custom input -------------------------------//-----------|------------------|
                                                 //           |                  |
     .i_cst_clk      ( rx_cst__i_cst_clk   ),    //      out  |  req             |
     .i_cst_cen      ( rx_cst__i_cst_cen   ),    //      out  |       opt        |
     .i_cst_ced      ( rx_cst__i_cst_ced   ),    //      out  |       opt        |
     .i_cst_rxd      ( rx_cst__i_cst_rxd   ),    //      out  |  req             |
     .i_cst_rx_dv    ( rx_cst__i_cst_rx_dv ),    //      out  |  req             |
                                                 //           |                  |
  // custom output ------------------------------//-----------|------------------|
                                                 //           |                  |
     .o_cst_clk      ( rx_cst__o_cst_clk   ),    //      out  |  req             |
     .o_cst_cen      ( rx_cst__o_cst_cen   ),    //      out  |       opt        |
     .o_cst_ced      ( rx_cst__o_cst_ced   ),    //      out  |       opt        |
     .o_cst_rxd      ( rx_cst__o_cst_rxd   ),    //      out  |  req             |
     .o_cst_rx_dv    ( rx_cst__o_cst_rx_dv )     //      out  |  req             |
                                                 //           |                  |
  );//---------------------------------------------------------------------------|

  // custom mode

  defparam  rx_cst.pT_CST_CEN  = pT_CST_CEN ;
  defparam  rx_cst.pT_CST_CED  = pT_CST_CED ;

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // ethernet preliminary receiver for mii-interface  [ rx_mii / eth_rx_mii_v2 ]
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  // clear

  assign  rx_mii__i_sclr_n     = ri_reset_n   ;

  // setting

  assign  rx_mii__i_set_speed  = ri_set_speed ;

  // mii input

  assign  rx_mii__i_mii_clk    = i_mii_clk    ;
  assign  rx_mii__i_mii_rxd    = i_mii_rxd    ;
  assign  rx_mii__i_mii_rx_dv  = i_mii_rx_dv  ;
  
  //-----------------------------------------------------------------------------|
                                                 //                              |
  eth_rx_mii_v2      rx_mii                      //                              |
  (                                              //                              |
  // clear --------------------------------------//------------------------------|
                                                 //           |                  |
     .i_sclr_n       ( rx_mii__i_sclr_n    ),    //  in       |       opt        |
                                                 //           |                  |
  // mii input ----------------------------------//-----------|------------------|
                                                 //           |                  |
     .i_mii_clk      ( rx_mii__i_mii_clk   ),    //  in       |  req             |
     .i_mii_rxd      ( rx_mii__i_mii_rxd   ),    //  in       |  req             |
     .i_mii_rx_dv    ( rx_mii__i_mii_rx_dv ),    //  in       |  req             |
                                                 //           |                  |
  // mii output ---------------------------------//-----------|------------------|
                                                 //           |                  |
     .o_lan_clk      ( rx_mii__o_lan_clk   ),    //      out  |  req             |
     .o_lan_cen      ( rx_mii__o_lan_cen   ),    //      out  |  req             |
     .o_lan_ced      ( rx_mii__o_lan_ced   ),    //      out  |  req             |
     .o_lan_rxd      ( rx_mii__o_lan_rxd   ),    //      out  |  req             |
     .o_lan_rx_dv    ( rx_mii__o_lan_rx_dv )     //      out  |  req             |
                                                 //           |                  |
  );//---------------------------------------------------------------------------|

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // ethernet preliminary receiver for rmii-interface  [ rx_rmii / eth_rx_rmii_v2 ]
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  // clear

  assign  rx_rmii__i_sclr_n       = ri_reset_n    ;

  // setting

  assign  rx_rmii__i_set_speed    = ri_set_speed  ;

  // rmii input

  assign  rx_rmii__i_rmii_rxd     = i_rmii_rxd    ;
  assign  rx_rmii__i_rmii_crs_dv  = i_rmii_crs_dv ;
  assign  rx_rmii__i_rmii_clk     = i_rmii_clk    ;
  
  //----------------------------------------------------------------------------------|
                                                      //                              |
  eth_rx_rmii_v2       rx_rmii                        //                              |
  (                                                   //                              |
  // clear -------------------------------------------//------------------------------|
                                                      //           |                  |
     .i_sclr_n         ( rx_rmii__i_sclr_n      ),    //  in       |       opt        |
                                                      //           |                  |
  // setting -----------------------------------------//-----------|------------------|
                                                      //           |                  |
     .i_set_speed      ( rx_rmii__i_set_speed   ),    //  in       |  req             |
                                                      //           |                  |
  // rmii input --------------------------------------//-----------|------------------|
                                                      //           |                  |
     .i_rmii_rxd       ( rx_rmii__i_rmii_rxd    ),    //  in       |  req             |
     .i_rmii_crs_dv    ( rx_rmii__i_rmii_crs_dv ),    //  in       |  req             |
     .i_rmii_clk       ( rx_rmii__i_rmii_clk    ),    //  in       |  req             |
                                                      //           |                  |
  // rmii recover ------------------------------------//-----------|------------------|
                                                      //           |                  |
     .o_rmii_rxd       ( rx_rmii__o_rmii_rxd    ),    //      out  |       opt        |
     .o_rmii_rx_dv     ( rx_rmii__o_rmii_rx_dv  ),    //      out  |       opt        |
     .o_rmii_crs       ( rx_rmii__o_rmii_crs    ),    //      out  |       opt        |
     .o_rmii_clk       ( rx_rmii__o_rmii_clk    ),    //      out  |            test  |
                                                      //           |                  |
  // rmii output -------------------------------------//-----------|------------------|
                                                      //           |                  |
     .o_lan_clk        ( rx_rmii__o_lan_clk     ),    //      out  |  req             |
     .o_lan_cen        ( rx_rmii__o_lan_cen     ),    //      out  |  req             |
     .o_lan_ced        ( rx_rmii__o_lan_ced     ),    //      out  |  req             |
     .o_lan_rxd        ( rx_rmii__o_lan_rxd     ),    //      out  |  req             |
     .o_lan_rx_dv      ( rx_rmii__o_lan_rx_dv   )     //      out  |  req             |
                                                      //           |                  |
  );//--------------------------------------------------------------------------------|

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // ethernet preliminary receiver for gmii-interface  [ rx_gmii / eth_rx_gmii_v2 ]
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  // clear

  assign  rx_gmii__i_sclr_n      = ri_reset_n   ;

  // setting

  assign  rx_gmii__i_set_speed   = ri_set_speed ;

  // gmii input

  assign  rx_gmii__i_gmii_rxd    = i_gmii_rxd   ;
  assign  rx_gmii__i_gmii_rx_dv  = i_gmii_rx_dv ;
  assign  rx_gmii__i_gmii_clk    = i_gmii_clk   ;
  
  //---------------------------------------------------------------------------------|
                                                     //                              |
  eth_rx_gmii_v2      rx_gmii                        //                              |
  (                                                  //                              |
  // clear ------------------------------------------//------------------------------|
                                                     //           |                  |
     .i_sclr_n        ( rx_gmii__i_sclr_n      ),    //  in       |       opt        |
                                                     //           |                  |
  // setting ----------------------------------------//-----------|------------------|
                                                     //           |                  |
     .i_set_speed     ( rx_gmii__i_set_speed   ),    //  in       |  req             |
                                                     //           |                  |
  // gmii input -------------------------------------//-----------|------------------|
                                                     //           |                  |
     .i_gmii_rxd      ( rx_gmii__i_gmii_rxd    ),    //  in       |  req             |
     .i_gmii_rx_dv    ( rx_gmii__i_gmii_rx_dv  ),    //  in       |  req             |
     .i_gmii_clk      ( rx_gmii__i_gmii_clk    ),    //  in       |  req             |
                                                     //           |                  |
  // gmii output ------------------------------------//-----------|------------------|
                                                     //           |                  |
     .o_lan_clk       ( rx_gmii__o_lan_clk     ),    //      out  |  req             |
     .o_lan_cen       ( rx_gmii__o_lan_cen     ),    //      out  |  req             |
     .o_lan_ced       ( rx_gmii__o_lan_ced     ),    //      out  |  req             |
     .o_lan_rxd       ( rx_gmii__o_lan_rxd     ),    //      out  |  req             |
     .o_lan_rx_dv     ( rx_gmii__o_lan_rx_dv   )     //      out  |  req             |
                                                     //           |                  |
  );//-------------------------------------------------------------------------------|

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // multiplexer of ethernet preliminary receivers  [ rx_mux / eth_rx_mux_v2 ]
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  // clear

  assign  rx_mux__i_sclr_n      = ri_reset_n           ;

  // custom mode

  assign  rx_mux__i_cst_clk     = rx_cst__o_cst_clk    ;
  assign  rx_mux__i_cst_cen     = rx_cst__o_cst_cen    ;
  assign  rx_mux__i_cst_ced     = rx_cst__o_cst_ced    ;
  assign  rx_mux__i_cst_rxd     = rx_cst__o_cst_rxd    ;
  assign  rx_mux__i_cst_rx_dv   = rx_cst__o_cst_rx_dv  ;

  // lan mii

  assign  rx_mux__i_mii_clk     = rx_mii__o_lan_clk    ;
  assign  rx_mux__i_mii_cen     = rx_mii__o_lan_cen    ;
  assign  rx_mux__i_mii_ced     = rx_mii__o_lan_ced    ;
  assign  rx_mux__i_mii_rxd     = rx_mii__o_lan_rxd    ;
  assign  rx_mux__i_mii_rx_dv   = rx_mii__o_lan_rx_dv  ;

  // lan rmii

  assign  rx_mux__i_rmii_clk    = rx_rmii__o_lan_clk   ;
  assign  rx_mux__i_rmii_cen    = rx_rmii__o_lan_cen   ;
  assign  rx_mux__i_rmii_ced    = rx_rmii__o_lan_ced   ;
  assign  rx_mux__i_rmii_rxd    = rx_rmii__o_lan_rxd   ;
  assign  rx_mux__i_rmii_rx_dv  = rx_rmii__o_lan_rx_dv ;

  // lan gmii

  assign  rx_mux__i_gmii_clk    = rx_gmii__o_lan_clk   ;
  assign  rx_mux__i_gmii_cen    = rx_gmii__o_lan_cen   ;
  assign  rx_mux__i_gmii_ced    = rx_gmii__o_lan_ced   ;
  assign  rx_mux__i_gmii_rxd    = rx_gmii__o_lan_rxd   ;
  assign  rx_mux__i_gmii_rx_dv  = rx_gmii__o_lan_rx_dv ;

  //-------------------------------------------------------------------------------|
                                                   //                              |
  eth_rx_mux_v2       rx_mux                       //                              |
  (                                                //                              |
  // clear ----------------------------------------//------------------------------|
                                                   //           |                  |
     .i_sclr_n        ( rx_mux__i_sclr_n     ),    //  in       |       opt        |
                                                   //           |                  |
  // custom mode ----------------------------------//-----------|------------------|
                                                   //           |                  |
     .i_cst_clk       ( rx_mux__i_cst_clk    ),    //  in       |       opt        |
     .i_cst_cen       ( rx_mux__i_cst_cen    ),    //  in       |       opt        |
     .i_cst_ced       ( rx_mux__i_cst_ced    ),    //  in       |       opt        |
     .i_cst_rxd       ( rx_mux__i_cst_rxd    ),    //  in       |       opt        |
     .i_cst_rx_dv     ( rx_mux__i_cst_rx_dv  ),    //  in       |       opt        |
                                                   //           |                  |
  // lan mii --------------------------------------//-----------|------------------|
                                                   //           |                  |
     .i_mii_clk       ( rx_mux__i_mii_clk    ),    //  in       |  req             |
     .i_mii_cen       ( rx_mux__i_mii_cen    ),    //  in       |  req             |
     .i_mii_ced       ( rx_mux__i_mii_ced    ),    //  in       |  req             |
     .i_mii_rxd       ( rx_mux__i_mii_rxd    ),    //  in       |  req             |
     .i_mii_rx_dv     ( rx_mux__i_mii_rx_dv  ),    //  in       |  req             |
                                                   //           |                  |
  // lan rmii -------------------------------------//-----------|------------------|
                                                   //           |                  |
     .i_rmii_clk      ( rx_mux__i_rmii_clk   ),    //  in       |  req             |
     .i_rmii_cen      ( rx_mux__i_rmii_cen   ),    //  in       |  req             |
     .i_rmii_ced      ( rx_mux__i_rmii_ced   ),    //  in       |  req             |
     .i_rmii_rxd      ( rx_mux__i_rmii_rxd   ),    //  in       |  req             |
     .i_rmii_rx_dv    ( rx_mux__i_rmii_rx_dv ),    //  in       |  req             |
                                                   //           |                  |
  // lan gmii -------------------------------------//-----------|------------------|
                                                   //           |                  |
     .i_gmii_clk      ( rx_mux__i_gmii_clk   ),    //  in       |  req             |
     .i_gmii_cen      ( rx_mux__i_gmii_cen   ),    //  in       |  req             |
     .i_gmii_ced      ( rx_mux__i_gmii_ced   ),    //  in       |  req             |
     .i_gmii_rxd      ( rx_mux__i_gmii_rxd   ),    //  in       |  req             |
     .i_gmii_rx_dv    ( rx_mux__i_gmii_rx_dv ),    //  in       |  req             |
                                                   //           |                  |
  // lan mux --------------------------------------//-----------|------------------|
                                                   //           |                  |
     .o_mux_clk       ( rx_mux__o_mux_clk    ),    //      out  |  req             |
     .o_mux_cen       ( rx_mux__o_mux_cen    ),    //      out  |  req             |
     .o_mux_ced       ( rx_mux__o_mux_ced    ),    //      out  |  req             |
     .o_mux_rxd       ( rx_mux__o_mux_rxd    ),    //      out  |  req             |
     .o_mux_rx_dv     ( rx_mux__o_mux_rx_dv  )     //      out  |  req             |
                                                   //           |                  |
  );//-----------------------------------------------------------------------------|

  // interface

  defparam  rx_mux.pS_IO_TYPE  = pS_IO_TYPE ;

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // frame header detector (preamble and start-frame delimiter)  [ rx_head / eth_rx_head_v2 ]
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  // clear

  assign  rx_head__i_sclr_n     = ri_reset_n ;

  // setting

  assign  rx_head__i_set_det    = (ri_set_speed == 2) ? 0 : // search sfd only       / for 10 mbit/s
                                                        1 ; // search preamble + sfd / for 100 mbit/s
  // lan clock

  assign  rx_head__i_lan_clk    = rx_mux__o_mux_clk   ;
  assign  rx_head__i_lan_cen    = rx_mux__o_mux_cen   ;
  assign  rx_head__i_lan_ced    = rx_mux__o_mux_ced   ;

  // lan data

  assign  rx_head__i_lan_rxd    = rx_mux__o_mux_rxd   ;
  assign  rx_head__i_lan_rx_dv  = rx_mux__o_mux_rx_dv ;

  //------------------------------------------------------------------------------|
                                                  //                              |
  eth_rx_head_v2     rx_head                      //                              |
  (                                               //                              |
  // clear ---------------------------------------//------------------------------|
                                                  //           |                  |
     .i_sclr_n       ( rx_head__i_sclr_n    ),    //  in       |       opt        |
                                                  //           |                  |
  // setting -------------------------------------//-----------|------------------|
                                                  //           |                  |
     .i_set_det      ( rx_head__i_set_det   ),    //  in       |  req             |
                                                  //           |                  |
  // lan clock -----------------------------------//-----------|------------------|
                                                  //           |                  |
     .i_lan_clk      ( rx_head__i_lan_clk   ),    //  in       |  req             |
     .i_lan_cen      ( rx_head__i_lan_cen   ),    //  in       |  req             |
     .i_lan_ced      ( rx_head__i_lan_ced   ),    //  in       |  req             |
                                                  //           |                  |
  // lan data ------------------------------------//-----------|------------------|
                                                  //           |                  |
     .i_lan_rxd      ( rx_head__i_lan_rxd   ),    //  in       |  req             |
     .i_lan_rx_dv    ( rx_head__i_lan_rx_dv ),    //  in       |  req             |
                                                  //           |                  |
  // lan packet ----------------------------------//-----------|------------------|
                                                  //           |                  |
     .o_dat_body     ( rx_head__o_dat_body  ),    //      out  |  req             |
     .o_str_body     ( rx_head__o_str_body  )     //      out  |  req             |
                                                  //           |                  |
  );//----------------------------------------------------------------------------|

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // strobe generator and length calculator for ethernet frames  [ rx_str / eth_rx_str_v2 ]
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  // clear

  assign  rx_str__i_sclr_n     = ri_reset_n ;

  // setting

  assign  rx_str__i_set_cut    = ri_set_cut   ;
  assign  rx_str__i_set_jumbo  = ri_set_jumbo ;

  // in packet

  assign  rx_str__i_clk        = (pE_RX_HEAD == 1) ? rx_head__i_lan_clk  : rx_mux__o_mux_clk   ;
  assign  rx_str__i_cen        = (pE_RX_HEAD == 1) ? rx_head__i_lan_cen  : rx_mux__o_mux_cen   ;
  assign  rx_str__i_ced        = (pE_RX_HEAD == 1) ? rx_head__i_lan_ced  : rx_mux__o_mux_ced   ;

  assign  rx_str__i_str_body   = (pE_RX_HEAD == 1) ? rx_head__o_str_body : rx_mux__o_mux_rx_dv ;
  assign  rx_str__i_dat_body   = (pE_RX_HEAD == 1) ? rx_head__o_dat_body : rx_mux__o_mux_rxd   ;

  //----------------------------------------------------------------------------|
                                                //                              |
  eth_rx_str_v2     rx_str                      //                              |
  (                                             //                              |
  // clear -------------------------------------//------------------------------|
                                                //           |                  |
     .i_sclr_n      ( rx_str__i_sclr_n    ),    //  in       |       opt        |
                                                //           |                  |
  // setting -----------------------------------//-----------|------------------|
                                                //           |                  |
     .i_set_cut     ( rx_str__i_set_cut   ),    //  in       |       opt        |
     .i_set_jumbo   ( rx_str__i_set_jumbo ),    //  in       |       opt        |
                                                //           |                  |
  // in packet ---------------------------------//-----------|------------------|
                                                //           |                  |
  // clock          clock                       //-----------|------------------|
                                                //           |                  |
     .i_clk         ( rx_str__i_clk       ),    //  in       |  req             |
     .i_cen         ( rx_str__i_cen       ),    //  in       |  req             |
     .i_ced         ( rx_str__i_ced       ),    //  in       |  req             |
                                                //           |                  |
  // data           data                        //-----------|------------------|
                                                //           |                  |
     .i_str_body    ( rx_str__i_str_body  ),    //  in       |  req             |
     .i_dat_body    ( rx_str__i_dat_body  ),    //  in       |  req             |
                                                //           |                  |
  // out packet --------------------------------//-----------|------------------|
                                                //           |                  |
  // clock          clock                       //-----------|------------------|
                                                //           |                  |
     .o_clk         ( rx_str__o_clk       ),    //      out  |  req             |
     .o_cen         ( rx_str__o_cen       ),    //      out  |  req             |
     .o_ced         ( rx_str__o_ced       ),    //      out  |  req             |
                                                //           |                  |
  // strob          strob                       //-----------|------------------|
                                                //           |                  |
     .o_str_body    ( rx_str__o_str_body  ),    //      out  |  req             |
     .o_cnt_body    ( rx_str__o_cnt_body  ),    //      out  |       opt        |
     .o_str_mac     ( rx_str__o_str_mac   ),    //      out  |       opt        |
     .o_cnt_mac     ( rx_str__o_cnt_mac   ),    //      out  |       opt        |
     .o_str_mda     ( rx_str__o_str_mda   ),    //      out  |       opt        |
     .o_str_msa     ( rx_str__o_str_msa   ),    //      out  |       opt        |
     .o_str_mlt     ( rx_str__o_str_mlt   ),    //      out  |       opt        |
     .o_str_ip      ( rx_str__o_str_ip    ),    //      out  |       opt        |
     .o_cnt_ip      ( rx_str__o_cnt_ip    ),    //      out  |       opt        |
     .o_str_pay     ( rx_str__o_str_pay   ),    //      out  |       opt        |
     .o_cnt_pay     ( rx_str__o_cnt_pay   ),    //      out  |       opt        |
     .o_str_gap     ( rx_str__o_str_gap   ),    //      out  |       opt        |
     .o_cnt_gap     ( rx_str__o_cnt_gap   ),    //      out  |       opt        |
                                                //           |                  |
  // data           data                        //-----------|------------------|
                                                //           |                  |
     .o_dat_load    ( rx_str__o_dat_load  ),    //      out  |  req             |
     .o_dat_body    ( rx_str__o_dat_body  ),    //      out  |  req             |
                                                //           |                  |
  // length         length                      //-----------|------------------|
                                                //           |                  |
     .o_rdy_body    ( rx_str__o_rdy_body  ),    //      out  |  req             |
     .o_len_body    ( rx_str__o_len_body  ),    //      out  |  req             |
     .o_rdy_gap     ( rx_str__o_rdy_gap   ),    //      out  |       opt        |
     .o_len_gap     ( rx_str__o_len_gap   ),    //      out  |       opt        |
                                                //           |                  |
  // detector ----------------------------------//-----------|------------------|
                                                //           |                  |
     .o_det_p_cut   ( rx_str__o_det_p_cut )     //      out  |       opt        |
                                                //           |                  |
  );//--------------------------------------------------------------------------|

  // packet manipulation

  defparam  rx_str.pE_RX_CUT    = pE_RX_CUT   ;
  defparam  rx_str.pE_RX_JUMBO  = pE_RX_JUMBO ;

  // packet structure

  defparam  rx_str.pW_RX_LEN    = pW_RX_LEN   ;
  defparam  rx_str.pV_RX_LEN    = pV_RX_LEN   ;
  defparam  rx_str.pW_RX_LEN_J  = pW_RX_LEN_J ;
  defparam  rx_str.pV_RX_LEN_J  = pV_RX_LEN_J ;
  defparam  rx_str.pW_RX_GAP    = pW_RX_GAP   ;
  defparam  rx_str.pE_RX_MAC    = pE_RX_MAC   ;
  defparam  rx_str.pE_RX_IP     = pE_RX_IP    ;
  defparam  rx_str.pN_RX_IPL    = pN_RX_IPL   ;
  defparam  rx_str.pN_RX_IPH    = pN_RX_IPH   ;

  // in/out registers

  defparam  rx_str.pN_REG_I     = 1; // def: 1
  defparam  rx_str.pN_REG_O     = 1; // def: 1

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // data extractor for ip and mac headers  [ rx_dat / eth_rx_dat_v2 ]
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  // clear

  assign  rx_dat__i_sclr_n    = ri_reset_n         ;

  // clock

  assign  rx_dat__i_clk       = rx_str__o_clk      ;
  assign  rx_dat__i_cen       = rx_str__o_cen      ;
  assign  rx_dat__i_ced       = rx_str__o_ced      ;

  // strob

  assign  rx_dat__i_str_mda   = rx_str__o_str_mda  ;
  assign  rx_dat__i_str_msa   = rx_str__o_str_msa  ;
  assign  rx_dat__i_str_mlt   = rx_str__o_str_mlt  ;
  assign  rx_dat__i_str_ip    = rx_str__o_str_ip   ;

  // data

  assign  rx_dat__i_dat_load  = rx_str__o_dat_load ;
  assign  rx_dat__i_dat_body  = rx_str__o_dat_body ;

  //----------------------------------------------------------------------------|
                                                //                              |
  eth_rx_dat_v2     rx_dat                      //                              |
  (                                             //                              |
  // clear -------------------------------------//------------------------------|
                                                //           |                  |
     .i_sclr_n      ( rx_dat__i_sclr_n    ),    //  in       |       opt        |
                                                //           |                  |
  // clock -------------------------------------//-----------|------------------|
                                                //           |                  |
     .i_clk         ( rx_dat__i_clk       ),    //  in       |  req             |
     .i_cen         ( rx_dat__i_cen       ),    //  in       |  req             |
     .i_ced         ( rx_dat__i_ced       ),    //  in       |  req             |
                                                //           |                  |
  // strob -------------------------------------//-----------|------------------|
                                                //           |                  |
     .i_str_mda     ( rx_dat__i_str_mda   ),    //  in       |  req             |
     .i_str_msa     ( rx_dat__i_str_msa   ),    //  in       |  req             |
     .i_str_mlt     ( rx_dat__i_str_mlt   ),    //  in       |  req             |
     .i_str_ip      ( rx_dat__i_str_ip    ),    //  in       |  req             |
                                                //           |                  |
  // data --------------------------------------//-----------|------------------|
                                                //           |                  |
     .i_dat_load    ( rx_dat__i_dat_load  ),    //  in       |  req             |
     .i_dat_body    ( rx_dat__i_dat_body  ),    //  in       |  req             |
                                                //           |                  |
     .o_rdy_mac     ( rx_dat__o_rdy_mac   ),    //      out  |  req             |
     .o_dat_mda     ( rx_dat__o_dat_mda   ),    //      out  |  req             |
     .o_dat_msa     ( rx_dat__o_dat_msa   ),    //      out  |  req             |
     .o_dat_mlt     ( rx_dat__o_dat_mlt   ),    //      out  |  req             |
                                                //           |                  |
     .o_rdy_ip      ( rx_dat__o_rdy_ip    ),    //      out  |  req             |
     .o_dat_ip      ( rx_dat__o_dat_ip    )     //      out  |  req             |
                                                //           |                  |
  );//--------------------------------------------------------------------------|

  // packet structure

  defparam  rx_dat.pE_RX_MAC  = pE_RX_MAC ;
  defparam  rx_dat.pE_RX_IP   = pE_RX_IP  ;
  defparam  rx_dat.pN_RX_IPL  = pN_RX_IPL ;
  defparam  rx_dat.pN_RX_IPH  = pN_RX_IPH ;

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // counters of ethernet frames (packets)  [ rx_cnt / eth_cnt_v2 ]
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  // clock

  assign  rx_cnt__i_clk         = rx_str__o_clk      ;
  assign  rx_cnt__i_clk_en      = rx_str__o_cen      ;
  assign  rx_cnt__i_sclr_n      = i_reset_n          ; // not 'ri' / used internal 3 reg

  // frame body

  assign  rx_cnt__i_str_body    = rx_str__o_str_body ;
  assign  rx_cnt__i_dat_body    = rx_str__o_dat_body ;
  assign  rx_cnt__i_dat_load    = rx_str__o_dat_load ;
  assign  rx_cnt__i_len_body    = rx_str__o_len_body ;

  // setting

  assign  rx_cnt__i_set_p_top   = i_rx_set_p_top     ;
  assign  rx_cnt__i_set_p_low   = i_rx_set_p_low     ;
  assign  rx_cnt__i_set_p_spd   = i_rx_set_p_spd     ; // used internal 3 reg

  // detector

  assign  rx_cnt__i_det_p_off   = i_rx_det_p_off     ; // used internal 1 reg

  // counters

  assign  rx_cnt__i_cnt_p_clr   = i_rx_cnt_p_clr     ; // used internal 3 reg
  assign  rx_cnt__i_rcnt_p_clr  = i_rx_rcnt_p_clr    ; // used internal 3 reg
  assign  rx_cnt__i_rcnt_p_upd  = i_rx_rcnt_p_upd    ; // used internal 3 reg

  //-------------------------------------------------------------------------------|
                                                   //                              |
  eth_cnt_v2          rx_cnt                       //                              |
  (                                                //                              |
  // clock ----------------------------------------//------------------------------|
                                                   //           |                  |
     .i_clk           ( rx_cnt__i_clk        ),    //  in       |  req             |
     .i_clk_en        ( rx_cnt__i_clk_en     ),    //  in       |       opt        |
     .i_sclr_n        ( rx_cnt__i_sclr_n     ),    //  in       |       opt        |
                                                   //           |                  |
  // frame body -----------------------------------//-----------|------------------|
                                                   //           |                  |
     .i_str_body      ( rx_cnt__i_str_body   ),    //  in       |  req             |
     .i_dat_body      ( rx_cnt__i_dat_body   ),    //  in       |  req             |
     .i_dat_load      ( rx_cnt__i_dat_load   ),    //  in       |  req             |
     .i_len_body      ( rx_cnt__i_len_body   ),    //  in       |       opt        |
                                                   //           |                  |
  // setting --------------------------------------//-----------|------------------|
                                                   //           |                  |
     .i_set_p_top     ( rx_cnt__i_set_p_top  ),    //  in       |       opt        |
     .i_set_p_low     ( rx_cnt__i_set_p_low  ),    //  in       |       opt        |
     .i_set_p_spd     ( rx_cnt__i_set_p_spd  ),    //  in       |       opt        |
                                                   //           |                  |
  // detector -------------------------------------//-----------|------------------|
                                                   //           |                  |
     .o_det_p_rdy     ( rx_cnt__o_det_p_rdy  ),    //      out  |  req             |
     .o_det_p_err     ( rx_cnt__o_det_p_err  ),    //      out  |  req             |
     .i_det_p_off     ( rx_cnt__i_det_p_off  ),    //  in       |       opt        |
                                                   //           |                  |
  // counters -------------------------------------//-----------|------------------|
                                                   //           |                  |
     .i_cnt_p_clr     ( rx_cnt__i_cnt_p_clr  ),    //  in       |       opt        |
     .o_cnt_p_rdy     ( rx_cnt__o_cnt_p_rdy  ),    //      out  |  req             |
     .o_cnt_p_all     ( rx_cnt__o_cnt_p_all  ),    //      out  |  req             |
     .o_cnt_p_err     ( rx_cnt__o_cnt_p_err  ),    //      out  |  req             |
     .o_cnt_p_off     ( rx_cnt__o_cnt_p_off  ),    //      out  |       opt        |
     .o_cnt_p_spd     ( rx_cnt__o_cnt_p_spd  ),    //      out  |       opt        |
                                                   //           |                  |
  // counters (rhythm) ----------------------------//-----------|------------------|
                                                   //           |                  |
     .i_rcnt_p_clr    ( rx_cnt__i_rcnt_p_clr ),    //  in       |       opt        |
     .i_rcnt_p_upd    ( rx_cnt__i_rcnt_p_upd ),    //  in       |       opt        |
     .o_rcnt_p_rdy    ( rx_cnt__o_rcnt_p_rdy ),    //      out  |       opt        |
     .o_rcnt_p_all    ( rx_cnt__o_rcnt_p_all ),    //      out  |       opt        |
     .o_rcnt_p_err    ( rx_cnt__o_rcnt_p_err ),    //      out  |       opt        |
     .o_rcnt_p_off    ( rx_cnt__o_rcnt_p_off ),    //      out  |       opt        |
     .o_rcnt_p_spd    ( rx_cnt__o_rcnt_p_spd )     //      out  |       opt        |
                                                   //           |                  |
  );//-----------------------------------------------------------------------------|

  // frame body

  defparam  rx_cnt.pW_LEN     = lpW_RX_LEN  ;

  // packet counters

  defparam  rx_cnt.pN_CNT     = pN_RX_CNT   ;
  defparam  rx_cnt.pW_CNT_P   = pW_RX_CNT_P ;
  defparam  rx_cnt.pW_CNT_S   = pW_RX_CNT_S ;

  // crc comparator

  defparam  rx_cnt.pV_CRC_DLY = pV_CRC_DLY  ;

  // signal tap (for tetsing)

  defparam  rx_cnt.pE_GEN_NPR = 0; // for testing only

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // output registers and latching data (mac and ip headers)  [ rx_out / eth_rx_out_v2 ]
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  // clear

  assign  rx_out__i_sclr_n      = ri_reset_n              ;

  // in packet

  assign  rx_out__i_clk         = rx_str__o_clk           ;
  assign  rx_out__i_cen         = rx_str__o_cen           ;
  assign  rx_out__i_ced         = rx_str__o_ced           ;

  assign  rx_out__i_str_body    = rx_str__o_str_body      ;
  assign  rx_out__i_cnt_body    = rx_str__o_cnt_body      ;
  assign  rx_out__i_str_mac     = rx_str__o_str_mac       ;
  assign  rx_out__i_cnt_mac     = rx_str__o_cnt_mac       ;
  assign  rx_out__i_str_mda     = rx_str__o_str_mda       ;
  assign  rx_out__i_str_msa     = rx_str__o_str_msa       ;
  assign  rx_out__i_str_mlt     = rx_str__o_str_mlt       ;
  assign  rx_out__i_str_ip      = rx_str__o_str_ip        ;
  assign  rx_out__i_cnt_ip      = rx_str__o_cnt_ip        ;
  assign  rx_out__i_str_pay     = rx_str__o_str_pay       ;
  assign  rx_out__i_cnt_pay     = rx_str__o_cnt_pay       ;
  assign  rx_out__i_str_gap     = rx_str__o_str_gap       ;
  assign  rx_out__i_cnt_gap     = rx_str__o_cnt_gap       ;

  assign  rx_out__i_dat_load    = rx_dat__i_dat_load      ;
  assign  rx_out__i_dat_body    = rx_dat__i_dat_body      ;
  assign  rx_out__i_rdy_mac     = rx_dat__o_rdy_mac       ;
  assign  rx_out__i_dat_mda     = rx_dat__o_dat_mda       ;
  assign  rx_out__i_dat_msa     = rx_dat__o_dat_msa       ;
  assign  rx_out__i_dat_mlt     = rx_dat__o_dat_mlt       ;
  assign  rx_out__i_rdy_ip      = rx_dat__o_rdy_ip        ;
  assign  rx_out__i_dat_ip      = rx_dat__o_dat_ip        ;

  assign  rx_out__i_rdy_body    = rx_str__o_rdy_body      ;
  assign  rx_out__i_len_body    = rx_str__o_len_body      ;
  assign  rx_out__i_rdy_gap     = rx_str__o_rdy_gap       ;
  assign  rx_out__i_len_gap     = rx_str__o_len_gap       ;

  // in counters

  assign  rx_out__i_det_p_rdy   = rx_cnt__o_det_p_rdy     ;
  assign  rx_out__i_det_p_err   = rx_cnt__o_det_p_err     ;
  assign  rx_out__i_det_p_cut   = rx_str__o_det_p_cut     ;

  assign  rx_out__i_cnt_p_rdy   = rx_cnt__o_cnt_p_rdy     ;
  assign  rx_out__i_cnt_p_all   = rx_cnt__o_cnt_p_all     ;
  assign  rx_out__i_cnt_p_err   = rx_cnt__o_cnt_p_err     ;
  assign  rx_out__i_cnt_p_off   = rx_cnt__o_cnt_p_off     ;
  assign  rx_out__i_cnt_p_spd   = rx_cnt__o_cnt_p_spd [0] ; // for all frame size only

  assign  rx_out__i_rcnt_p_rdy  = rx_cnt__o_rcnt_p_rdy    ;
  assign  rx_out__i_rcnt_p_all  = rx_cnt__o_rcnt_p_all    ;
  assign  rx_out__i_rcnt_p_err  = rx_cnt__o_rcnt_p_err    ;
  assign  rx_out__i_rcnt_p_off  = rx_cnt__o_rcnt_p_off    ;
  assign  rx_out__i_rcnt_p_spd  = rx_cnt__o_rcnt_p_spd    ;

  //-------------------------------------------------------------------------------|
                                                   //                              |
  eth_rx_out_v2       rx_out                       //                              |
  (                                                //                              |
  // clear ----------------------------------------//------------------------------|
                                                   //           |                  |
     .i_sclr_n        ( rx_out__i_sclr_n     ),    //  in       |       opt        |
                                                   //           |                  |
  // in packet ------------------------------------//-----------|------------------|
                                                   //           |                  |
     .i_clk           ( rx_out__i_clk        ),    //  in       |  req             |
     .i_cen           ( rx_out__i_cen        ),    //  in       |  req             |
     .i_ced           ( rx_out__i_ced        ),    //  in       |  req             |
                                                   //           |                  |
     .i_str_body      ( rx_out__i_str_body   ),    //  in       |  req             |
     .i_cnt_body      ( rx_out__i_cnt_body   ),    //  in       |       opt        |
     .i_str_mac       ( rx_out__i_str_mac    ),    //  in       |       opt        |
     .i_cnt_mac       ( rx_out__i_cnt_mac    ),    //  in       |       opt        |
     .i_str_mda       ( rx_out__i_str_mda    ),    //  in       |       opt        |
     .i_str_msa       ( rx_out__i_str_msa    ),    //  in       |       opt        |
     .i_str_mlt       ( rx_out__i_str_mlt    ),    //  in       |       opt        |
     .i_str_ip        ( rx_out__i_str_ip     ),    //  in       |       opt        |
     .i_cnt_ip        ( rx_out__i_cnt_ip     ),    //  in       |       opt        |
     .i_str_pay       ( rx_out__i_str_pay    ),    //  in       |       opt        |
     .i_cnt_pay       ( rx_out__i_cnt_pay    ),    //  in       |       opt        |
     .i_str_gap       ( rx_out__i_str_gap    ),    //  in       |       opt        |
     .i_cnt_gap       ( rx_out__i_cnt_gap    ),    //  in       |       opt        |
                                                   //           |                  |
     .i_dat_load      ( rx_out__i_dat_load   ),    //  in       |  req             |
     .i_dat_body      ( rx_out__i_dat_body   ),    //  in       |  req             |
     .i_rdy_mac       ( rx_out__i_rdy_mac    ),    //  in       |       opt        |
     .i_dat_mda       ( rx_out__i_dat_mda    ),    //  in       |       opt        |
     .i_dat_msa       ( rx_out__i_dat_msa    ),    //  in       |       opt        |
     .i_dat_mlt       ( rx_out__i_dat_mlt    ),    //  in       |       opt        |
     .i_rdy_ip        ( rx_out__i_rdy_ip     ),    //  in       |       opt        |
     .i_dat_ip        ( rx_out__i_dat_ip     ),    //  in       |       opt        |
                                                   //           |                  |
     .i_rdy_body      ( rx_out__i_rdy_body   ),    //  in       |  req             |
     .i_len_body      ( rx_out__i_len_body   ),    //  in       |  req             |
     .i_rdy_gap       ( rx_out__i_rdy_gap    ),    //  in       |       opt        |
     .i_len_gap       ( rx_out__i_len_gap    ),    //  in       |       opt        |
                                                   //           |                  |
  // in counters ----------------------------------//-----------|------------------|
                                                   //           |                  |
     .i_det_p_rdy     ( rx_out__i_det_p_rdy  ),    //  in       |  req             |
     .i_det_p_err     ( rx_out__i_det_p_err  ),    //  in       |  req             |
     .i_det_p_cut     ( rx_out__i_det_p_cut  ),    //  in       |       opt        |
                                                   //           |                  |
     .i_cnt_p_rdy     ( rx_out__i_cnt_p_rdy  ),    //  in       |       opt        |
     .i_cnt_p_all     ( rx_out__i_cnt_p_all  ),    //  in       |       opt        |
     .i_cnt_p_err     ( rx_out__i_cnt_p_err  ),    //  in       |       opt        |
     .i_cnt_p_off     ( rx_out__i_cnt_p_off  ),    //  in       |       opt        |
     .i_cnt_p_spd     ( rx_out__i_cnt_p_spd  ),    //  in       |       opt        |
                                                   //           |                  |
     .i_rcnt_p_rdy    ( rx_out__i_rcnt_p_rdy ),    //  in       |       opt        |
     .i_rcnt_p_all    ( rx_out__i_rcnt_p_all ),    //  in       |       opt        |
     .i_rcnt_p_err    ( rx_out__i_rcnt_p_err ),    //  in       |       opt        |
     .i_rcnt_p_off    ( rx_out__i_rcnt_p_off ),    //  in       |       opt        |
     .i_rcnt_p_spd    ( rx_out__i_rcnt_p_spd ),    //  in       |       opt        |
                                                   //           |                  |
  // out packet -----------------------------------//-----------|------------------|
                                                   //           |                  |
     .o_clk           ( rx_out__o_clk        ),    //      out  |  req             |
     .o_cen           ( rx_out__o_cen        ),    //      out  |  req             |
     .o_ced           ( rx_out__o_ced        ),    //      out  |  req             |
                                                   //           |                  |
     .o_str_body      ( rx_out__o_str_body   ),    //      out  |  req             |
     .o_cnt_body      ( rx_out__o_cnt_body   ),    //      out  |       opt        |
     .o_str_mac       ( rx_out__o_str_mac    ),    //      out  |       opt        |
     .o_cnt_mac       ( rx_out__o_cnt_mac    ),    //      out  |       opt        |
     .o_str_mda       ( rx_out__o_str_mda    ),    //      out  |       opt        |
     .o_str_msa       ( rx_out__o_str_msa    ),    //      out  |       opt        |
     .o_str_mlt       ( rx_out__o_str_mlt    ),    //      out  |       opt        |
     .o_str_ip        ( rx_out__o_str_ip     ),    //      out  |       opt        |
     .o_cnt_ip        ( rx_out__o_cnt_ip     ),    //      out  |       opt        |
     .o_str_pay       ( rx_out__o_str_pay    ),    //      out  |       opt        |
     .o_cnt_pay       ( rx_out__o_cnt_pay    ),    //      out  |       opt        |
     .o_str_gap       ( rx_out__o_str_gap    ),    //      out  |       opt        |
     .o_cnt_gap       ( rx_out__o_cnt_gap    ),    //      out  |       opt        |
                                                   //           |                  |
     .o_dat_load      ( rx_out__o_dat_load   ),    //      out  |  req             |
     .o_dat_body      ( rx_out__o_dat_body   ),    //      out  |  req             |
     .o_rdy_mac       ( rx_out__o_rdy_mac    ),    //      out  |       opt        |
     .o_dat_mda       ( rx_out__o_dat_mda    ),    //      out  |       opt        |
     .o_dat_msa       ( rx_out__o_dat_msa    ),    //      out  |       opt        |
     .o_dat_mlt       ( rx_out__o_dat_mlt    ),    //      out  |       opt        |
     .o_rdy_ip        ( rx_out__o_rdy_ip     ),    //      out  |       opt        |
     .o_dat_ip        ( rx_out__o_dat_ip     ),    //      out  |       opt        |
                                                   //           |                  |
     .o_rdy_body      ( rx_out__o_rdy_body   ),    //      out  |  req             |
     .o_len_body      ( rx_out__o_len_body   ),    //      out  |  req             |
     .o_rdy_gap       ( rx_out__o_rdy_gap    ),    //      out  |       opt        |
     .o_len_gap       ( rx_out__o_len_gap    ),    //      out  |       opt        |
                                                   //           |                  |
  // out counters ---------------------------------//-----------|------------------|
                                                   //           |                  |
     .o_det_p_rdy     ( rx_out__o_det_p_rdy  ),    //      out  |  req             |
     .o_det_p_err     ( rx_out__o_det_p_err  ),    //      out  |  req             |
     .o_det_p_cut     ( rx_out__o_det_p_cut  ),    //      out  |       opt        |
                                                   //           |                  |
     .o_cnt_p_rdy     ( rx_out__o_cnt_p_rdy  ),    //      out  |       opt        |
     .o_cnt_p_all     ( rx_out__o_cnt_p_all  ),    //      out  |       opt        |
     .o_cnt_p_err     ( rx_out__o_cnt_p_err  ),    //      out  |       opt        |
     .o_cnt_p_off     ( rx_out__o_cnt_p_off  ),    //      out  |       opt        |
     .o_cnt_p_spd     ( rx_out__o_cnt_p_spd  ),    //      out  |       opt        |
                                                   //           |                  |
     .o_rcnt_p_rdy    ( rx_out__o_rcnt_p_rdy ),    //      out  |       opt        |
     .o_rcnt_p_all    ( rx_out__o_rcnt_p_all ),    //      out  |       opt        |
     .o_rcnt_p_err    ( rx_out__o_rcnt_p_err ),    //      out  |       opt        |
     .o_rcnt_p_off    ( rx_out__o_rcnt_p_off ),    //      out  |       opt        |
     .o_rcnt_p_spd    ( rx_out__o_rcnt_p_spd )     //      out  |       opt        |
                                                   //           |                  |
  );//-----------------------------------------------------------------------------|

  // packet manipulation

  defparam  rx_out.pE_RX_JUMBO  = pE_RX_JUMBO ;

  // packet structure

  defparam  rx_out.pW_RX_LEN    = pW_RX_LEN   ;
  defparam  rx_out.pV_RX_LEN    = pV_RX_LEN   ;
  defparam  rx_out.pW_RX_LEN_J  = pW_RX_LEN_J ;
  defparam  rx_out.pV_RX_LEN_J  = pV_RX_LEN_J ;
  defparam  rx_out.pW_RX_GAP    = pW_RX_GAP   ;
  defparam  rx_out.pE_RX_MAC    = pE_RX_MAC   ;
  defparam  rx_out.pE_RX_IP     = pE_RX_IP    ;
  defparam  rx_out.pN_RX_IPL    = pN_RX_IPL   ;
  defparam  rx_out.pN_RX_IPH    = pN_RX_IPH   ;

  // packet counters

  defparam  rx_out.pN_RX_CNT    = pN_RX_CNT   ;
  defparam  rx_out.pW_RX_CNT_P  = pW_RX_CNT_P ;
  defparam  rx_out.pW_RX_CNT_S  = pW_RX_CNT_S ;

  // out register

  defparam  rx_out.pE_REG_OUT   = pE_REG_OUT  ;
  defparam  rx_out.pE_REG_CNT   = pE_REG_CNT  ;

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  //==========================================================================================================================================================================================================================//
  // testing (for signal tap)
  //==========================================================================================================================================================================================================================//

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // value of parameters
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  //----------------------------------// interface ----------------------|

  logic    [                   2 :0 ]    stp_pS_IO_TYPE =

                                        (pS_IO_TYPE == "custom") ? 1 :
                                        (pS_IO_TYPE == "gmii"  ) ? 2 :
                                        (pS_IO_TYPE == "rmii"  ) ? 3 :
                                        (pS_IO_TYPE == "mii"   ) ? 4 : 0 ;

  //----------------------------------// custom mode --------------------|

  logic    [ cbit(1,pT_CST_CEN ) :0 ]    stp_pT_CST_CEN   = pT_CST_CEN   ;
  logic    [ cbit(1,pT_CST_CED ) :0 ]    stp_pT_CST_CED   = pT_CST_CED   ;

  //----------------------------------// packet manipulation ------------|

  logic                                  stp_pE_RX_CUT    = pE_RX_CUT    ;
  logic                                  stp_pE_RX_JUMBO  = pE_RX_JUMBO  ;

  //----------------------------------// packet structure ---------------|

  //                                     frame header

  logic                                  stp_pE_RX_HEAD   = pE_RX_HEAD   ;

  //                                     frame body

  logic    [ cbit(1,pW_RX_LEN  ) :0 ]    stp_pW_RX_LEN    = pW_RX_LEN    ;
  logic    [ cbit(1,pV_RX_LEN  ) :0 ]    stp_pV_RX_LEN    = pV_RX_LEN    ;
  logic    [ cbit(1,pW_RX_LEN_J) :0 ]    stp_pW_RX_LEN_J  = pW_RX_LEN_J  ;
  logic    [ cbit(1,pV_RX_LEN_J) :0 ]    stp_pV_RX_LEN_J  = pV_RX_LEN_J  ;
  logic    [ cbit(1,pW_RX_GAP  ) :0 ]    stp_pW_RX_GAP    = pW_RX_GAP    ;

  //                                     mac header

  logic                                  stp_pE_RX_MAC    = pE_RX_MAC    ;

  //                                     ip header

  logic                                  stp_pE_RX_IP     = pE_RX_IP     ;
  logic    [ cbit(1,pN_RX_IPL  ) :0 ]    stp_pN_RX_IPL    = pN_RX_IPL    ;
  logic    [ cbit(1,pN_RX_IPH  ) :0 ]    stp_pN_RX_IPH    = pN_RX_IPH    ;

  //----------------------------------// packet counters ----------------|

  logic    [ cbit(1,pN_RX_CNT  ) :0 ]    stp_pN_RX_CNT    = pN_RX_CNT    ;
  logic    [ cbit(1,pW_RX_CNT_P) :0 ]    stp_pW_RX_CNT_P  = pW_RX_CNT_P  ;
  logic    [ cbit(1,pW_RX_CNT_S) :0 ]    stp_pW_RX_CNT_S  = pW_RX_CNT_S  ;

  //----------------------------------// local (for logic) --------------|

  //                                     crc comparator

  logic    [ cbit(1,pV_CRC_DLY ) :0 ]    stp_pV_CRC_DLY   = pV_CRC_DLY   ;

  //                                     out registers

  logic                                  stp_pE_REG_OUT   = pE_REG_OUT   ;
  logic                                  stp_pE_REG_CNT   = pE_REG_CNT   ;

  //----------------------------------// local (for i/o) ----------------|

  //                                     packet structure

  logic    [ cbit(1,lpW_RX_LEN ) :0 ]    stp_lpW_RX_LEN   = lpW_RX_LEN   ;
  logic    [ cbit(1,lpN_RX_IPL ) :0 ]    stp_lpN_RX_IPL   = lpN_RX_IPL   ;
  logic    [ cbit(1,lpN_RX_IPH ) :0 ]    stp_lpN_RX_IPH   = lpN_RX_IPH   ;
  logic    [ cbit(1,lpN_RX_IP  ) :0 ]    stp_lpN_RX_IP    = lpN_RX_IP    ;
  logic    [ cbit(1,lpW_RX_IP  ) :0 ]    stp_lpW_RX_IP    = lpW_RX_IP    ;
  logic    [ cbit(1,lpW_RX_IP_C) :0 ]    stp_lpW_RX_IP_C  = lpW_RX_IP_C  ;

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // registers with attribute "noprune" : interface  (off: pN_REG = 0, on: pN_REG = 1)
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  generate if (pE_GEN_NPR) begin: gen_npr__io

  // reset -------------------------------------// reset -----------------------------------------------//
 
  gen_npr #(.pN_REG (1), .pW_DATA (1)           )  stp__i_reset_n        (o_rx_clk, ri_reset_n          ); // used 'ri'
  //                 ^                                                              ^
  // setting -----------------------------------// setting ---------------------------------------------//

  gen_npr #(.pN_REG (1), .pW_DATA (1)           )  stp__i_set_cut        (o_rx_clk, ri_set_cut          ); // used 'ri'
  gen_npr #(.pN_REG (1), .pW_DATA (1)           )  stp__i_set_jumbo      (o_rx_clk, ri_set_jumbo        ); // used 'ri'
  gen_npr #(.pN_REG (1), .pW_DATA (2)           )  stp__i_set_speed      (o_rx_clk, ri_set_speed        ); // used 'ri'
  //                 ^                                                              ^
  // lan interface -----------------------------// lan interface ---------------------------------------//
 
  // lan mii                                       lan mii

  gen_npr #(.pN_REG (0), .pW_DATA (4)           )  stp__i_mii_rxd        (o_rx_clk, i_mii_rxd           ); // def: 0 / used fast input reg
  gen_npr #(.pN_REG (0), .pW_DATA (1)           )  stp__i_mii_rx_dv      (o_rx_clk, i_mii_rx_dv         ); // def: 0 / used fast input reg
  //                 ^                                                              ^
  // lan rmii                                      lan rmii

  gen_npr #(.pN_REG (0), .pW_DATA (2)           )  stp__i_rmii_rxd       (o_rx_clk, i_rmii_rxd          ); // def: 0 / used fast input reg
  gen_npr #(.pN_REG (0), .pW_DATA (1)           )  stp__i_rmii_rx_dv     (o_rx_clk, i_rmii_rx_dv        ); // def: 0 / used fast input reg
  //                 ^                                                              ^
  // lan gmii                                      lan gmii

  gen_npr #(.pN_REG (0), .pW_DATA (8)           )  stp__i_gmii_rxd       (o_rx_clk, i_gmii_rxd          ); // def: 0 / used fast input reg
  gen_npr #(.pN_REG (0), .pW_DATA (1)           )  stp__i_gmii_rx_dv     (o_rx_clk, i_gmii_rx_dv        ); // def: 0 / used fast input reg
  //                 ^                                                              ^
  // custom mode -------------------------------// custom mode -----------------------------------------//
 
  gen_npr #(.pN_REG (1), .pW_DATA (1)           )  stp__i_cst_cen        (o_rx_clk, i_cst_cen           );
  gen_npr #(.pN_REG (1), .pW_DATA (1)           )  stp__i_cst_ced        (o_rx_clk, i_cst_ced           );
  gen_npr #(.pN_REG (1), .pW_DATA (8)           )  stp__i_cst_rxd        (o_rx_clk, i_cst_rxd           );
  gen_npr #(.pN_REG (1), .pW_DATA (1)           )  stp__i_cst_rx_dv      (o_rx_clk, i_cst_rx_dv         );
  //                 ^                                                              ^
  // rx packet ---------------------------------// rx packet -------------------------------------------//
 
  // clock                                         clock

  gen_npr #(.pN_REG (1), .pW_DATA (1)           )  stp__o_rx_cen         (o_rx_clk, o_rx_cen            );
  gen_npr #(.pN_REG (1), .pW_DATA (1)           )  stp__o_rx_ced         (o_rx_clk, o_rx_ced            );
  //                 ^                                                              ^
  // strob                                         strob

  gen_npr #(.pN_REG (1), .pW_DATA (3)           )  stp__o_rx_str_body    (o_rx_clk, o_rx_str_body       );
  gen_npr #(.pN_REG (1), .pW_DATA (lpW_RX_LEN)  )  stp__o_rx_cnt_body    (o_rx_clk, o_rx_cnt_body       );
  gen_npr #(.pN_REG (1), .pW_DATA (3)           )  stp__o_rx_str_mac     (o_rx_clk, o_rx_str_mac        );
  gen_npr #(.pN_REG (1), .pW_DATA (4)           )  stp__o_rx_cnt_mac     (o_rx_clk, o_rx_cnt_mac        );
  gen_npr #(.pN_REG (1), .pW_DATA (3)           )  stp__o_rx_str_mda     (o_rx_clk, o_rx_str_mda        );
  gen_npr #(.pN_REG (1), .pW_DATA (3)           )  stp__o_rx_str_msa     (o_rx_clk, o_rx_str_msa        );
  gen_npr #(.pN_REG (1), .pW_DATA (3)           )  stp__o_rx_str_mlt     (o_rx_clk, o_rx_str_mlt        );
  gen_npr #(.pN_REG (1), .pW_DATA (3)           )  stp__o_rx_str_ip      (o_rx_clk, o_rx_str_ip         );
  gen_npr #(.pN_REG (1), .pW_DATA (lpW_RX_IP_C) )  stp__o_rx_cnt_ip      (o_rx_clk, o_rx_cnt_ip         );
  gen_npr #(.pN_REG (1), .pW_DATA (3)           )  stp__o_rx_str_pay     (o_rx_clk, o_rx_str_pay        );
  gen_npr #(.pN_REG (1), .pW_DATA (lpW_RX_LEN)  )  stp__o_rx_cnt_pay     (o_rx_clk, o_rx_cnt_pay        );
  gen_npr #(.pN_REG (1), .pW_DATA (3)           )  stp__o_rx_str_gap     (o_rx_clk, o_rx_str_gap        );
  gen_npr #(.pN_REG (1), .pW_DATA (pW_RX_GAP)   )  stp__o_rx_cnt_gap     (o_rx_clk, o_rx_cnt_gap        );
  //                 ^                                                              ^
  // data                                          data

  gen_npr #(.pN_REG (1), .pW_DATA (1)           )  stp__o_rx_dat_load    (o_rx_clk, o_rx_dat_load       );
  gen_npr #(.pN_REG (1), .pW_DATA (8)           )  stp__o_rx_dat_body    (o_rx_clk, o_rx_dat_body       );
  //                 ^                                                              ^
  gen_npr #(.pN_REG (1), .pW_DATA (1)           )  stp__o_rx_rdy_mac     (o_rx_clk, o_rx_rdy_mac        );
  gen_npr #(.pN_REG (1), .pW_DATA (48)          )  stp__o_rx_dat_mda     (o_rx_clk, o_rx_dat_mda        );
  gen_npr #(.pN_REG (1), .pW_DATA (48)          )  stp__o_rx_dat_msa     (o_rx_clk, o_rx_dat_msa        );
  gen_npr #(.pN_REG (1), .pW_DATA (16)          )  stp__o_rx_dat_mlt     (o_rx_clk, o_rx_dat_mlt        );
  //                 ^                                                              ^
  gen_npr #(.pN_REG (1), .pW_DATA (1)           )  stp__o_rx_rdy_ip      (o_rx_clk, o_rx_rdy_ip         );
  gen_npr #(.pN_REG (1), .pW_DATA (lpW_RX_IP)   )  stp__o_rx_dat_ip      (o_rx_clk, o_rx_dat_ip         );
  //                 ^                                                              ^
  // length                                        length

  gen_npr #(.pN_REG (1), .pW_DATA (1)           )  stp__o_rx_rdy_body    (o_rx_clk, o_rx_rdy_body       );
  gen_npr #(.pN_REG (1), .pW_DATA (lpW_RX_LEN)  )  stp__o_rx_len_body    (o_rx_clk, o_rx_len_body       );
  gen_npr #(.pN_REG (1), .pW_DATA (1)           )  stp__o_rx_rdy_gap     (o_rx_clk, o_rx_rdy_gap        );
  gen_npr #(.pN_REG (1), .pW_DATA (pW_RX_GAP)   )  stp__o_rx_len_gap     (o_rx_clk, o_rx_len_gap        );
  //                 ^                                                              ^
  // fcs crc                                       fcs crc

  gen_npr #(.pN_REG (1), .pW_DATA (1)           )  stp__o_rx_crc_rdy     (o_rx_clk, o_rx_crc_rdy        );
  gen_npr #(.pN_REG (1), .pW_DATA (1)           )  stp__o_rx_crc_err     (o_rx_clk, o_rx_crc_err        );
  //                 ^                                                              ^
  // rx counters -------------------------------// rx counters -----------------------------------------//
  
  // setting                                       setting

  for (j=1; j<=pN_RX_CNT; j++) begin:              stp__i_rx_set_p

  gen_npr #(.pN_REG (0), .pW_DATA (lpW_RX_LEN)  )  stp__i_rx_set_p_top   (o_rx_clk, i_rx_set_p_top [j]  );
  gen_npr #(.pN_REG (0), .pW_DATA (lpW_RX_LEN)  )  stp__i_rx_set_p_low   (o_rx_clk, i_rx_set_p_low [j]  ); end
  gen_npr #(.pN_REG (0), .pW_DATA (2)           )  stp__i_rx_set_p_spd   (o_rx_clk, i_rx_set_p_spd      );
  //                 ^                                                              ^
  // detector                                      detector

  gen_npr #(.pN_REG (1), .pW_DATA (1)           )  stp__o_rx_det_p_rdy   (o_rx_clk, o_rx_det_p_rdy      );
  gen_npr #(.pN_REG (1), .pW_DATA (1)           )  stp__o_rx_det_p_err   (o_rx_clk, o_rx_det_p_err      );
  gen_npr #(.pN_REG (1), .pW_DATA (1)           )  stp__o_rx_det_p_cut   (o_rx_clk, o_rx_det_p_cut      );
  gen_npr #(.pN_REG (1), .pW_DATA (1)           )  stp__i_rx_det_p_off   (o_rx_clk, i_rx_det_p_off      );
  //                 ^                                                              ^
  // counters                                      counters

  for (j=0; j<=pN_RX_CNT; j++) begin:              stp__o_rx_cnt

  gen_npr #(.pN_REG (0), .pW_DATA (pW_RX_CNT_P) )  stp__o_rx_cnt_p_all   (o_rx_clk, o_rx_cnt_p_all [j]  );
  gen_npr #(.pN_REG (0), .pW_DATA (pW_RX_CNT_P) )  stp__o_rx_cnt_p_err   (o_rx_clk, o_rx_cnt_p_err [j]  );
  gen_npr #(.pN_REG (0), .pW_DATA (pW_RX_CNT_P) )  stp__o_rx_cnt_p_off   (o_rx_clk, o_rx_cnt_p_off [j]  ); end
  gen_npr #(.pN_REG (0), .pW_DATA (pW_RX_CNT_S) )  stp__o_rx_cnt_p_spd   (o_rx_clk, o_rx_cnt_p_spd      );
  gen_npr #(.pN_REG (0), .pW_DATA (1)           )  stp__i_rx_cnt_p_clr   (o_rx_clk, i_rx_cnt_p_clr      );
  gen_npr #(.pN_REG (0), .pW_DATA (1)           )  stp__o_rx_cnt_p_rdy   (o_rx_clk, o_rx_cnt_p_rdy      );
  //                 ^                                                              ^
  // counters (rhythm)                             counters (rhythm)

  for (j=0; j<=pN_RX_CNT; j++) begin:              stp__o_rx_rcnt

  gen_npr #(.pN_REG (0), .pW_DATA (pW_RX_CNT_P) )  stp__o_rx_rcnt_p_all  (o_rx_clk, o_rx_rcnt_p_all [j] );
  gen_npr #(.pN_REG (0), .pW_DATA (pW_RX_CNT_P) )  stp__o_rx_rcnt_p_err  (o_rx_clk, o_rx_rcnt_p_err [j] );
  gen_npr #(.pN_REG (0), .pW_DATA (pW_RX_CNT_P) )  stp__o_rx_rcnt_p_off  (o_rx_clk, o_rx_rcnt_p_off [j] ); end
  gen_npr #(.pN_REG (0), .pW_DATA (pW_RX_CNT_S) )  stp__o_rx_rcnt_p_spd  (o_rx_clk, o_rx_rcnt_p_spd     );
  gen_npr #(.pN_REG (0), .pW_DATA (1)           )  stp__i_rx_rcnt_p_clr  (o_rx_clk, i_rx_rcnt_p_clr     );
  gen_npr #(.pN_REG (0), .pW_DATA (1)           )  stp__i_rx_rcnt_p_upd  (o_rx_clk, i_rx_rcnt_p_upd     );
  gen_npr #(.pN_REG (0), .pW_DATA (1)           )  stp__o_rx_rcnt_p_rdy  (o_rx_clk, o_rx_rcnt_p_rdy     );
  //                 ^                                                              ^
  //----------------------------------------------------------------------------------------------------//
  end endgenerate

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // registers with attribute "noprune" : parameters  (off: pN_REG = 0, on: pN_REG = 1)
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  generate if (pE_GEN_NPR) begin: gen_npr__par

  // interface -----------------------------------------// interface -----------------------------------//

  gen_npr #(.pN_REG (0), .pW_DATA (3)                   )  stp__pS_IO_TYPE   (o_rx_clk, stp_pS_IO_TYPE  );
  //                 ^                                                                  ^
  // custom mode ---------------------------------------// custom mode ---------------------------------//

  gen_npr #(.pN_REG (0), .pW_DATA (cbit(1,pT_CST_CEN )) )  stp__pT_CST_CEN   (o_rx_clk, stp_pT_CST_CEN  );
  gen_npr #(.pN_REG (0), .pW_DATA (cbit(1,pT_CST_CED )) )  stp__pT_CST_CED   (o_rx_clk, stp_pT_CST_CED  );
  //                 ^                                                                  ^
  // packet manipulation -------------------------------// packet manipulation -------------------------//

  gen_npr #(.pN_REG (0), .pW_DATA (1)                   )  stp__pE_RX_CUT    (o_rx_clk, stp_pE_RX_CUT   );
  gen_npr #(.pN_REG (0), .pW_DATA (1)                   )  stp__pE_RX_JUMBO  (o_rx_clk, stp_pE_RX_JUMBO );
  //                 ^                                                                  ^
  // packet structure ----------------------------------// packet structure ----------------------------//

  // frame header                                          frame header

  gen_npr #(.pN_REG (0), .pW_DATA (1)                   )  stp__pE_RX_HEAD   (o_rx_clk, stp_pE_RX_HEAD  );
  //                 ^                                                                  ^
  // frame body                                            frame body

  gen_npr #(.pN_REG (0), .pW_DATA (cbit(1,pW_RX_LEN  )) )  stp__pW_RX_LEN    (o_rx_clk, stp_pW_RX_LEN   );
  gen_npr #(.pN_REG (0), .pW_DATA (cbit(1,pV_RX_LEN  )) )  stp__pV_RX_LEN    (o_rx_clk, stp_pV_RX_LEN   );
  gen_npr #(.pN_REG (0), .pW_DATA (cbit(1,pW_RX_LEN_J)) )  stp__pW_RX_LEN_J  (o_rx_clk, stp_pW_RX_LEN_J );
  gen_npr #(.pN_REG (0), .pW_DATA (cbit(1,pV_RX_LEN_J)) )  stp__pV_RX_LEN_J  (o_rx_clk, stp_pV_RX_LEN_J );
  gen_npr #(.pN_REG (0), .pW_DATA (cbit(1,pW_RX_GAP  )) )  stp__pW_RX_GAP    (o_rx_clk, stp_pW_RX_GAP   );
  //                 ^                                                                  ^
  // mac header                                            mac header

  gen_npr #(.pN_REG (0), .pW_DATA (1)                   )  stp__pE_RX_MAC    (o_rx_clk, stp_pE_RX_MAC   );
  //                 ^                                                                  ^
  // ip header                                             ip header

  gen_npr #(.pN_REG (0), .pW_DATA (1)                   )  stp__pE_RX_IP     (o_rx_clk, stp_pE_RX_IP    );
  gen_npr #(.pN_REG (0), .pW_DATA (cbit(1,pN_RX_IPL  )) )  stp__pN_RX_IPL    (o_rx_clk, stp_pN_RX_IPL   );
  gen_npr #(.pN_REG (0), .pW_DATA (cbit(1,pN_RX_IPH  )) )  stp__pN_RX_IPH    (o_rx_clk, stp_pN_RX_IPH   );
  //                 ^                                                                  ^
  // packet counters -----------------------------------// packet counters -----------------------------//

  gen_npr #(.pN_REG (0), .pW_DATA (cbit(1,pN_RX_CNT  )) )  stp__pN_RX_CNT    (o_rx_clk, stp_pN_RX_CNT   );
  gen_npr #(.pN_REG (0), .pW_DATA (cbit(1,pW_RX_CNT_P)) )  stp__pW_RX_CNT_P  (o_rx_clk, stp_pW_RX_CNT_P );
  gen_npr #(.pN_REG (0), .pW_DATA (cbit(1,pW_RX_CNT_S)) )  stp__pW_RX_CNT_S  (o_rx_clk, stp_pW_RX_CNT_S );
  //                 ^                                                                  ^
  // local (for logic) ---------------------------------// local (for logic) ---------------------------//

  // crc comparator                                        crc comparator

  gen_npr #(.pN_REG (0), .pW_DATA (cbit(1,pV_CRC_DLY )) )  stp__pV_CRC_DLY   (o_rx_clk, stp_pV_CRC_DLY  );
  //                 ^                                                                  ^
  // out registers                                         out registers

  gen_npr #(.pN_REG (0), .pW_DATA (1)                   )  stp__pE_REG_OUT   (o_rx_clk, stp_pE_REG_OUT  );
  gen_npr #(.pN_REG (0), .pW_DATA (1)                   )  stp__pE_REG_CNT   (o_rx_clk, stp_pE_REG_CNT  );
  //                 ^                                                                  ^
  // local (for i/o) -----------------------------------// local (for i/o) -----------------------------//

  // packet structure                                      packet structure

  gen_npr #(.pN_REG (0), .pW_DATA (cbit(1,lpW_RX_LEN )) )  stp__lpW_RX_LEN   (o_rx_clk, stp_lpW_RX_LEN  );
  //                 ^                                                                  ^
  gen_npr #(.pN_REG (0), .pW_DATA (cbit(1,lpN_RX_IPL )) )  stp__lpN_RX_IPL   (o_rx_clk, stp_lpN_RX_IPL  );
  gen_npr #(.pN_REG (0), .pW_DATA (cbit(1,lpN_RX_IPH )) )  stp__lpN_RX_IPH   (o_rx_clk, stp_lpN_RX_IPH  );
  //                 ^                                                                  ^
  gen_npr #(.pN_REG (0), .pW_DATA (cbit(1,lpN_RX_IP  )) )  stp__lpN_RX_IP    (o_rx_clk, stp_lpN_RX_IP   );
  gen_npr #(.pN_REG (0), .pW_DATA (cbit(1,lpW_RX_IP  )) )  stp__lpW_RX_IP    (o_rx_clk, stp_lpW_RX_IP   );
  gen_npr #(.pN_REG (0), .pW_DATA (cbit(1,lpW_RX_IP_C)) )  stp__lpW_RX_IP_C  (o_rx_clk, stp_lpW_RX_IP_C );
  //                 ^                                                                  ^
  //----------------------------------------------------------------------------------------------------//
  end endgenerate

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  endmodule

  //==========================================================================================================================================================================================================================//
  // instantiation template
  //==========================================================================================================================================================================================================================//
  /*
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // interface
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  //---------------------------------------------// ethernet receiver -------| [ eth_rx / eth_rx_v2 ]

  //                                                reset

  var    logic                                      eth_rx__i_reset_n        ;

  //                                                setting

  var    logic                                      eth_rx__i_set_cut        ;
  var    logic    [                       1 :0 ]    eth_rx__i_set_speed      ;
  var    logic                                      eth_rx__i_set_jumbo      ;

  //                                                lan interface

  var    logic                                      eth_rx__i_mii_clk        ;
  var    logic    [                       3 :0 ]    eth_rx__i_mii_rxd        ;
  var    logic                                      eth_rx__i_mii_rx_dv      ;

  var    logic                                      eth_rx__i_rmii_clk       ;
  var    logic                                      eth_rx__o_rmii_clk_s     ;
  var    logic    [                       1 :0 ]    eth_rx__i_rmii_rxd       ;
  var    logic                                      eth_rx__i_rmii_crs_dv    ;

  var    logic                                      eth_rx__i_gmii_clk       ;
  var    logic    [                       7 :0 ]    eth_rx__i_gmii_rxd       ;
  var    logic                                      eth_rx__i_gmii_rx_dv     ;

  //                                                custom mode

  var    logic                                      eth_rx__i_cst_clk        ;
  var    logic                                      eth_rx__i_cst_cen        ;
  var    logic                                      eth_rx__i_cst_ced        ;
  var    logic    [                       7 :0 ]    eth_rx__i_cst_rxd        ;
  var    logic                                      eth_rx__i_cst_rx_dv      ;

  //                                                rx packet

  var    logic                                      eth_rx__o_rx_clk         ;
  var    logic                                      eth_rx__o_rx_cen         ;
  var    logic                                      eth_rx__o_rx_ced         ;

  var    logic    [                       2 :0 ]    eth_rx__o_rx_str_body    ;
  var    logic    [ ETH_RX__lpW_RX_LEN   -1 :0 ]    eth_rx__o_rx_cnt_body    ;
  var    logic    [                       2 :0 ]    eth_rx__o_rx_str_mac     ;
  var    logic    [                       3 :0 ]    eth_rx__o_rx_cnt_mac     ;
  var    logic    [                       2 :0 ]    eth_rx__o_rx_str_mda     ;
  var    logic    [                       2 :0 ]    eth_rx__o_rx_str_msa     ;
  var    logic    [                       2 :0 ]    eth_rx__o_rx_str_mlt     ;
  var    logic    [                       2 :0 ]    eth_rx__o_rx_str_ip      ;
  var    logic    [ ETH_RX__lpW_RX_IP_C  -1 :0 ]    eth_rx__o_rx_cnt_ip      ;
  var    logic    [                       2 :0 ]    eth_rx__o_rx_str_pay     ;
  var    logic    [ ETH_RX__lpW_RX_LEN   -1 :0 ]    eth_rx__o_rx_cnt_pay     ;
  var    logic    [                       2 :0 ]    eth_rx__o_rx_str_gap     ;
  var    logic    [ ETH_RX__pW_RX_GAP    -1 :0 ]    eth_rx__o_rx_cnt_gap     ;

  var    logic                                      eth_rx__o_rx_dat_load    ;
  var    logic    [                       7 :0 ]    eth_rx__o_rx_dat_body    ;
  var    logic                                      eth_rx__o_rx_rdy_mac     ;
  var    logic    [                      47 :0 ]    eth_rx__o_rx_dat_mda     ;
  var    logic    [                      47 :0 ]    eth_rx__o_rx_dat_msa     ;
  var    logic    [                      15 :0 ]    eth_rx__o_rx_dat_mlt     ;
  var    logic                                      eth_rx__o_rx_rdy_ip      ;
  var    logic    [                      47 :0 ]    eth_rx__o_rx_dat_ip      ;

  var    logic                                      eth_rx__o_rx_rdy_body    ;
  var    logic    [ ETH_RX__lpW_RX_LEN   -1 :0 ]    eth_rx__o_rx_len_body    ;
  var    logic                                      eth_rx__o_rx_rdy_gap     ;
  var    logic    [ ETH_RX__pW_RX_GAP    -1 :0 ]    eth_rx__o_rx_len_gap     ;

  var    logic                                      eth_rx__o_rx_crc_rdy     ;
  var    logic                                      eth_rx__o_rx_crc_err     ;

  //                                                rx counters / setting

  var    logic    [                       1 :0 ]    eth_rx__i_rx_set_p_spd   ;
  var    logic    [ ETH_RX__pN_RX_CNT       :1 ]
                  [ ETH_RX__lpW_RX_LEN   -1 :0 ]    eth_rx__i_rx_set_p_top   ;
  var    logic    [ ETH_RX__pN_RX_CNT       :1 ]
                  [ ETH_RX__lpW_RX_LEN   -1 :0 ]    eth_rx__i_rx_set_p_low   ;

  //                                                rx counters / detector

  var    logic                                      eth_rx__o_rx_det_p_rdy   ;
  var    logic                                      eth_rx__o_rx_det_p_err   ;
  var    logic                                      eth_rx__o_rx_det_p_cut   ;
  var    logic                                      eth_rx__i_rx_det_p_off   ;

  //                                                rx counters / counters

  var    logic                                      eth_rx__i_rx_cnt_p_clr   ;

  var    logic                                      eth_rx__o_rx_cnt_p_rdy   ;
  var    logic    [ ETH_RX__pN_RX_CNT       :0 ]
                  [ ETH_RX__pW_RX_CNT_P  -1 :0 ]    eth_rx__o_rx_cnt_p_all   ;
  var    logic    [ ETH_RX__pN_RX_CNT       :0 ]
                  [ ETH_RX__pW_RX_CNT_P  -1 :0 ]    eth_rx__o_rx_cnt_p_err   ;
  var    logic    [ ETH_RX__pN_RX_CNT       :0 ]
                  [ ETH_RX__pW_RX_CNT_P  -1 :0 ]    eth_rx__o_rx_cnt_p_off   ;

  var    logic    [ ETH_RX__pW_RX_CNT_S  -1 :0 ]    eth_rx__o_rx_cnt_p_spd   ;

  //                                                rx counters / rhythm

  var    logic                                      eth_rx__i_rx_rcnt_p_clr  ;

  var    logic                                      eth_rx__i_rx_rcnt_p_udp  ;

  var    logic                                      eth_rx__o_rx_rcnt_p_rdy  ;
  var    logic    [ ETH_RX__pN_RX_CNT       :0 ]
                  [ ETH_RX__pW_RX_CNT_P  -1 :0 ]    eth_rx__o_rx_rcnt_p_all  ;
  var    logic    [ ETH_RX__pN_RX_CNT       :0 ]
                  [ ETH_RX__pW_RX_CNT_P  -1 :0 ]    eth_rx__o_rx_rcnt_p_err  ;
  var    logic    [ ETH_RX__pN_RX_CNT       :0 ]
                  [ ETH_RX__pW_RX_CNT_P  -1 :0 ]    eth_rx__o_rx_rcnt_p_off  ;

  var    logic    [ ETH_RX__pW_RX_CNT_S  -1 :0 ]    eth_rx__o_rx_rcnt_p_spd  ;

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // receiver of ethernet frames  [ eth_rx / eth_rx_v2 ]
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  // reset

  assign  eth_rx__i_reset_n        = ;

  // setting

  assign  eth_rx__i_set_cut        = ;
  assign  eth_rx__i_set_speed      = ;
  assign  eth_rx__i_set_jumbo      = ;

  // lan interface

  assign  eth_rx__i_mii_clk        = ;
  assign  eth_rx__i_mii_rxd        = ;
  assign  eth_rx__i_mii_rx_dv      = ;

  assign  eth_rx__i_rmii_clk       = ;
  assign  eth_rx__i_rmii_rxd       = ;
  assign  eth_rx__i_rmii_crs_dv    = ;

  assign  eth_rx__i_gmii_clk       = ;
  assign  eth_rx__i_gmii_rx_dv     = ;
  assign  eth_rx__i_gmii_rxd       = ;

  // custom mode

  assign  eth_rx__i_cst_clk        = ;
  assign  eth_rx__i_cst_cen        = ;
  assign  eth_rx__i_cst_ced        = ;
  assign  eth_rx__i_cst_rxd        = ;
  assign  eth_rx__i_cst_rx_dv      = ;
 
  // rx counters / setting

  assign  eth_rx__i_rx_set_p_top   = ;
  assign  eth_rx__i_rx_set_p_low   = ;
  assign  eth_rx__i_rx_set_p_spd   = ;

  // rx counters / detector

  assign  eth_rx__i_rx_det_p_off   = ;

  // rx counters / counters

  assign  eth_rx__i_rx_cnt_p_clr   = ;
  assign  eth_rx__i_rx_rcnt_p_clr  = ;
  assign  eth_rx__i_rx_rcnt_p_upd  = ;

  //-------------------------------------------------------------------------------------|
                                                         //                              |
  eth_rx_v2              eth_rx                          //                              |
  (                                                      //                              |
  // reset ----------------------------------------------//------------------------------|
                                                         //           |                  |
     .i_reset_n          ( eth_rx__i_reset_n       ),    //  in       |       opt        |
                                                         //           |                  |
  // setting --------------------------------------------//-----------|------------------|
                                                         //           |                  |
     .i_set_cut          ( eth_rx__i_set_cut       ),    //  in       |       opt        |
     .i_set_speed        ( eth_rx__i_set_speed     ),    //  in       |  req             |
     .i_set_jumbo        ( eth_rx__i_set_jumbo     ),    //  in       |       opt        |
                                                         //           |                  |
  // lan interface --------------------------------------//-----------|------------------|
                                                         //           |                  |
  // lan mii             lan mii                         //-----------|------------------|
                                                         //           |                  |
     .i_mii_clk          ( eth_rx__i_mii_clk       ),    //  in       |  req             |
     .i_mii_rxd          ( eth_rx__i_mii_rxd       ),    //  in       |  req             |
     .i_mii_rx_dv        ( eth_rx__i_mii_rx_dv     ),    //  in       |  req             |
                                                         //           |                  |
  // lan rmii            lan rmii                        //-----------|------------------|
                                                         //           |                  |
     .i_rmii_clk         ( eth_rx__i_rmii_clk      ),    //  in       |  req             |
     .o_rmii_clk_s       ( eth_rx__o_rmii_clk_s    ),    //      out  |            test  |
     .i_rmii_rxd         ( eth_rx__i_rmii_rxd      ),    //  in       |  req             |
     .i_rmii_crs_dv      ( eth_rx__i_rmii_crs_dv   ),    //  in       |  req             |
                                                         //           |                  |
  // lan gmii            lan gmii                        //-----------|------------------|
                                                         //           |                  |
     .i_gmii_clk         ( eth_rx__i_gmii_clk      ),    //  in       |  req             |
     .i_gmii_rxd         ( eth_rx__i_gmii_rxd      ),    //  in       |  req             |
     .i_gmii_rx_dv       ( eth_rx__i_gmii_rx_dv    ),    //  in       |  req             |
                                                         //           |                  |
  // custom mode ----------------------------------------//-----------|------------------|
                                                         //           |                  |
     .i_cst_clk          ( eth_rx__i_cst_clk       ),    //  in       |       opt        |
     .i_cst_cen          ( eth_rx__i_cst_cen       ),    //  in       |       opt        |
     .i_cst_ced          ( eth_rx__i_cst_ced       ),    //  in       |       opt        |
     .i_cst_rxd          ( eth_rx__i_cst_rxd       ),    //  in       |       opt        |
     .i_cst_rx_dv        ( eth_rx__i_cst_rx_dv     ),    //  in       |       opt        |
                                                         //           |                  |
  // rx packet ------------------------------------------//-----------|------------------|
                                                         //           |                  |
  // clock               clock                           //-----------|------------------|
                                                         //           |                  |
     .o_rx_clk           ( eth_rx__o_rx_clk        ),    //      out  |  req             |
     .o_rx_cen           ( eth_rx__o_rx_cen        ),    //      out  |  req             |
     .o_rx_ced           ( eth_rx__o_rx_ced        ),    //      out  |  req             |
                                                         //           |                  |
  // strob               strob                           //-----------|------------------|
                                                         //           |                  |
     .o_rx_str_body      ( eth_rx__o_rx_str_body   ),    //      out  |  req             |
     .o_rx_cnt_body      ( eth_rx__o_rx_cnt_body   ),    //      out  |       opt        |
     .o_rx_str_mac       ( eth_rx__o_rx_str_mac    ),    //      out  |       opt        |
     .o_rx_cnt_mac       ( eth_rx__o_rx_cnt_mac    ),    //      out  |       opt        |
     .o_rx_str_mda       ( eth_rx__o_rx_str_mda    ),    //      out  |       opt        |
     .o_rx_str_msa       ( eth_rx__o_rx_str_msa    ),    //      out  |       opt        |
     .o_rx_str_mlt       ( eth_rx__o_rx_str_mlt    ),    //      out  |       opt        |
     .o_rx_str_ip        ( eth_rx__o_rx_str_ip     ),    //      out  |       opt        |
     .o_rx_cnt_ip        ( eth_rx__o_rx_cnt_ip     ),    //      out  |       opt        |
     .o_rx_str_pay       ( eth_rx__o_rx_str_pay    ),    //      out  |       opt        |
     .o_rx_cnt_pay       ( eth_rx__o_rx_cnt_pay    ),    //      out  |       opt        |
     .o_rx_str_gap       ( eth_rx__o_rx_str_gap    ),    //      out  |       opt        |
     .o_rx_cnt_gap       ( eth_rx__o_rx_cnt_gap    ),    //      out  |       opt        |
                                                         //           |                  |
  // data                data                            //-----------|------------------|
                                                         //           |                  |
     .o_rx_dat_load      ( eth_rx__o_rx_dat_load   ),    //      out  |  req             |
     .o_rx_dat_body      ( eth_rx__o_rx_dat_body   ),    //      out  |  req             |
                                                         //           |                  |
     .o_rx_rdy_mac       ( eth_rx__o_rx_rdy_mac    ),    //      out  |       opt        |
     .o_rx_dat_mda       ( eth_rx__o_rx_dat_mda    ),    //      out  |       opt        |
     .o_rx_dat_msa       ( eth_rx__o_rx_dat_msa    ),    //      out  |       opt        |
     .o_rx_dat_mlt       ( eth_rx__o_rx_dat_mlt    ),    //      out  |       opt        |
                                                         //           |                  |
     .o_rx_rdy_ip        ( eth_rx__o_rx_rdy_ip     ),    //      out  |       opt        |
     .o_rx_dat_ip        ( eth_rx__o_rx_dat_ip     ),    //      out  |       opt        |
                                                         //           |                  |
  // length              length                          //-----------|------------------|
                                                         //           |                  |
     .o_rx_rdy_body      ( eth_rx__o_rx_rdy_body   ),    //      out  |  req             |
     .o_rx_len_body      ( eth_rx__o_rx_len_body   ),    //      out  |  req             |
     .o_rx_rdy_gap       ( eth_rx__o_rx_rdy_gap    ),    //      out  |       opt        |
     .o_rx_len_gap       ( eth_rx__o_rx_len_gap    ),    //      out  |       opt        |
                                                         //           |                  |
  // fcs crc             fcs crc                         //-----------|------------------|
                                                         //           |                  |
     .o_rx_crc_rdy       ( eth_rx__o_rx_crc_rdy    ),    //      out  |  req             |
     .o_rx_crc_err       ( eth_rx__o_rx_crc_err    ),    //      out  |  req             |
                                                         //           |                  |
  // rx counters ----------------------------------------//-----------|------------------|
                                                         //           |                  |
  // setting             setting                         //-----------|------------------|
                                                         //           |                  |
     .i_rx_set_p_top     ( eth_rx__i_rx_set_p_top  ),    //  in       |       opt        |
     .i_rx_set_p_low     ( eth_rx__i_rx_set_p_low  ),    //  in       |       opt        |
     .i_rx_set_p_spd     ( eth_rx__i_rx_set_p_spd  ),    //  in       |       opt        |
                                                         //           |                  |
  // detector            detector                        //-----------|------------------|
                                                         //           |                  |
     .o_rx_det_p_rdy     ( eth_rx__o_rx_det_p_rdy  ),    //      out  |       opt        |
     .o_rx_det_p_err     ( eth_rx__o_rx_det_p_err  ),    //      out  |       opt        |
     .o_rx_det_p_cut     ( eth_rx__o_rx_det_p_cut  ),    //      out  |       opt        |
     .i_rx_det_p_off     ( eth_rx__i_rx_det_p_off  ),    //  in       |       opt        |
                                                         //           |                  |
  // counters            counters                        //-----------|------------------|
                                                         //           |                  |
     .i_rx_cnt_p_clr     ( eth_rx__i_rx_cnt_p_clr  ),    //  in       |       opt        |
     .o_rx_cnt_p_rdy     ( eth_rx__o_rx_cnt_p_rdy  ),    //      out  |       opt        |
     .o_rx_cnt_p_all     ( eth_rx__o_rx_cnt_p_all  ),    //      out  |       opt        |
     .o_rx_cnt_p_err     ( eth_rx__o_rx_cnt_p_err  ),    //      out  |       opt        |
     .o_rx_cnt_p_off     ( eth_rx__o_rx_cnt_p_off  ),    //      out  |       opt        |
     .o_rx_cnt_p_spd     ( eth_rx__o_rx_cnt_p_spd  ),    //      out  |       opt        |
                                                         //           |                  |
  // counters            counters (rhythm)               //-----------|------------------|
                                                         //           |                  |
     .i_rx_rcnt_p_clr    ( eth_rx__i_rx_rcnt_p_clr ),    //  in       |       opt        |
     .i_rx_rcnt_p_upd    ( eth_rx__i_rx_rcnt_p_upd ),    //  in       |       opt        |
     .o_rx_rcnt_p_rdy    ( eth_rx__o_rx_rcnt_p_rdy ),    //      out  |       opt        |
     .o_rx_rcnt_p_all    ( eth_rx__o_rx_rcnt_p_all ),    //      out  |       opt        |
     .o_rx_rcnt_p_err    ( eth_rx__o_rx_rcnt_p_err ),    //      out  |       opt        |
     .o_rx_rcnt_p_off    ( eth_rx__o_rx_rcnt_p_off ),    //      out  |       opt        |
     .o_rx_rcnt_p_spd    ( eth_rx__o_rx_rcnt_p_spd )     //      out  |       opt        |
                                                         //           |                  |
  );//-----------------------------------------------------------------------------------|

  // interface

  defparam  eth_rx.pS_IO_TYPE   = ETH_RX__pS_IO_TYPE  ;

  // custom mode

  defparam  eth_rx.pT_CST_CEN   = ETH_RX__pT_CST_CEN  ;
  defparam  eth_rx.pT_CST_CED   = ETH_RX__pT_CST_CED  ;

  // packet manipulation

  defparam  eth_rx.pE_RX_CUT    = ETH_RX__pE_RX_CUT   ;
  defparam  eth_rx.pE_RX_JUMBO  = ETH_RX__pE_RX_JUMBO ;

  // packet structure
  
  defparam  eth_rx.pE_RX_HEAD   = ETH_RX__pE_RX_HEAD  ;

  defparam  eth_rx.pW_RX_LEN    = ETH_RX__pW_RX_LEN   ;
  defparam  eth_rx.pV_RX_LEN    = ETH_RX__pV_RX_LEN   ;
  defparam  eth_rx.pW_RX_LEN_J  = ETH_RX__pW_RX_LEN_J ;
  defparam  eth_rx.pV_RX_LEN_J  = ETH_RX__pV_RX_LEN_J ;
  defparam  eth_rx.pW_RX_GAP    = ETH_RX__pW_RX_GAP   ;

  defparam  eth_rx.pE_RX_IP     = ETH_RX__pE_RX_IP    ;
  defparam  eth_rx.pN_RX_IPL    = ETH_RX__pN_RX_IPL   ;
  defparam  eth_rx.pN_RX_IPH    = ETH_RX__pN_RX_IPH   ;

  defparam  eth_rx.pE_RX_MAC    = ETH_RX__pE_RX_MAC   ;

  // packet counters

  defparam  eth_rx.pN_RX_CNT    = ETH_RX__pN_RX_CNT   ;
  defparam  eth_rx.pW_RX_CNT_P  = ETH_RX__pW_RX_CNT_P ;
  defparam  eth_rx.pW_RX_CNT_S  = ETH_RX__pW_RX_CNT_S ;

  // signal tap (for tetsing)

  defparam  eth_rx.pE_GEN_NPR   = 0; // for testing only

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  */




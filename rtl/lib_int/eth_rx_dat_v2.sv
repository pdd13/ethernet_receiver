  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Company      : Micran, Department of Telecommunications
  //
  //  Engineer     : Pogodaev Danil
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Library      : lib_ethernet / library of ethernet components
  //
  //  Component    : eth_rx_v2 / receiver of ethernet frames (packets)
  //
  //  File Name    : eth_rx_dat_v2 .sv (rtl)
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Description  : Data extractor for IP and MAC headers
  //
  //                  - ip header               / o_dat_ip
  //                  - mac destination address / o_dat_mda
  //                  - mac source address      / o_dat_msa
  //                  - mac length or type      / o_dat_mlt
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Table (1)    : Clock manager of ethernet frames for receiver
  //
  //                 |--------------------------------------------------------------------------------------------------|
  //                 |                       |          |                       frequency (period)                      |
  //                 |        setting        |  clock   |---------------------------------------------------------------|
  //                 |                       |          |      10 mbit/s      |     100 mbit/s     |    1000 mbit/s     |
  //                 |=======================|==========|=====================|====================|====================|
  //                 |                       |  rx_clk  |  2.50 mhz (400 ns)  |  25.0 mhz (40 ns)  |  no                |
  //                 |  mii interface:       |----------|---------------------|--------------------|--------------------|
  //                 |                       |  rx_cen  |  1'b1               |  1'b1              |  no                |
  //                 |  pS_IO_TYPE = mii     |----------|---------------------|--------------------|--------------------|
  //                 |                       |  rx_ced  |  1.25 mhz (800 ns)  |  12.5 mhz (80 ns)  |  no                |
  //                 |=======================|==========|=====================|====================|====================|
  //                 |                       |  rx_clk  |  50.00 mhz (20 ns)  |  50.00 mhz (20 ns) |  no                |
  //                 |  rmii interface:      |----------|---------------------|--------------------|--------------------|
  //                 |                       |  rx_cen  |  5.00 mhz (200 ns)  |  1'b1              |  no                |
  //                 |  pS_IO_TYPE = rmii    |----------|---------------------|--------------------|--------------------|
  //                 |                       |  rx_ced  |  1.25 mhz (800 ns)  |  12.5 mhz (80 ns)  |  no                |
  //                 |=======================|==========|=====================|====================|====================|
  //                 |                       |  rx_clk  |  2.50 mhz (400 ns)  |  25.0 mhz (40 ns)  |  125.0 mhz (8 ns)  |
  //                 |  gmii interface:      |----------|---------------------|--------------------|--------------------|
  //                 |                       |  rx_cen  |  1'b1               |  1'b1              |  1'b1              |
  //                 |  pS_IO_TYPE = gmii    |----------|---------------------|--------------------|--------------------|
  //                 |                       |  rx_ced  |  1.25 mhz (800 ns)  |  12.5 mhz (80 ns)  |  1'b1              |
  //                 |=======================|==========|===============================================================|
  //                 |                       |  rx_clk  |  i_cst_clk                                                    |
  //                 |  custom mode:         |----------|---------------------------------------------------------------|
  //                 |                       |  rx_cen  |  pT_CST_CEN = 0 : i_cst_cen                                   |
  //                 |  pS_IO_TYPE = custom  |          |  pT_CST_CEN > 0 : i_cst_clk * pT_CST_CEN                      |
  //                 |                       |----------|---------------------------------------------------------------|
  //                 |                       |  rx_ced  |  pT_CST_CED = 0 : i_cst_ced                                   |
  //                 |                       |          |  pT_CST_CED > 0 : i_cst_clk * pT_CST_CEN * pT_CST_CED         |
  //                 |==================================================================================================|
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Table (2)    : Structure of ethernet frames (std. 802.3) for receiver / see also note (1)
  //
  //                 |------------------------------------------------------------------------------|
  //                 | frame header  |      mac header       |        data       |  fcs  | ethernet |
  //                 |---------------|-----------------------|-------------------|-------| receiver |
  //                 |       8       |          14           |      46-1500      |   4   |          |
  //                 |===============|=======================|===================|=======|==========|
  //                 |  prm  |  sfd  |  mda  |  msa  |  mlt  |  payload  |  pad  |  crc  | strob    |
  //                 |-------|-------|-------|-------|-------|-----------|-------|-------| name     |
  //                 |   7   |   1   |   6   |   6   |   2   |  0-1500   | 0-46  |   4   |          |
  //                 |=======|=======|=======|=======|=======|===========|=======|=======|==========|
  //                 |       |       |       |       |       |           |       |       |          |
  //                 |       |       | ***** | ***** | ***** | ********* | ***** | ***** | str_body |
  //                 |       |       | ***** | ***** | ***** |           |       |       | str_mac  |
  //                 |       |       | ***** |       |       |           |       |       | str_mda  |
  //                 |       |       |       | ***** |       |           |       |       | str_msa  |
  //                 |       |       |       |       | ***** |           |       |       | str_mlt  |
  //                 |       |       |       |       |       | ********* | ***** | ***** | str_pay  | with crc (!)
  //                 |       |       |       |       |       | ***       |       |       | str_ip   |
  //                 |       |       |       |       |       |           |       |       |          |
  //                 |------------------------------------------------------------------------------|
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Include Files
  //  
  //  Category     Type       Name            Ver     Description
  //
  //  lib_global   function   cbit      .sv   1.0.0   bits calculator for input number (round up)
  //  lib_global   rtl        gen_npr   .sv   1.0.0   generator of registers with attribute "noprune"
  //  lib_global   rtl        z_line    .sv   1.0.0   delay line for serial and parallel data
  //  lib_global   rtl        shift_s2p .sv   1.0.0   shift register for serial to parallel
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Revision List
  //
  //  Version   Date       Author
  //
  //  2.0.0     26.09.16   Pogodaev Danil
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  module eth_rx_dat_v2 (

  //     clear
    
         i_sclr_n      ,

  //     clock

         i_clk         ,
         i_cen         ,
         i_ced         ,

  //     strob

         i_str_mda     ,
         i_str_msa     ,
         i_str_mlt     ,
         i_str_ip      ,

  //     data

         i_dat_load    ,
         i_dat_body    ,

         o_rdy_mac     ,
         o_dat_mda     ,
         o_dat_msa     ,
         o_dat_mlt     ,

         o_rdy_ip      ,
         o_dat_ip     );

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // parameters : setting
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
                                                                   //      |                                            |         |         |
  // packet structure ---------------------------------------------//------|--------------------------------------------|---------|---------|---------------------------------------------------------------------------------//
                                                                   //      |                                            |         |         |
  // mac header                                                    //------|--------------------------------------------|---------|---------|---------------------------------------------------------------------------------//
                                                                   //      |                                            |         |         |
  parameter bit  pE_RX_MAC   = 0                              ;    // req  | enable of mac header                       | on/off  | def: 0  |
                                                                   //      |                                            |         |         |
  // ip header                                                     //------|--------------------------------------------|---------|---------|---------------------------------------------------------------------------------//
                                                                   //      |                                            |         |         |  
  parameter int  pE_RX_IP    = 0                              ;    // req  | enable of ip header                        | on/off  | def: 0  |
  parameter int  pN_RX_IPL   = 1                              ;    // req  | number of first (low) byte in payload      | byte    | def: 1  |
  parameter int  pN_RX_IPH   = 4                              ;    // req  | number of last (high) byte in payload      | byte    |         |
                                                                   //      |                                            |         |         |
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // parameters : local (for i/o)
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
                                                                   //      |                                            |         |         |
  // packet structure ---------------------------------------------//------|--------------------------------------------|---------|---------|---------------------------------------------------------------------------------//
                                                                   //      |                                            |         |         |
  // ip header                                                     //------|--------------------------------------------|---------|---------|---------------------------------------------------------------------------------//
                                                                   //      |                                            |         |         |
  parameter int  lpN_RX_IPL  = pN_RX_IPL                      ;    // req  | number of first (low) byte in payload      | byte    | def: 1  |
  parameter int  lpN_RX_IPH  = pN_RX_IPH >= pN_RX_IPL ?            // req  | number of last (high) byte in payload      | byte    |         |
                               pN_RX_IPH  : pN_RX_IPL         ;    //      |                                            |         |         |
                                                                   //      |                                            |         |         |
  parameter int  lpN_RX_IP   = lpN_RX_IPH <= lpN_RX_IPL ? 1   :    //      |                                            |         |         |
                               lpN_RX_IPH  - lpN_RX_IPL + 1   ;    // req  | number of bytes                            | byte    |         |
                                                                   //      |                                            |         |         |
  parameter int  lpW_RX_IP   = lpN_RX_IP * 8                  ;    // req  | number of bits                             | bit     |         | info: used for 'o_dat_ip'
                                                                   //      |                                            |         |         |
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // interface
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
                                                                   //      |                                            |         |         |
  //----------------------------------// clear --------------------//------|--------------------------------------------|---------|---------|---------------------------------------------------------------------------------//
                                                                   //      |                                            |         |         |
  input  logic                           i_sclr_n             ;    // opt  | sync clear (active low)                    | strob   | def: 1  |
                                                                   //      |                                            |         |         |
  //----------------------------------// clock --------------------//------|--------------------------------------------|---------|---------|---------------------------------------------------------------------------------//
                                                                   //      |                                            |         |         |
  input  logic                           i_clk                ;    // req  | clock                                      | clock   |         |
  input  logic                           i_cen                ;    // req  | clock enable                               | pulse   |         | width: o_clk
  input  logic                           i_ced                ;    // req  | clock enable for data                      | pulse   |         | width: o_cen
                                                                   //      |                                            |         |         |
  //                                     strob                     //------|--------------------------------------------|---------|---------|---------------------------------------------------------------------------------//
                                                                   //      |                                            |         |         |
  input  logic    [            2 :0 ]    i_str_mda            ;    // req  | mac destination address  / strob full      | strob   |         |
  input  logic    [            2 :0 ]    i_str_msa            ;    // req  | mac source address       / strob full      | strob   |         |
  input  logic    [            2 :0 ]    i_str_mlt            ;    // req  | mac length or type       / strob full      | strob   |         |
  input  logic    [            2 :0 ]    i_str_ip             ;    // req  | ip header                / strob full      | strob   |         |
                                                                   //      |                                            |         |         |
  //                                     data                      //------|--------------------------------------------|---------|---------|---------------------------------------------------------------------------------//
                                                                   //      |                                            |         |         |
  input  logic                           i_dat_load           ;    // req  | frame body               / load (byte)     | pulse   |         | width: o_cen
  input  logic    [            7 :0 ]    i_dat_body           ;    // req  | frame body               / data (byte)     | pdata   |         |
                                                                   //      |                                            |         |         |
  output logic                           o_rdy_mac            ;    // req  | mac header               / ready           | pulse   |         | width: o_cen ; info: used for latch 'o_dat_mac'
  output logic    [           47 :0 ]    o_dat_mda            ;    // req  | mac destination address  / data            | pdata   |         |
  output logic    [           47 :0 ]    o_dat_msa            ;    // req  | mac source address       / data            | pdata   |         |
  output logic    [           15 :0 ]    o_dat_mlt            ;    // req  | mac length or type       / data            | pdata   |         |
                                                                   //      |                                            |         |         |
  output logic                           o_rdy_ip             ;    // req  | ip header                / ready           | pulse   |         | width: o_cen ; info: used for latch 'o_dat_ip'
  output logic    [ lpW_RX_IP -1 :0 ]    o_dat_ip             ;    // req  | ip header                / data            | pdata   |         |
                                                                   //      |                                            |         |         |
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // logic
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  //----------------------------------// mda data / shifter -------| [ shift_mda / shift_s2p ]

  //                                     clock

  var    logic                           shift_mda__i_clk          ;
  var    logic                           shift_mda__i_clk_en       ;
  var    logic                           shift_mda__i_sclr         ;

  //                                     serial

  var    logic                           shift_mda__i_sload        ;
  var    logic                           shift_mda__i_sload_en     ;
  var    logic    [            7 :0 ]    shift_mda__i_sdata        ;

  //                                     parallel

  var    logic                           shift_mda__o_pload        ;
  var    logic    [           47 :0 ]    shift_mda__o_pdata        ;

  //----------------------------------// msa data / shifter -------| [ shift_msa / shift_s2p ]

  //                                     clock

  var    logic                           shift_msa__i_clk          ;
  var    logic                           shift_msa__i_clk_en       ;
  var    logic                           shift_msa__i_sclr         ;

  //                                     serial

  var    logic                           shift_msa__i_sload        ;
  var    logic                           shift_msa__i_sload_en     ;
  var    logic    [            7 :0 ]    shift_msa__i_sdata        ;

  //                                     parallel

  var    logic                           shift_msa__o_pload        ;
  var    logic    [           47 :0 ]    shift_msa__o_pdata        ;

  //----------------------------------// mlt data / shifter -------| [ shift_mlt / shift_s2p ]

  //                                     clock

  var    logic                           shift_mlt__i_clk          ;
  var    logic                           shift_mlt__i_clk_en       ;
  var    logic                           shift_mlt__i_sclr         ;

  //                                     serial

  var    logic                           shift_mlt__i_sload        ;
  var    logic                           shift_mlt__i_sload_en     ;
  var    logic    [            7 :0 ]    shift_mlt__i_sdata        ;

  //                                     parallel

  var    logic                           shift_mlt__o_pload        ;
  var    logic    [           15 :0 ]    shift_mlt__o_pdata        ;

  //----------------------------------// ip data / shifter --------| [ shift_ip / shift_s2p ]

  //                                     clock

  var    logic                           shift_ip__i_clk           ;
  var    logic                           shift_ip__i_clk_en        ;
  var    logic                           shift_ip__i_sclr          ;

  //                                     serial

  var    logic                           shift_ip__i_sload         ;
  var    logic                           shift_ip__i_sload_en      ;
  var    logic    [            7 :0 ]    shift_ip__i_sdata         ;

  //                                     parallel

  var    logic                           shift_ip__o_pload         ;
  var    logic    [ lpW_RX_IP-1  :0 ]    shift_ip__o_pdata         ;

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  //==========================================================================================================================================================================================================================//
  // body of module
  //==========================================================================================================================================================================================================================//

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // out logic (without out reg)
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  // data / mac header

  assign  o_rdy_mac  = shift_mlt__o_pload ;
  assign  o_dat_mda  = shift_mda__o_pdata ;
  assign  o_dat_msa  = shift_msa__o_pdata ;
  assign  o_dat_mlt  = shift_mlt__o_pdata ;

  // data / ip header

  assign  o_rdy_ip   = shift_ip__o_pload  ;
  assign  o_dat_ip   = shift_ip__o_pdata  ;

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // data mda / shift : serial to parallel  [ shift_mda / shift_s2p ]
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  // clock

  assign  shift_mda__i_clk       = i_clk      ;
  assign  shift_mda__i_clk_en    = i_cen      ;
  assign  shift_mda__i_sclr      = ~i_sclr_n  ;

  // serial

  assign  shift_mda__i_sload     = i_str_mda  ;
  assign  shift_mda__i_sload_en  = i_dat_load ;
  assign  shift_mda__i_sdata     = i_dat_body ;

  //------------------------------------------------------------------------------|
                                                  //                              |
  shift_s2p         shift_mda                     //                              |
  (                                               //                              |
  // clock ---------------------------------------//------------------------------|
                                                  //           |                  |
     .i_clk         ( shift_mda__i_clk      ),    //  in       |  req             |
     .i_clk_en      ( shift_mda__i_clk_en   ),    //  in       |       opt        |
     .i_sclr        ( shift_mda__i_sclr     ),    //  in       |       opt        |
                                                  //           |                  |
  // serial --------------------------------------//-----------|------------------|
                                                  //           |                  |
     .i_sload       ( shift_mda__i_sload    ),    //  in       |  req             |
     .i_sload_en    ( shift_mda__i_sload_en ),    //  in       |       opt        |
     .i_sdata       ( shift_mda__i_sdata    ),    //  in       |  req             |
                                                  //           |                  |
  // parallel ------------------------------------//-----------|------------------|
                                                  //           |                  |
     .o_pload       ( shift_mda__o_pload    ),    //      out  |  req             |
     .o_pdata       ( shift_mda__o_pdata    )     //      out  |  req             |
                                                  //           |                  |
  );//----------------------------------------------------------------------------|

  // width of data

  defparam  shift_mda.pW_SDATA  = 8     ; //  8 bit = 1 byte
  defparam  shift_mda.pW_PDATA  = 48    ; // 48 bit = 6 bytes

  // first word

  defparam  shift_mda.pS_FIRST  = "msb" ;

  // auto mode

  defparam  shift_mda.pE_AUTO   = 0     ; // def: 0
  defparam  shift_mda.pT_AUTO   = 0     ; // def: 0

  // signal tap (for tetsing)

  defparam  shift_mda.pE_GNPR   = 0     ; // for testing only

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // data msa / shift : serial to parallel  [ shift_msa / shift_s2p ]
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  // clock

  assign  shift_msa__i_clk       = i_clk      ;
  assign  shift_msa__i_clk_en    = i_cen      ;
  assign  shift_msa__i_sclr      = ~i_sclr_n  ;

  // serial

  assign  shift_msa__i_sload     = i_str_msa  ;
  assign  shift_msa__i_sload_en  = i_dat_load ;
  assign  shift_msa__i_sdata     = i_dat_body ;

  //------------------------------------------------------------------------------|
                                                  //                              |
  shift_s2p         shift_msa                     //                              |
  (                                               //                              |
  // clock ---------------------------------------//------------------------------|
                                                  //           |                  |
     .i_clk         ( shift_msa__i_clk      ),    //  in       |  req             |
     .i_clk_en      ( shift_msa__i_clk_en   ),    //  in       |       opt        |
     .i_sclr        ( shift_msa__i_sclr     ),    //  in       |       opt        |
                                                  //           |                  |
  // serial --------------------------------------//-----------|------------------|
                                                  //           |                  |
     .i_sload       ( shift_msa__i_sload    ),    //  in       |  req             |
     .i_sload_en    ( shift_msa__i_sload_en ),    //  in       |       opt        |
     .i_sdata       ( shift_msa__i_sdata    ),    //  in       |  req             |
                                                  //           |                  |
  // parallel ------------------------------------//-----------|------------------|
                                                  //           |                  |
     .o_pload       ( shift_msa__o_pload    ),    //      out  |  req             |
     .o_pdata       ( shift_msa__o_pdata    )     //      out  |  req             |
                                                  //           |                  |
  );//----------------------------------------------------------------------------|

  // width of data

  defparam  shift_msa.pW_SDATA  = 8     ; //  8 bit = 1 byte
  defparam  shift_msa.pW_PDATA  = 48    ; // 48 bit = 6 bytes

  // first word

  defparam  shift_msa.pS_FIRST  = "msb" ;

  // auto mode

  defparam  shift_msa.pE_AUTO   = 0     ; // def: 0
  defparam  shift_msa.pT_AUTO   = 0     ; // def: 0

  // signal tap (for tetsing)

  defparam  shift_msa.pE_GNPR   = 0     ; // for testing only

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // data mlt / shift : serial to parallel  [ shift_mlt / shift_s2p ]
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  // clock

  assign  shift_mlt__i_clk       = i_clk      ;
  assign  shift_mlt__i_clk_en    = i_cen      ;
  assign  shift_mlt__i_sclr      = ~i_sclr_n  ;

  // serial

  assign  shift_mlt__i_sload     = i_str_mlt  ;
  assign  shift_mlt__i_sload_en  = i_dat_load ;
  assign  shift_mlt__i_sdata     = i_dat_body ;

  //------------------------------------------------------------------------------|
                                                  //                              |
  shift_s2p         shift_mlt                     //                              |
  (                                               //                              |
  // clock ---------------------------------------//------------------------------|
                                                  //           |                  |
     .i_clk         ( shift_mlt__i_clk      ),    //  in       |  req             |
     .i_clk_en      ( shift_mlt__i_clk_en   ),    //  in       |       opt        |
     .i_sclr        ( shift_mlt__i_sclr     ),    //  in       |       opt        |
                                                  //           |                  |
  // serial --------------------------------------//-----------|------------------|
                                                  //           |                  |
     .i_sload       ( shift_mlt__i_sload    ),    //  in       |  req             |
     .i_sload_en    ( shift_mlt__i_sload_en ),    //  in       |       opt        |
     .i_sdata       ( shift_mlt__i_sdata    ),    //  in       |  req             |
                                                  //           |                  |
  // parallel ------------------------------------//-----------|------------------|
                                                  //           |                  |
     .o_pload       ( shift_mlt__o_pload    ),    //      out  |  req             |
     .o_pdata       ( shift_mlt__o_pdata    )     //      out  |  req             |
                                                  //           |                  |
  );//----------------------------------------------------------------------------|

  // width of data

  defparam  shift_mlt.pW_SDATA  = 8     ; //  8 bit = 1 byte
  defparam  shift_mlt.pW_PDATA  = 16    ; // 16 bit = 2 bytes

  // first word

  defparam  shift_mlt.pS_FIRST  = "msb" ;

  // auto mode

  defparam  shift_mlt.pE_AUTO   = 0     ; // def: 0
  defparam  shift_mlt.pT_AUTO   = 0     ; // def: 0

  // signal tap (for tetsing)

  defparam  shift_mlt.pE_GNPR   = 0     ; // for testing only

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // data ip / shift : serial to parallel  [ shift_ip / shift_s2p ]
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  // clock

  assign  shift_ip__i_clk       = i_clk      ;
  assign  shift_ip__i_clk_en    = i_cen      ;
  assign  shift_ip__i_sclr      = ~i_sclr_n  ;

  // serial

  assign  shift_ip__i_sload     = i_str_ip   ;
  assign  shift_ip__i_sload_en  = i_dat_load ;
  assign  shift_ip__i_sdata     = i_dat_body ;

  //-----------------------------------------------------------------------------|
                                                 //                              |
  shift_s2p         shift_ip                     //                              |
  (                                              //                              |
  // clock --------------------------------------//------------------------------|
                                                 //           |                  |
     .i_clk         ( shift_ip__i_clk      ),    //  in       |  req             |
     .i_clk_en      ( shift_ip__i_clk_en   ),    //  in       |       opt        |
     .i_sclr        ( shift_ip__i_sclr     ),    //  in       |       opt        |
                                                 //           |                  |
  // serial -------------------------------------//-----------|------------------|
                                                 //           |                  |
     .i_sload       ( shift_ip__i_sload    ),    //  in       |  req             |
     .i_sload_en    ( shift_ip__i_sload_en ),    //  in       |       opt        |
     .i_sdata       ( shift_ip__i_sdata    ),    //  in       |  req             |
                                                 //           |                  |
  // parallel -----------------------------------//-----------|------------------|
                                                 //           |                  |
     .o_pload       ( shift_ip__o_pload    ),    //      out  |  req             |
     .o_pdata       ( shift_ip__o_pdata    )     //      out  |  req             |
                                                 //           |                  |
  );//---------------------------------------------------------------------------|

  // width of data

  defparam  shift_ip.pW_SDATA  = 8         ; // 1 byte
  defparam  shift_ip.pW_PDATA  = lpW_RX_IP ; // x bytes

  // first word

  defparam  shift_ip.pS_FIRST  = "msb"     ;

  // auto mode

  defparam  shift_ip.pE_AUTO   = 0         ; // def: 0
  defparam  shift_ip.pT_AUTO   = 0         ; // def: 0

  // signal tap (for tetsing)

  defparam  shift_ip.pE_GNPR   = 0         ; // for testing only

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  
  endmodule
  
  
  

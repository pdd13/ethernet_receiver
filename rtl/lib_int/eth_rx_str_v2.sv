  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Company      : Micran, Department of Telecommunications
  //
  //  Engineer     : Pogodaev Danil
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Library      : lib_ethernet / library of ethernet components
  //
  //  Component    : eth_rx_v2 / receiver of ethernet frames (packets)
  //
  //  File Name    : eth_rx_str_v2 .sv (rtl)
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Description  : Strobe generator and length calculator for ethernet frames (see table 1).
  //
  //  Purpose (1)  : Generate strobes for ethernet frames
  //
  //                  - strob                 / o_str_name [0]
  //                  - strob  / start bit    / o_str_name [1] / width: o_cen
  //                  - strob  / stop bit     / o_str_name [2] / width: o_cen
  //                  - strob  / byte counter / o_cnt_name
  //
  //  Purpose (2)  : Calculate length of ethernet frames
  //
  //                  - length / frame body   / o_len_body
  //                  - length / frame gap    / o_len_gap
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Note (1)     : Structure of ethernet frames (std. 802.3)
  //
  //                  - frame header preamble
  //                  - start-frame delimiter (sfd)
  //                  - destination mac address (mda)
  //                  - source mac address (msa)
  //                  - length of payload / type of protocol (mlt)
  //                  - data payload and pad
  //                  - frame check sequence (fcs) crc-32
  //                  - inter-frame gap
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Table (1)    : Clock manager of ethernet frames for receiver
  //
  //                 |--------------------------------------------------------------------------------------------------|
  //                 |                       |          |                       frequency (period)                      |
  //                 |        setting        |  clock   |---------------------------------------------------------------|
  //                 |                       |          |      10 mbit/s      |     100 mbit/s     |    1000 mbit/s     |
  //                 |=======================|==========|=====================|====================|====================|
  //                 |                       |  rx_clk  |  2.50 mhz (400 ns)  |  25.0 mhz (40 ns)  |  no                |
  //                 |  mii interface:       |----------|---------------------|--------------------|--------------------|
  //                 |                       |  rx_cen  |  1'b1               |  1'b1              |  no                |
  //                 |  pS_IO_TYPE = mii     |----------|---------------------|--------------------|--------------------|
  //                 |                       |  rx_ced  |  1.25 mhz (800 ns)  |  12.5 mhz (80 ns)  |  no                |
  //                 |=======================|==========|=====================|====================|====================|
  //                 |                       |  rx_clk  |  50.00 mhz (20 ns)  |  50.00 mhz (20 ns) |  no                |
  //                 |  rmii interface:      |----------|---------------------|--------------------|--------------------|
  //                 |                       |  rx_cen  |  5.00 mhz (200 ns)  |  1'b1              |  no                |
  //                 |  pS_IO_TYPE = rmii    |----------|---------------------|--------------------|--------------------|
  //                 |                       |  rx_ced  |  1.25 mhz (800 ns)  |  12.5 mhz (80 ns)  |  no                |
  //                 |=======================|==========|=====================|====================|====================|
  //                 |                       |  rx_clk  |  2.50 mhz (400 ns)  |  25.0 mhz (40 ns)  |  125.0 mhz (8 ns)  |
  //                 |  gmii interface:      |----------|---------------------|--------------------|--------------------|
  //                 |                       |  rx_cen  |  1'b1               |  1'b1              |  1'b1              |
  //                 |  pS_IO_TYPE = gmii    |----------|---------------------|--------------------|--------------------|
  //                 |                       |  rx_ced  |  1.25 mhz (800 ns)  |  12.5 mhz (80 ns)  |  1'b1              |
  //                 |=======================|==========|===============================================================|
  //                 |                       |  rx_clk  |  i_cst_clk                                                    |
  //                 |  custom mode:         |----------|---------------------------------------------------------------|
  //                 |                       |  rx_cen  |  pT_CST_CEN = 0 : i_cst_cen                                   |
  //                 |  pS_IO_TYPE = custom  |          |  pT_CST_CEN > 0 : i_cst_clk * pT_CST_CEN                      |
  //                 |                       |----------|---------------------------------------------------------------|
  //                 |                       |  rx_ced  |  pT_CST_CED = 0 : i_cst_ced                                   |
  //                 |                       |          |  pT_CST_CED > 0 : i_cst_clk * pT_CST_CEN * pT_CST_CED         |
  //                 |==================================================================================================|
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Table (2)    : Structure of ethernet frames (std. 802.3) for receiver / see also note (1)
  //
  //                 |------------------------------------------------------------------------------|
  //                 | frame header  |      mac header       |        data       |  fcs  | ethernet |
  //                 |---------------|-----------------------|-------------------|-------| receiver |
  //                 |       8       |          14           |      46-1500      |   4   |          |
  //                 |===============|=======================|===================|=======|==========|
  //                 |  prm  |  sfd  |  mda  |  msa  |  mlt  |  payload  |  pad  |  crc  | strob    |
  //                 |-------|-------|-------|-------|-------|-----------|-------|-------| name     |
  //                 |   7   |   1   |   6   |   6   |   2   |  0-1500   | 0-46  |   4   |          |
  //                 |=======|=======|=======|=======|=======|===========|=======|=======|==========|
  //                 |       |       |       |       |       |           |       |       |          |
  //                 |       |       | ***** | ***** | ***** | ********* | ***** | ***** | str_body |
  //                 |       |       | ***** | ***** | ***** |           |       |       | str_mac  |
  //                 |       |       | ***** |       |       |           |       |       | str_mda  |
  //                 |       |       |       | ***** |       |           |       |       | str_msa  |
  //                 |       |       |       |       | ***** |           |       |       | str_mlt  |
  //                 |       |       |       |       |       | ********* | ***** | ***** | str_pay  | with crc (!)
  //                 |       |       |       |       |       | ***       |       |       | str_ip   |
  //                 |       |       |       |       |       |           |       |       |          |
  //                 |------------------------------------------------------------------------------|
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Include Files
  //  
  //  Category     Type       Name          Ver     Description
  //
  //  lib_global   function   cbit    .sv   1.0.0   bits calculator for input number (round up)
  //  lib_global   rtl        gen_npr .sv   1.0.0   generator of registers with attribute "noprune"
  //  lib_global   rtl        det_se  .sv   1.0.0   detector of single edges for input strobe
  //  lib_global   rtl        det_de  .sv   1.0.0   detector of double edges for input strobe
  //  lib_global   rtl        z_line  .sv   1.0.0   delay line for serial and parallel data
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Revision List
  //
  //  Version   Date       Author
  //
  //  2.0.0     26.09.16   Pogodaev Danil
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  module eth_rx_str_v2 (

  //---- clear --------|
    
         i_sclr_n      ,

  //---- setting ------|

         i_set_cut     ,
         i_set_jumbo   ,

  //---- in packet ----|

  //     clock

         i_clk         ,
         i_cen         ,
         i_ced         ,

  //     data

         i_str_body    ,
         i_dat_body    ,

  //---- out packet ---|

  //     clock

         o_clk         ,
         o_cen         ,
         o_ced         ,

  //     strob

         o_str_body    ,
         o_cnt_body    ,
         o_str_mac     ,
         o_cnt_mac     ,
         o_str_mda     ,
         o_str_msa     ,
         o_str_mlt     ,
         o_str_ip      ,
         o_cnt_ip      ,
         o_str_pay     ,
         o_cnt_pay     ,
         o_str_gap     ,
         o_cnt_gap     ,

  //     data

         o_dat_load    ,
         o_dat_body    ,

  //     length

         o_rdy_body    ,
         o_len_body    ,
         o_rdy_gap     ,
         o_len_gap     ,

  //---- detector -----|

         o_det_p_cut  );

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // include files : functions
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
                                                                   //      |
  `include "cbit.sv"                                               // req  | bits calculator for input number (round up)
                                                                   //      |
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // parameters : setting
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
                                                                   //      |                                            |          |           |
  // packet manipulation ------------------------------------------//------|--------------------------------------------|----------|-----------|------------------------------------------------------------------------------//
                                                                   //      |                                            |          |           |
  parameter bit  pE_RX_CUT    = 0                             ;    // opt  | enable of cut off oversize frames          | on/off   | def: 0    | info: see also 'pV_RX_LEN'   and 'i_set_cut'
  parameter bit  pE_RX_JUMBO  = 0                             ;    // opt  | enable of jumbo-frames support             | on/off   | def: 0    | info: see also 'pV_RX_LEN_J' and 'i_set_jumbo'
                                                                   //      |                                            |          |           |
  // packet structure ---------------------------------------------//------|--------------------------------------------|----------|-----------|------------------------------------------------------------------------------//
                                                                   //      |                                            |          |           |
  // frame body                                                    //------|--------------------------------------------|----------|-----------|------------------------------------------------------------------------------//
                                                                   //      |                                            |          |           |
  parameter int  pW_RX_LEN    = 11                            ;    // req  | width of length / 802.3                    | bit      | def: 11   |
  parameter int  pV_RX_LEN    = 1518                          ;    // req  | value of length / 802.3                    | byte     | def: 1518 | info: used for cut off oversize frames
  parameter int  pW_RX_LEN_J  = 14                            ;    // opt  | width of length / jumbo                    | bit      | def: 14   |
  parameter int  pV_RX_LEN_J  = 9018                          ;    // opt  | value of length / jumbo                    | byte     | def: 9018 | info: used for cut off oversize frames
  parameter int  pW_RX_GAP    = 16                            ;    // opt  | width of gap                               | bit      | def: 16   |
                                                                   //      |                                            |          |           |
  // mac header                                                    //------|--------------------------------------------|----------|-----------|------------------------------------------------------------------------------//
                                                                   //      |                                            |          |           |
  parameter bit  pE_RX_MAC    = 0                             ;    // opt  | enable of mac header                       | on/off   | def: 0    |
                                                                   //      |                                            |          |           |  
  // ip header                                                     //------|--------------------------------------------|----------|-----------|------------------------------------------------------------------------------//
                                                                   //      |                                            |          |           |  
  parameter int  pE_RX_IP     = 0                             ;    // opt  | enable of ip header                        | on/off   | def: 0    |
  parameter int  pN_RX_IPL    = 1                             ;    // opt  | number of first (low) byte in payload      | byte     | def: 1    |
  parameter int  pN_RX_IPH    = 4                             ;    // opt  | number of last (high) byte in payload      | byte     |           |
                                                                   //      |                                            |          |           |
  // in/out registers ---------------------------------------------//------|--------------------------------------------|----------|-----------|------------------------------------------------------------------------------//
                                                                   //      |                                            |          |           |
  parameter int  pN_REG_I     = 1                             ;    // opt  | number of input registers                  | register | def: 1    | info: 1 reg = 1 t.clk
  parameter int  pN_REG_O     = 1                             ;    // opt  | number of output registers                 | register | def: 1    | info: 1 reg = 1 t.clk
                                                                   //      |                                            |          |           |
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // parameters : local (for i/o)
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
                                                                   //      |                                            |          |           |
  // packet structure ---------------------------------------------//------|--------------------------------------------|----------|-----------|------------------------------------------------------------------------------//
                                                                   //      |                                            |          |           |
  // frame body                                                    //------|--------------------------------------------|----------|-----------|------------------------------------------------------------------------------//
                                                                   //      |                                            |          |           |
  parameter int  lpW_RX_LEN   = pE_RX_JUMBO == 1 ?                 // req  | width of length / receiver                 | bit      |           |
                                pW_RX_LEN_J      :                 //      | width of length / jumbo                    | bit      | def: 14   |
                                pW_RX_LEN                     ;    //      | width of length / 802.3                    | bit      | def: 11   |
                                                                   //      |                                            |          |           |
  // ip header                                                     //------|--------------------------------------------|----------|-----------|------------------------------------------------------------------------------//
                                                                   //      |                                            |          |           |
  parameter int  lpN_RX_IPL   = pN_RX_IPL                     ;    // opt  | number of first (low) byte in payload      | byte     | def: 1    |
  parameter int  lpN_RX_IPH   = pN_RX_IPH >= pN_RX_IPL ?           // opt  | number of last (high) byte in payload      | byte     |           |
                                pN_RX_IPH  : pN_RX_IPL        ;    //      |                                            |          |           |
                                                                   //      |                                            |          |           |
  parameter int  lpN_RX_IP    = lpN_RX_IPH <= lpN_RX_IPL ? 1  :    //      |                                            |          |           |
                                lpN_RX_IPH  - lpN_RX_IPL + 1  ;    // opt  | number of bytes                            | byte     |           |
                                                                   //      |                                            |          |           |
  parameter int  lpW_RX_IP    = lpN_RX_IP * 8                 ;    // opt  | number of bits                             | bit      |           |
                                                                   //      |                                            |          |           |
  parameter int  lpW_RX_IP_C  = cbit(1,lpN_RX_IP)             ;    // opt  | width of byte counter                      | bit      |           | info: used for 'o_cnt_ip'
                                                                   //      |                                            |          |           |
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // interface
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
                                                                   //      |                                            |          |           |
  //-------------------------------------// clear -----------------//------|--------------------------------------------|----------|-----------|------------------------------------------------------------------------------//
                                                                   //      |                                            |          |           |
  input  logic                              i_sclr_n          ;    // opt  | sync clear (active low)                    | strob    | def: 1    |
                                                                   //      |                                            |          |           |
  //-------------------------------------// setting ---------------//------|--------------------------------------------|----------|-----------|------------------------------------------------------------------------------//
                                                                   //      |                                            |          |           |
  input  logic                              i_set_cut         ;    // opt  | enable of cut off oversize frames          | strob    | def: 0    | info: see also 'pE_RX_CUT'
                                                                   //      |                                            |          |           |
  input  logic                              i_set_jumbo       ;    // opt  | enable of jumbo-frames support             | strob    | def: 0    | info: see also 'pE_RX_JUMBO'
                                                                   //      |                                            |          |           |
  //-------------------------------------// in packet -------------//------|--------------------------------------------|----------|-----------|------------------------------------------------------------------------------//
                                                                   //      |                                            |          |           |
  //                                        clock                  //------|--------------------------------------------|----------|-----------|------------------------------------------------------------------------------//
                                                                   //      |                                            |          |           |
  input  logic                              i_clk             ;    // req  | clock                                      | clock    |           |
  input  logic                              i_cen             ;    // req  | clock enable                               | pulse    |           | width: i_clk
  input  logic                              i_ced             ;    // req  | clock enable for data                      | pulse    |           | width: i_cen
                                                                   //      |                                            |          |           |
  //                                        data                   //------|--------------------------------------------|----------|-----------|------------------------------------------------------------------------------//
                                                                   //      |                                            |          |           |
  input  logic                              i_str_body        ;    // req  | frame body / strob                         | strob    |           | info: without frame header (!)
  input  logic    [               7 :0 ]    i_dat_body        ;    // req  | frame body / data (byte)                   | pdata    |           | info: for latch used 'i_ced'
                                                                   //      |                                            |          |           |
  //-------------------------------------// out packet ------------//------|--------------------------------------------|----------|-----------|------------------------------------------------------------------------------//
                                                                   //      |                                            |          |           |
  //                                        clock                  //------|--------------------------------------------|----------|-----------|------------------------------------------------------------------------------//
                                                                   //      |                                            |          |           |
  output logic                              o_clk             ;    // req  | clock                                      | clock    |           |
  output logic                              o_cen             ;    // req  | clock enable                               | pulse    |           | width: o_clk
  output logic                              o_ced             ;    // req  | clock enable for data                      | pulse    |           | width: o_cen
                                                                   //      |                                            |          |           |
  //                                        strob                  //------|--------------------------------------------|----------|-----------|------------------------------------------------------------------------------//
                                                                   //      |                                            |          |           |
  output logic    [               2 :0 ]    o_str_body        ;    // req  | frame body               / strob full      | strob    |           | info : see purpose (1)
  output logic    [ lpW_RX_LEN   -1 :0 ]    o_cnt_body        ;    // opt  | frame body               / strob count     | count up |           |
  output logic    [               2 :0 ]    o_str_mac         ;    // opt  | mac header               / strob full      | strob    |           |
  output logic    [               3 :0 ]    o_cnt_mac         ;    // opt  | mac header               / strob count     | count up |           |
  output logic    [               2 :0 ]    o_str_mda         ;    // opt  | mac destination address  / strob full      | strob    |           |
  output logic    [               2 :0 ]    o_str_msa         ;    // opt  | mac source address       / strob full      | strob    |           |
  output logic    [               2 :0 ]    o_str_mlt         ;    // opt  | mac length or type       / strob full      | strob    |           |
  output logic    [               2 :0 ]    o_str_ip          ;    // opt  | ip header                / strob full      | strob    |           |
  output logic    [ lpW_RX_IP_C  -1 :0 ]    o_cnt_ip          ;    // opt  | ip header                / strob count     | count up |           |
  output logic    [               2 :0 ]    o_str_pay         ;    // opt  | payload & pad            / strob full      | strob    |           | info: with crc strob (!) ; see table (2)
  output logic    [ lpW_RX_LEN   -1 :0 ]    o_cnt_pay         ;    // opt  | payload & pad            / strob count     | count up |           |
  output logic    [               2 :0 ]    o_str_gap         ;    // opt  | inter-frame gap          / strob full      | strob    |           |
  output logic    [ pW_RX_GAP    -1 :0 ]    o_cnt_gap         ;    // opt  | inter-frame gap          / strob count     | count up |           |
                                                                   //      |                                            |          |           |
  //                                        data                   //------|--------------------------------------------|----------|-----------|------------------------------------------------------------------------------//
                                                                   //      |                                            |          |           |
  output logic                              o_dat_load        ;    // req  | frame body               / load (byte)     | pulse    |           | width: o_cen
  output logic    [               7 :0 ]    o_dat_body        ;    // req  | frame body               / data (byte)     | pdata    |           |
                                                                   //      |                                            |          |           |
  //                                        length                 //------|--------------------------------------------|----------|-----------|------------------------------------------------------------------------------//
                                                                   //      |                                            |          |           |
  output logic                              o_rdy_body        ;    // req  | frame body               / ready           | pulse    |           | width: o_cen ; info: used for latch 'o_len_body'
  output logic    [ lpW_RX_LEN   -1 :0 ]    o_len_body        ;    // req  | frame body               / length (byte)   | pdata    |           |
  output logic                              o_rdy_gap         ;    // opt  | inter-frame gap          / ready           | pulse    |           | width: o_cen ; info: used for latch 'o_len_gap'
  output logic    [ pW_RX_GAP    -1 :0 ]    o_len_gap         ;    // opt  | inter-frame gap          / length (byte)   | pdata    |           |
                                                                   //      |                                            |          |           |
  //-------------------------------------// detector --------------//------|--------------------------------------------|----------|-----------|------------------------------------------------------------------------------//
                                                                   //      |                                            |          |           |
  output logic                              o_det_p_cut       ;    // opt  | detector of cut off oversize frame         | pulse    |           | width: o_cen
                                                                   //      |                                            |          |           |
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // logic
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  //-------------------------------------// in logic --------------| [ after in reg ]

  var    logic                              ri_str_body            ;
  var    logic    [               7 :0 ]    ri_dat_body            ;

  //-------------------------------------// out logic -------------| [ after out reg ]

  //                                        clock

  var    logic                              ro_cen                 ;
  var    logic                              ro_ced                 ;

  //                                        strob

  var    logic    [               2 :0 ]    ro_str_body            ;
  var    logic    [ lpW_RX_LEN   -1 :0 ]    ro_cnt_body            ;
  var    logic    [               2 :0 ]    ro_str_mac             ;
  var    logic    [               3 :0 ]    ro_cnt_mac             ;
  var    logic    [               2 :0 ]    ro_str_mda             ;
  var    logic    [               2 :0 ]    ro_str_msa             ;
  var    logic    [               2 :0 ]    ro_str_mlt             ;
  var    logic    [               2 :0 ]    ro_str_ip              ;
  var    logic    [ lpW_RX_IP_C  -1 :0 ]    ro_cnt_ip              ;
  var    logic    [               2 :0 ]    ro_str_pay             ;
  var    logic    [ lpW_RX_LEN   -1 :0 ]    ro_cnt_pay             ;
  var    logic    [               2 :0 ]    ro_str_gap             ;
  var    logic    [ pW_RX_GAP    -1 :0 ]    ro_cnt_gap             ;

  //                                        data

  var    logic                              ro_dat_load            ;
  var    logic    [               7 :0 ]    ro_dat_body            ;

  //                                        length

  var    logic                              ro_rdy_body            ;
  var    logic    [ lpW_RX_LEN   -1 :0 ]    ro_len_body            ;
  var    logic                              ro_rdy_gap             ;
  var    logic    [ pW_RX_GAP    -1 :0 ]    ro_len_gap             ;

  //                                        detector

  var    logic                              ro_det_p_cut           ;

  //-------------------------------------// out logic -------------| [ before out reg ]

  //                                        clock

  var    logic                              no_clk                 ;
  var    logic                              no_cen                 ;
  var    logic                              no_ced                 ;

  //                                        strob

  var    logic    [               2 :0 ]    no_str_body            ;
  var    logic    [ lpW_RX_LEN   -1 :0 ]    no_cnt_body            ;
  var    logic    [               2 :0 ]    no_str_mac             ;
  var    logic    [               3 :0 ]    no_cnt_mac             ;
  var    logic    [               2 :0 ]    no_str_mda             ;
  var    logic    [               2 :0 ]    no_str_msa             ;
  var    logic    [               2 :0 ]    no_str_mlt             ;
  var    logic    [               2 :0 ]    no_str_ip              ;
  var    logic    [ lpW_RX_IP_C  -1 :0 ]    no_cnt_ip              ;
  var    logic    [               2 :0 ]    no_str_pay             ;
  var    logic    [ lpW_RX_LEN   -1 :0 ]    no_cnt_pay             ;
  var    logic    [               2 :0 ]    no_str_gap             ;
  var    logic    [ pW_RX_GAP    -1 :0 ]    no_cnt_gap             ;

  //                                        data

  var    logic                              no_dat_load            ;
  var    logic    [               7 :0 ]    no_dat_body            ;

  //                                        length

  var    logic                              no_rdy_body            ;
  var    logic    [ lpW_RX_LEN   -1 :0 ]    no_len_body            ;
  var    logic                              no_rdy_gap             ;
  var    logic    [ pW_RX_GAP    -1 :0 ]    no_len_gap             ;

  //                                        detector

  var    logic                              no_det_p_cut           ;

  //-------------------------------------// settings --------------|

  var    logic                              set_cut                ;
  var    logic                              set_jumbo              ;

  //-------------------------------------// str body / edge -------|

  var    logic                              ri_str_body_re         ;
  var    logic                              ri_str_body_fe         ;

  //-------------------------------------// str strob -------------|

  var    logic    [               2 :0 ]    str_body           = 0 ;
  var    logic    [ lpW_RX_LEN   -1 :0 ]    cnt_body           = 0 ;

  //-------------------------------------// str body / cut --------|

  var    logic                              cut_off_str        = 0 ;
  var    logic                              cut_off_det            ;
  var    logic                              cut_off_det_j0     = 0 ;
  var    logic                              cut_off_det_j1     = 0 ;
  var    logic                              cut_off_det_nxt    = 0 ;
  var    logic                              cut_off_det_nxt_re     ;

  //-------------------------------------// str gap ---------------|

  var    logic    [               2 :0 ]    str_gap            = 0 ;
  var    logic    [ pW_RX_GAP    -1 :0 ]    cnt_gap            = 0 ;
  var    logic                              cnt_gap_full       = 0 ;

  //-------------------------------------// str mac header --------|

  var    logic                              cnt_mac_5          = 0 ;
  var    logic                              cnt_mac_11         = 0 ;
  var    logic                              cnt_mac_13         = 0 ;

  var    logic    [               2 :0 ]    str_mac            = 0 ;
  var    logic    [               2 :0 ]    str_mda            = 0 ;
  var    logic    [               2 :0 ]    str_msa            = 0 ;
  var    logic    [               2 :0 ]    str_mlt            = 0 ;
  var    logic    [               3 :0 ]    cnt_mac            = 0 ;

  //-------------------------------------// str payload -----------|

  var    logic    [               2 :0 ]    str_pay            = 0 ;
  var    logic    [ lpW_RX_LEN   -1 :0 ]    cnt_pay            = 0 ;

  //-------------------------------------// str ip header ---------|

  var    logic    [               2 :0 ]    str_ip             = 0 ;
  var    logic    [ lpW_RX_IP_C  -1 :0 ]    cnt_ip             = 0 ;

  //-------------------------------------// data body -------------|

  var    logic    [               7 :0 ]    dat_body           = 0 ;
  var    logic                              dat_load               ;

  //-------------------------------------// length body -----------|

  var    logic    [ lpW_RX_LEN   -1 :0 ]    len_body           = 0 ;
  var    logic                              rdy_body           = 0 ;
  var    logic    [ pW_RX_GAP    -1 :0 ]    len_gap            = 0 ;
  var    logic                              rdy_gap            = 0 ;

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  //==========================================================================================================================================================================================================================//
  // body of module
  //==========================================================================================================================================================================================================================//

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // out logic : output (after out reg)
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  // clock

  assign  o_clk       =  no_clk      ; // not 'ro' / clock repeat
  assign  o_cen       =  ro_cen      ;
  assign  o_ced       =  ro_ced      ;

  // strob

  assign  o_str_body  = ro_str_body  ;
  assign  o_cnt_body  = ro_cnt_body  ;
  assign  o_str_mac   = ro_str_mac   ;
  assign  o_cnt_mac   = ro_cnt_mac   ;
  assign  o_str_mda   = ro_str_mda   ;
  assign  o_str_msa   = ro_str_msa   ;
  assign  o_str_mlt   = ro_str_mlt   ;
  assign  o_str_ip    = ro_str_ip    ;
  assign  o_cnt_ip    = ro_cnt_ip    ;
  assign  o_str_pay   = ro_str_pay   ;
  assign  o_cnt_pay   = ro_cnt_pay   ;
  assign  o_str_gap   = ro_str_gap   ;
  assign  o_cnt_gap   = ro_cnt_gap   ;

  // data

  assign  o_dat_load  = ro_dat_load  ;
  assign  o_dat_body  = ro_dat_body  ;

  // length

  assign  o_rdy_body  = ro_rdy_body  ;
  assign  o_len_body  = ro_len_body  ;
  assign  o_rdy_gap   = ro_rdy_gap   ;
  assign  o_len_gap   = ro_len_gap   ;

  // detector

  assign  o_det_p_cut = ro_det_p_cut ;

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // out logic : next to output (before out reg)
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  // clock

  assign  no_clk       = i_clk       ;
  assign  no_cen       = i_cen       ;
  assign  no_ced       = i_ced       ;

  // strob

  assign  no_str_body  = str_body    ;
  assign  no_cnt_body  = cnt_body    ;
  assign  no_str_mac   = str_mac     ;
  assign  no_cnt_mac   = cnt_mac     ;
  assign  no_str_mda   = str_mda     ;
  assign  no_str_msa   = str_msa     ;
  assign  no_str_mlt   = str_mlt     ;
  assign  no_str_ip    = str_ip      ;
  assign  no_cnt_ip    = cnt_ip      ;
  assign  no_str_pay   = str_pay     ;
  assign  no_cnt_pay   = cnt_pay     ;
  assign  no_str_gap   = str_gap     ;
  assign  no_cnt_gap   = cnt_gap     ;

  // data

  assign  no_dat_load  = dat_load    ;
  assign  no_dat_body  = dat_body    ;

  // length

  assign  no_rdy_body  = rdy_body    ;
  assign  no_len_body  = len_body    ;
  assign  no_rdy_gap   = rdy_gap     ;
  assign  no_len_gap   = len_gap     ;

  // detector

  assign  no_det_p_cut = cut_off_det & i_ced ; // width = cen 

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // settings
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  assign  set_cut   = (pE_RX_CUT   == 1) & (i_set_cut   == 1);
  assign  set_jumbo = (pE_RX_JUMBO == 1) & (i_set_jumbo == 1);

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // strob : frame body / edge detector
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  det_de // detector of double edges

  #(.pN_REG_I (0), // number of reg input  | bit
    .pN_REG_O (0)) // number of reg output | bit

  de__ri_str_body (.i_clk(i_clk), .i_clk_en(i_cen & i_ced), .i_sclr(~i_sclr_n), .i_strob(ri_str_body), .o_edge_re(ri_str_body_re), .o_edge_fe(ri_str_body_fe) );
  //                                                                                                              ^                           ^
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // strob : frame body
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  // strob

  always_ff @(posedge i_clk)
    if (~i_sclr_n)
      str_body[0] <= 1'b0;
    else
      if (i_cen)
        begin
          if (i_ced) // period = 1 byte
            begin
              str_body[0] <= (ri_str_body == 1) & (cut_off_det == 0) &
                                                  (cut_off_str == 0) ;
            end
        end

  // start & stop bits / width = cen

  always_comb
    str_body[1] <= i_ced & ri_str_body_re;

  always_comb
    str_body[2] <= i_ced & ((ri_str_body_fe & ~cut_off_str) |
                            (ri_str_body    &  cut_off_det) ); // cut off oversize frames
  // strob counter up

  always_ff @(posedge i_clk)
    if (~i_sclr_n)
      cnt_body <= 0;
    else
      if (i_cen)
        begin
          if (i_ced) // period = 1 byte
            begin
              if (str_body[1]) // start bit
                cnt_body <= 1;
              else
                begin
                  if (ri_str_body)
                    cnt_body <= cnt_body + 1;
                  else
                    cnt_body <= 0;
                end
            end
        end

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // strob : frame body / cut off (for oversize frames)
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  // cut off / detector

  always_ff @(posedge i_clk)
    if (~i_sclr_n)
      begin
        cut_off_det_j0 <= 0;
        cut_off_det_j1 <= 0;
      end
    else
      if (i_cen)
        begin
          if (i_ced) // period = 1 byte
            begin
              cut_off_det_j0 <= (cnt_body > (pV_RX_LEN   - 3)); // 'minus' used for prediction of cut off
              cut_off_det_j1 <= (cnt_body > (pV_RX_LEN_J - 3)); // 'minus' used for prediction of cut off
            end
        end

  always_ff @(posedge i_clk)
    if (~i_sclr_n)
      cut_off_det_nxt <= 1'b0;
    else
      if (i_cen)
        begin
          if (i_ced) // period = 1 byte
            begin
              if (set_cut)
                begin
                  if (set_jumbo == 0) cut_off_det_nxt <= cut_off_det_j0; else
                  if (set_jumbo == 1) cut_off_det_nxt <= cut_off_det_j1;
                end
             else
                cut_off_det_nxt <= 1'b0;
            end
        end

  assign  cut_off_det = cut_off_det_nxt_re & (ri_str_body == 1); // width = ced

  det_se // detector of rising edge

  #(.pS_EDGE  (1), // edge setting
    .pN_REG_I (0), // number of reg in  | register
    .pN_REG_O (0)) // number of reg out | register

  re__cut_off_det_nxt (.i_clk(i_clk), .i_clk_en(i_cen & i_ced), .i_sclr(~i_sclr_n), .i_strob(cut_off_det_nxt), .o_edge(cut_off_det_nxt_re) );
  //                                                                                         ^                         ^
  // cut off / strob

  always_ff @(posedge i_clk)
    if (~i_sclr_n)
      cut_off_str <= 1'b0;
    else
      begin
        if (i_cen)
          begin
            if (i_ced) // period = 1 byte
              begin
                if (cut_off_det)
                  cut_off_str <= 1'b1;
                else
                  if (ri_str_body == 0)
                    cut_off_str <= 1'b0;
              end
          end
      end

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // strob : frame gap
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  // start & stop bits / width = cen

  always_comb
    begin
      str_gap[1] <= str_body[2];
      str_gap[2] <= str_body[1];
    end

  // strob

  always_ff @(posedge i_clk)
    if (~i_sclr_n)
      str_gap[0] <= 1'b0;
    else
      if (i_cen)
        begin
          if (str_gap[1]) str_gap[0] <= 1'b1; else
          if (str_gap[2]) str_gap[0] <= 1'b0;
        end

  // strob counter up

  always_ff @(posedge i_clk)
    if (~i_sclr_n)
      cnt_gap <= 0;
    else
      if (i_cen)
        begin
          if (i_ced) // period = 1 byte
            begin
              if (str_gap[1]) // start bit
                cnt_gap <= 1;
              else
                begin
                  if (str_gap[0] & ~str_gap[2]) // strob and not stop bit
                    begin
                      if (cnt_gap_full == 0)
                        cnt_gap <= cnt_gap + 1;
                    end
                  else
                    cnt_gap <= 0;
                end
            end
        end

  always_ff @(posedge i_clk)
    if (~i_sclr_n)
      cnt_gap_full <= 1'b0;
    else
      if (i_cen)
        begin
          if (i_ced) // period = 1 byte
            begin
              cnt_gap_full <= (&cnt_gap[pW_RX_GAP-1:1]); // -1
            end
        end

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // strob : mac header
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  // start & stop bits / width = cen

  always_ff @(posedge i_clk)
    if (~i_sclr_n)
      begin
        cnt_mac_5  <= 1'b0;
        cnt_mac_11 <= 1'b0;
        cnt_mac_13 <= 1'b0;
      end
    else
      if (i_cen)
        begin
          if (i_ced) // period = 1 byte
            begin
              cnt_mac_5  <= (cnt_mac ==  5);
              cnt_mac_11 <= (cnt_mac == 11);
              cnt_mac_13 <= (cnt_mac == 13);
            end
        end

  always_comb
    if (pE_RX_MAC == 0)
      begin
        str_mac [1] <= 1'b0;
        str_mac [2] <= 1'b0;
        str_mda [1] <= 1'b0;
        str_mda [2] <= 1'b0;
        str_msa [1] <= 1'b0;
        str_msa [2] <= 1'b0;
        str_mlt [1] <= 1'b0;
        str_mlt [2] <= 1'b0;
      end
    else
      begin
        str_mac [1] <= str_body[1]        ;
        str_mac [2] <= cnt_mac_13 & i_ced ;
        str_mda [1] <= str_body[1]        ;
        str_mda [2] <= cnt_mac_5  & i_ced ;
        str_msa [1] <= cnt_mac_5  & i_ced ;
        str_msa [2] <= cnt_mac_11 & i_ced ;
        str_mlt [1] <= cnt_mac_11 & i_ced ;
        str_mlt [2] <= cnt_mac_13 & i_ced ;
      end

  // strob

  always_ff @(posedge i_clk)
    if (~i_sclr_n)
      begin
        str_mac [0] <= 1'b0;
        str_mda [0] <= 1'b0;
        str_msa [0] <= 1'b0;
        str_mlt [0] <= 1'b0;
      end
    else
      if (i_cen)
        begin
          if (pE_RX_MAC == 0)
            begin
              str_mac [0] <= 1'b0;
              str_mda [0] <= 1'b0;
              str_msa [0] <= 1'b0;
              str_mlt [0] <= 1'b0;
            end
          else
            begin
                if (str_mac [1]) str_mac [0] <= 1'b1; else
                if (str_mac [2]) str_mac [0] <= 1'b0;
                if (str_mda [1]) str_mda [0] <= 1'b1; else
                if (str_mda [2]) str_mda [0] <= 1'b0;
                if (str_msa [1]) str_msa [0] <= 1'b1; else
                if (str_msa [2]) str_msa [0] <= 1'b0;
                if (str_mlt [1]) str_mlt [0] <= 1'b1; else
                if (str_mlt [2]) str_mlt [0] <= 1'b0;
            end
        end

  // strob counter up

  always_ff @(posedge i_clk)
    if (~i_sclr_n)
      cnt_mac <= 0;
    else
      if (i_cen)
        begin
          if (i_ced) // period = 1 byte
            begin
              if (str_mac[1]) // start bit
                cnt_mac <= 1;
              else
                begin
                  if (str_mac[0] & ~str_mac[2]) // strob and not stop bit
                    cnt_mac <= cnt_mac + 1;
                  else
                    cnt_mac <= 0;
                end
            end
        end

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // strob : data payload & crc (!)
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  // start & stop bits / width = cen

  always_comb
    str_pay[1] <= (pE_RX_MAC == 1) ? str_mac[2] : str_body[1];

  always_comb
    str_pay[2] <= str_body[2];

  // strob

  always_ff @(posedge i_clk)
    if (~i_sclr_n)
      str_pay[0] <= 1'b0;
    else
      if (i_cen)
        begin
          if (i_ced) // period = 1 byte
            begin
              if (str_pay[1]) str_pay[0] <= 1'b1; else
              if (str_pay[2]) str_pay[0] <= 1'b0;
            end
        end

  // strob counter up
  
  always_ff @(posedge i_clk)
    if (~i_sclr_n)
      cnt_pay <= 0;
    else
      if (i_cen)
        begin
          if (i_ced) // period = 1 byte
            begin
              if (str_pay[1]) // start bit
                cnt_pay <= 1;
              else
                begin
                  if (str_pay[0] & ~str_pay[2]) // strob and not stop bit
                    cnt_pay <= cnt_pay + 1;
                  else
                    cnt_pay <= 0;
                end
            end
        end

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // strob : ip header
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  // start & stop bit / width = cen
  
  always_comb
    begin
      str_ip[1] <= (pE_RX_IP == 1) & i_ced & ((pN_RX_IPL == 1) ? str_pay[1]   : (cnt_pay == pN_RX_IPL-1));
      str_ip[2] <= (pE_RX_IP == 1) & i_ced & ((pN_RX_IPH == 1) ? cnt_pay == 1 : (cnt_pay == pN_RX_IPH  ));
    end

  // strob

  always_ff @(posedge i_clk)
    if (~i_sclr_n)
      str_ip[0] <= 1'b0;
    else
      if (i_cen)
        begin
          if (i_ced) // period = 1 byte
            begin
              if (str_ip[1]) // start bit
                str_ip[0] <= 1'b1;
              else
                if (str_ip[2]) // stop bit
                  str_ip[0] <= 1'b0;
            end
        end

  // strob counter up

  always_ff @(posedge i_clk)
    if (~i_sclr_n)
      cnt_ip <= 0;
    else
      if (i_cen)
        begin
          if (i_ced) // period = 1 byte
            begin
              if (str_ip[1]) // start bit
                cnt_ip <= 1;
              else
                begin
                  if (str_ip[0] & ~str_ip[2]) // strob and not stop bit
                    cnt_ip <= cnt_ip + 1;
                  else
                    cnt_ip <= 0;
                end
            end
        end

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // data : frame body (sync to frame)
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  always_ff @(posedge i_clk)
    if (~i_sclr_n)
      dat_body <= 0;
    else
      if (i_cen)
        begin
          if (i_ced) // period = 1 byte
            begin
              dat_body <= ri_dat_body; // delay = 1 ced
            end
        end

  assign  dat_load = i_ced & str_body;

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // length : frame body & gap
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  always_ff @(posedge i_clk)
    if (~i_sclr_n)
      begin
        len_body <= 0;
        len_gap  <= 0;
      end
    else
      if (i_cen)
        begin
          if (str_body[2]) len_body <= cnt_body     ;
                           rdy_body <= str_body [2] ; // stop bit

          if (str_body[1]) len_gap  <= cnt_gap      ;
                           rdy_gap  <= str_body [1] ; // start bit
        end

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // in/out registers : befor reg (i/no) / after reg (ri/ro)
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  // in packet -----------------------------------------// in packet ---------------------------------------------------------//

  z_line #(.pV_DELAY (pN_REG_I), .pW_DATA (1)           )  ri__str_body  ( i_clk , 1'b1, ~i_sclr_n, i_str_body  , ri_str_body );
  z_line #(.pV_DELAY (pN_REG_I), .pW_DATA (8)           )  ri__dat_body  ( i_clk , 1'b1, ~i_sclr_n, i_dat_body  , ri_dat_body );
  //                                                                                                ^             ^
  // out packet ----------------------------------------// out packet --------------------------------------------------------//
  
  // clock                                                 clock

  z_line #(.pV_DELAY (pN_REG_O), .pW_DATA (1)           )  ro__cen       ( o_clk , 1'b1, ~i_sclr_n, no_cen      , ro_cen      );
  z_line #(.pV_DELAY (pN_REG_O), .pW_DATA (1)           )  ro__ced       ( o_clk , 1'b1, ~i_sclr_n, no_ced      , ro_ced      );
  //                                                                                                ^             ^
  // strob                                                 strob

  z_line #(.pV_DELAY (pN_REG_O), .pW_DATA (3)           )  ro__str_body  ( o_clk , 1'b1, ~i_sclr_n, no_str_body , ro_str_body );
  z_line #(.pV_DELAY (pN_REG_O), .pW_DATA (lpW_RX_LEN)  )  ro__cnt_body  ( o_clk , 1'b1, ~i_sclr_n, no_cnt_body , ro_cnt_body );
  z_line #(.pV_DELAY (pN_REG_O), .pW_DATA (3)           )  ro__str_mac   ( o_clk , 1'b1, ~i_sclr_n, no_str_mac  , ro_str_mac  );
  z_line #(.pV_DELAY (pN_REG_O), .pW_DATA (4)           )  ro__cnt_mac   ( o_clk , 1'b1, ~i_sclr_n, no_cnt_mac  , ro_cnt_mac  );
  z_line #(.pV_DELAY (pN_REG_O), .pW_DATA (3)           )  ro__str_mda   ( o_clk , 1'b1, ~i_sclr_n, no_str_mda  , ro_str_mda  );
  z_line #(.pV_DELAY (pN_REG_O), .pW_DATA (3)           )  ro__str_msa   ( o_clk , 1'b1, ~i_sclr_n, no_str_msa  , ro_str_msa  );
  z_line #(.pV_DELAY (pN_REG_O), .pW_DATA (3)           )  ro__str_mlt   ( o_clk , 1'b1, ~i_sclr_n, no_str_mlt  , ro_str_mlt  );
  z_line #(.pV_DELAY (pN_REG_O), .pW_DATA (3)           )  ro__str_ip    ( o_clk , 1'b1, ~i_sclr_n, no_str_ip   , ro_str_ip   );
  z_line #(.pV_DELAY (pN_REG_O), .pW_DATA (lpW_RX_IP_C) )  ro__cnt_ip    ( o_clk , 1'b1, ~i_sclr_n, no_cnt_ip   , ro_cnt_ip   );
  z_line #(.pV_DELAY (pN_REG_O), .pW_DATA (3)           )  ro__str_pay   ( o_clk , 1'b1, ~i_sclr_n, no_str_pay  , ro_str_pay  );
  z_line #(.pV_DELAY (pN_REG_O), .pW_DATA (lpW_RX_LEN)  )  ro__cnt_pay   ( o_clk , 1'b1, ~i_sclr_n, no_cnt_pay  , ro_cnt_pay  );
  z_line #(.pV_DELAY (pN_REG_O), .pW_DATA (3)           )  ro__str_gap   ( o_clk , 1'b1, ~i_sclr_n, no_str_gap  , ro_str_gap  );
  z_line #(.pV_DELAY (pN_REG_O), .pW_DATA (pW_RX_GAP)   )  ro__cnt_gap   ( o_clk , 1'b1, ~i_sclr_n, no_cnt_gap  , ro_cnt_gap  );
  //                                                                                                ^             ^
  // data                                                  data

  z_line #(.pV_DELAY (pN_REG_O), .pW_DATA (1)           )  ro__dat_load  ( o_clk , 1'b1, ~i_sclr_n, no_dat_load , ro_dat_load );
  z_line #(.pV_DELAY (pN_REG_O), .pW_DATA (8)           )  ro__dat_body  ( o_clk , 1'b1, ~i_sclr_n, no_dat_body , ro_dat_body );
  //                                                                                                ^             ^
  // length                                                length

  z_line #(.pV_DELAY (pN_REG_O), .pW_DATA (1)           )  ro__rdy_body  ( o_clk , 1'b1, ~i_sclr_n, no_rdy_body , ro_rdy_body );
  z_line #(.pV_DELAY (pN_REG_O), .pW_DATA (lpW_RX_LEN)  )  ro__len_body  ( o_clk , 1'b1, ~i_sclr_n, no_len_body , ro_len_body );
  z_line #(.pV_DELAY (pN_REG_O), .pW_DATA (1)           )  ro__rdy_gap   ( o_clk , 1'b1, ~i_sclr_n, no_rdy_gap  , ro_rdy_gap  );
  z_line #(.pV_DELAY (pN_REG_O), .pW_DATA (pW_RX_GAP)   )  ro__len_gap   ( o_clk , 1'b1, ~i_sclr_n, no_len_gap  , ro_len_gap  );
  //                                                                                                ^             ^
  // detector ------------------------------------------// detector ----------------------------------------------------------//

  z_line #(.pV_DELAY (pN_REG_O), .pW_DATA (1)           )  ro__det_p_cut ( o_clk , 1'b1, ~i_sclr_n, no_det_p_cut, ro_det_p_cut);
  //                                                                                                ^             ^
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  
  endmodule
  
  
  

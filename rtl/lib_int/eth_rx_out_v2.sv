  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Company      : Micran, Department of Telecommunications
  //
  //  Engineer     : Pogodaev Danil
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Library      : lib_ethernet / library of ethernet components
  //
  //  Component    : eth_rx_v2 / receiver of ethernet frames (packets)
  //
  //  File Name    : eth_rx_out_v2 .sv (rtl)
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Description  : Output registers and latching data (mac and ip headers).
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Table (1)    : Clock manager of ethernet frames for receiver
  //
  //                 |--------------------------------------------------------------------------------------------------|
  //                 |                       |          |                       frequency (period)                      |
  //                 |        setting        |  clock   |---------------------------------------------------------------|
  //                 |                       |          |      10 mbit/s      |     100 mbit/s     |    1000 mbit/s     |
  //                 |=======================|==========|=====================|====================|====================|
  //                 |                       |  rx_clk  |  2.50 mhz (400 ns)  |  25.0 mhz (40 ns)  |  no                |
  //                 |  mii interface:       |----------|---------------------|--------------------|--------------------|
  //                 |                       |  rx_cen  |  1'b1               |  1'b1              |  no                |
  //                 |  pS_IO_TYPE = mii     |----------|---------------------|--------------------|--------------------|
  //                 |                       |  rx_ced  |  1.25 mhz (800 ns)  |  12.5 mhz (80 ns)  |  no                |
  //                 |=======================|==========|=====================|====================|====================|
  //                 |                       |  rx_clk  |  50.00 mhz (20 ns)  |  50.00 mhz (20 ns) |  no                |
  //                 |  rmii interface:      |----------|---------------------|--------------------|--------------------|
  //                 |                       |  rx_cen  |  5.00 mhz (200 ns)  |  1'b1              |  no                |
  //                 |  pS_IO_TYPE = rmii    |----------|---------------------|--------------------|--------------------|
  //                 |                       |  rx_ced  |  1.25 mhz (800 ns)  |  12.5 mhz (80 ns)  |  no                |
  //                 |=======================|==========|=====================|====================|====================|
  //                 |                       |  rx_clk  |  2.50 mhz (400 ns)  |  25.0 mhz (40 ns)  |  125.0 mhz (8 ns)  |
  //                 |  gmii interface:      |----------|---------------------|--------------------|--------------------|
  //                 |                       |  rx_cen  |  1'b1               |  1'b1              |  1'b1              |
  //                 |  pS_IO_TYPE = gmii    |----------|---------------------|--------------------|--------------------|
  //                 |                       |  rx_ced  |  1.25 mhz (800 ns)  |  12.5 mhz (80 ns)  |  1'b1              |
  //                 |=======================|==========|===============================================================|
  //                 |                       |  rx_clk  |  i_cst_clk                                                    |
  //                 |  custom mode:         |----------|---------------------------------------------------------------|
  //                 |                       |  rx_cen  |  pT_CST_CEN = 0 : i_cst_cen                                   |
  //                 |  pS_IO_TYPE = custom  |          |  pT_CST_CEN > 0 : i_cst_clk * pT_CST_CEN                      |
  //                 |                       |----------|---------------------------------------------------------------|
  //                 |                       |  rx_ced  |  pT_CST_CED = 0 : i_cst_ced                                   |
  //                 |                       |          |  pT_CST_CED > 0 : i_cst_clk * pT_CST_CEN * pT_CST_CED         |
  //                 |==================================================================================================|
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Table (2)    : Structure of ethernet frames (std. 802.3) for receiver / see also note (1)
  //
  //                 |------------------------------------------------------------------------------|
  //                 | frame header  |      mac header       |        data       |  fcs  | ethernet |
  //                 |---------------|-----------------------|-------------------|-------| receiver |
  //                 |       8       |          14           |      46-1500      |   4   |          |
  //                 |===============|=======================|===================|=======|==========|
  //                 |  prm  |  sfd  |  mda  |  msa  |  mlt  |  payload  |  pad  |  crc  | strob    |
  //                 |-------|-------|-------|-------|-------|-----------|-------|-------| name     |
  //                 |   7   |   1   |   6   |   6   |   2   |  0-1500   | 0-46  |   4   |          |
  //                 |=======|=======|=======|=======|=======|===========|=======|=======|==========|
  //                 |       |       |       |       |       |           |       |       |          |
  //                 |       |       | ***** | ***** | ***** | ********* | ***** | ***** | str_body |
  //                 |       |       | ***** | ***** | ***** |           |       |       | str_mac  |
  //                 |       |       | ***** |       |       |           |       |       | str_mda  |
  //                 |       |       |       | ***** |       |           |       |       | str_msa  |
  //                 |       |       |       |       | ***** |           |       |       | str_mlt  |
  //                 |       |       |       |       |       | ********* | ***** | ***** | str_pay  | with crc (!)
  //                 |       |       |       |       |       | ***       |       |       | str_ip   |
  //                 |       |       |       |       |       |           |       |       |          |
  //                 |------------------------------------------------------------------------------|
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Include Files
  //  
  //  Category     Type       Name          Ver     Description
  //
  //  lib_global   function   cbit    .sv   1.0.0   bits calculator for input number (round up)
  //  lib_global   rtl        gen_npr .sv   1.0.0   generator of registers with attribute "noprune"
  //  lib_global   rtl        z_line  .sv   1.0.0   delay line for serial and parallel data
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Revision List
  //
  //  Version   Date       Author
  //
  //  2.0.0     26.09.16   Pogodaev Danil
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  module eth_rx_out_v2 (

  //---- clear --------|
    
         i_sclr_n      ,

  //---- in packet ----|

  //     clock

         i_clk         ,
         i_cen         ,
         i_ced         ,

  //     strob

         i_str_body    ,
         i_cnt_body    ,
         i_str_mac     ,
         i_cnt_mac     ,
         i_str_mda     ,
         i_str_msa     ,
         i_str_mlt     ,
         i_str_ip      ,
         i_cnt_ip      ,
         i_str_pay     ,
         i_cnt_pay     ,
         i_str_gap     ,
         i_cnt_gap     ,

  //     data

         i_dat_load    ,
         i_dat_body    ,
         i_rdy_mac     ,
         i_dat_mda     ,
         i_dat_msa     ,
         i_dat_mlt     ,
         i_rdy_ip      ,
         i_dat_ip      ,

  //     length

         i_rdy_body    ,
         i_len_body    ,
         i_rdy_gap     ,
         i_len_gap     ,

  //     counters

         i_det_p_rdy   ,
         i_det_p_err   ,
         i_det_p_cut   ,

         i_cnt_p_rdy   ,
         i_cnt_p_all   ,
         i_cnt_p_err   ,
         i_cnt_p_off   ,
         i_cnt_p_spd   ,

         i_rcnt_p_rdy  ,
         i_rcnt_p_all  ,
         i_rcnt_p_err  ,
         i_rcnt_p_off  ,
         i_rcnt_p_spd  ,

  //---- out packet ---|

  //     clock

         o_clk         ,
         o_cen         ,
         o_ced         ,

  //     strob

         o_str_body    ,
         o_cnt_body    ,
         o_str_mac     ,
         o_cnt_mac     ,
         o_str_mda     ,
         o_str_msa     ,
         o_str_mlt     ,
         o_str_ip      ,
         o_cnt_ip      ,
         o_str_pay     ,
         o_cnt_pay     ,
         o_str_gap     ,
         o_cnt_gap     ,

  //     data

         o_dat_load    ,
         o_dat_body    ,
         o_rdy_mac     ,
         o_dat_mda     ,
         o_dat_msa     ,
         o_dat_mlt     ,
         o_rdy_ip      ,
         o_dat_ip      ,

  //     length

         o_rdy_body    ,
         o_len_body    ,
         o_rdy_gap     ,
         o_len_gap     ,

  //     counters

         o_det_p_rdy   ,
         o_det_p_err   ,
         o_det_p_cut   ,

         o_cnt_p_rdy   ,
         o_cnt_p_all   ,
         o_cnt_p_err   ,
         o_cnt_p_off   ,
         o_cnt_p_spd   ,

         o_rcnt_p_rdy  ,
         o_rcnt_p_all  ,
         o_rcnt_p_err  ,
         o_rcnt_p_off  ,
         o_rcnt_p_spd );

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // include files : functions
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
                                                                   //      |
  `include "cbit.sv"                                               // req  | bits calculator for input number (round up)
                                                                   //      |
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // parameters : setting
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
                                                                   //      |                                            |          |           |
  // packet manipulation ------------------------------------------//------|--------------------------------------------|----------|-----------|------------------------------------------------------------------------------//
                                                                   //      |                                            |          |           |
  parameter bit  pE_RX_JUMBO  = 0                             ;    // opt  | enable of jumbo-frames support             | on/off   | def: 0    | info: see also 'pV_RX_LEN_J' and 'i_set_jumbo'
                                                                   //      |                                            |          |           |
  // packet structure ---------------------------------------------//------|--------------------------------------------|----------|-----------|------------------------------------------------------------------------------//
                                                                   //      |                                            |          |           |
  // frame body                                                    //------|--------------------------------------------|----------|-----------|------------------------------------------------------------------------------//
                                                                   //      |                                            |          |           |
  parameter int  pW_RX_LEN    = 11                            ;    // req  | width of length / 802.3                    | bit      | def: 11   |
  parameter int  pV_RX_LEN    = 1518                          ;    // req  | value of length / 802.3                    | byte     | def: 1518 | info: used for cut off oversize frames
  parameter int  pW_RX_LEN_J  = 14                            ;    // opt  | width of length / jumbo                    | bit      | def: 14   |
  parameter int  pV_RX_LEN_J  = 9018                          ;    // opt  | value of length / jumbo                    | byte     | def: 9018 | info: used for cut off oversize frames
  parameter int  pW_RX_GAP    = 16                            ;    // opt  | width of gap                               | bit      | def: 16   |
                                                                   //      |                                            |          |           |
  // mac header                                                    //------|--------------------------------------------|----------|-----------|------------------------------------------------------------------------------//
                                                                   //      |                                            |          |           |
  parameter bit  pE_RX_MAC    = 0                             ;    // opt  | enable of mac header                       | on/off   | def: 0    |
                                                                   //      |                                            |          |           |
  // ip header                                                     //------|--------------------------------------------|----------|-----------|------------------------------------------------------------------------------//
                                                                   //      |                                            |          |           |
  parameter int  pE_RX_IP     = 0                             ;    // opt  | enable of ip header                        | on/off   | def: 0    |
  parameter int  pN_RX_IPL    = 1                             ;    // opt  | number of first (low) byte in payload      | byte     | def: 1    |
  parameter int  pN_RX_IPH    = 4                             ;    // opt  | number of last (high) byte in payload      | byte     |           |
                                                                   //      |                                            |          |           |
  // packet counters ----------------------------------------------//------|--------------------------------------------|----------|-----------|------------------------------------------------------------------------------//
                                                                   //      |                                            |          |           |
  parameter int  pN_RX_CNT    = 0                             ;    // opt  | number of counters groups                  | group    | def: 0    |
  parameter int  pW_RX_CNT_P  = 24                            ;    // opt  | width of packets counters                  | bit      | def: 24   |
  parameter int  pW_RX_CNT_S  = 32                            ;    // opt  | width of speed counters                    | bit      | def: 32   | info: byte counter
                                                                   //      |                                            |          |           |
  // out registers ------------------------------------------------//------|--------------------------------------------|----------|-----------|------------------------------------------------------------------------------//
                                                                   //      |                                            |          |           |
  parameter bit  pE_REG_OUT   = 1                             ;    // opt  | number of out registers                    | register | def: 1    | info: 1 reg = 1 t.clk
  parameter bit  pE_REG_CNT   = 0                             ;    // opt  | number of out registers / for counters     | register | def: 0    | info: 1 reg = 1 t.clk
                                                                   //      |                                            |          |           |
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // parameters : local (for i/o)
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
                                                                   //      |                                            |          |           |
  // packet structure ---------------------------------------------//------|--------------------------------------------|----------|-----------|------------------------------------------------------------------------------//
                                                                   //      |                                            |          |           |
  // frame body                                                    //------|--------------------------------------------|----------|-----------|------------------------------------------------------------------------------//
                                                                   //      |                                            |          |           |
  parameter int  lpW_RX_LEN   = pE_RX_JUMBO == 1 ?                 // req  | width of length / receiver                 | bit      |           |
                                pW_RX_LEN_J      :                 //      | width of length / jumbo                    | bit      | def: 14   |
                                pW_RX_LEN                     ;    //      | width of length / 802.3                    | bit      | def: 11   |
                                                                   //      |                                            |          |           |
  // ip header                                                     //------|--------------------------------------------|----------|-----------|------------------------------------------------------------------------------//
                                                                   //      |                                            |          |           |
  parameter int  lpN_RX_IPL   = pN_RX_IPL                     ;    // opt  | number of first (low) byte in payload      | byte     | def: 1    |
  parameter int  lpN_RX_IPH   = pN_RX_IPH >= pN_RX_IPL ?           // opt  | number of last (high) byte in payload      | byte     |           |
                                pN_RX_IPH  : pN_RX_IPL        ;    //      |                                            |          |           |
                                                                   //      |                                            |          |           |
  parameter int  lpN_RX_IP    = lpN_RX_IPH <= lpN_RX_IPL ? 1  :    //      |                                            |          |           |
                                lpN_RX_IPH  - lpN_RX_IPL + 1  ;    // opt  | number of bytes                            | byte     |           |
                                                                   //      |                                            |          |           |
  parameter int  lpW_RX_IP    = lpN_RX_IP * 8                 ;    // opt  | number of bits                             | bit      |           | info: used for 'i_dat_ip'
                                                                   //      |                                            |          |           |
  parameter int  lpW_RX_IP_C  = cbit(1,lpN_RX_IP)             ;    // opt  | width of byte counter                      | bit      |           | info: used for 'i_cnt_ip'
                                                                   //      |                                            |          |           |
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // interface
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
                                                                   //      |                                            |          |           |
  //-------------------------------------// clear -----------------//------|--------------------------------------------|----------|-----------|------------------------------------------------------------------------------//
                                                                   //      |                                            |          |           |
  input  logic                              i_sclr_n          ;    // opt  | sync clear (active low)                    | strob    | def: 1    |
                                                                   //      |                                            |          |           |
  //-------------------------------------// in packet -------------//------|--------------------------------------------|----------|-----------|------------------------------------------------------------------------------//
                                                                   //      |                                            |          |           |
  //                                        clock                  //------|--------------------------------------------|----------|-----------|------------------------------------------------------------------------------//
                                                                   //      |                                            |          |           |
  input  logic                              i_clk             ;    // req  | clock                                      | clock    |           |
  input  logic                              i_cen             ;    // req  | clock enable                               | pulse    | def: 1    | width: i_clk
  input  logic                              i_ced             ;    // req  | clock enable for data                      | pulse    | def: 1    | width: i_cen
                                                                   //      |                                            |          |           |
  //                                        strob                  //------|--------------------------------------------|----------|-----------|------------------------------------------------------------------------------//
                                                                   //      |                                            |          |           |
  input  logic    [               2 :0 ]    i_str_body        ;    // req  | frame body               / strob full      | strob    |           |
  input  logic    [ lpW_RX_LEN   -1 :0 ]    i_cnt_body        ;    // opt  | frame body               / strob count     | count up |           |
  input  logic    [               2 :0 ]    i_str_mac         ;    // opt  | mac header               / strob full      | strob    |           |
  input  logic    [               3 :0 ]    i_cnt_mac         ;    // opt  | mac header               / strob count     | count up |           |
  input  logic    [               2 :0 ]    i_str_mda         ;    // opt  | mac destination address  / strob full      | strob    |           |
  input  logic    [               2 :0 ]    i_str_msa         ;    // opt  | mac source address       / strob full      | strob    |           |
  input  logic    [               2 :0 ]    i_str_mlt         ;    // opt  | mac length or type       / strob full      | strob    |           |
  input  logic    [               2 :0 ]    i_str_ip          ;    // opt  | ip header                / strob full      | strob    |           |
  input  logic    [ lpW_RX_IP_C  -1 :0 ]    i_cnt_ip          ;    // opt  | ip header                / strob count     | count up |           |
  input  logic    [               2 :0 ]    i_str_pay         ;    // opt  | payload & pad            / strob full      | strob    |           |
  input  logic    [ lpW_RX_LEN   -1 :0 ]    i_cnt_pay         ;    // opt  | payload & pad            / strob count     | count up |           |
  input  logic    [               2 :0 ]    i_str_gap         ;    // opt  | inter-frame gap          / strob full      | strob    |           |
  input  logic    [ pW_RX_GAP    -1 :0 ]    i_cnt_gap         ;    // opt  | inter-frame gap          / strob count     | count up |           |
                                                                   //      |                                            |          |           |
  //                                        data                   //------|--------------------------------------------|----------|-----------|------------------------------------------------------------------------------//
                                                                   //      |                                            |          |           |
  input  logic                              i_dat_load        ;    // req  | frame body               / load (byte)     | pulse    |           | width: i_cen
  input  logic    [               7 :0 ]    i_dat_body        ;    // req  | frame body               / data (byte)     | pdata    |           |
                                                                   //      |                                            |          |           |
  input  logic                              i_rdy_mac         ;    // opt  | mac header               / ready           | pulse    |           | width: i_cen ; info: used for latch 'i_dat_mac'
  input  logic    [              47 :0 ]    i_dat_mda         ;    // opt  | mac destination address  / data            | pdata    |           |
  input  logic    [              47 :0 ]    i_dat_msa         ;    // opt  | mac source address       / data            | pdata    |           |
  input  logic    [              15 :0 ]    i_dat_mlt         ;    // opt  | mac length or type       / data            | pdata    |           |
                                                                   //      |                                            |          |           |
  input  logic                              i_rdy_ip          ;    // opt  | ip header                / ready           | pulse    |           | width: i_cen ; info: used for latch 'i_dat_ip'
  input  logic    [ lpW_RX_IP    -1 :0 ]    i_dat_ip          ;    // opt  | ip header                / data            | pdata    |           |
                                                                   //      |                                            |          |           |
  //                                        length                 //------|--------------------------------------------|----------|-----------|------------------------------------------------------------------------------//
                                                                   //      |                                            |          |           |
  input  logic                              i_rdy_body        ;    // req  | frame body               / ready           | pulse    |           | width: i_cen ; info: used for latch 'i_len_body'
  input  logic    [ lpW_RX_LEN   -1 :0 ]    i_len_body        ;    // req  | frame body               / length (byte)   | pdata    |           |
                                                                   //      |                                            |          |           |
  input  logic                              i_rdy_gap         ;    // opt  | inter-frame gap          / ready           | pulse    |           | width: i_cen ; info: used for latch 'i_len_gap'
  input  logic    [ pW_RX_GAP    -1 :0 ]    i_len_gap         ;    // opt  | inter-frame gap          / length (byte)   | pdata    |           |
                                                                   //      |                                            |          |           |
  //-------------------------------------// in counters -----------//------|--------------------------------------------|----------|-----------|------------------------------------------------------------------------------//
                                                                   //      |                                            |          |           |
  //                                        detector               //------|--------------------------------------------|----------|-----------|------------------------------------------------------------------------------//
                                                                   //      |                                            |          |           |
  input  logic                              i_det_p_rdy       ;    // req  | packer ready                               | pulse    |           | width: i_cen
  input  logic                              i_det_p_err       ;    // req  | packer error                               | strob    |           |
  input  logic                              i_det_p_cut       ;    // opt  | packet cut off                             | pulse    |           | width: i_cen
                                                                   //      |                                            |          |           |
  //                                        counters               //------|--------------------------------------------|----------|-----------|------------------------------------------------------------------------------//
                                                                   //      |                                            |          |           |
  input  logic                              i_cnt_p_rdy       ;    // opt  | current value / ready                      | pulse    |           | width: i_cen
  input  logic    [ pN_RX_CNT       :0 ]                           //      |                                            |          |           |
                  [ pW_RX_CNT_P  -1 :0 ]    i_cnt_p_all       ;    // opt  | current value / packet all                 | count up |           |
  input  logic    [ pN_RX_CNT       :0 ]                           //      |                                            |          |           |
                  [ pW_RX_CNT_P  -1 :0 ]    i_cnt_p_err       ;    // opt  | current value / packet error               | count up |           |
  input  logic    [ pN_RX_CNT       :0 ]                           //      |                                            |          |           |
                  [ pW_RX_CNT_P  -1 :0 ]    i_cnt_p_off       ;    // opt  | current value / packet offcast             | count up |           |
                                                                   //      |                                            |          |           |
  input  logic    [ pW_RX_CNT_S  -1 :0 ]    i_cnt_p_spd       ;    // opt  | current value / packet speed               | count up |           | info: byte counter
                                                                   //      |                                            |          |           |
  //                                        counters (rhythm)      //------|--------------------------------------------|----------|-----------|------------------------------------------------------------------------------//
                                                                   //      |                                            |          |           |
  input  logic                              i_rcnt_p_rdy      ;    // opt  | periodic value / ready                     | pulse    |           | width: i_cen
  input  logic    [ pN_RX_CNT       :0 ]                           //      |                                            |          |           |
                  [ pW_RX_CNT_P  -1 :0 ]    i_rcnt_p_all      ;    // opt  | periodic value / packet all                | pdata    |           |
  input  logic    [ pN_RX_CNT       :0 ]                           //      |                                            |          |           |
                  [ pW_RX_CNT_P  -1 :0 ]    i_rcnt_p_err      ;    // opt  | periodic value / packet error              | pdata    |           |
  input  logic    [ pN_RX_CNT       :0 ]                           //      |                                            |          |           |
                  [ pW_RX_CNT_P  -1 :0 ]    i_rcnt_p_off      ;    // opt  | periodic value / packet offcast            | pdata    |           |
                                                                   //      |                                            |          |           |
  input  logic    [ pW_RX_CNT_S  -1 :0 ]    i_rcnt_p_spd      ;    // opt  | periodic value / packet speed              | pdata    |           | info: byte counter
                                                                   //      |                                            |          |           |
  //-------------------------------------// out packet ------------//------|--------------------------------------------|----------|-----------|------------------------------------------------------------------------------//
                                                                   //      |                                            |          |           |
  //                                        clock                  //------|--------------------------------------------|----------|-----------|------------------------------------------------------------------------------//
                                                                   //      |                                            |          |           |
  output logic                              o_clk             ;    // req  | clock                                      | clock    |           |
  output logic                              o_cen             ;    // req  | clock enable                               | pulse    | def: 1    | width: o_clk
  output logic                              o_ced             ;    // req  | clock enable for data                      | pulse    | def: 1    | width: o_cen
                                                                   //      |                                            |          |           |
  //                                        strob                  //------|--------------------------------------------|----------|-----------|------------------------------------------------------------------------------//
                                                                   //      |                                            |          |           |
  output logic    [               2 :0 ]    o_str_body        ;    // req  | frame body               / strob full      | strob    |           |
  output logic    [ lpW_RX_LEN   -1 :0 ]    o_cnt_body        ;    // opt  | frame body               / strob count     | count up |           |
  output logic    [               2 :0 ]    o_str_mac         ;    // opt  | mac header               / strob full      | strob    |           |
  output logic    [               3 :0 ]    o_cnt_mac         ;    // opt  | mac header               / strob count     | count up |           |
  output logic    [               2 :0 ]    o_str_mda         ;    // opt  | mac destination address  / strob full      | strob    |           |
  output logic    [               2 :0 ]    o_str_msa         ;    // opt  | mac source address       / strob full      | strob    |           |
  output logic    [               2 :0 ]    o_str_mlt         ;    // opt  | mac length or type       / strob full      | strob    |           |
  output logic    [               2 :0 ]    o_str_ip          ;    // opt  | ip header                / strob full      | strob    |           |
  output logic    [ lpW_RX_IP_C  -1 :0 ]    o_cnt_ip          ;    // opt  | ip header                / strob count     | count up |           |
  output logic    [               2 :0 ]    o_str_pay         ;    // opt  | payload & pad            / strob full      | strob    |           | info: with crc strob (!) ; see table (2)
  output logic    [ lpW_RX_LEN   -1 :0 ]    o_cnt_pay         ;    // opt  | payload & pad            / strob count     | count up |           |
  output logic    [               2 :0 ]    o_str_gap         ;    // opt  | inter-frame gap          / strob full      | strob    |           |
  output logic    [ pW_RX_GAP    -1 :0 ]    o_cnt_gap         ;    // opt  | inter-frame gap          / strob count     | count up |           |
                                                                   //      |                                            |          |           |
  //                                        data                   //------|--------------------------------------------|----------|-----------|------------------------------------------------------------------------------//
                                                                   //      |                                            |          |           |
  output logic                              o_dat_load        ;    // req  | frame body               / load (byte)     | pulse    |           | width: o_cen
  output logic    [               7 :0 ]    o_dat_body        ;    // req  | frame body               / data (byte)     | pdata    |           |
                                                                   //      |                                            |          |           |
  output logic                              o_rdy_mac         ;    // opt  | mac header               / ready           | pulse    |           | width: o_cen ; info: used for latch 'o_dat_mac'
  output logic    [              47 :0 ]    o_dat_mda         ;    // opt  | mac destination address  / data            | pdata    |           |
  output logic    [              47 :0 ]    o_dat_msa         ;    // opt  | mac source address       / data            | pdata    |           |
  output logic    [              15 :0 ]    o_dat_mlt         ;    // opt  | mac length or type       / data            | pdata    |           |
                                                                   //      |                                            |          |           |
  output logic                              o_rdy_ip          ;    // opt  | ip header                / ready           | pulse    |           | width: o_cen ; info: used for latch 'o_dat_ip'
  output logic    [ lpW_RX_IP    -1 :0 ]    o_dat_ip          ;    // opt  | ip header                / data            | pdata    |           |
                                                                   //      |                                            |          |           |
  //                                        length                 //------|--------------------------------------------|----------|-----------|------------------------------------------------------------------------------//
                                                                   //      |                                            |          |           |
  output logic                              o_rdy_body        ;    // req  | frame body               / ready           | pulse    |           | width: o_cen ; info: used for latch 'o_len_body'
  output logic    [ lpW_RX_LEN   -1 :0 ]    o_len_body        ;    // req  | frame body               / length (byte)   | pdata    |           |
                                                                   //      |                                            |          |           |
  output logic                              o_rdy_gap         ;    // opt  | inter-frame gap          / ready           | pulse    |           | width: o_cen ; info: used for latch 'o_len_gap'
  output logic    [ pW_RX_GAP    -1 :0 ]    o_len_gap         ;    // opt  | inter-frame gap          / length (byte)   | pdata    |           |
                                                                   //      |                                            |          |           |
  //-------------------------------------// out counters ----------//------|--------------------------------------------|----------|-----------|------------------------------------------------------------------------------//
                                                                   //      |                                            |          |           |
  //                                        detector               //------|--------------------------------------------|----------|-----------|------------------------------------------------------------------------------//
                                                                   //      |                                            |          |           |
  output logic                              o_det_p_rdy       ;    // req  | packer ready                               | pulse    |           | width: o_cen
  output logic                              o_det_p_err       ;    // req  | packer error                               | strob    |           |
  output logic                              o_det_p_cut       ;    // opt  | packet cut off                             | pulse    |           | width: o_cen
                                                                   //      |                                            |          |           |
  //                                        counters               //------|--------------------------------------------|----------|-----------|------------------------------------------------------------------------------//
                                                                   //      |                                            |          |           |
  output logic                              o_cnt_p_rdy       ;    // opt  | current value / ready                      | pulse    |           | width: o_cen
  output logic    [ pN_RX_CNT       :0 ]                           //      |                                            |          |           |
                  [ pW_RX_CNT_P  -1 :0 ]    o_cnt_p_all       ;    // opt  | current value / packet all                 | count up |           |
  output logic    [ pN_RX_CNT       :0 ]                           //      |                                            |          |           |
                  [ pW_RX_CNT_P  -1 :0 ]    o_cnt_p_err       ;    // opt  | current value / packet error               | count up |           |
  output logic    [ pN_RX_CNT       :0 ]                           //      |                                            |          |           |
                  [ pW_RX_CNT_P  -1 :0 ]    o_cnt_p_off       ;    // opt  | current value / packet offcast             | count up |           |
                                                                   //      |                                            |          |           |
  output logic    [ pW_RX_CNT_S  -1 :0 ]    o_cnt_p_spd       ;    // opt  | current value / packet speed               | count up |           | info: byte counter
                                                                   //      |                                            |          |           |
  //                                        counters (rhythm)      //------|--------------------------------------------|----------|-----------|------------------------------------------------------------------------------//
                                                                   //      |                                            |          |           |
  output logic                              o_rcnt_p_rdy      ;    // opt  | periodic value / ready                     | pulse    |           | width: o_cen
  output logic    [ pN_RX_CNT       :0 ]                           //      |                                            |          |           |
                  [ pW_RX_CNT_P  -1 :0 ]    o_rcnt_p_all      ;    // opt  | periodic value / packet all                | pdata    |           |
  output logic    [ pN_RX_CNT       :0 ]                           //      |                                            |          |           |
                  [ pW_RX_CNT_P  -1 :0 ]    o_rcnt_p_err      ;    // opt  | periodic value / packet error              | pdata    |           |
  output logic    [ pN_RX_CNT       :0 ]                           //      |                                            |          |           |
                  [ pW_RX_CNT_P  -1 :0 ]    o_rcnt_p_off      ;    // opt  | periodic value / packet offcast            | pdata    |           |
                                                                   //      |                                            |          |           |  
  output logic    [ pW_RX_CNT_S  -1 :0 ]    o_rcnt_p_spd      ;    // opt  | periodic value / packet speed              | pdata    |           | info: byte counter
                                                                   //      |                                            |          |           |
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // logic
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  genvar                                    j                 ;

  //-------------------------------------// out logic --------| [ after out reg ]

  //                                        out packet

  var    logic                              ro_cen            ;
  var    logic                              ro_ced            ;

  var    logic    [               2 :0 ]    ro_str_body       ;
  var    logic    [ lpW_RX_LEN   -1 :0 ]    ro_cnt_body       ;
  var    logic    [               2 :0 ]    ro_str_mac        ;
  var    logic    [               3 :0 ]    ro_cnt_mac        ;
  var    logic    [               2 :0 ]    ro_str_mda        ;
  var    logic    [               2 :0 ]    ro_str_msa        ;
  var    logic    [               2 :0 ]    ro_str_mlt        ;
  var    logic    [               2 :0 ]    ro_str_ip         ;
  var    logic    [ lpW_RX_IP_C  -1 :0 ]    ro_cnt_ip         ;
  var    logic    [               2 :0 ]    ro_str_pay        ;
  var    logic    [ lpW_RX_LEN   -1 :0 ]    ro_cnt_pay        ;
  var    logic    [               2 :0 ]    ro_str_gap        ;
  var    logic    [ pW_RX_GAP    -1 :0 ]    ro_cnt_gap        ;

  var    logic                              ro_dat_load       ;
  var    logic    [               7 :0 ]    ro_dat_body       ;
  var    logic                              ro_rdy_mac        ;
  var    logic    [              47 :0 ]    ro_dat_mda        ;
  var    logic    [              47 :0 ]    ro_dat_msa        ;
  var    logic    [              15 :0 ]    ro_dat_mlt        ;
  var    logic                              ro_rdy_ip         ;
  var    logic    [ lpW_RX_IP    -1 :0 ]    ro_dat_ip         ;

  var    logic                              ro_rdy_body       ;
  var    logic    [ lpW_RX_LEN   -1 :0 ]    ro_len_body       ;
  var    logic                              ro_rdy_gap        ;
  var    logic    [ pW_RX_GAP    -1 :0 ]    ro_len_gap        ;

  //                                        out counters

  var    logic                              ro_det_p_rdy      ;
  var    logic                              ro_det_p_err      ;
  var    logic                              ro_det_p_cut      ;

  var    logic                              ro_cnt_p_rdy      ;
  var    logic    [ pN_RX_CNT       :0 ]
                  [ pW_RX_CNT_P  -1 :0 ]    ro_cnt_p_all      ;
  var    logic    [ pN_RX_CNT       :0 ]
                  [ pW_RX_CNT_P  -1 :0 ]    ro_cnt_p_err      ;
  var    logic    [ pN_RX_CNT       :0 ]
                  [ pW_RX_CNT_P  -1 :0 ]    ro_cnt_p_off      ;

  var    logic    [ pW_RX_CNT_S  -1 :0 ]    ro_cnt_p_spd      ;

  var    logic                              ro_rcnt_p_rdy     ;
  var    logic    [ pN_RX_CNT       :0 ]
                  [ pW_RX_CNT_P  -1 :0 ]    ro_rcnt_p_all     ;
  var    logic    [ pN_RX_CNT       :0 ]
                  [ pW_RX_CNT_P  -1 :0 ]    ro_rcnt_p_err     ;
  var    logic    [ pN_RX_CNT       :0 ]
                  [ pW_RX_CNT_P  -1 :0 ]    ro_rcnt_p_off     ;

  var    logic    [ pW_RX_CNT_S  -1 :0 ]    ro_rcnt_p_spd     ;

  //-------------------------------------// out logic --------| [ before out reg ]

  //                                        out packet

  var    logic                              no_clk            ;
  var    logic                              no_cen            ;
  var    logic                              no_ced            ;

  var    logic    [               2 :0 ]    no_str_body       ;
  var    logic    [ lpW_RX_LEN   -1 :0 ]    no_cnt_body       ;
  var    logic    [               2 :0 ]    no_str_mac        ;
  var    logic    [               3 :0 ]    no_cnt_mac        ;
  var    logic    [               2 :0 ]    no_str_mda        ;
  var    logic    [               2 :0 ]    no_str_msa        ;
  var    logic    [               2 :0 ]    no_str_mlt        ;
  var    logic    [               2 :0 ]    no_str_ip         ;
  var    logic    [ lpW_RX_IP_C  -1 :0 ]    no_cnt_ip         ;
  var    logic    [               2 :0 ]    no_str_pay        ;
  var    logic    [ lpW_RX_LEN   -1 :0 ]    no_cnt_pay        ;
  var    logic    [               2 :0 ]    no_str_gap        ;
  var    logic    [ pW_RX_GAP    -1 :0 ]    no_cnt_gap        ;

  var    logic                              no_dat_load       ;
  var    logic    [               7 :0 ]    no_dat_body       ;
  var    logic                              no_rdy_mac        ;
  var    logic    [              47 :0 ]    no_dat_mda        ;
  var    logic    [              47 :0 ]    no_dat_msa        ;
  var    logic    [              15 :0 ]    no_dat_mlt        ;
  var    logic                              no_rdy_ip         ;
  var    logic    [ lpW_RX_IP    -1 :0 ]    no_dat_ip         ;

  var    logic                              no_rdy_body       ;
  var    logic    [ lpW_RX_LEN   -1 :0 ]    no_len_body       ;
  var    logic                              no_rdy_gap        ;
  var    logic    [ pW_RX_GAP    -1 :0 ]    no_len_gap        ;

  //                                        out counters

  var    logic                              no_det_p_rdy      ;
  var    logic                              no_det_p_err      ;
  var    logic                              no_det_p_cut      ;

  var    logic                              no_cnt_p_rdy      ;
  var    logic    [ pN_RX_CNT       :0 ]
                  [ pW_RX_CNT_P  -1 :0 ]    no_cnt_p_all      ;
  var    logic    [ pN_RX_CNT       :0 ]
                  [ pW_RX_CNT_P  -1 :0 ]    no_cnt_p_err      ;
  var    logic    [ pN_RX_CNT       :0 ]
                  [ pW_RX_CNT_P  -1 :0 ]    no_cnt_p_off      ;

  var    logic    [ pW_RX_CNT_S  -1 :0 ]    no_cnt_p_spd      ;

  var    logic                              no_rcnt_p_rdy      ;
  var    logic    [ pN_RX_CNT       :0 ]
                  [ pW_RX_CNT_P  -1 :0 ]    no_rcnt_p_all      ;
  var    logic    [ pN_RX_CNT       :0 ]
                  [ pW_RX_CNT_P  -1 :0 ]    no_rcnt_p_err      ;
  var    logic    [ pN_RX_CNT       :0 ]
                  [ pW_RX_CNT_P  -1 :0 ]    no_rcnt_p_off      ;

  var    logic    [ pW_RX_CNT_S  -1 :0 ]    no_rcnt_p_spd      ;

  //-------------------------------------// data latched -----|

  //                                        mac header

  var    logic                              rdy_mac       = 0 ;
  var    logic    [              47 :0 ]    dat_mda       = 0 ;
  var    logic    [              47 :0 ]    dat_msa       = 0 ;
  var    logic    [              15 :0 ]    dat_mlt       = 0 ;

  var    logic                              s_rdy_mac         ;
  var    logic    [              47 :0 ]    s_dat_mda         ;
  var    logic    [              47 :0 ]    s_dat_msa         ;
  var    logic    [              15 :0 ]    s_dat_mlt         ;

  //                                        ip header

  var    logic                              rdy_ip        = 0 ;
  var    logic    [ lpW_RX_IP    -1 :0 ]    dat_ip        = 0 ;

  var    logic                              s_rdy_ip          ;
  var    logic    [ lpW_RX_IP    -1 :0 ]    s_dat_ip          ;

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  //==========================================================================================================================================================================================================================//
  // body of module
  //==========================================================================================================================================================================================================================//

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // out logic : output (after out reg)
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  // out packet / clock

  assign  o_clk         = no_clk        ; // not 'ro' / clock repeat
  assign  o_cen         = ro_cen        ;
  assign  o_ced         = ro_ced        ;

  // out packet / strob

  assign  o_str_body    = ro_str_body   ;
  assign  o_cnt_body    = ro_cnt_body   ;
  assign  o_str_mac     = ro_str_mac    ;
  assign  o_cnt_mac     = ro_cnt_mac    ;
  assign  o_str_mda     = ro_str_mda    ;
  assign  o_str_msa     = ro_str_msa    ;
  assign  o_str_mlt     = ro_str_mlt    ;
  assign  o_str_ip      = ro_str_ip     ;
  assign  o_cnt_ip      = ro_cnt_ip     ;
  assign  o_str_pay     = ro_str_pay    ;
  assign  o_cnt_pay     = ro_cnt_pay    ;
  assign  o_str_gap     = ro_str_gap    ;
  assign  o_cnt_gap     = ro_cnt_gap    ;

  // out packet / data

  assign  o_dat_load    = ro_dat_load   ;
  assign  o_dat_body    = ro_dat_body   ;
  assign  o_rdy_mac     = ro_rdy_mac    ;
  assign  o_dat_mda     = ro_dat_mda    ;
  assign  o_dat_msa     = ro_dat_msa    ;
  assign  o_dat_mlt     = ro_dat_mlt    ;
  assign  o_rdy_ip      = ro_rdy_ip     ;
  assign  o_dat_ip      = ro_dat_ip     ;

  // out packet / length

  assign  o_rdy_body    = ro_rdy_body   ;
  assign  o_len_body    = ro_len_body   ;
  assign  o_rdy_gap     = ro_rdy_gap    ;
  assign  o_len_gap     = ro_len_gap    ;

  // out counters / detector

  assign  o_det_p_rdy   = ro_det_p_rdy  ;
  assign  o_det_p_err   = ro_det_p_err  ;
  assign  o_det_p_cut   = ro_det_p_cut  ;
  
  // out counters / counters

  assign  o_cnt_p_rdy   = ro_cnt_p_rdy  ;
  assign  o_cnt_p_all   = ro_cnt_p_all  ;
  assign  o_cnt_p_err   = ro_cnt_p_err  ;
  assign  o_cnt_p_off   = ro_cnt_p_off  ;
  assign  o_cnt_p_spd   = ro_cnt_p_spd  ;

  // out counters / counters (rhythm)
 
  assign  o_rcnt_p_rdy  = ro_rcnt_p_rdy ;
  assign  o_rcnt_p_all  = ro_rcnt_p_all ;
  assign  o_rcnt_p_err  = ro_rcnt_p_err ;
  assign  o_rcnt_p_off  = ro_rcnt_p_off ;
  assign  o_rcnt_p_spd  = ro_rcnt_p_spd ;

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // out logic : next to output (before out reg)
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  // out packet / clock

  assign  no_clk         = i_clk        ;
  assign  no_cen         = i_cen        ;
  assign  no_ced         = i_ced        ;

  // out packet / strob

  assign  no_str_body    = i_str_body   ;
  assign  no_cnt_body    = i_cnt_body   ;
  assign  no_str_mac     = i_str_mac    ;
  assign  no_cnt_mac     = i_cnt_mac    ;
  assign  no_str_mda     = i_str_mda    ;
  assign  no_str_msa     = i_str_msa    ;
  assign  no_str_mlt     = i_str_mlt    ;
  assign  no_str_ip      = i_str_ip     ;
  assign  no_cnt_ip      = i_cnt_ip     ;
  assign  no_str_pay     = i_str_pay    ;
  assign  no_cnt_pay     = i_cnt_pay    ;
  assign  no_str_gap     = i_str_gap    ;
  assign  no_cnt_gap     = i_cnt_gap    ;

  // out packet / data

  assign  no_dat_load    = i_dat_load   ;
  assign  no_dat_body    = i_dat_body   ;
  assign  no_rdy_mac     = s_rdy_mac    ;
  assign  no_dat_mda     = s_dat_mda    ; // not 'i'
  assign  no_dat_msa     = s_dat_msa    ; // not 'i'
  assign  no_dat_mlt     = s_dat_mlt    ; // not 'i'
  assign  no_rdy_ip      = s_rdy_ip     ; // not 'i'
  assign  no_dat_ip      = s_dat_ip     ; // not 'i'

  // out packet / length

  assign  no_rdy_body    = i_rdy_body   ;
  assign  no_len_body    = i_len_body   ;
  assign  no_rdy_gap     = i_rdy_gap    ;
  assign  no_len_gap     = i_len_gap    ;

  // out counters / detector

  assign  no_det_p_rdy   = i_det_p_rdy  ;
  assign  no_det_p_err   = i_det_p_err  ;
  assign  no_det_p_cut   = i_det_p_cut  ;

  // out counters / counters

  assign  no_cnt_p_rdy   = i_cnt_p_rdy  ;
  assign  no_cnt_p_all   = i_cnt_p_all  ;
  assign  no_cnt_p_err   = i_cnt_p_err  ;
  assign  no_cnt_p_off   = i_cnt_p_off  ;
  assign  no_cnt_p_spd   = i_cnt_p_spd  ;

  // out counters / counters (rhythm)
 
  assign  no_rcnt_p_rdy  = i_rcnt_p_rdy ;
  assign  no_rcnt_p_all  = i_rcnt_p_all ;
  assign  no_rcnt_p_err  = i_rcnt_p_err ;
  assign  no_rcnt_p_off  = i_rcnt_p_off ;
  assign  no_rcnt_p_spd  = i_rcnt_p_spd ;

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // data latched : mac header
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  always_ff @(posedge i_clk)
    if (~i_sclr_n)
      begin
        rdy_mac <=  1'b0;
        dat_mda <= 48'h0;
        dat_msa <= 48'h0;
        dat_mlt <= 16'h0;
      end
    else
      if (i_cen)
        begin
          rdy_mac <= i_rdy_mac;

          if (i_rdy_mac)
            begin
              dat_mda <= i_dat_mda;
              dat_msa <= i_dat_msa;
              dat_mlt <= i_dat_mlt;
            end
        end

  assign  s_rdy_mac = (pE_REG_OUT == 0) ? i_rdy_mac : rdy_mac ;
  assign  s_dat_mda = (pE_REG_OUT == 0) ? i_dat_mda : dat_mda ;
  assign  s_dat_msa = (pE_REG_OUT == 0) ? i_dat_msa : dat_msa ;
  assign  s_dat_mlt = (pE_REG_OUT == 0) ? i_dat_mlt : dat_mlt ;

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // data latched : ip header
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  always_ff @(posedge i_clk)
    if (~i_sclr_n)
      begin
        rdy_ip <= 1'b0;
        dat_ip <=  'h0;
      end
    else
      if (i_cen)
        begin
          rdy_ip <= i_rdy_ip;

          if (i_rdy_ip)
            dat_ip <= i_dat_ip;
        end

  assign  s_rdy_ip = (pE_REG_OUT == 0) ? i_rdy_ip : rdy_ip ;
  assign  s_dat_ip = (pE_REG_OUT == 0) ? i_dat_ip : dat_ip ;

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // in/out registers : befor reg (i/no) / after reg (ri/ro)
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  // out packet ------------------------------------------// out packet ---------------------------------------------------------------------//
  
  // clock                                                   clock

  z_line #(.pV_DELAY (pE_REG_OUT), .pW_DATA (1)           )  ro__cen         ( o_clk, 1'b1, ~i_sclr_n, i_cen             , ro_cen            );
  z_line #(.pV_DELAY (pE_REG_OUT), .pW_DATA (1)           )  ro__ced         ( o_clk, 1'b1, ~i_sclr_n, i_ced             , ro_ced            );
  //                                                                                                   ^                   ^
  // strob                                                   strob

  z_line #(.pV_DELAY (pE_REG_OUT), .pW_DATA (3)           )  ro__str_body    ( o_clk, 1'b1, ~i_sclr_n, no_str_body       , ro_str_body       );
  z_line #(.pV_DELAY (pE_REG_OUT), .pW_DATA (lpW_RX_LEN)  )  ro__cnt_body    ( o_clk, 1'b1, ~i_sclr_n, no_cnt_body       , ro_cnt_body       );
  z_line #(.pV_DELAY (pE_REG_OUT), .pW_DATA (3)           )  ro__str_mac     ( o_clk, 1'b1, ~i_sclr_n, no_str_mac        , ro_str_mac        );
  z_line #(.pV_DELAY (pE_REG_OUT), .pW_DATA (4)           )  ro__cnt_mac     ( o_clk, 1'b1, ~i_sclr_n, no_cnt_mac        , ro_cnt_mac        );
  z_line #(.pV_DELAY (pE_REG_OUT), .pW_DATA (3)           )  ro__str_mda     ( o_clk, 1'b1, ~i_sclr_n, no_str_mda        , ro_str_mda        );
  z_line #(.pV_DELAY (pE_REG_OUT), .pW_DATA (3)           )  ro__str_msa     ( o_clk, 1'b1, ~i_sclr_n, no_str_msa        , ro_str_msa        );
  z_line #(.pV_DELAY (pE_REG_OUT), .pW_DATA (3)           )  ro__str_mlt     ( o_clk, 1'b1, ~i_sclr_n, no_str_mlt        , ro_str_mlt        );
  z_line #(.pV_DELAY (pE_REG_OUT), .pW_DATA (3)           )  ro__str_ip      ( o_clk, 1'b1, ~i_sclr_n, no_str_ip         , ro_str_ip         );
  z_line #(.pV_DELAY (pE_REG_OUT), .pW_DATA (lpW_RX_IP_C) )  ro__cnt_ip      ( o_clk, 1'b1, ~i_sclr_n, no_cnt_ip         , ro_cnt_ip         );
  z_line #(.pV_DELAY (pE_REG_OUT), .pW_DATA (3)           )  ro__str_pay     ( o_clk, 1'b1, ~i_sclr_n, no_str_pay        , ro_str_pay        );
  z_line #(.pV_DELAY (pE_REG_OUT), .pW_DATA (lpW_RX_LEN)  )  ro__cnt_pay     ( o_clk, 1'b1, ~i_sclr_n, no_cnt_pay        , ro_cnt_pay        );
  z_line #(.pV_DELAY (pE_REG_OUT), .pW_DATA (3)           )  ro__str_gap     ( o_clk, 1'b1, ~i_sclr_n, no_str_gap        , ro_str_gap        );
  z_line #(.pV_DELAY (pE_REG_OUT), .pW_DATA (pW_RX_GAP)   )  ro__cnt_gap     ( o_clk, 1'b1, ~i_sclr_n, no_cnt_gap        , ro_cnt_gap        );
  //                                                                                                   ^                   ^
  // data                                                    data

  z_line #(.pV_DELAY (pE_REG_OUT), .pW_DATA (1)           )  ro__dat_load    ( o_clk, 1'b1, ~i_sclr_n, no_dat_load       , ro_dat_load       );
  z_line #(.pV_DELAY (pE_REG_OUT), .pW_DATA (8)           )  ro__dat_body    ( o_clk, 1'b1, ~i_sclr_n, no_dat_body       , ro_dat_body       );
  z_line #(.pV_DELAY (0)         , .pW_DATA (1)           )  ro__rdy_mac     ( o_clk, 1'b1, ~i_sclr_n, no_rdy_mac        , ro_rdy_mac        );
  z_line #(.pV_DELAY (0)         , .pW_DATA (48)          )  ro__dat_mda     ( o_clk, 1'b1, ~i_sclr_n, no_dat_mda        , ro_dat_mda        );
  z_line #(.pV_DELAY (0)         , .pW_DATA (48)          )  ro__dat_msa     ( o_clk, 1'b1, ~i_sclr_n, no_dat_msa        , ro_dat_msa        );
  z_line #(.pV_DELAY (0)         , .pW_DATA (16)          )  ro__dat_mlt     ( o_clk, 1'b1, ~i_sclr_n, no_dat_mlt        , ro_dat_mlt        );
  z_line #(.pV_DELAY (0)         , .pW_DATA (1)           )  ro__rdy_ip      ( o_clk, 1'b1, ~i_sclr_n, no_rdy_ip         , ro_rdy_ip         );
  z_line #(.pV_DELAY (0)         , .pW_DATA (lpW_RX_IP)   )  ro__dat_ip      ( o_clk, 1'b1, ~i_sclr_n, no_dat_ip         , ro_dat_ip         );
  //                                                                                                   ^                   ^
  // length                                                  length

  z_line #(.pV_DELAY (pE_REG_OUT), .pW_DATA (1)           )  ro__rdy_body    ( o_clk, 1'b1, ~i_sclr_n, no_rdy_body       , ro_rdy_body       );
  z_line #(.pV_DELAY (pE_REG_OUT), .pW_DATA (lpW_RX_LEN)  )  ro__len_body    ( o_clk, 1'b1, ~i_sclr_n, no_len_body       , ro_len_body       );
  z_line #(.pV_DELAY (pE_REG_OUT), .pW_DATA (1)           )  ro__rdy_gap     ( o_clk, 1'b1, ~i_sclr_n, no_rdy_gap        , ro_rdy_gap        );
  z_line #(.pV_DELAY (pE_REG_OUT), .pW_DATA (pW_RX_GAP)   )  ro__len_gap     ( o_clk, 1'b1, ~i_sclr_n, no_len_gap        , ro_len_gap        );
  //                                                                                                   ^                   ^
  // out counters ----------------------------------------// out counters -------------------------------------------------------------------//

  // detector                                                detector

  z_line #(.pV_DELAY (pE_REG_CNT), .pW_DATA (1)           )  ro__det_p_rdy   ( o_clk, 1'b1, ~i_sclr_n, no_det_p_rdy      , ro_det_p_rdy      );
  z_line #(.pV_DELAY (pE_REG_CNT), .pW_DATA (1)           )  ro__det_p_err   ( o_clk, 1'b1, ~i_sclr_n, no_det_p_err      , ro_det_p_err      );
  z_line #(.pV_DELAY (pE_REG_CNT), .pW_DATA (1)           )  ro__det_p_cut   ( o_clk, 1'b1, ~i_sclr_n, no_det_p_cut      , ro_det_p_cut      );
  //                                                                                                   ^                   ^
  // counters                                                counters

  generate for (j=0; j<=pN_RX_CNT; j++) begin:               ro__cnt

  z_line #(.pV_DELAY (pE_REG_CNT), .pW_DATA (pW_RX_CNT_P) )  ro__cnt_p_all   ( o_clk, 1'b1, ~i_sclr_n, no_cnt_p_all [j]  , ro_cnt_p_all [j]  );
  z_line #(.pV_DELAY (pE_REG_CNT), .pW_DATA (pW_RX_CNT_P) )  ro__cnt_p_err   ( o_clk, 1'b1, ~i_sclr_n, no_cnt_p_err [j]  , ro_cnt_p_err [j]  );
  z_line #(.pV_DELAY (pE_REG_CNT), .pW_DATA (pW_RX_CNT_P) )  ro__cnt_p_off   ( o_clk, 1'b1, ~i_sclr_n, no_cnt_p_off [j]  , ro_cnt_p_off [j]  ); end endgenerate
  z_line #(.pV_DELAY (pE_REG_CNT), .pW_DATA (pW_RX_CNT_S) )  ro__cnt_p_spd   ( o_clk, 1'b1, ~i_sclr_n, no_cnt_p_spd      , ro_cnt_p_spd      );
  z_line #(.pV_DELAY (pE_REG_CNT), .pW_DATA (1)           )  ro__cnt_p_rdy   ( o_clk, 1'b1, ~i_sclr_n, no_cnt_p_rdy      , ro_cnt_p_rdy      );
  //                                                                                                   ^                   ^
  // counters (rhythm)                                       counters (rhythm)

  generate for (j=0; j<=pN_RX_CNT; j++) begin:               ro__rcnt

  z_line #(.pV_DELAY (pE_REG_CNT), .pW_DATA (pW_RX_CNT_P) )  ro__rcnt_p_all  ( o_clk, 1'b1, ~i_sclr_n, no_rcnt_p_all [j] , ro_rcnt_p_all [j] );
  z_line #(.pV_DELAY (pE_REG_CNT), .pW_DATA (pW_RX_CNT_P) )  ro__rcnt_p_err  ( o_clk, 1'b1, ~i_sclr_n, no_rcnt_p_err [j] , ro_rcnt_p_err [j] );
  z_line #(.pV_DELAY (pE_REG_CNT), .pW_DATA (pW_RX_CNT_P) )  ro__rcnt_p_off  ( o_clk, 1'b1, ~i_sclr_n, no_rcnt_p_off [j] , ro_rcnt_p_off [j] ); end endgenerate
  z_line #(.pV_DELAY (pE_REG_CNT), .pW_DATA (pW_RX_CNT_S) )  ro__rcnt_p_spd  ( o_clk, 1'b1, ~i_sclr_n, no_rcnt_p_spd     , ro_rcnt_p_spd     );
  z_line #(.pV_DELAY (pE_REG_CNT), .pW_DATA (1)           )  ro__rcnt_p_rdy  ( o_clk, 1'b1, ~i_sclr_n, no_rcnt_p_rdy     , ro_rcnt_p_rdy     );
  //                                                                                                   ^                   ^
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  endmodule
  
  
  

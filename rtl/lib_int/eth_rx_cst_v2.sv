  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Company      : Micran, Department of Telecommunications
  //
  //  Engineer     : Pogodaev Danil
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Library      : lib_ethernet / library of ethernet components
  //
  //  Component    : eth_rx_v2 / receiver of ethernet frames (packets)
  //
  //  File Name    : eth_rx_cst_v2 .sv (rtl)
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Description  : Ethernet preliminary receiver for custom mode (custom clock).
  //
  //  Purpose      : Generate clocks of ethernet frames for custom mode / see table (1)
  //
  //                  - clock                 / o_cst_clk
  //                  - clock enable          / o_cst_cen / width: pT_CST_CEN
  //                  - clock enable for data / o_cst_ced / width: pT_CST_CED
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Table (1)    : Clock manager of ethernet frames for receiver
  //
  //                 |--------------------------------------------------------------------------------------------------|
  //                 |                       |          |                       frequency (period)                      |
  //                 |        setting        |  clock   |---------------------------------------------------------------|
  //                 |                       |          |      10 mbit/s      |     100 mbit/s     |    1000 mbit/s     |
  //                 |=======================|==========|=====================|====================|====================|
  //                 |                       |  rx_clk  |  2.50 mhz (400 ns)  |  25.0 mhz (40 ns)  |  no                |
  //                 |  mii interface:       |----------|---------------------|--------------------|--------------------|
  //                 |                       |  rx_cen  |  1'b1               |  1'b1              |  no                |
  //                 |  pS_IO_TYPE = mii     |----------|---------------------|--------------------|--------------------|
  //                 |                       |  rx_ced  |  1.25 mhz (800 ns)  |  12.5 mhz (80 ns)  |  no                |
  //                 |=======================|==========|=====================|====================|====================|
  //                 |                       |  rx_clk  |  50.00 mhz (20 ns)  |  50.00 mhz (20 ns) |  no                |
  //                 |  rmii interface:      |----------|---------------------|--------------------|--------------------|
  //                 |                       |  rx_cen  |  5.00 mhz (200 ns)  |  1'b1              |  no                |
  //                 |  pS_IO_TYPE = rmii    |----------|---------------------|--------------------|--------------------|
  //                 |                       |  rx_ced  |  1.25 mhz (800 ns)  |  12.5 mhz (80 ns)  |  no                |
  //                 |=======================|==========|=====================|====================|====================|
  //                 |                       |  rx_clk  |  2.50 mhz (400 ns)  |  25.0 mhz (40 ns)  |  125.0 mhz (8 ns)  |
  //                 |  gmii interface:      |----------|---------------------|--------------------|--------------------|
  //                 |                       |  rx_cen  |  1'b1               |  1'b1              |  1'b1              |
  //                 |  pS_IO_TYPE = gmii    |----------|---------------------|--------------------|--------------------|
  //                 |                       |  rx_ced  |  1.25 mhz (800 ns)  |  12.5 mhz (80 ns)  |  1'b1              |
  //                 |=======================|==========|===============================================================|
  //                 |                       |  rx_clk  |  i_cst_clk                                                    |
  //                 |  custom mode:         |----------|---------------------------------------------------------------|
  //                 |                       |  rx_cen  |  pT_CST_CEN = 0 : i_cst_cen                                   |
  //                 |  pS_IO_TYPE = custom  |          |  pT_CST_CEN > 0 : i_cst_clk * pT_CST_CEN                      |
  //                 |                       |----------|---------------------------------------------------------------|
  //                 |                       |  rx_ced  |  pT_CST_CED = 0 : i_cst_ced                                   |
  //                 |                       |          |  pT_CST_CED > 0 : i_cst_clk * pT_CST_CEN * pT_CST_CED         |
  //                 |==================================================================================================|
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Include Files
  //  
  //  Category     Type       Name          Ver     Description
  //
  //  lib_global   function   cbit    .sv   1.0.0   bits calculator for input number (round up)
  //  lib_global   rtl        z_line  .sv   1.0.0   delay line for serial and parallel data
  //  lib_global   rtl        gen_npr .sv   1.0.0   generator of registers with attribute "noprune"
  //  lib_global   rtl        gen_ced .sv   1.0.0   generator of clock enable for data
  //  lib_global   rtl        gen_cen .sv   1.0.0   generator of clock enable
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Revision List
  //
  //  Version   Date       Author
  //
  //  2.0.0     26.09.16   Pogodaev Danil
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  module eth_rx_cst_v2 (

  //     clear
    
         i_sclr_n      ,

  //     custom in

         i_cst_clk     ,
         i_cst_cen     ,
         i_cst_ced     ,
         i_cst_rxd     ,
         i_cst_rx_dv   ,

  //     custom out

         o_cst_clk     ,
         o_cst_cen     ,
         o_cst_ced     ,
         o_cst_rxd     ,
         o_cst_rx_dv  );

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // parameters : setting
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
                                                  //      |                                  |         |         |
  // custom mode ---------------------------------//------|----------------------------------|---------|---------|------------------------------------------------------------------------------------------------------------//
                                                  //      |                                  |         |         |
  parameter int  pT_CST_CEN  = 10            ;    // opt  | period of clock enable           | t.clk   | def: 0  | info: when '0', used 'i_cst_cen' / see table (1)
  parameter int  pT_CST_CED  = 4             ;    // opt  | period of clock enable for data  | t.cen   | def: 0  | info: when '0', used 'i_cst_ced' / see table (1)
                                                  //      |                                  |         |         |
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // interface
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
                                                  //      |                                  |         |         |
  //-----------------------// clear --------------//------|----------------------------------|---------|---------|------------------------------------------------------------------------------------------------------------//
                                                  //      |                                  |         |         |
  input  logic                i_sclr_n       ;    // opt  | sync clear (ative low)           | strob   | def: 1  |
                                                  //      |                                  |         |         |
  //-----------------------// custom input -------//------|----------------------------------|---------|---------|------------------------------------------------------------------------------------------------------------//
                                                  //      |                                  |         |         |
  input  logic                i_cst_clk      ;    // req  | receive clock                    | clock   |         |
  input  logic                i_cst_cen      ;    // opt  | receive clock enable             | pulse   | def: 1  | width: i_cst_clk
  input  logic                i_cst_ced      ;    // opt  | receive clock enable for data    | pulse   | def: 1  | width: i_cst_cen
                                                  //      |                                  |         |         |
  input  logic    [ 7 :0 ]    i_cst_rxd      ;    // req  | receive data                     | pdata   |         | info: for latching used rising edge of clock
  input  logic                i_cst_rx_dv    ;    // req  | receive data valid               | strob   |         | 
                                                  //      |                                  |         |         |
  //-----------------------// custom output ------//------|----------------------------------|---------|---------|------------------------------------------------------------------------------------------------------------//
                                                  //      |                                  |         |         |
  output logic                o_cst_clk      ;    // req  | receive clock                    | clock   |         |
  output logic                o_cst_cen      ;    // opt  | receive clock enable             | pulse   |         | width: o_cst_clk
  output logic                o_cst_ced      ;    // opt  | receive clock enable for data    | pulse   |         | width: o_cst_cen
                                                  //      |                                  |         |         |
  output logic    [ 7 :0 ]    o_cst_rxd      ;    // req  | receive data                     | pdata   |         |
  output logic                o_cst_rx_dv    ;    // req  | receive data valid               | strob   |         | 
                                                  //      |                                  |         |         |
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // logic
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  //-----------------------// custom clock --|

  var    logic                cst_clk        ;
  var    logic                cst_cen        ;
  var    logic                cst_ced        ;

  var    logic                gen_cst_cen    ;
  var    logic                gen_cst_ced    ;

  //-----------------------// custom data ---|

  var    logic    [ 7 :0 ]    cst_rxd    = 0 ;
  var    logic                cst_rx_dv  = 0 ;

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  //==========================================================================================================================================================================================================================//
  // body of module
  //==========================================================================================================================================================================================================================//

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // out logic (without out reg)
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  assign  o_cst_clk    = cst_clk   ;
  assign  o_cst_cen    = cst_cen   ;
  assign  o_cst_ced    = cst_ced   ;
  assign  o_cst_rxd    = cst_rxd   ;
  assign  o_cst_rx_dv  = cst_rx_dv ;

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // custom clock
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  assign  cst_clk  =                     i_cst_clk               ; // repeat
  assign  cst_cen  = (pT_CST_CEN == 0) ? i_cst_cen : gen_cst_cen ;
  assign  cst_ced  = (pT_CST_CED == 0) ? i_cst_ced : gen_cst_ced ;

  gen_cen // generator of clock enable

  #(.pT_CEN (pT_CST_CEN), // period of clock ena  | t.clk
    .pN_REG (0         )) // number of output reg | register

  gen_cen (.i_sclr(~i_sclr_n), .i_clk(cst_clk), .o_cen(gen_cst_cen) );
  //                                                   ^
  gen_ced // generator of clock enable for data

  #(.pT_CEN   (0         ), // period of clock ena          | t.clk
    .pT_CED   (pT_CST_CED), // period of clock ena for data | t.cen
    .pN_REG_I (0         ), // number of reg input          | register
    .pN_REG_O (0         )) // number of reg output         | register

  gen_ced (.i_sclr(~i_sclr_n), .i_clk(cst_clk), .i_cen(cst_cen), .o_ced(gen_cst_ced) );
  //                                                                    ^
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // custom data and valid
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  always_ff @(posedge cst_clk)
    if (~i_sclr_n)
      begin
        cst_rx_dv <= 1'b0;
        cst_rxd   <= 8'd0;
      end
    else
      if (cst_cen)
        begin
          if (cst_ced) // sync to cst_ced
            begin
              cst_rx_dv <= i_cst_rx_dv ;
              cst_rxd   <= i_cst_rxd   ;
            end
        end

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  endmodule
  
  
  

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Company      : Micran, Department of Telecommunications
  //
  //  Engineer     : Pogodaev Danil
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Library      : lib_ethernet / library of ethernet components
  //
  //  Component    : eth_rx_v2 / receiver of ethernet frames (packets)
  //
  //  File Name    : eth_rx_gmii_v2 .sv (rtl)
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Description  : Ethernet preliminary receiver for GMII-interface.
  //
  //  Purpose (1)  : Generate clocks of ethernet frames / see table (1)
  //
  //                  - receive clock (repeater) / o_lan_clk
  //
  //                     for 1000 mbit/s : 125 mhz
  //                     for 100  mbit/s : 25 mhz
  //                     for 10   mbit/s : 2.5 mhz
  //
  //                  - receive clock enable / o_lan_cen
  //
  //                     for 1000 mbit/s : high
  //                     for 100  mbit/s : high
  //                     for 10   mbit/s : high
  //
  //                  - receive clock enable for data / o_lan_ced
  //
  //                     for 1000 mbit/s : high
  //                     for 100  mbit/s : period = 2 lan_cen
  //                     for 10   mbit/s : period = 2 lan_cen
  //
  //  Purpose (2)  : Convert data stream 4-bit to 8-bit (for 10/100 mbit/s)
  //
  //                  - receiver data 8 bit / o_lan_rxd
  //                  - receiver data valid / o_lan_rx_dv
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Table (1)    : Clock manager of ethernet frames for receiver
  //
  //                 |--------------------------------------------------------------------------------------------------|
  //                 |                       |          |                       frequency (period)                      |
  //                 |        setting        |  clock   |---------------------------------------------------------------|
  //                 |                       |          |      10 mbit/s      |     100 mbit/s     |    1000 mbit/s     |
  //                 |=======================|==========|=====================|====================|====================|
  //                 |                       |  rx_clk  |  2.50 mhz (400 ns)  |  25.0 mhz (40 ns)  |  no                |
  //                 |  mii interface:       |----------|---------------------|--------------------|--------------------|
  //                 |                       |  rx_cen  |  1'b1               |  1'b1              |  no                |
  //                 |  pS_IO_TYPE = mii     |----------|---------------------|--------------------|--------------------|
  //                 |                       |  rx_ced  |  1.25 mhz (800 ns)  |  12.5 mhz (80 ns)  |  no                |
  //                 |=======================|==========|=====================|====================|====================|
  //                 |                       |  rx_clk  |  50.00 mhz (20 ns)  |  50.00 mhz (20 ns) |  no                |
  //                 |  rmii interface:      |----------|---------------------|--------------------|--------------------|
  //                 |                       |  rx_cen  |  5.00 mhz (200 ns)  |  1'b1              |  no                |
  //                 |  pS_IO_TYPE = rmii    |----------|---------------------|--------------------|--------------------|
  //                 |                       |  rx_ced  |  1.25 mhz (800 ns)  |  12.5 mhz (80 ns)  |  no                |
  //                 |=======================|==========|=====================|====================|====================|
  //                 |                       |  rx_clk  |  2.50 mhz (400 ns)  |  25.0 mhz (40 ns)  |  125.0 mhz (8 ns)  |
  //                 |  gmii interface:      |----------|---------------------|--------------------|--------------------|
  //                 |                       |  rx_cen  |  1'b1               |  1'b1              |  1'b1              |
  //                 |  pS_IO_TYPE = gmii    |----------|---------------------|--------------------|--------------------|
  //                 |                       |  rx_ced  |  1.25 mhz (800 ns)  |  12.5 mhz (80 ns)  |  1'b1              |
  //                 |=======================|==========|===============================================================|
  //                 |                       |  rx_clk  |  i_cst_clk                                                    |
  //                 |  custom mode:         |----------|---------------------------------------------------------------|
  //                 |                       |  rx_cen  |  pT_CST_CEN = 0 : i_cst_cen                                   |
  //                 |  pS_IO_TYPE = custom  |          |  pT_CST_CEN > 0 : i_cst_clk * pT_CST_CEN                      |
  //                 |                       |----------|---------------------------------------------------------------|
  //                 |                       |  rx_ced  |  pT_CST_CED = 0 : i_cst_ced                                   |
  //                 |                       |          |  pT_CST_CED > 0 : i_cst_clk * pT_CST_CEN * pT_CST_CED         |
  //                 |==================================================================================================|
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Literature (1) : IEEE Std 802.3-2002 / Clause 35 : Gigabit Media Independent Interface (GMII)
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Include Files
  //  
  //  Category     Type       Name          Ver     Description
  //
  //  lib_global   function   cbit    .sv   1.0.0   bits calculator for input number (round up)
  //  lib_global   rtl        z_line  .sv   1.0.0   delay line for serial and parallel data
  //  lib_global   rtl        gen_npr .sv   1.0.0   generator of registers with attribute "noprune"
  //  lib_global   rtl        gen_ced .sv   1.0.0   generator of clock enable for data
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  //
  //  Revision List
  //
  //  Version   Date       Author
  //
  //  2.0.0     26.09.16   Pogodaev Danil
  //
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  module eth_rx_gmii_v2 (

  //     clear
    
         i_sclr_n       ,

  //     setting

         i_set_speed    ,

  //     gmii input

         i_gmii_clk     ,
         i_gmii_rxd     ,
         i_gmii_rx_dv   ,

  //     gmii output

         o_lan_clk      ,
         o_lan_cen      ,
         o_lan_ced      ,
         o_lan_rxd      ,
         o_lan_rx_dv   );

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // interface
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
                                                      //      |                                          |         |         |
  //-----------------------// clear ------------------//------|------------------------------------------|---------|---------|------------------------------------------------------------------------------------------------//
                                                      //      |                                          |         |         |
  input  logic                i_sclr_n           ;    // opt  | sync clear (active low)                  | strob   | def: 1  |
                                                      //      |                                          |         |         |
  //-----------------------// setting ----------------//------|------------------------------------------|---------|---------|------------------------------------------------------------------------------------------------//
                                                      //      |                                          |         |         |
  input  logic    [ 1 :0 ]    i_set_speed        ;    // req  | speed of interface                       | pdata   | def: 0  |
                                                      //      |                                          |         |         |
                                                      //      |   0 : 1000 mbit/s                        |         |         |
                                                      //      |   1 :  100 mbit/s                        |         |         |
                                                      //      |   2 :   10 mbit/s                        |         |         |
                                                      //      |                                          |         |         |
  //-----------------------// gmii input -------------//------|------------------------------------------|---------|---------|------------------------------------------------------------------------------------------------//
                                                      //      |                                          |         |         |
  input  logic                i_gmii_clk         ;    // req  | receive clock                            | clock   |         |
                                                      //      |                                          |         |         |
                                                      //      |  - for 1000 mbit/s : 125 mhz             |         |         |
                                                      //      |  - for 100  mbit/s : 25 mhz              |         |         |
                                                      //      |  - for 10   mbit/s : 2.5 mhz             |         |         |
                                                      //      |                                          |         |         |
  input  logic    [ 7 :0 ]    i_gmii_rxd         ;    // req  | receive data                             | pdata   |         | info: for latching used rising edge of clock
  input  logic                i_gmii_rx_dv       ;    // req  | receive data valid                       | strob   |         | 
                                                      //      |                                          |         |         |
  //-----------------------// gmii output ------------//------|------------------------------------------|---------|---------|------------------------------------------------------------------------------------------------//
                                                      //      |                                          |         |         |
  output logic                o_lan_clk          ;    // req  | receive clock                            | clock   |         |
                                                      //      |                                          |         |         |
                                                      //      |  - for 1000 mbit/s : 125 mhz             |         |         |
                                                      //      |  - for 100  mbit/s : 25 mhz              |         |         |
                                                      //      |  - for 10   mbit/s : 2.5 mhz             |         |         |
                                                      //      |                                          |         |         |
  output logic                o_lan_cen          ;    // req  | receive clock enable                     | vcc     |         |
                                                      //      |                                          |         |         |
                                                      //      |  - for 1000 mbit/s : high                |         |         |
                                                      //      |  - for 100  mbit/s : high                |         |         |
                                                      //      |  - for 10   mbit/s : high                |         |         |
                                                      //      |                                          |         |         |
  output logic                o_lan_ced          ;    // req  | receive clock enable for data            | pulse   |         | width: lan_clk
                                                      //      |                                          |         |         |
                                                      //      |  - for 1000 mbit/s : high                |         |         |
                                                      //      |  - for 100  mbit/s : period = 2 lan_cen  |         |         |
                                                      //      |  - for 10   mbit/s : period = 2 lan_cen  |         |         |
                                                      //      |                                          |         |         |
  output logic    [ 7 :0 ]    o_lan_rxd          ;    // req  | receive data                             | pdata   |         |
  output logic                o_lan_rx_dv        ;    // req  | receive data valid                       | strob   |         | 
                                                      //      |                                          |         |         |
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // logic
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  //-----------------------// in logic ----------| [ after in reg ]

  var    logic    [ 7 :0 ]    ri_gmii_rxd        ;
  var    logic                ri_gmii_rx_dv      ;

  //-----------------------// clock manager -----|

  var    logic                gmii_clk           ;
  var    logic                gmii_cen           ;
  var    logic                gmii_ced           ;

  var    logic                gen_gmii_ced       ;

  //-----------------------// deserializer ------|

  var    logic                shift_ena          ;
  var    logic    [ 2 :0 ]    shift_ena_z        ;
  var    logic    [ 7 :0 ]    shift_reg      = 0 ;
  var    logic                shift_cnt      = 0 ;
  var    logic    [ 3 :0 ]    shift_sd           ;
  var    logic    [ 7 :0 ]    shift_pd       = 0 ;
  var    logic                shift_latch    = 0 ;
  var    logic                shift_load     = 0 ;
  var    logic                shift_str      = 0 ;

  //-----------------------// mii output --------|

  var    logic                lan_clk            ;
  var    logic                lan_cen            ;
  var    logic                lan_ced            ;

  var    logic    [ 7 :0 ]    lan_rxd        = 0 ;
  var    logic                lan_rx_dv      = 0 ;

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  //==========================================================================================================================================================================================================================//
  // body of module
  //==========================================================================================================================================================================================================================//

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // out logic (without out reg)
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  assign  o_lan_clk    = lan_clk   ;
  assign  o_lan_cen    = lan_cen   ;
  assign  o_lan_ced    = lan_ced   ;
  assign  o_lan_rxd    = lan_rxd   ;
  assign  o_lan_rx_dv  = lan_rx_dv ;

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // clock manager
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  assign  gmii_clk  =                               i_gmii_clk ; // repeat
  assign  gmii_cen  =                      1'b1                ; // always high
  assign  gmii_ced  = (i_set_speed == 0) ? 1'b1 : gen_gmii_ced ;

  gen_ced // generator of clock enable for data

  #(.pT_CEN   (1), // period of clock ena          | t.clk
    .pT_CED   (2), // period of clock ena for data | t.cen
    .pN_REG_I (0), // number of reg input          | register
    .pN_REG_O (1)) // number of reg output         | register

  gen_ced (.i_sclr(~i_sclr_n), .i_clk(gmii_clk), .o_ced(gen_gmii_ced) );
  //                                                    ^
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // shift register / deserializer (for 10/100 mbit/s)
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  assign  shift_ena = ri_gmii_rx_dv ; // shift enable
  assign  shift_sd  = ri_gmii_rxd   ; // serial data (4 bit)

  // shift register (8 bit)

  always_ff @(posedge gmii_clk)
    if (~i_sclr_n)
      shift_reg <= 8'h0;
    else
      if (gmii_cen)
        begin
          if (shift_ena == 0)
            shift_reg [7:0] <= 8'h0;
          else
            begin
              shift_reg [7:4] <= shift_sd  [3:0] ;
              shift_reg [3:0] <= shift_reg [7:4] ;
            end
        end

  // shift counter (0 to 1)

  always_ff @(posedge gmii_clk)
    if (~i_sclr_n)
      shift_cnt <= 1'd0;
    else
      if (gmii_cen)
        begin
          if (shift_ena == 0)
            shift_cnt <= 1'd0;
          else
            shift_cnt <= shift_cnt + 1;
        end

  // shift latch

  always_ff @(posedge gmii_clk)
    if (~i_sclr_n)
      shift_latch <= 1'b0;
    else
      if (gmii_cen)
        shift_latch <= & shift_cnt; // shift_cnt = 1

  // shift load & parallel data (8 bit)

  always_ff @(posedge gmii_clk)
    if (~i_sclr_n)
      begin
        shift_pd   <= 8'h0;
        shift_load <= 1'b0;
        shift_str  <= 1'b0;
      end
    else
      if (gmii_cen)
        begin
          if (shift_latch)
            shift_pd   <= shift_reg       ;
            shift_load <= shift_latch     ; // for testing only
            shift_str  <= shift_ena_z [2] ;
        end

  z_line // delay line

  #(.pV_DELAY (2), // value of delay | t.clk_en
    .pW_DATA  (1)) // width of data  | bit

  z__shift_ena (.i_clk(gmii_clk), .i_clk_en(gmii_cen), .i_sclr(~i_sclr_n), .i_data(shift_ena), .o_delay(shift_ena_z));
  //                                                                               ^                    ^
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // gmii output (8 bit)
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  // clock

  assign  lan_clk  = gmii_clk ;
  assign  lan_cen  = gmii_cen ;
  assign  lan_ced  = gmii_ced ;

  // data (8 bit)

  always_ff @(posedge gmii_clk)
    if (~i_sclr_n)
      begin
        lan_rx_dv <= 1'b0;
        lan_rxd   <= 8'h0;
      end
    else
      if (gmii_cen)
        begin
          if (gmii_ced) // sync to gmii_ced
            begin
              lan_rx_dv <= (i_set_speed == 0) ? ri_gmii_rx_dv : shift_str ;
              lan_rxd   <= (i_set_speed == 0) ? ri_gmii_rxd   : shift_pd  ;
            end
        end

  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
  // in/out registers : befor reg (i/no) / after reg (ri/ro)
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  // gmii input -----------------------// gmii input ------------------------------------------------------------//

  z_line #(.pV_DELAY (2), .pW_DATA (8))  ri__gmii_rxd    ( i_gmii_clk, 1'b1, 1'b0, i_gmii_rxd   , ri_gmii_rxd   ); // used fast input reg
  z_line #(.pV_DELAY (2), .pW_DATA (1))  ri__gmii_rx_dv  ( i_gmii_clk, 1'b1, 1'b0, i_gmii_rx_dv , ri_gmii_rx_dv ); // used fast input reg
  //                  ^                                                            ^              ^
  //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//

  endmodule
  
  
  

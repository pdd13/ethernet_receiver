  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  #
  #  Company      : Micran, Department of Telecommunications
  #
  #  Engineer     : Pogodaev Danil
  #
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  #
  #  Library      : lib_ethernet / library of ethernet components
  #
  #  Component    : eth_rx_v2 / receiver of ethernet frames (packets)
  #
  #  File Name    : sim .do (tcl sim aldec)
  #
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  #
  #  Description  : Simulation script for Aldec simulator.
  #
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  #
  #  Note (1)     : Command 'asim'           Invokes the simulator.
  #
  #                  +access                 Enables access to design objects. Read access (+r) allows you to record signals to the simulation database, examine signal values with
  #                                          the examine command and/or debugging tools.
  #
  #                  -t <resolution>         Specifies the simulation resolution. The resolution should be specified as a combination of a time value (a number) and
  #                                          a time unit (fs, ps, ns, us, ms, s). In Verilog, the smallest time precision argument of all the `timescale directives
  #                                          or the timeunit or timeprecision constructs specified in a design is used. In VHDL designs and Verilog designs with neither
  #                                          directives nor statements determining the time precision, the simulation resolution is set to 1ps by default.
  #                                          If this argument is omitted, the resolution is determined automatically. (Verilog default: minimum time_precision in the design)
  #
  #                  -lib <library_name>     Specifies the name of the library containing the top-level design unit. If omitted, the current working library is assumed. (Default: work)
  #
  #                  -L <library_name>       In Verilog, this argument allows specifying additional libraries that should be searched for units instantiated in Verilog.
  #                                          The simulator resorts to libraries specified with -L only to search for units that were neither bound by the compiler during compilation
  #                                          nor cannot be found in the library where the simulation top-level unit is located.
  #
  #                  [<library>].<unit>      Specifies the name of the simulation top-level unit.
  #                                          It can be a VHDL configuration or an entity, a Verilog module, a System Verilog program, a System C module, or an EDIF cell.
  #                                          A VHDL entity can be followed by the name of an architecture. If only the entity name is specified then the last architecture compiled
  #                                          for the specified entity is simulated. If no top-level unit is specified then all Verilog top-level modules are simulated.
  #                                          Multiple units specified as the simulation top-level must reside in the same library.
  #
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  #
  #  Note (2)     : Command 'run'            Runs the simulation. If no argument is specified, the command behaves as if it were invoked with the -all argument.
  #
  #                  -all                    Advances the simulation until no simulation events are scheduled or until the simulation is stopped by a code or a signal breakpoint.
  #
  #                  <time_step>             Specifies the time step by which the simulation will be advanced. The time unit can be specified as fs, ps, ns, us, ms, s or sec.
  #                                          If the time unit is omitted, it is assumed that the time step is specified using the current simulation resolution.
  #
  #                  @<time>                 An absolute time point until which the simulation will advance. The time unit can be specified as fs, ps, ns, us, ms, s or sec.
  #                                          If the time unit is omitted, it is assumed that the time step is specified using the current simulation resolution.
  #
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  #
  #  Note (3)     : Command 'wave'           Adds objects to the Waveform Viewer.
  #  
  #                                          If the window is not open yet, issuing the command first opens the viewer and then adds the specified objects to the Object View pane
  #                                          of the Waveform Viewer window. The order in which objects are added depends on the method you use to group or add them to the viewer window.
  #                                          If you use wildcards, objects are added in the same order they are declared in source code.
  #                                          If you issue a sequence of the wave commands, the objects are displayed in the order the commands have been executed.
  #
  #                  -expand                 Automatically expands the object added to the waveform.
  #
  #                  -vgroup                 Creates a virtual group. The name of the object is specified by the <name> argument.
  #                                          Virtual groups can be nested or they can contain other virtual objects: virtual arrays, virtual records, or named rows.
  #                                          You can create a virtual group that contains nested virtual objects or nested named rows by using the following syntax:
  #
  #                                          wave -vgroup <virtual_name> <signals> ( -vgroup <nested_virtual_name> <signals> ) <signals>
  #
  #                  -height <pixels>        Specifies the height of the signal waveform in pixels.
  #
  #                  -bold                   When this argument is passed, the name of the object listed in the Object View pane and its value displayed in the Waveform View pane
  #                                          will be presented in bold. The argument does not affect the display of object waves.
  #
  #                  -color <color>          Specifies the color of the signal name in the Object View pane. The color can be either an RGB value or a name.
  #
  #                                          The RGB value should be specified with a hash sign (#) followed by hex digits. One, two, three, or four digits can be used for each color.
  #                                          The RGB value can be specified with three decimal values separated by commas.
  #
  #                                          Definitions of color names (e.g. steelblue or gainsboro) have been provided by the World Wide Web Consortium and are available at
  #                                          www.w3.org/TR/SVG/types.html#ColorKeywords. Color transparent represents the absence of a color.
  #
  #                  -color_waveform         Applies the color specified with the -color argument to the timing waveform in the Waveform View pane.
  #
  #                  -ascii                  Specifies how the waveform of the signal should be displayed.
  #                  -binary                 You can use different radices and different notations for the decimal radix. If radix is not specified, the radix returned
  #                  -decimal                by the radix command is used. Available radices include ASCII (-ascii), binary (-binary), octal (-octal), decimal (-decimal),
  #                  -unsigned               hexadecimal (-hexadecimal), and floating-point (-floatingpoint). It is assumed that values displayed in the decimal radix (-decimal)
  #                  -hexadecimal            use two's complement notation.
  #
  #                                          The notation for the decimal radix can be specified explicitly.
  #                                          Available options include unsigned (-unsigned) one's complement (-1complement), two's complement (-2complement), and signed magnitude
  #                                          (-magnitude). Note that options -decimal and -2complement are equivalent. When the decimal notation is selected, you can specify
  #                                          a fixed-point notation by using the -binarypoint argument.
  #
  #                                          The floating-point representation adheres to IEEE Std 754 and is available for floating-point variables and for 64-bit or 32-bit
  #                                          vectors only.
  #
  #                  -named_row | -divider   Inserts a named row or a row separator to the waveform window. The name of the object is specified by the <name> argument.
  #                                          Named rows and row dividers do not show timing waveforms. They are used to improve the legibility of data shown in the waveform window.
  #                                          For example, you can add a row displaying the string MEMORY PORTS above the ports of a memory unit:
  #
  #                                          If the name of the row contains spaces, it should be written with quotation marks.
  #                                          Note that specifying the space character instead of the row name will result in inserting a row without any visible name
  #                                          (inserts an empty row).
  #
  #                  -label <name>           Renames an object added to the waveform. The <name> parameter specifies the name which will replace the original name.
  #                                          To rename more than one object, use the wave command with the -label argument for each object separately. The name is set only for
  #                                          the display purposes. Even if the object is renamed, it can be referred to (for example by the force or examine commands) only
  #                                          by using the original name.
  #
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  #
  #  Revision List
  #
  #  Version   Date       Author
  #
  #  2.0       26.09.16   Pogodaev Danil
  #
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  # invokes the simulator and arguments setting
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

  asim +access +r -t 1ns -lib work  work.tb_top ; # tb_top as the top level module

  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  # run time setting
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

  set run_time 10000000 ; # measure : time resolution (asim -t)

  #-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  # waveform : module selection
  #-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

  #---------------------------------------------------------------------------------------------------------------------------------|
  # top testbench                                                                                                                   |
  #---------------------------------------------------------------------------------------------------------------------------------|
  #                              |                                                             |               |                    |
  set wave_top      1   ; # top  | receiver of ethernet frames (packets) / tb top              | eth_rx        | eth_rx_v2          |
  #                 ^ ^          |                                                             |               |                    |
  #--------------------------------------------------------------------------------------------|---------------|--------------------|
  # top generator                                                                              |               |                    |
  #--------------------------------------------------------------------------------------------|---------------|--------------------|
  #                              |                                                             |               |                    |
  set wave_tbg      1   ; # gen  | receiver of ethernet frames (packets) / tb generator        | tb_gen        | tb_gen             |
  #                 ^ ^          |                                                             |               |                    |
  #--------------------------------------------------------------------------------------------|---------------|--------------------|
  # top generator / ethernet emulator                                                          |               |                    |
  #--------------------------------------------------------------------------------------------|---------------|--------------------|
  #                              |                                                             |               |                    |
  set wave_tbg_1    1   ; # 1.0  | emulator of ethernet frames                                 | eth_emu       | tb_eth_emu_v2      |
  set wave_tbg_1_x  1   ; # 1.x  | emulator of ethernet frames / tb generator                  | tb_gen_emu    | tb_gen_emu         | value: 0 1 0 (!)
  set wave_tbg_1_y  0   ; # 1.y  | emulator of ethernet frames / tb generator / manual mode    | tb_gen_emu_m  | tb_gen_emu_m       | value: 0 0 1 (!)
  #                 ^ ^          |                                                             |               |                    |
  set wave_tbg_1_1    0 ; # 1.1  | generator of initialization signal                          | emu_ini       | tb_eth_emu_ini_v2  |
  set wave_tbg_1_2    0 ; # 1.2  | clocks generator of ethernet frames                         | emu_clk       | tb_eth_emu_clk_v2  |
  set wave_tbg_1_3    0 ; # 1.3  | strobes generator of ethernet frames                        | emu_str       | tb_eth_emu_str_v2  |
  set wave_tbg_1_4    0 ; # 1.4  | data stream generator of ethernet frames                    | emu_dat       | tb_eth_emu_dat_v2  |
  set wave_tbg_1_5    0 ; # 1.5  | lan ethernet interface implementation                       | emu_lan       | tb_eth_emu_lan_v2  |
  set wave_tbg_1_6    0 ; # 1.6  | speed calculator (calculator of gap)                        | emu_spd       | tb_eth_emu_spd_v2  |
  #                 ^ ^          |                                                             |               |                    |
  #--------------------------------------------------------------------------------------------|---------------|--------------------|
  # lib component                                                                              |               |                    |
  #--------------------------------------------------------------------------------------------|---------------|--------------------|
  #                              |                                                             |               |                    |
  set wave_lib_1    1   ; # 1.0  | ethernet preliminary receiver for custom mode               | rx_cst        | eth_rx_cst_v2      |
  set wave_lib_2    1   ; # 2.0  | ethernet preliminary receiver for mii-interface             | rx_mii        | eth_rx_mii_v2      |
  set wave_lib_3    1   ; # 3.0  | ethernet preliminary receiver for rmii-interface            | rx_rmii       | eth_rx_rmii_v2     |
  set wave_lib_4    1   ; # 4.0  | ethernet preliminary receiver for gmii-interface            | rx_gmii       | eth_rx_gmii_v2     |
  set wave_lib_5    1   ; # 5.0  | multiplexer of ethernet preliminary receivers               | rx_mux        | eth_rx_mux_v2      |
  set wave_lib_6    1   ; # 6.0  | frame header detector (preamble and start-frame delimiter)  | rx_head       | eth_rx_head_v2     |
  set wave_lib_7    1   ; # 7.0  | strobe generator and length calculator for ethernet frames  | rx_str        | eth_rx_str_v2      |
  set wave_lib_8    1   ; # 8.0  | data extractor for ip and mac headers                       | rx_dat        | eth_rx_str_v2      |
  set wave_lib_9    1   ; # 9.0  | counters of ethernet frames (packets)                       | rx_cnt        | eth_cnt_v2         |
  set wave_lib_9_1    0 ; # 9.1  | crc calculator for received ethernet frames                 | rx_crc        | eth_rx_crc_v2      |
  set wave_lib_10   1   ; # 10.0 | output registers and latching data (mac and ip headers)     | rx_out        | eth_rx_out_v2      |
  #                 ^ ^          |                                                             |               |                    |
  #---------------------------------------------------------------------------------------------------------------------------------|

  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  # waveform : radix setting
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

  set r_hex "hexadecimal"
  set r_uns "unsigned"
  set r_bin "binary"
  set r_asc "ascii"

  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  # waveform : height setting
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

  set h_mod 30 ; # module
  set h_grp 30 ; # group
  set h_ch1 30 ; # divider (chapter 1)
  set h_ch2 25 ; # divider (chapter 2)
  set h_sig 20 ; # signal
  set h_par 20 ; # parameters
  set h_nul 15 ; # null

  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  # waveform : color setting
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

  # available colors

  set black              "0,0,0"
  set white              "255,255,255"
  set red                "255,0,0"
  set red_dark           "128,0,0"
  set green              "0,255,0"
  set green_dark         "0,128,0"
  set blue               "0,0,255"
  set blue_dark          "0,0,128"
  set cyan               "0,255,255"
  set cyan_dark          "0,128,128"
  set maganta            "255,0,255"
  set maganta_dark       "128,0,128"
  set yellow             "255,255,0"
  set yellow_dark        "128,128,0"
  set gray               "160,160,164"
  set gray_dark          "128,128,128"
  set gray_light         "192,192,192"

  # set color for name

  set cn_mod $black      ; # module
  set cn_grp $red        ; # group
  set cn_ch1 $blue       ; # divider (chapter 1)
  set cn_ch2 $green_dark ; # divider (chapter 2)
  set cn_sig $black      ; # signal
  set cn_par $black      ; # parameters
  set cn_nul $gray       ; # null
  set cn_spc $red        ; # special

  # set bold for name (1/0)

  set bn_mod 1           ; # module
  set bn_grp 0           ; # group
  set bn_ch1 0           ; # divider (chapter 1)
  set bn_ch2 0           ; # divider (chapter 2)
  set bn_sig 0           ; # signal
  set bn_par 0           ; # parameters
  set bn_nul 0           ; # null
  set bn_spc 0           ; # special

  # set same color for wave (1/0)

  set cw_mod 1           ; # module
  set cw_grp 1           ; # group
  set cw_ch1 1           ; # divider (chapter 1)
  set cw_ch2 1           ; # divider (chapter 2)
  set cw_sig 1           ; # signal
  set cw_par 1           ; # parameters
  set cw_nul 1           ; # null
  set cw_spc 1           ; # special

  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  # waveform : color setting / result
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

  # add color for wave

  if {$cw_mod} { append cn_mod "\n-color_waveform" }; # module
  if {$cw_grp} { append cn_grp "\n-color_waveform" }; # group
  if {$cw_ch1} { append cn_ch1 "\n-color_waveform" }; # divider (chapter 1)
  if {$cw_ch2} { append cn_ch2 "\n-color_waveform" }; # divider (chapter 2)
  if {$cw_sig} { append cn_sig "\n-color_waveform" }; # signal
  if {$cw_par} { append cn_par "\n-color_waveform" }; # parameters
  if {$cw_nul} { append cn_nul "\n-color_waveform" }; # null
  if {$cw_spc} { append cn_spc "\n-color_waveform" }; # special

  # add bold for name

  if {$bn_mod} { append cn_mod "\n-bold" }; # module
  if {$bn_grp} { append cn_grp "\n-bold" }; # group
  if {$bn_ch1} { append cn_ch1 "\n-bold" }; # divider (chapter 1)
  if {$bn_ch2} { append cn_ch2 "\n-bold" }; # divider (chapter 2)
  if {$bn_sig} { append cn_sig "\n-bold" }; # signal
  if {$bn_par} { append cn_par "\n-bold" }; # parameters
  if {$bn_nul} { append cn_nul "\n-bold" }; # null
  if {$bn_spc} { append cn_spc "\n-bold" }; # special

  # result color settings

  set c_mod $cn_mod ; # module
  set c_grp $cn_grp ; # group
  set c_ch1 $cn_ch1 ; # divider (chapter 1)
  set c_ch2 $cn_ch2 ; # divider (chapter 2)
  set c_sig $cn_sig ; # signal
  set c_par $cn_par ; # parameters
  set c_nul $cn_nul ; # null
  set c_spc $cn_spc ; # special

  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  # waveform : add do-files
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

  # top testbench

  if {$wave_top     } {do $dir_sim/wave/wave_top.do     };

  # top generator 

  if {$wave_tbg     } {do $dir_sim/wave/wave_tbg.do     };

  # top generator / emulator

  if {$wave_tbg_1   } {do $dir_sim/wave/wave_tbg_1.do   };
  if {$wave_tbg_1_x } {do $dir_sim/wave/wave_tbg_1_x.do };
  if {$wave_tbg_1_y } {do $dir_sim/wave/wave_tbg_1_y.do };
  if {$wave_tbg_1_1 } {do $dir_sim/wave/wave_tbg_1_1.do };
  if {$wave_tbg_1_2 } {do $dir_sim/wave/wave_tbg_1_2.do };
  if {$wave_tbg_1_3 } {do $dir_sim/wave/wave_tbg_1_3.do };
  if {$wave_tbg_1_4 } {do $dir_sim/wave/wave_tbg_1_4.do };
  if {$wave_tbg_1_5 } {do $dir_sim/wave/wave_tbg_1_5.do };
  if {$wave_tbg_1_6 } {do $dir_sim/wave/wave_tbg_1_6.do };

  # lib component

  if {$wave_lib_1   } {do $dir_sim/wave/wave_lib_1.do   };
  if {$wave_lib_2   } {do $dir_sim/wave/wave_lib_2.do   };
  if {$wave_lib_3   } {do $dir_sim/wave/wave_lib_3.do   };
  if {$wave_lib_4   } {do $dir_sim/wave/wave_lib_4.do   };
  if {$wave_lib_5   } {do $dir_sim/wave/wave_lib_5.do   };
  if {$wave_lib_6   } {do $dir_sim/wave/wave_lib_6.do   };
  if {$wave_lib_7   } {do $dir_sim/wave/wave_lib_7.do   };
  if {$wave_lib_8   } {do $dir_sim/wave/wave_lib_8.do   };
  if {$wave_lib_9   } {do $dir_sim/wave/wave_lib_9.do   };
  if {$wave_lib_9_1 } {do $dir_sim/wave/wave_lib_9_1.do };
  if {$wave_lib_10  } {do $dir_sim/wave/wave_lib_10.do  };

  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  # running the simulation
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

  run $run_time

  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#




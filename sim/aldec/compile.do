  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  #
  #  Company      : Micran, Department of Telecommunications
  #
  #  Engineer     : Pogodaev Danil
  #
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  #
  #  Library      : lib_ethernet / library of ethernet components
  #
  #  Component    : eth_rx_v2 / receiver of ethernet frames (packet)
  #
  #  File Name    : compile .do (tcl sim aldec)
  #
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  #
  #  Description  : Compilation script for Aldec simulator.
  #
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  #
  #  Note (1)     : Command 'alib'                 Creates a new library and modifies the library list that describes mapping between logical names and library index files (.lib).
  #  
  #  Note (2)     : Command 'alog'                 Invokes the Verilog/SystemVerilog compiler. By default the compiler accepts source code conforming to IEEE Std 1800-2005.
  #
  #                  -v95                          Disables LRM 1364-2001 and System Verilog extensions. Only Verilog 95 constructs are allowed.
  #                  -v2k                          Disables System Verilog extensions. Only Verilog 2001 constructs are allowed.
  #                  -v2k5 | -v2005                Forces the compiler to conform to IEEE Std 1364-2005. System Verilog extensions are not allowed.
  #
  #                  -sv2k5                        Forces the compiler to conform to System Verilog IEEE 1800-2005.
  #                  -sv2k9                        Forces the compiler to conform to System Verilog IEEE 1800-2009.
  #
  #                  -incdir <path>                Specifies the directory to be searched for files included with the `include directive.
  #
  #                  +incdir<+path>[<+path> ...]   Specifies directories to be searched for files included with the `include directive.
  #                                                Multiple directories specified with one +incdir argument should be separated with the (+) sign.
  #
  #                  <filename> [<filename> ...]   The name of the source file to be compiled. Several file names separated by spaces can be specified.
  # 
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  #
  #  Revision List
  #
  #  Version   Date       Author
  #
  #  2.0       26.09.16   Pogodaev Danil
  #
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  # create the work library
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

  alib work

  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  # compile : testbench files
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

  # top testbench                             +function                  +include

  alog $dir_tb__top_tb/tb_top.sv              +incdir+$dir_fun__lib_glb  +incdir+$dir_inc__top_tb

  # top generator                             +function                  +include

  alog $dir_tb__top_gen/tb_gen.sv             +incdir+$dir_fun__lib_glb  +incdir+$dir_inc__top_tb+$dir_inc__top_gen
  alog $dir_tb__top_gen/tb_gen_emu.sv         +incdir+$dir_fun__lib_glb  +incdir+$dir_inc__top_tb+$dir_inc__top_gen
  alog $dir_tb__top_gen/tb_gen_emu_m.sv       +incdir+$dir_fun__lib_glb  +incdir+$dir_inc__top_tb+$dir_inc__top_gen

  # lib ethernet                              +function

  alog $dir_tb__lib_eth/tb_eth_emu_v2.sv      +incdir+$dir_fun__lib_glb
  alog $dir_tb__lib_eth/tb_eth_emu_ini_v2.sv  +incdir+$dir_fun__lib_glb
  alog $dir_tb__lib_eth/tb_eth_emu_clk_v2.sv  +incdir+$dir_fun__lib_glb
  alog $dir_tb__lib_eth/tb_eth_emu_str_v2.sv  +incdir+$dir_fun__lib_glb
  alog $dir_tb__lib_eth/tb_eth_emu_dat_v2.sv  +incdir+$dir_fun__lib_glb+$dir_fun__lib_eth
  alog $dir_tb__lib_eth/tb_eth_emu_lan_v2.sv  +incdir+$dir_fun__lib_glb
  alog $dir_tb__lib_eth/tb_eth_emu_spd_v2.sv  +incdir+$dir_fun__lib_glb

  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  # compile : rtl files
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

  # lib internal                              +function

  alog $dir_rtl__lib_int/eth_rx_v2.sv         +incdir+$dir_fun__lib_glb
  alog $dir_rtl__lib_int/eth_rx_mii_v2.sv     +incdir+$dir_fun__lib_glb
  alog $dir_rtl__lib_int/eth_rx_rmii_v2.sv    +incdir+$dir_fun__lib_glb
  alog $dir_rtl__lib_int/eth_rx_gmii_v2.sv    +incdir+$dir_fun__lib_glb
  alog $dir_rtl__lib_int/eth_rx_cst_v2.sv     +incdir+$dir_fun__lib_glb
  alog $dir_rtl__lib_int/eth_rx_mux_v2.sv     +incdir+$dir_fun__lib_glb
  alog $dir_rtl__lib_int/eth_rx_head_v2.sv    +incdir+$dir_fun__lib_glb
  alog $dir_rtl__lib_int/eth_rx_str_v2.sv     +incdir+$dir_fun__lib_glb
  alog $dir_rtl__lib_int/eth_rx_dat_v2.sv     +incdir+$dir_fun__lib_glb
  alog $dir_rtl__lib_int/eth_rx_out_v2.sv     +incdir+$dir_fun__lib_glb
  
  # lib ethernet                              +function
  
  alog $dir_rtl__lib_eth/eth_cnt_v2.sv        +incdir+$dir_fun__lib_glb+$dir_fun__lib_eth
  alog $dir_rtl__lib_eth/eth_rx_crc_v2.sv     +incdir+$dir_fun__lib_glb+$dir_fun__lib_eth
    
  # lib global                                +function

  alog $dir_rtl__lib_glb/z_line.sv            +incdir+$dir_fun__lib_glb
  alog $dir_rtl__lib_glb/gen_npr.sv           +incdir+$dir_fun__lib_glb
  alog $dir_rtl__lib_glb/det_se.sv            +incdir+$dir_fun__lib_glb
  alog $dir_rtl__lib_glb/det_de.sv            +incdir+$dir_fun__lib_glb
  alog $dir_rtl__lib_glb/gen_sstr.sv          +incdir+$dir_fun__lib_glb
  alog $dir_rtl__lib_glb/gen_mstr.sv          +incdir+$dir_fun__lib_glb
  alog $dir_rtl__lib_glb/gen_cen.sv           +incdir+$dir_fun__lib_glb
  alog $dir_rtl__lib_glb/gen_ced.sv           +incdir+$dir_fun__lib_glb
  alog $dir_rtl__lib_glb/shift_p2s.sv         +incdir+$dir_fun__lib_glb
  alog $dir_rtl__lib_glb/shift_s2p.sv         +incdir+$dir_fun__lib_glb

  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#




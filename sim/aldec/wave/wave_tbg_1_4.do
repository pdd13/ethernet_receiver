  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  #
  #  Company      : Micran, Department of Telecommunications
  #
  #  Engineer     : Pogodaev Danil
  #
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  #
  #  Library      : lib_ethernet / library of ethernet components
  #
  #  Component    : eth_rx_v2 / receiver of ethernet frames (packets)
  #
  #  File Name    : wave_tbg_1_4 .do (tcl sim aldec)
  #
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  #
  #  Description  : List of module waveforms for Aldec simulator.
  #
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  #
  #  Note (1)     : Command 'wave'           Adds objects to the Waveform Viewer.
  #  
  #                                          If the window is not open yet, issuing the command first opens the viewer and then adds the specified objects to the Object View pane
  #                                          of the Waveform Viewer window. The order in which objects are added depends on the method you use to group or add them to the viewer window.
  #                                          If you use wildcards, objects are added in the same order they are declared in source code.
  #                                          If you issue a sequence of the wave commands, the objects are displayed in the order the commands have been executed.
  #
  #                  -expand                 Automatically expands the object added to the waveform.
  #
  #                  -vgroup                 Creates a virtual group. The name of the object is specified by the <name> argument.
  #                                          Virtual groups can be nested or they can contain other virtual objects: virtual arrays, virtual records, or named rows.
  #                                          You can create a virtual group that contains nested virtual objects or nested named rows by using the following syntax:
  #
  #                                          wave -vgroup <virtual_name> <signals> ( -vgroup <nested_virtual_name> <signals> ) <signals>
  #
  #                  -height <pixels>        Specifies the height of the signal waveform in pixels.
  #
  #                  -bold                   When this argument is passed, the name of the object listed in the Object View pane and its value displayed in the Waveform View pane
  #                                          will be presented in bold. The argument does not affect the display of object waves.
  #
  #                  -color <color>          Specifies the color of the signal name in the Object View pane. The color can be either an RGB value or a name.
  #
  #                                          The RGB value should be specified with a hash sign (#) followed by hex digits. One, two, three, or four digits can be used for each color.
  #                                          The RGB value can be specified with three decimal values separated by commas.
  #
  #                                          Definitions of color names (e.g. steelblue or gainsboro) have been provided by the World Wide Web Consortium and are available at
  #                                          www.w3.org/TR/SVG/types.html#ColorKeywords. Color transparent represents the absence of a color.
  #
  #                  -color_waveform         Applies the color specified with the -color argument to the timing waveform in the Waveform View pane.
  #
  #                  -ascii                  Specifies how the waveform of the signal should be displayed.
  #                  -binary                 You can use different radices and different notations for the decimal radix. If radix is not specified, the radix returned
  #                  -decimal                by the radix command is used. Available radices include ASCII (-ascii), binary (-binary), octal (-octal), decimal (-decimal),
  #                  -unsigned               hexadecimal (-hexadecimal), and floating-point (-floatingpoint). It is assumed that values displayed in the decimal radix (-decimal)
  #                  -hexadecimal            use two's complement notation.
  #
  #                                          The notation for the decimal radix can be specified explicitly.
  #                                          Available options include unsigned (-unsigned) one's complement (-1complement), two's complement (-2complement), and signed magnitude
  #                                          (-magnitude). Note that options -decimal and -2complement are equivalent. When the decimal notation is selected, you can specify
  #                                          a fixed-point notation by using the -binarypoint argument.
  #
  #                                          The floating-point representation adheres to IEEE Std 754 and is available for floating-point variables and for 64-bit or 32-bit
  #                                          vectors only.
  #
  #                  -named_row | -divider   Inserts a named row or a row separator to the waveform window. The name of the object is specified by the <name> argument.
  #                                          Named rows and row dividers do not show timing waveforms. They are used to improve the legibility of data shown in the waveform window.
  #                                          For example, you can add a row displaying the string MEMORY PORTS above the ports of a memory unit:
  #
  #                                          If the name of the row contains spaces, it should be written with quotation marks.
  #                                          Note that specifying the space character instead of the row name will result in inserting a row without any visible name
  #                                          (inserts an empty row).
  #
  #                  -label <name>           Renames an object added to the waveform. The <name> parameter specifies the name which will replace the original name.
  #                                          To rename more than one object, use the wave command with the -label argument for each object separately. The name is set only for
  #                                          the display purposes. Even if the object is renamed, it can be referred to (for example by the force or examine commands) only
  #                                          by using the original name.
  #
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  #
  #  Revision List
  #
  #  Version   Date       Author
  #
  #  2.0       26.09.16   Pogodaev Danil
  #
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  # module setting
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

  set module_name "tbg 1.4 : emu_dat"

  set module_path "/tb_top/tb_gen/eth_emu/emu_dat"

  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  # simulation waveform
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

  wave -expand         -height $h_mod  -color $c_mod           -vgroup  "$module_name"                                               \
                                                                                                                                     \
             (         -height $h_nul  -color $c_nul           -divider {-------------------------------------------------------} )  \
                                                                                                                                     \
             ( -expand -height $h_grp  -color $c_grp           -vgroup  "parameters"                                                 \
                                                                                                                                     \
                     ( -height $h_ch1  -color $c_ch1           -divider {- packet structure -}                                    )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {frame length}                                            )  \
                                                                                                                                     \
                     ( -height $h_par  -color $c_par  -$r_uns  -label   {req: pW_LAN_LEN        } $module_path/pW_LAN_LEN         )  \
                     ( -height $h_par  -color $c_par  -$r_uns  -label   {req: pW_LAN_GAP        } $module_path/pW_LAN_GAP         )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {ip header}                                               )  \
                                                                                                                                     \
                     ( -height $h_par  -color $c_par  -$r_uns  -label   {opt: pN_LAN_IPL        } $module_path/pN_LAN_IPL         )  \
                     ( -height $h_par  -color $c_par  -$r_uns  -label   {opt: pN_LAN_IPH        } $module_path/pN_LAN_IPH         )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {payload}                                                 )  \
                                                                                                                                     \
                     ( -height $h_par  -color $c_par  -$r_uns  -label   {req: pW_LAN_PAY        } $module_path/pW_LAN_PAY         )  \
                                                                                                                                     \
                     ( -height $h_ch1  -color $c_ch1           -divider {- local (for i/o) -}                                     )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {packet structure}                                        )  \
                                                                                                                                     \
                     ( -height $h_par  -color $c_par  -$r_uns  -label   {opt: lpN_LAN_IPL       } $module_path/lpN_LAN_IPL        )  \
                     ( -height $h_par  -color $c_par  -$r_uns  -label   {opt: lpN_LAN_IPH       } $module_path/lpN_LAN_IPH        )  \
                     ( -height $h_par  -color $c_par  -$r_uns  -label   {opt: lpN_LAN_IP        } $module_path/lpN_LAN_IP         )  \
                     ( -height $h_par  -color $c_par  -$r_uns  -label   {opt: lpW_LAN_IP        } $module_path/lpW_LAN_IP         )  \
                     ( -height $h_par  -color $c_par  -$r_uns  -label   {opt: lpW_LAN_IP_C      } $module_path/lpW_LAN_IP_C       )  \
                                                                                                                                     \
                     ( -height $h_nul  -color $c_nul           -divider {-------------------------------------------------------} )  \
             )                                                                                                                       \
             ( -expand -height $h_grp  -color $c_grp           -vgroup  "interface"                                                  \
                                                                                                                                     \
                     ( -height $h_ch1  -color $c_ch1           -divider {- clear -}                                               )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {opt: i_sclr_n          } $module_path/i_sclr_n           )  \
                                                                                                                                     \
                     ( -height $h_ch1  -color $c_ch1           -divider {- setting -}                                             )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {initialization}                                          )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {req: i_set_ini         } $module_path/i_set_ini          )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {mac header}                                              )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {opt: i_set_mac_ena     } $module_path/i_set_mac_ena      )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {mac header / mda}                                        )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {opt: i_set_mda_type    } $module_path/i_set_mda_type     )  \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {opt: i_set_mda_const   } $module_path/i_set_mda_const    )  \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {opt: i_set_mda_min     } $module_path/i_set_mda_min      )  \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {opt: i_set_mda_max     } $module_path/i_set_mda_max      )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {mac header / msa}                                        )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {opt: i_set_msa_type    } $module_path/i_set_msa_type     )  \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {opt: i_set_msa_const   } $module_path/i_set_msa_const    )  \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {opt: i_set_msa_min     } $module_path/i_set_msa_min      )  \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {opt: i_set_msa_max     } $module_path/i_set_msa_max      )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {mac header / mlt}                                        )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {opt: i_set_mlt_type    } $module_path/i_set_mlt_type     )  \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {opt: i_set_mlt_const   } $module_path/i_set_mlt_const    )  \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {opt: i_set_mlt_min     } $module_path/i_set_mlt_min      )  \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {opt: i_set_mlt_max     } $module_path/i_set_mlt_max      )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {ip header}                                               )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {opt: i_set_ip_ena      } $module_path/i_set_ip_ena       )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {opt: i_set_ip_type     } $module_path/i_set_ip_type      )  \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {opt: i_set_ip_const    } $module_path/i_set_ip_const     )  \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {opt: i_set_ip_min      } $module_path/i_set_ip_min       )  \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {opt: i_set_ip_max      } $module_path/i_set_ip_max       )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {payload}                                                 )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {req: i_set_pay_type    } $module_path/i_set_pay_type     )  \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {req: i_set_pay_const   } $module_path/i_set_pay_const    )  \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {opt: i_set_pay_min     } $module_path/i_set_pay_min      )  \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {opt: i_set_pay_max     } $module_path/i_set_pay_max      )  \
                                                                                                                                     \
                     ( -height $h_ch1  -color $c_ch1           -divider {- lan packet -}                                          )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {clock}                                                   )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {req: i_clk             } $module_path/i_clk              )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {req: i_cen             } $module_path/i_cen              )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {req: i_ced             } $module_path/i_ced              )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {strob}                                                   )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {req: i_str_frame       } $module_path/i_str_frame(0)     )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {tst: i_cnt_frame       } $module_path/i_cnt_frame        )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {req: i_str_head        } $module_path/i_str_head(0)      )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {tst: i_cnt_head        } $module_path/i_cnt_head         )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {req: i_str_prm         } $module_path/i_str_prm(0)       )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {req: i_str_sfd         } $module_path/i_str_sfd(0)       )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {tst: i_str_body        } $module_path/i_str_body(0)      )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {tst: i_cnt_body        } $module_path/i_cnt_body         )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {opt: i_str_mac         } $module_path/i_str_mac(0)       )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {tst: i_cnt_mac         } $module_path/i_cnt_mac          )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {opt: i_str_mda         } $module_path/i_str_mda(0)       )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {opt: i_str_msa         } $module_path/i_str_msa(0)       )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {opt: i_str_mlt         } $module_path/i_str_mlt(0)       )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {opt: i_str_ip          } $module_path/i_str_ip(0)        )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {tst: i_cnt_ip          } $module_path/i_cnt_ip           )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {req: i_str_pay         } $module_path/i_str_pay(0)       )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {tst: i_cnt_pay         } $module_path/i_cnt_pay          )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {req: i_str_crc         } $module_path/i_str_crc(0)       )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {req: i_cnt_crc         } $module_path/i_cnt_crc          )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {tst: i_str_gap         } $module_path/i_str_gap(0)       )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {tst: i_cnt_gap         } $module_path/i_cnt_gap          )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {data}                                                    )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {req: o_dat_load        } $module_path/o_dat_load         )  \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {req: o_dat_frame       } $module_path/o_dat_frame        )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {length}                                                  )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {tst: i_len_frame       } $module_path/i_len_frame        )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {tst: i_len_body        } $module_path/i_len_body         )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {req: i_len_pay         } $module_path/i_len_pay          )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {tst: i_len_gap         } $module_path/i_len_gap          )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {fcs crc}                                                 )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {opt: i_crc_rdy         } $module_path/i_crc_rdy          )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {opt: i_crc_err         } $module_path/i_crc_err          )  \
                                                                                                                                     \
                     ( -height $h_nul  -color $c_nul           -divider {-------------------------------------------------------} )  \
             )                                                                                                                       \
             ( -expand -height $h_grp  -color $c_grp           -vgroup  "logic"                                                      \
                                                                                                                                     \
                     ( -height $h_ch1  -color $c_ch1           -divider {- frmae header : data -}                                 )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {clock}                                                   )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_clk                  } $module_path/i_clk              )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_cen                  } $module_path/i_cen              )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_ced                  } $module_path/i_ced              )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {header / strob}                                          )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_str_head [0]         } $module_path/i_str_head(0)      )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_str_prm  [0]         } $module_path/i_str_prm(0)       )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_str_sfd  [0]         } $module_path/i_str_sfd(0)       )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {header / data}                                           )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {str_head               } $module_path/str_head           )  \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {dat_head               } $module_path/dat_head           )  \
                                                                                                                                     \
                     ( -height $h_ch1  -color $c_ch1           -divider {- mac header : data -}                                   )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {clock}                                                   )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_clk                  } $module_path/i_clk              )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_cen                  } $module_path/i_cen              )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_ced                  } $module_path/i_ced              )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {setting / mac}                                           )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_set_mda_ena          } $module_path/i_set_mda_ena      )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {setting / mda}                                           )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {i_set_mda_type         } $module_path/i_set_mda_type     )  \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {i_set_mda_const        } $module_path/i_set_mda_const    )  \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {i_set_mda_min          } $module_path/i_set_mda_min      )  \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {i_set_mda_max          } $module_path/i_set_mda_max      )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {setting / msa}                                           )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {i_set_msa_type         } $module_path/i_set_msa_type     )  \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {i_set_msa_const        } $module_path/i_set_msa_const    )  \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {i_set_msa_min          } $module_path/i_set_msa_min      )  \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {i_set_msa_max          } $module_path/i_set_msa_max      )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {setting / mlt}                                           )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {i_set_mlt_type         } $module_path/i_set_mlt_type     )  \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {i_set_mlt_const        } $module_path/i_set_mlt_const    )  \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {i_set_mlt_min          } $module_path/i_set_mlt_min      )  \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {i_set_mlt_max          } $module_path/i_set_mlt_max      )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {mac header / data}                                       )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {str_mac [1]            } $module_path/i_str_mac(1)       )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {str_mac [0]            } $module_path/i_str_mac(0)       )  \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {dat_mac                } $module_path/dat_mac            )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {str_mda [0]            } $module_path/i_str_mda(0)       )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {str_msa [0]            } $module_path/i_str_msa(0)       )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {str_mlt [0]            } $module_path/i_str_mlt(0)       )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {str_mac [2]            } $module_path/i_str_mac(2)       )  \
                                                                                                                                     \
                     ( -height $h_ch1  -color $c_ch1           -divider {- mda : data -}                                          )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {clock}                                                   )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_clk                  } $module_path/i_clk              )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_cen                  } $module_path/i_cen              )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_ced                  } $module_path/i_ced              )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {setting}                                                 )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_set_ini              } $module_path/i_set_mac_ena      )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_set_mac_ena          } $module_path/i_set_mac_ena      )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {i_set_mda_type         } $module_path/i_set_mda_type     )  \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {i_set_mda_const        } $module_path/i_set_mda_const    )  \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {i_set_mda_min          } $module_path/i_set_mda_min      )  \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {i_set_mda_max          } $module_path/i_set_mda_max      )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {strob}                                                   )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_str_mda [1]          } $module_path/i_str_mda(1)       )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_str_mda [0]          } $module_path/i_str_mda(0)       )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_str_mda [2]          } $module_path/i_str_mda(2)       )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {data}                                                    )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {dat_mda                } $module_path/dat_mda            )  \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {dat_mda_0              } $module_path/dat_mda_0          )  \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {dat_mda_1              } $module_path/dat_mda_1          )  \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {dat_mda_2              } $module_path/dat_mda_2          )  \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {dat_mda_3              } $module_path/dat_mda_3          )  \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {dat_mda_4              } $module_path/dat_mda_4          )  \
                                                                                                                                     \
                     ( -height $h_ch1  -color $c_ch1           -divider {- msa : data -}                                          )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {clock}                                                   )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_clk                  } $module_path/i_clk              )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_cen                  } $module_path/i_cen              )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_ced                  } $module_path/i_ced              )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {setting}                                                 )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_set_ini              } $module_path/i_set_mac_ena      )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_set_mac_ena          } $module_path/i_set_mac_ena      )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {i_set_msa_type         } $module_path/i_set_msa_type     )  \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {i_set_msa_const        } $module_path/i_set_msa_const    )  \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {i_set_msa_min          } $module_path/i_set_msa_min      )  \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {i_set_msa_max          } $module_path/i_set_msa_max      )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {strob}                                                   )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_str_msa [1]          } $module_path/i_str_msa(1)       )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_str_msa [0]          } $module_path/i_str_msa(0)       )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_str_msa [2]          } $module_path/i_str_msa(2)       )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {data}                                                    )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {dat_msa                } $module_path/dat_msa            )  \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {dat_msa_0              } $module_path/dat_msa_0          )  \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {dat_msa_1              } $module_path/dat_msa_1          )  \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {dat_msa_2              } $module_path/dat_msa_2          )  \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {dat_msa_3              } $module_path/dat_msa_3          )  \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {dat_msa_4              } $module_path/dat_msa_4          )  \
                                                                                                                                     \
                     ( -height $h_ch1  -color $c_ch1           -divider {- mlt : data -}                                          )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {clock}                                                   )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_clk                  } $module_path/i_clk              )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_cen                  } $module_path/i_cen              )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_ced                  } $module_path/i_ced              )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {setting}                                                 )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_set_ini              } $module_path/i_set_mac_ena      )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_set_mac_ena          } $module_path/i_set_mac_ena      )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {i_set_mlt_type         } $module_path/i_set_mlt_type     )  \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {i_set_mlt_const        } $module_path/i_set_mlt_const    )  \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {i_set_mlt_min          } $module_path/i_set_mlt_min      )  \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {i_set_mlt_max          } $module_path/i_set_mlt_max      )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {strob}                                                   )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_str_mlt [1]          } $module_path/i_str_mlt(1)       )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_str_mlt [0]          } $module_path/i_str_mlt(0)       )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_str_mlt [2]          } $module_path/i_str_mlt(2)       )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {length}                                                  )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {i_len_pay              } $module_path/i_len_pay          )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {data}                                                    )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {dat_mlt                } $module_path/dat_mlt            )  \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {dat_mlt_0              } $module_path/dat_mlt_0          )  \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {dat_mlt_1              } $module_path/dat_mlt_1          )  \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {dat_mlt_2              } $module_path/dat_mlt_2          )  \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {dat_mlt_3              } $module_path/dat_mlt_3          )  \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {dat_mlt_4              } $module_path/dat_mlt_4          )  \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {dat_mlt_5              } $module_path/dat_mlt_5          )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {dat_mlt_5_b1           } $module_path/dat_mlt_5_b1       )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {dat_mlt_5_b2           } $module_path/dat_mlt_5_b2       )  \
                                                                                                                                     \
                     ( -height $h_ch1  -color $c_ch1           -divider {- ip header : data -}                                    )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {clock}                                                   )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_clk                  } $module_path/i_clk              )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_cen                  } $module_path/i_cen              )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_ced                  } $module_path/i_ced              )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {setting}                                                 )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_set_ini              } $module_path/i_set_mac_ena      )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_set_ip_ena           } $module_path/i_set_ip_ena       )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {i_set_ip_type          } $module_path/i_set_ip_type      )  \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {i_set_ip_const         } $module_path/i_set_ip_const     )  \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {i_set_ip_min           } $module_path/i_set_ip_min       )  \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {i_set_ip_max           } $module_path/i_set_ip_max       )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {strob}                                                   )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_str_ip [1]           } $module_path/i_str_ip(1)        )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_str_ip [0]           } $module_path/i_str_ip(0)        )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_str_ip [2]           } $module_path/i_str_ip(2)        )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {data}                                                    )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {dat_ip                 } $module_path/dat_ip             )  \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {dat_ip_0               } $module_path/dat_ip_0           )  \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {dat_ip_1               } $module_path/dat_ip_1           )  \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {dat_ip_2               } $module_path/dat_ip_2           )  \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {dat_ip_3               } $module_path/dat_ip_3           )  \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {dat_ip_4               } $module_path/dat_ip_4           )  \
                                                                                                                                     \
                     ( -height $h_ch1  -color $c_ch1           -divider {- payload : data -}                                      )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {clock}                                                   )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_clk                  } $module_path/i_clk              )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_cen                  } $module_path/i_cen              )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_ced                  } $module_path/i_ced              )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {setting}                                                 )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_set_ini              } $module_path/i_set_mac_ena      )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {i_set_pay_type         } $module_path/i_set_pay_type     )  \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {i_set_pay_const        } $module_path/i_set_pay_const    )  \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {i_set_pay_min          } $module_path/i_set_pay_min      )  \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {i_set_pay_max          } $module_path/i_set_pay_max      )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {strob}                                                   )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_str_pay [1]          } $module_path/i_str_pay(1)       )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_str_pay [0]          } $module_path/i_str_pay(0)       )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_str_pay [2]          } $module_path/i_str_pay(2)       )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {data pay}                                                )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {dat_pay                } $module_path/dat_pay            )  \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {dat_pay_0              } $module_path/dat_pay_0          )  \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {dat_pay_1              } $module_path/dat_pay_1          )  \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {dat_pay_2              } $module_path/dat_pay_2          )  \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {dat_pay_3              } $module_path/dat_pay_3          )  \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {dat_pay_4              } $module_path/gen_dat_pay_4      )  \
                                                                                                                                     \
                     ( -height $h_ch1  -color $c_ch1           -divider {- fcs : data -}                                          )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {clock}                                                   )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_clk                  } $module_path/i_clk              )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_cen                  } $module_path/i_cen              )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_ced                  } $module_path/i_ced              )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {crc sum}                                                 )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {crc_sum                } $module_path/crc_sum            )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {crc error}                                               )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {crc_err                } $module_path/crc_err            )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {crc data}                                                )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {str_crc [1]            } $module_path/i_str_crc(1)       )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {str_crc [0]            } $module_path/i_str_crc(0)       )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {str_crc [2]            } $module_path/i_str_crc(2)       )  \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {dat_crc                } $module_path/dat_crc            )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {cnt_crc                } $module_path/cnt_crc            )  \
                                                                                                                                     \
                     ( -height $h_ch1  -color $c_ch1           -divider {- fcs : calculator -}                                    )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {clock}                                                   )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_clk                  } $module_path/i_clk              )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_cen                  } $module_path/i_cen              )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_ced                  } $module_path/i_ced              )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {frame}                                                   )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {str_mac[0]             } $module_path/i_str_mac(0)       )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {str_pay[0]             } $module_path/i_str_pay(0)       )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {str_frame[0]           } $module_path/i_str_frame(0)     )  \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {dat_frame              } $module_path/dat_frame          )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {str_frame[2]           } $module_path/i_str_frame(2)     )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {calculator}                                              )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {crc_calc               } $module_path/crc_calc           )  \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {crc_dec                } $module_path/crc_dec            )  \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {crc_sum                } $module_path/crc_sum            )  \
                                                                                                                                     \
                     ( -height $h_nul  -color $c_nul           -divider {-------------------------------------------------------} )  \
             )                                                                                                                       \
             ( -expand -height $h_grp  -color $c_grp           -vgroup  "all wave"                                                   \
                     ( -height $h_sig  -color $c_sig                                              $module_path/*                  )  \
                                                                                                                                     \
                     ( -height $h_nul  -color $c_nul           -divider {-------------------------------------------------------} )  \
             )                                                                                                                       \
             (         -height $h_nul  -color $c_nul           -divider {-------------------------------------------------------} )  \

  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#




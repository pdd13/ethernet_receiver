  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  #
  #  Company      : Micran, Department of Telecommunications
  #
  #  Engineer     : Pogodaev Danil
  #
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  #
  #  Library      : lib_ethernet / library of ethernet components
  #
  #  Component    : eth_rx_v2 / receiver of ethernet frames (packets)
  #
  #  File Name    : wave_tbg_1_3 .do (tcl sim aldec)
  #
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  #
  #  Description  : List of module waveforms for Aldec simulator.
  #
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  #
  #  Note (1)     : Command 'wave'           Adds objects to the Waveform Viewer.
  #  
  #                                          If the window is not open yet, issuing the command first opens the viewer and then adds the specified objects to the Object View pane
  #                                          of the Waveform Viewer window. The order in which objects are added depends on the method you use to group or add them to the viewer window.
  #                                          If you use wildcards, objects are added in the same order they are declared in source code.
  #                                          If you issue a sequence of the wave commands, the objects are displayed in the order the commands have been executed.
  #
  #                  -expand                 Automatically expands the object added to the waveform.
  #
  #                  -vgroup                 Creates a virtual group. The name of the object is specified by the <name> argument.
  #                                          Virtual groups can be nested or they can contain other virtual objects: virtual arrays, virtual records, or named rows.
  #                                          You can create a virtual group that contains nested virtual objects or nested named rows by using the following syntax:
  #
  #                                          wave -vgroup <virtual_name> <signals> ( -vgroup <nested_virtual_name> <signals> ) <signals>
  #
  #                  -height <pixels>        Specifies the height of the signal waveform in pixels.
  #
  #                  -bold                   When this argument is passed, the name of the object listed in the Object View pane and its value displayed in the Waveform View pane
  #                                          will be presented in bold. The argument does not affect the display of object waves.
  #
  #                  -color <color>          Specifies the color of the signal name in the Object View pane. The color can be either an RGB value or a name.
  #
  #                                          The RGB value should be specified with a hash sign (#) followed by hex digits. One, two, three, or four digits can be used for each color.
  #                                          The RGB value can be specified with three decimal values separated by commas.
  #
  #                                          Definitions of color names (e.g. steelblue or gainsboro) have been provided by the World Wide Web Consortium and are available at
  #                                          www.w3.org/TR/SVG/types.html#ColorKeywords. Color transparent represents the absence of a color.
  #
  #                  -color_waveform         Applies the color specified with the -color argument to the timing waveform in the Waveform View pane.
  #
  #                  -ascii                  Specifies how the waveform of the signal should be displayed.
  #                  -binary                 You can use different radices and different notations for the decimal radix. If radix is not specified, the radix returned
  #                  -decimal                by the radix command is used. Available radices include ASCII (-ascii), binary (-binary), octal (-octal), decimal (-decimal),
  #                  -unsigned               hexadecimal (-hexadecimal), and floating-point (-floatingpoint). It is assumed that values displayed in the decimal radix (-decimal)
  #                  -hexadecimal            use two's complement notation.
  #
  #                                          The notation for the decimal radix can be specified explicitly.
  #                                          Available options include unsigned (-unsigned) one's complement (-1complement), two's complement (-2complement), and signed magnitude
  #                                          (-magnitude). Note that options -decimal and -2complement are equivalent. When the decimal notation is selected, you can specify
  #                                          a fixed-point notation by using the -binarypoint argument.
  #
  #                                          The floating-point representation adheres to IEEE Std 754 and is available for floating-point variables and for 64-bit or 32-bit
  #                                          vectors only.
  #
  #                  -named_row | -divider   Inserts a named row or a row separator to the waveform window. The name of the object is specified by the <name> argument.
  #                                          Named rows and row dividers do not show timing waveforms. They are used to improve the legibility of data shown in the waveform window.
  #                                          For example, you can add a row displaying the string MEMORY PORTS above the ports of a memory unit:
  #
  #                                          If the name of the row contains spaces, it should be written with quotation marks.
  #                                          Note that specifying the space character instead of the row name will result in inserting a row without any visible name
  #                                          (inserts an empty row).
  #
  #                  -label <name>           Renames an object added to the waveform. The <name> parameter specifies the name which will replace the original name.
  #                                          To rename more than one object, use the wave command with the -label argument for each object separately. The name is set only for
  #                                          the display purposes. Even if the object is renamed, it can be referred to (for example by the force or examine commands) only
  #                                          by using the original name.
  #
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  #
  #  Revision List
  #
  #  Version   Date       Author
  #
  #  2.0       26.09.16   Pogodaev Danil
  #
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  # module setting
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

  set module_name "tbg 1.3 : emu_str"

  set module_path "/tb_top/tb_gen/eth_emu/emu_str"

  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  # simulation waveform
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

  wave -expand         -height $h_mod  -color $c_mod           -vgroup  "$module_name"                                               \
                                                                                                                                     \
             (         -height $h_nul  -color $c_nul           -divider {-------------------------------------------------------} )  \
                                                                                                                                     \
             ( -expand -height $h_grp  -color $c_grp           -vgroup  "parameters"                                                 \
                                                                                                                                     \
                     ( -height $h_ch1  -color $c_ch1           -divider {- packet structure -}                                    )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {frame header}                                            )  \
                                                                                                                                     \
                     ( -height $h_par  -color $c_par  -$r_uns  -label   {req: pE_LAN_HEAD       } $module_path/pE_LAN_HEAD        )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {frame length}                                            )  \
                                                                                                                                     \
                     ( -height $h_par  -color $c_par  -$r_uns  -label   {req: pW_LAN_LEN        } $module_path/pW_LAN_LEN         )  \
                     ( -height $h_par  -color $c_par  -$r_uns  -label   {req: pW_LAN_GAP        } $module_path/pW_LAN_GAP         )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {ip header}                                               )  \
                                                                                                                                     \
                     ( -height $h_par  -color $c_par  -$r_uns  -label   {opt: pN_LAN_IPL        } $module_path/pN_LAN_IPL         )  \
                     ( -height $h_par  -color $c_par  -$r_uns  -label   {opt: pN_LAN_IPH        } $module_path/pN_LAN_IPH         )  \
                                                                                                                                     \
                     ( -height $h_ch1  -color $c_ch1           -divider {- packet counters -}                                     )  \
                                                                                                                                     \
                     ( -height $h_par  -color $c_par  -$r_uns  -label   {opt: pN_LAN_CNT        } $module_path/pN_LAN_CNT         )  \
                     ( -height $h_par  -color $c_par  -$r_uns  -label   {opt: pW_LAN_CNT        } $module_path/pW_LAN_CNT         )  \
                                                                                                                                     \
                     ( -height $h_ch1  -color $c_ch1           -divider {- local (for i/o) -}                                     )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {packet structure}                                        )  \
                                                                                                                                     \
                     ( -height $h_par  -color $c_par  -$r_uns  -label   {opt: lpN_LAN_IP        } $module_path/lpN_LAN_IP         )  \
                     ( -height $h_par  -color $c_par  -$r_uns  -label   {opt: lpW_LAN_IP_C      } $module_path/lpW_LAN_IP_C       )  \
                                                                                                                                     \
                     ( -height $h_ch1  -color $c_ch1           -divider {- local (for logic) -}                                   )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {multi strob}                                             )  \
                                                                                                                                     \
                     ( -height $h_par  -color $c_par  -$r_uns  -label   {req: lpN_STR_GEN       } $module_path/lpN_STR_GEN        )  \
                     ( -height $h_par  -color $c_par  -$r_uns  -label   {req: lpW_STR_GEN       } $module_path/lpW_STR_GEN        )  \
                                                                                                                                     \
                     ( -height $h_nul  -color $c_nul           -divider {-------------------------------------------------------} )  \
             )                                                                                                                       \
             ( -expand -height $h_grp  -color $c_grp           -vgroup  "interface"                                                  \
                                                                                                                                     \
                     ( -height $h_ch1  -color $c_ch1           -divider {- clear -}                                               )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {opt: i_sclr_n          } $module_path/i_sclr_n           )  \
                                                                                                                                     \
                     ( -height $h_ch1  -color $c_ch1           -divider {- setting -}                                             )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {initialization}                                          )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {req: i_set_ini         } $module_path/i_set_ini          )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {interface}                                               )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {opt: i_set_io_pause    } $module_path/i_set_io_pause     )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {transmit speed}                                          )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {opt: i_set_tx_mode     } $module_path/i_set_tx_mode      )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {opt: i_set_tx_gap      } $module_path/i_set_tx_gap       )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {crc error}                                               )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {opt: i_set_err_ena     } $module_path/i_set_err_ena      )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {opt: i_set_err_per     } $module_path/i_set_err_per      )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {mac header}                                              )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {opt: i_set_mac_ena     } $module_path/i_set_mac_ena      )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {ip header}                                               )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {opt: i_set_ip_ena      } $module_path/i_set_ip_ena       )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {frame length}                                            )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {req: i_set_len_type    } $module_path/i_set_len_type     )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {req: i_set_len_const   } $module_path/i_set_len_const    )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {opt: i_set_len_min     } $module_path/i_set_len_min      )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {opt: i_set_len_max     } $module_path/i_set_len_max      )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {frame gap}                                               )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {req: i_set_gap_type    } $module_path/i_set_gap_type     )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {req: i_set_gap_const   } $module_path/i_set_gap_const    )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {opt: i_set_gap_min     } $module_path/i_set_gap_min      )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {opt: i_set_gap_max     } $module_path/i_set_gap_max      )  \
                                                                                                                                     \
                     ( -height $h_ch1  -color $c_ch1           -divider {- lan packet -}                                          )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {clock}                                                   )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {req: i_clk             } $module_path/i_clk              )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {req: i_cen             } $module_path/i_cen              )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {req: i_ced             } $module_path/i_ced              )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {strob}                                                   )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {req: o_str_sop         } $module_path/o_str_frame(1)     )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {opt: o_str_frame       } $module_path/o_str_frame(0)     )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {opt: o_cnt_frame       } $module_path/o_cnt_frame        )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {opt: o_str_head        } $module_path/o_str_head(0)      )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {opt: o_cnt_head        } $module_path/o_cnt_head         )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {opt: o_str_prm         } $module_path/o_str_prm(0)       )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {opt: o_str_sfd         } $module_path/o_str_sfd(0)       )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {opt: o_str_body        } $module_path/o_str_body(0)      )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {opt: o_cnt_body        } $module_path/o_cnt_body         )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {opt: o_str_mac         } $module_path/o_str_mac(0)       )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {opt: o_cnt_mac         } $module_path/o_cnt_mac          )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {opt: o_str_mda         } $module_path/o_str_mda(0)       )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {opt: o_str_msa         } $module_path/o_str_msa(0)       )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {opt: o_str_mlt         } $module_path/o_str_mlt(0)       )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {opt: o_str_pay         } $module_path/o_str_pay(0)       )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {opt: o_cnt_pay         } $module_path/o_cnt_pay          )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {opt: o_str_ip          } $module_path/o_str_ip(0)        )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {opt: o_cnt_ip          } $module_path/o_cnt_ip           )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {opt: o_str_crc         } $module_path/o_str_crc(0)       )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {opt: o_cnt_crc         } $module_path/o_cnt_crc          )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {opt: o_str_gap         } $module_path/o_str_gap(0)       )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {opt: o_cnt_gap         } $module_path/o_cnt_gap          )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {req: o_str_eop         } $module_path/o_str_frame(2)     )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {length}                                                  )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {req: o_len_frame       } $module_path/o_len_frame        )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {opt: o_len_body        } $module_path/o_len_body         )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {opt: o_len_pay         } $module_path/o_len_pay          )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {opt: o_len_gap         } $module_path/o_len_gap          )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {fcs crc}                                                 )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {req: o_crc_rdy         } $module_path/o_crc_rdy          )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {req: o_crc_err         } $module_path/o_crc_err          )  \
                                                                                                                                     \
                     ( -height $h_ch1  -color $c_ch1           -divider {- lan counters -}                                        )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {setting}                                                 )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {opt: i_set_p_top       } $module_path/i_set_p_top        )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {opt: i_set_p_low       } $module_path/i_set_p_low        )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {detector}                                                )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {opt: o_det_p_rdy       } $module_path/o_det_p_rdy        )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {opt: o_det_p_err       } $module_path/o_det_p_err        )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {counters}                                                )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {opt: i_cnt_p_clr       } $module_path/i_cnt_p_clr        )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {opt: o_cnt_p_rdy       } $module_path/o_cnt_p_rdy        )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {opt: o_cnt_p_all       } $module_path/o_cnt_p_all        )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {opt: o_cnt_p_err       } $module_path/o_cnt_p_err        )  \
                                                                                                                                     \
                     ( -height $h_nul  -color $c_nul           -divider {-------------------------------------------------------} )  \
             )                                                                                                                       \
             ( -expand -height $h_grp  -color $c_grp           -vgroup  "logic"                                                      \
                                                                                                                                     \
                     ( -height $h_ch1  -color $c_ch1           -divider {- start of emulator -}                                   )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {clock}                                                   )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_clk                  } $module_path/i_clk              )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_cen                  } $module_path/i_cen              )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_ced                  } $module_path/i_ced              )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {setting}                                                 )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_set_ini              } $module_path/i_set_ini          )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {frame (packet)}                                          )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {str_frame              } $module_path/str_frame(0)       )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {str_gap                } $module_path/str_gap(0)         )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {start / first}                                           )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {start_idle             } $module_path/start_idle         )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {start_idle_re          } $module_path/start_idle_re      )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {start_first            } $module_path/start_first        )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {start}                                                   )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {start_nxt              } $module_path/start_nxt          )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {start                  } $module_path/start              )  \
                                                                                                                                     \
                     ( -height $h_ch1  -color $c_ch1           -divider {- frame (packet) : length -}                             )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {clock}                                                   )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_clk                  } $module_path/i_clk              )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_cen                  } $module_path/i_cen              )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_ced                  } $module_path/i_ced              )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {setting / ini}                                           )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_set_ini              } $module_path/i_set_ini          )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {setting / length}                                        )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {i_set_len_type         } $module_path/i_set_len_type     )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {i_set_len_const        } $module_path/i_set_len_const    )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {i_set_len_min          } $module_path/i_set_len_min      )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {i_set_len_max          } $module_path/i_set_len_max      )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {start}                                                   )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {start_nxt              } $module_path/start_nxt          )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {start                  } $module_path/start              )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {length}                                                  )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {len_frame_nxt          } $module_path/len_frame_nxt      )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {len_frame              } $module_path/len_frame          )  \
                                                                                                                                     \
                     ( -height $h_ch1  -color $c_ch1           -divider {- frame body : length -}                                 )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {clock}                                                   )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_clk                  } $module_path/i_clk              )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_cen                  } $module_path/i_cen              )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_ced                  } $module_path/i_ced              )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {length / constant}                                       )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {len_head               } $module_path/len_head           )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {len_mac                } $module_path/len_mac            )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {len_crc                } $module_path/len_crc            )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {length / next to}                                        )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {len_body_nxt           } $module_path/len_body_nxt       )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {len_pay_nxt            } $module_path/len_pay_nxt        )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {length / sync}                                           )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {len_body               } $module_path/len_body           )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {len_pay                } $module_path/len_pay            )  \
                                                                                                                                     \
                     ( -height $h_ch1  -color $c_ch1           -divider {- frame gap : length -}                                  )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {clock}                                                   )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_clk                  } $module_path/i_clk              )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_cen                  } $module_path/i_cen              )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_ced                  } $module_path/i_ced              )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {setting / ini}                                           )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_set_ini              } $module_path/i_set_ini          )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {setting / speed}                                         )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {i_set_tx_mode          } $module_path/i_set_tx_mode      )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {i_set_tx_gap           } $module_path/i_set_tx_gap       )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {setting / gap}                                           )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {i_set_gap_type         } $module_path/i_set_gap_type     )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {i_set_gap_const        } $module_path/i_set_gap_const    )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {i_set_gap_min          } $module_path/i_set_gap_min      )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {i_set_gap_max          } $module_path/i_set_gap_max      )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {start}                                                   )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {start_nxt              } $module_path/start_nxt          )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {start                  } $module_path/start              )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {length}                                                  )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {len_gap                } $module_path/len_gap            )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {len_gap_nxt            } $module_path/len_gap_nxt        )  \
                                                                                                                                     \
                     ( -height $h_ch1  -color $c_ch1           -divider {- frame (packet) : multi str -}                          )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {clock}                                                   )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_clk                  } $module_path/mstr/i_clk         )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_clk_en               } $module_path/mstr/i_clk_en      )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_sclr                 } $module_path/mstr/i_sclr        )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {control}                                                 )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_start                } $module_path/mstr/i_start       )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {i_length               } $module_path/mstr/i_length      )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_reset                } $module_path/mstr/i_reset       )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {generator}                                               )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {o_start                } $module_path/mstr/o_start       )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {o_stop                 } $module_path/mstr/o_stop        )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {o_strob                } $module_path/mstr/o_strob       )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {o_count_up             } $module_path/mstr/o_count_up    )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {o_count_dn             } $module_path/mstr/o_count_dn    )  \
                                                                                                                                     \
                     ( -height $h_ch1  -color $c_ch1           -divider {- frame header : strob -}                                )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {parameters}                                              )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {pE_LAN_HEAD            } $module_path/pE_LAN_HEAD        )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {clock}                                                   )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_clk                  } $module_path/i_clk              )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_cen                  } $module_path/i_cen              )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_ced                  } $module_path/i_ced              )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {preamble / strob}                                        )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {str_prm [1]            } $module_path/str_prm(1)         )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {str_prm [0]            } $module_path/str_prm(0)         )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {str_prm [2]            } $module_path/str_prm(2)         )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {sfd / strob}                                             )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {str_sfd [1]            } $module_path/str_sfd(1)         )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {str_sfd [0]            } $module_path/str_sfd(0)         )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {str_sfd [2]            } $module_path/str_sfd(2)         )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {header / strob}                                          )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {str_head [1]           } $module_path/str_head(1)        )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {str_head [0]           } $module_path/str_head(0)        )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {str_head [2]           } $module_path/str_head(2)        )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {header / counter}                                        )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {cnt_head_nxt           } $module_path/cnt_head_nxt       )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {cnt_head               } $module_path/cnt_head           )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {header / nulling}                                        )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {str_head_x             } $module_path/str_head_x(0)      )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {cnt_head_x             } $module_path/cnt_head_x         )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {str_prm_x              } $module_path/str_prm_x(0)       )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {str_sfd_x              } $module_path/str_sfd_x(0)       )  \
                                                                                                                                     \
                     ( -height $h_ch1  -color $c_ch1           -divider {- mac header : strob -}                                  )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {clock}                                                   )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_clk                  } $module_path/i_clk              )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_cen                  } $module_path/i_cen              )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_ced                  } $module_path/i_ced              )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {setting}                                                 )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_set_mac_ena          } $module_path/i_set_mac_ena      )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {mda / strob}                                             )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {str_mda [1]            } $module_path/str_mda(1)         )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {str_mda [0]            } $module_path/str_mda(0)         )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {str_mda [2]            } $module_path/str_mda(2)         )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {msa / strob}                                             )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {str_msa [1]            } $module_path/str_msa(1)         )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {str_msa [0]            } $module_path/str_msa(0)         )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {str_msa [2]            } $module_path/str_msa(2)         )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {mlt / strob}                                             )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {str_mlt [1]            } $module_path/str_mlt(1)         )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {str_mlt [0]            } $module_path/str_mlt(0)         )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {str_mlt [2]            } $module_path/str_mlt(2)         )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {mac / strob}                                             )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {str_mac [1]            } $module_path/str_mac(1)         )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {str_mac [0]            } $module_path/str_mac(0)         )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {str_mac [2]            } $module_path/str_mac(2)         )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {mac / counter}                                           )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {cnt_mac_nxt            } $module_path/cnt_mac_nxt        )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {cnt_mac                } $module_path/cnt_mac            )  \
                                                                                                                                     \
                     ( -height $h_ch1  -color $c_ch1           -divider {- payload : strob -}                                     )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {clock}                                                   )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_clk                  } $module_path/i_clk              )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_cen                  } $module_path/i_cen              )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_ced                  } $module_path/i_ced              )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {payload / strob}                                         )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {str_pay [1]            } $module_path/str_pay(1)         )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {str_pay [0]            } $module_path/str_pay(0)         )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {str_pay [2]            } $module_path/str_pay(2)         )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {payload / counter}                                       )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {cnt_pay_nxt            } $module_path/cnt_pay_nxt        )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {cnt_pay                } $module_path/cnt_pay            )  \
                                                                                                                                     \
                     ( -height $h_ch1  -color $c_ch1           -divider {- ip header : strob -}                                   )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {parameters}                                              )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {pN_LAN_IPL             } $module_path/pN_LAN_IPL         )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {pN_LAN_IPH             } $module_path/pN_LAN_IPH         )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {clock}                                                   )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_clk                  } $module_path/i_clk              )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_cen                  } $module_path/i_cen              )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_ced                  } $module_path/i_ced              )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {setting}                                                 )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_set_ip_ena           } $module_path/i_set_ip_ena       )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {payload}                                                 )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {str_pay [1]            } $module_path/str_pay(1)         )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {str_pay [0]            } $module_path/str_pay(0)         )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {cnt_pay                } $module_path/cnt_pay            )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {ip / strob}                                              )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {str_ip [1]             } $module_path/str_ip(1)          )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {str_ip [0]             } $module_path/str_ip(0)          )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {str_ip [2]             } $module_path/str_ip(2)          )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {ip / counter}                                            )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {cnt_ip_nxt             } $module_path/cnt_ip_nxt         )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {cnt_ip                 } $module_path/cnt_ip             )  \
                                                                                                                                     \
                     ( -height $h_ch1  -color $c_ch1           -divider {- fsc crc : strob -}                                     )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {clock}                                                   )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_clk                  } $module_path/i_clk              )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_cen                  } $module_path/i_cen              )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_ced                  } $module_path/i_ced              )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {fcs / strob}                                             )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {str_crc [1]            } $module_path/str_crc(1)         )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {str_crc [0]            } $module_path/str_crc(0)         )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {str_crc [2]            } $module_path/str_crc(2)         )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {fcs / counter}                                           )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {cnt_crc_nxt            } $module_path/cnt_crc_nxt        )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {cnt_crc                } $module_path/cnt_crc            )  \
                                                                                                                                     \
                     ( -height $h_ch1  -color $c_ch1           -divider {- frame body : strob -}                                  )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {clock}                                                   )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_clk                  } $module_path/i_clk              )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_cen                  } $module_path/i_cen              )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_ced                  } $module_path/i_ced              )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {body / strob}                                            )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {str_body [1]           } $module_path/str_body(1)        )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {str_body [0]           } $module_path/str_body(0)        )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {str_body [2]           } $module_path/str_body(2)        )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {body / counter}                                          )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {cnt_body_nxt           } $module_path/cnt_body_nxt       )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {cnt_body               } $module_path/cnt_body           )  \
                                                                                                                                     \
                     ( -height $h_ch1  -color $c_ch1           -divider {- frame (packet) : strob -}                              )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {clock}                                                   )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_clk                  } $module_path/i_clk              )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_cen                  } $module_path/i_cen              )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_ced                  } $module_path/i_ced              )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {frame / strob}                                           )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {str_frame [1]          } $module_path/str_frame(1)       )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {str_frame [0]          } $module_path/str_frame(0)       )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {str_frame [2]          } $module_path/str_frame(2)       )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {frame / counter}                                         )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {cnt_frame_nxt          } $module_path/cnt_frame_nxt      )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {cnt_frame              } $module_path/cnt_frame          )  \
                                                                                                                                     \
                     ( -height $h_ch1  -color $c_ch1           -divider {- frame gap : strob -}                                   )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {clock}                                                   )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_clk                  } $module_path/i_clk              )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_cen                  } $module_path/i_cen              )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_ced                  } $module_path/i_ced              )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {start}                                                   )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {start_first            } $module_path/start_first        )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {gap / strob}                                             )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {str_gap [1]            } $module_path/str_gap(1)         )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {str_gap [0]            } $module_path/str_gap(0)         )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {str_gap [2]            } $module_path/str_gap(2)         )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {gap / stop count}                                        )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {len_gap_nxt            } $module_path/len_gap_nxt        )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {cnt_gap_stop           } $module_path/cnt_gap_stop       )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {gap / counter}                                           )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {cnt_gap_nxt            } $module_path/cnt_gap_nxt        )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {cnt_gap                } $module_path/cnt_gap            )  \
                                                                                                                                     \
                     ( -height $h_ch1  -color $c_ch1           -divider {- fcs crc : error emulate -}                             )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {clock}                                                   )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_clk                  } $module_path/i_clk              )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_cen                  } $module_path/i_cen              )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_ced                  } $module_path/i_ced              )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {setting}                                                 )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_set_err_ena          } $module_path/i_set_err_ena      )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {i_set_err_per          } $module_path/i_set_err_per      )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {start}                                                   )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {start_nxt              } $module_path/start_nxt          )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {start                  } $module_path/start              )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {error}                                                   )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {crc_err_nxt            } $module_path/crc_err_nxt        )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {crc_err                } $module_path/crc_err            )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {crc_rdy                } $module_path/crc_rdy            )  \
                                                                                                                                     \
                     ( -height $h_ch1  -color $c_ch1           -divider {- packet counters -}                                     )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {clock}                                                   )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_clk                  } $module_path/i_clk              )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_cen                  } $module_path/i_cen              )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_ced                  } $module_path/i_ced              )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {setting}                                                 )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_cnt_p_clr            } $module_path/i_cnt_p_clr        )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {i_cnt_p_low            } $module_path/i_cnt_p_low        )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {i_cnt_p_top            } $module_path/i_cnt_p_top        )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {start}                                                   )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {start                  } $module_path/start              )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {detector}                                                )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {det_p_rdy              } $module_path/det_p_rdy          )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {det_p_err              } $module_path/det_p_err          )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {frame body}                                              )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {len_body               } $module_path/len_body           )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {len_body_nxt           } $module_path/len_body_nxt       )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {str_body               } $module_path/str_body(0)        )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {counters}                                                )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {cnt_p_clr              } $module_path/cnt_p_clr          )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {cnt_p_rdy              } $module_path/cnt_p_rdy          )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {cnt_p_all              } $module_path/cnt_p_all          )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {cnt_p_err              } $module_path/cnt_p_err          )  \
                                                                                                                                     \
                     ( -height $h_nul  -color $c_nul           -divider {-------------------------------------------------------} )  \
             )                                                                                                                       \
             ( -expand -height $h_grp  -color $c_grp           -vgroup  "all wave"                                                   \
                     ( -height $h_sig  -color $c_sig                                              $module_path/*                  )  \
                                                                                                                                     \
                     ( -height $h_nul  -color $c_nul           -divider {-------------------------------------------------------} )  \
             )                                                                                                                       \
             (         -height $h_nul  -color $c_nul           -divider {-------------------------------------------------------} )  \

  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#




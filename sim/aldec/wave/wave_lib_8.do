  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  #
  #  Company      : Micran, Department of Telecommunications
  #
  #  Engineer     : Pogodaev Danil
  #
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  #
  #  Library      : lib_ethernet / library of ethernet components
  #
  #  Component    : eth_rx_v2 / receiver of ethernet frames (packets)
  #
  #  File Name    : wave_lib_8 .do (tcl sim aldec)
  #
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  #
  #  Description  : List of module waveforms for Aldec simulator.
  #
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  #
  #  Note (1)     : Command 'wave'           Adds objects to the Waveform Viewer.
  #  
  #                                          If the window is not open yet, issuing the command first opens the viewer and then adds the specified objects to the Object View pane
  #                                          of the Waveform Viewer window. The order in which objects are added depends on the method you use to group or add them to the viewer window.
  #                                          If you use wildcards, objects are added in the same order they are declared in source code.
  #                                          If you issue a sequence of the wave commands, the objects are displayed in the order the commands have been executed.
  #
  #                  -expand                 Automatically expands the object added to the waveform.
  #
  #                  -vgroup                 Creates a virtual group. The name of the object is specified by the <name> argument.
  #                                          Virtual groups can be nested or they can contain other virtual objects: virtual arrays, virtual records, or named rows.
  #                                          You can create a virtual group that contains nested virtual objects or nested named rows by using the following syntax:
  #
  #                                          wave -vgroup <virtual_name> <signals> ( -vgroup <nested_virtual_name> <signals> ) <signals>
  #
  #                  -height <pixels>        Specifies the height of the signal waveform in pixels.
  #
  #                  -bold                   When this argument is passed, the name of the object listed in the Object View pane and its value displayed in the Waveform View pane
  #                                          will be presented in bold. The argument does not affect the display of object waves.
  #
  #                  -color <color>          Specifies the color of the signal name in the Object View pane. The color can be either an RGB value or a name.
  #
  #                                          The RGB value should be specified with a hash sign (#) followed by hex digits. One, two, three, or four digits can be used for each color.
  #                                          The RGB value can be specified with three decimal values separated by commas.
  #
  #                                          Definitions of color names (e.g. steelblue or gainsboro) have been provided by the World Wide Web Consortium and are available at
  #                                          www.w3.org/TR/SVG/types.html#ColorKeywords. Color transparent represents the absence of a color.
  #
  #                  -color_waveform         Applies the color specified with the -color argument to the timing waveform in the Waveform View pane.
  #
  #                  -ascii                  Specifies how the waveform of the signal should be displayed.
  #                  -binary                 You can use different radices and different notations for the decimal radix. If radix is not specified, the radix returned
  #                  -decimal                by the radix command is used. Available radices include ASCII (-ascii), binary (-binary), octal (-octal), decimal (-decimal),
  #                  -unsigned               hexadecimal (-hexadecimal), and floating-point (-floatingpoint). It is assumed that values displayed in the decimal radix (-decimal)
  #                  -hexadecimal            use two's complement notation.
  #
  #                                          The notation for the decimal radix can be specified explicitly.
  #                                          Available options include unsigned (-unsigned) one's complement (-1complement), two's complement (-2complement), and signed magnitude
  #                                          (-magnitude). Note that options -decimal and -2complement are equivalent. When the decimal notation is selected, you can specify
  #                                          a fixed-point notation by using the -binarypoint argument.
  #
  #                                          The floating-point representation adheres to IEEE Std 754 and is available for floating-point variables and for 64-bit or 32-bit
  #                                          vectors only.
  #
  #                  -named_row | -divider   Inserts a named row or a row separator to the waveform window. The name of the object is specified by the <name> argument.
  #                                          Named rows and row dividers do not show timing waveforms. They are used to improve the legibility of data shown in the waveform window.
  #                                          For example, you can add a row displaying the string MEMORY PORTS above the ports of a memory unit:
  #
  #                                          If the name of the row contains spaces, it should be written with quotation marks.
  #                                          Note that specifying the space character instead of the row name will result in inserting a row without any visible name
  #                                          (inserts an empty row).
  #
  #                  -label <name>           Renames an object added to the waveform. The <name> parameter specifies the name which will replace the original name.
  #                                          To rename more than one object, use the wave command with the -label argument for each object separately. The name is set only for
  #                                          the display purposes. Even if the object is renamed, it can be referred to (for example by the force or examine commands) only
  #                                          by using the original name.
  #
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  #
  #  Revision List
  #
  #  Version   Date       Author
  #
  #  2.0       26.09.16   Pogodaev Danil
  #
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  # module setting
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

  set module_name "lib 8.0 : rx_dat"

  set module_path "/tb_top/eth_rx/rx_dat"

  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  # simulation waveform
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

  wave -expand         -height $h_mod  -color $c_mod           -vgroup  "$module_name"                                             \
                                                                                                                                   \
             (         -height $h_nul  -color $c_nul           -divider {-----------------------------------------------------} )  \
                                                                                                                                   \
             ( -expand -height $h_grp  -color $c_grp           -vgroup  "parameters"                                               \
                                                                                                                                   \
                     ( -height $h_ch1  -color $c_ch1           -divider {- packet structure -}                                  )  \
                                                                                                                                   \
                     ( -height $h_ch2  -color $c_ch2           -divider {mac header}                                            )  \
                                                                                                                                   \
                     ( -height $h_par  -color $c_par  -$r_uns  -label   {req: pE_RX_MAC     } $module_path/pE_RX_MAC            )  \
                                                                                                                                   \
                     ( -height $h_ch2  -color $c_ch2           -divider {ip header}                                             )  \
                                                                                                                                   \
                     ( -height $h_par  -color $c_par  -$r_uns  -label   {req: pE_RX_IP      } $module_path/pE_RX_IP             )  \
                     ( -height $h_par  -color $c_par  -$r_uns  -label   {req: pN_RX_IPL     } $module_path/pN_RX_IPL            )  \
                     ( -height $h_par  -color $c_par  -$r_uns  -label   {req: pN_RX_IPH     } $module_path/pN_RX_IPH            )  \
                                                                                                                                   \
                     ( -height $h_ch1  -color $c_ch1           -divider {- local (for i/o) -}                                   )  \
                                                                                                                                   \
                     ( -height $h_ch2  -color $c_ch2           -divider {packet structure}                                      )  \
                                                                                                                                   \
                     ( -height $h_par  -color $c_par  -$r_uns  -label   {req: lpN_RX_IPL    } $module_path/lpN_RX_IPL           )  \
                     ( -height $h_par  -color $c_par  -$r_uns  -label   {req: lpN_RX_IPH    } $module_path/lpN_RX_IPH           )  \
                     ( -height $h_par  -color $c_par  -$r_uns  -label   {req: lpN_RX_IP     } $module_path/lpN_RX_IP            )  \
                     ( -height $h_par  -color $c_par  -$r_uns  -label   {req: lpW_RX_IP     } $module_path/lpW_RX_IP            )  \
                                                                                                                                   \
                     ( -height $h_nul  -color $c_nul           -divider {-----------------------------------------------------} )  \
             )                                                                                                                     \
             ( -expand -height $h_grp  -color $c_grp           -vgroup  "interface"                                                \
                                                                                                                                   \
                     ( -height $h_ch1  -color $c_ch1           -divider {- clear -}                                             )  \
                                                                                                                                   \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {opt: i_sclr_n      } $module_path/i_sclr_n             )  \
                                                                                                                                   \
                     ( -height $h_ch1  -color $c_ch1           -divider {- clock -}                                             )  \
                                                                                                                                   \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {req: i_clk         } $module_path/i_clk                )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {req: i_cen         } $module_path/i_cen                )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {req: i_ced         } $module_path/i_ced                )  \
                                                                                                                                   \
                     ( -height $h_ch1  -color $c_ch1           -divider {- strob -}                                             )  \
                                                                                                                                   \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {req: i_str_mda     } $module_path/i_str_mda(0)         )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {req: i_str_msa     } $module_path/i_str_msa(0)         )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {req: i_str_mlt     } $module_path/i_str_mlt(0)         )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {req: i_str_ip      } $module_path/i_str_ip(0)          )  \
                                                                                                                                   \
                     ( -height $h_ch1  -color $c_ch1           -divider {- data / body -}                                       )  \
                                                                                                                                   \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {req: i_dat_load    } $module_path/i_dat_load           )  \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {req: i_dat_body    } $module_path/i_dat_body           )  \
                                                                                                                                   \
                     ( -height $h_ch1  -color $c_ch1           -divider {- data / mac -}                                        )  \
                                                                                                                                   \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {req: o_rdy_mac     } $module_path/o_rdy_mac            )  \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {req: o_dat_mda     } $module_path/o_dat_mda            )  \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {req: o_dat_msa     } $module_path/o_dat_msa            )  \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {req: o_dat_mlt     } $module_path/o_dat_mlt            )  \
                                                                                                                                   \
                     ( -height $h_ch1  -color $c_ch1           -divider {- data / ip -}                                         )  \
                                                                                                                                   \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {req: o_rdy_ip      } $module_path/o_rdy_ip             )  \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {req: o_dat_ip      } $module_path/o_dat_ip             )  \
                                                                                                                                   \
                     ( -height $h_nul  -color $c_nul           -divider {-----------------------------------------------------} )  \
             )                                                                                                                     \
             ( -expand -height $h_grp  -color $c_grp           -vgroup  "logic"                                                    \
                                                                                                                                   \
                     ( -height $h_ch1  -color $c_ch1           -divider {- data mda / shift -}                                  )  \
                                                                                                                                   \
                     ( -height $h_ch2  -color $c_ch2           -divider {clock}                                                 )  \
                                                                                                                                   \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_clk              } $module_path/shift_mda/i_clk      )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_clk_en           } $module_path/shift_mda/i_clk_en   )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_sclr             } $module_path/shift_mda/i_sclr     )  \
                                                                                                                                   \
                     ( -height $h_ch2  -color $c_ch2           -divider {serial}                                                )  \
                                                                                                                                   \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_sload            } $module_path/shift_mda/i_sload    )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_sload_en         } $module_path/shift_mda/i_sload_en )  \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {i_sdata            } $module_path/shift_mda/i_sdata    )  \
                                                                                                                                   \
                     ( -height $h_ch2  -color $c_ch2           -divider {parallel}                                              )  \
                                                                                                                                   \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {o_pload            } $module_path/shift_mda/o_pload    )  \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {o_pdata            } $module_path/shift_mda/o_pdata    )  \
                                                                                                                                   \
                     ( -height $h_ch1  -color $c_ch1           -divider {- data msa / shift -}                                  )  \
                                                                                                                                   \
                     ( -height $h_ch2  -color $c_ch2           -divider {clock}                                                 )  \
                                                                                                                                   \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_clk              } $module_path/shift_msa/i_clk      )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_clk_en           } $module_path/shift_msa/i_clk_en   )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_sclr             } $module_path/shift_msa/i_sclr     )  \
                                                                                                                                   \
                     ( -height $h_ch2  -color $c_ch2           -divider {serial}                                                )  \
                                                                                                                                   \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_sload            } $module_path/shift_msa/i_sload    )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_sload_en         } $module_path/shift_msa/i_sload_en )  \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {i_sdata            } $module_path/shift_msa/i_sdata    )  \
                                                                                                                                   \
                     ( -height $h_ch2  -color $c_ch2           -divider {parallel}                                              )  \
                                                                                                                                   \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {o_pload            } $module_path/shift_msa/o_pload    )  \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {o_pdata            } $module_path/shift_msa/o_pdata    )  \
                                                                                                                                   \
                     ( -height $h_ch1  -color $c_ch1           -divider {- data mlt / shift -}                                  )  \
                                                                                                                                   \
                     ( -height $h_ch2  -color $c_ch2           -divider {clock}                                                 )  \
                                                                                                                                   \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_clk              } $module_path/shift_mlt/i_clk      )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_clk_en           } $module_path/shift_mlt/i_clk_en   )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_sclr             } $module_path/shift_mlt/i_sclr     )  \
                                                                                                                                   \
                     ( -height $h_ch2  -color $c_ch2           -divider {serial}                                                )  \
                                                                                                                                   \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_sload            } $module_path/shift_mlt/i_sload    )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_sload_en         } $module_path/shift_mlt/i_sload_en )  \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {i_sdata            } $module_path/shift_mlt/i_sdata    )  \
                                                                                                                                   \
                     ( -height $h_ch2  -color $c_ch2           -divider {parallel}                                              )  \
                                                                                                                                   \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {o_pload            } $module_path/shift_mlt/o_pload    )  \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {o_pdata            } $module_path/shift_mlt/o_pdata    )  \
                                                                                                                                   \
                     ( -height $h_ch1  -color $c_ch1           -divider {- data ip / shift -}                                   )  \
                                                                                                                                   \
                     ( -height $h_ch2  -color $c_ch2           -divider {clock}                                                 )  \
                                                                                                                                   \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_clk              } $module_path/shift_ip/i_clk       )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_clk_en           } $module_path/shift_ip/i_clk_en    )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_sclr             } $module_path/shift_ip/i_sclr      )  \
                                                                                                                                   \
                     ( -height $h_ch2  -color $c_ch2           -divider {serial}                                                )  \
                                                                                                                                   \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_sload            } $module_path/shift_ip/i_sload     )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_sload_en         } $module_path/shift_ip/i_sload_en  )  \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {i_sdata            } $module_path/shift_ip/i_sdata     )  \
                                                                                                                                   \
                     ( -height $h_ch2  -color $c_ch2           -divider {parallel}                                              )  \
                                                                                                                                   \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {o_pload            } $module_path/shift_ip/o_pload     )  \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {o_pdata            } $module_path/shift_ip/o_pdata     )  \
                                                                                                                                   \
                     ( -height $h_nul  -color $c_nul           -divider {-----------------------------------------------------} )  \
             )                                                                                                                     \
             ( -expand -height $h_grp  -color $c_grp           -vgroup  "all wave"                                                 \
                     ( -height $h_sig  -color $c_sig                                          $module_path/*                    )  \
                                                                                                                                   \
                     ( -height $h_nul  -color $c_nul           -divider {-----------------------------------------------------} )  \
             )                                                                                                                     \
             (         -height $h_nul  -color $c_nul           -divider {-----------------------------------------------------} )  \

  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#




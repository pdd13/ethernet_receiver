  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  #
  #  Company      : Micran, Department of Telecommunications
  #
  #  Engineer     : Pogodaev Danil
  #
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  #
  #  Library      : lib_ethernet / library of ethernet components
  #
  #  Component    : eth_rx_v2 / receiver of ethernet frames (packets)
  #
  #  File Name    : wave_lib_9 .do (tcl sim aldec)
  #
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  #
  #  Description  : List of module waveforms for Aldec simulator.
  #
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  #
  #  Note (1)     : Command 'wave'           Adds objects to the Waveform Viewer.
  #  
  #                                          If the window is not open yet, issuing the command first opens the viewer and then adds the specified objects to the Object View pane
  #                                          of the Waveform Viewer window. The order in which objects are added depends on the method you use to group or add them to the viewer window.
  #                                          If you use wildcards, objects are added in the same order they are declared in source code.
  #                                          If you issue a sequence of the wave commands, the objects are displayed in the order the commands have been executed.
  #
  #                  -expand                 Automatically expands the object added to the waveform.
  #
  #                  -vgroup                 Creates a virtual group. The name of the object is specified by the <name> argument.
  #                                          Virtual groups can be nested or they can contain other virtual objects: virtual arrays, virtual records, or named rows.
  #                                          You can create a virtual group that contains nested virtual objects or nested named rows by using the following syntax:
  #
  #                                          wave -vgroup <virtual_name> <signals> ( -vgroup <nested_virtual_name> <signals> ) <signals>
  #
  #                  -height <pixels>        Specifies the height of the signal waveform in pixels.
  #
  #                  -bold                   When this argument is passed, the name of the object listed in the Object View pane and its value displayed in the Waveform View pane
  #                                          will be presented in bold. The argument does not affect the display of object waves.
  #
  #                  -color <color>          Specifies the color of the signal name in the Object View pane. The color can be either an RGB value or a name.
  #
  #                                          The RGB value should be specified with a hash sign (#) followed by hex digits. One, two, three, or four digits can be used for each color.
  #                                          The RGB value can be specified with three decimal values separated by commas.
  #
  #                                          Definitions of color names (e.g. steelblue or gainsboro) have been provided by the World Wide Web Consortium and are available at
  #                                          www.w3.org/TR/SVG/types.html#ColorKeywords. Color transparent represents the absence of a color.
  #
  #                  -color_waveform         Applies the color specified with the -color argument to the timing waveform in the Waveform View pane.
  #
  #                  -ascii                  Specifies how the waveform of the signal should be displayed.
  #                  -binary                 You can use different radices and different notations for the decimal radix. If radix is not specified, the radix returned
  #                  -decimal                by the radix command is used. Available radices include ASCII (-ascii), binary (-binary), octal (-octal), decimal (-decimal),
  #                  -unsigned               hexadecimal (-hexadecimal), and floating-point (-floatingpoint). It is assumed that values displayed in the decimal radix (-decimal)
  #                  -hexadecimal            use two's complement notation.
  #
  #                                          The notation for the decimal radix can be specified explicitly.
  #                                          Available options include unsigned (-unsigned) one's complement (-1complement), two's complement (-2complement), and signed magnitude
  #                                          (-magnitude). Note that options -decimal and -2complement are equivalent. When the decimal notation is selected, you can specify
  #                                          a fixed-point notation by using the -binarypoint argument.
  #
  #                                          The floating-point representation adheres to IEEE Std 754 and is available for floating-point variables and for 64-bit or 32-bit
  #                                          vectors only.
  #
  #                  -named_row | -divider   Inserts a named row or a row separator to the waveform window. The name of the object is specified by the <name> argument.
  #                                          Named rows and row dividers do not show timing waveforms. They are used to improve the legibility of data shown in the waveform window.
  #                                          For example, you can add a row displaying the string MEMORY PORTS above the ports of a memory unit:
  #
  #                                          If the name of the row contains spaces, it should be written with quotation marks.
  #                                          Note that specifying the space character instead of the row name will result in inserting a row without any visible name
  #                                          (inserts an empty row).
  #
  #                  -label <name>           Renames an object added to the waveform. The <name> parameter specifies the name which will replace the original name.
  #                                          To rename more than one object, use the wave command with the -label argument for each object separately. The name is set only for
  #                                          the display purposes. Even if the object is renamed, it can be referred to (for example by the force or examine commands) only
  #                                          by using the original name.
  #
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  #
  #  Revision List
  #
  #  Version   Date       Author
  #
  #  2.0       26.09.16   Pogodaev Danil
  #
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  # module setting
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

  set module_name "lib 9.0 : rx_cnt"

  set module_path "/tb_top/eth_rx/rx_cnt"

  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  # simulation waveform
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

  wave -expand         -height $h_mod  -color $c_mod           -vgroup  "$module_name"                                               \
                                                                                                                                     \
             (         -height $h_nul  -color $c_nul           -divider {-------------------------------------------------------} )  \
                                                                                                                                     \
             ( -expand -height $h_grp  -color $c_grp           -vgroup  "parameters"                                                 \
                                                                                                                                     \
                     ( -height $h_ch1  -color $c_ch1           -divider {- frame body -}                                          )  \
                                                                                                                                     \
                     ( -height $h_par  -color $c_par  -$r_uns  -label   {opt: pW_LEN            } $module_path/pW_LEN             )  \
                                                                                                                                     \
                     ( -height $h_ch1  -color $c_ch1           -divider {- packet counters -}                                     )  \
                                                                                                                                     \
                     ( -height $h_par  -color $c_par  -$r_uns  -label   {opt: pN_CNT            } $module_path/pN_CNT             )  \
                     ( -height $h_par  -color $c_par  -$r_uns  -label   {req: pW_CNT_P          } $module_path/pW_CNT_P           )  \
                     ( -height $h_par  -color $c_par  -$r_uns  -label   {opt: pW_CNT_S          } $module_path/pW_CNT_S           )  \
                                                                                                                                     \
                     ( -height $h_ch1  -color $c_ch1           -divider {- crc comparator -}                                      )  \
                                                                                                                                     \
                     ( -height $h_par  -color $c_par  -$r_uns  -label   {opt: pV_CRC_DLY        } $module_path/pV_CRC_DLY         )  \
                                                                                                                                     \
                     ( -height $h_ch1  -color $c_ch1           -divider {- signal tap -}                                          )  \
                                                                                                                                     \
                     ( -height $h_par  -color $c_par  -$r_uns  -label   {test: pE_GEN_NPR       } $module_path/pE_GEN_NPR         )  \
                                                                                                                                     \
                     ( -height $h_ch1  -color $c_ch1           -divider {- local (for logic) -}                                   )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {out registers}                                           )  \
                                                                                                                                     \
                     ( -height $h_par  -color $c_par  -$r_uns  -label   {opt: pN_REG_CNT        } $module_path/pN_REG_CNT         )  \
                                                                                                                                     \
                     ( -height $h_nul  -color $c_nul           -divider {-------------------------------------------------------} )  \
             )                                                                                                                       \
             ( -expand -height $h_grp  -color $c_grp           -vgroup  "interface"                                                  \
                                                                                                                                     \
                     ( -height $h_ch1  -color $c_ch1           -divider {- clock -}                                               )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {req: i_clk             } $module_path/i_clk              )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {opt: i_clk_en          } $module_path/i_clk_en           )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {opt: i_sclr_n          } $module_path/i_sclr_n           )  \
                                                                                                                                     \
                     ( -height $h_ch1  -color $c_ch1           -divider {- frame body -}                                          )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {req: i_str_body        } $module_path/i_str_body         )  \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {req: i_dat_body        } $module_path/i_dat_body         )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {req: i_dat_load        } $module_path/i_dat_load         )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {opt: i_len_body        } $module_path/i_len_body         )  \
                                                                                                                                     \
                     ( -height $h_ch1  -color $c_ch1           -divider {- setting -}                                             )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {opt: i_set_p_spd       } $module_path/i_set_p_spd        )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {opt: i_set_p_top       } $module_path/i_set_p_top        )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {opt: i_set_p_low       } $module_path/i_set_p_low        )  \
                                                                                                                                     \
                     ( -height $h_ch1  -color $c_ch1           -divider {- detector -}                                            )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {req: o_det_p_rdy       } $module_path/o_det_p_rdy        )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {req: o_det_p_err       } $module_path/o_det_p_err        )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {opt: i_det_p_off       } $module_path/i_det_p_off        )  \
                                                                                                                                     \
                     ( -height $h_ch1  -color $c_ch1           -divider {- counters -}                                            )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {opt: i_cnt_p_clr       } $module_path/i_cnt_p_clr        )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {req: o_cnt_p_rdy       } $module_path/o_cnt_p_rdy        )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {req: o_cnt_p_all       } $module_path/o_cnt_p_all        )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {req: o_cnt_p_err       } $module_path/o_cnt_p_err        )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {opt: o_cnt_p_off       } $module_path/o_cnt_p_off        )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {opt: o_cnt_p_spd       } $module_path/o_cnt_p_spd        )  \
                                                                                                                                     \
                     ( -height $h_ch1  -color $c_ch1           -divider {- counters (rhythm) -}                                   )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {opt: i_rcnt_p_clr      } $module_path/i_rcnt_p_clr       )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {opt: i_rcnt_p_upd      } $module_path/i_rcnt_p_upd       )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {opt: o_rcnt_p_rdy      } $module_path/o_rcnt_p_rdy       )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {opt: o_rcnt_p_all      } $module_path/o_rcnt_p_all       )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {opt: o_rcnt_p_err      } $module_path/o_rcnt_p_err       )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {opt: o_rcnt_p_off      } $module_path/o_rcnt_p_off       )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {opt: o_rcnt_p_spd      } $module_path/o_rcnt_p_spd       )  \
                                                                                                                                     \
                     ( -height $h_nul  -color $c_nul           -divider {-------------------------------------------------------} )  \
             )                                                                                                                       \
             ( -expand -height $h_grp  -color $c_grp           -vgroup  "logic"                                                      \
                                                                                                                                     \
                     ( -height $h_ch1  -color $c_ch1           -divider {- sync clear -}                                          )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {clock}                                                   )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_clk                  } $module_path/i_clk              )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {clear / external}                                        )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_sclr_n               } $module_path/i_sclr_n           )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {ri_sclr_n              } $module_path/ri_sclr_n          )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {sclr_n_ext             } $module_path/sclr_n_ext         )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {clear / ini}                                             )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {sclr_n_ini             } $module_path/sclr_n_ini         )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {sclr_n_ini_cnt         } $module_path/sclr_n_ini_cnt     )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {clear / result}                                          )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {sclr_n                 } $module_path/sclr_n             )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {sclr_n_ini             } $module_path/sclr_n_ini         )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {sclr_n_ext             } $module_path/sclr_n_ext         )  \
                                                                                                                                     \
                     ( -height $h_ch1  -color $c_ch1           -divider {- crc calculator -}                                      )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {clock}                                                   )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {clk                    } $module_path/rx_crc/i_clk       )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {clk_en                 } $module_path/rx_crc/i_clk_en    )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {sclr_n                 } $module_path/rx_crc/i_sclr_n    )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {frame body}                                              )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_str_body             } $module_path/rx_crc/i_str_body  )  \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {i_dat_body             } $module_path/rx_crc/i_dat_body  )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_dat_load             } $module_path/rx_crc/i_dat_load  )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {fcs crc}                                                 )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {o_crc_rdy              } $module_path/rx_crc/o_crc_rdy   )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {o_crc_err              } $module_path/rx_crc/o_crc_err   )  \
                                                                                                                                     \
                     ( -height $h_ch1  -color $c_ch1           -divider {- counters : update -}                                   )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {clock}                                                   )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {clk                    } $module_path/rx_crc/i_clk       )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {clk_en                 } $module_path/rx_crc/i_clk_en    )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {sclr_n                 } $module_path/rx_crc/sclr_n      )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {update / external}                                       )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_rcnt_p_upd           } $module_path/i_rcnt_p_upd       )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {ri_rcnt_p_upd          } $module_path/ri_rcnt_p_upd      )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {ri_rcnt_p_upd_re       } $module_path/ri_rcnt_p_upd_re   )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {rcnt_p_upd_ext         } $module_path/rcnt_p_upd_ext     )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {update / request}                                        )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {rcnt_p_upd_ext         } $module_path/rcnt_p_upd_ext     )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {rcnt_p_upd_req         } $module_path/rcnt_p_upd_req     )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {rcnt_p_upd             } $module_path/rcnt_p_upd         )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {update / next to}                                        )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {rcnt_p_upd_nxt         } $module_path/rcnt_p_upd_nxt     )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {rcnt_p_upd_req         } $module_path/rcnt_p_upd_req     )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {rcnt_p_upd_dis         } $module_path/rcnt_p_upd_dis     )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {update / result}                                         )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {rcnt_p_upd_nxt         } $module_path/rcnt_p_upd_nxt     )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {rcnt_p_upd_nxt_re      } $module_path/rcnt_p_upd_nxt_re  )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {rcnt_p_upd             } $module_path/rcnt_p_upd         )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {update / disable}                                        )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {ri_str_body            } $module_path/ri_str_body        )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {ri_str_body_fe         } $module_path/ri_str_body_fe     )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {rcnt_p_upd_dis         } $module_path/rcnt_p_upd_dis     )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {cnt_p_rdy              } $module_path/cnt_p_rdy          )  \
                                                                                                                                     \
                     ( -height $h_ch1  -color $c_ch1           -divider {- counters : clear -}                                    )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {clock}                                                   )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {clk                    } $module_path/rx_crc/i_clk       )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {clk_en                 } $module_path/rx_crc/i_clk_en    )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {sclr_n                 } $module_path/rx_crc/sclr_n      )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {counters}                                                )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_cnt_p_clr            } $module_path/i_cnt_p_clr        )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {ri_cnt_p_clr           } $module_path/ri_cnt_p_clr       )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {ri_cnt_p_clr_re        } $module_path/ri_cnt_p_clr_re    )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {cnt_p_clr              } $module_path/cnt_p_clr          )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {counters (rhythm)}                                       )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_rcnt_p_clr           } $module_path/i_rcnt_p_clr       )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {ri_rcnt_p_clr          } $module_path/ri_rcnt_p_clr      )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {ri_rcnt_p_clr_re       } $module_path/ri_rcnt_p_clr_re   )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {rcnt_p_clr             } $module_path/rcnt_p_clr         )  \
                                                                                                                                     \
                     ( -height $h_ch1  -color $c_ch1           -divider {- counters : ready -}                                    )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {clock}                                                   )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {clk                    } $module_path/rx_crc/i_clk       )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {clk_en                 } $module_path/rx_crc/i_clk_en    )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {sclr_n                 } $module_path/rx_crc/sclr_n      )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {counters}                                                )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {det_crc_rdy            } $module_path/det_crc_rdy        )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {det_crc_rdy_z          } $module_path/det_crc_rdy_z      )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {cnt_p_rdy              } $module_path/cnt_p_rdy          )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {counters (rhythm)}                                       )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {rcnt_p_upd             } $module_path/rcnt_p_upd         )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {rcnt_p_upd_z           } $module_path/rcnt_p_upd_z       )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {rcnt_p_rdy             } $module_path/rcnt_p_rdy         )  \
                                                                                                                                     \
                     ( -height $h_ch1  -color $c_ch1           -divider {- counters : speed sel -}                                )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {clock}                                                   )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {clk                    } $module_path/rx_crc/i_clk       )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {clk_en                 } $module_path/rx_crc/i_clk_en    )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {sclr_n                 } $module_path/rx_crc/sclr_n      )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {setting}                                                 )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {i_set_p_spd            } $module_path/i_set_p_spd        )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {ri_set_p_spd           } $module_path/ri_set_p_spd       )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {packet body}                                             )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {ri_str_body            } $module_path/ri_str_body        )  \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {ri_dat_body            } $module_path/ri_dat_body        )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {ri_dat_load            } $module_path/ri_dat_load        )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {speed strob / cut count}                                 )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {cnt_body_cut           } $module_path/cnt_body_cut       )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {cnt_body_cut_1         } $module_path/cnt_body_cut_1     )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {cnt_body_cut_2         } $module_path/cnt_body_cut_2     )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {speed strob / next to}                                   )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {dat_load_spd_nxt       } $module_path/dat_load_spd_nxt   )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {str_body_spd_nxt       } $module_path/str_body_spd_nxt   )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {speed strob}                                             )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {dat_load_spd           } $module_path/dat_load_spd       )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {str_body_spd           } $module_path/str_body_spd       )  \
                                                                                                                                     \
                     ( -height $h_ch1  -color $c_ch1           -divider {- counters : bounds -}                                   )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {clock}                                                   )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {clk                    } $module_path/rx_crc/i_clk       )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {clk_en                 } $module_path/rx_crc/i_clk_en    )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {sclr_n                 } $module_path/rx_crc/sclr_n      )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {packet body}                                             )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {ri_str_body            } $module_path/ri_str_body        )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {ri_len_body            } $module_path/ri_len_body        )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {counter setting}                                         )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {ri_set_p_low           } $module_path/ri_set_p_low       )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {ri_set_p_top           } $module_path/ri_set_p_top       )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {counter bounds}                                          )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {cnt_len_bound          } $module_path/cnt_len_bound      )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {cnt_len_bound_low      } $module_path/cnt_len_bound_low  )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {cnt_len_bound_top      } $module_path/cnt_len_bound_top  )  \
                                                                                                                                     \
                     ( -height $h_ch1  -color $c_ch1           -divider {- counters : packet all -}                               )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {clock}                                                   )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {clk                    } $module_path/rx_crc/i_clk       )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {clk_en                 } $module_path/rx_crc/i_clk_en    )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {sclr_n                 } $module_path/rx_crc/sclr_n      )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {bounds of length}                                        )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {cnt_len_bound          } $module_path/cnt_len_bound      )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {crc ready}                                               )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {det_crc_rdy            } $module_path/det_crc_rdy        )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {clear}                                                   )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {cnt_p_clr              } $module_path/cnt_p_clr          )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {rcnt_p_clr             } $module_path/rcnt_p_clr         )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {update}                                                  )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {rcnt_p_upd_dis         } $module_path/rcnt_p_upd_dis     )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {rcnt_p_upd             } $module_path/rcnt_p_upd         )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {rcnt_p_upd_z           } $module_path/rcnt_p_upd_z       )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {counters}                                                )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {cnt_p_all              } $module_path/cnt_p_all          )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {rcnt_p_all             } $module_path/rcnt_p_all         )  \
                                                                                                                                     \
                     ( -height $h_ch1  -color $c_ch1           -divider {- counters : packet err -}                               )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {clock}                                                   )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {clk                    } $module_path/rx_crc/i_clk       )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {clk_en                 } $module_path/rx_crc/i_clk_en    )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {sclr_n                 } $module_path/rx_crc/sclr_n      )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {bounds of length}                                        )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {cnt_len_bound          } $module_path/cnt_len_bound      )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {crc ready}                                               )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {det_crc_rdy            } $module_path/det_crc_rdy        )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {det_crc_err            } $module_path/det_crc_err        )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {clear}                                                   )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {cnt_p_clr              } $module_path/cnt_p_clr          )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {rcnt_p_clr             } $module_path/rcnt_p_clr         )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {update}                                                  )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {rcnt_p_upd_dis         } $module_path/rcnt_p_upd_dis     )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {rcnt_p_upd             } $module_path/rcnt_p_upd         )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {rcnt_p_upd_z           } $module_path/rcnt_p_upd_z       )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {counters}                                                )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {cnt_p_err              } $module_path/cnt_p_err          )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {rcnt_p_err             } $module_path/rcnt_p_err         )  \
                                                                                                                                     \
                     ( -height $h_ch1  -color $c_ch1           -divider {- counters : packet offcast -}                           )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {clock}                                                   )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {clk                    } $module_path/rx_crc/i_clk       )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {clk_en                 } $module_path/rx_crc/i_clk_en    )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {sclr_n                 } $module_path/rx_crc/sclr_n      )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {bounds of length}                                        )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {cnt_len_bound          } $module_path/cnt_len_bound      )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {crc ready}                                               )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {det_crc_rdy            } $module_path/det_crc_rdy        )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {offcast strob}                                           )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_det_p_off            } $module_path/i_det_p_off        )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {ri_det_p_off           } $module_path/ri_det_p_off       )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {det_p_off              } $module_path/det_p_off          )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {clear}                                                   )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {cnt_p_clr              } $module_path/cnt_p_clr          )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {rcnt_p_clr             } $module_path/rcnt_p_clr         )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {update}                                                  )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {rcnt_p_upd_dis         } $module_path/rcnt_p_upd_dis     )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {rcnt_p_upd             } $module_path/rcnt_p_upd         )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {rcnt_p_upd_z           } $module_path/rcnt_p_upd_z       )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {counters}                                                )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {cnt_p_off              } $module_path/cnt_p_off          )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {rcnt_p_off             } $module_path/rcnt_p_off         )  \
                                                                                                                                     \
                     ( -height $h_ch1  -color $c_ch1           -divider {- counters : packet speed -}                             )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {clock}                                                   )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {clk                    } $module_path/rx_crc/i_clk       )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {clk_en                 } $module_path/rx_crc/i_clk_en    )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {sclr_n                 } $module_path/rx_crc/sclr_n      )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {bounds of length}                                        )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {cnt_len_bound          } $module_path/cnt_len_bound      )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {packet body}                                             )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {str_body               } $module_path/ri_str_body        )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {str_body_spd           } $module_path/str_body_spd       )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {dat_load_spd           } $module_path/dat_load_spd       )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {clear}                                                   )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {cnt_p_clr              } $module_path/cnt_p_clr          )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {rcnt_p_clr             } $module_path/rcnt_p_clr         )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {update}                                                  )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {rcnt_p_upd_dis         } $module_path/rcnt_p_upd_dis     )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {rcnt_p_upd             } $module_path/rcnt_p_upd         )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {rcnt_p_upd_z           } $module_path/rcnt_p_upd_z       )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {counters}                                                )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {cnt_p_spd              } $module_path/cnt_p_spd          )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {rcnt_p_spd             } $module_path/rcnt_p_spd         )  \
                                                                                                                                     \
                     ( -height $h_nul  -color $c_nul           -divider {-------------------------------------------------------} )  \
             )                                                                                                                       \
             ( -expand -height $h_grp  -color $c_grp           -vgroup  "all wave"                                                   \
                     ( -height $h_sig  -color $c_sig                                              $module_path/*                  )  \
                                                                                                                                     \
                     ( -height $h_nul  -color $c_nul           -divider {-------------------------------------------------------} )  \
             )                                                                                                                       \
             (         -height $h_nul  -color $c_nul           -divider {-------------------------------------------------------} )  \

  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#




  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  #
  #  Company      : Micran, Department of Telecommunications
  #
  #  Engineer     : Pogodaev Danil
  #
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  #
  #  Library      : lib_ethernet / library of ethernet components
  #
  #  Component    : eth_rx_v2 / receiver of ethernet frames (packets)
  #
  #  File Name    : wave_tbg_1_6 .do (tcl sim aldec)
  #
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  #
  #  Description  : List of module waveforms for Aldec simulator.
  #
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  #
  #  Note (1)     : Command 'wave'           Adds objects to the Waveform Viewer.
  #  
  #                                          If the window is not open yet, issuing the command first opens the viewer and then adds the specified objects to the Object View pane
  #                                          of the Waveform Viewer window. The order in which objects are added depends on the method you use to group or add them to the viewer window.
  #                                          If you use wildcards, objects are added in the same order they are declared in source code.
  #                                          If you issue a sequence of the wave commands, the objects are displayed in the order the commands have been executed.
  #
  #                  -expand                 Automatically expands the object added to the waveform.
  #
  #                  -vgroup                 Creates a virtual group. The name of the object is specified by the <name> argument.
  #                                          Virtual groups can be nested or they can contain other virtual objects: virtual arrays, virtual records, or named rows.
  #                                          You can create a virtual group that contains nested virtual objects or nested named rows by using the following syntax:
  #
  #                                          wave -vgroup <virtual_name> <signals> ( -vgroup <nested_virtual_name> <signals> ) <signals>
  #
  #                  -height <pixels>        Specifies the height of the signal waveform in pixels.
  #
  #                  -bold                   When this argument is passed, the name of the object listed in the Object View pane and its value displayed in the Waveform View pane
  #                                          will be presented in bold. The argument does not affect the display of object waves.
  #
  #                  -color <color>          Specifies the color of the signal name in the Object View pane. The color can be either an RGB value or a name.
  #
  #                                          The RGB value should be specified with a hash sign (#) followed by hex digits. One, two, three, or four digits can be used for each color.
  #                                          The RGB value can be specified with three decimal values separated by commas.
  #
  #                                          Definitions of color names (e.g. steelblue or gainsboro) have been provided by the World Wide Web Consortium and are available at
  #                                          www.w3.org/TR/SVG/types.html#ColorKeywords. Color transparent represents the absence of a color.
  #
  #                  -color_waveform         Applies the color specified with the -color argument to the timing waveform in the Waveform View pane.
  #
  #                  -ascii                  Specifies how the waveform of the signal should be displayed.
  #                  -binary                 You can use different radices and different notations for the decimal radix. If radix is not specified, the radix returned
  #                  -decimal                by the radix command is used. Available radices include ASCII (-ascii), binary (-binary), octal (-octal), decimal (-decimal),
  #                  -unsigned               hexadecimal (-hexadecimal), and floating-point (-floatingpoint). It is assumed that values displayed in the decimal radix (-decimal)
  #                  -hexadecimal            use two's complement notation.
  #
  #                                          The notation for the decimal radix can be specified explicitly.
  #                                          Available options include unsigned (-unsigned) one's complement (-1complement), two's complement (-2complement), and signed magnitude
  #                                          (-magnitude). Note that options -decimal and -2complement are equivalent. When the decimal notation is selected, you can specify
  #                                          a fixed-point notation by using the -binarypoint argument.
  #
  #                                          The floating-point representation adheres to IEEE Std 754 and is available for floating-point variables and for 64-bit or 32-bit
  #                                          vectors only.
  #
  #                  -named_row | -divider   Inserts a named row or a row separator to the waveform window. The name of the object is specified by the <name> argument.
  #                                          Named rows and row dividers do not show timing waveforms. They are used to improve the legibility of data shown in the waveform window.
  #                                          For example, you can add a row displaying the string MEMORY PORTS above the ports of a memory unit:
  #
  #                                          If the name of the row contains spaces, it should be written with quotation marks.
  #                                          Note that specifying the space character instead of the row name will result in inserting a row without any visible name
  #                                          (inserts an empty row).
  #
  #                  -label <name>           Renames an object added to the waveform. The <name> parameter specifies the name which will replace the original name.
  #                                          To rename more than one object, use the wave command with the -label argument for each object separately. The name is set only for
  #                                          the display purposes. Even if the object is renamed, it can be referred to (for example by the force or examine commands) only
  #                                          by using the original name.
  #
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  #
  #  Revision List
  #
  #  Version   Date       Author
  #
  #  2.0       26.09.16   Pogodaev Danil
  #
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  # module setting
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

  set module_name "tbg 1.6 : emu_spd"

  set module_path "/tb_top/tb_gen/eth_emu/emu_spd"

  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  # simulation waveform
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

  wave -expand         -height $h_mod  -color $c_mod           -vgroup  "$module_name"                                               \
                                                                                                                                     \
             (         -height $h_nul  -color $c_nul           -divider {-------------------------------------------------------} )  \
                                                                                                                                     \
             ( -expand -height $h_grp  -color $c_grp           -vgroup  "parameters"                                                 \
                                                                                                                                     \
                     ( -height $h_ch1  -color $c_ch1           -divider {- packet structure -}                                    )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {frame length}                                            )  \
                                                                                                                                     \
                     ( -height $h_par  -color $c_par  -$r_uns  -label   {req: pW_LAN_LEN        } $module_path/pW_LAN_LEN         )  \
                     ( -height $h_par  -color $c_par  -$r_uns  -label   {req: pW_LAN_GAP        } $module_path/pW_LAN_GAP         )  \
                                                                                                                                     \
                     ( -height $h_nul  -color $c_nul           -divider {-------------------------------------------------------} )  \
             )                                                                                                                       \
             ( -expand -height $h_grp  -color $c_grp           -vgroup  "interface"                                                  \
                                                                                                                                     \
                     ( -height $h_ch1  -color $c_ch1           -divider {- setting -}                                             )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {interface}                                               )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {req: i_set_io_type     } $module_path/i_set_io_type      )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {req: i_set_io_speed    } $module_path/i_set_io_speed     )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {transmit speed}                                          )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {req: i_set_tx_mode     } $module_path/i_set_tx_mode      )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {req: i_set_tx_speed    } $module_path/i_set_tx_speed     )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {opt: o_set_tx_speed    } $module_path/o_set_tx_speed     )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {opt: o_set_tx_limit    } $module_path/o_set_tx_limit     )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {req: i_set_tx_sel      } $module_path/i_set_tx_sel       )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {frame length}                                            )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {req: i_set_len_type    } $module_path/i_set_len_type     )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {req: i_set_len_const   } $module_path/i_set_len_const    )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {opt: i_set_len_min     } $module_path/i_set_len_min      )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {opt: i_set_len_max     } $module_path/i_set_len_max      )  \
                                                                                                                                     \
                     ( -height $h_ch1  -color $c_ch1           -divider {- lan packet -}                                          )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {length}                                                  )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {req: i_len_frame       } $module_path/i_len_frame        )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {opt: i_len_gap         } $module_path/i_len_gap          )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {req: o_len_gap         } $module_path/o_len_gap          )  \
                                                                                                                                     \
                     ( -height $h_nul  -color $c_nul           -divider {-------------------------------------------------------} )  \
             )                                                                                                                       \
             ( -expand -height $h_grp  -color $c_grp           -vgroup  "logic"                                                      \
                                                                                                                                     \
                     ( -height $h_ch1  -color $c_ch1           -divider {- speed bandwidth -}                                     )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {setting}                                                 )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {i_set_io_type          } $module_path/i_set_io_type      )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {i_set_io_speed         } $module_path/i_set_io_speed     )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {bandwidth}                                               )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {bandwidth              } $module_path/bandwidth          )  \
                                                                                                                                     \
                     ( -height $h_ch1  -color $c_ch1           -divider {- speed selection -}                                     )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {setting}                                                 )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {i_set_tx_sel           } $module_path/i_set_tx_sel       )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {selection}                                               )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {speed_sel_head         } $module_path/speed_sel_head     )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {speed_sel_mac          } $module_path/speed_sel_mac      )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {speed_sel_crc          } $module_path/speed_sel_crc      )  \
                                                                                                                                     \
                     ( -height $h_ch1  -color $c_ch1           -divider {- length calculator -}                                   )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {setting}                                                 )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {i_set_len_type         } $module_path/i_set_len_type     )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {i_set_len_const        } $module_path/i_set_len_const    )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {i_set_len_min          } $module_path/i_set_len_min      )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {i_set_len_max          } $module_path/i_set_len_max      )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {selection}                                               )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {speed_sel_head         } $module_path/speed_sel_head     )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {speed_sel_mac          } $module_path/speed_sel_mac      )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {speed_sel_crc          } $module_path/speed_sel_crc      )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {frame length}                                            )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {len_frame_sel          } $module_path/len_frame_sel      )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {len_frame_full         } $module_path/len_frame_full     )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {len_frame_diff         } $module_path/len_frame_diff     )  \
                                                                                                                                     \
                     ( -height $h_ch1  -color $c_ch1           -divider {- speed calc / limit -}                                  )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {bandwidth}                                               )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {bandwidth              } $module_path/bandwidth          )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {frame length}                                            )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {len_frame_sel          } $module_path/len_frame_sel      )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {len_frame_full         } $module_path/len_frame_full     )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {speed limit}                                             )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {speed_lim_sel          } $module_path/speed_lim_sel      )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {speed_lim_full         } $module_path/speed_lim_full     )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {speed_lim_diff         } $module_path/speed_lim_diff     )  \
                                                                                                                                     \
                     ( -height $h_ch1  -color $c_ch1           -divider {- speed calc / set -}                                    )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {frame length}                                            )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {len_frame_sel          } $module_path/len_frame_sel      )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {len_frame_full         } $module_path/len_frame_full     )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {speed set}                                               )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {speed_set_sel          } $module_path/speed_set_sel      )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {speed_set_full         } $module_path/speed_set_full     )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {speed_set_diff         } $module_path/speed_set_diff     )  \
                                                                                                                                     \
                     ( -height $h_ch1  -color $c_ch1           -divider {- speed calc / real -}                                   )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {bandwidth}                                               )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {bandwidth              } $module_path/bandwidth          )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {frame length}                                            )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {len_frame_sel          } $module_path/len_frame_sel      )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {len_frame_full         } $module_path/len_frame_full     )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {length gap}                                              )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {len_gap                } $module_path/len_gap            )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {i_len_gap              } $module_path/i_len_gap          )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {speed real}                                              )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {speed_real_sel         } $module_path/speed_real_sel     )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {speed_real_full        } $module_path/speed_real_full    )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {speed_real_diff        } $module_path/speed_real_diff    )  \
                                                                                                                                     \
                     ( -height $h_ch1  -color $c_ch1           -divider {- speed calc / frame -}                                  )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {bandwidth}                                               )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {bandwidth              } $module_path/bandwidth          )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {speed set}                                               )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {speed_set_full         } $module_path/speed_set_full     )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {speed_lim_full         } $module_path/speed_lim_full     )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {speed frame}                                             )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {speed_frame_full       } $module_path/speed_frame_full   )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {speed_gap_full         } $module_path/speed_gap_full     )  \
                                                                                                                                     \
                     ( -height $h_ch1  -color $c_ch1           -divider {- gap calculator -}                                      )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {speed frame}                                             )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {speed_frame_full       } $module_path/speed_frame_full   )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {speed_gap_full         } $module_path/speed_gap_full     )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {frame length}                                            )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {len_frame_full         } $module_path/len_frame_full     )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {length gap}                                              )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {len_gap                } $module_path/len_gap            )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {len_gap_min            } $module_path/len_gap_min        )  \
                                                                                                                                     \
                     ( -height $h_nul  -color $c_nul           -divider {-------------------------------------------------------} )  \
             )                                                                                                                       \
             ( -expand -height $h_grp  -color $c_grp           -vgroup  "all wave"                                                   \
                     ( -height $h_sig  -color $c_sig                                              $module_path/*                  )  \
                                                                                                                                     \
                     ( -height $h_nul  -color $c_nul           -divider {-------------------------------------------------------} )  \
             )                                                                                                                       \
             (         -height $h_nul  -color $c_nul           -divider {-------------------------------------------------------} )  \

  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#




  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  #
  #  Company      : Micran, Department of Telecommunications
  #
  #  Engineer     : Pogodaev Danil
  #
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  #
  #  Library      : lib_ethernet / library of ethernet components
  #
  #  Component    : eth_rx_v2 / receiver of ethernet frames (packets)
  #
  #  File Name    : wave_tbg_1_5 .do (tcl sim aldec)
  #
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  #
  #  Description  : List of module waveforms for Aldec simulator.
  #
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  #
  #  Note (1)     : Command 'wave'           Adds objects to the Waveform Viewer.
  #  
  #                                          If the window is not open yet, issuing the command first opens the viewer and then adds the specified objects to the Object View pane
  #                                          of the Waveform Viewer window. The order in which objects are added depends on the method you use to group or add them to the viewer window.
  #                                          If you use wildcards, objects are added in the same order they are declared in source code.
  #                                          If you issue a sequence of the wave commands, the objects are displayed in the order the commands have been executed.
  #
  #                  -expand                 Automatically expands the object added to the waveform.
  #
  #                  -vgroup                 Creates a virtual group. The name of the object is specified by the <name> argument.
  #                                          Virtual groups can be nested or they can contain other virtual objects: virtual arrays, virtual records, or named rows.
  #                                          You can create a virtual group that contains nested virtual objects or nested named rows by using the following syntax:
  #
  #                                          wave -vgroup <virtual_name> <signals> ( -vgroup <nested_virtual_name> <signals> ) <signals>
  #
  #                  -height <pixels>        Specifies the height of the signal waveform in pixels.
  #
  #                  -bold                   When this argument is passed, the name of the object listed in the Object View pane and its value displayed in the Waveform View pane
  #                                          will be presented in bold. The argument does not affect the display of object waves.
  #
  #                  -color <color>          Specifies the color of the signal name in the Object View pane. The color can be either an RGB value or a name.
  #
  #                                          The RGB value should be specified with a hash sign (#) followed by hex digits. One, two, three, or four digits can be used for each color.
  #                                          The RGB value can be specified with three decimal values separated by commas.
  #
  #                                          Definitions of color names (e.g. steelblue or gainsboro) have been provided by the World Wide Web Consortium and are available at
  #                                          www.w3.org/TR/SVG/types.html#ColorKeywords. Color transparent represents the absence of a color.
  #
  #                  -color_waveform         Applies the color specified with the -color argument to the timing waveform in the Waveform View pane.
  #
  #                  -ascii                  Specifies how the waveform of the signal should be displayed.
  #                  -binary                 You can use different radices and different notations for the decimal radix. If radix is not specified, the radix returned
  #                  -decimal                by the radix command is used. Available radices include ASCII (-ascii), binary (-binary), octal (-octal), decimal (-decimal),
  #                  -unsigned               hexadecimal (-hexadecimal), and floating-point (-floatingpoint). It is assumed that values displayed in the decimal radix (-decimal)
  #                  -hexadecimal            use two's complement notation.
  #
  #                                          The notation for the decimal radix can be specified explicitly.
  #                                          Available options include unsigned (-unsigned) one's complement (-1complement), two's complement (-2complement), and signed magnitude
  #                                          (-magnitude). Note that options -decimal and -2complement are equivalent. When the decimal notation is selected, you can specify
  #                                          a fixed-point notation by using the -binarypoint argument.
  #
  #                                          The floating-point representation adheres to IEEE Std 754 and is available for floating-point variables and for 64-bit or 32-bit
  #                                          vectors only.
  #
  #                  -named_row | -divider   Inserts a named row or a row separator to the waveform window. The name of the object is specified by the <name> argument.
  #                                          Named rows and row dividers do not show timing waveforms. They are used to improve the legibility of data shown in the waveform window.
  #                                          For example, you can add a row displaying the string MEMORY PORTS above the ports of a memory unit:
  #
  #                                          If the name of the row contains spaces, it should be written with quotation marks.
  #                                          Note that specifying the space character instead of the row name will result in inserting a row without any visible name
  #                                          (inserts an empty row).
  #
  #                  -label <name>           Renames an object added to the waveform. The <name> parameter specifies the name which will replace the original name.
  #                                          To rename more than one object, use the wave command with the -label argument for each object separately. The name is set only for
  #                                          the display purposes. Even if the object is renamed, it can be referred to (for example by the force or examine commands) only
  #                                          by using the original name.
  #
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  #
  #  Revision List
  #
  #  Version   Date       Author
  #
  #  2.0       26.09.16   Pogodaev Danil
  #
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  # module setting
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

  set module_name "tbg 1.5 : emu_lan"

  set module_path "/tb_top/tb_gen/eth_emu/emu_lan"

  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  # simulation waveform
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

  wave -expand         -height $h_mod  -color $c_mod           -vgroup  "$module_name"                                               \
                                                                                                                                     \
             (         -height $h_nul  -color $c_nul           -divider {-------------------------------------------------------} )  \
                                                                                                                                     \
             ( -expand -height $h_grp  -color $c_grp           -vgroup  "parameters"                                                 \
                                                                                                                                     \
                     ( -height $h_ch1  -color $c_ch1           -divider {- custom mode (clock) -}                                 )  \
                                                                                                                                     \
                     ( -height $h_par  -color $c_par  -$r_uns  -label   {opt: pE_CST_CLK        } $module_path/pE_CST_CLK         )  \
                                                                                                                                     \
                     ( -height $h_nul  -color $c_nul           -divider {-------------------------------------------------------} )  \
             )                                                                                                                       \
             ( -expand -height $h_grp  -color $c_grp           -vgroup  "interface"                                                  \
                                                                                                                                     \
                     ( -height $h_ch1  -color $c_ch1           -divider {- clear -}                                               )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {opt: i_sclr_n          } $module_path/i_sclr_n           )  \
                                                                                                                                     \
                     ( -height $h_ch1  -color $c_ch1           -divider {- setting -}                                             )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {req: i_set_type        } $module_path/i_set_type         )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {req: i_set_speed       } $module_path/i_set_speed        )  \
                                                                                                                                     \
                     ( -height $h_ch1  -color $c_ch1           -divider {- lan packet -}                                          )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {clock}                                                   )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {req: o_clk             } $module_path/o_clk              )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {req: o_cen             } $module_path/o_cen              )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {req: o_ced             } $module_path/o_ced              )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {data}                                                    )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {req: i_str_frame       } $module_path/i_str_frame(0)     )  \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {req: i_dat_frame       } $module_path/i_dat_frame        )  \
                                                                                                                                     \
                     ( -height $h_ch1  -color $c_ch1           -divider {- lan interface -}                                       )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {lan mii}                                                 )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {req: o_mii_tx_clk      } $module_path/o_mii_tx_clk       )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {req: o_mii_tx_en       } $module_path/o_mii_tx_en        )  \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {req: o_mii_txd         } $module_path/o_mii_txd          )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {lan rmii}                                                )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {req: o_rmii_tx_clk     } $module_path/o_rmii_tx_clk      )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {tst: o_rmii_tx_clk_s   } $module_path/o_rmii_tx_clk_s    )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {req: o_rmii_tx_en      } $module_path/o_rmii_tx_en       )  \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {req: o_rmii_txd        } $module_path/o_rmii_txd         )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {lan gmii}                                                )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {req: o_gmii_tx_clk     } $module_path/o_gmii_tx_clk      )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {req: o_gmii_tx_en      } $module_path/o_gmii_tx_en       )  \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {req: o_gmii_txd        } $module_path/o_gmii_txd         )  \
                                                                                                                                     \
                     ( -height $h_nul  -color $c_nul           -divider {-------------------------------------------------------} )  \
             )                                                                                                                       \
             ( -expand -height $h_grp  -color $c_grp           -vgroup  "logic"                                                      \
                                                                                                                                     \
                     ( -height $h_ch1  -color $c_ch1           -divider {- lan rmii -}                                            )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {clock}                                                   )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_clk                  } $module_path/i_clk              )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_cen                  } $module_path/i_cen              )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_ced                  } $module_path/i_ced              )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {setting}                                                 )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {i_set_type             } $module_path/i_set_type         )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {i_set_speed            } $module_path/i_set_speed        )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {input packet}                                            )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_str_frame            } $module_path/i_str_frame(0)     )  \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {i_dat_frame            } $module_path/i_dat_frame        )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {rmii clock}                                              )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {rmii_tx_clk            } $module_path/rmii_tx_clk        )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {rmii_tx_clk_s          } $module_path/rmii_tx_clk_s      )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {rmii_tx_clk_10         } $module_path/rmii_tx_clk_10     )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {rmii data}                                               )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {rmii_tx_en             } $module_path/rmii_tx_en         )  \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {rmii_txd               } $module_path/rmii_txd           )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {rmii_cnt               } $module_path/rmii_cnt           )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {rmii interface}                                          )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {rmii_tx_clk            } $module_path/rmii_tx_clk        )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {rmii_tx_en             } $module_path/rmii_tx_en         )  \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {rmii_txd               } $module_path/rmii_txd           )  \
                                                                                                                                     \
                     ( -height $h_ch1  -color $c_ch1           -divider {- lan mii -}                                             )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {clock}                                                   )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_clk                  } $module_path/i_clk              )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_cen                  } $module_path/i_cen              )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_ced                  } $module_path/i_ced              )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {setting}                                                 )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {i_set_io_type          } $module_path/i_set_io_type      )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {i_set_io_speed         } $module_path/i_set_io_speed     )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {input packet}                                            )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_str_frame            } $module_path/i_str_frame(0)     )  \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {i_dat_frame            } $module_path/i_dat_frame        )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {mii data}                                                )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {mii_txd                } $module_path/mii_txd            )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {mii_cnt                } $module_path/mii_cnt            )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {mii interface}                                           )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {mii_tx_clk             } $module_path/mii_tx_clk         )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {mii_tx_en              } $module_path/mii_tx_en          )  \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {mii_txd                } $module_path/mii_txd            )  \
                                                                                                                                     \
                     ( -height $h_ch1  -color $c_ch1           -divider {- lan gmii -}                                            )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {clock}                                                   )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_clk                  } $module_path/i_clk              )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_cen                  } $module_path/i_cen              )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_ced                  } $module_path/i_ced              )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {setting}                                                 )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {i_set_type             } $module_path/i_set_type         )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {i_set_speed            } $module_path/i_set_speed        )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {input packet}                                            )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {i_str_frame            } $module_path/i_str_frame(0)     )  \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {i_dat_frame            } $module_path/i_dat_frame        )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {gmii data}                                               )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {gmii_txd               } $module_path/gmii_txd           )  \
                     ( -height $h_sig  -color $c_sig  -$r_uns  -label   {gmii_cnt               } $module_path/gmii_cnt           )  \
                                                                                                                                     \
                     ( -height $h_ch2  -color $c_ch2           -divider {gmii interface}                                          )  \
                                                                                                                                     \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {gmii_tx_clk            } $module_path/gmii_tx_clk        )  \
                     ( -height $h_sig  -color $c_sig  -$r_bin  -label   {gmii_tx_en             } $module_path/gmii_tx_en         )  \
                     ( -height $h_sig  -color $c_sig  -$r_hex  -label   {gmii_txd               } $module_path/gmii_txd           )  \
                                                                                                                                     \
                     ( -height $h_nul  -color $c_nul           -divider {-------------------------------------------------------} )  \
             )                                                                                                                       \
             ( -expand -height $h_grp  -color $c_grp           -vgroup  "all wave"                                                   \
                     ( -height $h_sig  -color $c_sig                                              $module_path/*                  )  \
                                                                                                                                     \
                     ( -height $h_nul  -color $c_nul           -divider {-------------------------------------------------------} )  \
             )                                                                                                                       \
             (         -height $h_nul  -color $c_nul           -divider {-------------------------------------------------------} )  \

  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#




#-------------------------------------------------------------------------------------------------#
#
#  Company      : Micran, Department of Telecommunications
#
#  Engineer     : Pogodaev Danil
#
#-------------------------------------------------------------------------------------------------#
#
#  Library      : lib_ethernet / library of ethernet components
#
#  File Name    : directory .py (python 3)
#
#-------------------------------------------------------------------------------------------------#
#
#  Description  : Specify directory for simulation. Used for 'aldec' folder.
#
#                  - creating a tcl file 'directory.do'
#                  - creating a text file 'console.txt'
#                  - creating a text file 'readme.txt'
#
#-------------------------------------------------------------------------------------------------#
#
#  Note         : Instructions
#
#                 1. Launch the python-script 'directory.py' for directory settings
#                 2. Copy the contents (command) of the text file 'console .txt'
#                 3. Insert command (do ../main.do) in the Aldec simulator console
#                 4. Press Enter
#
#-------------------------------------------------------------------------------------------------#
#
#  Limitation   : Placement of the python-script file 'directory .py'
#
#                 ../hard/sim/aldec/directory.py
#
#-------------------------------------------------------------------------------------------------#
#
#  Revision List
#
#  Version   Date       Author
#
#  1.0.0     16.09.16   Pogodaev Danil
#
#-------------------------------------------------------------------------------------------------#

#-------------------------------------------------------------------------------------------------#
# import
#-------------------------------------------------------------------------------------------------#

import os
import re

#-------------------------------------------------------------------------------------------------#
# path to folder 'hard/sim/aldec'
#-------------------------------------------------------------------------------------------------#

# path getting

path_aldec = os.path.dirname(__file__)
path_aldec_len = len(path_aldec)

# replace backslash (\) to slash (/)

backslash = "\\"
path_aldec_new = ""

for letter in path_aldec:
    if letter not in backslash:
        path_aldec_new += letter
    else:
        path_aldec_new += "/"

# path result

path_aldec = path_aldec_new

# path checking

check = "hard/sim/aldec"
path_true = 0

if check in path_aldec:
    path_true = 1
else:
    path_true = 0

    print("\n Error!\n                                                     ")
    print("\n Reason  : Incorrect placement of the file 'directory.py'.    ")
    print("\n Actions : Move the file 'directory.py' to correct placement: ")
    print("\n           .../hard/sim/aldec/directory.py                    ")

#-------------------------------------------------------------------------------------------------#
# path to file 'main.do' (folder 'aldec')
#-------------------------------------------------------------------------------------------------#

if path_true:

    path_main = path_aldec + "/main.do"
    do_main = "do " + path_main

#-------------------------------------------------------------------------------------------------#
# path to folder 'hard'
#-------------------------------------------------------------------------------------------------#

# length calculator

if path_true:

    path_over = "sim/aldec"

    path_over_len = len(path_over) + 1
    path_hard_len = path_aldec_len - path_over_len

# path getting

if path_true:

    path_cnt  = 0
    path_hard = ""

    for letter in path_aldec:
        if path_cnt < path_hard_len:
            path_hard += letter
            path_cnt  += 1

#-------------------------------------------------------------------------------------------------#
# create file 'console.txt'
#-------------------------------------------------------------------------------------------------#

if path_true:

    text_file = open("console.txt", "w")
    text_file.write(do_main)
    text_file.close()

#-------------------------------------------------------------------------------------------------#
# main.do editing / directory setting
#-------------------------------------------------------------------------------------------------#

if path_true:

    if os.path.exists("main.do"):

        set_dir = "  set set_dir  " + path_hard

        line_number = 39 # line number for 'set_dir' setting

        text_file = open("main.do", "r+")

        line     = 0
        line_cnt = 0
        line_new = ""

        for line in text_file:
            line_cnt += 1
            if line_cnt == line_number:
                line_new += set_dir + "\n" # line replacement
            else:
                line_new += line
    
        text_file.close()

        text_file = open("main.do", "w")

        text_file.writelines(line_new) # file replacement
  
        text_file.close()

    else:

        path_true = 0
        
        print("\n Error! Missing file 'main.do'\n ")

#-------------------------------------------------------------------------------------------------#
# create file 'readme.txt'
#-------------------------------------------------------------------------------------------------#

if path_true:

    text_file = open("readme.txt", "w")

    # write to file

    set_readme = """
 Instructions
 ------------

 1. Launch the script "directory.py" (Python 3) for directory settings.

 2. Copy the contents (command) of the text file "console .txt".

 3. Paste command (do ../main.do) in the Aldec simulator console.

 4. Press Enter.
"""
    text_file.writelines(set_readme)
    text_file.close()

#-------------------------------------------------------------------------------------------------#
# print information
#-------------------------------------------------------------------------------------------------#

# path info

if path_true:

    print(" \n                        "             )
    print(" PATHS:                    "             )
    print(" \n                        "             )
    print(" Path to folder 'hard'    :", path_hard  )
    print(" Path to folder 'aldec'   :", path_aldec )
    print(" Path to file   'main.do' :", path_main  )

# files info

if path_true:

    print(" \n                                 ")
    print(" FILES:                             ")
    print(" \n                                 ")
    print(" Creating a text file: readme  .txt ")
    print(" Creating a text file: console .txt ")

# instructions

if path_true:

    print(" \n                                                                  ")
    print(" Instructions:                                                       ")
    print(" ------------                                                        ")
    print(" \n                                                                  ")
    print(" 1. Launch the python-script 'directory.py' for directory settings.  ")
    print(" 2. Copy the contents (command) of the text file 'console .txt'.     ")
    print(" 3. Paste command (do ../main.do) in the Aldec simulator console.    ")
    print(" 4. Press Enter.                                                     ")

#-------------------------------------------------------------------------------------------------#
# waiting...
#-------------------------------------------------------------------------------------------------#

input("\n\n Press Enter to exit...")

#-------------------------------------------------------------------------------------------------#




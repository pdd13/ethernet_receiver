#-------------------------------------------------------------------------------------------------#
#
#  Company      : Micran, Department of Telecommunications
#
#  Engineer     : Pogodaev Danil
#
#-------------------------------------------------------------------------------------------------#
#
#  Library      : lib_ethernet / library of ethernet components
#
#  File Name    : directory .py (python 3)
#
#-------------------------------------------------------------------------------------------------#
#
#  Description  : Specify directory for simulation. Used for 'mentor' folder.
#
#                  - creating a tcl file 'directory.do'
#                  - creating a text file 'console.txt'
#                  - creating a text file 'readme.txt'
#
#-------------------------------------------------------------------------------------------------#
#
#  Note         : Instructions
#
#                 1. Launch the python-script 'directory.py' for directory settings
#                 2. Copy the contents (command) of the text file 'console .txt'
#                 3. Insert command (do ../main.do) in the Mentor simulator console
#                 4. Press Enter
#
#-------------------------------------------------------------------------------------------------#
#
#  Limitation   : Placement of the python-script file 'directory .py'
#
#                 ../hard/sim/mentor/directory.py
#
#-------------------------------------------------------------------------------------------------#
#
#  Revision List
#
#  Version   Date       Author
#
#  1.0.0     16.09.16   Pogodaev Danil
#
#-------------------------------------------------------------------------------------------------#

#-------------------------------------------------------------------------------------------------#
# import
#-------------------------------------------------------------------------------------------------#

import os
import re

#-------------------------------------------------------------------------------------------------#
# path to folder 'hard/sim/mentor'
#-------------------------------------------------------------------------------------------------#

# path getting

path_mentor = os.path.dirname(__file__)
path_mentor_len = len(path_mentor)

# replace backslash (\) to slash (/)

backslash = "\\"
path_mentor_new = ""

for letter in path_mentor:
    if letter not in backslash:
        path_mentor_new += letter
    else:
        path_mentor_new += "/"

# path result

path_mentor = path_mentor_new

# path checking

check = "hard/sim/mentor"
path_true = 0

if check in path_mentor:
    path_true = 1
else:
    path_true = 0

    print("\n Error!\n                                                     ")
    print("\n Reason  : Incorrect placement of the file 'directory.py'.    ")
    print("\n Actions : Move the file 'directory.py' to correct placement: ")
    print("\n           .../hard/sim/mentor/directory.py                   ")

#-------------------------------------------------------------------------------------------------#
# path to file 'main.do' (folder 'mentor')
#-------------------------------------------------------------------------------------------------#

if path_true:

    path_main = path_mentor + "/main.do"
    do_main = "do " + path_main

#-------------------------------------------------------------------------------------------------#
# path to folder 'hard'
#-------------------------------------------------------------------------------------------------#

# length calculator

if path_true:

    path_over = "sim/mentor"

    path_over_len = len(path_over) + 1
    path_hard_len = path_mentor_len - path_over_len

# path getting

if path_true:

    path_cnt  = 0
    path_hard = ""

    for letter in path_mentor:
        if path_cnt < path_hard_len:
            path_hard += letter
            path_cnt  += 1

#-------------------------------------------------------------------------------------------------#
# create file 'console.txt'
#-------------------------------------------------------------------------------------------------#

if path_true:

    text_file = open("console.txt", "w")
    text_file.write(do_main)
    text_file.close()

#-------------------------------------------------------------------------------------------------#
# create file 'directory.do'
#-------------------------------------------------------------------------------------------------#

if path_true:

    text_file = open("directory.do", "w")

    # line comment

    comment_cnt = 0
    commemt_len = 147 # 147 + 3 = 150
    comment     = ""

    for i in range(commemt_len):
    
        if comment_cnt == 0: # first symbol
            comment_cnt += 1
            comment += "\n  #"
        
        elif comment_cnt < (commemt_len - 1):
            comment_cnt += 1
            comment += "-"

        elif comment_cnt == (commemt_len - 1): # last symbol
            comment_cnt += 1
            comment += "#\n"

    # write to file

    set_console = "  # " + do_main

    text_file.writelines(comment)
    text_file.writelines("  # launch the main do-file in console of the mentor simulator")
    text_file.writelines(comment)
    text_file.writelines("  \n")
    text_file.writelines(set_console)
    text_file.writelines("  \n")

    set_dir = "  set set_dir  " + path_hard

    text_file.writelines(comment)
    text_file.writelines("  # directory setting (hard folder)")
    text_file.writelines(comment)
    text_file.writelines("  \n")
    text_file.writelines(set_dir)
    text_file.writelines("  \n")
    text_file.writelines(comment)

    text_file.close()

#-------------------------------------------------------------------------------------------------#
# create file 'readme.txt'
#-------------------------------------------------------------------------------------------------#

if path_true:

    text_file = open("readme.txt", "w")

    # write to file

    set_readme = """
 Instructions
 ------------

 1. Launch the script "directory.py" (Python 3) for directory settings.

 2. Copy the contents (command) of the text file "console .txt".

 3. Paste command (do ../main.do) in the Mentor simulator console.

 4. Press Enter.
"""
    text_file.writelines(set_readme)
    text_file.close()

#-------------------------------------------------------------------------------------------------#
# print information
#-------------------------------------------------------------------------------------------------#

# path info

if path_true:

    print(" \n                        "              )
    print(" PATHS:                    "              )
    print(" \n                        "              )
    print(" Path to folder 'hard'    :", path_hard   )
    print(" Path to folder 'mentor'  :", path_mentor )
    print(" Path to file   'main.do' :", path_main   )

# files info

if path_true:

    print(" \n                                   ")
    print(" FILES:                               ")
    print(" \n                                   ")
    print(" Creating a text file: readme    .txt ")
    print(" Creating a text file: console   .txt ")
    print(" Creating a tcl  file: directory .do  ")

# instructions

if path_true:

    print(" \n                                                                 ")
    print(" Instructions:                                                      ")
    print(" ------------                                                       ")
    print(" \n                                                                 ")
    print(" 1. Launch the python-script 'directory.py' for directory settings. ")
    print(" 2. Copy the contents (command) of the text file 'console .txt'.    ")
    print(" 3. Paste command (do ../main.do) in the Mentor simulator console.  ")
    print(" 4. Press Enter.                                                    ")

#-------------------------------------------------------------------------------------------------#
# waiting...
#-------------------------------------------------------------------------------------------------#

input("\n\n Press Enter to exit...")

#-------------------------------------------------------------------------------------------------#




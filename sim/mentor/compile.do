  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  #
  #  Company      : Micran, Department of Telecommunications
  #
  #  Engineer     : Pogodaev Danil
  #
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  #
  #  Library      : lib_ethernet / library of ethernet components
  #
  #  Component    : eth_rx_v2 / receiver of ethernet frames (packets)
  #
  #  File Name    : compile .do (tcl sim mentor)
  #
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  #
  #  Description  : Compilation script for Aldec simulator.
  #
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  #
  #  Description  : Compilation script for Mentor simulator.
  #
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  #
  #  Note (1)     : Command 'vlib'         The vlib command creates a design library.
  #  
  #  Note (2)     : Command 'vlog'         The vlog command compiles Verilog source code and System Verilog extensions into a specified working library (or to the work library by default).
  #
  #                  -sv                   Enables SystemVerilog features and keywords. Optional. By default compiler follows the rules of IEEE Std 1364-2001 and ignores
  #                                        SystemVerilog keywords. If a source file has a ".sv" extension, compiler will automatically parse SystemVerilog keywords.
  #
  #                  +incdir+<directory>   Specifies directories to search for files included with `include compiler directives.
  #                                        By default, the current directory is searched first and then the directories specified by the +incdir options in the order they appear on
  #                                        the command line. You may specify multiple +incdir options as well as multiple directories separated by "+" in a single +incdir option.
  #
  #                  <filename>            Specifies the name of the Verilog source code file to compile. One filename is required.
  #                                        Multiple filenames can be entered separated by spaces. Wildcards can be used.
  #
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  #
  #  Revision List
  #
  #  Version   Date       Author
  #
  #  2.0       26.09.16   Pogodaev Danil
  #
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  # create the work library
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

  vlib work

  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  # compile : testbench files
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

  # top testbench                                 +function                  +include

  vlog -sv $dir_tb__top_tb/tb_top.sv              +incdir+$dir_fun__lib_glb  +incdir+$dir_inc__top_tb

  # top generator                                 +function                  +include

  vlog -sv $dir_tb__top_gen/tb_gen.sv             +incdir+$dir_fun__lib_glb  +incdir+$dir_inc__top_tb+$dir_inc__top_gen
  vlog -sv $dir_tb__top_gen/tb_gen_emu.sv         +incdir+$dir_fun__lib_glb  +incdir+$dir_inc__top_tb+$dir_inc__top_gen
  vlog -sv $dir_tb__top_gen/tb_gen_emu_m.sv       +incdir+$dir_fun__lib_glb  +incdir+$dir_inc__top_tb+$dir_inc__top_gen

  # lib ethernet                                  +function

  vlog -sv $dir_tb__lib_eth/tb_eth_emu_v2.sv      +incdir+$dir_fun__lib_glb
  vlog -sv $dir_tb__lib_eth/tb_eth_emu_ini_v2.sv  +incdir+$dir_fun__lib_glb
  vlog -sv $dir_tb__lib_eth/tb_eth_emu_clk_v2.sv  +incdir+$dir_fun__lib_glb
  vlog -sv $dir_tb__lib_eth/tb_eth_emu_str_v2.sv  +incdir+$dir_fun__lib_glb
  vlog -sv $dir_tb__lib_eth/tb_eth_emu_dat_v2.sv  +incdir+$dir_fun__lib_glb+$dir_fun__lib_eth
  vlog -sv $dir_tb__lib_eth/tb_eth_emu_lan_v2.sv  +incdir+$dir_fun__lib_glb
  vlog -sv $dir_tb__lib_eth/tb_eth_emu_spd_v2.sv  +incdir+$dir_fun__lib_glb

  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  # compile : rtl files
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

  # lib internal                                  +function

  vlog -sv $dir_rtl__lib_int/eth_rx_v2.sv         +incdir+$dir_fun__lib_glb
  vlog -sv $dir_rtl__lib_int/eth_rx_mii_v2.sv     +incdir+$dir_fun__lib_glb
  vlog -sv $dir_rtl__lib_int/eth_rx_rmii_v2.sv    +incdir+$dir_fun__lib_glb
  vlog -sv $dir_rtl__lib_int/eth_rx_gmii_v2.sv    +incdir+$dir_fun__lib_glb
  vlog -sv $dir_rtl__lib_int/eth_rx_cst_v2.sv     +incdir+$dir_fun__lib_glb
  vlog -sv $dir_rtl__lib_int/eth_rx_mux_v2.sv     +incdir+$dir_fun__lib_glb
  vlog -sv $dir_rtl__lib_int/eth_rx_head_v2.sv    +incdir+$dir_fun__lib_glb
  vlog -sv $dir_rtl__lib_int/eth_rx_str_v2.sv     +incdir+$dir_fun__lib_glb
  vlog -sv $dir_rtl__lib_int/eth_rx_dat_v2.sv     +incdir+$dir_fun__lib_glb
  vlog -sv $dir_rtl__lib_int/eth_rx_out_v2.sv     +incdir+$dir_fun__lib_glb
  
  # lib ethernet                                  +function
  
  vlog -sv $dir_rtl__lib_eth/eth_cnt_v2.sv        +incdir+$dir_fun__lib_glb
  vlog -sv $dir_rtl__lib_eth/eth_rx_crc_v2.sv     +incdir+$dir_fun__lib_glb+$dir_fun__lib_eth
    
  # lib global                                    +function

  vlog -sv $dir_rtl__lib_glb/z_line.sv            +incdir+$dir_fun__lib_glb
  vlog -sv $dir_rtl__lib_glb/gen_npr.sv           +incdir+$dir_fun__lib_glb
  vlog -sv $dir_rtl__lib_glb/det_se.sv            +incdir+$dir_fun__lib_glb
  vlog -sv $dir_rtl__lib_glb/det_de.sv            +incdir+$dir_fun__lib_glb
  vlog -sv $dir_rtl__lib_glb/gen_sstr.sv          +incdir+$dir_fun__lib_glb
  vlog -sv $dir_rtl__lib_glb/gen_mstr.sv          +incdir+$dir_fun__lib_glb
  vlog -sv $dir_rtl__lib_glb/gen_cen.sv           +incdir+$dir_fun__lib_glb
  vlog -sv $dir_rtl__lib_glb/gen_ced.sv           +incdir+$dir_fun__lib_glb
  vlog -sv $dir_rtl__lib_glb/shift_p2s.sv         +incdir+$dir_fun__lib_glb
  vlog -sv $dir_rtl__lib_glb/shift_s2p.sv         +incdir+$dir_fun__lib_glb

  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#




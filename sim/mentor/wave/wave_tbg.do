  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  #
  #  Company      : Micran, Department of Telecommunications
  #
  #  Engineer     : Pogodaev Danil
  #
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  #
  #  Library      : lib_ethernet / library of ethernet components
  #
  #  Component    : eth_rx_v2 / receiver of ethernet frames (packets)
  #
  #  File Name    : wave_tbg .do (tcl sim mentor)
  #
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  #
  #  Description  : List of module waveforms for Mentor simulator.
  #
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  #
  #  Note (1)     : Command 'add wave'              The add wave command adds the following objects to the Wave window:
  #
  #                                                  � VHDL signals and variables
  #                                                  � Verilog nets and registers
  #                                                  � System Verilog class objects
  #                                                  � SystemC primitive channels (signals)
  #                                                  � Dividers and user-defined buses
  #
  #                  -noupdate                      A switch that prevents the Wave window from updating when a series of add wave commands are executed in series.
  #
  #                                                 If no port mode is specified, add wave will display all objects in the selected region with names matching the object
  #                                                 name specification.
  #
  #                  -expand <signal_name>          A switch and argument pair that instructs the command to expand a compound signal immediately, but only one level down.
  #                                                 <signal_name> � a string that specifies the name of the signal. This string can include wildcards.
  #
  #                  -group <group_name>            A switch and argument group that creates a wave group with the specified group_name.
  #                  [<sig_name1> ...]   
  #                                                 <group_name> � a string that specifies the name of the group. You must enclose this argument in quotes (") or braces ({ })
  #                                                 if it contains any white space.
  #
  #                                                 <sig_name> ... � a string, which is repeatable in a space separated list, that specifies the signals to add to the group.
  #                                                 This command creates an empty group if you do not specify any signal names.
  #
  #                  -height <pixels>               A switch and argument pair that specifies the height, in pixels, of the waveform.
  #
  #                  -color <standard_color_name>   A switch and argument pair that specifies the color used to display a waveform.
  #
  #                                                 <standard_color_name> � You can use either of the following:
  #
  #                                                  � standard X Window color name � enclose 2-word names in quotes ("), for example: -color "light blue"
  #                                                  � rgb value, for example: -color #357f77
  #  
  #                                                 Definitions of color names (e.g. steelblue or gainsboro) have been provided by the World Wide Web Consortium and
  #                                                 are available at www.w3.org/TR/SVG/types.html#ColorKeywords. Color transparent represents the absence of a color.
  #
  #                  -<format>                      A choice between switches that specify the display format of the objects. Valid entries are:
  #
  #                                                  -format literal               Literal waveforms are displayed as a box containing the object value.
  #                                                  -format logic                 Logic signals may be U, X, 0, 1, Z, W, L, H, or �-�.
  #                                                  -format analog-step           Analog-step changes to the new time before plotting the new Y.
  #                                                  -format analog-interpolated   Analog-interpolated draws a diagonal line.
  #                                                  -format analog-backstep       Analog-backstep plots the new Y before moving to the new time.
  #                                                  -format event                 Displays a mark at every transition.
  #
  #                  -radix <type>                  A choice between switches that specify the radix for the objects that follow in the command.
  #                                                 Valid entries (or any unique abbreviations) are:
  #                  -<radix_type>   
  #                                                  -radix binary    -binary
  #                                                  -radix ascii     -ascii
  #                                                  -radix unsigned  -unsigned
  #                                                  -radix decimal   -decimal
  #                                                  -radix octal     -octal
  #                                                  -radix hex       -hex
  #                                                  -radix symbolic  -symbolic
  #                                                  -radix time      -time
  #                                                  -radix default   -default
  #
  #                                                 If no radix is specified for an enumerated type, the default representation is used.
  #
  #                  -divider [<divider_name>]      A switch and argument pair that adds a divider to the Wave window.
  #
  #                                                 <divider_name> � A string, which is repeatable in a space separated list, that specifies the name of the divider,
  #                                                 which appears in the pathnames column.
  #
  #                                                 You cannot begin a name with a hyphen (-).
  #                                                 You can begin a name with a space, but you must enclose the name within quotes (") or braces ({ })
  #                                                 If you do not specify this argument, the command inserts an unnamed divider.
  #
  #                  -label <name>                  A switch and argument pair that specifies an alternative name for the signal being added.
  #                                                 This alternative name is not valid in a force or examine command; however, it can be used in a search command
  #                                                 with the wave option.
  #
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  #
  #  Revision List
  #
  #  Version   Date       Author
  #
  #  2.0       26.09.16   Pogodaev Danil
  #
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  # setting : module
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

  set module_index "tbg"
  set module_name  "eth_rx"
  set module_path  "/tb_top/tb_gen"

  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  # setting : wave groups (on/off)
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

  set wave_par 1 ; # group : parameters (1/0)
  set wave_io  1 ; # group : interface  (1/0)
  set wave_log 1 ; # group : logic      (1/0)
  set wave_all 0 ; # group : all wave   (1/0)

  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  # setting : wave groups (display)
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

  set g_mod " $module_index :  $module_name " ; # group : module
  set g_par " $module_index :  parameters   " ; # group : parameter
  set g_io  " $module_index :  interface    " ; # group : interface
  set g_log " $module_index :  logic        " ; # group : logic
  set g_all " $module_index :  all wave     " ; # group : all wave

  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  # add wave : module name
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

  add wave -noupdate                                                 -height $h_mod  -expand  -group $g_mod
  add wave -noupdate  -group $g_mod                   -divider       -height $h_nul  <NULL>
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  # add wave : parameters
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  if {$wave_par} {
  add wave -noupdate  -group $g_mod                                  -height $h_grp  -group $g_par
  add wave -noupdate  -group $g_par                   -divider       -height $h_nul  <NULL>
  add wave -noupdate  -group $g_par                   -divider       -height $h_ch1                        {//  rx counters ---------------------------------------------------------------------------------------------------}

  add wave -noupdate  -group $g_par  -color $c_param  -format logic  -height $h_sig  -radix $r_uns  -label { lpS_RCNT_P_UPD    } $module_path/lpS_RCNT_P_UPD

  add wave -noupdate  -group $g_par                   -divider       -height $h_ch1                        {//  local (for logic) ---------------------------------------------------------------------------------------------}

  add wave -noupdate  -group $g_par                   -divider       -height $h_ch2                        { >  custom mode                                                                                                   /}

  add wave -noupdate  -group $g_par  -color $c_param  -format logic  -height $h_sig  -radix $r_uns  -label { lpE_CST_CLK       } $module_path/lpE_CST_CLK
  add wave -noupdate  -group $g_par                   -divider       -height $h_nul  <NULL>
  }; # wave_par
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  # add wave : interface
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  if {$wave_io} {
  add wave -noupdate  -group $g_mod                                  -height $h_grp  -group $g_io
  add wave -noupdate  -group $g_io                    -divider       -height $h_nul  <NULL>
  add wave -noupdate  -group $g_io                    -divider       -height $h_ch1                        {//  reset ---------------------------------------------------------------------------------------------------------}

  add wave -noupdate  -group $g_io   -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_reset_n         } $module_path/i_reset_n

  add wave -noupdate  -group $g_io                    -divider       -height $h_ch1                        {//  setting -------------------------------------------------------------------------------------------------------}

  add wave -noupdate  -group $g_io   -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { i_set_cut         } $module_path/i_set_cut
  add wave -noupdate  -group $g_io   -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { i_set_jumbo       } $module_path/i_set_jumbo
  add wave -noupdate  -group $g_io   -color $c_pdata  -format logic  -height $h_sig  -radix $r_uns  -label { i_set_speed       } $module_path/i_set_speed

  add wave -noupdate  -group $g_io                    -divider       -height $h_ch1                        {//  lan interface -------------------------------------------------------------------------------------------------}

  add wave -noupdate  -group $g_io                    -divider       -height $h_ch2                        { >  lan mii                                                                                                       /}

  add wave -noupdate  -group $g_io   -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_mii_clk         } $module_path/i_mii_clk
  add wave -noupdate  -group $g_io   -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { i_mii_rx_dv       } $module_path/i_mii_rx_dv
  add wave -noupdate  -group $g_io   -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { i_mii_rxd         } $module_path/i_mii_rxd

  add wave -noupdate  -group $g_io                    -divider       -height $h_ch2                        { >  lan rmii                                                                                                      /}

  add wave -noupdate  -group $g_io   -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_rmii_clk        } $module_path/i_rmii_clk
  add wave -noupdate  -group $g_io   -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { o_rmii_clk_s      } $module_path/o_rmii_clk_s
  add wave -noupdate  -group $g_io   -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { i_rmii_crs_dv     } $module_path/i_rmii_crs_dv
  add wave -noupdate  -group $g_io   -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { i_rmii_rxd        } $module_path/i_rmii_rxd

  add wave -noupdate  -group $g_io                    -divider       -height $h_ch2                        { >  lan gmii                                                                                                      /}

  add wave -noupdate  -group $g_io   -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_gmii_clk        } $module_path/i_gmii_clk
  add wave -noupdate  -group $g_io   -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { i_gmii_rx_dv      } $module_path/i_gmii_rx_dv
  add wave -noupdate  -group $g_io   -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { i_gmii_rxd        } $module_path/i_gmii_rxd

  add wave -noupdate  -group $g_io                    -divider       -height $h_ch1                        {//  custom mode ---------------------------------------------------------------------------------------------------}

  add wave -noupdate  -group $g_io   -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_cst_clk         } $module_path/i_cst_clk
  add wave -noupdate  -group $g_io   -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_cst_cen         } $module_path/i_cst_cen
  add wave -noupdate  -group $g_io   -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_cst_ced         } $module_path/i_cst_ced
  add wave -noupdate  -group $g_io   -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { i_cst_rx_dv       } $module_path/i_cst_rx_dv
  add wave -noupdate  -group $g_io   -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { i_cst_rxd         } $module_path/i_cst_rxd

  add wave -noupdate  -group $g_io                    -divider       -height $h_ch1                        {//  rx packet -----------------------------------------------------------------------------------------------------}

  add wave -noupdate  -group $g_io                    -divider       -height $h_ch2                        { >  clock                                                                                                         /}

  add wave -noupdate  -group $g_io   -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { o_rx_clk          } $module_path/o_rx_clk
  add wave -noupdate  -group $g_io   -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { o_rx_cen          } $module_path/o_rx_cen
  add wave -noupdate  -group $g_io   -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { o_rx_ced          } $module_path/o_rx_ced

  add wave -noupdate  -group $g_io                    -divider       -height $h_ch2                        { >  strob                                                                                                         /}

  add wave -noupdate  -group $g_io   -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { o_rx_str_body     } $module_path/o_rx_str_body(0)
  add wave -noupdate  -group $g_io   -color $c_count  -format logic  -height $h_sig  -radix $r_uns  -label { o_rx_cnt_body     } $module_path/o_rx_cnt_body
  add wave -noupdate  -group $g_io   -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { o_rx_str_mac      } $module_path/o_rx_str_mac(0)
  add wave -noupdate  -group $g_io   -color $c_count  -format logic  -height $h_sig  -radix $r_uns  -label { o_rx_cnt_mac      } $module_path/o_rx_cnt_mac
  add wave -noupdate  -group $g_io   -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { o_rx_str_mda      } $module_path/o_rx_str_mda(0)
  add wave -noupdate  -group $g_io   -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { o_rx_str_msa      } $module_path/o_rx_str_msa(0)
  add wave -noupdate  -group $g_io   -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { o_rx_str_mlt      } $module_path/o_rx_str_mlt(0)
  add wave -noupdate  -group $g_io   -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { o_rx_str_ip       } $module_path/o_rx_str_ip(0)
  add wave -noupdate  -group $g_io   -color $c_count  -format logic  -height $h_sig  -radix $r_uns  -label { o_rx_cnt_ip       } $module_path/o_rx_cnt_ip
  add wave -noupdate  -group $g_io   -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { o_rx_str_pay      } $module_path/o_rx_str_pay(0)
  add wave -noupdate  -group $g_io   -color $c_count  -format logic  -height $h_sig  -radix $r_uns  -label { o_rx_cnt_pay      } $module_path/o_rx_cnt_pay
  add wave -noupdate  -group $g_io   -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { o_rx_str_gap      } $module_path/o_rx_str_gap(0)
  add wave -noupdate  -group $g_io   -color $c_count  -format logic  -height $h_sig  -radix $r_uns  -label { o_rx_cnt_gap      } $module_path/o_rx_cnt_gap

  add wave -noupdate  -group $g_io                    -divider       -height $h_ch2                        { >  data                                                                                                          /}

  add wave -noupdate  -group $g_io   -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { o_rx_dat_load     } $module_path/o_rx_dat_load
  add wave -noupdate  -group $g_io   -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { o_rx_dat_body     } $module_path/o_rx_dat_body

  add wave -noupdate  -group $g_io   -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { o_rx_rdy_mac      } $module_path/o_rx_rdy_mac
  add wave -noupdate  -group $g_io   -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { o_rx_dat_mda      } $module_path/o_rx_dat_mda
  add wave -noupdate  -group $g_io   -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { o_rx_dat_msa      } $module_path/o_rx_dat_msa
  add wave -noupdate  -group $g_io   -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { o_rx_dat_mlt      } $module_path/o_rx_dat_mlt

  add wave -noupdate  -group $g_io   -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { o_rx_rdy_ip       } $module_path/o_rx_rdy_ip
  add wave -noupdate  -group $g_io   -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { o_rx_dat_ip       } $module_path/o_rx_dat_ip

  add wave -noupdate  -group $g_io                    -divider       -height $h_ch2                        { >  length                                                                                                        /}

  add wave -noupdate  -group $g_io   -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { o_rx_rdy_body     } $module_path/o_rx_rdy_body
  add wave -noupdate  -group $g_io   -color $c_pdata  -format logic  -height $h_sig  -radix $r_uns  -label { o_rx_len_body     } $module_path/o_rx_len_body

  add wave -noupdate  -group $g_io   -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { o_rx_rdy_gap      } $module_path/o_rx_rdy_gap
  add wave -noupdate  -group $g_io   -color $c_pdata  -format logic  -height $h_sig  -radix $r_uns  -label { o_rx_len_gap      } $module_path/o_rx_len_gap

  add wave -noupdate  -group $g_io                    -divider       -height $h_ch2                        { >  fcs crc                                                                                                       /}

  add wave -noupdate  -group $g_io   -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { o_rx_crc_rdy      } $module_path/o_rx_crc_rdy
  add wave -noupdate  -group $g_io   -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { o_rx_crc_err      } $module_path/o_rx_crc_err

  add wave -noupdate  -group $g_io                    -divider       -height $h_ch1                        {//  rx counters ---------------------------------------------------------------------------------------------------}

  add wave -noupdate  -group $g_io                    -divider       -height $h_ch2                        { >  setting                                                                                                       /}

  add wave -noupdate  -group $g_io   -color $c_pdata  -format logic  -height $h_sig  -radix $r_uns  -label { i_rx_set_p_spd    } $module_path/i_rx_set_p_spd
  add wave -noupdate  -group $g_io   -color $c_pdata  -format logic  -height $h_sig  -radix $r_uns  -label { i_rx_set_p_top    } $module_path/i_rx_set_p_top
  add wave -noupdate  -group $g_io   -color $c_pdata  -format logic  -height $h_sig  -radix $r_uns  -label { i_rx_set_p_low    } $module_path/i_rx_set_p_low

  add wave -noupdate  -group $g_io                    -divider       -height $h_ch2                        { >  detector                                                                                                      /}

  add wave -noupdate  -group $g_io   -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { o_rx_det_p_rdy    } $module_path/o_rx_det_p_rdy
  add wave -noupdate  -group $g_io   -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { o_rx_det_p_err    } $module_path/o_rx_det_p_err
  add wave -noupdate  -group $g_io   -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { o_rx_det_p_cut    } $module_path/o_rx_det_p_cut
  add wave -noupdate  -group $g_io   -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { i_rx_det_p_off    } $module_path/i_rx_det_p_off

  add wave -noupdate  -group $g_io                    -divider       -height $h_ch2                        { >  counters                                                                                                      /}

  add wave -noupdate  -group $g_io   -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { i_rx_cnt_p_clr    } $module_path/i_rx_cnt_p_clr
  add wave -noupdate  -group $g_io   -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { o_rx_cnt_p_rdy    } $module_path/o_rx_cnt_p_rdy
  add wave -noupdate  -group $g_io   -color $c_count  -format logic  -height $h_sig  -radix $r_uns  -label { o_rx_cnt_p_all    } $module_path/o_rx_cnt_p_all
  add wave -noupdate  -group $g_io   -color $c_count  -format logic  -height $h_sig  -radix $r_uns  -label { o_rx_cnt_p_err    } $module_path/o_rx_cnt_p_err
  add wave -noupdate  -group $g_io   -color $c_count  -format logic  -height $h_sig  -radix $r_uns  -label { o_rx_cnt_p_off    } $module_path/o_rx_cnt_p_off
  add wave -noupdate  -group $g_io   -color $c_count  -format logic  -height $h_sig  -radix $r_uns  -label { o_rx_cnt_p_spd    } $module_path/o_rx_cnt_p_spd

  add wave -noupdate  -group $g_io                    -divider       -height $h_ch2                        { >  counters (rhythm)                                                                                             /}

  add wave -noupdate  -group $g_io   -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { i_rx_rcnt_p_clr   } $module_path/i_rx_rcnt_p_clr
  add wave -noupdate  -group $g_io   -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { i_rx_rcnt_p_upd   } $module_path/i_rx_rcnt_p_upd
  add wave -noupdate  -group $g_io   -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { o_rx_rcnt_p_rdy   } $module_path/o_rx_rcnt_p_rdy
  add wave -noupdate  -group $g_io   -color $c_count  -format logic  -height $h_sig  -radix $r_uns  -label { o_rx_rcnt_p_all   } $module_path/o_rx_rcnt_p_all
  add wave -noupdate  -group $g_io   -color $c_count  -format logic  -height $h_sig  -radix $r_uns  -label { o_rx_rcnt_p_err   } $module_path/o_rx_rcnt_p_err
  add wave -noupdate  -group $g_io   -color $c_count  -format logic  -height $h_sig  -radix $r_uns  -label { o_rx_rcnt_p_off   } $module_path/o_rx_rcnt_p_off
  add wave -noupdate  -group $g_io   -color $c_count  -format logic  -height $h_sig  -radix $r_uns  -label { o_rx_rcnt_p_spd   } $module_path/o_rx_rcnt_p_spd
  add wave -noupdate  -group $g_io                    -divider       -height $h_nul  <NULL>
  }; # wave_io
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  # add wave : logic
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  if {$wave_log} {
  add wave -noupdate  -group $g_mod                                  -height $h_grp  -group $g_log
  add wave -noupdate  -group $g_log                   -divider       -height $h_nul  <NULL>
  add wave -noupdate  -group $g_log                   -divider       -height $h_ch1                        {//  global --------------------------------------------------------------------------------------------------------}

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  clock                                                                                                         /}

  add wave -noupdate  -group $g_log  -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { clk               } $module_path/clk
  add wave -noupdate  -group $g_log  -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { cen               } $module_path/cen
  add wave -noupdate  -group $g_log  -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { ced               } $module_path/ced

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  clear                                                                                                         /}

  add wave -noupdate  -group $g_log  -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { sclr              } $module_path/sclr

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch1                        {//  sync counters -------------------------------------------------------------------------------------------------}

  add wave -noupdate  -group $g_log  -color $c_count  -format logic  -height $h_sig  -radix $r_uns  -label { cnt_clk           } $module_path/cnt_clk
  add wave -noupdate  -group $g_log  -color $c_count  -format logic  -height $h_sig  -radix $r_uns  -label { cnt_cen           } $module_path/cnt_cen
  add wave -noupdate  -group $g_log  -color $c_count  -format logic  -height $h_sig  -radix $r_uns  -label { cnt_ced           } $module_path/cnt_ced
  add wave -noupdate  -group $g_log  -color $c_count  -format logic  -height $h_sig  -radix $r_uns  -label { cnt_pack          } $module_path/cnt_pack

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch1                        {//  reset ---------------------------------------------------------------------------------------------------------}

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  parameters                                                                                                    /}

  add wave -noupdate  -group $g_log  -color $c_param  -format logic  -height $h_sig  -radix $r_uns  -label { lpE_RESET_N       } $module_path/lpE_RESET_N

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  clock                                                                                                         /}

  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { clk               } $module_path/clk

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  reset                                                                                                         /}

  add wave -noupdate  -group $g_log  -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { reset_n           } $module_path/reset_n
  add wave -noupdate  -group $g_log  -color $c_count  -format logic  -height $h_sig  -radix $r_uns  -label { reset_n_cnt       } $module_path/reset_n_cnt

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch1                        {//  setting -------------------------------------------------------------------------------------------------------}

  add wave -noupdate  -group $g_log  -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { set_cut           } $module_path/set_cut
  add wave -noupdate  -group $g_log  -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { set_jumbo         } $module_path/set_jumbo
  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_uns  -label { set_speed         } $module_path/set_speed

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch1                        {//  rx counters / offcast -----------------------------------------------------------------------------------------}

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  parameters                                                                                                    /}

  add wave -noupdate  -group $g_log  -color $c_param  -format logic  -height $h_sig  -radix $r_uns  -label { lpE_DET_P_OFF     } $module_path/lpE_DET_P_OFF

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  clock                                                                                                         /}

  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { clk               } $module_path/clk
  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { cen               } $module_path/cen
  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { ced               } $module_path/ced

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  counters                                                                                                      /}

  add wave -noupdate  -group $g_log  -color $c_count  -format logic  -height $h_sig  -radix $r_uns  -label { cnt_clk           } $module_path/cnt_clk
  add wave -noupdate  -group $g_log  -color $c_count  -format logic  -height $h_sig  -radix $r_uns  -label { cnt_cen           } $module_path/cnt_cen
  add wave -noupdate  -group $g_log  -color $c_count  -format logic  -height $h_sig  -radix $r_uns  -label { cnt_ced           } $module_path/cnt_ced
  add wave -noupdate  -group $g_log  -color $c_count  -format logic  -height $h_sig  -radix $r_uns  -label { cnt_pack          } $module_path/cnt_pack

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  offcast                                                                                                       /}

  add wave -noupdate  -group $g_log  -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { det_p_off         } $module_path/det_p_off
  add wave -noupdate  -group $g_log  -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { det_p_off_sync    } $module_path/det_p_off_sync

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch1                        {//  rx counters / clear -------------------------------------------------------------------------------------------}

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  parameters                                                                                                    /}

  add wave -noupdate  -group $g_log  -color $c_param  -format logic  -height $h_sig  -radix $r_uns  -label { lpE_CNT_P_CLR     } $module_path/lpE_CNT_P_CLR
  add wave -noupdate  -group $g_log  -color $c_param  -format logic  -height $h_sig  -radix $r_uns  -label { lpE_RCNT_P_CLR    } $module_path/lpE_RCNT_P_CLR

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  clock                                                                                                         /}

  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { clk               } $module_path/clk
  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { cen               } $module_path/cen
  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { ced               } $module_path/ced

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  counters                                                                                                      /}

  add wave -noupdate  -group $g_log  -color $c_count  -format logic  -height $h_sig  -radix $r_uns  -label { cnt_clk           } $module_path/cnt_clk
  add wave -noupdate  -group $g_log  -color $c_count  -format logic  -height $h_sig  -radix $r_uns  -label { cnt_cen           } $module_path/cnt_cen
  add wave -noupdate  -group $g_log  -color $c_count  -format logic  -height $h_sig  -radix $r_uns  -label { cnt_ced           } $module_path/cnt_ced
  add wave -noupdate  -group $g_log  -color $c_count  -format logic  -height $h_sig  -radix $r_uns  -label { cnt_pack          } $module_path/cnt_pack

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  clear                                                                                                         /}

  add wave -noupdate  -group $g_log  -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { cnt_p_clr         } $module_path/cnt_p_clr
  add wave -noupdate  -group $g_log  -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { rcnt_p_clr        } $module_path/rcnt_p_clr

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch1                        {//  counters / update ---------------------------------------------------------------------------------------------}

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  parameters                                                                                                    /}

  add wave -noupdate  -group $g_log  -color $c_param  -format logic  -height $h_sig  -radix $r_uns  -label { lpT_UDP           } $module_path/lpT_UDP

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  clock                                                                                                         /}

  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { clk               } $module_path/clk
  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { cen               } $module_path/cen
  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { ced               } $module_path/ced

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  counter                                                                                                       /}

  add wave -noupdate  -group $g_log  -color $c_count  -format logic  -height $h_sig  -radix $r_uns  -label { rcnt_p_upd_cnt    } $module_path/rcnt_p_upd_cnt

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  update                                                                                                        /}

  add wave -noupdate  -group $g_log  -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { rcnt_p_upd        } $module_path/rcnt_p_upd
  add wave -noupdate  -group $g_log  -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { rcnt_p_upd_sync   } $module_path/rcnt_p_upd_sync

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch1                        {//  rx counters / update (speed) ----------------------------------------------------------------------------------}

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  parameters                                                                                                    /}

  add wave -noupdate  -group $g_log  -color $c_param  -format logic  -height $h_sig  -radix $r_uns  -label { lpT_CLK           } $module_path/lpT_CLK
  add wave -noupdate  -group $g_log  -color $c_param  -format logic  -height $h_sig  -radix $r_uns  -label { lpT_UDP_S         } $module_path/lpT_UDP_S
  add wave -noupdate  -group $g_log  -color $c_param  -format logic  -height $h_sig  -radix $r_uns  -label { lpT_CNT           } $module_path/lpT_CNT
  add wave -noupdate  -group $g_log  -color $c_param  -format logic  -height $h_sig  -radix $r_uns  -label { lpC_SEC           } $module_path/lpC_SEC

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  clock                                                                                                         /}

  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { clk               } $module_path/clk
  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { cen               } $module_path/cen
  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { ced               } $module_path/ced

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  counter                                                                                                       /}

  add wave -noupdate  -group $g_log  -color $c_count  -format logic  -height $h_sig  -radix $r_uns  -label { rcnt_p_upd_s_cnt  } $module_path/rcnt_p_upd_s_cnt
  add wave -noupdate  -group $g_log  -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { rcnt_p_upd_s_nxt  } $module_path/rcnt_p_upd_s_nxt

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  update                                                                                                        /}

  add wave -noupdate  -group $g_log  -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { rcnt_p_upd_s      } $module_path/rcnt_p_upd_s
  add wave -noupdate  -group $g_log  -color $c_count  -format logic  -height $h_sig  -radix $r_uns  -label { rcnt_p_spd        } $module_path/o_rx_rcnt_p_spd

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  speed                                                                                                         /}

  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_uns  -label { rx_speed          } $module_path/rx_speed

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch1                        {//  rx counters / setting -----------------------------------------------------------------------------------------}

  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_uns  -label { set_p_spd         } $module_path/set_p_spd
  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_uns  -label { set_p_top         } $module_path/set_p_top
  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_uns  -label { set_p_low         } $module_path/set_p_low
  add wave -noupdate  -group $g_log                   -divider       -height $h_nul  <NULL>
  }; # wave_log
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  # add wave : all wave
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  if {$wave_all} {
  add wave -noupdate  -group $g_mod                                  -height $h_grp  -group $g_all
  add wave -noupdate  -group $g_all                   -divider       -height $h_nul  <NULL>
  add wave -noupdate  -group $g_all                                                                                              $module_path/*
  add wave -noupdate  -group $g_all                   -divider       -height $h_nul  <NULL>
  }; # wave_all
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

  add wave -noupdate  -group $g_mod                   -divider       -height $h_nul  <NULL>

  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#




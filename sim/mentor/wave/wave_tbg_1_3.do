  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  #
  #  Company      : Micran, Department of Telecommunications
  #
  #  Engineer     : Pogodaev Danil
  #
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  #
  #  Library      : lib_ethernet / library of ethernet components
  #
  #  Component    : eth_rx_v2 / receiver of ethernet frames (packets)
  #
  #  File Name    : wave_tbg_1_3 .do (tcl sim mentor)
  #
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  #
  #  Description  : List of module waveforms for Mentor simulator.
  #
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  #
  #  Note (1)     : Command 'add wave'              The add wave command adds the following objects to the Wave window:
  #
  #                                                  � VHDL signals and variables
  #                                                  � Verilog nets and registers
  #                                                  � System Verilog class objects
  #                                                  � SystemC primitive channels (signals)
  #                                                  � Dividers and user-defined buses
  #
  #                  -noupdate                      A switch that prevents the Wave window from updating when a series of add wave commands are executed in series.
  #
  #                                                 If no port mode is specified, add wave will display all objects in the selected region with names matching the object
  #                                                 name specification.
  #
  #                  -expand <signal_name>          A switch and argument pair that instructs the command to expand a compound signal immediately, but only one level down.
  #                                                 <signal_name> � a string that specifies the name of the signal. This string can include wildcards.
  #
  #                  -group <group_name>            A switch and argument group that creates a wave group with the specified group_name.
  #                  [<sig_name1> ...]   
  #                                                 <group_name> � a string that specifies the name of the group. You must enclose this argument in quotes (") or braces ({ })
  #                                                 if it contains any white space.
  #
  #                                                 <sig_name> ... � a string, which is repeatable in a space separated list, that specifies the signals to add to the group.
  #                                                 This command creates an empty group if you do not specify any signal names.
  #
  #                  -height <pixels>               A switch and argument pair that specifies the height, in pixels, of the waveform.
  #
  #                  -color <standard_color_name>   A switch and argument pair that specifies the color used to display a waveform.
  #
  #                                                 <standard_color_name> � You can use either of the following:
  #
  #                                                  � standard X Window color name � enclose 2-word names in quotes ("), for example: -color "light blue"
  #                                                  � rgb value, for example: -color #357f77
  #  
  #                                                 Definitions of color names (e.g. steelblue or gainsboro) have been provided by the World Wide Web Consortium and
  #                                                 are available at www.w3.org/TR/SVG/types.html#ColorKeywords. Color transparent represents the absence of a color.
  #
  #                  -<format>                      A choice between switches that specify the display format of the objects. Valid entries are:
  #
  #                                                  -format literal               Literal waveforms are displayed as a box containing the object value.
  #                                                  -format logic                 Logic signals may be U, X, 0, 1, Z, W, L, H, or �-�.
  #                                                  -format analog-step           Analog-step changes to the new time before plotting the new Y.
  #                                                  -format analog-interpolated   Analog-interpolated draws a diagonal line.
  #                                                  -format analog-backstep       Analog-backstep plots the new Y before moving to the new time.
  #                                                  -format event                 Displays a mark at every transition.
  #
  #                  -radix <type>                  A choice between switches that specify the radix for the objects that follow in the command.
  #                                                 Valid entries (or any unique abbreviations) are:
  #                  -<radix_type>   
  #                                                  -radix binary    -binary
  #                                                  -radix ascii     -ascii
  #                                                  -radix unsigned  -unsigned
  #                                                  -radix decimal   -decimal
  #                                                  -radix octal     -octal
  #                                                  -radix hex       -hex
  #                                                  -radix symbolic  -symbolic
  #                                                  -radix time      -time
  #                                                  -radix default   -default
  #
  #                                                 If no radix is specified for an enumerated type, the default representation is used.
  #
  #                  -divider [<divider_name>]      A switch and argument pair that adds a divider to the Wave window.
  #
  #                                                 <divider_name> � A string, which is repeatable in a space separated list, that specifies the name of the divider,
  #                                                 which appears in the pathnames column.
  #
  #                                                 You cannot begin a name with a hyphen (-).
  #                                                 You can begin a name with a space, but you must enclose the name within quotes (") or braces ({ })
  #                                                 If you do not specify this argument, the command inserts an unnamed divider.
  #
  #                  -label <name>                  A switch and argument pair that specifies an alternative name for the signal being added.
  #                                                 This alternative name is not valid in a force or examine command; however, it can be used in a search command
  #                                                 with the wave option.
  #
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  #
  #  Revision List
  #
  #  Version   Date       Author
  #
  #  2.0       26.09.16   Pogodaev Danil
  #
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  # setting : module
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

  set module_index "tbg 1.3"
  set module_name  "emu_str"
  set module_path  "/tb_top/tb_gen/eth_emu/emu_str"

  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  # setting : wave groups (on/off)
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

  set wave_par 1 ; # group : parameters (1/0)
  set wave_io  1 ; # group : interface  (1/0)
  set wave_log 1 ; # group : logic      (1/0)
  set wave_all 0 ; # group : all wave   (1/0)

  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  # setting : wave groups (display)
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

  set g_mod " $module_index :  $module_name " ; # group : module
  set g_par " $module_index :  parameters   " ; # group : parameter
  set g_io  " $module_index :  interface    " ; # group : interface
  set g_log " $module_index :  logic        " ; # group : logic
  set g_all " $module_index :  all wave     " ; # group : all wave

  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  # add wave : module name
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

  add wave -noupdate                                                 -height $h_mod  -expand  -group $g_mod
  add wave -noupdate  -group $g_mod                   -divider       -height $h_nul  <NULL>
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  # add wave : parameters
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  if {$wave_par} {
  add wave -noupdate  -group $g_mod                                  -height $h_grp  -group $g_par
  add wave -noupdate  -group $g_par                   -divider       -height $h_nul  <NULL>
  add wave -noupdate  -group $g_par                   -divider       -height $h_ch1                        {//  packet structure ----------------------------------------------------------------------------------------------}

  add wave -noupdate  -group $g_par                   -divider       -height $h_ch2                        { >  frame header                                                                                                  /}

  add wave -noupdate  -group $g_par  -color $c_param  -format logic  -height $h_sig  -radix $r_uns  -label { pE_LAN_HEAD       } $module_path/pE_LAN_HEAD

  add wave -noupdate  -group $g_par                   -divider       -height $h_ch2                        { >  frame length                                                                                                  /}

  add wave -noupdate  -group $g_par  -color $c_param  -format logic  -height $h_sig  -radix $r_uns  -label { pW_LAN_LEN        } $module_path/pW_LAN_LEN
  add wave -noupdate  -group $g_par  -color $c_param  -format logic  -height $h_sig  -radix $r_uns  -label { pW_LAN_GAP        } $module_path/pW_LAN_GAP

  add wave -noupdate  -group $g_par                   -divider       -height $h_ch2                        { >  ip header                                                                                                     /}

  add wave -noupdate  -group $g_par  -color $c_param  -format logic  -height $h_sig  -radix $r_uns  -label { pN_LAN_IPL        } $module_path/pN_LAN_IPL
  add wave -noupdate  -group $g_par  -color $c_param  -format logic  -height $h_sig  -radix $r_uns  -label { pN_LAN_IPH        } $module_path/pN_LAN_IPH

  add wave -noupdate  -group $g_par                   -divider       -height $h_ch1                        {//  packet counters -----------------------------------------------------------------------------------------------}

  add wave -noupdate  -group $g_par  -color $c_param  -format logic  -height $h_sig  -radix $r_uns  -label { pN_LAN_CNT        } $module_path/pN_LAN_CNT
  add wave -noupdate  -group $g_par  -color $c_param  -format logic  -height $h_sig  -radix $r_uns  -label { pW_LAN_CNT        } $module_path/pW_LAN_CNT

  add wave -noupdate  -group $g_par                   -divider       -height $h_ch1                        {//  local (for i/o) -----------------------------------------------------------------------------------------------}

  add wave -noupdate  -group $g_par                   -divider       -height $h_ch2                        { >  packet structure                                                                                              /}

  add wave -noupdate  -group $g_par  -color $c_param  -format logic  -height $h_sig  -radix $r_uns  -label { lpN_LAN_IP        } $module_path/lpN_LAN_IP
  add wave -noupdate  -group $g_par  -color $c_param  -format logic  -height $h_sig  -radix $r_uns  -label { lpW_LAN_IP_C      } $module_path/lpW_LAN_IP_C

  add wave -noupdate  -group $g_par                   -divider       -height $h_ch1                        {//  local (for logic) ---------------------------------------------------------------------------------------------}

  add wave -noupdate  -group $g_par                   -divider       -height $h_ch2                        { >  multi strob                                                                                                   /}
 
  add wave -noupdate  -group $g_par  -color $c_param  -format logic  -height $h_sig  -radix $r_uns  -label { lpN_STR_GEN       } $module_path/lpN_STR_GEN
  add wave -noupdate  -group $g_par  -color $c_param  -format logic  -height $h_sig  -radix $r_uns  -label { lpW_STR_GEN       } $module_path/lpW_STR_GEN
  add wave -noupdate  -group $g_par                   -divider       -height $h_nul  <NULL>
  }; # wave_par
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  # add wave : interface
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  if {$wave_io} {
  add wave -noupdate  -group $g_mod                                  -height $h_grp  -group $g_io
  add wave -noupdate  -group $g_io                    -divider       -height $h_nul  <NULL>
  add wave -noupdate  -group $g_io                    -divider       -height $h_ch1                        {//  clear ---------------------------------------------------------------------------------------------------------}

  add wave -noupdate  -group $g_io   -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_sclr_n          } $module_path/i_sclr_n

  add wave -noupdate  -group $g_io                    -divider       -height $h_ch1                        {//  setting -------------------------------------------------------------------------------------------------------}

  add wave -noupdate  -group $g_io                    -divider       -height $h_ch2                        { >  initialization                                                                                                /}

  add wave -noupdate  -group $g_io   -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { i_set_ini         } $module_path/i_set_ini

  add wave -noupdate  -group $g_io                    -divider       -height $h_ch2                        { >  interface                                                                                                     /}

  add wave -noupdate  -group $g_io   -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { i_set_io_pause    } $module_path/i_set_io_pause

  add wave -noupdate  -group $g_io                    -divider       -height $h_ch2                        { >  transmit speed                                                                                                /}

  add wave -noupdate  -group $g_io   -color $c_pdata  -format logic  -height $h_sig  -radix $r_uns  -label { i_set_tx_mode     } $module_path/i_set_tx_mode
  add wave -noupdate  -group $g_io   -color $c_pdata  -format logic  -height $h_sig  -radix $r_uns  -label { i_set_tx_gap      } $module_path/i_set_tx_gap

  add wave -noupdate  -group $g_io                    -divider       -height $h_ch2                        { >  crc error                                                                                                     /}

  add wave -noupdate  -group $g_io   -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { i_set_err_ena     } $module_path/i_set_err_ena
  add wave -noupdate  -group $g_io   -color $c_pdata  -format logic  -height $h_sig  -radix $r_uns  -label { i_set_err_per     } $module_path/i_set_err_per

  add wave -noupdate  -group $g_io                    -divider       -height $h_ch2                        { >  mac header                                                                                                    /}

  add wave -noupdate  -group $g_io   -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { i_set_mac_ena     } $module_path/i_set_mac_ena

  add wave -noupdate  -group $g_io                    -divider       -height $h_ch2                        { >  ip header                                                                                                     /}

  add wave -noupdate  -group $g_io   -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { i_set_ip_ena      } $module_path/i_set_ip_ena

  add wave -noupdate  -group $g_io                    -divider       -height $h_ch2                        { >  frame length                                                                                                  /}

  add wave -noupdate  -group $g_io   -color $c_pdata  -format logic  -height $h_sig  -radix $r_uns  -label { i_set_len_type    } $module_path/i_set_len_type
  add wave -noupdate  -group $g_io   -color $c_pdata  -format logic  -height $h_sig  -radix $r_uns  -label { i_set_len_const   } $module_path/i_set_len_const
  add wave -noupdate  -group $g_io   -color $c_pdata  -format logic  -height $h_sig  -radix $r_uns  -label { i_set_len_min     } $module_path/i_set_len_min
  add wave -noupdate  -group $g_io   -color $c_pdata  -format logic  -height $h_sig  -radix $r_uns  -label { i_set_len_max     } $module_path/i_set_len_max

  add wave -noupdate  -group $g_io                    -divider       -height $h_ch2                        { >  frame gap                                                                                                     /}

  add wave -noupdate  -group $g_io   -color $c_pdata  -format logic  -height $h_sig  -radix $r_uns  -label { i_set_gap_type    } $module_path/i_set_gap_type
  add wave -noupdate  -group $g_io   -color $c_pdata  -format logic  -height $h_sig  -radix $r_uns  -label { i_set_gap_const   } $module_path/i_set_gap_const
  add wave -noupdate  -group $g_io   -color $c_pdata  -format logic  -height $h_sig  -radix $r_uns  -label { i_set_gap_min     } $module_path/i_set_gap_min
  add wave -noupdate  -group $g_io   -color $c_pdata  -format logic  -height $h_sig  -radix $r_uns  -label { i_set_gap_max     } $module_path/i_set_gap_max

  add wave -noupdate  -group $g_io                    -divider       -height $h_ch1                        {//  lan packet ----------------------------------------------------------------------------------------------------}

  add wave -noupdate  -group $g_io                    -divider       -height $h_ch2                        { >  clock                                                                                                         /}

  add wave -noupdate  -group $g_io   -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_clk             } $module_path/i_clk
  add wave -noupdate  -group $g_io   -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_cen             } $module_path/i_cen
  add wave -noupdate  -group $g_io   -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_ced             } $module_path/i_ced

  add wave -noupdate  -group $g_io                    -divider       -height $h_ch2                        { >  strob                                                                                                         /}

  add wave -noupdate  -group $g_io   -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { o_str_sop         } $module_path/o_str_frame(1)
  add wave -noupdate  -group $g_io   -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { o_str_frame       } $module_path/o_str_frame(0)
  add wave -noupdate  -group $g_io   -color $c_count  -format logic  -height $h_sig  -radix $r_uns  -label { o_cnt_frame       } $module_path/o_cnt_frame
  add wave -noupdate  -group $g_io   -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { o_str_head        } $module_path/o_str_head(0)
  add wave -noupdate  -group $g_io   -color $c_count  -format logic  -height $h_sig  -radix $r_uns  -label { o_cnt_head        } $module_path/o_cnt_head
  add wave -noupdate  -group $g_io   -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { o_str_prm         } $module_path/o_str_prm(0)
  add wave -noupdate  -group $g_io   -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { o_str_sfd         } $module_path/o_str_sfd(0)
  add wave -noupdate  -group $g_io   -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { o_str_body        } $module_path/o_str_body(0)
  add wave -noupdate  -group $g_io   -color $c_count  -format logic  -height $h_sig  -radix $r_uns  -label { o_cnt_body        } $module_path/o_cnt_body
  add wave -noupdate  -group $g_io   -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { o_str_mac         } $module_path/o_str_mac(0)
  add wave -noupdate  -group $g_io   -color $c_count  -format logic  -height $h_sig  -radix $r_uns  -label { o_cnt_mac         } $module_path/o_cnt_mac
  add wave -noupdate  -group $g_io   -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { o_str_mda         } $module_path/o_str_mda(0)
  add wave -noupdate  -group $g_io   -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { o_str_msa         } $module_path/o_str_msa(0)
  add wave -noupdate  -group $g_io   -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { o_str_mlt         } $module_path/o_str_mlt(0)
  add wave -noupdate  -group $g_io   -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { o_str_pay         } $module_path/o_str_pay(0)
  add wave -noupdate  -group $g_io   -color $c_count  -format logic  -height $h_sig  -radix $r_uns  -label { o_cnt_pay         } $module_path/o_cnt_pay
  add wave -noupdate  -group $g_io   -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { o_str_ip          } $module_path/o_str_ip(0)
  add wave -noupdate  -group $g_io   -color $c_count  -format logic  -height $h_sig  -radix $r_uns  -label { o_cnt_ip          } $module_path/o_cnt_ip
  add wave -noupdate  -group $g_io   -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { o_str_crc         } $module_path/o_str_crc(0)
  add wave -noupdate  -group $g_io   -color $c_count  -format logic  -height $h_sig  -radix $r_uns  -label { o_cnt_crc         } $module_path/o_cnt_crc
  add wave -noupdate  -group $g_io   -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { o_str_gap         } $module_path/o_str_gap(0)
  add wave -noupdate  -group $g_io   -color $c_count  -format logic  -height $h_sig  -radix $r_uns  -label { o_cnt_gap         } $module_path/o_cnt_gap
  add wave -noupdate  -group $g_io   -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { o_str_eop         } $module_path/o_str_frame(2)

  add wave -noupdate  -group $g_io                    -divider       -height $h_ch2                        { >  length                                                                                                        /}

  add wave -noupdate  -group $g_io   -color $c_pdata  -format logic  -height $h_sig  -radix $r_uns  -label { o_len_frame       } $module_path/o_len_frame
  add wave -noupdate  -group $g_io   -color $c_pdata  -format logic  -height $h_sig  -radix $r_uns  -label { o_len_body        } $module_path/o_len_body
  add wave -noupdate  -group $g_io   -color $c_pdata  -format logic  -height $h_sig  -radix $r_uns  -label { o_len_pay         } $module_path/o_len_pay
  add wave -noupdate  -group $g_io   -color $c_pdata  -format logic  -height $h_sig  -radix $r_uns  -label { o_len_gap         } $module_path/o_len_gap

  add wave -noupdate  -group $g_io                    -divider       -height $h_ch2                        { >  fcs crc                                                                                                       /}

  add wave -noupdate  -group $g_io   -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { o_crc_rdy         } $module_path/o_crc_rdy
  add wave -noupdate  -group $g_io   -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { o_crc_err         } $module_path/o_crc_err

  add wave -noupdate  -group $g_io                    -divider       -height $h_ch1                        {//  lan counters --------------------------------------------------------------------------------------------------}

  add wave -noupdate  -group $g_io                    -divider       -height $h_ch2                        { >  setting                                                                                                       /}

  add wave -noupdate  -group $g_io   -color $c_pdata  -format logic  -height $h_sig  -radix $r_uns  -label { i_set_p_top       } $module_path/i_set_p_top
  add wave -noupdate  -group $g_io   -color $c_pdata  -format logic  -height $h_sig  -radix $r_uns  -label { i_set_p_low       } $module_path/i_set_p_low

  add wave -noupdate  -group $g_io                    -divider       -height $h_ch2                        { >  detector                                                                                                      /}

  add wave -noupdate  -group $g_io   -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { o_det_p_rdy       } $module_path/o_det_p_rdy
  add wave -noupdate  -group $g_io   -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { o_det_p_err       } $module_path/o_det_p_err

  add wave -noupdate  -group $g_io                    -divider       -height $h_ch2                        { >  counters                                                                                                      /}

  add wave -noupdate  -group $g_io   -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { i_cnt_p_clr       } $module_path/i_cnt_p_clr
  add wave -noupdate  -group $g_io   -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { o_cnt_p_rdy       } $module_path/o_cnt_p_rdy
  add wave -noupdate  -group $g_io   -color $c_count  -format logic  -height $h_sig  -radix $r_uns  -label { o_cnt_p_all       } $module_path/o_cnt_p_all
  add wave -noupdate  -group $g_io   -color $c_count  -format logic  -height $h_sig  -radix $r_uns  -label { o_cnt_p_err       } $module_path/o_cnt_p_err
  add wave -noupdate  -group $g_io                    -divider       -height $h_nul  <NULL>
  }; # wave_io
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  # add wave : logic
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  if {$wave_log} {
  add wave -noupdate  -group $g_mod                                  -height $h_grp  -group $g_log
  add wave -noupdate  -group $g_log                   -divider       -height $h_nul  <NULL>
  add wave -noupdate  -group $g_log                   -divider       -height $h_ch1                        {//  start of emulator ---------------------------------------------------------------------------------------------}

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  clear                                                                                                         /}

  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_sclr_n          } $module_path/i_sclr_n

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  clock                                                                                                         /}

  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_clk             } $module_path/i_clk
  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_cen             } $module_path/i_cen
  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_ced             } $module_path/i_ced

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  setting                                                                                                       /}

  add wave -noupdate  -group $g_log  -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { i_set_ini         } $module_path/i_set_ini

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  frame (packet)                                                                                                /}

  add wave -noupdate  -group $g_log  -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { str_frame         } $module_path/str_frame(0)
  add wave -noupdate  -group $g_log  -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { str_gap           } $module_path/str_gap(0)

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  start / first                                                                                                 /}

  add wave -noupdate  -group $g_log  -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { state_idle        } $module_path/state_idle
  add wave -noupdate  -group $g_log  -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { state_idle_re     } $module_path/state_idle_re
  add wave -noupdate  -group $g_log  -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { start_first       } $module_path/start_first

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  start                                                                                                         /}

  add wave -noupdate  -group $g_log  -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { start_nxt         } $module_path/start_nxt
  add wave -noupdate  -group $g_log  -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { start             } $module_path/start

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch1                        {//  frame (packet) : length ---------------------------------------------------------------------------------------}

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  clock                                                                                                         /}

  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_clk             } $module_path/i_clk
  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_cen             } $module_path/i_cen
  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_ced             } $module_path/i_ced

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  setting / ini                                                                                                 /}

  add wave -noupdate  -group $g_log  -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { i_set_ini         } $module_path/i_set_ini

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  setting / length                                                                                              /}

  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_uns  -label { i_set_len_type    } $module_path/i_set_len_type
  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_uns  -label { i_set_len_const   } $module_path/i_set_len_const
  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_uns  -label { i_set_len_min     } $module_path/i_set_len_min
  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_uns  -label { i_set_len_max     } $module_path/i_set_len_max

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  start                                                                                                         /}

  add wave -noupdate  -group $g_log  -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { start_nxt         } $module_path/start_nxt
  add wave -noupdate  -group $g_log  -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { start             } $module_path/start

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  length                                                                                                        /}

  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_uns  -label { len_frame_nxt     } $module_path/len_frame_nxt
  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_uns  -label { len_frame         } $module_path/len_frame

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch1                        {//  frame body : length -------------------------------------------------------------------------------------------}

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  clock                                                                                                         /}

  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_clk             } $module_path/i_clk
  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_cen             } $module_path/i_cen
  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_ced             } $module_path/i_ced

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  length / constant                                                                                             /}

  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_uns  -label { len_head          } $module_path/len_head
  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_uns  -label { len_mac           } $module_path/len_mac
  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_uns  -label { len_crc           } $module_path/len_crc

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  length / next to                                                                                              /}

  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_uns  -label { len_body_nxt      } $module_path/len_body_nxt
  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_uns  -label { len_pay_nxt       } $module_path/len_pay_nxt

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  length / sync                                                                                                 /}

  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_uns  -label { len_body          } $module_path/len_body
  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_uns  -label { len_pay           } $module_path/len_pay

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch1                        {//  frame gap : length --------------------------------------------------------------------------------------------}

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  clock                                                                                                         /}

  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_clk             } $module_path/i_clk
  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_cen             } $module_path/i_cen
  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_ced             } $module_path/i_ced

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  setting / ini                                                                                                 /}

  add wave -noupdate  -group $g_log  -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { i_set_ini         } $module_path/i_set_ini

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  setting / speed                                                                                               /}

  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_uns  -label { i_set_tx_mode     } $module_path/i_set_tx_mode
  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_uns  -label { i_set_tx_gap      } $module_path/i_set_tx_gap

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  setting / gap                                                                                                 /}

  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_uns  -label { i_set_gap_type    } $module_path/i_set_gap_type
  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_uns  -label { i_set_gap_const   } $module_path/i_set_gap_const
  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_uns  -label { i_set_gap_min     } $module_path/i_set_gap_min
  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_uns  -label { i_set_gap_max     } $module_path/i_set_gap_max

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  start                                                                                                         /}

  add wave -noupdate  -group $g_log  -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { start_nxt         } $module_path/start_nxt
  add wave -noupdate  -group $g_log  -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { start             } $module_path/start

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  length                                                                                                        /}

  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_uns  -label { len_gap           } $module_path/len_gap
  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_uns  -label { len_gap_nxt       } $module_path/len_gap_nxt

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch1                        {//  frame (packet) : multi str ------------------------------------------------------------------------------------}

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  clock                                                                                                         /}

  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_clk             } $module_path/mstr/i_clk
  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_clk_en          } $module_path/mstr/i_clk_en
  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_sclr            } $module_path/mstr/i_sclr

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  control                                                                                                       /}

  add wave -noupdate  -group $g_log  -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { i_start           } $module_path/mstr/i_start
  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_uns  -label { i_length          } $module_path/mstr/i_length
  add wave -noupdate  -group $g_log  -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { i_reset           } $module_path/mstr/i_reset

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  generator                                                                                                     /}

  add wave -noupdate  -group $g_log  -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { o_start           } $module_path/mstr/o_start
  add wave -noupdate  -group $g_log  -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { o_stop            } $module_path/mstr/o_stop
  add wave -noupdate  -group $g_log  -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { o_strob           } $module_path/mstr/o_strob
  add wave -noupdate  -group $g_log  -color $c_count  -format logic  -height $h_sig  -radix $r_uns  -label { o_count_up        } $module_path/mstr/o_count_up
  add wave -noupdate  -group $g_log  -color $c_count  -format logic  -height $h_sig  -radix $r_uns  -label { o_count_dn        } $module_path/mstr/o_count_dn

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch1                        {//  frame header : strob ------------------------------------------------------------------------------------------}

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  parameters                                                                                                    /}

  add wave -noupdate  -group $g_log  -color $c_param  -format logic  -height $h_sig  -radix $r_uns  -label { pE_LAN_HEAD       } $module_path/pE_LAN_HEAD

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  clock                                                                                                         /}

  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_clk             } $module_path/i_clk
  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_cen             } $module_path/i_cen
  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_ced             } $module_path/i_ced

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  preamble / strob                                                                                              /}

  add wave -noupdate  -group $g_log  -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { str_prm [1]       } $module_path/str_prm(1)
  add wave -noupdate  -group $g_log  -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { str_prm [0]       } $module_path/str_prm(0)
  add wave -noupdate  -group $g_log  -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { str_prm [2]       } $module_path/str_prm(2)

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  sfd / strob                                                                                                   /}

  add wave -noupdate  -group $g_log  -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { str_sfd [1]       } $module_path/str_sfd(1)
  add wave -noupdate  -group $g_log  -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { str_sfd [0]       } $module_path/str_sfd(0)
  add wave -noupdate  -group $g_log  -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { str_sfd [2]       } $module_path/str_sfd(2)

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  header / strob                                                                                                /}

  add wave -noupdate  -group $g_log  -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { str_head [1]      } $module_path/str_head(1)
  add wave -noupdate  -group $g_log  -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { str_head [0]      } $module_path/str_head(0)
  add wave -noupdate  -group $g_log  -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { str_head [2]      } $module_path/str_head(2)

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  header / counter                                                                                              /}

  add wave -noupdate  -group $g_log  -color $c_count  -format logic  -height $h_sig  -radix $r_uns  -label { cnt_head_nxt      } $module_path/cnt_head_nxt
  add wave -noupdate  -group $g_log  -color $c_count  -format logic  -height $h_sig  -radix $r_uns  -label { cnt_head          } $module_path/cnt_head

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  header / nulling                                                                                              /}

  add wave -noupdate  -group $g_log  -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { str_head_x        } $module_path/str_head_x(0)
  add wave -noupdate  -group $g_log  -color $c_count  -format logic  -height $h_sig  -radix $r_uns  -label { cnt_head_x        } $module_path/cnt_head_x
  add wave -noupdate  -group $g_log  -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { str_prm_x         } $module_path/str_prm_x(0)
  add wave -noupdate  -group $g_log  -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { str_sfd_x         } $module_path/str_sfd_x(0)

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch1                        {//  mac header : strob --------------------------------------------------------------------------------------------}

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  clock                                                                                                         /}

  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_clk             } $module_path/i_clk
  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_cen             } $module_path/i_cen
  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_ced             } $module_path/i_ced

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  setting                                                                                                       /}

  add wave -noupdate  -group $g_log  -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { i_set_mac_ena     } $module_path/i_set_mac_ena

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  mda / strob                                                                                                   /}

  add wave -noupdate  -group $g_log  -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { str_mda [1]       } $module_path/str_mda(1)
  add wave -noupdate  -group $g_log  -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { str_mda [0]       } $module_path/str_mda(0)
  add wave -noupdate  -group $g_log  -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { str_mda [2]       } $module_path/str_mda(2)

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  msa / strob                                                                                                   /}

  add wave -noupdate  -group $g_log  -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { str_msa [1]       } $module_path/str_msa(1)
  add wave -noupdate  -group $g_log  -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { str_msa [0]       } $module_path/str_msa(0)
  add wave -noupdate  -group $g_log  -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { str_msa [2]       } $module_path/str_msa(2)

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  mlt / strob                                                                                                   /}

  add wave -noupdate  -group $g_log  -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { str_mlt [1]       } $module_path/str_mlt(1)
  add wave -noupdate  -group $g_log  -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { str_mlt [0]       } $module_path/str_mlt(0)
  add wave -noupdate  -group $g_log  -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { str_mlt [2]       } $module_path/str_mlt(2)

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  mac / strob                                                                                                   /}

  add wave -noupdate  -group $g_log  -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { str_mac [1]       } $module_path/str_mac(1)
  add wave -noupdate  -group $g_log  -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { str_mac [0]       } $module_path/str_mac(0)
  add wave -noupdate  -group $g_log  -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { str_mac [2]       } $module_path/str_mac(2)

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  mac / counter                                                                                                 /}

  add wave -noupdate  -group $g_log  -color $c_count  -format logic  -height $h_sig  -radix $r_uns  -label { cnt_mac_nxt       } $module_path/cnt_mac_nxt
  add wave -noupdate  -group $g_log  -color $c_count  -format logic  -height $h_sig  -radix $r_uns  -label { cnt_mac           } $module_path/cnt_mac

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch1                        {//  payload : strob -----------------------------------------------------------------------------------------------}

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  clock                                                                                                         /}

  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_clk             } $module_path/i_clk
  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_cen             } $module_path/i_cen
  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_ced             } $module_path/i_ced

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  payload / strob                                                                                               /}

  add wave -noupdate  -group $g_log  -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { str_pay [1]       } $module_path/str_pay(1)
  add wave -noupdate  -group $g_log  -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { str_pay [0]       } $module_path/str_pay(0)
  add wave -noupdate  -group $g_log  -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { str_pay [2]       } $module_path/str_pay(2)

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  payload / counter                                                                                             /}

  add wave -noupdate  -group $g_log  -color $c_count  -format logic  -height $h_sig  -radix $r_uns  -label { cnt_pay_nxt       } $module_path/cnt_pay_nxt
  add wave -noupdate  -group $g_log  -color $c_count  -format logic  -height $h_sig  -radix $r_uns  -label { cnt_pay           } $module_path/cnt_pay

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch1                        {//  ip header : strob ---------------------------------------------------------------------------------------------}

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  parameters                                                                                                    /}

  add wave -noupdate  -group $g_log  -color $c_param  -format logic  -height $h_sig  -radix $r_uns  -label { pN_LAN_IPL        } $module_path/pN_LAN_IPL
  add wave -noupdate  -group $g_log  -color $c_param  -format logic  -height $h_sig  -radix $r_uns  -label { pN_LAN_IPH        } $module_path/pN_LAN_IPH

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  clock                                                                                                         /}

  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_clk             } $module_path/i_clk
  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_cen             } $module_path/i_cen
  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_ced             } $module_path/i_ced

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  setting                                                                                                       /}

  add wave -noupdate  -group $g_log  -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { i_set_ip_ena      } $module_path/i_set_ip_ena

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  payload                                                                                                       /}

  add wave -noupdate  -group $g_log  -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { str_pay [1]       } $module_path/str_pay(1)
  add wave -noupdate  -group $g_log  -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { str_pay [0]       } $module_path/str_pay(0)
  add wave -noupdate  -group $g_log  -color $c_count  -format logic  -height $h_sig  -radix $r_uns  -label { cnt_pay           } $module_path/cnt_pay

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  ip / strob                                                                                                    /}

  add wave -noupdate  -group $g_log  -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { str_ip [1]        } $module_path/str_ip(1)
  add wave -noupdate  -group $g_log  -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { str_ip [0]        } $module_path/str_ip(0)
  add wave -noupdate  -group $g_log  -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { str_ip [2]        } $module_path/str_ip(2)

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  ip / counter                                                                                                  /}

  add wave -noupdate  -group $g_log  -color $c_count  -format logic  -height $h_sig  -radix $r_uns  -label { cnt_ip_nxt        } $module_path/cnt_ip_nxt
  add wave -noupdate  -group $g_log  -color $c_count  -format logic  -height $h_sig  -radix $r_uns  -label { cnt_ip            } $module_path/cnt_ip

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch1                        {//  fsc crc : strob -----------------------------------------------------------------------------------------------}

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  clock                                                                                                         /}

  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_clk             } $module_path/i_clk
  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_cen             } $module_path/i_cen
  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_ced             } $module_path/i_ced

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  fcs / strob                                                                                                   /}

  add wave -noupdate  -group $g_log  -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { str_crc [1]       } $module_path/str_crc(1)
  add wave -noupdate  -group $g_log  -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { str_crc [0]       } $module_path/str_crc(0)
  add wave -noupdate  -group $g_log  -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { str_crc [2]       } $module_path/str_crc(2)

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  fcs / counter                                                                                                 /}

  add wave -noupdate  -group $g_log  -color $c_count  -format logic  -height $h_sig  -radix $r_uns  -label { cnt_crc_nxt       } $module_path/cnt_crc_nxt
  add wave -noupdate  -group $g_log  -color $c_count  -format logic  -height $h_sig  -radix $r_uns  -label { cnt_crc           } $module_path/cnt_crc

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch1                        {//  frame body : strob --------------------------------------------------------------------------------------------}

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  clock                                                                                                         /}

  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_clk             } $module_path/i_clk
  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_cen             } $module_path/i_cen
  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_ced             } $module_path/i_ced

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  body / strob                                                                                                  /}

  add wave -noupdate  -group $g_log  -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { str_body [1]      } $module_path/str_body(1)
  add wave -noupdate  -group $g_log  -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { str_body [0]      } $module_path/str_body(0)
  add wave -noupdate  -group $g_log  -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { str_body [2]      } $module_path/str_body(2)

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  body / counter                                                                                                /}

  add wave -noupdate  -group $g_log  -color $c_count  -format logic  -height $h_sig  -radix $r_uns  -label { cnt_body_nxt      } $module_path/cnt_body_nxt
  add wave -noupdate  -group $g_log  -color $c_count  -format logic  -height $h_sig  -radix $r_uns  -label { cnt_body          } $module_path/cnt_body

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch1                        {//  frame (packet) : strob ----------------------------------------------------------------------------------------}

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  clock                                                                                                         /}

  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_clk             } $module_path/i_clk
  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_cen             } $module_path/i_cen
  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_ced             } $module_path/i_ced

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  frame / strob                                                                                                 /}

  add wave -noupdate  -group $g_log  -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { str_frame [1]     } $module_path/str_frame(1)
  add wave -noupdate  -group $g_log  -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { str_frame [0]     } $module_path/str_frame(0)
  add wave -noupdate  -group $g_log  -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { str_frame [2]     } $module_path/str_frame(2)

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  frame / counter                                                                                               /}

  add wave -noupdate  -group $g_log  -color $c_count  -format logic  -height $h_sig  -radix $r_uns  -label { cnt_frame_nxt     } $module_path/cnt_frame_nxt
  add wave -noupdate  -group $g_log  -color $c_count  -format logic  -height $h_sig  -radix $r_uns  -label { cnt_frame         } $module_path/cnt_frame

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch1                        {//  frame gap : strob ---------------------------------------------------------------------------------------------}

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  clock                                                                                                         /}

  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_clk             } $module_path/i_clk
  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_cen             } $module_path/i_cen
  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_ced             } $module_path/i_ced

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  start                                                                                                         /}

  add wave -noupdate  -group $g_log  -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { start_first       } $module_path/start_first

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  gap / strob                                                                                                   /}

  add wave -noupdate  -group $g_log  -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { str_gap [1]       } $module_path/str_gap(1)
  add wave -noupdate  -group $g_log  -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { str_gap [0]       } $module_path/str_gap(0)
  add wave -noupdate  -group $g_log  -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { str_gap [2]       } $module_path/str_gap(2)

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  gap / stop count                                                                                              /}

  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_uns  -label { len_gap_nxt       } $module_path/len_gap_nxt
  add wave -noupdate  -group $g_log  -color $c_count  -format logic  -height $h_sig  -radix $r_uns  -label { cnt_gap_stop      } $module_path/cnt_gap_stop

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  gap / counter                                                                                                 /}

  add wave -noupdate  -group $g_log  -color $c_count  -format logic  -height $h_sig  -radix $r_uns  -label { cnt_gap_nxt       } $module_path/cnt_gap_nxt
  add wave -noupdate  -group $g_log  -color $c_count  -format logic  -height $h_sig  -radix $r_uns  -label { cnt_gap           } $module_path/cnt_gap

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch1                        {//  fcs crc : error emulator --------------------------------------------------------------------------------------}

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  clock                                                                                                         /}

  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_clk             } $module_path/i_clk
  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_cen             } $module_path/i_cen
  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_ced             } $module_path/i_ced

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  setting                                                                                                       /}

  add wave -noupdate  -group $g_log  -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { i_set_err_ena     } $module_path/i_set_err_ena
  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_uns  -label { i_set_err_per     } $module_path/i_set_err_per

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  start                                                                                                         /}

  add wave -noupdate  -group $g_log  -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { start_nxt         } $module_path/start_nxt
  add wave -noupdate  -group $g_log  -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { start             } $module_path/start

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  error                                                                                                         /}

  add wave -noupdate  -group $g_log  -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { crc_err_nxt       } $module_path/crc_err_nxt
  add wave -noupdate  -group $g_log  -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { crc_err           } $module_path/crc_err
  add wave -noupdate  -group $g_log  -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { crc_rdy           } $module_path/crc_rdy

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch1                        {//  packet counters -----------------------------------------------------------------------------------------------}

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  clock                                                                                                         /}

  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_clk             } $module_path/i_clk
  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_cen             } $module_path/i_cen
  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_ced             } $module_path/i_ced

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  setting                                                                                                       /}

  add wave -noupdate  -group $g_log  -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { i_cnt_p_clr       } $module_path/i_cnt_p_clr
  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_uns  -label { i_set_p_low       } $module_path/i_set_p_low
  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_uns  -label { i_set_p_top       } $module_path/i_set_p_top

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  start                                                                                                         /}

  add wave -noupdate  -group $g_log  -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { start             } $module_path/start

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  detector                                                                                                      /}

  add wave -noupdate  -group $g_log  -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { det_p_rdy         } $module_path/det_p_rdy
  add wave -noupdate  -group $g_log  -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { det_p_err         } $module_path/det_p_err

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  frame body                                                                                                    /}

  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_uns  -label { len_body          } $module_path/len_body
  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_uns  -label { len_body_nxt      } $module_path/len_body_nxt
  add wave -noupdate  -group $g_log  -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { str_body          } $module_path/str_body

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  counters                                                                                                      /}

  add wave -noupdate  -group $g_log  -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { cnt_p_clr         } $module_path/cnt_p_clr
  add wave -noupdate  -group $g_log  -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { cnt_p_rdy         } $module_path/cnt_p_rdy
  add wave -noupdate  -group $g_log  -color $c_count  -format logic  -height $h_sig  -radix $r_uns  -label { cnt_p_all         } $module_path/cnt_p_all
  add wave -noupdate  -group $g_log  -color $c_count  -format logic  -height $h_sig  -radix $r_uns  -label { cnt_p_err         } $module_path/cnt_p_err
  add wave -noupdate  -group $g_log                   -divider       -height $h_nul  <NULL>
  }; # wave_log
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  # add wave : all wave
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  if {$wave_all} {
  add wave -noupdate  -group $g_mod                                  -height $h_grp  -group $g_all
  add wave -noupdate  -group $g_all                   -divider       -height $h_nul  <NULL>
  add wave -noupdate  -group $g_all                                                                                              $module_path/*
  add wave -noupdate  -group $g_all                   -divider       -height $h_nul  <NULL>
  }; # wave_all
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

  add wave -noupdate  -group $g_mod                   -divider       -height $h_nul  <NULL>

  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#




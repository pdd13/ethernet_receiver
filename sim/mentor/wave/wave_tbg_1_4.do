  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  #
  #  Company      : Micran, Department of Telecommunications
  #
  #  Engineer     : Pogodaev Danil
  #
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  #
  #  Library      : lib_ethernet / library of ethernet components
  #
  #  Component    : eth_rx_v2 / receiver of ethernet frames (packets)
  #
  #  File Name    : wave_tbg_1_4 .do (tcl sim mentor)
  #
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  #
  #  Description  : List of module waveforms for Mentor simulator.
  #
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  #
  #  Note (1)     : Command 'add wave'              The add wave command adds the following objects to the Wave window:
  #
  #                                                  � VHDL signals and variables
  #                                                  � Verilog nets and registers
  #                                                  � System Verilog class objects
  #                                                  � SystemC primitive channels (signals)
  #                                                  � Dividers and user-defined buses
  #
  #                  -noupdate                      A switch that prevents the Wave window from updating when a series of add wave commands are executed in series.
  #
  #                                                 If no port mode is specified, add wave will display all objects in the selected region with names matching the object
  #                                                 name specification.
  #
  #                  -expand <signal_name>          A switch and argument pair that instructs the command to expand a compound signal immediately, but only one level down.
  #                                                 <signal_name> � a string that specifies the name of the signal. This string can include wildcards.
  #
  #                  -group <group_name>            A switch and argument group that creates a wave group with the specified group_name.
  #                  [<sig_name1> ...]   
  #                                                 <group_name> � a string that specifies the name of the group. You must enclose this argument in quotes (") or braces ({ })
  #                                                 if it contains any white space.
  #
  #                                                 <sig_name> ... � a string, which is repeatable in a space separated list, that specifies the signals to add to the group.
  #                                                 This command creates an empty group if you do not specify any signal names.
  #
  #                  -height <pixels>               A switch and argument pair that specifies the height, in pixels, of the waveform.
  #
  #                  -color <standard_color_name>   A switch and argument pair that specifies the color used to display a waveform.
  #
  #                                                 <standard_color_name> � You can use either of the following:
  #
  #                                                  � standard X Window color name � enclose 2-word names in quotes ("), for example: -color "light blue"
  #                                                  � rgb value, for example: -color #357f77
  #  
  #                                                 Definitions of color names (e.g. steelblue or gainsboro) have been provided by the World Wide Web Consortium and
  #                                                 are available at www.w3.org/TR/SVG/types.html#ColorKeywords. Color transparent represents the absence of a color.
  #
  #                  -<format>                      A choice between switches that specify the display format of the objects. Valid entries are:
  #
  #                                                  -format literal               Literal waveforms are displayed as a box containing the object value.
  #                                                  -format logic                 Logic signals may be U, X, 0, 1, Z, W, L, H, or �-�.
  #                                                  -format analog-step           Analog-step changes to the new time before plotting the new Y.
  #                                                  -format analog-interpolated   Analog-interpolated draws a diagonal line.
  #                                                  -format analog-backstep       Analog-backstep plots the new Y before moving to the new time.
  #                                                  -format event                 Displays a mark at every transition.
  #
  #                  -radix <type>                  A choice between switches that specify the radix for the objects that follow in the command.
  #                                                 Valid entries (or any unique abbreviations) are:
  #                  -<radix_type>   
  #                                                  -radix binary    -binary
  #                                                  -radix ascii     -ascii
  #                                                  -radix unsigned  -unsigned
  #                                                  -radix decimal   -decimal
  #                                                  -radix octal     -octal
  #                                                  -radix hex       -hex
  #                                                  -radix symbolic  -symbolic
  #                                                  -radix time      -time
  #                                                  -radix default   -default
  #
  #                                                 If no radix is specified for an enumerated type, the default representation is used.
  #
  #                  -divider [<divider_name>]      A switch and argument pair that adds a divider to the Wave window.
  #
  #                                                 <divider_name> � A string, which is repeatable in a space separated list, that specifies the name of the divider,
  #                                                 which appears in the pathnames column.
  #
  #                                                 You cannot begin a name with a hyphen (-).
  #                                                 You can begin a name with a space, but you must enclose the name within quotes (") or braces ({ })
  #                                                 If you do not specify this argument, the command inserts an unnamed divider.
  #
  #                  -label <name>                  A switch and argument pair that specifies an alternative name for the signal being added.
  #                                                 This alternative name is not valid in a force or examine command; however, it can be used in a search command
  #                                                 with the wave option.
  #
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  #
  #  Revision List
  #
  #  Version   Date       Author
  #
  #  2.0       26.09.16   Pogodaev Danil
  #
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  # setting : module
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

  set module_index "tbg 1.4"
  set module_name  "emu_dat"
  set module_path  "/tb_top/tb_gen/eth_emu/emu_dat"

  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  # setting : wave groups (on/off)
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

  set wave_par 1 ; # group : parameters (1/0)
  set wave_io  1 ; # group : interface  (1/0)
  set wave_log 1 ; # group : logic      (1/0)
  set wave_all 0 ; # group : all wave   (1/0)

  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  # setting : wave groups (display)
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

  set g_mod " $module_index :  $module_name " ; # group : module
  set g_par " $module_index :  parameters   " ; # group : parameter
  set g_io  " $module_index :  interface    " ; # group : interface
  set g_log " $module_index :  logic        " ; # group : logic
  set g_all " $module_index :  all wave     " ; # group : all wave

  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  # add wave : module name
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

  add wave -noupdate                                                 -height $h_mod  -expand  -group $g_mod
  add wave -noupdate  -group $g_mod                   -divider       -height $h_nul  <NULL>
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  # add wave : parameters
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  if {$wave_par} {
  add wave -noupdate  -group $g_mod                                  -height $h_grp  -group $g_par
  add wave -noupdate  -group $g_par                   -divider       -height $h_nul  <NULL>
  add wave -noupdate  -group $g_par                   -divider       -height $h_ch1                        {//  packet structure ----------------------------------------------------------------------------------------------}

  add wave -noupdate  -group $g_par                   -divider       -height $h_ch2                        { >  frame length                                                                                                  /}

  add wave -noupdate  -group $g_par  -color $c_param  -format logic  -height $h_sig  -radix $r_uns  -label { pW_LAN_LEN        } $module_path/pW_LAN_LEN
  add wave -noupdate  -group $g_par  -color $c_param  -format logic  -height $h_sig  -radix $r_uns  -label { pW_LAN_GAP        } $module_path/pW_LAN_GAP

  add wave -noupdate  -group $g_par                   -divider       -height $h_ch2                        { >  ip header                                                                                                     /}

  add wave -noupdate  -group $g_par  -color $c_param  -format logic  -height $h_sig  -radix $r_uns  -label { pN_LAN_IPL        } $module_path/pN_LAN_IPL
  add wave -noupdate  -group $g_par  -color $c_param  -format logic  -height $h_sig  -radix $r_uns  -label { pN_LAN_IPH        } $module_path/pN_LAN_IPH

  add wave -noupdate  -group $g_par                   -divider       -height $h_ch2                        { >  payload                                                                                                       /}

  add wave -noupdate  -group $g_par  -color $c_param  -format logic  -height $h_sig  -radix $r_uns  -label { pW_LAN_PAY        } $module_path/pW_LAN_PAY

  add wave -noupdate  -group $g_par                   -divider       -height $h_ch1                        {//  local (for i/o) -----------------------------------------------------------------------------------------------}

  add wave -noupdate  -group $g_par                   -divider       -height $h_ch2                        { >  packet structure                                                                                              /}

  add wave -noupdate  -group $g_par  -color $c_param  -format logic  -height $h_sig  -radix $r_uns  -label { lpN_LAN_IPL       } $module_path/lpN_LAN_IPL
  add wave -noupdate  -group $g_par  -color $c_param  -format logic  -height $h_sig  -radix $r_uns  -label { lpN_LAN_IPH       } $module_path/lpN_LAN_IPH
  add wave -noupdate  -group $g_par  -color $c_param  -format logic  -height $h_sig  -radix $r_uns  -label { lpN_LAN_IP        } $module_path/lpN_LAN_IP
  add wave -noupdate  -group $g_par  -color $c_param  -format logic  -height $h_sig  -radix $r_uns  -label { lpW_LAN_IP        } $module_path/lpW_LAN_IP
  add wave -noupdate  -group $g_par  -color $c_param  -format logic  -height $h_sig  -radix $r_uns  -label { lpW_LAN_IP_C      } $module_path/lpW_LAN_IP_C
  add wave -noupdate  -group $g_par                   -divider       -height $h_nul  <NULL>
  }; # wave_par
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  # add wave : interface
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  if {$wave_io} {
  add wave -noupdate  -group $g_mod                                  -height $h_grp  -group $g_io
  add wave -noupdate  -group $g_io                    -divider       -height $h_nul  <NULL>
  add wave -noupdate  -group $g_io                    -divider       -height $h_ch1                        {//  clear ---------------------------------------------------------------------------------------------------------}

  add wave -noupdate  -group $g_io   -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_sclr_n          } $module_path/i_sclr_n

  add wave -noupdate  -group $g_io                    -divider       -height $h_ch1                        {//  setting -------------------------------------------------------------------------------------------------------}

  add wave -noupdate  -group $g_io                    -divider       -height $h_ch2                        { >  initialization                                                                                                /}

  add wave -noupdate  -group $g_io   -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { i_set_ini         } $module_path/i_set_ini

  add wave -noupdate  -group $g_io                    -divider       -height $h_ch2                        { >  mac header                                                                                                    /}

  add wave -noupdate  -group $g_io   -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { i_set_mac_ena     } $module_path/i_set_mac_ena

  add wave -noupdate  -group $g_io                    -divider       -height $h_ch2                        { >  mac header / mda                                                                                              /}

  add wave -noupdate  -group $g_io   -color $c_pdata  -format logic  -height $h_sig  -radix $r_uns  -label { i_set_mda_type    } $module_path/i_set_mda_type
  add wave -noupdate  -group $g_io   -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { i_set_mda_const   } $module_path/i_set_mda_const
  add wave -noupdate  -group $g_io   -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { i_set_mda_min     } $module_path/i_set_mda_min
  add wave -noupdate  -group $g_io   -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { i_set_mda_max     } $module_path/i_set_mda_max

  add wave -noupdate  -group $g_io                    -divider       -height $h_ch2                        { >  mac header / msa                                                                                              /}

  add wave -noupdate  -group $g_io   -color $c_pdata  -format logic  -height $h_sig  -radix $r_uns  -label { i_set_msa_type    } $module_path/i_set_msa_type
  add wave -noupdate  -group $g_io   -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { i_set_msa_const   } $module_path/i_set_msa_const
  add wave -noupdate  -group $g_io   -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { i_set_msa_min     } $module_path/i_set_msa_min
  add wave -noupdate  -group $g_io   -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { i_set_msa_max     } $module_path/i_set_msa_max

  add wave -noupdate  -group $g_io                    -divider       -height $h_ch2                        { >  mac header / mlt                                                                                              /}

  add wave -noupdate  -group $g_io   -color $c_pdata  -format logic  -height $h_sig  -radix $r_uns  -label { i_set_mlt_type    } $module_path/i_set_mlt_type
  add wave -noupdate  -group $g_io   -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { i_set_mlt_const   } $module_path/i_set_mlt_const
  add wave -noupdate  -group $g_io   -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { i_set_mlt_min     } $module_path/i_set_mlt_min
  add wave -noupdate  -group $g_io   -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { i_set_mlt_max     } $module_path/i_set_mlt_max

  add wave -noupdate  -group $g_io                    -divider       -height $h_ch2                        { >  ip header                                                                                                     /}

  add wave -noupdate  -group $g_io   -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { i_set_ip_ena      } $module_path/i_set_ip_ena
  add wave -noupdate  -group $g_io   -color $c_pdata  -format logic  -height $h_sig  -radix $r_uns  -label { i_set_ip_type     } $module_path/i_set_ip_type
  add wave -noupdate  -group $g_io   -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { i_set_ip_const    } $module_path/i_set_ip_const
  add wave -noupdate  -group $g_io   -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { i_set_ip_min      } $module_path/i_set_ip_min
  add wave -noupdate  -group $g_io   -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { i_set_ip_max      } $module_path/i_set_ip_max

  add wave -noupdate  -group $g_io                    -divider       -height $h_ch2                        { >  payload                                                                                                       /}

  add wave -noupdate  -group $g_io   -color $c_pdata  -format logic  -height $h_sig  -radix $r_uns  -label { i_set_pay_type    } $module_path/i_set_pay_type
  add wave -noupdate  -group $g_io   -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { i_set_pay_const   } $module_path/i_set_pay_const
  add wave -noupdate  -group $g_io   -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { i_set_pay_min     } $module_path/i_set_pay_min
  add wave -noupdate  -group $g_io   -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { i_set_pay_max     } $module_path/i_set_pay_max

  add wave -noupdate  -group $g_io                    -divider       -height $h_ch1                        {//  lan packet ----------------------------------------------------------------------------------------------------}

  add wave -noupdate  -group $g_io                    -divider       -height $h_ch2                        { >  clock                                                                                                         /}

  add wave -noupdate  -group $g_io   -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_clk             } $module_path/i_clk
  add wave -noupdate  -group $g_io   -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_cen             } $module_path/i_cen
  add wave -noupdate  -group $g_io   -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_ced             } $module_path/i_ced

  add wave -noupdate  -group $g_io                    -divider       -height $h_ch2                        { >  strob                                                                                                         /}

  add wave -noupdate  -group $g_io   -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { i_str_frame       } $module_path/i_str_frame(0)
  add wave -noupdate  -group $g_io   -color $c_count  -format logic  -height $h_sig  -radix $r_uns  -label { i_cnt_frame       } $module_path/i_cnt_frame
  add wave -noupdate  -group $g_io   -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { i_str_head        } $module_path/i_str_head(0)
  add wave -noupdate  -group $g_io   -color $c_count  -format logic  -height $h_sig  -radix $r_uns  -label { i_cnt_head        } $module_path/i_cnt_head
  add wave -noupdate  -group $g_io   -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { i_str_prm         } $module_path/i_str_prm(0)
  add wave -noupdate  -group $g_io   -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { i_str_sfd         } $module_path/i_str_sfd(0)
  add wave -noupdate  -group $g_io   -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { i_str_body        } $module_path/i_str_body(0)
  add wave -noupdate  -group $g_io   -color $c_count  -format logic  -height $h_sig  -radix $r_uns  -label { i_cnt_body        } $module_path/i_cnt_body
  add wave -noupdate  -group $g_io   -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { i_str_mac         } $module_path/i_str_mac(0)
  add wave -noupdate  -group $g_io   -color $c_count  -format logic  -height $h_sig  -radix $r_uns  -label { i_cnt_mac         } $module_path/i_cnt_mac
  add wave -noupdate  -group $g_io   -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { i_str_mda         } $module_path/i_str_mda(0)
  add wave -noupdate  -group $g_io   -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { i_str_msa         } $module_path/i_str_msa(0)
  add wave -noupdate  -group $g_io   -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { i_str_mlt         } $module_path/i_str_mlt(0)
  add wave -noupdate  -group $g_io   -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { i_str_pay         } $module_path/i_str_pay(0)
  add wave -noupdate  -group $g_io   -color $c_count  -format logic  -height $h_sig  -radix $r_uns  -label { i_cnt_pay         } $module_path/i_cnt_pay
  add wave -noupdate  -group $g_io   -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { i_str_ip          } $module_path/i_str_ip(0)
  add wave -noupdate  -group $g_io   -color $c_count  -format logic  -height $h_sig  -radix $r_uns  -label { i_cnt_ip          } $module_path/i_cnt_ip
  add wave -noupdate  -group $g_io   -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { i_str_crc         } $module_path/i_str_crc(0)
  add wave -noupdate  -group $g_io   -color $c_count  -format logic  -height $h_sig  -radix $r_uns  -label { i_cnt_crc         } $module_path/i_cnt_crc
  add wave -noupdate  -group $g_io   -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { i_str_gap         } $module_path/i_str_gap(0)
  add wave -noupdate  -group $g_io   -color $c_count  -format logic  -height $h_sig  -radix $r_uns  -label { i_cnt_gap         } $module_path/i_cnt_gap

  add wave -noupdate  -group $g_io                    -divider       -height $h_ch2                        { >  data                                                                                                          /}

  add wave -noupdate  -group $g_io   -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { o_dat_load        } $module_path/o_dat_load
  add wave -noupdate  -group $g_io   -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { o_dat_frame       } $module_path/o_dat_frame

  add wave -noupdate  -group $g_io                    -divider       -height $h_ch2                        { >  length                                                                                                        /}

  add wave -noupdate  -group $g_io   -color $c_pdata  -format logic  -height $h_sig  -radix $r_uns  -label { i_len_frame       } $module_path/i_len_frame
  add wave -noupdate  -group $g_io   -color $c_pdata  -format logic  -height $h_sig  -radix $r_uns  -label { i_len_body        } $module_path/i_len_body
  add wave -noupdate  -group $g_io   -color $c_pdata  -format logic  -height $h_sig  -radix $r_uns  -label { i_len_pay         } $module_path/i_len_pay
  add wave -noupdate  -group $g_io   -color $c_pdata  -format logic  -height $h_sig  -radix $r_uns  -label { i_len_gap         } $module_path/i_len_gap

  add wave -noupdate  -group $g_io                    -divider       -height $h_ch2                        { >  fcs crc                                                                                                       /}

  add wave -noupdate  -group $g_io   -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { i_crc_rdy         } $module_path/i_crc_rdy
  add wave -noupdate  -group $g_io   -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { i_crc_err         } $module_path/i_crc_err
  add wave -noupdate  -group $g_io                    -divider       -height $h_nul  <NULL>
  }; # wave_io
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  # add wave : logic
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  if {$wave_log} {
  add wave -noupdate  -group $g_mod                                  -height $h_grp  -group $g_log
  add wave -noupdate  -group $g_log                   -divider       -height $h_nul  <NULL>
  add wave -noupdate  -group $g_log                   -divider       -height $h_ch1                        {//  frmae header : data -------------------------------------------------------------------------------------------}

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  clock                                                                                                         /}

  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_clk             } $module_path/i_clk
  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_cen             } $module_path/i_cen
  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_ced             } $module_path/i_ced

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  header / strob                                                                                                /}

  add wave -noupdate  -group $g_log  -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { i_str_head [0]    } $module_path/i_str_head(0)
  add wave -noupdate  -group $g_log  -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { i_str_prm  [0]    } $module_path/i_str_prm(0)
  add wave -noupdate  -group $g_log  -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { i_str_sfd  [0]    } $module_path/i_str_sfd(0)

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  header / data                                                                                                 /}

  add wave -noupdate  -group $g_log  -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { str_head          } $module_path/i_str_head
  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { dat_head          } $module_path/dat_head

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch1                        {//  mac header : data ---------------------------------------------------------------------------------------------}

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  clock                                                                                                         /}

  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_clk             } $module_path/i_clk
  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_cen             } $module_path/i_cen
  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_ced             } $module_path/i_ced

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  setting / mac                                                                                                 /}

  add wave -noupdate  -group $g_log  -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { i_set_mac_ena     } $module_path/i_set_mac_ena

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  setting / mda                                                                                                 /}

  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_uns  -label { i_set_mda_type    } $module_path/i_set_mda_type
  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { i_set_mda_const   } $module_path/i_set_mda_const
  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { i_set_mda_min     } $module_path/i_set_mda_min
  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { i_set_mda_max     } $module_path/i_set_mda_max

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  setting / msa                                                                                                 /}

  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_uns  -label { i_set_msa_type    } $module_path/i_set_msa_type
  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { i_set_msa_const   } $module_path/i_set_msa_const
  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { i_set_msa_min     } $module_path/i_set_msa_min
  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { i_set_msa_max     } $module_path/i_set_msa_max

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  setting / mlt                                                                                                 /}

  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_uns  -label { i_set_mlt_type    } $module_path/i_set_mlt_type
  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { i_set_mlt_const   } $module_path/i_set_mlt_const
  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { i_set_mlt_min     } $module_path/i_set_mlt_min
  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { i_set_mlt_max     } $module_path/i_set_mlt_max

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  mac / data                                                                                                    /}

  add wave -noupdate  -group $g_log  -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { str_mac [1]       } $module_path/i_str_mac(1)
  add wave -noupdate  -group $g_log  -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { str_mac [0]       } $module_path/i_str_mac(0)
  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { dat_mac           } $module_path/dat_mac
  add wave -noupdate  -group $g_log  -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { str_mda [0]       } $module_path/i_str_mda(0)
  add wave -noupdate  -group $g_log  -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { str_msa [0]       } $module_path/i_str_msa(0)
  add wave -noupdate  -group $g_log  -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { str_mlt [0]       } $module_path/i_str_mlt(0)
  add wave -noupdate  -group $g_log  -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { str_mac [2]       } $module_path/i_str_mac(2)

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch1                        {//  mda : data ----------------------------------------------------------------------------------------------------}

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  clock                                                                                                         /}

  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_clk             } $module_path/i_clk
  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_cen             } $module_path/i_cen
  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_ced             } $module_path/i_ced

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  setting                                                                                                       /}

  add wave -noupdate  -group $g_log  -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { i_set_ini         } $module_path/i_set_ini
  add wave -noupdate  -group $g_log  -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { i_set_mac_ena     } $module_path/i_set_mac_ena
  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_uns  -label { i_set_mda_type    } $module_path/i_set_mda_type
  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { i_set_mda_const   } $module_path/i_set_mda_const
  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { i_set_mda_min     } $module_path/i_set_mda_min
  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { i_set_mda_max     } $module_path/i_set_mda_max

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  strob                                                                                                         /}

  add wave -noupdate  -group $g_log  -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { i_str_mda [1]     } $module_path/i_str_mda(1)
  add wave -noupdate  -group $g_log  -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { i_str_mda [0]     } $module_path/i_str_mda(0)
  add wave -noupdate  -group $g_log  -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { i_str_mda [2]     } $module_path/i_str_mda(2)

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  data                                                                                                          /}

  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { dat_mda           } $module_path/dat_mda
  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { dat_mda_0         } $module_path/dat_mda_0
  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { dat_mda_1         } $module_path/dat_mda_1
  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { dat_mda_2         } $module_path/dat_mda_2
  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { dat_mda_3         } $module_path/dat_mda_3
  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { dat_mda_4         } $module_path/dat_mda_4

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch1                        {//  msa : data ----------------------------------------------------------------------------------------------------}

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  clock                                                                                                         /}

  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_clk             } $module_path/i_clk
  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_cen             } $module_path/i_cen
  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_ced             } $module_path/i_ced

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  setting                                                                                                       /}

  add wave -noupdate  -group $g_log  -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { i_set_ini         } $module_path/i_set_ini
  add wave -noupdate  -group $g_log  -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { i_set_mac_ena     } $module_path/i_set_mac_ena
  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_uns  -label { i_set_msa_type    } $module_path/i_set_msa_type
  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { i_set_msa_const   } $module_path/i_set_msa_const
  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { i_set_msa_min     } $module_path/i_set_msa_min
  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { i_set_msa_max     } $module_path/i_set_msa_max

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  strob                                                                                                         /}

  add wave -noupdate  -group $g_log  -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { i_str_msa [1]     } $module_path/i_str_msa(1)
  add wave -noupdate  -group $g_log  -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { i_str_msa [0]     } $module_path/i_str_msa(0)
  add wave -noupdate  -group $g_log  -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { i_str_msa [2]     } $module_path/i_str_msa(2)

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  data                                                                                                          /}

  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { dat_msa           } $module_path/dat_msa
  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { dat_msa_0         } $module_path/dat_msa_0
  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { dat_msa_1         } $module_path/dat_msa_1
  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { dat_msa_2         } $module_path/dat_msa_2
  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { dat_msa_3         } $module_path/dat_msa_3
  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { dat_msa_4         } $module_path/dat_msa_4

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch1                        {//  mlt : data ----------------------------------------------------------------------------------------------------}

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  clock                                                                                                         /}

  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_clk             } $module_path/i_clk
  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_cen             } $module_path/i_cen
  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_ced             } $module_path/i_ced

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  setting                                                                                                       /}

  add wave -noupdate  -group $g_log  -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { i_set_ini         } $module_path/i_set_ini
  add wave -noupdate  -group $g_log  -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { i_set_mac_ena     } $module_path/i_set_mac_ena
  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_uns  -label { i_set_mlt_type    } $module_path/i_set_mlt_type
  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { i_set_mlt_const   } $module_path/i_set_mlt_const
  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { i_set_mlt_min     } $module_path/i_set_mlt_min
  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { i_set_mlt_max     } $module_path/i_set_mlt_max

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  strob                                                                                                         /}

  add wave -noupdate  -group $g_log  -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { i_str_mlt [1]     } $module_path/i_str_mlt(1)
  add wave -noupdate  -group $g_log  -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { i_str_mlt [0]     } $module_path/i_str_mlt(0)
  add wave -noupdate  -group $g_log  -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { i_str_mlt [2]     } $module_path/i_str_mlt(2)

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  data                                                                                                          /}

  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { dat_mlt           } $module_path/dat_mlt
  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { dat_mlt_0         } $module_path/dat_mlt_0
  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { dat_mlt_1         } $module_path/dat_mlt_1
  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { dat_mlt_2         } $module_path/dat_mlt_2
  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { dat_mlt_3         } $module_path/dat_mlt_3
  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { dat_mlt_4         } $module_path/dat_mlt_4
  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { dat_mlt_5         } $module_path/dat_mlt_5
  add wave -noupdate  -group $g_log  -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { dat_mlt_5_b1      } $module_path/dat_mlt_5_b1
  add wave -noupdate  -group $g_log  -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { dat_mlt_5_b2      } $module_path/dat_mlt_5_b2

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch1                        {//  ip header : data ----------------------------------------------------------------------------------------------}

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  clock                                                                                                         /}

  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_clk             } $module_path/i_clk
  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_cen             } $module_path/i_cen
  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_ced             } $module_path/i_ced

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  setting                                                                                                       /}

  add wave -noupdate  -group $g_log  -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { i_set_ini         } $module_path/i_set_ini
  add wave -noupdate  -group $g_log  -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { i_set_ip_ena      } $module_path/i_set_ip_ena
  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_uns  -label { i_set_ip_type     } $module_path/i_set_ip_type
  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { i_set_ip_const    } $module_path/i_set_ip_const
  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { i_set_ip_min      } $module_path/i_set_ip_min
  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { i_set_ip_max      } $module_path/i_set_ip_max

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  strob                                                                                                         /}

  add wave -noupdate  -group $g_log  -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { i_str_ip [1]      } $module_path/i_str_ip(1)
  add wave -noupdate  -group $g_log  -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { i_str_ip [0]      } $module_path/i_str_ip(0)
  add wave -noupdate  -group $g_log  -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { i_str_ip [2]      } $module_path/i_str_ip(2)

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  data                                                                                                          /}

  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { dat_ip            } $module_path/dat_ip
  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { dat_ip_0          } $module_path/dat_ip_0
  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { dat_ip_1          } $module_path/dat_ip_1
  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { dat_ip_2          } $module_path/dat_ip_2
  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { dat_ip_3          } $module_path/dat_ip_3
  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { dat_ip_4          } $module_path/dat_ip_4

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch1                        {//  payload : data ------------------------------------------------------------------------------------------------}

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  clock                                                                                                         /}

  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_clk             } $module_path/i_clk
  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_cen             } $module_path/i_cen
  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_ced             } $module_path/i_ced

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  setting                                                                                                       /}

  add wave -noupdate  -group $g_log  -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { i_set_ini         } $module_path/i_set_ini
  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_uns  -label { i_set_pay_type    } $module_path/i_set_pay_type
  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { i_set_pay_const   } $module_path/i_set_pay_const
  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { i_set_pay_min     } $module_path/i_set_pay_min
  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { i_set_pay_max     } $module_path/i_set_pay_max

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  strob                                                                                                         /}

  add wave -noupdate  -group $g_log  -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { i_str_pay [1]     } $module_path/i_str_pay(1)
  add wave -noupdate  -group $g_log  -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { i_str_pay [0]     } $module_path/i_str_pay(0)
  add wave -noupdate  -group $g_log  -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { i_str_pay [2]     } $module_path/i_str_pay(2)

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  data                                                                                                          /}

  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { dat_pay           } $module_path/dat_pay
  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { dat_pay_0         } $module_path/dat_pay_0
  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { dat_pay_1         } $module_path/dat_pay_1
  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { dat_pay_2         } $module_path/dat_pay_2
  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { dat_pay_3         } $module_path/dat_pay_3
  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { dat_pay_4         } $module_path/dat_pay_4

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch1                        {//  fcs : data ----------------------------------------------------------------------------------------------------}

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  clock                                                                                                         /}

  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_clk             } $module_path/i_clk
  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_cen             } $module_path/i_cen
  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_ced             } $module_path/i_ced

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  crc sum                                                                                                       /}

  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { crc_sum           } $module_path/crc_sum

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  crc error                                                                                                     /}

  add wave -noupdate  -group $g_log  -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { crc_err           } $module_path/i_crc_err

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  crc data                                                                                                      /}

  add wave -noupdate  -group $g_log  -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { str_crc [1]       } $module_path/i_str_crc(1)
  add wave -noupdate  -group $g_log  -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { str_crc [0]       } $module_path/i_str_crc(0)
  add wave -noupdate  -group $g_log  -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { str_crc [2]       } $module_path/i_str_crc(2)
  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { dat_crc           } $module_path/dat_crc
  add wave -noupdate  -group $g_log  -color $c_count  -format logic  -height $h_sig  -radix $r_hex  -label { cnt_crc           } $module_path/i_cnt_crc

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch1                        {//  fcs : calculator ----------------------------------------------------------------------------------------------}

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  clock                                                                                                         /}

  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_clk             } $module_path/i_clk
  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_cen             } $module_path/i_cen
  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_ced             } $module_path/i_ced

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  frame                                                                                                         /}

  add wave -noupdate  -group $g_log  -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { str_mac[0]        } $module_path/i_str_mac(0)
  add wave -noupdate  -group $g_log  -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { str_pay[0]        } $module_path/i_str_pay(0)
  add wave -noupdate  -group $g_log  -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { str_frame[0]      } $module_path/i_str_frame(0)
  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { dat_frame         } $module_path/dat_frame
  add wave -noupdate  -group $g_log  -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { str_frame[2]      } $module_path/i_str_frame(2)

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  calculator                                                                                                    /}

  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { crc_calc          } $module_path/crc_calc
  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { crc_dec           } $module_path/crc_dec
  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { crc_sum           } $module_path/crc_sum
  add wave -noupdate  -group $g_log                   -divider       -height $h_nul  <NULL>
  }; # wave_log
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  # add wave : all wave
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  if {$wave_all} {
  add wave -noupdate  -group $g_mod                                  -height $h_grp  -group $g_all
  add wave -noupdate  -group $g_all                   -divider       -height $h_nul  <NULL>
  add wave -noupdate  -group $g_all                                                                                              $module_path/*
  add wave -noupdate  -group $g_all                   -divider       -height $h_nul  <NULL>
  }; # wave_all
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

  add wave -noupdate  -group $g_mod                   -divider       -height $h_nul  <NULL>

  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#




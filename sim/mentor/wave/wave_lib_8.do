  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  #
  #  Company      : Micran, Department of Telecommunications
  #
  #  Engineer     : Pogodaev Danil
  #
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  #
  #  Library      : lib_ethernet / library of ethernet components
  #
  #  Component    : eth_rx_v2 / testbench emulator of ethernet frames (packets)
  #
  #  File Name    : wave_lib_8 .do (tcl sim mentor)
  #
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  #
  #  Description  : List of module waveforms for Mentor simulator.
  #
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  #
  #  Note (1)     : Command 'add wave'              The add wave command adds the following objects to the Wave window:
  #
  #                                                  � VHDL signals and variables
  #                                                  � Verilog nets and registers
  #                                                  � System Verilog class objects
  #                                                  � SystemC primitive channels (signals)
  #                                                  � Dividers and user-defined buses
  #
  #                  -noupdate                      A switch that prevents the Wave window from updating when a series of add wave commands are executed in series.
  #
  #                                                 If no port mode is specified, add wave will display all objects in the selected region with names matching the object
  #                                                 name specification.
  #
  #                  -expand <signal_name>          A switch and argument pair that instructs the command to expand a compound signal immediately, but only one level down.
  #                                                 <signal_name> � a string that specifies the name of the signal. This string can include wildcards.
  #
  #                  -group <group_name>            A switch and argument group that creates a wave group with the specified group_name.
  #                  [<sig_name1> ...]   
  #                                                 <group_name> � a string that specifies the name of the group. You must enclose this argument in quotes (") or braces ({ })
  #                                                 if it contains any white space.
  #
  #                                                 <sig_name> ... � a string, which is repeatable in a space separated list, that specifies the signals to add to the group.
  #                                                 This command creates an empty group if you do not specify any signal names.
  #
  #                  -height <pixels>               A switch and argument pair that specifies the height, in pixels, of the waveform.
  #
  #                  -color <standard_color_name>   A switch and argument pair that specifies the color used to display a waveform.
  #
  #                                                 <standard_color_name> � You can use either of the following:
  #
  #                                                  � standard X Window color name � enclose 2-word names in quotes ("), for example: -color "light blue"
  #                                                  � rgb value, for example: -color #357f77
  #  
  #                                                 Definitions of color names (e.g. steelblue or gainsboro) have been provided by the World Wide Web Consortium and
  #                                                 are available at www.w3.org/TR/SVG/types.html#ColorKeywords. Color transparent represents the absence of a color.
  #
  #                  -<format>                      A choice between switches that specify the display format of the objects. Valid entries are:
  #
  #                                                  -format literal               Literal waveforms are displayed as a box containing the object value.
  #                                                  -format logic                 Logic signals may be U, X, 0, 1, Z, W, L, H, or �-�.
  #                                                  -format analog-step           Analog-step changes to the new time before plotting the new Y.
  #                                                  -format analog-interpolated   Analog-interpolated draws a diagonal line.
  #                                                  -format analog-backstep       Analog-backstep plots the new Y before moving to the new time.
  #                                                  -format event                 Displays a mark at every transition.
  #
  #                  -radix <type>                  A choice between switches that specify the radix for the objects that follow in the command.
  #                                                 Valid entries (or any unique abbreviations) are:
  #                  -<radix_type>   
  #                                                  -radix binary    -binary
  #                                                  -radix ascii     -ascii
  #                                                  -radix unsigned  -unsigned
  #                                                  -radix decimal   -decimal
  #                                                  -radix octal     -octal
  #                                                  -radix hex       -hex
  #                                                  -radix symbolic  -symbolic
  #                                                  -radix time      -time
  #                                                  -radix default   -default
  #
  #                                                 If no radix is specified for an enumerated type, the default representation is used.
  #
  #                  -divider [<divider_name>]      A switch and argument pair that adds a divider to the Wave window.
  #
  #                                                 <divider_name> � A string, which is repeatable in a space separated list, that specifies the name of the divider,
  #                                                 which appears in the pathnames column.
  #
  #                                                 You cannot begin a name with a hyphen (-).
  #                                                 You can begin a name with a space, but you must enclose the name within quotes (") or braces ({ })
  #                                                 If you do not specify this argument, the command inserts an unnamed divider.
  #
  #                  -label <name>                  A switch and argument pair that specifies an alternative name for the signal being added.
  #                                                 This alternative name is not valid in a force or examine command; however, it can be used in a search command
  #                                                 with the wave option.
  #
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  #
  #  Revision List
  #
  #  Version   Date       Author
  #
  #  2.0       26.09.16   Pogodaev Danil
  #
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  # setting : module
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

  set module_index "lib 8.0"
  set module_name  "rx_dat"
  set module_path  "/tb_top/eth_rx/rx_dat"

  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  # setting : wave groups (on/off)
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

  set wave_par 1 ; # group : parameters (1/0)
  set wave_io  1 ; # group : interface  (1/0)
  set wave_log 1 ; # group : logic      (1/0)
  set wave_all 0 ; # group : all wave   (1/0)

  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  # setting : wave groups (display)
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

  set g_mod " $module_index :  $module_name " ; # group : module
  set g_par " $module_index :  parameters   " ; # group : parameter
  set g_io  " $module_index :  interface    " ; # group : interface
  set g_log " $module_index :  logic        " ; # group : logic
  set g_all " $module_index :  all wave     " ; # group : all wave

  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  # add wave : module name
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

  add wave -noupdate                                                 -height $h_mod  -expand  -group $g_mod
  add wave -noupdate  -group $g_mod                   -divider       -height $h_nul  <NULL>
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  # add wave : parameters
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  if {$wave_par} {
  add wave -noupdate  -group $g_mod                                  -height $h_grp  -group $g_par
  add wave -noupdate  -group $g_par                   -divider       -height $h_nul  <NULL>
  add wave -noupdate  -group $g_par                   -divider       -height $h_ch1                        {//  packet structure ----------------------------------------------------------------------------------------------}

  add wave -noupdate  -group $g_par                   -divider       -height $h_ch2                        { >  mac header                                                                                                    /}

  add wave -noupdate  -group $g_par  -color $c_param  -format logic  -height $h_sig  -radix $r_uns  -label { pE_RX_MAC           } $module_path/pE_RX_MAC

  add wave -noupdate  -group $g_par                   -divider       -height $h_ch2                        { >  ip header                                                                                                     /}

  add wave -noupdate  -group $g_par  -color $c_param  -format logic  -height $h_sig  -radix $r_uns  -label { pE_RX_IP            } $module_path/pE_RX_IP
  add wave -noupdate  -group $g_par  -color $c_param  -format logic  -height $h_sig  -radix $r_uns  -label { pN_RX_IPL           } $module_path/pN_RX_IPL
  add wave -noupdate  -group $g_par  -color $c_param  -format logic  -height $h_sig  -radix $r_uns  -label { pN_RX_IPH           } $module_path/pN_RX_IPH

  add wave -noupdate  -group $g_par                   -divider       -height $h_ch1                        {//  local (for i/o) -----------------------------------------------------------------------------------------------}

  add wave -noupdate  -group $g_par                   -divider       -height $h_ch2                        { >  packet structure                                                                                              /}

  add wave -noupdate  -group $g_par  -color $c_param  -format logic  -height $h_sig  -radix $r_uns  -label { lpN_RX_IPL          } $module_path/lpN_RX_IPL
  add wave -noupdate  -group $g_par  -color $c_param  -format logic  -height $h_sig  -radix $r_uns  -label { lpN_RX_IPH          } $module_path/lpN_RX_IPH
  add wave -noupdate  -group $g_par  -color $c_param  -format logic  -height $h_sig  -radix $r_uns  -label { lpN_RX_IP           } $module_path/lpN_RX_IP
  add wave -noupdate  -group $g_par  -color $c_param  -format logic  -height $h_sig  -radix $r_uns  -label { lpW_RX_IP           } $module_path/lpW_RX_IP
  add wave -noupdate  -group $g_par                   -divider       -height $h_nul  <NULL>
  }; # wave_par
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  # add wave : interface
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  if {$wave_io} {
  add wave -noupdate  -group $g_mod                                  -height $h_grp  -group $g_io
  add wave -noupdate  -group $g_io                    -divider       -height $h_nul  <NULL>
  add wave -noupdate  -group $g_io                    -divider       -height $h_ch1                        {//  clear ---------------------------------------------------------------------------------------------------------}

  add wave -noupdate  -group $g_io   -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_sclr_n            } $module_path/i_sclr_n

  add wave -noupdate  -group $g_io                    -divider       -height $h_ch1                        {//  clock ---------------------------------------------------------------------------------------------------------}

  add wave -noupdate  -group $g_io   -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_clk               } $module_path/i_clk
  add wave -noupdate  -group $g_io   -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_cen               } $module_path/i_cen
  add wave -noupdate  -group $g_io   -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_ced               } $module_path/i_ced

  add wave -noupdate  -group $g_io                    -divider       -height $h_ch1                        {//  strob ---------------------------------------------------------------------------------------------------------}

  add wave -noupdate  -group $g_io   -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { i_str_mda           } $module_path/i_str_mda(0)
  add wave -noupdate  -group $g_io   -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { i_str_msa           } $module_path/i_str_msa(0)
  add wave -noupdate  -group $g_io   -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { i_str_mlt           } $module_path/i_str_mlt(0)
  add wave -noupdate  -group $g_io   -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { i_str_ip            } $module_path/i_str_ip(0)

  add wave -noupdate  -group $g_io                    -divider       -height $h_ch1                        {//  data / body ---------------------------------------------------------------------------------------------------}

  add wave -noupdate  -group $g_io   -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { i_dat_load          } $module_path/i_dat_load
  add wave -noupdate  -group $g_io   -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { i_dat_body          } $module_path/i_dat_body

  add wave -noupdate  -group $g_io                    -divider       -height $h_ch1                        {//  data / mac ----------------------------------------------------------------------------------------------------}

  add wave -noupdate  -group $g_io   -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { o_rdy_mac           } $module_path/o_rdy_mac
  add wave -noupdate  -group $g_io   -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { o_dat_mda           } $module_path/o_dat_mda
  add wave -noupdate  -group $g_io   -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { o_dat_msa           } $module_path/o_dat_msa
  add wave -noupdate  -group $g_io   -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { o_dat_mlt           } $module_path/o_dat_mlt

  add wave -noupdate  -group $g_io                    -divider       -height $h_ch1                        {//  data / ip -----------------------------------------------------------------------------------------------------}

  add wave -noupdate  -group $g_io   -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { o_rdy_ip            } $module_path/o_rdy_ip
  add wave -noupdate  -group $g_io   -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { o_dat_ip            } $module_path/o_dat_ip
  add wave -noupdate  -group $g_io                    -divider       -height $h_nul  <NULL>
  }; # wave_io
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  # add wave : logic
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  if {$wave_log} {
  add wave -noupdate  -group $g_mod                                  -height $h_grp  -group $g_log
  add wave -noupdate  -group $g_log                   -divider       -height $h_nul  <NULL>
  add wave -noupdate  -group $g_log                   -divider       -height $h_ch1                        {//  data mda / shift ----------------------------------------------------------------------------------------------}

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  clock                                                                                                         /}

  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_clk               } $module_path/shift_mda/i_clk
  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_clk_en            } $module_path/shift_mda/i_clk_en
  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_sclr              } $module_path/shift_mda/i_sclr

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  serial                                                                                                        /}

  add wave -noupdate  -group $g_log  -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { i_sload             } $module_path/shift_mda/i_sload
  add wave -noupdate  -group $g_log  -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { i_sload_en          } $module_path/shift_mda/i_sload_en
  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { i_sdata             } $module_path/shift_mda/i_sdata

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  parallel                                                                                                      /}

  add wave -noupdate  -group $g_log  -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { o_pload             } $module_path/shift_mda/o_pload
  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { o_pdata             } $module_path/shift_mda/o_pdata

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch1                        {//  data msa / shift ----------------------------------------------------------------------------------------------}

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  clock                                                                                                         /}

  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_clk               } $module_path/shift_msa/i_clk
  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_clk_en            } $module_path/shift_msa/i_clk_en
  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_sclr              } $module_path/shift_msa/i_sclr

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  serial                                                                                                        /}

  add wave -noupdate  -group $g_log  -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { i_sload             } $module_path/shift_msa/i_sload
  add wave -noupdate  -group $g_log  -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { i_sload_en          } $module_path/shift_msa/i_sload_en
  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { i_sdata             } $module_path/shift_msa/i_sdata

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  parallel                                                                                                      /}

  add wave -noupdate  -group $g_log  -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { o_pload             } $module_path/shift_msa/o_pload
  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { o_pdata             } $module_path/shift_msa/o_pdata

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch1                        {//  data mlt / shift ----------------------------------------------------------------------------------------------}

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  clock                                                                                                         /}

  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_clk               } $module_path/shift_mlt/i_clk
  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_clk_en            } $module_path/shift_mlt/i_clk_en
  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_sclr              } $module_path/shift_mlt/i_sclr

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  serial                                                                                                        /}

  add wave -noupdate  -group $g_log  -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { i_sload             } $module_path/shift_mlt/i_sload
  add wave -noupdate  -group $g_log  -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { i_sload_en          } $module_path/shift_mlt/i_sload_en
  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { i_sdata             } $module_path/shift_mlt/i_sdata

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  parallel                                                                                                      /}

  add wave -noupdate  -group $g_log  -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { o_pload             } $module_path/shift_mlt/o_pload
  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { o_pdata             } $module_path/shift_mlt/o_pdata

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch1                        {//  data ip / shift -----------------------------------------------------------------------------------------------}

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  clock                                                                                                         /}

  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_clk                } $module_path/shift_ip/i_clk
  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_clk_en             } $module_path/shift_ip/i_clk_en
  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_sclr               } $module_path/shift_ip/i_sclr

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  serial                                                                                                        /}

  add wave -noupdate  -group $g_log  -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { i_sload              } $module_path/shift_ip/i_sload
  add wave -noupdate  -group $g_log  -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { i_sload_en           } $module_path/shift_ip/i_sload_en
  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { i_sdata              } $module_path/shift_ip/i_sdata

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  parallel                                                                                                      /}

  add wave -noupdate  -group $g_log  -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { o_pload              } $module_path/shift_ip/o_pload
  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { o_pdata              } $module_path/shift_ip/o_pdata
  add wave -noupdate  -group $g_log                   -divider       -height $h_nul  <NULL>
  }; # wave_log
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  # add wave : all wave
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  if {$wave_all} {
  add wave -noupdate  -group $g_mod                                  -height $h_grp  -group $g_all
  add wave -noupdate  -group $g_all                   -divider       -height $h_nul  <NULL>
  add wave -noupdate  -group $g_all                                                                                                 $module_path/*
  add wave -noupdate  -group $g_all                   -divider       -height $h_nul  <NULL>
  }; # wave_all
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

  add wave -noupdate  -group $g_mod                   -divider       -height $h_nul  <NULL>

  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#




  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  #
  #  Company      : Micran, Department of Telecommunications
  #
  #  Engineer     : Pogodaev Danil
  #
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  #
  #  Library      : lib_ethernet / library of ethernet components
  #
  #  Component    : eth_rx_v2 / testbench emulator of ethernet frames (packets)
  #
  #  File Name    : wave_lib_3 .do (tcl sim mentor)
  #
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  #
  #  Description  : List of module waveforms for Mentor simulator.
  #
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  #
  #  Note (1)     : Command 'add wave'              The add wave command adds the following objects to the Wave window:
  #
  #                                                  � VHDL signals and variables
  #                                                  � Verilog nets and registers
  #                                                  � System Verilog class objects
  #                                                  � SystemC primitive channels (signals)
  #                                                  � Dividers and user-defined buses
  #
  #                  -noupdate                      A switch that prevents the Wave window from updating when a series of add wave commands are executed in series.
  #
  #                                                 If no port mode is specified, add wave will display all objects in the selected region with names matching the object
  #                                                 name specification.
  #
  #                  -expand <signal_name>          A switch and argument pair that instructs the command to expand a compound signal immediately, but only one level down.
  #                                                 <signal_name> � a string that specifies the name of the signal. This string can include wildcards.
  #
  #                  -group <group_name>            A switch and argument group that creates a wave group with the specified group_name.
  #                  [<sig_name1> ...]   
  #                                                 <group_name> � a string that specifies the name of the group. You must enclose this argument in quotes (") or braces ({ })
  #                                                 if it contains any white space.
  #
  #                                                 <sig_name> ... � a string, which is repeatable in a space separated list, that specifies the signals to add to the group.
  #                                                 This command creates an empty group if you do not specify any signal names.
  #
  #                  -height <pixels>               A switch and argument pair that specifies the height, in pixels, of the waveform.
  #
  #                  -color <standard_color_name>   A switch and argument pair that specifies the color used to display a waveform.
  #
  #                                                 <standard_color_name> � You can use either of the following:
  #
  #                                                  � standard X Window color name � enclose 2-word names in quotes ("), for example: -color "light blue"
  #                                                  � rgb value, for example: -color #357f77
  #  
  #                                                 Definitions of color names (e.g. steelblue or gainsboro) have been provided by the World Wide Web Consortium and
  #                                                 are available at www.w3.org/TR/SVG/types.html#ColorKeywords. Color transparent represents the absence of a color.
  #
  #                  -<format>                      A choice between switches that specify the display format of the objects. Valid entries are:
  #
  #                                                  -format literal               Literal waveforms are displayed as a box containing the object value.
  #                                                  -format logic                 Logic signals may be U, X, 0, 1, Z, W, L, H, or �-�.
  #                                                  -format analog-step           Analog-step changes to the new time before plotting the new Y.
  #                                                  -format analog-interpolated   Analog-interpolated draws a diagonal line.
  #                                                  -format analog-backstep       Analog-backstep plots the new Y before moving to the new time.
  #                                                  -format event                 Displays a mark at every transition.
  #
  #                  -radix <type>                  A choice between switches that specify the radix for the objects that follow in the command.
  #                                                 Valid entries (or any unique abbreviations) are:
  #                  -<radix_type>   
  #                                                  -radix binary    -binary
  #                                                  -radix ascii     -ascii
  #                                                  -radix unsigned  -unsigned
  #                                                  -radix decimal   -decimal
  #                                                  -radix octal     -octal
  #                                                  -radix hex       -hex
  #                                                  -radix symbolic  -symbolic
  #                                                  -radix time      -time
  #                                                  -radix default   -default
  #
  #                                                 If no radix is specified for an enumerated type, the default representation is used.
  #
  #                  -divider [<divider_name>]      A switch and argument pair that adds a divider to the Wave window.
  #
  #                                                 <divider_name> � A string, which is repeatable in a space separated list, that specifies the name of the divider,
  #                                                 which appears in the pathnames column.
  #
  #                                                 You cannot begin a name with a hyphen (-).
  #                                                 You can begin a name with a space, but you must enclose the name within quotes (") or braces ({ })
  #                                                 If you do not specify this argument, the command inserts an unnamed divider.
  #
  #                  -label <name>                  A switch and argument pair that specifies an alternative name for the signal being added.
  #                                                 This alternative name is not valid in a force or examine command; however, it can be used in a search command
  #                                                 with the wave option.
  #
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  #
  #  Revision List
  #
  #  Version   Date       Author
  #
  #  2.0       26.09.16   Pogodaev Danil
  #
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  # setting : module
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

  set module_index "lib 3.0"
  set module_name  "rx_rmii"
  set module_path  "/tb_top/eth_rx/rx_rmii"

  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  # setting : wave groups (on/off)
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

  set wave_par 1 ; # group : parameters (1/0)
  set wave_io  1 ; # group : interface  (1/0)
  set wave_log 1 ; # group : logic      (1/0)
  set wave_all 0 ; # group : all wave   (1/0)

  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  # setting : wave groups (display)
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

  set g_mod " $module_index :  $module_name " ; # group : module
  set g_par " $module_index :  parameters   " ; # group : parameter
  set g_io  " $module_index :  interface    " ; # group : interface
  set g_log " $module_index :  logic        " ; # group : logic
  set g_all " $module_index :  all wave     " ; # group : all wave

  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  # add wave : module name
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

  add wave -noupdate                                                 -height $h_mod  -expand  -group $g_mod
  add wave -noupdate  -group $g_mod                   -divider       -height $h_nul  <NULL>
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  # add wave : parameters
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  if {$wave_par} {
  add wave -noupdate  -group $g_mod                                  -height $h_grp  -group $g_par
  add wave -noupdate  -group $g_par                   -divider       -height $h_nul  <NULL>
  add wave -noupdate  -group $g_par                   -divider       -height $h_ch1                        { < none > }
  add wave -noupdate  -group $g_par                   -divider       -height $h_nul  <NULL>
  }; # wave_par
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  # add wave : interface
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  if {$wave_io} {
  add wave -noupdate  -group $g_mod                                  -height $h_grp  -group $g_io
  add wave -noupdate  -group $g_io                    -divider       -height $h_nul  <NULL>
  add wave -noupdate  -group $g_io                    -divider       -height $h_ch1                        {//  clear ---------------------------------------------------------------------------------------------------------}

  add wave -noupdate  -group $g_io   -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_sclr_n          } $module_path/i_sclr_n

  add wave -noupdate  -group $g_io                    -divider       -height $h_ch1                        {//  setting -------------------------------------------------------------------------------------------------------}

  add wave -noupdate  -group $g_io   -color $c_pdata  -format logic  -height $h_sig  -radix $r_uns  -label { i_set_speed       } $module_path/i_set_speed

  add wave -noupdate  -group $g_io                    -divider       -height $h_ch1                        {//  rmii input ----------------------------------------------------------------------------------------------------}

  add wave -noupdate  -group $g_io   -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_rmii_clk        } $module_path/i_rmii_clk
  add wave -noupdate  -group $g_io   -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { i_rmii_crs_dv     } $module_path/i_rmii_crs_dv
  add wave -noupdate  -group $g_io   -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { i_rmii_rxd        } $module_path/i_rmii_rxd

  add wave -noupdate  -group $g_io                    -divider       -height $h_ch1                        {//  rmii recover --------------------------------------------------------------------------------------------------}

  add wave -noupdate  -group $g_io   -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { o_rmii_clk        } $module_path/o_rmii_clk
  add wave -noupdate  -group $g_io   -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { o_rmii_crs        } $module_path/o_rmii_crs
  add wave -noupdate  -group $g_io   -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { o_rmii_rx_dv      } $module_path/o_rmii_rx_dv
  add wave -noupdate  -group $g_io   -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { o_rmii_rxd        } $module_path/o_rmii_rxd

  add wave -noupdate  -group $g_io                    -divider       -height $h_ch1                        {//  rmii output ---------------------------------------------------------------------------------------------------}

  add wave -noupdate  -group $g_io   -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { o_lan_clk         } $module_path/o_lan_clk
  add wave -noupdate  -group $g_io   -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { o_lan_cen         } $module_path/o_lan_cen
  add wave -noupdate  -group $g_io   -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { o_lan_ced         } $module_path/o_lan_ced

  add wave -noupdate  -group $g_io   -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { o_lan_rx_dv       } $module_path/o_lan_rx_dv
  add wave -noupdate  -group $g_io   -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { o_lan_rxd         } $module_path/o_lan_rxd
  add wave -noupdate  -group $g_io                    -divider       -height $h_nul  <NULL>
  }; # wave_io
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  # add wave : logic
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  if {$wave_log} {
  add wave -noupdate  -group $g_mod                                  -height $h_grp  -group $g_log
  add wave -noupdate  -group $g_log                   -divider       -height $h_nul  <NULL>
  add wave -noupdate  -group $g_log                   -divider       -height $h_ch1                        {//  clock manager -------------------------------------------------------------------------------------------------}

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  clock                                                                                                         /}

  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_rmii_clk          } $module_path/i_rmii_clk

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  setting                                                                                                       /}

  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_uns  -label { i_set_speed         } $module_path/i_set_speed

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  generator                                                                                                     /}

  add wave -noupdate  -group $g_log  -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { gen_rmii_cen        } $module_path/gen_rmii_cen
  add wave -noupdate  -group $g_log  -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { gen_rmii_ced        } $module_path/gen_rmii_ced
  add wave -noupdate  -group $g_log  -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { gen_rmii_clk_10     } $module_path/gen_rmii_clk_10

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  clock manager                                                                                                 /}

  add wave -noupdate  -group $g_log  -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { rmii_clk            } $module_path/rmii_clk
  add wave -noupdate  -group $g_log  -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { rmii_clk_s          } $module_path/rmii_clk_s
  add wave -noupdate  -group $g_log  -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { rmii_cen            } $module_path/rmii_cen
  add wave -noupdate  -group $g_log  -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { rmii_ced            } $module_path/rmii_ced

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch1                        {//  recover rx_dv -------------------------------------------------------------------------------------------------}

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  rmii input                                                                                                    /}

  add wave -noupdate  -group $g_log  -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { ri_rmii_crs_dv      } $module_path/ri_rmii_crs_dv
  add wave -noupdate  -group $g_log  -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { ri_rmii_crs_dv_z    } $module_path/ri_rmii_crs_dv_z
  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { ri_rmii_rxd         } $module_path/ri_rmii_rxd

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  rmii recover                                                                                                  /}

  add wave -noupdate  -group $g_log  -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { rec_rx_dv           } $module_path/rec_rx_dv
  add wave -noupdate  -group $g_log  -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { rec_rx_dv_keep      } $module_path/rec_rx_dv_keep

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch1                        {//  recover crs ---------------------------------------------------------------------------------------------------}

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  rmii input                                                                                                    /}

  add wave -noupdate  -group $g_log  -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { ri_rmii_crs_dv      } $module_path/ri_rmii_crs_dv
  add wave -noupdate  -group $g_log  -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { ri_rmii_crs_dv_re   } $module_path/ri_rmii_crs_dv_re
  add wave -noupdate  -group $g_log  -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { ri_rmii_crs_dv_fe   } $module_path/ri_rmii_crs_dv_fe
  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { ri_rmii_rxd         } $module_path/ri_rmii_rxd

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  rmii recover                                                                                                  /}

  add wave -noupdate  -group $g_log  -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { rec_rx_dv           } $module_path/rec_rx_dv
  add wave -noupdate  -group $g_log  -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { rec_crs             } $module_path/rec_crs

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch1                        {//  rmii sync -----------------------------------------------------------------------------------------------------}

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  rmii input                                                                                                    /}

  add wave -noupdate  -group $g_log  -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { ri_rmii_crs_dv_z    } $module_path/ri_rmii_crs_dv_z
  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { ri_rmii_rxd_z       } $module_path/ri_rmii_rxd_z

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  rmii recover                                                                                                  /}

  add wave -noupdate  -group $g_log  -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { rec_crs_z           } $module_path/rec_crs_z
  add wave -noupdate  -group $g_log  -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { rec_rx_dv           } $module_path/rec_rx_dv
  add wave -noupdate  -group $g_log  -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { rec_rx_dv_z         } $module_path/rec_rx_dv_z

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  rmii sync                                                                                                     /}

  add wave -noupdate  -group $g_log  -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { rmii_crs_dv         } $module_path/rmii_crs_dv
  add wave -noupdate  -group $g_log  -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { rmii_crs            } $module_path/rmii_crs
  add wave -noupdate  -group $g_log  -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { rmii_rx_dv          } $module_path/rmii_rx_dv
  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { rmii_rxd            } $module_path/rmii_rxd

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch1                        {//  deserializer --------------------------------------------------------------------------------------------------}

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  clock manager                                                                                                 /}

  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { rmii_clk            } $module_path/rmii_clk
  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { rmii_cen            } $module_path/rmii_cen
  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { rmii_ced            } $module_path/rmii_ced

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  rmii sync                                                                                                     /}

  add wave -noupdate  -group $g_log  -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { rmii_rx_dv          } $module_path/rmii_rx_dv
  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { rmii_rxd            } $module_path/rmii_rxd

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  shift register                                                                                                /}

  add wave -noupdate  -group $g_log  -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { shift_ena           } $module_path/shift_ena
  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { shift_sd            } $module_path/shift_sd
  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { shift_reg           } $module_path/shift_reg
  add wave -noupdate  -group $g_log  -color $c_count  -format logic  -height $h_sig  -radix $r_uns  -label { shift_cnt           } $module_path/shift_cnt
  add wave -noupdate  -group $g_log  -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { shift_latch         } $module_path/shift_latch
  add wave -noupdate  -group $g_log  -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { shift_str           } $module_path/shift_str
  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { shift_pd            } $module_path/shift_pd
  add wave -noupdate  -group $g_log  -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { shift_load          } $module_path/shift_load

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch1                        {//  lan output (8 bit) --------------------------------------------------------------------------------------------}

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  clock                                                                                                         /}

  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { lan_clk             } $module_path/lan_clk
  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { lan_cen             } $module_path/lan_cen
  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { lan_ced             } $module_path/lan_ced

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  data (8 bit)                                                                                                  /}

  add wave -noupdate  -group $g_log  -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { lan_rx_dv           } $module_path/lan_rx_dv
  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { lan_rxd             } $module_path/lan_rxd
  add wave -noupdate  -group $g_log                   -divider       -height $h_nul  <NULL>
  }; # wave_log
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  # add wave : all wave
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  if {$wave_all} {
  add wave -noupdate  -group $g_mod                                  -height $h_grp  -group $g_all
  add wave -noupdate  -group $g_all                   -divider       -height $h_nul  <NULL>
  add wave -noupdate  -group $g_all                                                                                                $module_path/*
  add wave -noupdate  -group $g_all                   -divider       -height $h_nul  <NULL>
  }; # wave_all
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

  add wave -noupdate  -group $g_mod                   -divider       -height $h_nul  <NULL>

  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#




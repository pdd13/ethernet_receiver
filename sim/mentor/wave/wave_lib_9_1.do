  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  #
  #  Company      : Micran, Department of Telecommunications
  #
  #  Engineer     : Pogodaev Danil
  #
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  #
  #  Library      : lib_ethernet / library of ethernet components
  #
  #  Component    : eth_rx_v2 / receiver of ethernet frames (packets)
  #
  #  File Name    : wave_lib_9_1 .do (tcl sim mentor)
  #
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  #
  #  Description  : List of module waveforms for Mentor simulator.
  #
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  #
  #  Note (1)     : Command 'add wave'              The add wave command adds the following objects to the Wave window:
  #
  #                                                  � VHDL signals and variables
  #                                                  � Verilog nets and registers
  #                                                  � System Verilog class objects
  #                                                  � SystemC primitive channels (signals)
  #                                                  � Dividers and user-defined buses
  #
  #                  -noupdate                      A switch that prevents the Wave window from updating when a series of add wave commands are executed in series.
  #
  #                                                 If no port mode is specified, add wave will display all objects in the selected region with names matching the object
  #                                                 name specification.
  #
  #                  -expand <signal_name>          A switch and argument pair that instructs the command to expand a compound signal immediately, but only one level down.
  #                                                 <signal_name> � a string that specifies the name of the signal. This string can include wildcards.
  #
  #                  -group <group_name>            A switch and argument group that creates a wave group with the specified group_name.
  #                  [<sig_name1> ...]   
  #                                                 <group_name> � a string that specifies the name of the group. You must enclose this argument in quotes (") or braces ({ })
  #                                                 if it contains any white space.
  #
  #                                                 <sig_name> ... � a string, which is repeatable in a space separated list, that specifies the signals to add to the group.
  #                                                 This command creates an empty group if you do not specify any signal names.
  #
  #                  -height <pixels>               A switch and argument pair that specifies the height, in pixels, of the waveform.
  #
  #                  -color <standard_color_name>   A switch and argument pair that specifies the color used to display a waveform.
  #
  #                                                 <standard_color_name> � You can use either of the following:
  #
  #                                                  � standard X Window color name � enclose 2-word names in quotes ("), for example: -color "light blue"
  #                                                  � rgb value, for example: -color #357f77
  #  
  #                                                 Definitions of color names (e.g. steelblue or gainsboro) have been provided by the World Wide Web Consortium and
  #                                                 are available at www.w3.org/TR/SVG/types.html#ColorKeywords. Color transparent represents the absence of a color.
  #
  #                  -<format>                      A choice between switches that specify the display format of the objects. Valid entries are:
  #
  #                                                  -format literal               Literal waveforms are displayed as a box containing the object value.
  #                                                  -format logic                 Logic signals may be U, X, 0, 1, Z, W, L, H, or �-�.
  #                                                  -format analog-step           Analog-step changes to the new time before plotting the new Y.
  #                                                  -format analog-interpolated   Analog-interpolated draws a diagonal line.
  #                                                  -format analog-backstep       Analog-backstep plots the new Y before moving to the new time.
  #                                                  -format event                 Displays a mark at every transition.
  #
  #                  -radix <type>                  A choice between switches that specify the radix for the objects that follow in the command.
  #                                                 Valid entries (or any unique abbreviations) are:
  #                  -<radix_type>   
  #                                                  -radix binary    -binary
  #                                                  -radix ascii     -ascii
  #                                                  -radix unsigned  -unsigned
  #                                                  -radix decimal   -decimal
  #                                                  -radix octal     -octal
  #                                                  -radix hex       -hex
  #                                                  -radix symbolic  -symbolic
  #                                                  -radix time      -time
  #                                                  -radix default   -default
  #
  #                                                 If no radix is specified for an enumerated type, the default representation is used.
  #
  #                  -divider [<divider_name>]      A switch and argument pair that adds a divider to the Wave window.
  #
  #                                                 <divider_name> � A string, which is repeatable in a space separated list, that specifies the name of the divider,
  #                                                 which appears in the pathnames column.
  #
  #                                                 You cannot begin a name with a hyphen (-).
  #                                                 You can begin a name with a space, but you must enclose the name within quotes (") or braces ({ })
  #                                                 If you do not specify this argument, the command inserts an unnamed divider.
  #
  #                  -label <name>                  A switch and argument pair that specifies an alternative name for the signal being added.
  #                                                 This alternative name is not valid in a force or examine command; however, it can be used in a search command
  #                                                 with the wave option.
  #
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  #
  #  Revision List
  #
  #  Version   Date       Author
  #
  #  2.0       26.09.16   Pogodaev Danil
  #
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  # setting : module
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

  set module_index "lib 9.1"
  set module_name  "rx_crc"
  set module_path  "/tb_top/eth_rx/rx_cnt/rx_crc"
  
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  # setting : wave groups (on/off)
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

  set wave_par 1 ; # group : parameters (1/0)
  set wave_io  1 ; # group : interface  (1/0)
  set wave_log 1 ; # group : logic      (1/0)
  set wave_all 0 ; # group : all wave   (1/0)

  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  # setting : wave groups (display)
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

  set g_mod " $module_index :  $module_name " ; # group : module
  set g_par " $module_index :  parameters   " ; # group : parameter
  set g_io  " $module_index :  interface    " ; # group : interface
  set g_log " $module_index :  logic        " ; # group : logic
  set g_all " $module_index :  all wave     " ; # group : all wave

  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  # add wave : module name
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

  add wave -noupdate                                                 -height $h_mod  -expand  -group $g_mod
  add wave -noupdate  -group $g_mod                   -divider       -height $h_nul  <NULL>
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  # add wave : parameters
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  if {$wave_par} {
  add wave -noupdate  -group $g_mod                                  -height $h_grp  -group $g_par
  add wave -noupdate  -group $g_par                   -divider       -height $h_nul  <NULL>

  add wave -noupdate  -group $g_par                   -divider       -height $h_ch1                        {//  crc comparator ------------------------------------------------------------------------------------------------}

  add wave -noupdate  -group $g_par  -color $c_param  -format logic  -height $h_sig  -radix $r_uns  -label { pV_DELAY          } $module_path/pV_DELAY

  add wave -noupdate  -group $g_par                   -divider       -height $h_ch1                        {//  in/out registers ----------------------------------------------------------------------------------------------}

  add wave -noupdate  -group $g_par  -color $c_param  -format logic  -height $h_sig  -radix $r_uns  -label { pN_REG_I          } $module_path/pN_REG_I
  add wave -noupdate  -group $g_par  -color $c_param  -format logic  -height $h_sig  -radix $r_uns  -label { pN_REG_O          } $module_path/pN_REG_O

  add wave -noupdate  -group $g_par                   -divider       -height $h_ch1                        {//  signal tap ----------------------------------------------------------------------------------------------------}

  add wave -noupdate  -group $g_par  -color $c_param  -format logic  -height $h_sig  -radix $r_uns  -label { pE_GNPR           } $module_path/pE_GNPR
  add wave -noupdate  -group $g_par                   -divider       -height $h_nul  <NULL>
  }; # wave_par
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  # add wave : interface
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  if {$wave_io} {
  add wave -noupdate  -group $g_mod                                  -height $h_grp  -group $g_io
  add wave -noupdate  -group $g_io                    -divider       -height $h_nul  <NULL>
  add wave -noupdate  -group $g_io                    -divider       -height $h_ch1                        {//  clock ---------------------------------------------------------------------------------------------------------}

  add wave -noupdate  -group $g_io   -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_clk             } $module_path/i_clk
  add wave -noupdate  -group $g_io   -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_clk_en          } $module_path/i_clk_en
  add wave -noupdate  -group $g_io   -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_sclr_n          } $module_path/i_sclr_n

  add wave -noupdate  -group $g_io                    -divider       -height $h_ch1                        {//  frame body ----------------------------------------------------------------------------------------------------}

  add wave -noupdate  -group $g_io   -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { i_str_body        } $module_path/i_str_body
  add wave -noupdate  -group $g_io   -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { i_dat_body        } $module_path/i_dat_body
  add wave -noupdate  -group $g_io   -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { i_dat_load        } $module_path/i_dat_load

  add wave -noupdate  -group $g_io                    -divider       -height $h_ch1                        {//  fcs crc -------------------------------------------------------------------------------------------------------}

  add wave -noupdate  -group $g_io   -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { o_crc_rdy         } $module_path/o_crc_rdy
  add wave -noupdate  -group $g_io   -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { o_crc_err         } $module_path/o_crc_err
  add wave -noupdate  -group $g_io                    -divider       -height $h_nul  <NULL>
  }; # wave_io
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  # add wave : logic
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  if {$wave_log} {
  add wave -noupdate  -group $g_mod                                  -height $h_grp  -group $g_log
  add wave -noupdate  -group $g_log                   -divider       -height $h_nul  <NULL>
  add wave -noupdate  -group $g_log                   -divider       -height $h_ch1                        {//  sync clear ----------------------------------------------------------------------------------------------------}

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  clock                                                                                                         /}

  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { i_clk             } $module_path/i_clk

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  clear / external                                                                                              /}

  add wave -noupdate  -group $g_log  -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { i_sclr_n          } $module_path/i_sclr_n
  add wave -noupdate  -group $g_log  -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { ri_sclr_n         } $module_path/ri_sclr_n

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  clear / ini                                                                                                   /}

  add wave -noupdate  -group $g_log  -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { sclr_n_ini        } $module_path/sclr_n_ini
  add wave -noupdate  -group $g_log  -color $c_count  -format logic  -height $h_sig  -radix $r_uns  -label { sclr_n_ini_cnt    } $module_path/sclr_n_ini_cnt

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  clear / result                                                                                                /}

  add wave -noupdate  -group $g_log  -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { sclr_n            } $module_path/sclr_n
  add wave -noupdate  -group $g_log  -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { sclr_n_ini        } $module_path/sclr_n_ini
  add wave -noupdate  -group $g_log  -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { sclr_n_ext        } $module_path/sclr_n_ext

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch1                        {//  frame body ----------------------------------------------------------------------------------------------------}

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  clock                                                                                                         /}

  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { clk               } $module_path/i_clk
  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { clk_en            } $module_path/i_clk_en
  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { sclr_n            } $module_path/sclr_n

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  frame body / input                                                                                            /}

  add wave -noupdate  -group $g_log  -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { i_str_body        } $module_path/i_str_body
  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { i_dat_body        } $module_path/i_dat_body
  add wave -noupdate  -group $g_log  -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { i_dat_load        } $module_path/i_dat_load

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  frame body / work                                                                                             /}

  add wave -noupdate  -group $g_log  -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { str_body          } $module_path/str_body
  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { dat_body          } $module_path/dat_body
  add wave -noupdate  -group $g_log  -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { dat_load          } $module_path/dat_load

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch1                        {//  crc calculator ------------------------------------------------------------------------------------------------}

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  clock                                                                                                         /}

  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { clk               } $module_path/i_clk
  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { clk_en            } $module_path/i_clk_en
  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { sclr_n            } $module_path/sclr_n

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  frame body                                                                                                    /}

  add wave -noupdate  -group $g_log  -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { str_body          } $module_path/str_body
  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { dat_body          } $module_path/dat_body
  add wave -noupdate  -group $g_log  -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { dat_load          } $module_path/dat_load

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  crc calculator                                                                                                /}

  add wave -noupdate  -group $g_log  -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { crc_rdy           } $module_path/crc_rdy
  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { crc_sum           } $module_path/crc_sum

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch1                        {//  crc comparator ------------------------------------------------------------------------------------------------}

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  clock                                                                                                         /}

  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { clk               } $module_path/i_clk
  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { clk_en            } $module_path/i_clk_en
  add wave -noupdate  -group $g_log  -color $c_clock  -format logic  -height $h_sig  -radix $r_bin  -label { sclr_n            } $module_path/sclr_n

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  frame body                                                                                                    /}

  add wave -noupdate  -group $g_log  -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { str_body          } $module_path/str_body
  add wave -noupdate  -group $g_log  -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { str_body_fe       } $module_path/str_body_fe
  add wave -noupdate  -group $g_log  -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { str_body_fe_z     } $module_path/str_body_fe_z

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  crc compare                                                                                                   /}

  add wave -noupdate  -group $g_log  -color $c_pdata  -format logic  -height $h_sig  -radix $r_hex  -label { crc_sum           } $module_path/crc_sum
  add wave -noupdate  -group $g_log  -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { crc_compare       } $module_path/crc_compare

  add wave -noupdate  -group $g_log                   -divider       -height $h_ch2                        { >  crc error                                                                                                     /}

  add wave -noupdate  -group $g_log  -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { crc_check         } $module_path/crc_check
  add wave -noupdate  -group $g_log  -color $c_strob  -format logic  -height $h_sig  -radix $r_bin  -label { crc_err           } $module_path/crc_err
  add wave -noupdate  -group $g_log  -color $c_pulse  -format logic  -height $h_sig  -radix $r_bin  -label { crc_rdy           } $module_path/crc_rdy
  add wave -noupdate  -group $g_log                   -divider       -height $h_nul  <NULL>
  }; # wave_log
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  # add wave : all wave
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  if {$wave_all} {
  add wave -noupdate  -group $g_mod                                  -height $h_grp  -group $g_all
  add wave -noupdate  -group $g_all                   -divider       -height $h_nul  <NULL>
  add wave -noupdate  -group $g_all                                                                                              $module_path/*
  add wave -noupdate  -group $g_all                   -divider       -height $h_nul  <NULL>
  }; # wave_all
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

  add wave -noupdate  -group $g_mod                   -divider       -height $h_nul  <NULL>

  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#




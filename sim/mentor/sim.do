  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  #
  #  Company      : Micran, Department of Telecommunications
  #
  #  Engineer     : Pogodaev Danil
  #
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  #
  #  Library      : lib_ethernet / library of ethernet components
  #
  #  Component    : eth_rx_v2 / receiver of ethernet frames (packets)
  #
  #  File Name    : sim .do (tcl sim mentor)
  #
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  #
  #  Description  : Simulation script for Mentor simulator.
  #
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  #
  #  Note (1)     : Command 'vsim'                  The vsim command is used to invoke the VSIM simulator.
  #
  #                  -t [<multiplier>]<time_unit>   Specifies the simulator time resolution. <time_unit> must be one of the following: fs, ps, ns, us, ms, sec.
  #                                                 The default is 1ns; the optional <multiplier> may be 1, 10 or 100. 
  #                                                 Note that there is no space between the multiplier and the unit (for example, 10fs, not 10 fs).
  #
  #                                                 If you omit the -t argument, the default simulator time resolution depends on design type:
  #
  #                                                  - In a Verilog design with �timescale directives�the minimum specified time precision of all directives is used.
  #                                                  - In a Verilog design with no �timescale directives�the value specified for the Resolution variable in the modelsim.ini file is used.
  #
  #                                                 If there are no �timescale directives in the design, the value specified for the Resolution variable in modelsim.ini is used.
  #
  #                  -vopt                          Optimizing Designs. Instructs vsim to run the vopt command automatically if vopt was not manually invoked.
  #                                                 Refer to the chapter entitled "Optimizing Designs with vopt" for more information.
  #
  #                  -novopt                        Prevents simulator from running the vopt command automatically.
  #                                                 If you have the VoptFlow variable set to 1 (optimizations turned on) in the modelsim.ini file,
  #                                                 vsim automatically runs vopt if you didn�t invoke it manually.
  #
  #                  -lib <libname>                 Specifies the default working library where vsim will look for the design unit(s). Default is "work".
  #
  #                  -L <library_name> �            Specifies the library to search for design units instantiated from Verilog and for VHDL default component binding.
  #                                                 Refer to "Library Usage" for more information. If multiple libraries are specified, each must be preceded by the -L option.
  #                                                 Libraries are searched in the order in which they appear on the command line.
  #
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  #
  #  Note (2)     : Command 'run'                   The run command advances the simulation by the specified number of timesteps.
  #
  #                  -all                           Causes the simulator to run the current simulation forever, or until it hits a breakpoint or specified break event.
  #
  #                  <timesteps>[<time_units>]      Specifies the number of timesteps for the simulation to run.
  #                                                 The number may be fractional, or may be specified absolute by preceding the value with the character @.
  #                                                 In addition, optional <time_units> may be specified as: fs, ps, ns, us, ms, or sec.
  #
  #                                                 The default <timesteps> and <time_units> specifications can be changed during a simulator session by
  #                                                 selecting Simulate > Simulation Options (Main window). Time steps and time units may also be set with
  #                                                 the RunLength and UserTimeUnit variables in the modelsim.ini file.
  #
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  #
  #  Note (3)     : Command 'add wave'              The add wave command adds the following objects to the Wave window:
  #
  #                                                  � VHDL signals and variables
  #                                                  � Verilog nets and registers
  #                                                  � System Verilog class objects
  #                                                  � SystemC primitive channels (signals)
  #                                                  � Dividers and user-defined buses
  #
  #                  -noupdate                      A switch that prevents the Wave window from updating when a series of add wave commands are executed in series.
  #
  #                                                 If no port mode is specified, add wave will display all objects in the selected region with names matching the object
  #                                                 name specification.
  #
  #                  -expand <signal_name>          A switch and argument pair that instructs the command to expand a compound signal immediately, but only one level down.
  #                                                 <signal_name> � a string that specifies the name of the signal. This string can include wildcards.
  #
  #                  -group <group_name>            A switch and argument group that creates a wave group with the specified group_name.
  #                  [<sig_name1> ...]   
  #                                                 <group_name> � a string that specifies the name of the group. You must enclose this argument in quotes (") or braces ({ })
  #                                                 if it contains any white space.
  #
  #                                                 <sig_name> ... � a string, which is repeatable in a space separated list, that specifies the signals to add to the group.
  #                                                 This command creates an empty group if you do not specify any signal names.
  #
  #                  -height <pixels>               A switch and argument pair that specifies the height, in pixels, of the waveform.
  #
  #                  -color <standard_color_name>   A switch and argument pair that specifies the color used to display a waveform.
  #
  #                                                 <standard_color_name> � You can use either of the following:
  #
  #                                                  � standard X Window color name � enclose 2-word names in quotes ("), for example: -color "light blue"
  #                                                  � rgb value, for example: -color #357f77
  #  
  #                                                 Definitions of color names (e.g. steelblue or gainsboro) have been provided by the World Wide Web Consortium and
  #                                                 are available at www.w3.org/TR/SVG/types.html#ColorKeywords. Color transparent represents the absence of a color.
  #
  #                  -<format>                      A choice between switches that specify the display format of the objects. Valid entries are:
  #
  #                                                  -format literal               Literal waveforms are displayed as a box containing the object value.
  #                                                  -format logic                 Logic signals may be U, X, 0, 1, Z, W, L, H, or �-�.
  #                                                  -format analog-step           Analog-step changes to the new time before plotting the new Y.
  #                                                  -format analog-interpolated   Analog-interpolated draws a diagonal line.
  #                                                  -format analog-backstep       Analog-backstep plots the new Y before moving to the new time.
  #                                                  -format event                 Displays a mark at every transition.
  #
  #                  -radix <type>                  A choice between switches that specify the radix for the objects that follow in the command.
  #                                                 Valid entries (or any unique abbreviations) are:
  #                  -<radix_type>   
  #                                                  -radix binary    -binary
  #                                                  -radix ascii     -ascii
  #                                                  -radix unsigned  -unsigned
  #                                                  -radix decimal   -decimal
  #                                                  -radix octal     -octal
  #                                                  -radix hex       -hex
  #                                                  -radix symbolic  -symbolic
  #                                                  -radix time      -time
  #                                                  -radix default   -default
  #
  #                                                 If no radix is specified for an enumerated type, the default representation is used.
  #
  #                  -divider [<divider_name>]      A switch and argument pair that adds a divider to the Wave window.
  #
  #                                                 <divider_name> � A string, which is repeatable in a space separated list, that specifies the name of the divider,
  #                                                 which appears in the pathnames column.
  #
  #                                                 You cannot begin a name with a hyphen (-).
  #                                                 You can begin a name with a space, but you must enclose the name within quotes (") or braces ({ })
  #                                                 If you do not specify this argument, the command inserts an unnamed divider.
  #
  #                  -label <name>                  A switch and argument pair that specifies an alternative name for the signal being added.
  #                                                 This alternative name is not valid in a force or examine command; however, it can be used in a search command
  #                                                 with the wave option.
  #
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  #
  #  Revision List
  #
  #  Version   Date       Author
  #
  #  2.0       26.09.16   Pogodaev Danil
  #
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  # invokes the simulator and arguments setting
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

  vsim -t 1ns -novopt -lib work  work.tb_top ; # tb_top as the top level module

  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  # run time setting
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

  set run_time 10000000 ; # measure : time resolution (vsim -t)

  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  # waveform : module selection
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

  #---------------------------------------------------------------------------------------------------------------------------------|
  # top testbench                                                                                                                   |
  #---------------------------------------------------------------------------------------------------------------------------------|
  #                              |                                                             |               |                    |
  set wave_top      1   ; # top  | receiver of ethernet frames (packets) / tb top              | eth_rx        | eth_rx_v2          |
  #                 ^ ^          |                                                             |               |                    |
  #--------------------------------------------------------------------------------------------|---------------|--------------------|
  # top generator                                                                              |               |                    |
  #--------------------------------------------------------------------------------------------|---------------|--------------------|
  #                              |                                                             |               |                    |
  set wave_tbg      1   ; # gen  | receiver of ethernet frames (packets) / tb generator        | tb_gen        | tb_gen             |
  #                 ^ ^          |                                                             |               |                    |
  #--------------------------------------------------------------------------------------------|---------------|--------------------|
  # top generator / ethernet emulator                                                          |               |                    |
  #--------------------------------------------------------------------------------------------|---------------|--------------------|
  #                              |                                                             |               |                    |
  set wave_tbg_1    1   ; # 1.0  | emulator of ethernet frames                                 | eth_emu       | tb_eth_emu_v2      |
  set wave_tbg_1_x  1   ; # 1.x  | emulator of ethernet frames / tb generator                  | tb_gen_emu    | tb_gen_emu         | value: 0 1 0 (!)
  set wave_tbg_1_y  0   ; # 1.y  | emulator of ethernet frames / tb generator / manual mode    | tb_gen_emu_m  | tb_gen_emu_m       | value: 0 0 1 (!)
  #                 ^ ^          |                                                             |               |                    |
  set wave_tbg_1_1    0 ; # 1.1  | generator of initialization signal                          | emu_ini       | tb_eth_emu_ini_v2  |
  set wave_tbg_1_2    0 ; # 1.2  | clocks generator of ethernet frames                         | emu_clk       | tb_eth_emu_clk_v2  |
  set wave_tbg_1_3    0 ; # 1.3  | strobes generator of ethernet frames                        | emu_str       | tb_eth_emu_str_v2  |
  set wave_tbg_1_4    0 ; # 1.4  | data stream generator of ethernet frames                    | emu_dat       | tb_eth_emu_dat_v2  |
  set wave_tbg_1_5    0 ; # 1.5  | lan ethernet interface implementation                       | emu_lan       | tb_eth_emu_lan_v2  |
  set wave_tbg_1_6    0 ; # 1.6  | speed calculator (calculator of gap)                        | emu_spd       | tb_eth_emu_spd_v2  |
  #                 ^ ^          |                                                             |               |                    |
  #--------------------------------------------------------------------------------------------|---------------|--------------------|
  # lib component                                                                              |               |                    |
  #--------------------------------------------------------------------------------------------|---------------|--------------------|
  #                              |                                                             |               |                    |
  set wave_lib_1    1   ; # 1.0  | ethernet preliminary receiver for custom mode               | rx_cst        | eth_rx_cst_v2      |
  set wave_lib_2    1   ; # 2.0  | ethernet preliminary receiver for mii-interface             | rx_mii        | eth_rx_mii_v2      |
  set wave_lib_3    1   ; # 3.0  | ethernet preliminary receiver for rmii-interface            | rx_rmii       | eth_rx_rmii_v2     |
  set wave_lib_4    1   ; # 4.0  | ethernet preliminary receiver for gmii-interface            | rx_gmii       | eth_rx_gmii_v2     |
  set wave_lib_5    1   ; # 5.0  | multiplexer of ethernet preliminary receivers               | rx_mux        | eth_rx_mux_v2      |
  set wave_lib_6    1   ; # 6.0  | frame header detector (preamble and start-frame delimiter)  | rx_head       | eth_rx_head_v2     |
  set wave_lib_7    1   ; # 7.0  | strobe generator and length calculator for ethernet frames  | rx_str        | eth_rx_str_v2      |
  set wave_lib_8    1   ; # 8.0  | data extractor for ip and mac headers                       | rx_dat        | eth_rx_str_v2      |
  set wave_lib_9    1   ; # 9.0  | counters of ethernet frames (packets)                       | rx_cnt        | eth_cnt_v2         |
  set wave_lib_9_1    0 ; # 9.1  | crc calculator for received ethernet frames                 | rx_crc        | eth_rx_crc_v2      |
  set wave_lib_10   1   ; # 10.0 | output registers and latching data (mac and ip headers)     | rx_out        | eth_rx_out_v2      |
  #                 ^ ^          |                                                             |               |                    |
  #---------------------------------------------------------------------------------------------------------------------------------|

  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  # waveform : radix setting
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

  set r_hex "hexadecimal"
  set r_uns "unsigned"
  set r_bin "binary"
  set r_asc "ascii"

  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  # waveform : height setting
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

  set h_mod 30 ; # module name
  set h_grp 30 ; # group name
  set h_ch1 30 ; # divider (chapter 1)
  set h_ch2 20 ; # divider (chapter 2)
  set h_sig 20 ; # signal
  set h_par 20 ; # parameters
  set h_nul 20 ; # null

  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  # waveform : available colors
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

  #  < white / black >              < yellow >                < red >                           < green >                           < blue >                          < violet >           < default >

  set c_white "White"  ; set c_ylw01 "Yellow"    ; set c_red01 "Red"               ; set c_grn01 "Green"               ; set c_blu01 "Cadet Blue"        ; set c_vio01 "Dark Orchild"   ; set c_def {}  ;
  set c_black "Black"  ; set c_ylw02 "Gold"      ; set c_red02 "Orange Red"        ; set c_grn02 "Spring Green"        ; set c_blu02 "Medium Aquamarine" ; set c_vio02 "Medium Orchild" ;               ;
  set c_gry10 "Gray10" ; set c_ylw03 "Khaki"     ; set c_red03 "Indian Red"        ; set c_grn03 "Medium Spring Green" ; set c_blu03 "Aquamarine"        ; set c_vio03 "Orchild"        ;               ;
  set c_gry20 "Gray20" ; set c_ylw04 "Wheate"    ; set c_red04 "Firebrick"         ; set c_grn04 "Green Yellow"        ; set c_blu04 "Turquoise"         ; set c_vio04 "Magnenta"       ;               ;
  set c_gry30 "Gray30" ; set c_ylw05 "Tan"       ; set c_red05 "Brown"             ; set c_grn05 "Pale Green"          ; set c_blu05 "Cyan"              ; set c_vio05 "Violet"         ;               ;
  set c_gry40 "Gray40" ; set c_ylw06 "Goldenrod" ; set c_red06 "Sienna"            ; set c_grn06 "Yellow Green"        ; set c_blu06 "Blue"              ; set c_vio06 "Plum"           ;               ;
  set c_gry50 "Gray50" ; set c_ylw07 "Orange"    ; set c_red07 "Maroon"            ; set c_grn07 "Lime Green"          ; set c_blu07 "Medium Blue"       ; set c_vio07 "Thistle"        ;               ;
  set c_gry55 "Gray55" ; set c_ylw08 "Coral"     ; set c_red08 "Medium Violet Red" ; set c_grn08 "Medium Sea Green"    ; set c_blu08 "Medium State Blue" ; set c_vio08 "Blue Violet"    ;               ;
  set c_gry60 "Gray60" ; set c_ylw09 "Salmon"    ; set c_red09 "Violet Red"        ; set c_grn09 "Sea Green"           ; set c_blu09 "Cornflower Blue"   ; set c_vio09 "Dark Orchild"   ;               ;
  set c_gry65 "Gray65" ;                         ; set c_red10 "Pink"              ; set c_grn10 "Forest Green"        ; set c_blu10 "Sky Blue"          ;                              ;               ;
  set c_gry70 "Gray70" ;                         ;                                 ; set c_grn11 "Olive Drab"          ; set c_blu11 "Light Blue"        ;                              ;               ;
  set c_gry75 "Gray75" ;                         ;                                 ; set c_grn12 "Dark Olive Green"    ; set c_blu12 "Light Steel Blue"  ;                              ;               ;
  set c_gry80 "Gray80" ;                         ;                                 ; set c_grn13 "Dark Green"          ; set c_blu13 "Steel Blue"        ;                              ;               ;
  set c_gry90 "Gray90" ;                         ;                                 ; set c_grn14 "Dark Slate Gray"     ; set c_blu14 "Dark State Blue"   ;                              ;               ;
                       ;                         ;                                 ;                                   ; set c_blu15 "Navy"              ;                              ;               ;
                       ;                         ;                                 ;                                   ; set c_blu16 "Midnight Blue"     ;                              ;               ;
                       ;                         ;                                 ;                                   ; set c_blu17 "Slate Blue"        ;                              ;               ;

  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  # waveform : color setting
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

  set color_on 0 ; # def: 0

  if ($color_on) {

    set c_param $c_gry60 ; # parameters : $c_def : $c_gry60
    set c_clock $c_gry70 ; # clock      : $c_def : $c_gry70
    set c_strob $c_grn07 ; # strob      : $c_def : $c_grn07
    set c_pulse $c_grn07 ; # pulse      : $c_def : $c_grn07
    set c_sdata $c_grn08 ; # sdata      : $c_def : $c_grn08
    set c_pdata $c_grn08 ; # pdata      : $c_def : $c_grn08
    set c_count $c_grn08 ; # counter    : $c_def : $c_grn08
    set c_array $c_grn08 ; # array      : $c_def : $c_grn08

  } else { # default

    set c_param $c_def ; # parameters : $c_def : $c_gry60
    set c_clock $c_def ; # clock      : $c_def : $c_gry70
    set c_strob $c_def ; # strob      : $c_def : $c_grn07
    set c_pulse $c_def ; # pulse      : $c_def : $c_grn07
    set c_sdata $c_def ; # sdata      : $c_def : $c_grn08
    set c_pdata $c_def ; # pdata      : $c_def : $c_grn08
    set c_count $c_def ; # counter    : $c_def : $c_grn08
    set c_array $c_def ; # array      : $c_def : $c_grn08
  }
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  # waveform : add do-files
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

  # top testbench

  if {$wave_top     } {do $dir_sim/wave/wave_top.do     };

  # top generator 

  if {$wave_tbg     } {do $dir_sim/wave/wave_tbg.do     };

  # top generator / emulator

  if {$wave_tbg_1   } {do $dir_sim/wave/wave_tbg_1.do   };
  if {$wave_tbg_1_x } {do $dir_sim/wave/wave_tbg_1_x.do };
  if {$wave_tbg_1_y } {do $dir_sim/wave/wave_tbg_1_y.do };
  if {$wave_tbg_1_1 } {do $dir_sim/wave/wave_tbg_1_1.do };
  if {$wave_tbg_1_2 } {do $dir_sim/wave/wave_tbg_1_2.do };
  if {$wave_tbg_1_3 } {do $dir_sim/wave/wave_tbg_1_3.do };
  if {$wave_tbg_1_4 } {do $dir_sim/wave/wave_tbg_1_4.do };
  if {$wave_tbg_1_5 } {do $dir_sim/wave/wave_tbg_1_5.do };
  if {$wave_tbg_1_6 } {do $dir_sim/wave/wave_tbg_1_6.do };

  # lib component

  if {$wave_lib_1   } {do $dir_sim/wave/wave_lib_1.do   };
  if {$wave_lib_2   } {do $dir_sim/wave/wave_lib_2.do   };
  if {$wave_lib_3   } {do $dir_sim/wave/wave_lib_3.do   };
  if {$wave_lib_4   } {do $dir_sim/wave/wave_lib_4.do   };
  if {$wave_lib_5   } {do $dir_sim/wave/wave_lib_5.do   };
  if {$wave_lib_6   } {do $dir_sim/wave/wave_lib_6.do   };
  if {$wave_lib_7   } {do $dir_sim/wave/wave_lib_7.do   };
  if {$wave_lib_8   } {do $dir_sim/wave/wave_lib_8.do   };
  if {$wave_lib_9   } {do $dir_sim/wave/wave_lib_9.do   };
  if {$wave_lib_9_1 } {do $dir_sim/wave/wave_lib_9_1.do };
  if {$wave_lib_10  } {do $dir_sim/wave/wave_lib_10.do  };

  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
  # running the simulation
  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#

  run $run_time

  #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#



